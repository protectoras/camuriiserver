object CIISMainFrm: TCIISMainFrm
  Left = 1247
  Top = 53
  Caption = 'Camur II Server'
  ClientHeight = 341
  ClientWidth = 499
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object BBReset: TBitBtn
    Left = 395
    Top = 300
    Width = 94
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Reset'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 0
  end
  object BBDebugWin: TBitBtn
    Left = 294
    Top = 300
    Width = 94
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Debug'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555500000000
      0555555F7777777775F55500FFFFFFFFF0555577F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF777FFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF7F777FF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF77F7FFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7FF5F5F5F57F550F00F0F0F0F
      005557F77F7F7F7F77555055070707070555575F7F7F7F7F7F55550507070707
      0555557575757575755555505050505055555557575757575555}
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BBDebugWinClick
  end
  object GBLoggerIP: TGroupBox
    Left = 280
    Top = 89
    Width = 191
    Height = 51
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Logger IP'
    TabOrder = 2
    object EdLocalIP: TEdit
      Left = 5
      Top = 19
      Width = 166
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 0
    end
  end
  object GBServerPortsConnected: TGroupBox
    Left = 24
    Top = 10
    Width = 191
    Height = 71
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Active Client'
    TabOrder = 3
    object LCIIPM: TLabel
      Left = 53
      Top = 20
      Width = 71
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Camur II PM'
    end
    object LCIIMonitor: TLabel
      Left = 53
      Top = 40
      Width = 95
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Camur II Monitor'
    end
    object ShPMConnected: TShape
      Left = 16
      Top = 23
      Width = 22
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object ShMonitorConnected: TShape
      Left = 16
      Top = 43
      Width = 22
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
  end
  object GBClientPorts: TGroupBox
    Left = 24
    Top = 89
    Width = 191
    Height = 51
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Active Server'
    TabOrder = 4
    object LCIILogger: TLabel
      Left = 53
      Top = 20
      Width = 94
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Camur II Logger'
    end
    object ShLoggerConnected: TShape
      Left = 16
      Top = 23
      Width = 22
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
  end
  object BBStartCIISUser: TBitBtn
    Left = 280
    Top = 233
    Width = 191
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Start CIISUser'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 5
    OnClick = BBStartCIISUserClick
  end
  object GBSysInfo: TGroupBox
    Left = 24
    Top = 148
    Width = 187
    Height = 57
    Caption = 'System Info'
    TabOrder = 6
    object LTSysValue: TLabel
      Left = 18
      Top = 25
      Width = 7
      Height = 16
      Caption = '0'
    end
    object LTSys: TLabel
      Left = 53
      Top = 25
      Width = 64
      Height = 16
      Caption = 'T Sys ( % )'
    end
  end
  object TimerSysCheck: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerSysCheckTimer
    Left = 224
    Top = 16
  end
end
