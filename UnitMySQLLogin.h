//---------------------------------------------------------------------------

#ifndef UnitMySQLLoginH
#define UnitMySQLLoginH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormMySQLLogin : public TForm
{
__published:	// IDE-managed Components
	TBitBtn *BitBtnYes;
	TLabeledEdit *LabeledEditHostName;
	TLabeledEdit *LabeledEditUserName;
	TLabeledEdit *LabeledEditPassword;
	TBitBtn *BitBtnNo;
	TLabeledEdit *LabeledEditDbName;
private:	// User declarations
public:		// User declarations
	__fastcall TFormMySQLLogin(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMySQLLogin *FormMySQLLogin;
//---------------------------------------------------------------------------
#endif
