//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISCorrRate.h"
#include "math.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

__fastcall TCIISCorrRate::TCIISCorrRate( TCIISDBModule *SetDB, TDebugWin *SetDebugWin )
{
  DB = SetDB;
  DW = SetDebugWin;
  LPRValList = new TList;
  T1 = new TList;
  T2 = new TList;
}

__fastcall TCIISCorrRate::~TCIISCorrRate()
{
  for( int32_t i = LPRValList->Count - 1; i >= 0; i-- )
  {
		CIISLPRValueRec *LPRValRec = (CIISLPRValueRec*)LPRValList->Items[i];
		delete LPRValRec;
		LPRValList->Delete(i);
  }

  for( int32_t i = T1->Count - 1; i >= 0; i-- )
  {
		CIISCorrRateValueRec *CRValRec = (CIISCorrRateValueRec*)T1->Items[i];
		delete CRValRec;
		T1->Delete(i);
  }
  for( int32_t i = T2->Count - 1; i >= 0; i-- )
  {
		CIISCorrRateValueRec *CRValRec = (CIISCorrRateValueRec*)T2->Items[i];
		delete CRValRec;
		T2->Delete(i);
  }

  delete LPRValList;
  delete T1;
	delete T2;

}

void __fastcall TCIISCorrRate::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

int32_t __fastcall TCIISCorrRate::CalcCorrRate( int32_t Recording, int32_t Sensor, String ValuType )
{
  int32_t NoOffValuesStartToMax, NoOffValuesMinToMax, NoOffLPRValues, NoOffCalcValues;
  int32_t LPRValuesCount, LPRStep;
  bool MoreRecords;
  int32_t T1Start, T1End, T2Start, T2End, LPRValNo;
  double k_T1, k_T2, i_T1, i_T2;
  double SumV_T1, SumI_T1, SumVV_T1, SumVI_T1;
  double SumV_T2, SumI_T2, SumVV_T2, SumVI_T2;
  CIISRecordingRec RR;

  FResultQuality = -1; // -2 = uncompleted recording, -1 = rectype error, 2-8 no off datapoints

	if( DB->LocateRecordingNo( Recording ) ) DB->GetRecordingRec( &RR, false );
	else return -1;

	if(( RR.RecType != RT_LPR ) || ( RR.LPRMode != 0 ) || ( RR.LPRStep == 0 )) return -1;

  NoOffValuesStartToMax = RR.LPRRange/RR.LPRStep;
  NoOffValuesMinToMax = (2*RR.LPRRange)/RR.LPRStep;
  NoOffLPRValues = 1 + NoOffValuesStartToMax +
									 1 + NoOffValuesMinToMax +
									 1 + NoOffValuesMinToMax;
  NoOffCalcValues = min( 8, NoOffValuesStartToMax );
  FResultQuality = NoOffCalcValues;

  DB->SetLPRQuery(RR.RecStart, Recording, Sensor, ValuType );

  #if DebugCIISCorrRate == 1
  Debug(
  "SetLPRQuery : Recording = " + IntToStr(Recording) +
  " Sensor = " + IntToStr(Sensor) +
  " ValuType = " + ValuType
  );
  #endif


  LPRValuesCount = DB->GetLPRLocalValuesRecCount();

  #if DebugCIISCorrRate == 1
  Debug(
  "GetLPRLocalValuesRecCount : LPRValuesCount = " + IntToStr(LPRValuesCount));
  #endif


  if(( LPRValuesCount != NoOffLPRValues ) || ( LPRValuesCount == 0 )) return -2;

  T1End = 1 + NoOffValuesStartToMax + 1 + NoOffValuesMinToMax;
  T1Start = T1End - NoOffCalcValues + 1;
  T2End = NoOffLPRValues;
  T2Start = T2End - NoOffCalcValues + 1;

  LPRStep = 0;
  SumV_T1 = 0;
  SumI_T1 = 0;
  SumVV_T1 = 0;
  SumVI_T1 = 0;
  SumV_T2 = 0;
  SumI_T2 = 0;
  SumVV_T2 = 0;
  SumVI_T2 = 0;

  DB->FindFirstLPRLocalValue();

  #if DebugCIISCorrRate == 1
  Debug(
  "FindFirstLPRLocalValue"
  );
  #endif

  do
  {
    CIISLPRValueRec LPRValRec;

		MoreRecords = DB->GetLPRValueRec( &LPRValRec, true);
		LPRStep++;

		#if DebugCIISCorrRate == 1
		Debug(
		"GetLPRValueRec : LPRStep = " + IntToStr(LPRStep) +
		" ValueV = " + FloatToStr(LPRValRec.ValueV) +
		" ValueI = " + FloatToStr(LPRValRec.ValueI)
		);
		#endif

		if(( LPRStep >= T1Start ) && ( LPRStep <= T1End ))
		{
			SumV_T1	+= LPRValRec.ValueV;
			SumVV_T1	+= pow( LPRValRec.ValueV, 2 );
			SumI_T1	+= LPRValRec.ValueI;
			SumVI_T1	+= LPRValRec.ValueV * LPRValRec.ValueI;
		}
		if(( LPRStep >= T2Start ) && ( LPRStep <= T2End ))
		{
			SumV_T2	+= LPRValRec.ValueV;
			SumVV_T2	+= pow(LPRValRec.ValueV, 2);
			SumI_T2	+= LPRValRec.ValueI;
			SumVI_T2	+= LPRValRec.ValueV * LPRValRec.ValueI;
		}

	} while ( MoreRecords );

	try
	{
		k_T1 = 	( NoOffCalcValues * SumVI_T1 - SumV_T1 * SumI_T1 )/
						( NoOffCalcValues * SumVV_T1 - SumV_T1 * SumV_T1 );

		k_T2 = 	( NoOffCalcValues * SumVI_T2 - SumV_T2 * SumI_T2 )/
						( NoOffCalcValues * SumVV_T2 - SumV_T2 * SumV_T2 );
		i_T1 = k_T1 * SternGearyConst;
		i_T2 = k_T2 * SternGearyConst;

		FICorr = ( i_T1 + i_T2 ) / 2;
		FISigma = sqrt( pow( i_T1 - FICorr, 2 ) + pow( i_T2 - FICorr, 2 ) );
	}
	catch (Exception &exception)
	{
		FICorr = 0;
		FISigma = 0;
		FResultQuality = -3;
	}

  #if DebugCIISCorrRate == 1
  Debug(
  "Result : ICorr = " + FloatToStr(FICorr) +
  " ISigma = " + FloatToStr(FISigma)
  );
  #endif

	return FResultQuality;

}

/*
int32_t __fastcall TCIISCorrRate::Test_GenData( int32_t Sensor, int32_t ZoneNo )
{
  TDateTime DTS;
  CIISRecordingRec RR;


	if( DB->FindLastRecording() )
	{
	    DB->GetRecordingRec( &RR, false );
	    RR.RecNo = RR.RecNo + 1;
	}
	else
        RR.RecNo = 1;

	RR.RecStart = Now();
	RR.RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	RR.RecType = RT_LPR;
	RR.SampInterval= 0;
	RR.ZoneNo = ZoneNo;
	RR.DecayDelay = 0;
	RR.DecaySampInterval = 0;
	RR.DecaySampInterval2 = 0;
	RR.DecayDuration = 0;
	RR.DecayDuration2 = 0;
	RR.LPRRange = 20;
	RR.LPRStep = 2;
	RR.LPRDelay1 = 20;
	RR.LPRDelay2 = 0;
	RR.LPRMode = 0;

	DB->AppendRecordingRec( &RR );
    DB->ApplyUpdatesRecording();

	DTS = Now();
	Test_AddRec( DTS, RR.RecNo, Sensor, -424.51,	0.0064952 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -422.52,	0.0412910 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -420.54,	0.0739210 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -418.63,	0.1049300 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -416.57,	0.1357000 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -414.51,	0.1651600 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -412.53,	0.1942400 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -410.47,	0.2220700 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -408.56,	0.2474400 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -406.65,	0.2734200 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -404.59,	0.3012500 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -404.36,	0.2925100 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -406.42,	0.2473600 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -408.33,	0.2091600 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -410.32,	0.1712700 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -412.3 ,	0.1357000 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -414.36,	0.1029200 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -416.5 ,	0.0685090 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -418.4 ,	0.0384300 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -420.46,	0.0068818 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -422.45	,-0.0237380 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -424.51	,-0.0525030 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -426.34	,-0.0782510 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -428.24	,-0.1060100 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -430.38	,-0.1343900 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -432.52	,-0.1621500 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -434.5	,-0.1882800 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -436.48	,-0.2141900 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -438.47	,-0.2385400 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -440.38	,-0.2625900 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -442.36	,-0.2871800 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -444.27	,-0.3107600 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -444.72	,-0.3052000 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -442.74	,-0.2615900 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -440.68	,-0.2210700 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -438.7	,-0.1843400 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -436.79	,-0.1498500 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -434.81	,-0.1161400 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -432.82	,-0.0818080 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -430.76	,-0.0484050 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -428.78	,-0.0176300 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -426.72,	0.0119850 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -424.73,	0.0405950 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -422.75,	0.0705190 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -420.77,	0.0975820 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -418.63,	0.1257300 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -416.72,	0.1535600 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -414.66,	0.1811700 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -412.6 ,	0.2089300 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -410.7 ,	0.2329800 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -408.71,	0.2595800 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -406.73,	0.2847800 );
	Test_AddRec( DTS, RR.RecNo, Sensor, -404.67,	0.3110000 );

	return RR.RecNo;
}

void __fastcall TCIISCorrRate::Test_AddRec( TDateTime DTS, int32_t RecNo, int32_t SerNo, double ValV, double ValI )
{
  CIISLPRValueRec LPRValRec;

  LPRValRec.DateTimeStamp = DTS;
  LPRValRec.RecNo = RecNo;
  LPRValRec.SensorSerialNo = SerNo;
  LPRValRec.ValueType = "C1";
  LPRValRec.ValueV = ValV;
  LPRValRec.ValueI = ValI;

  DB->AppendLPRValueRec( &LPRValRec );
  DB->ApplyUpdatesLPRValue();
}
*/


