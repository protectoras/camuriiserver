//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CIIServerSocket.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

__fastcall TCIIServerSocket::TCIIServerSocket(MessInQueueFP SetMInQ, int32_t SetPortNo, TDebugWin * SetDebugWin)
{
  MInQ = SetMInQ;
  DW = SetDebugWin;

  PortNo = SetPortNo;
	ReceivedMessageList = new TList;   // K� f�r inkommande meddelanden
	SendMessageList = new TList;  // K� f�r utg�ende meddelanden

  SocketInputIdx = 0;

  FClientConnected = false;
	FClientLoggedIn = false;
  FPrjSelected = false;
  FPrjSelOnline = false;
  FUserLevel = ULNone;
  ServSocket = new TServerSocket( NULL );
  ServSocket->OnAccept = SocketAccept;
  ServSocket->OnClientConnect = ClientConnect;
  ServSocket->OnClientDisconnect = ClientDisconnect;
  ServSocket->OnClientError = ClientOnError;
  ServSocket->OnClientRead = ClientRead;
  ServSocket->Port = PortNo;
  ServSocket->ServerType = stNonBlocking;
  ServSocket->Tag = 0;
  ServSocket->ThreadCacheSize = 10;
	ServSocket->Open();

	FClientAktiv = true;

  #if DebugServerSocket == 1
  Debug("CIIServerSocket Created on port " + IntToStr( PortNo ));
  #endif
}

__fastcall TCIIServerSocket::~TCIIServerSocket()
{
	delete ReceivedMessageList;
	delete SendMessageList;
  ServSocket->Close();
	delete ServSocket;
}

#pragma argsused
void __fastcall TCIIServerSocket::SocketAccept(TObject * Sender, TCustomWinSocket * Socket)
{
	if(FClientConnected && CWSocket->RemoteAddress != Socket->RemoteAddress)
	{
		#if DebugServerSocket == 1
		Debug( "Refusing connection from " + Socket->RemoteAddress);
		#endif
		Socket->Close();
	}
	else
	{
		SocketInputIdx = 0;
		ResponseCount = 0;
		#if DebugServerSocket == 1
		Debug( "Accepted connection from " + Socket->RemoteAddress );
		#endif
		CWSocket = Socket;
		FClientConnected = true;
	}




}

#pragma argsused
void __fastcall TCIIServerSocket::ClientConnect(TObject * Sender, TCustomWinSocket * Socket)
{
	if(FClientConnected && CWSocket->RemoteAddress != Socket->RemoteAddress)
	{
		#if DebugServerSocket == 1
		Debug( "Second connection from " + Socket->RemoteAddress);
		#endif
	}
	else
	{
		#if DebugServerSocket == 1
		Debug( "Incoming connection from " + Socket->RemoteAddress )
		#endif
		;
	}

}

#pragma argsused
void __fastcall TCIIServerSocket::ClientDisconnect(TObject * Sender, TCustomWinSocket * Socket)
{
	if(CWSocket->RemoteAddress != Socket->RemoteAddress)
  {
    #if DebugServerSocket == 1
		Debug( "Refused connection disconnected " + Socket->RemoteAddress);
    #endif
	}
	else
	{
		CWSocket = NULL;
		FClientConnected = false;
		FClientLoggedIn = false;
		FPrjSelected = false;
		FPrjSelOnline = false;
		FUserLevel = ULNone;

		#if DebugServerSocket == 1
		Debug( "Closed connection from " + Socket->RemoteAddress );
		#endif
	}



}

#pragma argsused
void __fastcall TCIIServerSocket::ClientOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int32_t &ErrorCode)
{
	ErrorCode = 0;

	CWSocket = NULL;
	FClientConnected = false;
	FClientLoggedIn = false;
	FPrjSelected = false;
	FPrjSelOnline = false;
	FUserLevel = ULNone;

  ErrorCode = 0;
	#if DebugServerSocket == 1
	Debug(  "ClientOnError. Error = " + IntToStr(ErrorCode)  );
	#endif
}

#pragma argsused
void __fastcall TCIIServerSocket::ClientRead(TObject * Sender, TCustomWinSocket * Socket)
{
	int32_t b;

	#if DebugMsg == 1
	Debug( "ServerSocket: ClientRead" );
	#endif
	FClientAktiv = true;
	b = Socket->ReceiveBuf( SocketInBuf, SocketInBufSize );
	while( b > 0 )
	{
		if( b > 0 ) AddSocketInputQueue( SocketInBuf, b );
		b = Socket->ReceiveBuf( SocketInBuf, SocketInBufSize );
	}
}

void __fastcall TCIIServerSocket::AddSocketInputQueue(char* Buf, int32_t N)
{
	#if DebugMsg == 1
	Debug( "ServerSocket: AddSocketInputQueue" );
	#endif

	Move( Buf, SocketInputQueue + SocketInputIdx, N );
	SocketInputIdx += N;
	#if DebugServerSocket == 1
	Debug( "SocketInputQueue: Adding " + IntToStr( N ) + ", L = " + IntToStr( SocketInputIdx ) + " bytes" );
	#endif


	while( ParseMessages() );  //Packar upp och kontrollerar inkomna meddelanden

  if( MInQ != NULL ) MInQ();  // Signalera att meddelande finns att h�mta
}

bool __fastcall TCIIServerSocket::ParseMessages()
{

  int32_t c, n, ArgStart, ArgEnd, ArgNum;
  bool FirstArg;
  String s, ArgVal;
  TCamurPacket *p;
  TDataArg *Arg;
  int32_t start = -1;
  int32_t stop = -1;
	int32_t x = 0;

	#if DebugMsg == 1
	Debug( "ServerSocket: ParseMessages" );
	#endif


  while( ((start == -1) || (stop == -1)) && ( x < SocketInputIdx ))
  {
    if(( start == -1) && (SocketInputQueue[x] == 1))
    {
      start = x;
      stop = start + SocketInputQueue[x+4]+SocketInputQueue[x+5]*256+9;
      if( stop > SocketInputIdx )
      {
        stop = -1;
        x = SocketInputIdx;
      }
    }
    x++;
  }
  if( (start >= 0) && (stop >= 0) )
  {
    #if DebugServerSocket == 1
    Debug( "SocketInputQueue: Message found " + IntToStr( start ) + " - " + IntToStr( stop ));
    #endif

    SocketInputIdx = SocketInputIdx - stop;
    //CRC16Full( W||d(c), @SocketInputQueue[start], stop-3-start );
    p = new TCamurPacket;
    p->PortNo = PortNo;
    p->MessageType = SocketInputQueue[start+6];
    p->MessageNumber = SocketInputQueue[start+7];
    p->MessageCode = SocketInputQueue[start+8];
    p->MessageCommand = SocketInputQueue[start+9];
    p->MessageTable = SocketInputQueue[start+10];
    c = SocketInputQueue[start+11]+SocketInputQueue[start+12]*256;
    for( int32_t i = 1; i <= c ; i++ )
      p->MessageData = p->MessageData + Char(SocketInputQueue[start+12+i]);
    // Dela upp MessageData i argument
    p->Args->Clear();
    s = p->MessageData;
    n = 99;

    while( ( s.Length() > 0 ) && (( s[1] == 0xA ) || ( s[1] == 0xD )) ) s.Delete(1,1);
    FirstArg = true;
    while( s.Length() > 0 )
    {
      ArgStart =  s.Pos( "=" );
      try
      {
        ArgNum = StrToInt( s.SubString( 1, ArgStart - 1 ));
        s.Delete( 1, ArgStart ); // Remove "xx="
      }
      catch (Exception &exception)
      {
        ArgNum = n;
      }
      ArgEnd = min( s.Pos( "\n" ), s.Pos( "\r" ));
      if( ArgEnd == 0 ) ArgEnd = s.Length();
      ArgVal = s.SubString(1, ArgEnd - 1 );
      s.Delete(1, ArgEnd ); // Remove arg
      // Remove termination sign/signs
      while(( s.Length() > 0 ) && (( s[1] == 0xA ) || ( s[1] == 0xD )))
        s.Delete( 1, 1 );
      if( FirstArg )
      {
        Arg = (TDataArg*)p->Args->Add();
        Arg->Num = ArgNum;
        n = ArgNum;
        Arg->Val = ArgVal;
        FirstArg = false;
      }
      else
      {
        if( Arg->Num == ArgNum ) Arg->Val = Arg->Val + "\r" + "\n" + ArgVal;
        else
        {
          Arg = (TDataArg*)p->Args->Add();
          Arg->Num = ArgNum;
          n = ArgNum;
          Arg->Val = ArgVal;
        }
      }
		}
		ReceivedMessageList->Add( p );
    Move( SocketInputQueue + stop, SocketInputQueue, SocketInputIdx );

    #if DebugServerSocket == 1
    Debug( "Recived on port " + IntToStr( PortNo ) );
    Debug( "Type: " + IntToStr( p->MessageType ) +
           " / Number: " + IntToStr( p->MessageNumber ) +
           " / Code: " + IntToStr( p->MessageCode ) +
           " / Command: " + IntToStr( p->MessageCommand ) +
           " / Table: " + IntToStr( p->MessageTable ) +
           " / Length: " + IntToStr( p->MessageData.Length() ));
    Debug( "Data: " + p->MessageData );
    #endif

    return true;
  }
  else if(( start > 0 ) )
  {
    #if DebugServerSocket == 1
    Debug( "SocketInputQueue: Start no stop. Skip " + IntToStr( start ) + " bytes before start");
    #endif

    Move( SocketInputQueue + start, SocketInputQueue, SocketInputIdx - start );
		SocketInputIdx = SocketInputIdx - start;
	}
  #if DebugServerSocket == 1
  Debug( "SocketInputQueue = "+IntToStr( SocketInputIdx )+" bytes" );
  #endif
  return false;
}

void __fastcall TCIIServerSocket::SendMessage()
{
  TCamurPacket *p;
  int32_t c;

	while( SendMessageList->Count > 0 )
  {
		p = (TCamurPacket*)SendMessageList->Items[0];

    SocketOutBuf[0] = 1;
		SocketOutBuf[1] = 50;
    SocketOutBuf[2] = ResponseCount & 255;
    SocketOutBuf[3] = ResponseCount >> 8;
    c = p->MessageData.Length() + MessageHeaderLength;
    SocketOutBuf[4] = c & 255;
    SocketOutBuf[5] = c >> 8;
    SocketOutBuf[6] = p->MessageType;
    SocketOutBuf[7] = p->MessageNumber;
    SocketOutBuf[8] = p->MessageCode;
    SocketOutBuf[9] = p->MessageCommand;
    SocketOutBuf[10] = p->MessageTable;
    SocketOutBuf[11] = p->MessageData.Length() & 255;
    SocketOutBuf[12] = p->MessageData.Length() >> 8;
    for( int32_t i = 1; i <= p->MessageData.Length(); i++ )
    SocketOutBuf[12+i] = Byte(p->MessageData[i]);
    SocketOutBuf[13 + p->MessageData.Length()] = 3;
    //CRC16Full( c, @SocketOutBuf[1], 12+Length(p->MessageData()));
    // I st�llet f�r CRC
    SocketOutBuf[14 + p->MessageData.Length()] = 0;
    SocketOutBuf[15 + p->MessageData.Length()] = 0;

		SendMessageList->Delete(0);
    ResponseCount++;

    if( CWSocket != NULL ) CWSocket->SendBuf(SocketOutBuf, 16 + p->MessageData.Length());

		#if DebugServerSocket == 1
		Debug( "SendMessageList " + IntToStr( SendMessageList->Count ) );
    Debug( "Sent to port " + IntToStr( PortNo ) );

    Debug( "Type: " + IntToStr( p->MessageType ) +
           " / Number: " + IntToStr( p->MessageNumber ) +
           " / Code: " + IntToStr( p->MessageCode ) +
           " / Command: " + IntToStr( p->MessageCommand ) +
           " / Table: " + IntToStr( p->MessageTable ) +
           " / Length: " + IntToStr( p->MessageData.Length() ));
    Debug( "Data: " + p->MessageData );
    #endif

    delete p;

  }
}

void __fastcall TCIIServerSocket::SetDebugWin(TDebugWin * SetDebugWin)
{
  DW = SetDebugWin;
}

void __fastcall TCIIServerSocket::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

TCamurPacket* __fastcall TCIIServerSocket::GetMessage()
{
  TCamurPacket *p;

	if( ReceivedMessageList->Count > 0 )
  {
		p = (TCamurPacket*)ReceivedMessageList->Items[0];
		ReceivedMessageList->Delete(0);
  }
  else p = NULL;

  return p;
}

void __fastcall TCIIServerSocket::PutMessage(TCamurPacket * o)
{
	SendMessageList->Add( o );
}

void __fastcall TCIIServerSocket::CancelSendMessage()
{
	TCamurPacket *p;
	
	for( int32_t i = 0; i < SendMessageList->Count; i++ )
	{
		p = (TCamurPacket*)SendMessageList->Items[i];
		delete p;
		SendMessageList->Delete(i);
	}
}

void __fastcall TCIIServerSocket::ConnectClient()
{
  ServSocket->Open();
}

void __fastcall TCIIServerSocket::DisconnectClient()
{
  ServSocket->Close();
}

bool __fastcall TCIIServerSocket::GetClientAktiv()
{
	TmpClientAktiv = FClientAktiv;
	FClientAktiv = false;
	return TmpClientAktiv;
}


