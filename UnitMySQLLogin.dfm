object FormMySQLLogin: TFormMySQLLogin
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Login to MySQL'
  ClientHeight = 193
  ClientWidth = 272
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtnYes: TBitBtn
    Left = 175
    Top = 8
    Width = 75
    Height = 25
    Kind = bkYes
    NumGlyphs = 2
    TabOrder = 0
  end
  object LabeledEditHostName: TLabeledEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 21
    Hint = 'Name or IP-address of computer where the server is running'
    EditLabel.Width = 51
    EditLabel.Height = 13
    EditLabel.Hint = 'Name or IP-address of computer where the server is running'
    EditLabel.Caption = 'Host name'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object LabeledEditUserName: TLabeledEdit
    Left = 16
    Top = 64
    Width = 105
    Height = 21
    Hint = 'User name to use when logging in to server'
    EditLabel.Width = 51
    EditLabel.Height = 13
    EditLabel.Hint = 'Name or IP-address of computer where the server is running'
    EditLabel.Caption = 'User name'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
  end
  object LabeledEditPassword: TLabeledEdit
    Left = 16
    Top = 104
    Width = 105
    Height = 21
    Hint = 'Password to use when logging in to server'
    EditLabel.Width = 46
    EditLabel.Height = 13
    EditLabel.Hint = 'Name or IP-address of computer where the server is running'
    EditLabel.Caption = 'Password'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
  end
  object BitBtnNo: TBitBtn
    Left = 175
    Top = 39
    Width = 75
    Height = 25
    Kind = bkNo
    NumGlyphs = 2
    TabOrder = 5
  end
  object LabeledEditDbName: TLabeledEdit
    Left = 16
    Top = 144
    Width = 121
    Height = 21
    Hint = 'Select this databaser after logging in to server'
    EditLabel.Width = 46
    EditLabel.Height = 13
    EditLabel.Hint = 'Name or IP-address of computer where the server is running'
    EditLabel.Caption = 'Database'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
  end
end
