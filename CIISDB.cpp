//---------------------------------------------------------------------------
//
// Description: Camur II Server Database Module
//
// Comment: Using dbExpress and MySQL ver 5.1
// Requires
// dbxmys30.dll (distributed with C++ Builder 2007)
// libmysql.dll 2008-08-04
//
// History
//
// Date			Comment										Sign
// 2009-05-28   MySQL										Bo H
// 2009-07-16   LPRAppend dataset
//				Refresh query for append datasets every hour
// 				in order to not to waste memory
//				Corrected FindLastMonitorValue(),FindLastLPRValue,FindLastDecayValue()
//              FindLastMonitorValueInRecording()
// 				Corrected GetMonitorValueRecCount(),GetLPRValueRecCount(),GetDecayValueRecCount()
//				TabClone() removed
// 2009-08-27	Now using dbexpmda40.dll (ver 4.40.0.16 2009-06-11)
//				from DevArt as interface between dbExpress and libMySQL.dll
//				Please use the libmysql.dll distributed with
//				the installed version of MySQL . E.g. from 2009-04-01
//				This also means that dbxmys30.dll and midas.dll no longer are necessary.
// 2009-10-05   Note! midas.dll is still necessary to distribute!
//              IsMySQLServiceStarted() returns true if MySQL service is running.
//
// 2009-12-04   IsLoginPossible/DatabaseExists - functions to be called by main
//              Now possible to adjust HostName
//				New table:WLink
//				Controller table upgraded with new fields for Decay (2nd interval etc)
//
// 2009-12-11   Recordings table upgraded with new fields for Decay (2nd interval etc)
//
// 2010-01-14   IndexFields assigned also for tables with multiple primary keys
//              LogEvent (limiting no of records) simplified
//
// 2010-11-12	New field ValueType in ValuesLPR and ValuesDecay
// 				Primary keys for ValuesLPR and ValuesDecay extended to
//				be identical to ValuesMonitor (including e.g. ValueType)
//
// 2011-01-19	New table (ValuesMisc) for timestamped miscellaneous values
//				New field ValuesMiscSize in project record. This value is the
//				max number of records in the ValuesMisc - table.
//
//	2011-03-05	SetLPRQuery(..),GetLPRLocalValuesRecCount(..),
//				FindFirstLPRLocalValue(..)
//				Used when calculating LPR results
//
//  2011-03-09	ValuesCalc table. To store LPR results and later
//				perhaps other calculated values
//
//	2011-08-29	Convert all table-names and db-name to lowercase
//				 (in order to be *nix-compatible)
//
//	2011-12-05	Query from start of combined timestamp (TimeStamp + Fraction)
//				in Locate-functions used during synchronization.
//
//  2012-03-04	Converted to Xe2
//
//  2012-11-21  Using dbxmys.dll and libMySQL again (2012-11-21, 5.1.59)
//              Deleting old records in ApplyUpdates instead of AppendRecord
//              for table ValuesMisc
//              use PeekMiscValuesRecCount() to check the size at Init and drop the table if too large
//
//  2013-02-25  ClearSensorMiscValues();
//
//  2013-03-05  SensorChannelCount
//	2015-02-11	Converted to Xe7 and FireDAC      				Bo H
//
//  2016-05-12  New field Fallback in table PowerSupply         BL
//  2016-11-04  New field PSTemp in table PowerSupply           BL
//  2017-02-10  LastStoredEvent, LastStoredFraction added to prevent that FDTableEventLog->FindLast()
//              finds a record that not is the last because ApplyUpdate has not been executet yet.
//              This can occur after that the ApplyUpdate call has been centralised to
// 							speed up handling of the event table            BL
//  2018-10-09  New field VerNotSupported in node tables        BL
//  2019-11-07  New field Disabled in Sensors for channels
//								removed from report and export.                              BL
//  2020-01-21  New field PreCommOff2, PreCommOff3, PreCommOff4 int32_t Sensor tab (for P4 nodes)
//  2020-01-21  New field InvDO1, InvDO2 in PSTable (for PI nodes)
//  2020-01-27  New field CANAlalrmEnable in PSTable, Alarm and Sensor.
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CIISDB.h"
#include <stdio.h>
#include "system.hpp"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCIISDBModule *TCIISDBModule;
//---------------------------------------------------------------------------
//
// Description: Constructor
//
// Comment:
//
__fastcall TCIISDBModule::TCIISDBModule(TComponent* Owner)
				: TDataModule(Owner)
{
 IFrm = new TCIISDBInitFrm(this);
 NoOpts.Clear();

 DW = NULL;

	LastStoredEvent = Now_ms_cleard();
	LastStoredFraction = 0;

}
//---------------------------------------------------------------------------
//
// Description: Destructor
//
// Comment:
//
__fastcall TCIISDBModule::~TCIISDBModule()
{
 delete IFrm;
}


//---------------------------------------------------------------------------
//
// Description: Retrieves the last error and displays a formatted message.
//
// Comment:
//
void __fastcall TCIISDBModule::DisplayLastErr(String MessageCaption)
{
 LPVOID lpMsgBuf;
 LPVOID lpDisplayBuf;

 DWORD dw = GetLastError();

 FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );

 lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + 40)*sizeof(TCHAR));

 wsprintf((LPTSTR)lpDisplayBuf, TEXT("Failed with error %d: %s"), dw, lpMsgBuf);
 Application->MessageBox(String((LPCTSTR)lpDisplayBuf).w_str(),MessageCaption.w_str(), MB_ICONEXCLAMATION);

 LocalFree(lpMsgBuf);
 LocalFree(lpDisplayBuf);
}
//---------------------------------------------------------------------------
//
// Description: Checks that MySQL service is started
//
// Comment:
//
bool __fastcall TCIISDBModule::IsMySQLServiceStarted(void)
{
 bool ServiceStarted = false;
	// Open Service Control Manager on the local machine
 SC_HANDLE hSCManager = OpenSCManager(NULL,NULL,GENERIC_READ);
 if (hSCManager)
  {
   SC_HANDLE hService = OpenService(hSCManager,"MySQL",GENERIC_READ);
   if (hService)
	{
	 SERVICE_STATUS_PROCESS ServiceStatus;
	 DWORD BytesNeeded;
	 if (QueryServiceStatusEx(hService,SC_STATUS_PROCESS_INFO,(LPBYTE)&ServiceStatus,
		 sizeof(ServiceStatus),&BytesNeeded))
	  {
	   ServiceStarted = (ServiceStatus.dwCurrentState == SERVICE_RUNNING) ? true:false;
	  }
	 else
	   DisplayLastErr("QueryServiceStatusEx");
	}
   else
	 DisplayLastErr("OpenService");
  }
 else
   DisplayLastErr("OpenSCManager");

 return ServiceStarted;
}
//---------------------------------------------------------------------------
//
// Description: Checks that MySQL log in credentials are Ok
//
// Comment: Using "mysql" as default-schema because it is always present
//
bool __fastcall TCIISDBModule::IsLoginPossible( String DBHostName,
												String DBUserName,
												String DBPassword)
{
 String HostName;
 int32_t PortNo=3306;   // Default MySQL port-number


 bool LoginPossible = true;

	// Port number must be separated from HostName
	int32_t ColonPos=DBHostName.Pos(":");
	if (ColonPos > 0)
	{
		String PortNoStr = DBHostName.SubString(ColonPos+1,DBHostName.Length()-ColonPos);
		HostName = DBHostName.SubString(1,ColonPos-1);
		PortNo=StrToIntDef(PortNoStr,3306);
	}
	else
	{
		HostName = DBHostName;
	}

 FDConnection->Params->Values["Server"]=HostName;
 FDConnection->Params->UserName=DBUserName;
 FDConnection->Params->Password=DBPassword;
 FDConnection->Params->Database="mysql";
 FDConnection->Params->Values["Port"]=PortNo;

 try
  {
   FDConnection->Connected=true;
   FDConnection->Connected=false;
  }
 catch (const Exception &E)
  {
   LoginPossible = false;
  }
 return LoginPossible;
}
//---------------------------------------------------------------------------
//
// Description: Checks that MySQL database exists
//
// Comment: Also checks that login-credentials are ok
//
bool __fastcall TCIISDBModule::DatabaseExists( String DBHostName,
												String DBUserName,
												String DBPassword,
												String DBName)
{
 String HostName;
 int32_t PortNo=3306;   // Default MySQL port-number

	// Port number must be separated from HostName
	int32_t ColonPos=DBHostName.Pos(":");
	if (ColonPos > 0)
	{
		String PortNoStr = DBHostName.SubString(ColonPos+1,DBHostName.Length()-ColonPos);
		HostName = DBHostName.SubString(1,ColonPos-1);
		PortNo=StrToIntDef(PortNoStr,3306);
	}
	else
	{
		HostName = DBHostName;
	}

 bool DBExists = true;

 FDConnection->Params->Values["Server"]=HostName;
 FDConnection->Params->UserName=DBUserName;
 FDConnection->Params->Password=DBPassword;
 FDConnection->Params->Database=DBName.LowerCase();
 FDConnection->Params->Values["Port"]=PortNo;

 try
  {
   FDConnection->Connected=true;
   FDConnection->Connected=false;
  }
 catch (const Exception &E)
  {
   DBExists = false;
  }
 return DBExists;
}
//---------------------------------------------------------------------------
//
// Description: Open database connection to MySQL
//
// Comment:
//
bool __fastcall TCIISDBModule::InitCIISDB(String SetDBHostName, String SetDBName,
			CIISServerMode SetServerMode ,String SetDBUserName, String SetDBPassword,
			TDebugWin * SetDebugWin)
{

	DW = SetDebugWin;

	ServerMode = SetServerMode;

	String HostName;
	int32_t PortNo=3306;   // Default MySQL port-number


			// MySQL
	DBHostName=SetDBHostName;
	DBUserName=SetDBUserName;
	DBPassword=SetDBPassword;
	DBName = SetDBName.LowerCase();


	// Port number must be separated from HostName
	int32_t ColonPos=DBHostName.Pos(":");
	if (ColonPos > 0)
	{
		String PortNoStr = DBHostName.SubString(ColonPos+1,DBHostName.Length()-ColonPos);
		HostName = DBHostName.SubString(1,ColonPos-1);
		PortNo=StrToIntDef(PortNoStr,3306);
	}
	else
	{
		HostName = DBHostName;
	}
		// First open SQL connection in order to check that db exists
	FDConnection->Params->Values["Server"]=HostName;
	FDConnection->Params->UserName=DBUserName;
	FDConnection->Params->Password=DBPassword;
	FDConnection->Params->Database="mysql";
	FDConnection->Params->Values["Port"]=PortNo;
	FDConnection->Connected=true;

		// Create database if non-existant
	CreateSQLDatabase(FDConnection,DBName);

		// Reopen SQL Connection with selected db as default
	FDConnection->Connected=false;
	FDConnection->Params->Database=DBName.LowerCase(); // MySQL DbName
	FDConnection->Connected=true;
	if (!FDConnection->Connected)
	{
		ShowMessage("FireDAC - MySQL Server !?!");
		return false;
	}

	IFrm->Show();

	IFrm->MsgCl(clBlack);


	// Need to specify both index and UpdateOptions->KeyFields
	// otherwise FireDAC will try to update with a WHERE clause including all
	// fields which is a BAD idea

	FDTableProject->IndexFieldNames="PrjName";
	FDTableProject->UpdateOptions->KeyFields = FDTableProject->IndexFieldNames;

	FDTableControllers->IndexFieldNames="CtrlName";
	FDTableControllers->UpdateOptions->KeyFields=FDTableControllers->IndexFieldNames;

	FDTableWLink->IndexFieldNames="WLinkSerialNo";
	FDTableWLink->UpdateOptions->KeyFields=FDTableWLink->IndexFieldNames;

	FDTableZones->IndexFieldNames="ZoneNo";
	FDTableZones->UpdateOptions->KeyFields=FDTableZones->IndexFieldNames;

	FDTableSensors->IndexFieldNames="SensorSerialNo";
	FDTableSensors->UpdateOptions->KeyFields=FDTableSensors->IndexFieldNames;

	FDTableRecordings->IndexFieldNames="RecNo";
	FDTableRecordings->UpdateOptions->KeyFields=FDTableRecordings->IndexFieldNames;

	FDTablePowerSupply->IndexFieldNames="PSSerialNo";
	FDTablePowerSupply->UpdateOptions->KeyFields = FDTablePowerSupply->IndexFieldNames;

	FDTableAlarm->IndexFieldNames="AlarmSerialNo";
	FDTableAlarm->UpdateOptions->KeyFields=FDTableAlarm->IndexFieldNames;

	FDTableBIChannels->IndexFieldNames="SerialNo";
	FDTableBIChannels->UpdateOptions->KeyFields=FDTableBIChannels->IndexFieldNames;


	FDQueryValues->IndexFieldNames="DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType";

	TabValuesAppend->IndexFieldNames=FDQueryValues->IndexFieldNames;
	TabValuesAppend->CreateDataSet();
	TabValuesAppend->LogChanges = false;

	FDTableEventLog->IndexFieldNames="EventDateTime;Fraction;EventType;EventCode;EventLevel;EventNo";
	FDTableEventLog->UpdateOptions->KeyFields = FDTableEventLog->IndexFieldNames;

	// 2010-11-12 ver 3.6.0
	// Primary keys extended for ValuesLPR and ValuesDecay
	FDQueryValuesLPR->IndexFieldNames="DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType";
	TabValuesLPRAppend->IndexFieldNames=FDQueryValuesLPR->IndexFieldNames;
	TabValuesLPRAppend->CreateDataSet();
	TabValuesLPRAppend->LogChanges = false;

	FDQueryValuesDecay->IndexFieldNames="DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType";
	TabValuesDecayAppend->IndexFieldNames=FDQueryValuesDecay->IndexFieldNames;
	TabValuesDecayAppend->CreateDataSet();
	TabValuesDecayAppend->LogChanges = false;

	// 2011-01-19, New table ValuesMisc
	FDTableValuesMisc->IndexFieldNames="DateTimeStamp;Fraction;SensorSerialNo;ValueType";
	FDTableValuesMisc->UpdateOptions->KeyFields=FDTableValuesMisc->IndexFieldNames;
	ClientDataSetValuesMisc->CreateDataSet();
	ClientDataSetValuesMisc->LogChanges = false;

	// 2011-03-07, New table ValuesCalc
	FDTableValuesCalc->IndexFieldNames="DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType";
	FDTableValuesCalc->UpdateOptions->KeyFields=FDTableValuesCalc->IndexFieldNames;

	// Create/upgrade database-tables
	if (!VerifyDB(DBName.LowerCase()))
	{
		ShowMessage("MySQL database:"+DBName.LowerCase()+" not found!");
		return false;
	}


	// Set Client datasets active
	FDTableProject->Active=true;
	FDTableControllers->Active=true;
	FDTableWLink->Active=true;
	FDTableZones->Active=true;
	FDTableSensors->Active=true;
	FDTablePowerSupply->Active=true;
	FDTableRecordings->Active=true;

	FDQueryValues->Active=true;
	TabValuesAppend->Active=true;
	FDQueryValuesLPR->Active=true;
	TabValuesLPRAppend->Active=true;
	FDQueryValuesDecay->Active=true;
	TabValuesDecayAppend->Active=true;
	FDTableValuesMisc->Active=true;
	ClientDataSetValuesMisc->Active=true;
	FDTableValuesCalc->Active=true;
	FDTableEventLog->Active=true;
	FDTableAlarm->Active=true;
	FDTableBIChannels->Active=true;

	IFrm->MsgCl(clBlack);
	IFrm->Close();

	UpdateRecEnd();
	UpdateVer();

	LastValueMonitorTimeStamp = GetLastMonitorTimestamp();
	LastValueLPRTimeStamp = GetLastLPRTimestamp();
	LastValueDecayTimeStamp = GetLastDecayTimestamp();
	LastValueMiscTimeStamp = GetLastMiscTimestamp();
	LastValueCalcTimeStamp = GetLastCalcTimestamp();

	InsertedMiscValues = 0;
	MiscValueInserted = false;
	EventInserted = false;

	TDateTime ThisTime=Now();


	if( ServerMode == CIISLocalServer )
	{
		if (LastValueMonitorTimeStamp > ThisTime)
		{
			Application->MessageBox(String("Last timestamp("+LastValueMonitorTimeStamp.DateTimeString()+") in table is later than current time:"+ThisTime.DateTimeString()).c_str(),L"ValuesMonitor",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else if (LastValueLPRTimeStamp > ThisTime)
		{
			Application->MessageBox(String("Last timestamp("+LastValueLPRTimeStamp.DateTimeString()+") in table is later than current time:"+ThisTime.DateTimeString()).c_str(),L"ValuesLPR",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else if (LastValueCalcTimeStamp > ThisTime)
		{
			Application->MessageBox(String("Last timestamp("+LastValueCalcTimeStamp.DateTimeString()+") in table is later than current time:"+ThisTime.DateTimeString()).c_str(),L"ValuesCalc",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else if (LastValueDecayTimeStamp > ThisTime)
		{
			Application->MessageBox(String("Last timestamp("+LastValueDecayTimeStamp.DateTimeString()+") in table is later than current time:"+ThisTime.DateTimeString()).c_str(),L"ValuesDecay",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else if (LastValueMiscTimeStamp > ThisTime)
		{
			Application->MessageBox(String("Last timestamp("+LastValueMiscTimeStamp.DateTimeString()+") in table is later than current time:"+ThisTime.DateTimeString()).c_str(),L"ValuesMisc",MB_OK|MB_ICONEXCLAMATION);
			return false;
		}
		else
			return true;
	}
	else
      return true;

}

//---------------------------------------------------------------------------
//
// Description:
// Verify database
// Check that database exists (create if non-existant)
// Check that tables exist (create if non-existant)
//
// TODO:
// Check that tables are of correct version (restructure if necessary)
//
// Comment: returns true if database verified Ok
//
bool __fastcall  TCIISDBModule::VerifyDB(String DBName)
{
 if (!SQLTableExists(DBName,"eventlog"))
  {
   IFrm->Msg("Table eventlog not found! Creating new table...");
	 CreateTabEventLog();
	}

 if (SQLTableExists(DBName,"project"))
  {
	 CheckUpdateTabProject();
  }
 else
  {
   IFrm->Msg("Table project not found! Creating new table...");
   CreateTabProject();
	}

 if (SQLTableExists(DBName,"controllers"))
  {
	 CheckUpdateTabControllers();
  }
 else
  {
   IFrm->Msg("Table controllers not found! Creating new table...");
   CreateTabControllers();
	}

 if (SQLTableExists(DBName,"wlink"))
	{
		CheckUpdateTabWLink();
	}
	else
	{
	 IFrm->Msg("Table wlink not found! Creating new table...");
	 CreateTabWLink();
	}

 if (SQLTableExists(DBName,"zones"))
	{
		CheckUpdateTabZones();
	}
	else
	{
	 IFrm->Msg("Table zones not found! Creating new table...");
	 CreateTabZones();
	}

 if (SQLTableExists(DBName,"sensors"))
	{
		CheckUpdateTabSensors();
	}
 else
	{
	 IFrm->Msg("Table sensors not found! Creating new table...");
	 CreateTabSensors();
	}

 if (SQLTableExists(DBName,"powersupply"))
	{
	 CheckUpdateTabPowerSupply();
	}
	else
	{
	 IFrm->Msg("Table powersupply not found! Creating new table...");
	 CreateTabPowerSupply();
	}

 if (SQLTableExists(DBName,"recordings"))
	{
	 CheckUpdateTabRecordings();
	}
 else
	{
	 IFrm->Msg("Table recordings not found! Creating new table...");
	 CreateTabRecordings();
	}

 if (SQLTableExists(DBName,"valuesmonitor"))
	{
		CheckUpdateTabValues();
	}
	else
	{
	 IFrm->Msg("Table values not found! Creating new table...");
	 CreateTabValues();
	}

 if (SQLTableExists(DBName,"valueslpr"))
	{
	 CheckUpdateTabValuesLPR();
	}
 else
	{
	 IFrm->Msg("Table valueslpr not found! Creating new table...");
	 CreateTabValuesLPR();
	}

 if (SQLTableExists(DBName,"valuesdecay"))
	{
	 CheckUpdateTabValuesDecay();
	}
 else
	{
	 IFrm->Msg("Table valuesdecay not found! Creating new table...");
	 CreateTabValuesDecay();
	}

 if (SQLTableExists(DBName,"valuesmisc"))
 {
				// Open prj table
	 FDTableProject->IndexFieldNames="PrjName";
	 FDTableProject->Active=true;

	 CIISPrjRec PrjRec;
	 if (GetPrjRec(&PrjRec))
		{
				// Delete table if size too large
		 if (PeekMiscValuesRecCount() > (uint32_t) PrjRec.ValuesMiscSize)
			{
			 IFrm->Msg("Table valuesmisc too large! Delete table...");
			 DeleteSQLTable(FDConnection,DBName,"valuesmisc");
			}
		}
		FDTableProject->Active=false;
 }

 if (!SQLTableExists(DBName,"valuesmisc"))
	{
	 IFrm->Msg("Table valuesmisc not found! Creating new table...");
	 CreateTabValuesMisc();
	}

 if (!SQLTableExists(DBName,"valuescalc"))
	{
	 IFrm->Msg("Table valuescalc not found! Creating new table...");
	 CreateTabValuesCalc();
	}

 if (SQLTableExists(DBName,"alarm"))
	{
	 CheckUpdateTabAlarm();
	}
 else
	{
	 IFrm->Msg("Table alarm not found! Creating new table...");
	 CreateTabAlarm();
	}

 if (SQLTableExists(DBName,"bichannels"))
 {
   CheckUpdateTabBIChannels();
 }
 else
 {
	 IFrm->Msg("Table BIChannels not found! Creating new table...");
	 CreateTabBIChannels();
 }

 return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Close all tables and their connection to the database
//
// Comment:
//
void __fastcall TCIISDBModule::CloseCIISDB(void)
{
 FDTableProject->Active = false;
 FDTableControllers->Active = false;
 FDTableWLink->Active = false;
 FDTableZones->Active = false;
 FDTableSensors->Active = false;
 FDTableRecordings->Active = false;
 FDQueryValues->Active = false;
 TabValuesAppend->Active = false;
 FDQueryValuesLPR->Active = false;
 TabValuesLPRAppend->Active = false;
 FDQueryValuesDecay->Active = false;
 TabValuesDecayAppend->Active = false;
 FDTablePowerSupply->Active = false;
 FDTableValuesMisc->Active = false;
 ClientDataSetValuesMisc->Active = false;
 FDTableValuesCalc->Active = false;
 FDTableEventLog->Active = false;
 FDTableAlarm->Active = false;
 FDTableBIChannels->Active = false;

 FDConnection->Connected=false;
}

//---------------------------------------------------------------------------
//
// Description:
// Create project table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabProject()
{
	IFrm->Msg("Creating TabProject");

  FDTableProject->FieldDefs->Clear();
  FDTableProject->FieldDefs->Add( "PrjName", ftString, 20, false );
  FDTableProject->FieldDefs->Add( "PrjNo", ftString, 20, false );
  FDTableProject->FieldDefs->Add( "Client", ftString, 100, false );
  FDTableProject->FieldDefs->Add( "Consultant", ftString, 100, false );
  FDTableProject->FieldDefs->Add( "Manager", ftString, 100, false );
  FDTableProject->FieldDefs->Add( "ContractDescription", ftMemo, 240, false );
  FDTableProject->FieldDefs->Add( "MMResponsibility", ftMemo, 240, false );
  FDTableProject->FieldDefs->Add( "Commissioning", ftDate, 0, false );
  FDTableProject->FieldDefs->Add( "Drawings", ftMemo, 240, false );
  FDTableProject->FieldDefs->Add( "Criteria", ftMemo, 240, false );
  FDTableProject->FieldDefs->Add( "ConnType", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "ConnRemote", ftString, 20, false );
  FDTableProject->FieldDefs->Add( "ConnServerIP", ftString, 80, false );
	FDTableProject->FieldDefs->Add( "PrjAlarmStatus", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "EventLogSize", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "EventLevel", ftSmallint, 0, false );
  FDTableProject->FieldDefs->Add( "ServerStatus", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "ServerMode", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "ConfigVer", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "DataVer", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "LastValVer", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "DBVer", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "PWReadOnly", ftString, 25, false );
  FDTableProject->FieldDefs->Add( "PWModify", ftString, 25, false );
  FDTableProject->FieldDefs->Add( "PWErase", ftString, 25, false );
  FDTableProject->FieldDefs->Add( "PrgVer", ftInteger, 0, false );
  FDTableProject->FieldDefs->Add( "DTAdj", ftDateTime, 0, false );
  FDTableProject->FieldDefs->Add( "DTNew", ftDateTime, 0, false );
  FDTableProject->FieldDefs->Add( "ValuesMiscSize", ftInteger, 0, false );
  FDTableProject->IndexDefs->Clear();
  FDTableProject->IndexDefs->Add( "IxPrjName", "PrjName", TIndexOptions() <<ixPrimary );
//  FDTableProject->CreateTable();

			// MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Project",FDTableProject);

  FDConnection->ExecSQL(SQLString);

  try
   {
	FDTableProject->Active = true;
	FDTableProject->Append();
	FDTableProject->FieldValues["PrjName"] = "Project 1";
	FDTableProject->FieldValues["PrjNo"] = "0";
	WriteDate(FDTableProject,"Commissioning",TDate(1980,1,1));
	FDTableProject->FieldValues["ConnType"] = 0;
	FDTableProject->FieldValues["PrjAlarmStatus"] = 0;
	FDTableProject->FieldValues["EventLogSize"] = DefaultEventTabSize;
	FDTableProject->FieldValues["EventLevel"] = DefaultEventLevel;
	FDTableProject->FieldValues["ServerStatus"] = CIISDisconnected;
	FDTableProject->FieldValues["ServerMode"] = CIISNotInitiated;
	FDTableProject->FieldValues["ConfigVer"] = 0;
	FDTableProject->FieldValues["DataVer"] = 0;
	FDTableProject->FieldValues["LastValVer"] = 0;
	FDTableProject->FieldValues["DBVer"] = DBVer;
	FDTableProject->FieldValues["PWReadOnly"] = "Protector";
	FDTableProject->FieldValues["PWModify"] = "ProModify";
	FDTableProject->FieldValues["PWErase"] = "ProErase";
	FDTableProject->FieldValues["PrgVer"] = 0;
	WriteDateTime(FDTableProject,"DTAdj",TDateTime(1980,1,1,0,0,0,0));
	WriteDateTime(FDTableProject,"DTNew",TDateTime(1980,1,1,0,0,0,0));
	FDTableProject->FieldValues["ValuesMiscSize"] = 10000;
	FDTableProject->Post();
	FDTableProject->ApplyUpdates();
	FDTableProject->CommitUpdates();
	FDTableProject->Active = false;
   }
  catch (const Exception &E)
   {
	Debug(Name + "::CreateTabProject() " + FDTableProject->TableName + ":" + E.Message);
	FDTableProject->Cancel();
   }

  ForceUpdate = true;
}

//---------------------------------------------------------------------------
//
// Description:
// Check if project table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabProject()
{
	if (!SQLFieldExists(DBName,"Project","ValuesMiscSize"))
	{
		SQLAddField(DBName,"Project","ValuesMiscSize",ftInteger,"10000");
	}

	return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Write program version to database (table project)
//
// Comment:
//
void __fastcall TCIISDBModule::UpdateVer()
{
  String PVer;
  int32_t PrgVer, OldPrgVer, OldDBVer, OldConfigVer;

 if( ServerMode == CIISLocalServer )
  {

	PVer = GetProgVersion();
	do
	{
	  PrgVer = PVer.Pos( "." );
	  if( PrgVer > 0 ) PVer.Delete( PrgVer, 1 );
	} while( PrgVer > 0 );

	PrgVer = StrToInt( PVer );

	OldPrgVer = FDTableProject->FieldByName("PrgVer")->AsInteger;
	OldDBVer = FDTableProject->FieldByName("DBVer")->AsInteger;
	OldConfigVer = FDTableProject->FieldByName("ConfigVer")->AsInteger;

	if(( PrgVer != OldPrgVer ) || ( DBVer != OldDBVer))
	{
	 try
	  {
	   FDTableProject->Edit();
	   FDTableProject->FieldValues["PrgVer"] = PrgVer;
	   FDTableProject->FieldValues["DBVer"] = DBVer;
	   FDTableProject->FieldValues["ConfigVer"] = ++OldConfigVer;
	   FDTableProject->Post();
	   FDTableProject->ApplyUpdates();
	   FDTableProject->CommitUpdates();
	  }
	 catch (const Exception &E)
	  {
	   Debug(Name + "::UpdateVer() " + FDTableProject->TableName + ":" + E.Message);
	   FDTableProject->Cancel();
	  }
	}
  }
}

//---------------------------------------------------------------------------
//
// Description:
// Create controllers table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabControllers()
{
	IFrm->Msg("Creating TabControllers");

  FDTableControllers->FieldDefs->Clear();
  FDTableControllers->FieldDefs->Add( "CtrlName", ftString, 20, false );
  FDTableControllers->FieldDefs->Add( "NoIR", ftBoolean, 0, false );
  FDTableControllers->FieldDefs->Add( "ScheduleDateTime", ftDateTime, 0, false );
  FDTableControllers->FieldDefs->Add( "SchedulePeriod", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "DecaySampInterval", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "DecayDuration", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRRange", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRStep", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRDelay1", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRDelay2", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SampInterval1", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SampInterval2", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SampInterval3", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SampInterval4", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SampInterval5", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "NextCanAdr", ftSmallint, 0, false );
  FDTableControllers->FieldDefs->Add( "DefaultSensorLow", ftFloat, 0, false );
  FDTableControllers->FieldDefs->Add( "DefaultSensorHigh", ftFloat, 0, false );
	FDTableControllers->FieldDefs->Add( "DefaultAlarmEnable", ftBoolean, 0, false );
	FDTableControllers->FieldDefs->Add( "CtrlAlarmStatus", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "PrjName", ftString, 20, false );
  FDTableControllers->FieldDefs->Add( "USBCANStatus", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "NodeCount", ftInteger, 0, false );
	FDTableControllers->FieldDefs->Add( "MonitorCount", ftInteger, 0, false );
	FDTableControllers->FieldDefs->Add( "MonitorExtCount", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "DecayCount", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRCount", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "SchedulePeriodUnit", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "ScheduleDecay", ftBoolean, 0, false );
	FDTableControllers->FieldDefs->Add( "ScheduleLPR", ftBoolean, 0, false );
  FDTableControllers->FieldDefs->Add( "CtrlScheduleStatus", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "LPRMode", ftInteger, 0, false );

  FDTableControllers->FieldDefs->Add( "DecayDelay", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "DecaySampInterval2", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "DecayDuration2", ftInteger, 0, false );

	FDTableControllers->FieldDefs->Add( "WatchDogEnable", ftBoolean, 0, false );
  FDTableControllers->FieldDefs->Add( "BusIntSerialNo", ftInteger, 0, false );
  FDTableControllers->FieldDefs->Add( "BusIntVerMajor", ftInteger, 0, false );
	FDTableControllers->FieldDefs->Add( "BusIntVerMinor", ftInteger, 0, false );
	FDTableControllers->FieldDefs->Add( "ScheduleZRA", ftBoolean, 0, false );
	FDTableControllers->FieldDefs->Add( "ScheduleResMes", ftBoolean, 0, false );

/*
WatchDogEnable	Boolean
BusIntSerialNo	Interger
BusIntVerMajor	Integer
BusIntVerMinor	Integer
*/

  FDTableControllers->IndexDefs->Clear();
  FDTableControllers->IndexDefs->Add( "IxCtrlName", "CtrlName", TIndexOptions() <<ixPrimary );

//  FDTableControllers->CreateTable();

			// MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Controllers",FDTableControllers);

  FDConnection->ExecSQL(SQLString);


  try
   {
	FDTableControllers->Active = true;
	FDTableControllers->Append();
	FDTableControllers->FieldValues["CtrlName"] = "Controller 1";
	WriteBool(FDTableControllers,"NoIR",false);
	WriteDateTime(FDTableControllers,"ScheduleDateTime",TDateTime(1980,1,1,0,0,0,0));
	FDTableControllers->FieldValues["DecaySampInterval"] = 2;
	FDTableControllers->FieldValues["DecayDuration"] = 86400;
	FDTableControllers->FieldValues["LPRRange"] = 10;
	FDTableControllers->FieldValues["LPRStep"] = 2;
	FDTableControllers->FieldValues["LPRDelay1"] = 5;
	FDTableControllers->FieldValues["LPRDelay2"] = 0;
	FDTableControllers->FieldValues["SampInterval1"] = 60;
	FDTableControllers->FieldValues["SampInterval2"] = 900;
	FDTableControllers->FieldValues["SampInterval3"] = 3600;
	FDTableControllers->FieldValues["SampInterval4"] = 28800;
	FDTableControllers->FieldValues["SampInterval5"] = 86400;
	FDTableControllers->FieldValues["NextCanAdr"] = 301;
	WriteBool(FDTableControllers,"DefaultAlarmEnable",false);

	FDTableControllers->FieldValues["CtrlAlarmStatus"] = 0;
	FDTableControllers->FieldValues["PrjName"] = "Project 1";
	FDTableControllers->FieldValues["USBCANStatus"] = 0;
	FDTableControllers->FieldValues["NodeCount"] = 0;
	FDTableControllers->FieldValues["MonitorCount"] = 0;
	FDTableControllers->FieldValues["MonitorExtCount"] = 0;
	FDTableControllers->FieldValues["DecayCount"] = 0;
	FDTableControllers->FieldValues["LPRCount"] = 0;
	FDTableControllers->FieldValues["SchedulePeriodUnit"] = 3;
	WriteBool(FDTableControllers,"ScheduleDecay",true);
	WriteBool(FDTableControllers,"ScheduleLPR",true);
	FDTableControllers->FieldValues["CtrlScheduleStatus"] = 0;
	FDTableControllers->FieldValues["LPRMode"] = 0;

	FDTableControllers->FieldValues["DecayDelay"] = 500;
	FDTableControllers->FieldValues["DecaySampInterval2"] = 2;
	FDTableControllers->FieldValues["DecayDuration2"] = 18;

	WriteBool(FDTableControllers,"WatchDogEnable",false);
	FDTableControllers->FieldValues["BusIntSerialNo"] = 0;
	FDTableControllers->FieldValues["BusIntVerMajor"] = 0;
	FDTableControllers->FieldValues["BusIntVerMinor"] = 0;

	WriteBool(FDTableControllers,"ScheduleZRA",false);
	WriteBool(FDTableControllers,"ScheduleResMes",false);

	FDTableControllers->Post();
	FDTableControllers->ApplyUpdates();
	FDTableControllers->CommitUpdates();
	FDTableControllers->Active = false;
   }
  catch (const Exception &E)
   {
	Debug(Name + "::CreateTabControllers() " + FDTableControllers->TableName + ":" + E.Message);
	FDTableControllers->Cancel();
   }
}

//---------------------------------------------------------------------------
//
// Description:
// Check if controllers table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabControllers()
{
 if (!SQLFieldExists(DBName,"Controllers","DecayDelay"))
  {
   SQLAddField(DBName,"Controllers","DecayDelay",ftInteger,"500");
  }

 if (!SQLFieldExists(DBName,"Controllers","DecaySampInterval2"))
  {
   SQLAddField(DBName,"Controllers","DecaySampInterval2",ftInteger,"2");
  }

 if (!SQLFieldExists(DBName,"Controllers","DecayDuration2"))
  {
   SQLAddField(DBName,"Controllers","DecayDuration2",ftInteger,"18");
  }

 if (!SQLFieldExists(DBName,"Controllers","WatchDogEnable"))
  {
	 SQLAddField(DBName,"Controllers","WatchDogEnable",ftBoolean,"0");
  }
 if (!SQLFieldExists(DBName,"Controllers","BusIntSerialNo"))
	{
	 SQLAddField(DBName,"Controllers","BusIntSerialNo",ftInteger,"0");
	}
 if (!SQLFieldExists(DBName,"Controllers","BusIntVerMajor"))
  {
   SQLAddField(DBName,"Controllers","BusIntVerMajor",ftInteger,"0");
  }

 if (!SQLFieldExists(DBName,"Controllers","BusIntVerMinor"))
  {
   SQLAddField(DBName,"Controllers","BusIntVerMinor",ftInteger,"0");
	}

 if (!SQLFieldExists(DBName,"Controllers","MonitorExtCount"))
  {
	 SQLAddField(DBName,"Controllers","MonitorExtCount",ftInteger,"0",0,"MonitorCount");
	}

 if (!SQLFieldExists(DBName,"Controllers","ScheduleZRA"))
	{
	 SQLAddField(DBName,"Controllers","ScheduleZRA",ftBoolean,"0");
	 SQLAddField(DBName,"Controllers","ScheduleResMes",ftBoolean,"0");
	}

 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create wlink table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabWLink()
{
 IFrm->Msg("Creating TabWLink");

 FDTableWLink->FieldDefs->Clear();
 FDTableWLink->FieldDefs->Add( "WLinkSerialNo", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkCanAdr", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkStatus", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkName", ftString, 20, false );
 FDTableWLink->FieldDefs->Add( "WLinkType", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkRequestStatus", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkConnected", ftBoolean, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkVerMajor", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkVerMinor", ftInteger, 0, false );

 FDTableWLink->FieldDefs->Add( "WLinkDH", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkDL", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkMY", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkCH", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkID", ftInteger, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkPL", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkSignal", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "WLinkMode", ftSmallint, 0, false );
 FDTableWLink->FieldDefs->Add( "VerNotSupported", ftBoolean, 0, false );

 FDTableWLink->FieldDefs->Add( "CtrlName", ftString, 20, false );

 FDTableWLink->IndexDefs->Clear();
 FDTableWLink->IndexDefs->Add( "IxWLinkSerialNo", "WLinkSerialNo", TIndexOptions() <<ixPrimary );

//  FDTableWLink->CreateTable();

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"WLink",FDTableWLink);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if WLink table needs upgrade
//
// Comment: Add non-existant fields if necessary
//
bool __fastcall TCIISDBModule::CheckUpdateTabWLink()
{
	if (!SQLFieldExists(DBName,"WLink","VerNotSupported"))
	{
			SQLAddField(DBName,"WLink","VerNotSupported",ftBoolean,"0");
	}


	return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create zones table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabZones()
{
	IFrm->Msg("Creating TabZones");
  FDTableZones->FieldDefs->Clear();
  FDTableZones->FieldDefs->Add( "ZoneNo", ftInteger, 0, false );
  FDTableZones->FieldDefs->Add( "ZoneName", ftString, 20, false );
  FDTableZones->FieldDefs->Add( "AnodeArea", ftFloat, 0, false );
  FDTableZones->FieldDefs->Add( "CathodeArea", ftFloat, 0, false );
  FDTableZones->FieldDefs->Add( "Comment", ftString, 100, false );
  FDTableZones->FieldDefs->Add( "ZoneSampInterval", ftSmallint, 0, false );
  FDTableZones->FieldDefs->Add( "RecType", ftSmallint, 0, false ); // Stp/Sup/LPR/Dec
  FDTableZones->FieldDefs->Add( "RecNo", ftInteger, 0, false );
  FDTableZones->FieldDefs->Add( "ZoneCanAdr", ftSmallint, 0, false );
  FDTableZones->FieldDefs->Add( "ZoneAlarmStatus", ftInteger, 0, false );
  FDTableZones->FieldDefs->Add( "CtrlName", ftString, 20, false );
	FDTableZones->FieldDefs->Add( "ZoneScheduleStatus", ftInteger, 0, false );
	FDTableZones->FieldDefs->Add( "BIChSerNo", ftInteger, 0, false );
	FDTableZones->FieldDefs->Add( "IncludeInSchedule", ftBoolean, 0, false );
	FDTableZones->FieldDefs->Add( "RecTypeBeforeSchedule", ftSmallint, 0, false ); // Stp/Sup/LPR/Dec
	FDTableZones->FieldDefs->Add( "uCtrl", ftBoolean, 0, false );
	FDTableZones->FieldDefs->Add( "PSOffOnAlarm", ftBoolean, 0, false );
	FDTableZones->FieldDefs->Add( "PSOffHold", ftBoolean, 0, false );
  FDTableZones->IndexDefs->Clear();
  FDTableZones->IndexDefs->Add( "IxZonelNo", "ZoneNo", TIndexOptions() <<ixPrimary );

		  // MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Zones",FDTableZones);

  FDConnection->ExecSQL(SQLString);
  try
  {
	FDTableZones->Active = true;
	FDTableZones->Append();
	FDTableZones->FieldValues["ZoneNo"] = 1;
	FDTableZones->FieldValues["ZoneName"] = "Available";
	FDTableZones->FieldValues["Comment"] = "Unused senors";
	FDTableZones->FieldValues["ZoneSampInterval"] = 3;
	FDTableZones->FieldValues["RecType"] = 0;
	FDTableZones->FieldValues["RecNo"] = 0;
	FDTableZones->FieldValues["ZoneCanAdr"] = CANStartAdrZone;
	FDTableZones->FieldValues["ZoneAlarmStatus"] = 0;
	FDTableZones->FieldValues["CtrlName"] = "Controller 1";
	FDTableZones->FieldValues["ZoneScheduleStatus"] = 0;
	FDTableZones->FieldValues["BIChSerNo"] = 0;
	WriteBool(FDTableZones,"IncludeInSchedule",false);
	FDTableZones->FieldValues["RecTypeBeforeSchedule"] = 0;
	WriteBool(FDTableZones,"uCtrl",false);
	WriteBool(FDTableZones,"PSOffOnAlarm",false);
	WriteBool(FDTableZones,"PSOffHold",false);

	FDTableZones->Post();
	FDTableZones->ApplyUpdates();
	FDTableZones->CommitUpdates();

	FDTableZones->Active = false;
  }
  catch (const Exception &E)
  {
	Debug(Name + "::CreateTabZones() " + FDTableZones->TableName + ":" + E.Message);
	FDTableZones->Cancel();
  }
}
//---------------------------------------------------------------------------
//
// Description:
// Check if zones table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabZones()
{
 if (!SQLFieldExists(DBName,"Zones","BIChSerNo"))
	{
	 SQLAddField(DBName,"Zones","BIChSerNo",ftInteger,"0");
	}

 if (!SQLFieldExists(DBName,"Zones","IncludeInSchedule"))
	{
	 SQLAddField(DBName,"Zones","IncludeInSchedule",ftBoolean,"-1");
	}


 if (!SQLFieldExists(DBName,"Zones","RecTypeBeforeSchedule"))
	{
	 SQLAddField(DBName,"Zones","RecTypeBeforeSchedule",ftSmallint,"0");
	}

 if (!SQLFieldExists(DBName,"Zones","uCtrl"))
	{
	 SQLAddField(DBName,"Zones","uCtrl",ftBoolean,"0");
	}

 if (!SQLFieldExists(DBName,"Zones","PSOffOnAlarm"))
	{
	 SQLAddField(DBName,"Zones","PSOffOnAlarm",ftBoolean,"0");
	}

 if (!SQLFieldExists(DBName,"Zones","PSOffHold"))
	{
	 SQLAddField(DBName,"Zones","PSOffHold",ftBoolean,"0");
	}

 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create sensors table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabSensors()
{
	IFrm->Msg("Creating TabSensors");

  FDTableSensors->FieldDefs->Clear();
  FDTableSensors->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorCanAdr", ftSmallint, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorStatus", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorName", ftString, 20, false );
  FDTableSensors->FieldDefs->Add( "SensorType", ftSmallint, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorLastValue", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "PreCommOff", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "PreCommLPR", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorArea", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorGain", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorOffset", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorUnit", ftString, 4, false );
  FDTableSensors->FieldDefs->Add( "SensorLow", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorHigh", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorAlarmEnabled", ftBoolean, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorAlarmStatus", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorXPos", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorYPos", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorZPos", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorSectionNo", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "RequestStatus", ftSmallint, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorIShunt", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "ZoneNo", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorLPRStep", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorConnected", ftBoolean, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorWarmUp", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorTemp", ftBoolean, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorLastTemp", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorVerMajor", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorVerMinor", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorIShunt1", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorIShunt2", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorIShunt3", ftFloat, 0, false );
  FDTableSensors->FieldDefs->Add( "SensorLastValue2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorGain2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorOffset2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorUnit2", ftString, 4, false );
	FDTableSensors->FieldDefs->Add( "SensorLow2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorHigh2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorAlarmStatus2", ftInteger, 0, false );
  FDTableSensors->FieldDefs->Add( "PowerShutdownEnabled", ftBoolean, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorChannelCount", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorName2", ftString, 20, false );
	FDTableSensors->FieldDefs->Add( "SensorName3", ftString, 20, false );
	FDTableSensors->FieldDefs->Add( "SensorName4", ftString, 20, false );
	FDTableSensors->FieldDefs->Add( "SensorGain3", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorOffset3", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorUnit3", ftString, 4, false );
	FDTableSensors->FieldDefs->Add( "SensorLow3", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorHigh3", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorAlarmStatus3", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorGain4", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorOffset4", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorUnit4", ftString, 4, false );
	FDTableSensors->FieldDefs->Add( "SensorLow4", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorHigh4", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorAlarmStatus4", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "SensorLPRIRange", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "VerNotSupported", ftBoolean, 0, false );
	FDTableSensors->FieldDefs->Add( "DisabledChannels", ftInteger, 0, false );
	FDTableSensors->FieldDefs->Add( "PreCommOff2", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "PreCommOff3", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "PreCommOff4", ftFloat, 0, false );
	FDTableSensors->FieldDefs->Add( "CANAlarmEnabled", ftBoolean, 0, false );
	FDTableSensors->FieldDefs->Add( "CANAlarmStatus", ftInteger, 0, false );

  FDTableSensors->IndexDefs->Clear();
  FDTableSensors->IndexDefs->Add( "IxSensorSerialNo", "SensorSerialNo", TIndexOptions() <<ixPrimary );

//  FDTableSensors->CreateTable();
			// MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Sensors",FDTableSensors);

  FDConnection->ExecSQL(SQLString);

}
//---------------------------------------------------------------------------
//
// Description:
// Check if sensors table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabSensors()
{
	if (!SQLFieldExists(DBName,"Sensors","PowerShutdownEnabled"))
	{
			SQLAddField(DBName,"Sensors","PowerShutdownEnabled",ftBoolean,"0");
	}
	if (!SQLFieldExists(DBName,"Sensors","SensorChannelCount"))
	{
			SQLAddField(DBName,"Sensors","SensorChannelCount",ftInteger,"4");
	}
	if (!SQLFieldExists(DBName,"Sensors","SensorName2"))
	{
			SQLAddField(DBName,"Sensors","SensorName2",ftString,"'-2'",20);
			SQLAddField(DBName,"Sensors","SensorName3",ftString,"'-3'",20);
			SQLAddField(DBName,"Sensors","SensorName4",ftString,"'-4'",20);

			SQLAddField(DBName,"Sensors","SensorGain3",ftFloat,"1");
			SQLAddField(DBName,"Sensors","SensorOffset3",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorUnit3",ftString,"'mV'",4);
			SQLAddField(DBName,"Sensors","SensorLow3",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorHigh3",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorAlarmStatus3",ftInteger,"0");

			SQLAddField(DBName,"Sensors","SensorGain4",ftFloat,"1");
			SQLAddField(DBName,"Sensors","SensorOffset4",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorUnit4",ftString,"'mV'",4);
			SQLAddField(DBName,"Sensors","SensorLow4",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorHigh4",ftFloat,"0");
			SQLAddField(DBName,"Sensors","SensorAlarmStatus4",ftInteger,"0");
	}
	if  (!SQLFieldExists(DBName,"Sensors","SensorLPRIRange"))
	{
			SQLAddField(DBName,"Sensors","SensorLPRIRange",ftInteger,"2");
	}
	if  (!SQLFieldExists(DBName,"Sensors","VerNotSupported"))
	{
			SQLAddField(DBName,"Sensors","VerNotSupported",ftBoolean,"-1");
	}
	if  (!SQLFieldExists(DBName,"Sensors","DisabledChannels"))
	{
			SQLAddField(DBName,"Sensors","DisabledChannels",ftInteger,"0");
	}
	if  (!SQLFieldExists(DBName,"Sensors","PreCommOff2"))
	{
			SQLAddField(DBName,"Sensors","PreCommOff2",ftFloat,"0");
			SQLAddField(DBName,"Sensors","PreCommOff3",ftFloat,"0");
			SQLAddField(DBName,"Sensors","PreCommOff4",ftFloat,"0");
	}
	if  (!SQLFieldExists(DBName,"Sensors","CANAlarmEnabled"))
	{
			SQLAddField(DBName,"Sensors","CANAlarmEnabled",ftBoolean,"-1");
			SQLAddField(DBName,"Sensors","CANAlarmStatus",ftInteger,"0");
	}

		return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create power supply table
//
// Comment:
//


void __fastcall TCIISDBModule::CreateTabAlarm()
{
 IFrm->Msg("Creating TabAlarm");

 FDTableAlarm->FieldDefs->Clear();
 FDTableAlarm->FieldDefs->Add( "AlarmSerialNo", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmCanAdr", ftSmallint, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmStatus", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmName", ftString, 20, false );
 FDTableAlarm->FieldDefs->Add( "AlarmType", ftSmallint, 0, false );
 FDTableAlarm->FieldDefs->Add( "RequestStatus", ftSmallint, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmConnected", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmVerMajor", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "AlarmVerMinor", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO1", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO2", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO3", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO4", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO1AlarmType", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO2AlarmType", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO3AlarmType", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "DO4AlarmType", ftInteger, 0, false );
 FDTableAlarm->FieldDefs->Add( "InvDO1", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "InvDO2", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "InvDO3", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "InvDO4", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "VerNotSupported", ftBoolean,0, false );
 FDTableAlarm->FieldDefs->Add( "CANAlarmEnabled", ftBoolean, 0, false );
 FDTableAlarm->FieldDefs->Add( "CANAlarmStatus", ftInteger, 0, false );

 FDTableAlarm->FieldDefs->Add( "ZoneNo", ftInteger, 0, false );

 FDTableAlarm->IndexDefs->Clear();
 FDTableAlarm->IndexDefs->Add( "IxAlarmSerialNo", "AlarmSerialNo", TIndexOptions() <<ixPrimary );

//  FDTableAlarm->CreateTable();

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Alarm",FDTableAlarm);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if Alarm table needs upgrade
//
// Comment: Add non-existant fields if necessary
//
bool __fastcall TCIISDBModule::CheckUpdateTabAlarm()
{
	if (!SQLFieldExists(DBName,"Alarm","InvDO1"))
	{
			SQLAddField(DBName,"Alarm","InvDO1",ftBoolean,"0");
			SQLAddField(DBName,"Alarm","InvDO2",ftBoolean,"0");
			SQLAddField(DBName,"Alarm","InvDO3",ftBoolean,"0");
			SQLAddField(DBName,"Alarm","InvDO4",ftBoolean,"0");
			SQLAddField(DBName,"Alarm","VerNotSupported",ftBoolean,"0");
	}
	if (!SQLFieldExists(DBName,"Alarm","CANAlarmEnabled"))
	{
			SQLAddField(DBName,"Alarm","CANAlarmEnabled",ftBoolean,"-1");
			SQLAddField(DBName,"Alarm","CANAlarmStatus",ftInteger,"0");

	}


 return true;
}

void __fastcall TCIISDBModule::CreateTabBIChannels()
{
 IFrm->Msg("Creating TabBIChannels");

 FDTableBIChannels->FieldDefs->Clear();
 FDTableBIChannels->FieldDefs->Add( "SerialNo", ftInteger, 0, false );
 FDTableBIChannels->FieldDefs->Add( "Type", ftSmallint, 0, false );
 FDTableBIChannels->FieldDefs->Add( "Name", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "Included", ftBoolean, 0, false );
 FDTableBIChannels->FieldDefs->Add( "Active", ftBoolean, 0, false );
 FDTableBIChannels->FieldDefs->Add( "WatchDog", ftBoolean, 0, false );
 FDTableBIChannels->FieldDefs->Add( "WDTime", ftInteger, 0, false );
 FDTableBIChannels->FieldDefs->Add( "Mode", ftSmallint, 0, false );
 FDTableBIChannels->FieldDefs->Add( "IP", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "StaticIP", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "Gateway", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "Netmask", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "MACAddress", ftString, 20, false );
 FDTableBIChannels->FieldDefs->Add( "VerMajor", ftInteger, 0, false );
 FDTableBIChannels->FieldDefs->Add( "VerMinor", ftInteger, 0, false );
 FDTableBIChannels->FieldDefs->Add( "VerNotSupported", ftBoolean, 0, false );

 FDTableBIChannels->IndexDefs->Clear();
 FDTableBIChannels->IndexDefs->Add( "IxSerialNo", "SerialNo", TIndexOptions() <<ixPrimary );

	// MySQL Create table
	String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"BIChannels",FDTableBIChannels);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if BIChannels table needs upgrade
//
// Comment: Add non-existant fields if necessary
//
bool __fastcall TCIISDBModule::CheckUpdateTabBIChannels()
{
	if (!SQLFieldExists(DBName,"BIChannels","VerNotSupported"))
	{
		SQLAddField(DBName,"BIChannels","VerNotSupported",ftBoolean,"0");
	}

	return true;
}


void __fastcall TCIISDBModule::CreateTabPowerSupply()
{
	IFrm->Msg("Creating TabPowerSupply");
  FDTablePowerSupply->FieldDefs->Clear();
  FDTablePowerSupply->FieldDefs->Add( "PSSerialNo", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSCanAdr", ftSmallint, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSStatus", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSName", ftString, 20, false );
  FDTablePowerSupply->FieldDefs->Add( "PSSerialID", ftString, 20, false );
  FDTablePowerSupply->FieldDefs->Add( "PSType", ftSmallint, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLastValueU", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLastValueI", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSMode", ftSmallint, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSSetVoltage", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSSetCurrent", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSOutGain", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSOutOffset", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLowU", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLowI", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSHighU", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSHighI", ftFloat, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "PSAlarmEnabled", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "PSAlarmStatusU", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSAlarmStatusI", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSVOutEnabled", ftBoolean, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSRemote", ftBoolean, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "RequestStatus", ftSmallint, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "ZoneNo", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSConnected", ftBoolean, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLastValueCh3", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSLastValueCh4", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch1Gain", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch1Offset", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch1Unit", ftString, 4, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch2Gain", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch2Offset", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch2Unit", ftString, 4, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch3Gain", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch3Offset", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch3Unit", ftString, 4, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch4Gain", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch4Offset", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch4Unit", ftString, 4, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch1BitVal", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch2BitVal", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch3BitVal", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "Ch4BitVal", ftFloat, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSVerMajor", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "PSVerMinor", ftInteger, 0, false );
  FDTablePowerSupply->FieldDefs->Add( "SenseGuardEnabled", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "Fallback", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "PSTemp", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "VerNotSupported", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "DisabledChannels", ftInteger, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "InvDO1", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "InvDO2", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "CANAlarmEnabled", ftBoolean, 0, false );
	FDTablePowerSupply->FieldDefs->Add( "CANAlarmStatus", ftInteger, 0, false );

  FDTablePowerSupply->IndexDefs->Clear();
	FDTablePowerSupply->IndexDefs->Add( "IxPSSerialNo", "PSSerialNo", TIndexOptions() <<ixPrimary );
//  TabPowerSupply->CreateTable();
			// MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"PowerSupply",FDTablePowerSupply);

  FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if power supply table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabPowerSupply()
{
 if (!SQLFieldExists(DBName,"PowerSupply","SenseGuardEnabled"))
  {
   SQLAddField(DBName,"PowerSupply","SenseGuardEnabled",ftBoolean,"0");
  }

 if (!SQLFieldExists(DBName,"PowerSupply","Fallback"))
  {
	 SQLAddField(DBName,"PowerSupply","Fallback",ftBoolean,"1");
  }

 if (!SQLFieldExists(DBName,"PowerSupply","PSTemp"))
	{
	 SQLAddField(DBName,"PowerSupply","PSTemp",ftBoolean,"0");
	}

 if (!SQLFieldExists(DBName,"PowerSupply","VerNotSupported"))
	{
	 SQLAddField(DBName,"PowerSupply","VerNotSupported",ftBoolean,"0");
	}

 if (!SQLFieldExists(DBName,"PowerSupply","DisabledChannels"))
	{
	 SQLAddField(DBName,"PowerSupply","DisabledChannels",ftInteger,"0");
	}

 if (!SQLFieldExists(DBName,"PowerSupply","InvDO1"))
	{
	 SQLAddField(DBName,"PowerSupply","InvDO1",ftBoolean,"0");
	 SQLAddField(DBName,"PowerSupply","InvDO2",ftBoolean,"0");
	}

 if (!SQLFieldExists(DBName,"PowerSupply","CANAlarmEnabled"))
	{
	 SQLAddField(DBName,"PowerSupply","CANAlarmEnabled",ftBoolean,"1");
	 SQLAddField(DBName,"PowerSupply","CANAlarmStatus",ftInteger,"0");
	}


 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create recordings table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabRecordings()
{
	IFrm->Msg("Creating TabRecordings");
  FDTableRecordings->FieldDefs->Clear();
  FDTableRecordings->FieldDefs->Add( "RecNo", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "RecStart", ftDateTime, 0, false );
  FDTableRecordings->FieldDefs->Add( "RecStop", ftDateTime, 0, false );
  FDTableRecordings->FieldDefs->Add( "RecType", ftSmallint, 0, false ); //Mon/LPR/Dec
  FDTableRecordings->FieldDefs->Add( "DecaySampInterval", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "DecayDuration", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "LPRRange", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "LPRStep", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "LPRDelay1", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "LPRDelay2", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "SampInterval", ftInteger, 0, false );
	FDTableRecordings->FieldDefs->Add( "ZoneNo", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "LPRMode", ftInteger, 0, false );

  FDTableRecordings->FieldDefs->Add( "DecayDelay", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "DecaySampInterval2", ftInteger, 0, false );
  FDTableRecordings->FieldDefs->Add( "DecayDuration2", ftInteger, 0, false );

  FDTableRecordings->IndexDefs->Clear();
  FDTableRecordings->IndexDefs->Add( "IxRecNo", "RecNo", TIndexOptions() <<ixPrimary );
//  FDTableRecordings->CreateTable();
			// MySQL Create table
  String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"Recordings",FDTableRecordings);

  FDConnection->ExecSQL(SQLString);

}
//---------------------------------------------------------------------------
//
// Description:
// Check if recordings table needs upgrade
//
// Comment:
//
bool __fastcall TCIISDBModule::CheckUpdateTabRecordings()
{
 if (!SQLFieldExists(DBName,"Recordings","DecayDelay"))
  {
   SQLAddField(DBName,"Recordings","DecayDelay",ftInteger,"0");
  }

 if (!SQLFieldExists(DBName,"Recordings","DecaySampInterval2"))
  {
   SQLAddField(DBName,"Recordings","DecaySampInterval2",ftInteger,"0");
  }

 if (!SQLFieldExists(DBName,"Recordings","DecayDuration2"))
  {
   SQLAddField(DBName,"Recordings","DecayDuration2",ftInteger,"0");
  }

 return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Updates end of recordings
// Reads last value in corresponding value table with a matching recording nr
//
// Comment:
//

void __fastcall TCIISDBModule::UpdateRecEnd()
{
 if( ServerMode == CIISLocalServer )
 {
		// access only a part of the recordings table using a query
	FDTableRecordings->Active=false;
	FDTableRecordings->Filtered=false;
	FDTableRecordings->Filter="RecStop < RecStart";
	FDTableRecordings->Filtered=true;
	FDTableRecordings->Active=true;

	if( FDTableRecordings->FindFirst() )
	{
		do
		{
			int32_t RecNo = FDTableRecordings->FieldByName("RecNo")->AsInteger;
			switch (FDTableRecordings->FieldByName("RecType")->AsInteger)
			{
			case RT_Monitor:
				FDTableRecordings->Edit();
				if (FindLastMonitorValueInRecording(RecNo))
				{
					TDateTime RecStop=ReadDateTime(FDQueryValues,"DateTimeStamp");
					Debug("UpdateRecEnd(Monitor) RecNo:"+IntToStr(RecNo) + " from TabValues:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				else
				{
					TDateTime RecStop=ReadDateTime(FDTableRecordings,"RecStart");
					Debug("UpdateRecEnd(Monitor) RecNo:"+IntToStr(RecNo) + " from FDTableRecordings:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				FDTableRecordings->Post();
				FDTableRecordings->ApplyUpdates();
				FDTableRecordings->CommitUpdates();
				break;

			case RT_MonitorExt:
				FDTableRecordings->Edit();
				if (FindLastMonitorValueInRecording(RecNo))
				{
					TDateTime RecStop=ReadDateTime(FDQueryValues,"DateTimeStamp");
					Debug("UpdateRecEnd(Monitor) RecNo:"+IntToStr(RecNo) + " from TabValues:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				else
				{
					TDateTime RecStop=ReadDateTime(FDTableRecordings,"RecStart");
					Debug("UpdateRecEnd(Monitor) RecNo:"+IntToStr(RecNo) + " from FDTableRecordings:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				FDTableRecordings->Post();
				FDTableRecordings->ApplyUpdates();
				FDTableRecordings->CommitUpdates();
				break;

		 case RT_LPR:
				FDTableRecordings->Edit();
				if (FindLastLPRValueInRecording(RecNo))
				{
					TDateTime RecStop=ReadDateTime(FDQueryValuesLPR,"DateTimeStamp");
					Debug("UpdateRecEnd(LPR) RecNo:"+IntToStr(RecNo) + " from TabValuesLPR:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				else
				{
					TDateTime RecStop=ReadDateTime(FDTableRecordings,"RecStart");
					Debug("UpdateRecEnd(LPR) RecNo:"+IntToStr(RecNo) + " from FDTableRecordings:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}

				FDTableRecordings->Post();
				FDTableRecordings->ApplyUpdates();
				FDTableRecordings->CommitUpdates();
				break;

		 case RT_Decay:
				FDTableRecordings->Edit();
				if (FindLastDecayValueInRecording(RecNo))
				{
					TDateTime RecStop=ReadDateTime(FDQueryValuesDecay,"DateTimeStamp");
					Debug("UpdateRecEnd(Decay) RecNo:"+IntToStr(RecNo) + " from TabValuesDecay:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}
				else
				{
					TDateTime RecStop=ReadDateTime(FDTableRecordings,"RecStart");
					Debug("UpdateRecEnd(Decay) RecNo:"+IntToStr(RecNo) + " from FDTableRecordings:" + RecStop.DateTimeString());
					WriteDateTime(FDTableRecordings,"RecStop",RecStop);
				}

				FDTableRecordings->Post();
				FDTableRecordings->ApplyUpdates();
				FDTableRecordings->CommitUpdates();
				break;

			}
		}while (FDTableRecordings->FindNext());
	}

		// access entire table again
	FDTableRecordings->Active=false;
	FDTableRecordings->Filtered=false;
	FDTableRecordings->Filter="";
	FDTableRecordings->Active=true;
 }
}
//---------------------------------------------------------------------------
//
// Description:
// Create monitor values table
//
// Comment:
//

void __fastcall TCIISDBModule::CreateTabValues()
{
 IFrm->Msg("Creating TabValues");
 FDQueryValues->FieldDefs->Clear();
 FDQueryValues->FieldDefs->Add( "DateTimeStamp", ftDateTime, 0 , false );

			// MySQL
 FDQueryValues->FieldDefs->Add( "Fraction", ftInteger, 0 , false );

 FDQueryValues->FieldDefs->Add( "RecNo", ftInteger, 0, false );
 FDQueryValues->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
 FDQueryValues->FieldDefs->Add( "ValueType", ftString, 4, false );
 FDQueryValues->FieldDefs->Add( "ValueUnit", ftString, 4, false );
 FDQueryValues->FieldDefs->Add( "Value", ftFloat, 0, false );
 FDQueryValues->FieldDefs->Add( "RawValue", ftFloat, 0, false );
 FDQueryValues->FieldDefs->Add( "SampleDateTime", ftDateTime, 0 , false );

 FDQueryValues->IndexFieldNames="DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType";

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"ValuesMonitor",FDQueryValues);

 FDConnection->ExecSQL(SQLString);

}
//---------------------------------------------------------------------------
//
// Description:
// Check if values table needs upgrade
//
// Comment:
//

bool __fastcall TCIISDBModule::CheckUpdateTabValues()
{
 if (!SQLFieldExists(DBName,"ValuesMonitor","SampleDateTime"))
	{
	 SQLAddField(DBName,"ValuesMonitor","SampleDateTime",ftDateTime);
	 SQLUpdateField(DBName,"ValuesMonitor","SampleDateTime","DateTimeStamp" );
	}

 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create LPR values table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabValuesLPR()
{
 IFrm->Msg("Creating TabValuesLPR");
 FDQueryValuesLPR->FieldDefs->Clear();
 FDQueryValuesLPR->IndexDefs->Clear();

 FDQueryValuesLPR->FieldDefs->Add( "DateTimeStamp", ftDateTime, 0, false );
 FDQueryValuesLPR->FieldDefs->Add( "Fraction", ftInteger, 0, false );
 FDQueryValuesLPR->FieldDefs->Add( "RecNo", ftInteger, 0, false );
 FDQueryValuesLPR->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
 FDQueryValuesLPR->FieldDefs->Add( "ValueType", ftString, 4, false ); // 2010-11-12
 FDQueryValuesLPR->FieldDefs->Add( "ValueV", ftFloat, 0, false );
 FDQueryValuesLPR->FieldDefs->Add( "ValueI", ftFloat, 0, false );

 FDQueryValuesLPR->IndexDefs->Add( "IxValueLPR", "DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", TIndexOptions() <<ixPrimary );

 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"ValuesLPR",FDQueryValuesLPR);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if LPR values table needs upgrade
//
// Comment:
//

bool __fastcall TCIISDBModule::CheckUpdateTabValuesLPR()
{

 if (!SQLFieldExists(DBName,"ValuesLPR","ValueType"))
  {
	 SQLAddField(DBName,"ValuesLPR","ValueType",ftString,"'C1'",4,"SensorSerialNo");
   SQLDropPrimaryKey(DBName,"ValuesLPR");  // Drop primary index
   SQLAddPrimaryKey(DBName,"ValuesLPR","`DateTimeStamp`,`Fraction`,`RecNo`,`SensorSerialNo`,`ValueType`"); // Create new primary index including also RecNo,SensorSerialNo,ValueType
  }

 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create decay values table
//
// Comment:
//
void __fastcall TCIISDBModule::CreateTabValuesDecay()
{
 IFrm->Msg("Creating TabValuesDecay");
 FDQueryValuesDecay->FieldDefs->Clear();
 FDQueryValuesDecay->FieldDefs->Add( "DateTimeStamp", ftDateTime, 0 , false );
 FDQueryValuesDecay->FieldDefs->Add( "Fraction", ftInteger, 0 , false );
 FDQueryValuesDecay->FieldDefs->Add( "RecNo", ftInteger, 0, false );
 FDQueryValuesDecay->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
 FDQueryValuesDecay->FieldDefs->Add( "ValueType", ftString, 4, false );
 FDQueryValuesDecay->FieldDefs->Add( "Value", ftFloat, 0, false );
 FDQueryValuesDecay->FieldDefs->Add( "SampleDateTime", ftDateTime, 0 , false );
 FDQueryValuesDecay->IndexDefs->Clear();
 FDQueryValuesDecay->IndexDefs->Add( "IxValueDecay", "DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", TIndexOptions() <<ixPrimary );
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"ValuesDecay",FDQueryValuesDecay);
 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Check if decay values table needs upgrade
//
// Comment:
//

bool __fastcall TCIISDBModule::CheckUpdateTabValuesDecay()
{
 if (!SQLFieldExists(DBName,"ValuesDecay","ValueType"))
  {
   SQLAddField(DBName,"ValuesDecay","ValueType",ftString,"'U'",4,"SensorSerialNo");
   SQLDropPrimaryKey(DBName,"ValuesDecay");
   SQLAddPrimaryKey(DBName,"ValuesDecay","`DateTimeStamp`,`Fraction`,`RecNo`,`SensorSerialNo`,`ValueType`");
	}

 if (!SQLFieldExists(DBName,"ValuesDecay","SampleDateTime"))
	{
	 SQLAddField(DBName,"ValuesDecay","SampleDateTime",ftDateTime);
	 SQLUpdateField(DBName,"ValuesDecay","SampleDateTime","DateTimeStamp" );
	}
 return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Create miscellaneous values table
//
// Comment:
//

void __fastcall TCIISDBModule::CreateTabValuesMisc()
{
 IFrm->Msg("Creating TabValuesMisc");
 FDTableValuesMisc->FieldDefs->Clear();
 FDTableValuesMisc->FieldDefs->Add( "DateTimeStamp", ftDateTime, 0 , false );

			// MySQL
 FDTableValuesMisc->FieldDefs->Add( "Fraction", ftInteger, 0 , false );

 FDTableValuesMisc->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
 FDTableValuesMisc->FieldDefs->Add( "ValueType", ftString, 4, false );
 FDTableValuesMisc->FieldDefs->Add( "ValueUnit", ftString, 4, false );
 FDTableValuesMisc->FieldDefs->Add( "Value", ftFloat, 0, false );
 FDTableValuesMisc->IndexDefs->Clear();
 FDTableValuesMisc->IndexDefs->Add( "IxValue", "DateTimeStamp;Fraction;SensorSerialNo;ValueType", TIndexOptions() <<ixPrimary );

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"ValuesMisc",FDTableValuesMisc);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Create calculated values table
//
// Comment:
//

void __fastcall TCIISDBModule::CreateTabValuesCalc()
{
 IFrm->Msg("Creating TabValuesCalc");
 FDTableValuesCalc->FieldDefs->Clear();
 FDTableValuesCalc->FieldDefs->Add( "DateTimeStamp", ftDateTime, 0 , false );

			// MySQL
 FDTableValuesCalc->FieldDefs->Add( "Fraction", ftInteger, 0 , false );

 FDTableValuesCalc->FieldDefs->Add( "RecNo", ftInteger, 0, false );
 FDTableValuesCalc->FieldDefs->Add( "SensorSerialNo", ftInteger, 0, false );
 FDTableValuesCalc->FieldDefs->Add( "ValueType", ftString, 4, false );
 FDTableValuesCalc->FieldDefs->Add( "ValueUnit", ftString, 4, false );
 FDTableValuesCalc->FieldDefs->Add( "Value", ftFloat, 0, false );
 FDTableValuesCalc->FieldDefs->Add( "Code", ftInteger, 0, false );
 FDTableValuesCalc->IndexDefs->Clear();
 FDTableValuesCalc->IndexDefs->Add( "IxValue", "DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", TIndexOptions() <<ixPrimary );

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"ValuesCalc",FDTableValuesCalc);

 FDConnection->ExecSQL(SQLString);
}
//---------------------------------------------------------------------------
//
// Description:
// Create event log table
//
// Comment:
//

void __fastcall TCIISDBModule::CreateTabEventLog()
{
 IFrm->Msg("Creating TabEventLog");
 FDTableEventLog->FieldDefs->Clear();
 FDTableEventLog->FieldDefs->Add( "EventDateTime", ftDateTime, 0 , false );
				// MySQL
 FDTableEventLog->FieldDefs->Add( "Fraction", ftInteger, 0 , false );

 FDTableEventLog->FieldDefs->Add( "EventType", ftInteger, 0, false );
 FDTableEventLog->FieldDefs->Add( "EventCode", ftInteger, 0, false );
 FDTableEventLog->FieldDefs->Add( "EventLevel", ftSmallint, 0, false );
 FDTableEventLog->FieldDefs->Add( "EventNo", ftInteger, 0, false );
 FDTableEventLog->FieldDefs->Add( "EventString", ftString, 20, false );
 FDTableEventLog->FieldDefs->Add( "EventInt", ftInteger, 0, false );
 FDTableEventLog->FieldDefs->Add( "EventFloat", ftFloat, 0, false );
 FDTableEventLog->IndexDefs->Clear();
 FDTableEventLog->IndexDefs->Add( "IxEvent", "EventDateTime;Fraction;EventType;EventCode;EventLevel;EventNo", TIndexOptions() <<ixPrimary );

			// MySQL Create table
 String SQLString = GenerateSQLCreateTable(/* Target DB */ DBName,"EventLog",FDTableEventLog);

 FDConnection->ExecSQL(SQLString);


 FDTableEventLog->Active = true;
}
//---------------------------------------------------------------------------
//
// Description:
// Check if event log table needs upgrade
//
// Comment:
//

bool __fastcall TCIISDBModule::CheckUpdateTabEventLog()
{
 return true;
}


String __fastcall TCIISDBModule::DatabaseName(void)
{
 return DBName;
}

bool __fastcall TCIISDBModule::SetDBDir( String NewDir )
{
		// MySQL
  CloseCIISDB();
		// NewDir is really a MySQL database-name
  return InitCIISDB(DBHostName,NewDir, CIISRemoteServer , DBUserName, DBPassword);

}

bool __fastcall TCIISDBModule::RecRunning()
{
 FDTableControllers->FindLast();
 return FDTableControllers->FieldByName("MonitorCount")->AsInteger > 0 ||
		FDTableControllers->FieldByName("MonitorExtCount")->AsInteger > 0 ||
		FDTableControllers->FieldByName("LPRCount")->AsInteger > 0 ||
		FDTableControllers->FieldByName("DecayCount")->AsInteger > 0;
}


//		Project table
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of the project
//
// Comment:
// Returns true if the project record exists
//
bool __fastcall TCIISDBModule::GetPrjRec( CIISPrjRec *PrjRec )
{
 bool FoundRec = false;
 if( FDTableProject->FindFirst() )
  {
   FoundRec = true;
   PrjRec->PrjName = FDTableProject->FieldByName("PrjName")->AsString;
   PrjRec->PrjNo = FDTableProject->FieldByName("PrjNo")->AsString;
   PrjRec->Client = FDTableProject->FieldByName("Client")->AsString;
   PrjRec->Consultant = FDTableProject->FieldByName("Consultant")->AsString;
   PrjRec->Manager = FDTableProject->FieldByName("Manager")->AsString;
   PrjRec->ContractDescription = FDTableProject->FieldByName("ContractDescription")->AsString;
   PrjRec->MMResponsibility = FDTableProject->FieldByName("MMResponsibility")->AsString;
   PrjRec->Commissioning = ReadDate(FDTableProject,"Commissioning");
   PrjRec->Criteria = FDTableProject->FieldByName("Criteria")->AsString;
   PrjRec->ConnType = FDTableProject->FieldByName("ConnType")->AsInteger;
   PrjRec->ConnRemote = FDTableProject->FieldByName("ConnRemote")->AsString;
   PrjRec->ConnServerIP = FDTableProject->FieldByName("ConnServerIP")->AsString;
	 PrjRec->PrjAlarmStatus = FDTableProject->FieldByName("PrjAlarmStatus")->AsInteger;
   PrjRec->EventLogSize = FDTableProject->FieldByName("EventLogSize")->AsInteger;
   PrjRec->EventLevel = FDTableProject->FieldByName("EventLevel")->AsInteger;
   PrjRec->ServerStatus = (enum CIIServerStatus)FDTableProject->FieldByName("ServerStatus")->AsInteger;
   PrjRec->ServerMode = (enum CIISServerMode)FDTableProject->FieldByName("ServerMode")->AsInteger;
   PrjRec->ConfigVer = FDTableProject->FieldByName("ConfigVer")->AsInteger;
   PrjRec->DataVer = FDTableProject->FieldByName("DataVer")->AsInteger;
   PrjRec->LastValVer = FDTableProject->FieldByName("LastValVer")->AsInteger;
   PrjRec->DBVer = FDTableProject->FieldByName("DBVer")->AsInteger;
   PrjRec->PWReadOnly = FDTableProject->FieldByName("PWReadOnly")->AsString;
   PrjRec->PWModify = FDTableProject->FieldByName("PWModify")->AsString;
   PrjRec->PWErase = FDTableProject->FieldByName("PWErase")->AsString;
   PrjRec->PrgVer = FDTableProject->FieldByName("PrgVer")->AsInteger;
//   PrjRec->DTAdj = FDTableProject->FieldByName("DTAdj")->AsDateTime;
   PrjRec->DTAdj = ReadDateTime(FDTableProject,"DTAdj");
//   PrjRec->DTNew = FDTableProject->FieldByName("DTNew")->AsDateTime;
   PrjRec->DTNew = ReadDateTime(FDTableProject,"DTNew");
   PrjRec->ValuesMiscSize = FDTableProject->FieldByName("ValuesMiscSize")->AsInteger;
  }
 return FoundRec;
}
//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of the project and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesProject() in order to apply the changes.
//

bool __fastcall TCIISDBModule::SetPrjRec( CIISPrjRec *PrjRec )
{
 if( FDTableProject->FindFirst() )
  {
   FDTableProject->Edit();
   return PostPrjRec(PrjRec);
  }
 else
  return false;
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of the project and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesProject() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostPrjRec( CIISPrjRec *PrjRec )
{
	bool RecUpdated = false;
	FDTableProject->FieldValues["PrjName"] = PrjRec->PrjName;
	FDTableProject->FieldValues["PrjNo"] = PrjRec->PrjNo;
	FDTableProject->FieldValues["Client"] = PrjRec->Client;
	FDTableProject->FieldValues["Consultant"] = PrjRec->Consultant;
	FDTableProject->FieldValues["Manager"] = PrjRec->Manager;
	FDTableProject->FieldValues["ContractDescription"] = PrjRec->ContractDescription;
	FDTableProject->FieldValues["MMResponsibility"] = PrjRec->MMResponsibility;
	// FDTableProject->FieldValues["Commissioning"] = PrjRec->Commissioning;
	WriteDate(FDTableProject,"Commissioning",PrjRec->Commissioning);
	FDTableProject->FieldValues["Criteria"] = PrjRec->Criteria;
	FDTableProject->FieldValues["ConnType"] = PrjRec->ConnType;
	FDTableProject->FieldValues["ConnRemote"] = PrjRec->ConnRemote;
	FDTableProject->FieldValues["ConnServerIP"] = PrjRec->ConnServerIP;
	FDTableProject->FieldValues["PrjAlarmStatus"] = PrjRec->PrjAlarmStatus;
	FDTableProject->FieldValues["EventLogSize"] = PrjRec->EventLogSize;
	FDTableProject->FieldValues["EventLevel"] = PrjRec->EventLevel;
	FDTableProject->FieldValues["ServerStatus"] = PrjRec->ServerStatus;
	FDTableProject->FieldValues["ServerMode"] = PrjRec->ServerMode;
	FDTableProject->FieldValues["ConfigVer"] = PrjRec->ConfigVer;
	FDTableProject->FieldValues["DataVer"] = PrjRec->DataVer;
	FDTableProject->FieldValues["LastValVer"] = PrjRec->LastValVer;
	FDTableProject->FieldValues["DBVer"] = PrjRec->DBVer;
	FDTableProject->FieldValues["PWReadOnly"] = PrjRec->PWReadOnly;
	FDTableProject->FieldValues["PWModify"] = PrjRec->PWModify;
	FDTableProject->FieldValues["PWErase"] = PrjRec->PWErase;
	FDTableProject->FieldValues["PrgVer"] = PrjRec->PrgVer;
	WriteDateTime(FDTableProject,"DTAdj",PrjRec->DTAdj);
	WriteDateTime(FDTableProject,"DTNew",PrjRec->DTNew);

	try
	{
		FDTableProject->Post();
		RecUpdated = true;
	}
	catch (const Exception &E)
	{
		RecUpdated = false;
		Debug(Name + "::PostPrjRec() " + FDTableProject->TableName + ":" + E.Message);
		FDTableProject->Cancel();
	}
 	return RecUpdated;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to project table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesPrj(void)
{
 int32_t NoOfErrors=FDTableProject->ApplyUpdates();
 FDTableProject->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesPrj() " + FDTableProject->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}

//		Ctrl table

bool __fastcall TCIISDBModule::FindFirstCtrl()
{
 return FDTableControllers->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate a controller(with a given CtrlName) in the database
//
// Comment: return true if controller found
//
bool __fastcall TCIISDBModule::LocateCtrlRec( CIISCtrlRec *CtrlRec )
{
 return FDTableControllers->Locate( "CtrlName", CtrlRec->CtrlName, NoOpts );
}
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a controller
//
// Comment:
// Returns true if the next controller record exists
// Returns false if the no more controller records available
//

bool __fastcall TCIISDBModule::GetCtrlRec( CIISCtrlRec *CtrlRec, bool AutoInc )
{
 CtrlRec->CtrlName = FDTableControllers->FieldByName("CtrlName")->AsString;
 CtrlRec->NoIR = ReadBool(FDTableControllers,"NoIR");
 CtrlRec->ScheduleDateTime = ReadDateTime(FDTableControllers,"ScheduleDateTime");
 CtrlRec->SchedulePeriod = FDTableControllers->FieldByName("SchedulePeriod")->AsInteger;
 CtrlRec->DecaySampInterval = FDTableControllers->FieldByName("DecaySampInterval")->AsInteger;
 CtrlRec->DecayDuration = FDTableControllers->FieldByName("DecayDuration")->AsInteger;
 CtrlRec->LPRRange = FDTableControllers->FieldByName("LPRRange")->AsInteger;
 CtrlRec->LPRStep = FDTableControllers->FieldByName("LPRStep")->AsInteger;
 CtrlRec->LPRDelay1 = FDTableControllers->FieldByName("LPRDelay1")->AsInteger;
 CtrlRec->LPRDelay2 = FDTableControllers->FieldByName("LPRDelay2")->AsInteger;
 CtrlRec->SampInterval1 = FDTableControllers->FieldByName("SampInterval1")->AsInteger;
 CtrlRec->SampInterval2 = FDTableControllers->FieldByName("SampInterval2")->AsInteger;
 CtrlRec->SampInterval3 = FDTableControllers->FieldByName("SampInterval3")->AsInteger;
 CtrlRec->SampInterval4 = FDTableControllers->FieldByName("SampInterval4")->AsInteger;
 CtrlRec->SampInterval5 = FDTableControllers->FieldByName("SampInterval5")->AsInteger;
 CtrlRec->NextCanAdr = FDTableControllers->FieldByName("NextCanAdr")->AsInteger;
 CtrlRec->DefaultSensorLow = FDTableControllers->FieldByName("DefaultSensorLow")->AsFloat;
 CtrlRec->DefaultSensorHigh = FDTableControllers->FieldByName("DefaultSensorHigh")->AsFloat;
 CtrlRec->DefaultAlarmEnable = ReadBool(FDTableControllers,"DefaultAlarmEnable");
 CtrlRec->CtrlAlarmStatus = FDTableControllers->FieldByName("CtrlAlarmStatus")->AsInteger;
 CtrlRec->USBCANStatus = FDTableControllers->FieldByName("USBCANStatus")->AsInteger;
 CtrlRec->NodeCount = FDTableControllers->FieldByName("NodeCount")->AsInteger;
 CtrlRec->MonitorCount = FDTableControllers->FieldByName("MonitorCount")->AsInteger;
 CtrlRec->MonitorExtCount = FDTableControllers->FieldByName("MonitorExtCount")->AsInteger;
 CtrlRec->DecayCount = FDTableControllers->FieldByName("DecayCount")->AsInteger;
 CtrlRec->LPRCount = FDTableControllers->FieldByName("LPRCount")->AsInteger;
 CtrlRec->SchedulePeriodUnit = FDTableControllers->FieldByName("SchedulePeriodUnit")->AsInteger;
 CtrlRec->ScheduleDecay = ReadBool(FDTableControllers,"ScheduleDecay");
 CtrlRec->ScheduleLPR = ReadBool(FDTableControllers,"ScheduleLPR");
 CtrlRec->CtrlScheduleStatus = FDTableControllers->FieldByName("CtrlScheduleStatus")->AsInteger;
 CtrlRec->LPRMode = FDTableControllers->FieldByName("LPRMode")->AsInteger;

 CtrlRec->DecayDelay = FDTableControllers->FieldByName("DecayDelay")->AsInteger;
 CtrlRec->DecaySampInterval2 = FDTableControllers->FieldByName("DecaySampInterval2")->AsInteger;
 CtrlRec->DecayDuration2 = FDTableControllers->FieldByName("DecayDuration2")->AsInteger;

 CtrlRec->WatchDogEnable = ReadBool(FDTableControllers,"WatchDogEnable");
 CtrlRec->BusIntSerialNo = FDTableControllers->FieldByName("BusIntSerialNo")->AsInteger;
 CtrlRec->BusIntVerMajor = FDTableControllers->FieldByName("BusIntVerMajor")->AsInteger;
 CtrlRec->BusIntVerMinor = FDTableControllers->FieldByName("BusIntVerMinor")->AsInteger;

 CtrlRec->ScheduleZRA = ReadBool(FDTableControllers,"ScheduleZRA");
 CtrlRec->ScheduleResMes = ReadBool(FDTableControllers,"ScheduleResMes");

 CtrlRec->PrjName = FDTableControllers->FieldByName("PrjName")->AsString;

 if( AutoInc ) return FDTableControllers->FindNext();
 else return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a controller and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesCtrl() in order to apply the changes.
//

bool __fastcall TCIISDBModule::SetCtrlRec( CIISCtrlRec *CtrlRec )
{
 FDTableControllers->Edit();
 return PostCtrlRec(CtrlRec);
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a controller and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesCtrl() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostCtrlRec( CIISCtrlRec *CtrlRec )
{
 FDTableControllers->FieldValues["CtrlName"] = CtrlRec->CtrlName;
 WriteBool(FDTableControllers,"NoIR",CtrlRec->NoIR);
 WriteDateTime(FDTableControllers,"ScheduleDateTime",CtrlRec->ScheduleDateTime);
 FDTableControllers->FieldValues["SchedulePeriod"] = CtrlRec->SchedulePeriod;
 FDTableControllers->FieldValues["DecaySampInterval"] = CtrlRec->DecaySampInterval;
 FDTableControllers->FieldValues["DecayDuration"] = CtrlRec->DecayDuration;
 FDTableControllers->FieldValues["LPRRange"] = CtrlRec->LPRRange;
 FDTableControllers->FieldValues["LPRStep"] = CtrlRec->LPRStep;
 FDTableControllers->FieldValues["LPRDelay1"] = CtrlRec->LPRDelay1;
 FDTableControllers->FieldValues["LPRDelay2"] = CtrlRec->LPRDelay2;
 FDTableControllers->FieldValues["SampInterval1"] = CtrlRec->SampInterval1;
 FDTableControllers->FieldValues["SampInterval2"] = CtrlRec->SampInterval2;
 FDTableControllers->FieldValues["SampInterval3"] = CtrlRec->SampInterval3;
 FDTableControllers->FieldValues["SampInterval4"] = CtrlRec->SampInterval4;
 FDTableControllers->FieldValues["SampInterval5"] = CtrlRec->SampInterval5;
 FDTableControllers->FieldValues["NextCanAdr"] = CtrlRec->NextCanAdr;
 FDTableControllers->FieldValues["DefaultSensorLow"] = CtrlRec->DefaultSensorLow;
 FDTableControllers->FieldValues["DefaultSensorHigh"] = CtrlRec->DefaultSensorHigh;
 WriteBool(FDTableControllers,"DefaultAlarmEnable",CtrlRec->DefaultAlarmEnable);
 FDTableControllers->FieldValues["CtrlAlarmStatus"] = CtrlRec->CtrlAlarmStatus;
 FDTableControllers->FieldValues["USBCANStatus"] = CtrlRec->USBCANStatus;
 FDTableControllers->FieldValues["NodeCount"] = CtrlRec->NodeCount;
 FDTableControllers->FieldValues["MonitorCount"] = CtrlRec->MonitorCount;
 FDTableControllers->FieldValues["MonitorExtCount"] = CtrlRec->MonitorExtCount;
 FDTableControllers->FieldValues["DecayCount"] = CtrlRec->DecayCount;
 FDTableControllers->FieldValues["LPRCount"] = CtrlRec->LPRCount;
 FDTableControllers->FieldValues["SchedulePeriodUnit"] = CtrlRec->SchedulePeriodUnit;
 WriteBool(FDTableControllers,"ScheduleDecay",CtrlRec->ScheduleDecay);
 WriteBool(FDTableControllers,"ScheduleLPR",CtrlRec->ScheduleLPR);
 FDTableControllers->FieldValues["CtrlScheduleStatus"] = CtrlRec->CtrlScheduleStatus;
 FDTableControllers->FieldValues["LPRMode"] = CtrlRec->LPRMode;

 FDTableControllers->FieldValues["DecayDelay"] = CtrlRec->DecayDelay;
 FDTableControllers->FieldValues["DecaySampInterval2"] = CtrlRec->DecaySampInterval2;
 FDTableControllers->FieldValues["DecayDuration2"] = CtrlRec->DecayDuration2;

 WriteBool(FDTableControllers,"WatchDogEnable",CtrlRec->WatchDogEnable);
 FDTableControllers->FieldValues["BusIntSerialNo"] = CtrlRec->BusIntSerialNo;
 FDTableControllers->FieldValues["BusIntVerMajor"] = CtrlRec->BusIntVerMajor;
 FDTableControllers->FieldValues["BusIntVerMinor"] = CtrlRec->BusIntVerMinor;

 WriteBool(FDTableControllers,"ScheduleZRA",CtrlRec->ScheduleZRA);
 WriteBool(FDTableControllers,"ScheduleResMes",CtrlRec->ScheduleResMes);

 FDTableControllers->FieldValues["PrjName"] = CtrlRec->PrjName;
 bool RecUpdated = true;
 try
  {
   FDTableControllers->Post();
  }
 catch (const Exception &E)
  {
   RecUpdated = false;
   Debug(Name + "::PostCtrlRec() " + FDTableControllers->TableName + ":" + E.Message);
   FDTableControllers->Cancel();
  }
 return RecUpdated;
}

//
// Note! ApplyUpdates is necessary also after Delete!
//
bool	__fastcall TCIISDBModule::DeleteCtrlRec( CIISCtrlRec *CtrlRec )
{
 bool RecDeleted;

 if( LocateCtrlRec( CtrlRec ))
  try
   {
	FDTableControllers->Delete();
	RecDeleted = true;
   }
  catch (const Exception &E)
   {
	RecDeleted = false;
	Debug(Name + "::DeleteCtrlRec() " + FDTableControllers->TableName + ":" + E.Message);
   }
 return RecDeleted;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to controller table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesCtrl(void)
{
 int32_t NoOfErrors=FDTableControllers->ApplyUpdates();
 FDTableControllers->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesCtrl() " + FDTableControllers->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}

//		WLink table

uint32_t __fastcall TCIISDBModule::GetWLinkRecCount()
{
 return FDTableWLink->RecordCount;
}

void __fastcall TCIISDBModule::SetWLinkFilter( CIISCtrlRec *CtrlRec )
{
 FDTableWLink->Filter = "CtrlName = '" + CtrlRec->CtrlName + "'";
 FDTableWLink->Filtered = true;
}

void __fastcall TCIISDBModule::ClearWLinkFilter()

{
 FDTableWLink->Filter = "";
 FDTableWLink->Filtered = false;
 FDTableWLink->Refresh();
}

bool __fastcall TCIISDBModule::FindFirstWLink()
{
 return FDTableWLink->FindFirst();
}

bool __fastcall TCIISDBModule::LocateWLinkRec( CIISWLinkRec *WLinkRec )
{
 return FDTableWLink->Locate("WLinkSerialNo", WLinkRec->WLinkSerialNo, NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a WLink
//
// Comment:
// Note! AutoInc
// Returns true if the next WLink record exists
// Returns false if the no more WLink records are available
//

bool __fastcall TCIISDBModule::GetWLinkRec( CIISWLinkRec *WLinkRec, bool AutoInc )
{
 WLinkRec->WLinkSerialNo = FDTableWLink->FieldByName("WLinkSerialNo")->AsInteger;
 WLinkRec->WLinkCanAdr = FDTableWLink->FieldByName("WLinkCanAdr")->AsInteger;
 WLinkRec->WLinkStatus = FDTableWLink->FieldByName("WLinkStatus")->AsInteger;
 WLinkRec->WLinkName = FDTableWLink->FieldByName("WLinkName")->AsString;
 WLinkRec->WLinkType = FDTableWLink->FieldByName("WLinkType")->AsInteger;
 WLinkRec->WLinkRequestStatus = FDTableWLink->FieldByName("WLinkRequestStatus")->AsInteger;
 WLinkRec->WLinkConnected = ReadBool(FDTableWLink,"WLinkConnected");
 WLinkRec->WLinkVerMajor = FDTableWLink->FieldByName("WLinkVerMajor")->AsInteger;
 WLinkRec->WLinkVerMinor = FDTableWLink->FieldByName("WLinkVerMinor")->AsInteger;
 WLinkRec->WLinkDH = FDTableWLink->FieldByName("WLinkDH")->AsInteger;
 WLinkRec->WLinkDL = FDTableWLink->FieldByName("WLinkDL")->AsInteger;
 WLinkRec->WLinkMY = FDTableWLink->FieldByName("WLinkMY")->AsInteger;
 WLinkRec->WLinkCH = FDTableWLink->FieldByName("WLinkCH")->AsInteger;
 WLinkRec->WLinkID = FDTableWLink->FieldByName("WLinkID")->AsInteger;
 WLinkRec->WLinkPL = FDTableWLink->FieldByName("WLinkPL")->AsInteger;
 WLinkRec->WLinkSignal = FDTableWLink->FieldByName("WLinkSignal")->AsInteger;
 WLinkRec->WLinkMode = FDTableWLink->FieldByName("WLinkMode")->AsInteger;
 WLinkRec->VerNotSupported = ReadBool(FDTableWLink,"VerNotSupported");
 WLinkRec->CtrlName = FDTableWLink->FieldByName("CtrlName")->AsString;

 if( AutoInc )
  return FDTableWLink->FindNext();
 else
  return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a WLink and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesWLink() in order to apply the changes.
//

bool __fastcall TCIISDBModule::SetWLinkRec( CIISWLinkRec *WLinkRec)
{
 FDTableWLink->Edit();
 return PostWLinkRec(WLinkRec);
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a WLink and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesWLink() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostWLinkRec( CIISWLinkRec *WLinkRec)
{
 FDTableWLink->FieldValues["WLinkSerialNo"] = WLinkRec->WLinkSerialNo ;
 FDTableWLink->FieldValues["WLinkCanAdr"] = WLinkRec->WLinkCanAdr ;
 FDTableWLink->FieldValues["WLinkStatus"] = WLinkRec->WLinkStatus ;
 FDTableWLink->FieldValues["WLinkName"] = WLinkRec->WLinkName ;
 FDTableWLink->FieldValues["WLinkType"] = WLinkRec->WLinkType ;
 FDTableWLink->FieldValues["WLinkRequestStatus"] = WLinkRec->WLinkRequestStatus ;
 WriteBool(FDTableWLink,"WLinkConnected",WLinkRec->WLinkConnected);
 FDTableWLink->FieldValues["WLinkVerMajor"] = WLinkRec->WLinkVerMajor ;
 FDTableWLink->FieldValues["WLinkVerMinor"] = WLinkRec->WLinkVerMinor ;
 FDTableWLink->FieldValues["WLinkDH"] = WLinkRec->WLinkDH ;
 FDTableWLink->FieldValues["WLinkDL"] = WLinkRec->WLinkDL ;
 FDTableWLink->FieldValues["WLinkMY"] = WLinkRec->WLinkMY ;
 FDTableWLink->FieldValues["WLinkCH"] = WLinkRec->WLinkCH ;
 FDTableWLink->FieldValues["WLinkID"] = WLinkRec->WLinkID ;
 FDTableWLink->FieldValues["WLinkPL"] = WLinkRec->WLinkPL ;
 FDTableWLink->FieldValues["WLinkSignal"] = WLinkRec->WLinkSignal ;
 FDTableWLink->FieldValues["WLinkMode"] = WLinkRec->WLinkMode ;
 WriteBool(FDTableWLink,"VerNotSupported",WLinkRec->VerNotSupported);
 FDTableWLink->FieldValues["CtrlName"] = WLinkRec->CtrlName;
 bool RecUpdated= false;
 try
  {
   FDTableWLink->Post();
   RecUpdated= true;
  }
 catch (const Exception &E)
  {
   RecUpdated = false;
   Debug(Name + "::PostWLinkRec() " + FDTableWLink->TableName + ":" + E.Message);
   FDTableWLink->Cancel();
  }
 return RecUpdated;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new WLink record and edit the fields and post the changes
//
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesWLink() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendWLinkRec( CIISWLinkRec *WLinkRec )
{
 FDTableWLink->Append();
 return PostWLinkRec( WLinkRec );
}

//---------------------------------------------------------------------------
//
// Description:
// Delete the WLink record identified by WLinkNo
// Returns true if delete was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesWLink() in order to apply the changes.
//
bool	__fastcall TCIISDBModule::DeleteWLinkRec( CIISWLinkRec *WLinkRec )
{
 bool RecDeleted;

 if( LocateWLinkRec( WLinkRec ))
  try
   {
	FDTableWLink->Delete();
	RecDeleted = true;
   }
  catch (const Exception &E)
   {
	RecDeleted = false;
	Debug(Name + "::DeleteWLinkRec() " + FDTableWLink->TableName + ":" + E.Message);
   }
  return RecDeleted;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to WLinks table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesWLink(void)
{
 int32_t NoOfErrors=FDTableWLink->ApplyUpdates();
 FDTableWLink->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesWLink() " + FDTableWLink->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}



//		Zone table

uint32_t __fastcall TCIISDBModule::GetZoneRecCount()
{
 return FDTableZones->RecordCount;
}

void __fastcall TCIISDBModule::SetZoneFilter( CIISCtrlRec *CtrlRec )
{
 FDTableZones->Filter = "CtrlName = '" + CtrlRec->CtrlName + "'";
 FDTableZones->Filtered = true;
}

void __fastcall TCIISDBModule::ClearZoneFilter()

{
 FDTableZones->Filter = "";
 FDTableZones->Filtered = false;
 FDTableZones->Refresh();
}

bool __fastcall TCIISDBModule::FindFirstZone()
{
 return FDTableZones->FindFirst();
}

bool __fastcall TCIISDBModule::LocateZoneRec( CIISZoneRec *ZoneRec )
{
 return FDTableZones->Locate( "ZoneNo", ZoneRec->ZoneNo, NoOpts );
}
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a zone
//
// Comment:
// Returns true if the next zone record exists
// Returns false if the no more zone records are available
//

bool __fastcall TCIISDBModule::GetZoneRec( CIISZoneRec *ZoneRec, bool AutoInc )
{
 ZoneRec->ZoneNo = FDTableZones->FieldByName("ZoneNo")->AsInteger;
 ZoneRec->ZoneName = FDTableZones->FieldByName("ZoneName")->AsString;
 ZoneRec->AnodeArea = FDTableZones->FieldByName("AnodeArea")->AsFloat;
 ZoneRec->CathodeArea = FDTableZones->FieldByName("CathodeArea")->AsFloat;
 ZoneRec->Comment = FDTableZones->FieldByName("Comment")->AsString;
 ZoneRec->ZoneSampInterval = FDTableZones->FieldByName("ZoneSampInterval")->AsInteger;
 ZoneRec->RecType = FDTableZones->FieldByName("RecType")->AsInteger;
 ZoneRec->RecNo = FDTableZones->FieldByName("RecNo")->AsInteger;
 ZoneRec->ZoneCanAdr = FDTableZones->FieldByName("ZoneCanAdr")->AsInteger;
 ZoneRec->ZoneAlarmStatus = FDTableZones->FieldByName("ZoneAlarmStatus")->AsInteger;
 ZoneRec->ZoneScheduleStatus = FDTableZones->FieldByName("ZoneScheduleStatus")->AsInteger;
 ZoneRec->BIChSerNo = FDTableZones->FieldByName("BIChSerNo")->AsInteger;
 ZoneRec->IncludeInSchedule = ReadBool(FDTableZones,"IncludeInSchedule");
 ZoneRec->RecTypeBeforeSchedule = FDTableZones->FieldByName("RecTypeBeforeSchedule")->AsInteger;
 ZoneRec->uCtrl = ReadBool(FDTableZones,"uCtrl");
 ZoneRec->PSOffOnAlarm = ReadBool(FDTableZones,"PSOffOnAlarm");
 ZoneRec->PSOffHold = ReadBool(FDTableZones,"PSOffHold");
 ZoneRec->CtrlName = FDTableZones->FieldByName("CtrlName")->AsString;

 if( AutoInc ) return FDTableZones->FindNext();
 else return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a zone and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//

// Call ApplyUpdatesZone() in order to apply the changes.
//

bool __fastcall TCIISDBModule::SetZoneRec( CIISZoneRec *ZoneRec)
{
 FDTableZones->Edit();
 return PostZoneRec(ZoneRec);
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a zone and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesZone() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostZoneRec( CIISZoneRec *ZoneRec)
{
 FDTableZones->FieldValues["ZoneNo"] = ZoneRec->ZoneNo;
 FDTableZones->FieldValues["ZoneName"] = ZoneRec->ZoneName;
 FDTableZones->FieldValues["AnodeArea"] = ZoneRec->AnodeArea;
 FDTableZones->FieldValues["CathodeArea"] = ZoneRec->CathodeArea;
 FDTableZones->FieldValues["Comment"] = ZoneRec->Comment;
 FDTableZones->FieldValues["ZoneSampInterval"] = ZoneRec->ZoneSampInterval;
 FDTableZones->FieldValues["RecType"] = ZoneRec->RecType;
 FDTableZones->FieldValues["RecNo"] = ZoneRec->RecNo;
 FDTableZones->FieldValues["ZoneCanAdr"] = ZoneRec->ZoneCanAdr;
 FDTableZones->FieldValues["ZoneAlarmStatus"] = ZoneRec->ZoneAlarmStatus;
 FDTableZones->FieldValues["ZoneScheduleStatus"] = ZoneRec->ZoneScheduleStatus;
 FDTableZones->FieldValues["BIChSerNo"] = ZoneRec->BIChSerNo;
 WriteBool(FDTableZones,"IncludeInSchedule",ZoneRec->IncludeInSchedule);
 FDTableZones->FieldValues["RecTypeBeforeSchedule"] = ZoneRec->RecTypeBeforeSchedule;
 WriteBool(FDTableZones,"uCtrl",ZoneRec->uCtrl);
 WriteBool(FDTableZones,"PSOffOnAlarm",ZoneRec->PSOffOnAlarm);
 WriteBool(FDTableZones,"PSOffHold",ZoneRec->PSOffHold);
 FDTableZones->FieldValues["CtrlName"] = ZoneRec->CtrlName;
 bool RecUpdated= false;
 try
  {
   FDTableZones->Post();
   RecUpdated= true;
  }
 catch (const Exception &E)
  {
   RecUpdated = false;
   Debug(Name + "::PostZoneRec() " + FDTableZones->TableName + ":" + E.Message);
   FDTableZones->Cancel();
  }
 return RecUpdated;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new zone record and edit the fields and post the changes
// The new zone is assigned a ZoneNo = "last ZoneNo" + 1
//
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesZone() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendZoneRec( CIISZoneRec *ZoneRec, bool AutoInc )
{
	bool NewRecUpdated;
	int32_t ZoneNo;

	try
	{
		if( AutoInc )
		{
			FDTableZones->FindLast();
			ZoneNo = FDTableZones->FieldByName("ZoneNo")->AsInteger + 1;
			ZoneRec->ZoneNo = ZoneNo;
		}
		FDTableZones->Append();
		NewRecUpdated = true;
	}
	catch (const Exception &E)
	{
		NewRecUpdated = false;
		Debug(Name + "::AppendZoneRec() " + FDTableZones->TableName + ":" + E.Message);
	}

	if (NewRecUpdated) NewRecUpdated = PostZoneRec( ZoneRec );
	else NewRecUpdated = false;

	return NewRecUpdated;
}

//---------------------------------------------------------------------------
//
// Description:
// Delete the zone record identified by ZoneNo
// Returns true if delete was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesZone() in order to apply the changes.
//
bool	__fastcall TCIISDBModule::DeleteZoneRec( CIISZoneRec *ZoneRec )
{
 bool RecDeleted;

 if( LocateZoneRec( ZoneRec ))
  try
   {
	FDTableZones->Delete();
	RecDeleted = true;
   }
  catch (const Exception &E)
   {
	RecDeleted = false;
	Debug(Name + "::DeleteZoneRec() " + FDTableZones->TableName + ":" + E.Message);
   }
  return RecDeleted;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to zones table
//
// Comment: returns false if at least one pending update failed
//

bool	__fastcall TCIISDBModule::ApplyUpdatesZone(void)
{
 int32_t NoOfErrors=FDTableZones->ApplyUpdates();
 FDTableZones->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesZones() " + FDTableZones->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}

//		Sensor table

uint32_t __fastcall TCIISDBModule::GetSensorRecCount()
{
  return FDTableSensors->RecordCount;
}

void __fastcall TCIISDBModule::SetSensorFilter( CIISZoneRec *ZoneRec )
{
	FDTableSensors->Filter = "ZoneNo = " + IntToStr(ZoneRec->ZoneNo);
	FDTableSensors->Filtered = true;
}

void __fastcall TCIISDBModule::ClearSensorFilter()
{
	FDTableSensors->Filter = "";
	FDTableSensors->Filtered = false;
	FDTableSensors->Refresh();
}

bool __fastcall TCIISDBModule::FindFirstSensor()
{
  return FDTableSensors->FindFirst();
}

bool __fastcall TCIISDBModule::LocateSensorRec( CIISSensorRec *SensorRec )
{
    return FDTableSensors->Locate( "SensorSerialNo", SensorRec->SensorSerialNo, NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a sensor
//
// Comment: Note !! Moves to the sensor in db before return
// Returns true if next sensor is available.
// Returns false if end of sensors
//
bool __fastcall TCIISDBModule::GetSensorRec( CIISSensorRec *SensorRec, bool AutoInc )
{
  SensorRec->SensorSerialNo = FDTableSensors->FieldByName( "SensorSerialNo" )->AsInteger;
  SensorRec->SensorCanAdr = FDTableSensors->FieldByName( "SensorCanAdr" )->AsInteger;
	SensorRec->SensorStatus = FDTableSensors->FieldByName( "SensorStatus" )->AsInteger;
	SensorRec->SensorName = FDTableSensors->FieldByName( "SensorName" )->AsString;
	SensorRec->SensorType = FDTableSensors->FieldByName( "SensorType" )->AsInteger;
  SensorRec->SensorLastValue = FDTableSensors->FieldByName("SensorLastValue")->AsFloat;
	SensorRec->PreCommOff = FDTableSensors->FieldByName("PreCommOff")->AsFloat;
  SensorRec->PreCommLPR = FDTableSensors->FieldByName("PreCommLPR")->AsFloat;
  SensorRec->SensorArea = FDTableSensors->FieldByName("SensorArea")->AsFloat;
  SensorRec->SensorGain = FDTableSensors->FieldByName("SensorGain")->AsFloat;
  SensorRec->SensorOffset = FDTableSensors->FieldByName("SensorOffset")->AsFloat;
  SensorRec->SensorUnit = FDTableSensors->FieldByName( "SensorUnit" )->AsString;
  SensorRec->SensorLow = FDTableSensors->FieldByName("SensorLow")->AsFloat;
	SensorRec->SensorHigh = FDTableSensors->FieldByName("SensorHigh")->AsFloat;
	SensorRec->SensorAlarmEnabled = ReadBool(FDTableSensors,"SensorAlarmEnabled" );
	SensorRec->SensorAlarmStatus = FDTableSensors->FieldByName( "SensorAlarmStatus" )->AsInteger;
  SensorRec->SensorXPos = FDTableSensors->FieldByName("SensorXPos")->AsFloat;
  SensorRec->SensorYPos = FDTableSensors->FieldByName("SensorYPos")->AsFloat;
  SensorRec->SensorZPos = FDTableSensors->FieldByName("SensorZPos")->AsFloat;
  SensorRec->SensorSectionNo = FDTableSensors->FieldByName( "SensorSectionNo" )->AsInteger;
	SensorRec->RequestStatus = FDTableSensors->FieldByName( "RequestStatus" )->AsInteger;
	SensorRec->SensorIShunt = FDTableSensors->FieldByName("SensorIShunt")->AsFloat;
  SensorRec->SensorLPRStep = FDTableSensors->FieldByName( "SensorLPRStep" )->AsInteger;
  SensorRec->SensorConnected = ReadBool(FDTableSensors,"SensorConnected");
  SensorRec->SensorWarmUp = FDTableSensors->FieldByName( "SensorWarmUp" )->AsInteger;
  SensorRec->SensorTemp = ReadBool(FDTableSensors,"SensorTemp");
  SensorRec->SensorLastTemp = FDTableSensors->FieldByName("SensorLastTemp")->AsFloat;
  SensorRec->SensorVerMajor = FDTableSensors->FieldByName("SensorVerMajor")->AsInteger;
  SensorRec->SensorVerMinor = FDTableSensors->FieldByName("SensorVerMinor")->AsInteger;
  SensorRec->SensorIShunt1 = FDTableSensors->FieldByName("SensorIShunt1")->AsFloat;
  SensorRec->SensorIShunt2 = FDTableSensors->FieldByName("SensorIShunt2")->AsFloat;
  SensorRec->SensorIShunt3 = FDTableSensors->FieldByName("SensorIShunt3")->AsFloat;
  SensorRec->SensorLastValue2 = FDTableSensors->FieldByName("SensorLastValue2")->AsFloat;
	SensorRec->SensorGain2 = FDTableSensors->FieldByName("SensorGain2")->AsFloat;
	SensorRec->SensorOffset2 = FDTableSensors->FieldByName("SensorOffset2")->AsFloat;
	SensorRec->SensorUnit2 = FDTableSensors->FieldByName("SensorUnit2")->AsString;
	SensorRec->SensorLow2 = FDTableSensors->FieldByName("SensorLow2")->AsFloat;
	SensorRec->SensorHigh2 = FDTableSensors->FieldByName("SensorHigh2")->AsFloat;
	SensorRec->SensorAlarmStatus2 = FDTableSensors->FieldByName("SensorAlarmStatus2")->AsInteger;
	SensorRec->PowerShutdownEnabled = ReadBool(FDTableSensors,"PowerShutdownEnabled");
	SensorRec->SensorChannelCount = FDTableSensors->FieldByName("SensorChannelCount")->AsInteger;
	SensorRec->SensorName2 = FDTableSensors->FieldByName( "SensorName2" )->AsString;
	SensorRec->SensorName3 = FDTableSensors->FieldByName( "SensorName3" )->AsString;
	SensorRec->SensorName4 = FDTableSensors->FieldByName( "SensorName4" )->AsString;
	SensorRec->SensorGain3 = FDTableSensors->FieldByName("SensorGain3")->AsFloat;
	SensorRec->SensorOffset3 = FDTableSensors->FieldByName("SensorOffset3")->AsFloat;
	SensorRec->SensorUnit3 = FDTableSensors->FieldByName("SensorUnit3")->AsString;
	SensorRec->SensorLow3 = FDTableSensors->FieldByName("SensorLow3")->AsFloat;
	SensorRec->SensorHigh3 = FDTableSensors->FieldByName("SensorHigh3")->AsFloat;
	SensorRec->SensorAlarmStatus3 = FDTableSensors->FieldByName("SensorAlarmStatus3")->AsInteger;
	SensorRec->SensorGain4 = FDTableSensors->FieldByName("SensorGain4")->AsFloat;
	SensorRec->SensorOffset4 = FDTableSensors->FieldByName("SensorOffset4")->AsFloat;
	SensorRec->SensorUnit4 = FDTableSensors->FieldByName("SensorUnit4")->AsString;
	SensorRec->SensorLow4 = FDTableSensors->FieldByName("SensorLow4")->AsFloat;
	SensorRec->SensorHigh4 = FDTableSensors->FieldByName("SensorHigh4")->AsFloat;
	SensorRec->SensorAlarmStatus4 = FDTableSensors->FieldByName("SensorAlarmStatus4")->AsInteger;
	SensorRec->SensorLPRIRange = FDTableSensors->FieldByName("SensorLPRIRange")->AsInteger;
	SensorRec->VerNotSupported = ReadBool(FDTableSensors,"VerNotSupported");
	SensorRec->DisabledChannels = FDTableSensors->FieldByName( "DisabledChannels" )->AsInteger;
	SensorRec->PreCommOff2 = FDTableSensors->FieldByName("PreCommOff2")->AsFloat;
	SensorRec->PreCommOff3 = FDTableSensors->FieldByName("PreCommOff3")->AsFloat;
	SensorRec->PreCommOff4 = FDTableSensors->FieldByName("PreCommOff4")->AsFloat;
	SensorRec->CANAlarmEnabled = ReadBool(FDTableSensors,"CANAlarmEnabled" );
	SensorRec->CANAlarmStatus = FDTableSensors->FieldByName( "CANAlarmStatus" )->AsInteger;

  SensorRec->ZoneNo = FDTableSensors->FieldByName( "ZoneNo" )->AsInteger;

	if( AutoInc ) return FDTableSensors->FindNext();
	else return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a sensor and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesSensor() in order to apply the changes.
//
bool __fastcall TCIISDBModule::SetSensorRec( CIISSensorRec *SensorRec )
{
 FDTableSensors->Edit();

 return PostSensorRec(SensorRec);
}

//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a sensor and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesSensor() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostSensorRec( CIISSensorRec *SensorRec )
{

 FDTableSensors->FieldValues[ "SensorSerialNo"] = SensorRec->SensorSerialNo;
 FDTableSensors->FieldValues[ "SensorCanAdr"] = SensorRec->SensorCanAdr;
 FDTableSensors->FieldValues[ "SensorStatus"] = SensorRec->SensorStatus;
 FDTableSensors->FieldValues[ "SensorName"] = SensorRec->SensorName;
 FDTableSensors->FieldValues[ "SensorType"] = SensorRec->SensorType;
// FDTableSensors->FieldValues[ "SensorLastValue"] = SensorRec->SensorLastValue;
 FDTableSensors->FieldValues[ "PreCommOff"] = SensorRec->PreCommOff;
 FDTableSensors->FieldValues[ "PreCommLPR"] = SensorRec->PreCommLPR;
 FDTableSensors->FieldValues[ "SensorArea"] = SensorRec->SensorArea;
 FDTableSensors->FieldValues[ "SensorGain"] = SensorRec->SensorGain;
 FDTableSensors->FieldValues[ "SensorOffset"] = SensorRec->SensorOffset;
 FDTableSensors->FieldValues[ "SensorUnit"] = SensorRec->SensorUnit;
 FDTableSensors->FieldValues[ "SensorLow"] = SensorRec->SensorLow;
 FDTableSensors->FieldValues[ "SensorHigh"] = SensorRec->SensorHigh;
 WriteBool(FDTableSensors,"SensorAlarmEnabled",SensorRec->SensorAlarmEnabled);
 FDTableSensors->FieldValues[ "SensorAlarmStatus"] = SensorRec->SensorAlarmStatus;
 FDTableSensors->FieldValues[ "SensorXPos"] = SensorRec->SensorXPos;
 FDTableSensors->FieldValues[ "SensorYPos"] = SensorRec->SensorYPos;
 FDTableSensors->FieldValues[ "SensorZPos"] = SensorRec->SensorZPos;
 FDTableSensors->FieldValues[ "SensorSectionNo"] = SensorRec->SensorSectionNo;
 FDTableSensors->FieldValues[ "RequestStatus"] = SensorRec->RequestStatus;
 FDTableSensors->FieldValues[ "SensorIShunt"] = SensorRec->SensorIShunt;
 FDTableSensors->FieldValues[ "SensorLPRStep"] = SensorRec->SensorLPRStep;
 WriteBool(FDTableSensors,"SensorConnected",SensorRec->SensorConnected);
 FDTableSensors->FieldValues[ "SensorWarmUp"] = SensorRec->SensorWarmUp;
 WriteBool(FDTableSensors,"SensorTemp",SensorRec->SensorTemp);
 FDTableSensors->FieldValues[ "SensorLastTemp"] = SensorRec->SensorLastTemp;
 FDTableSensors->FieldValues[ "SensorVerMajor"] = SensorRec->SensorVerMajor;
 FDTableSensors->FieldValues[ "SensorVerMinor"] = SensorRec->SensorVerMinor;
 FDTableSensors->FieldValues[ "SensorIShunt1"] = SensorRec->SensorIShunt1;
 FDTableSensors->FieldValues[ "SensorIShunt2"] = SensorRec->SensorIShunt2;
 FDTableSensors->FieldValues[ "SensorIShunt3"] = SensorRec->SensorIShunt3;
// FDTableSensors->FieldValues[ "SensorLastValue2"] = SensorRec->SensorLastValue2;
 FDTableSensors->FieldValues[ "SensorGain2"] = SensorRec->SensorGain2;
 FDTableSensors->FieldValues[ "SensorOffset2"] = SensorRec->SensorOffset2;
 FDTableSensors->FieldValues[ "SensorUnit2"] = SensorRec->SensorUnit2;
 FDTableSensors->FieldValues[ "SensorLow2"] = SensorRec->SensorLow2;
 FDTableSensors->FieldValues[ "SensorHigh2"] = SensorRec->SensorHigh2;
 FDTableSensors->FieldValues[ "SensorAlarmStatus2"] = SensorRec->SensorAlarmStatus2;
 WriteBool(FDTableSensors,"PowerShutdownEnabled",SensorRec->PowerShutdownEnabled);
 FDTableSensors->FieldValues[ "SensorChannelCount"] = SensorRec->SensorChannelCount;
 FDTableSensors->FieldValues[ "SensorName2"] = SensorRec->SensorName2;
 FDTableSensors->FieldValues[ "SensorName3"] = SensorRec->SensorName3;
 FDTableSensors->FieldValues[ "SensorName4"] = SensorRec->SensorName4;
 FDTableSensors->FieldValues[ "SensorGain3"] = SensorRec->SensorGain3;
 FDTableSensors->FieldValues[ "SensorOffset3"] = SensorRec->SensorOffset3;
 FDTableSensors->FieldValues[ "SensorUnit3"] = SensorRec->SensorUnit3;
 FDTableSensors->FieldValues[ "SensorLow3"] = SensorRec->SensorLow3;
 FDTableSensors->FieldValues[ "SensorHigh3"] = SensorRec->SensorHigh3;
 FDTableSensors->FieldValues[ "SensorAlarmStatus3"] = SensorRec->SensorAlarmStatus3;
 FDTableSensors->FieldValues[ "SensorGain4"] = SensorRec->SensorGain4;
 FDTableSensors->FieldValues[ "SensorOffset4"] = SensorRec->SensorOffset4;
 FDTableSensors->FieldValues[ "SensorUnit4"] = SensorRec->SensorUnit4;
 FDTableSensors->FieldValues[ "SensorLow4"] = SensorRec->SensorLow4;
 FDTableSensors->FieldValues[ "SensorHigh4"] = SensorRec->SensorHigh4;
 FDTableSensors->FieldValues[ "SensorAlarmStatus4"] = SensorRec->SensorAlarmStatus4;
 FDTableSensors->FieldValues[ "SensorLPRIRange"] = SensorRec->SensorLPRIRange;
 WriteBool(FDTableSensors,"VerNotSupported",SensorRec->VerNotSupported);
 FDTableSensors->FieldValues[ "DisabledChannels"] = SensorRec->DisabledChannels;
 FDTableSensors->FieldValues[ "PreCommOff2"] = SensorRec->PreCommOff2;
 FDTableSensors->FieldValues[ "PreCommOff3"] = SensorRec->PreCommOff3;
 FDTableSensors->FieldValues[ "PreCommOff4"] = SensorRec->PreCommOff4;
 WriteBool(FDTableSensors,"CANAlarmEnabled",SensorRec->CANAlarmEnabled);
 FDTableSensors->FieldValues[ "CANAlarmStatus"] = SensorRec->CANAlarmStatus;

 FDTableSensors->FieldValues[ "ZoneNo"] = SensorRec->ZoneNo;
 bool PostOk;
 try
  {
   FDTableSensors->Post();
   PostOk=true;
  }
 catch (const Exception &E)
  {
   PostOk = false;
   Debug(Name + "::PostSensorRec() " + FDTableSensors->TableName + ":" + E.Message);
   FDTableSensors->Cancel();
  }

 return PostOk;
}

//---------------------------------------------------------------------------
//
// Description:
// Append a new sensor record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesSensor() in order to apply the changes.
//
bool __fastcall TCIISDBModule::AppendSensorRec( CIISSensorRec *SensorRec )
{
 FDTableSensors->Append();
 return PostSensorRec( SensorRec );
}

//---------------------------------------------------------------------------
//
// Description:
// Delete a record in the sensor table
//
// Comment: returns true if record found and deleted
// Note !! Call ApplyUpdatesPS() in order to remove the record permanently
// from the SQL database
//
bool	__fastcall TCIISDBModule::DeleteSensorRec( CIISSensorRec *SensorRec )
{
 bool RecDeleted;

 if( LocateSensorRec( SensorRec ))
  try
   {
	FDTableSensors->Delete();
	RecDeleted = true;
   }
  catch (const Exception &E)
   {
	RecDeleted = false;
	Debug(Name + "::DeleteSensorRec() " + FDTableSensors->TableName + ":" + E.Message);
   }


 return RecDeleted;
}

//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to sensor table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesSensor(void)
{
 int32_t NoOfErrors=FDTableSensors->ApplyUpdates();
 FDTableSensors->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesSensor() " + FDTableSensors->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}




//		Alarm table

uint32_t __fastcall TCIISDBModule::GetAlarmRecCount()
{
 return FDTableAlarm->RecordCount;
}

void __fastcall TCIISDBModule::SetAlarmFilter( CIISZoneRec *ZoneRec )
{
	FDTableAlarm->Filter = "ZoneNo = " + IntToStr(ZoneRec->ZoneNo);
	FDTableAlarm->Filtered = true;
}

void __fastcall TCIISDBModule::ClearAlarmFilter()
{
	FDTableAlarm->Filter = "";
	FDTableAlarm->Filtered = false;
	FDTableAlarm->Refresh();
}

bool __fastcall TCIISDBModule::FindFirstAlarm()
{
 return FDTableAlarm->FindFirst();
}

bool __fastcall TCIISDBModule::LocateAlarmRec( CIISAlarmRec *AlarmRec )
{
 return FDTableAlarm->Locate("AlarmSerialNo", AlarmRec->AlarmSerialNo, NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a Alarm
//
// Comment:
// Note! AutoInc
// Returns true if the next Alarm record exists
// Returns false if the no more Alarm records are available
//

bool __fastcall TCIISDBModule::GetAlarmRec( CIISAlarmRec *AlarmRec, bool AutoInc )
{
 AlarmRec->AlarmSerialNo = FDTableAlarm->FieldByName("AlarmSerialNo")->AsInteger;
 AlarmRec->AlarmCanAdr = FDTableAlarm->FieldByName("AlarmCanAdr")->AsInteger;
 AlarmRec->AlarmStatus = FDTableAlarm->FieldByName("AlarmStatus")->AsInteger;
 AlarmRec->AlarmName = FDTableAlarm->FieldByName("AlarmName")->AsString;
 AlarmRec->AlarmType = FDTableAlarm->FieldByName("AlarmType")->AsInteger;
 AlarmRec->RequestStatus = FDTableAlarm->FieldByName("RequestStatus")->AsInteger;
 AlarmRec->AlarmConnected = ReadBool(FDTableAlarm,"AlarmConnected");
 AlarmRec->AlarmVerMajor = FDTableAlarm->FieldByName("AlarmVerMajor")->AsInteger;
 AlarmRec->AlarmVerMinor = FDTableAlarm->FieldByName("AlarmVerMinor")->AsInteger;
 AlarmRec->DO1 = ReadBool(FDTableAlarm,"DO1");
 AlarmRec->DO2 = ReadBool(FDTableAlarm,"DO2");
 AlarmRec->DO3 = ReadBool(FDTableAlarm,"DO3");
 AlarmRec->DO4 = ReadBool(FDTableAlarm,"DO4");
 AlarmRec->DO1AlarmType = FDTableAlarm->FieldByName("DO1AlarmType")->AsInteger;
 AlarmRec->DO2AlarmType = FDTableAlarm->FieldByName("DO2AlarmType")->AsInteger;
 AlarmRec->DO3AlarmType = FDTableAlarm->FieldByName("DO3AlarmType")->AsInteger;
 AlarmRec->DO4AlarmType = FDTableAlarm->FieldByName("DO4AlarmType")->AsInteger;
 AlarmRec->InvDO1 = ReadBool(FDTableAlarm,"InvDO1");
 AlarmRec->InvDO2 = ReadBool(FDTableAlarm,"InvDO2");
 AlarmRec->InvDO3 = ReadBool(FDTableAlarm,"InvDO3");
 AlarmRec->InvDO4 = ReadBool(FDTableAlarm,"InvDO4");
 AlarmRec->VerNotSupported = ReadBool(FDTableAlarm, "VerNotSupported" );
 AlarmRec->CANAlarmEnabled = ReadBool(FDTableAlarm,"CANAlarmEnabled");
 AlarmRec->CANAlarmStatus = FDTableAlarm->FieldByName("CANAlarmStatus")->AsInteger;
 AlarmRec->ZoneNo = FDTableAlarm->FieldByName("ZoneNo")->AsInteger;

 if( AutoInc )
	return FDTableAlarm->FindNext();
 else
	return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a Alarm and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesAlarm() in order to apply the changes.
//

bool __fastcall TCIISDBModule::SetAlarmRec( CIISAlarmRec *AlarmRec)
{
 FDTableAlarm->Edit();
 return PostAlarmRec(AlarmRec);
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a Alarm and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesAlarm() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostAlarmRec( CIISAlarmRec *AlarmRec)
{
 FDTableAlarm->FieldValues["AlarmSerialNo"] = AlarmRec->AlarmSerialNo ;
 FDTableAlarm->FieldValues["AlarmCanAdr"] = AlarmRec->AlarmCanAdr ;
 FDTableAlarm->FieldValues["AlarmStatus"] = AlarmRec->AlarmStatus ;
 FDTableAlarm->FieldValues["AlarmName"] = AlarmRec->AlarmName ;
 FDTableAlarm->FieldValues["AlarmType"] = AlarmRec->AlarmType ;
 FDTableAlarm->FieldValues["RequestStatus"] = AlarmRec->RequestStatus ;
 WriteBool(FDTableAlarm,"AlarmConnected",AlarmRec->AlarmConnected);
 FDTableAlarm->FieldValues["AlarmVerMajor"] = AlarmRec->AlarmVerMajor ;
 FDTableAlarm->FieldValues["AlarmVerMinor"] = AlarmRec->AlarmVerMinor ;

 WriteBool(FDTableAlarm,"DO1",AlarmRec->DO1);
 WriteBool(FDTableAlarm,"DO2",AlarmRec->DO2);
 WriteBool(FDTableAlarm,"DO3",AlarmRec->DO3);
 WriteBool(FDTableAlarm,"DO4",AlarmRec->DO4);

 FDTableAlarm->FieldValues["DO1AlarmType"] = AlarmRec->DO1AlarmType ;
 FDTableAlarm->FieldValues["DO2AlarmType"] = AlarmRec->DO2AlarmType ;
 FDTableAlarm->FieldValues["DO3AlarmType"] = AlarmRec->DO3AlarmType ;
 FDTableAlarm->FieldValues["DO4AlarmType"] = AlarmRec->DO4AlarmType ;

 WriteBool(FDTableAlarm,"InvDO1",AlarmRec->InvDO1);
 WriteBool(FDTableAlarm,"InvDO2",AlarmRec->InvDO2);
 WriteBool(FDTableAlarm,"InvDO3",AlarmRec->InvDO3);
 WriteBool(FDTableAlarm,"InvDO4",AlarmRec->InvDO4);

 WriteBool(FDTableAlarm,"VerNotSupported",AlarmRec->VerNotSupported);

 WriteBool(FDTableAlarm,"CANAlarmEnabled",AlarmRec->CANAlarmEnabled);
 FDTableAlarm->FieldValues["CANAlarmStatus"] = AlarmRec->CANAlarmStatus ;

 FDTableAlarm->FieldValues["ZoneNo"] = AlarmRec->ZoneNo;
 bool RecUpdated= false;
 try
	{
	 FDTableAlarm->Post();
	 RecUpdated= true;
	}
 catch (const Exception &E)
	{
	 RecUpdated = false;
	 Debug(Name + "::PostAlarmRec() " + FDTableAlarm->TableName + ":" + E.Message);
	 FDTableAlarm->Cancel();
	}
 return RecUpdated;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new Alarm record and edit the fields and post the changes
//
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesAlarm() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendAlarmRec( CIISAlarmRec *AlarmRec )
{
 FDTableAlarm->Append();
 return PostAlarmRec( AlarmRec );
}

//---------------------------------------------------------------------------
//
// Description:
// Delete the Alarm record identified by AlarmNo
// Returns true if delete was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesAlarm() in order to apply the changes.
//
bool	__fastcall TCIISDBModule::DeleteAlarmRec( CIISAlarmRec *AlarmRec )
{
 bool RecDeleted;

 if( LocateAlarmRec( AlarmRec ))
	try
	 {
	FDTableAlarm->Delete();
	RecDeleted = true;
	 }
	catch (const Exception &E)
	 {
	RecDeleted = false;
	Debug(Name + "::DeleteAlarmRec() " + FDTableAlarm->TableName + ":" + E.Message);
	 }
	return RecDeleted;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to Alarms table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesAlarm(void)
{
 int32_t NoOfErrors=FDTableAlarm->ApplyUpdates();
 FDTableAlarm->CommitUpdates();

 if (NoOfErrors)
	{
	 Debug(Name + "::ApplyUpdatesAlarm() " + FDTableAlarm->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
	 return false;
	}
 else
	return true;
}



//		BIChannels table

uint32_t __fastcall TCIISDBModule::GetBIChannelsRecCount()
{
	return FDTableBIChannels->RecordCount;
}

bool __fastcall TCIISDBModule::FindFirstBIChannel()
{
	return FDTableBIChannels->FindFirst();
}

bool __fastcall TCIISDBModule::LocateBIChannelRec( CIISBIChannelRec *BIChannelRec )
{
	return FDTableBIChannels->Locate("SerialNo", BIChannelRec->SerialNo, NoOpts );
}

bool __fastcall TCIISDBModule::GetBIChannelRec( CIISBIChannelRec *BIChannelRec, bool AutoInc )
{
	BIChannelRec->SerialNo = FDTableBIChannels->FieldByName("SerialNo")->AsInteger;
	BIChannelRec->Type = FDTableBIChannels->FieldByName("Type")->AsInteger;
	BIChannelRec->Name = FDTableBIChannels->FieldByName("Name")->AsString;
	BIChannelRec->Included = ReadBool(FDTableBIChannels,"Included");
	BIChannelRec->Active = ReadBool(FDTableBIChannels,"Active");
	BIChannelRec->WatchDog = ReadBool(FDTableBIChannels,"WatchDog");
	BIChannelRec->WDTime = FDTableBIChannels->FieldByName("WDTime")->AsInteger;
	BIChannelRec->Mode = FDTableBIChannels->FieldByName("Mode")->AsInteger;
	BIChannelRec->IP = FDTableBIChannels->FieldByName("IP")->AsString;
	BIChannelRec->StaticIP = FDTableBIChannels->FieldByName("StaticIP")->AsString;
	BIChannelRec->Gateway = FDTableBIChannels->FieldByName("Gateway")->AsString;
	BIChannelRec->Netmask = FDTableBIChannels->FieldByName("Netmask")->AsString;
	BIChannelRec->MACAddress = FDTableBIChannels->FieldByName("MACAddress")->AsString;
	BIChannelRec->VerMajor = FDTableBIChannels->FieldByName("VerMajor")->AsInteger;
	BIChannelRec->VerMinor = FDTableBIChannels->FieldByName("VerMinor")->AsInteger;
	BIChannelRec->VerNotSupported = ReadBool(FDTableBIChannels,"VerNotSupported");

	if( AutoInc )
		return FDTableBIChannels->FindNext();
	else
		return true;
}

bool __fastcall TCIISDBModule::SetBIChannelRec( CIISBIChannelRec *BIChannelRec)
{
	FDTableBIChannels->Edit();
	return PostBIChannelRec(BIChannelRec);
}

bool __fastcall TCIISDBModule::PostBIChannelRec( CIISBIChannelRec *BIChannelRec)
{
	FDTableBIChannels->FieldValues["SerialNo"] = BIChannelRec->SerialNo ;
	FDTableBIChannels->FieldValues["Type"] = BIChannelRec->Type ;
	FDTableBIChannels->FieldValues["Name"] = BIChannelRec->Name ;
	WriteBool(FDTableBIChannels,"Included",BIChannelRec->Included);
	WriteBool(FDTableBIChannels,"Active",BIChannelRec->Active);
	WriteBool(FDTableBIChannels,"WatchDog",BIChannelRec->WatchDog);
	FDTableBIChannels->FieldValues["WDTime"] = BIChannelRec->WDTime ;
	FDTableBIChannels->FieldValues["Mode"] = BIChannelRec->Mode;
	FDTableBIChannels->FieldValues["IP"] = BIChannelRec->IP;
	FDTableBIChannels->FieldValues["StaticIP"] = BIChannelRec->StaticIP;
	FDTableBIChannels->FieldValues["Gateway"] = BIChannelRec->Gateway;
	FDTableBIChannels->FieldValues["Netmask"] = BIChannelRec->Netmask;
	FDTableBIChannels->FieldValues["MACAddress"] = BIChannelRec->MACAddress;
	FDTableBIChannels->FieldValues["VerMajor"] = BIChannelRec->VerMajor;
	FDTableBIChannels->FieldValues["VerMinor"] = BIChannelRec->VerMinor;
	WriteBool(FDTableBIChannels,"VerNotSupported",BIChannelRec->VerNotSupported);
	bool RecUpdated= false;
	try
	{
		FDTableBIChannels->Post();
		RecUpdated= true;
	}
	catch (const Exception &E)
	{
		RecUpdated = false;
		Debug(Name + "::PostBIChannelsRec() " + FDTableBIChannels->TableName + ":" + E.Message);
		FDTableBIChannels->Cancel();
	}
	return RecUpdated;
}

bool __fastcall TCIISDBModule::AppendBIChannelRec( CIISBIChannelRec *BIChannelRec )
{
	FDTableBIChannels->Append();
	return PostBIChannelRec( BIChannelRec );
}

bool	__fastcall TCIISDBModule::DeleteBIChannelRec( CIISBIChannelRec *BIChannelRec )
{
	bool RecDeleted;

	if( LocateBIChannelRec( BIChannelRec ))
	try
	 {
			FDTableBIChannels->Delete();
			RecDeleted = true;
	 }
	catch (const Exception &E)
	 {
			RecDeleted = false;
			Debug(Name + "::DeleteBIChannelsRec() " + FDTableBIChannels->TableName + ":" + E.Message);
	 }
	return RecDeleted;
}

bool	__fastcall TCIISDBModule::ApplyUpdatesBIChannels(void)
{
	int32_t NoOfErrors = FDTableBIChannels->ApplyUpdates();
	FDTableBIChannels->CommitUpdates();

	if (NoOfErrors)
	{
		Debug(Name + "::ApplyUpdatesBIChannels() " + FDTableBIChannels->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
		return false;
	}
	else
		return true;
}




//		PowerSupply table
//---------------------------------------------------------------------------
//
// Description:
// Get no of power supplies in database
//
// Comment:
//
uint32_t	__fastcall TCIISDBModule::GetPSRecCount()
{
  return FDTablePowerSupply->RecordCount;
}
//---------------------------------------------------------------------------
//
// Description:
// Set power supply filter (belongs to zone)
//
// Comment:
//
void __fastcall TCIISDBModule::SetPSFilter( CIISZoneRec *ZoneRec )
{
  FDTablePowerSupply->Filter = "ZoneNo = " + IntToStr(ZoneRec->ZoneNo);
  FDTablePowerSupply->Filtered = true;
}
//---------------------------------------------------------------------------
//
// Description:
// Clear power supply filter
//
// Comment:
//
void __fastcall TCIISDBModule::ClearPSFilter()
{
  FDTablePowerSupply->Filter = "";
  FDTablePowerSupply->Filtered = false;
  FDTablePowerSupply->Refresh();
}

//---------------------------------------------------------------------------
//
// Description:
// Find first power supply in database
//
// Comment:
//
bool __fastcall TCIISDBModule::FindFirstPS()
{
 return FDTablePowerSupply->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate a power supply(with a given SerialNo) in database
//
// Comment:
//
bool __fastcall TCIISDBModule::LocatePSRec( CIISPowerSupplyRec *PSRec )
{
 return FDTablePowerSupply->Locate( "PSSerialNo", PSRec->PSSerialNo, NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a power supply
//
// Comment: Note !! Moves to the next power supply in db before return
// Returns true if next power supply available.
// Returns false if end of power supplies
//
bool __fastcall TCIISDBModule::GetPSRec( CIISPowerSupplyRec *PSRec, bool AutoInc )
{
  PSRec->PSSerialNo = FDTablePowerSupply->FieldByName( "PSSerialNo" )->AsInteger;
  PSRec->PSCanAdr = FDTablePowerSupply->FieldByName( "PSCanAdr" )->AsInteger;
  PSRec->PSStatus = FDTablePowerSupply->FieldByName( "PSStatus" )->AsInteger;
  PSRec->PSName = FDTablePowerSupply->FieldByName( "PSName" )->AsString;
  PSRec->PSSerialID = FDTablePowerSupply->FieldByName( "PSSerialID" )->AsString;
  PSRec->PSType = FDTablePowerSupply->FieldByName( "PSType" )->AsInteger;
  PSRec->PSLastValueU = FDTablePowerSupply->FieldByName("PSLastValueU")->AsFloat;
  PSRec->PSLastValueI = FDTablePowerSupply->FieldByName("PSLastValueI")->AsFloat;
  PSRec->PSLastValueCh3 = FDTablePowerSupply->FieldByName("PSLastValueCh3")->AsFloat;
  PSRec->PSLastValueCh4 = FDTablePowerSupply->FieldByName("PSLastValueCh4")->AsFloat;
  PSRec->PSMode = FDTablePowerSupply->FieldByName( "PSMode" )->AsInteger;
  PSRec->PSSetVoltage = FDTablePowerSupply->FieldByName("PSSetVoltage")->AsFloat;
  PSRec->PSSetCurrent = FDTablePowerSupply->FieldByName("PSSetCurrent")->AsFloat;
  PSRec->PSOutGain = FDTablePowerSupply->FieldByName("PSOutGain")->AsFloat;
  PSRec->PSOutOffset = FDTablePowerSupply->FieldByName("PSOutOffset")->AsFloat;
  PSRec->PSLowU = FDTablePowerSupply->FieldByName("PSLowU")->AsFloat;
  PSRec->PSLowI = FDTablePowerSupply->FieldByName("PSLowI")->AsFloat;
  PSRec->PSHighU = FDTablePowerSupply->FieldByName("PSHighU")->AsFloat;
  PSRec->PSHighI = FDTablePowerSupply->FieldByName("PSHighI")->AsFloat;
	PSRec->PSAlarmEnabled = ReadBool(FDTablePowerSupply,"PSAlarmEnabled");
	PSRec->PSAlarmStatusU = FDTablePowerSupply->FieldByName( "PSAlarmStatusU" )->AsInteger;
  PSRec->PSAlarmStatusI = FDTablePowerSupply->FieldByName( "PSAlarmStatusI" )->AsInteger;
  PSRec->PSVOutEnabled = ReadBool(FDTablePowerSupply,"PSVOutEnabled");
  PSRec->PSRemote = ReadBool(FDTablePowerSupply,"PSRemote" );
  PSRec->RequestStatus = FDTablePowerSupply->FieldByName( "RequestStatus" )->AsInteger;
  PSRec->PSConnected = ReadBool(FDTablePowerSupply,"PSConnected" );
  PSRec->Ch1Gain = FDTablePowerSupply->FieldByName("Ch1Gain")->AsFloat;
  PSRec->Ch1Offset = FDTablePowerSupply->FieldByName("Ch1Offset")->AsFloat;
  PSRec->Ch1Unit = FDTablePowerSupply->FieldByName( "Ch1Unit" )->AsString;
  PSRec->Ch2Gain = FDTablePowerSupply->FieldByName("Ch2Gain")->AsFloat;
  PSRec->Ch2Offset = FDTablePowerSupply->FieldByName("Ch2Offset")->AsFloat;
  PSRec->Ch2Unit = FDTablePowerSupply->FieldByName( "Ch2Unit" )->AsString;
  PSRec->Ch3Gain = FDTablePowerSupply->FieldByName("Ch3Gain")->AsFloat;
  PSRec->Ch3Offset = FDTablePowerSupply->FieldByName("Ch3Offset")->AsFloat;
  PSRec->Ch3Unit = FDTablePowerSupply->FieldByName( "Ch3Unit" )->AsString;
  PSRec->Ch4Gain = FDTablePowerSupply->FieldByName("Ch4Gain")->AsFloat;
  PSRec->Ch4Offset = FDTablePowerSupply->FieldByName("Ch4Offset")->AsFloat;
  PSRec->Ch4Unit = FDTablePowerSupply->FieldByName( "Ch4Unit" )->AsString;
  PSRec->Ch1BitVal = FDTablePowerSupply->FieldByName("Ch1BitVal")->AsFloat;
  PSRec->Ch2BitVal = FDTablePowerSupply->FieldByName("Ch2BitVal")->AsFloat;
  PSRec->Ch3BitVal = FDTablePowerSupply->FieldByName("Ch3BitVal")->AsFloat;
  PSRec->Ch4BitVal = FDTablePowerSupply->FieldByName("Ch4BitVal")->AsFloat;
	PSRec->PSVerMajor = FDTablePowerSupply->FieldByName( "PSVerMajor" )->AsInteger;
	PSRec->PSVerMinor = FDTablePowerSupply->FieldByName( "PSVerMinor" )->AsInteger;
	PSRec->SenseGuardEnabled = ReadBool(FDTablePowerSupply,"SenseGuardEnabled" );
	PSRec->Fallback = ReadBool(FDTablePowerSupply,"Fallback" );
	PSRec->PSTemp = ReadBool(FDTablePowerSupply,"PSTemp" );
	PSRec->VerNotSupported = ReadBool(FDTablePowerSupply,"VerNotSupported" );
	PSRec->DisabledChannels = FDTablePowerSupply->FieldByName( "DisabledChannels" )->AsInteger;
	PSRec->InvDO1 = ReadBool(FDTablePowerSupply,"InvDO1" );
	PSRec->InvDO2 = ReadBool(FDTablePowerSupply,"InvDO2" );
	PSRec->CANAlarmEnabled = ReadBool(FDTablePowerSupply,"CANAlarmEnabled");
	PSRec->CANAlarmStatus = FDTablePowerSupply->FieldByName( "CANAlarmStatus" )->AsInteger;
	PSRec->ZoneNo = FDTablePowerSupply->FieldByName( "ZoneNo" )->AsInteger;

  if( AutoInc ) return FDTablePowerSupply->FindNext();
  else return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a power supply and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesPS() in order to apply the changes.
//
bool __fastcall TCIISDBModule::SetPSRec( CIISPowerSupplyRec *PSRec )
{
  FDTablePowerSupply->Edit();
  return PostPSRec(PSRec);
}
//---------------------------------------------------------------------------
//
// Description:
// Fill the fields of a power supply and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Note !! It is necessary to set the table in proper state (Edit or Append)
// before calling this function.
//
// Call ApplyUpdatesPS() in order to apply the changes.
//
bool __fastcall TCIISDBModule::PostPSRec( CIISPowerSupplyRec *PSRec )
{
 FDTablePowerSupply->FieldValues["PSSerialNo"] = PSRec->PSSerialNo;
 FDTablePowerSupply->FieldValues["PSCanAdr"] = PSRec->PSCanAdr;
 FDTablePowerSupply->FieldValues["PSStatus"] = PSRec->PSStatus;
 FDTablePowerSupply->FieldValues["PSName"] = PSRec->PSName;
 FDTablePowerSupply->FieldValues["PSSerialID"] = PSRec->PSSerialID;
 FDTablePowerSupply->FieldValues["PSType"] = PSRec->PSType;
 FDTablePowerSupply->FieldValues["PSLastValueU"] = PSRec->PSLastValueU;
 FDTablePowerSupply->FieldValues["PSLastValueI"] = PSRec->PSLastValueI;
 FDTablePowerSupply->FieldValues["PSMode"] = PSRec->PSMode;
 FDTablePowerSupply->FieldValues["PSSetVoltage"] = PSRec->PSSetVoltage;
 FDTablePowerSupply->FieldValues["PSSetCurrent"] = PSRec->PSSetCurrent;
 FDTablePowerSupply->FieldValues["PSOutGain"] = PSRec->PSOutGain;
 FDTablePowerSupply->FieldValues["PSOutOffset"] = PSRec->PSOutOffset;
 FDTablePowerSupply->FieldValues["PSLowU"] = PSRec->PSLowU;
 FDTablePowerSupply->FieldValues["PSLowI"] = PSRec->PSLowI;
 FDTablePowerSupply->FieldValues["PSHighU"] = PSRec->PSHighU;
 FDTablePowerSupply->FieldValues["PSHighI"] = PSRec->PSHighI;
 WriteBool(FDTablePowerSupply,"PSAlarmEnabled",PSRec->PSAlarmEnabled);
 FDTablePowerSupply->FieldValues["PSAlarmStatusU"] = PSRec->PSAlarmStatusU;
 FDTablePowerSupply->FieldValues["PSAlarmStatusI"] = PSRec->PSAlarmStatusI;
 WriteBool(FDTablePowerSupply,"PSVOutEnabled",PSRec->PSVOutEnabled);
 WriteBool(FDTablePowerSupply,"PSRemote",PSRec->PSRemote);
 FDTablePowerSupply->FieldValues["RequestStatus"] = PSRec->RequestStatus;
 WriteBool(FDTablePowerSupply,"PSConnected",PSRec->PSConnected);
 FDTablePowerSupply->FieldValues["Ch1Gain"] = PSRec->Ch1Gain;
 FDTablePowerSupply->FieldValues["Ch1Offset"] = PSRec->Ch1Offset;
 FDTablePowerSupply->FieldValues["Ch1Unit"] = PSRec->Ch1Unit;
 FDTablePowerSupply->FieldValues["Ch2Gain"] = PSRec->Ch2Gain;
 FDTablePowerSupply->FieldValues["Ch2Offset"] = PSRec->Ch2Offset;
 FDTablePowerSupply->FieldValues["Ch2Unit"] = PSRec->Ch2Unit;
 FDTablePowerSupply->FieldValues["Ch3Gain"] = PSRec->Ch3Gain;
 FDTablePowerSupply->FieldValues["Ch3Offset"] = PSRec->Ch3Offset;
 FDTablePowerSupply->FieldValues["Ch3Unit"] = PSRec->Ch3Unit;
 FDTablePowerSupply->FieldValues["Ch4Gain"] = PSRec->Ch4Gain;
 FDTablePowerSupply->FieldValues["Ch4Offset"] = PSRec->Ch4Offset;
 FDTablePowerSupply->FieldValues["Ch4Unit"] = PSRec->Ch4Unit;
 FDTablePowerSupply->FieldValues["Ch1BitVal"] = PSRec->Ch1BitVal;
 FDTablePowerSupply->FieldValues["Ch2BitVal"] = PSRec->Ch2BitVal;
 FDTablePowerSupply->FieldValues["Ch3BitVal"] = PSRec->Ch3BitVal;
 FDTablePowerSupply->FieldValues["Ch4BitVal"] = PSRec->Ch4BitVal;
 FDTablePowerSupply->FieldValues["PSLastValueCh3"] = PSRec->PSLastValueCh3;
 FDTablePowerSupply->FieldValues["PSLastValueCh4"] = PSRec->PSLastValueCh4;
 FDTablePowerSupply->FieldValues["PSVerMajor"] = PSRec->PSVerMajor;
 FDTablePowerSupply->FieldValues["PSVerMinor"] = PSRec->PSVerMinor;
 WriteBool(FDTablePowerSupply,"SenseGuardEnabled",PSRec->SenseGuardEnabled);
 WriteBool(FDTablePowerSupply,"Fallback",PSRec->Fallback);
 WriteBool(FDTablePowerSupply,"PSTemp",PSRec->PSTemp);
 WriteBool(FDTablePowerSupply,"VerNotSupported",PSRec->VerNotSupported);
 FDTablePowerSupply->FieldValues["PSVerMajor"] = PSRec->PSVerMajor;
 FDTablePowerSupply->FieldValues["DisabledChannels"] = PSRec->DisabledChannels;
 WriteBool(FDTablePowerSupply,"InvDO1",PSRec->InvDO1);
 WriteBool(FDTablePowerSupply,"InvDO2",PSRec->InvDO2);
 WriteBool(FDTablePowerSupply,"CANAlarmEnabled",PSRec->CANAlarmEnabled);
 FDTablePowerSupply->FieldValues["CANAlarmStatus"] = PSRec->CANAlarmStatus;

 FDTablePowerSupply->FieldValues["ZoneNo"] = PSRec->ZoneNo;
 bool PostOk=false;
 try
	{
   FDTablePowerSupply->Post();
   PostOk=true;
  }
 catch (const Exception &E)
  {
   PostOk = false;
   Debug(Name + "::PostPSRec() " + FDTablePowerSupply->TableName + ":" + E.Message);
   FDTablePowerSupply->Cancel();
  }
 return PostOk;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new power supply record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesPS() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendPSRec( CIISPowerSupplyRec *PSRec )
{
 bool NewRecUpdated;

 try
  {
   FDTablePowerSupply->Append();
   NewRecUpdated = PostPSRec( PSRec );
  }
 catch (const Exception &E)
  {
   NewRecUpdated = false;
   Debug(Name + "::AppendPSRec() " + FDTablePowerSupply->TableName + ":" + E.Message);
  }
 return NewRecUpdated;
}

//---------------------------------------------------------------------------
//
// Description:
// Delete a record in the power supply table
//
// Comment: returns true if record found and deleted
// Note !! Call ApplyUpdatesPS() in order to remove the record permanently
// from the SQL database
//
bool	__fastcall TCIISDBModule::DeletePSRec( CIISPowerSupplyRec *PSRec )
{
 bool RecDeleted;

 if( LocatePSRec( PSRec ))
  try
   {
	FDTablePowerSupply->Delete();
	RecDeleted = true;
   }
  catch (const Exception &E)
   {
	RecDeleted = false;
	Debug(Name + "::DeletePSRec() " + FDTablePowerSupply->TableName + ":" + E.Message);
   }
  return RecDeleted;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to power supply table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesPS(void)
{
 int32_t NoOfErrors=FDTablePowerSupply->ApplyUpdates();
 FDTablePowerSupply->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesPS() " + FDTablePowerSupply->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}

//		Eventlog table

//---------------------------------------------------------------------------
//
// Description:
// Delete the first 100 rows in the eventlog table
//
// Comment:
//
void __fastcall TCIISDBModule::DeleteOldEvents()
{
   FDConnection->ExecSQL("DELETE FROM eventlog LIMIT 100;");
}

//---------------------------------------------------------------------------
//
// Description:
// Log event
// Save event in table if EvLevel > PrjEvLevel
//
// Comment:
//

void __fastcall TCIISDBModule::LogEvent(CIIEventType EvType, CIIEventCode EvCode,
																	 CIIEventLevel EvLevel, String EvString,
																	 Integer EvInt, double EvFloat, int32_t EvLogSize)
{
 if ( ServerMode == CIISRemoteServer )
	return;

 Boolean EvLogAct;

 EvLogAct = FDTableEventLog->Active;

 try
  {
		FDTableEventLog->Active = true;

		TDateTime CurrentEvTime = Now_ms_cleard();
		int32_t CurrentFraction = 0;

		//FDTableEventLog->FindLast();
		//TDateTime LastEvTime = FDTableEventLog->FieldByName("EventDateTime")->AsDateTime;

		if ( CurrentEvTime <= LastStoredEvent )
		{
			CurrentEvTime = LastStoredEvent;
			CurrentFraction = ++LastStoredFraction;
			//int32_t LastFraction = FDTableEventLog->FieldByName("Fraction")->AsInteger;
			//CurrentFraction = LastFraction + 1;
		}

			// Delete the oldest events when eventlog is too large
		if( FDTableEventLog->RecordCount >= EvLogSize )
		{
			DeleteOldEvents();
			FDTableEventLog->Refresh();
		}

		FDTableEventLog->Append();
		FDTableEventLog->FieldValues["EventDateTime"] = CurrentEvTime;
		FDTableEventLog->FieldValues["Fraction"] = CurrentFraction;
		FDTableEventLog->FieldValues["EventType"] = EvType;
		FDTableEventLog->FieldValues["EventCode"] = EvCode;
		FDTableEventLog->FieldValues["EventLevel"] = EvLevel;
		FDTableEventLog->FieldValues["EventNo"] = 0;
		FDTableEventLog->FieldValues["EventString"] = EvString;
		FDTableEventLog->FieldValues["EventInt"] = EvInt;
		FDTableEventLog->FieldValues["EventFloat"] = EvFloat;
		FDTableEventLog->Post();

		ApplyUpdatesEvent();

		LastStoredEvent = CurrentEvTime;
		LastStoredFraction = CurrentFraction;

		EventInserted=true;

	}
 catch(const Exception &E)
  {
		 Debug(Name + "::Log() " + FDTableEventLog->TableName + ":" + E.Message);
		 FDTableEventLog->Cancel();
  }

 FDTableEventLog->Active = EvLogAct;
}

uint32_t __fastcall TCIISDBModule::GetEventLogRecCount()
{
 return FDTableEventLog->RecordCount;
}

bool __fastcall TCIISDBModule::FindFirstEvent()
{
  return FDTableEventLog->FindFirst();
}

bool __fastcall TCIISDBModule::FindLastEvent()
{
  return FDTableEventLog->FindLast();
}


bool __fastcall TCIISDBModule::LocateEvent( TDateTime EvDateTime, int32_t EventNo )
{
 if (EventInserted)
 {
    FDTableEventLog->Refresh();
    EventInserted = false;
 }

		// MySQL
 int32_t Fraction;
 Variant Args[3];
 Args[0] = DateTimeNoMilliSecs(EvDateTime,&Fraction);
 Args[1] = Fraction;
 Args[2] = EventNo;

 return  FDTableEventLog->Locate("EventDateTime;Fraction;EventNo", VarArrayOf( Args, 2 ), NoOpts );
}

bool __fastcall TCIISDBModule::FindNextEvent()
{
 return FDTableEventLog->FindNext();
}

bool __fastcall TCIISDBModule::GetEventRec( CIISEventRec *EventRec, bool AutoInc)
{
 int32_t millisecs=FDTableEventLog->FieldByName("Fraction")->AsInteger;
 EventRec->EventDateTime = ReadDateTime(FDTableEventLog,"EventDateTime")
										+ TDateTime( One_mS * millisecs );
 EventRec->EventType = FDTableEventLog->FieldByName("EventType")->AsInteger;
 EventRec->EventCode = FDTableEventLog->FieldByName("EventCode")->AsInteger;
 EventRec->EventLevel = FDTableEventLog->FieldByName("EventLevel")->AsInteger;
 EventRec->EventNo = FDTableEventLog->FieldByName("EventNo")->AsInteger;
 EventRec->EventString = FDTableEventLog->FieldByName("EventString")->AsString;
 EventRec->EventInt = FDTableEventLog->FieldByName("EventInt")->AsInteger;
 EventRec->EventFloat = FDTableEventLog->FieldByName("EventFloat")->AsFloat;

 if( AutoInc ) return FDTableEventLog->FindNext();
 else return true;
}

bool __fastcall TCIISDBModule::PostEventRec( CIISEventRec *EventRec)
{
 bool PostOk;
 int32_t Fraction;
 TDateTime TimestampWithoutMilliSecs = DateTimeNoMilliSecs(EventRec->EventDateTime,&Fraction);


 FDTableEventLog->FieldValues["EventDateTime"] = TimestampWithoutMilliSecs;
 FDTableEventLog->FieldValues["Fraction"] = Fraction;
 FDTableEventLog->FieldValues["EventType"] = EventRec->EventType;
 FDTableEventLog->FieldValues["EventCode"] = EventRec->EventCode;
 FDTableEventLog->FieldValues["EventLevel"] = EventRec->EventLevel;
 FDTableEventLog->FieldValues["EventNo"] = 0;
 FDTableEventLog->FieldValues["EventString"] = EventRec->EventString;
 FDTableEventLog->FieldValues["EventInt"] = EventRec->EventInt;
 FDTableEventLog->FieldValues["EventFloat"] = EventRec->EventFloat;

 try
  {
	FDTableEventLog->Post();
	PostOk=true;
  }
 catch (const Exception &E)
  {
	PostOk = false;
	Debug(Name + "::PostEventRec() " + FDTableEventLog->TableName + ":" + E.Message);
	FDTableEventLog->Cancel();
  }

 return PostOk;
}

bool __fastcall TCIISDBModule::AppendEventRec( CIISEventRec *EventRec)
{
 bool NewRecUpdated;

 try
  {
   FDTableEventLog->Append();
   NewRecUpdated = PostEventRec( EventRec );
  }
 catch (const Exception &E)
  {
   NewRecUpdated = false;
   Debug(Name + "::AppendEventRec() " + FDTableEventLog->TableName + ":" + E.Message);
  }
 return NewRecUpdated;
}

bool __fastcall TCIISDBModule::ApplyUpdatesEvent(void)
{
 int32_t NoOfErrors=FDTableEventLog->ApplyUpdates();
 FDTableEventLog->CommitUpdates();

 if (NoOfErrors)
 {
   Debug(Name + "::ApplyUpdatesEvent() " + FDTableEventLog->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
 }
 else
  return true;

}

//		Recording table

uint32_t __fastcall TCIISDBModule::GetRecordingRecCount()
{
 return FDTableRecordings->RecordCount;
}

bool __fastcall TCIISDBModule::FindFirstRecording()
{
 return FDTableRecordings->FindFirst();
}
bool	__fastcall TCIISDBModule::FindLastRecording()
{
 return FDTableRecordings->FindLast();
}


bool __fastcall TCIISDBModule::LocateRecordingNo( uint32_t RecordingNo )
{
 return FDTableRecordings->Locate("RecNo", RecordingNo, NoOpts );
}

bool __fastcall TCIISDBModule::FindNextRecording()
{
 return FDTableRecordings->FindNext();
}

bool __fastcall TCIISDBModule::LocateLastRecNo( int32_t ZoneNo )
{
	// access only a part of the recordings table using a filter
	FDTableRecordings->Active=false;

	FDTableRecordings->Filtered = false;
	FDTableRecordings->Filter = "ZoneNo = '" + IntToStr( ZoneNo ) + "'";
	FDTableRecordings->Filtered = true;
	FDTableRecordings->Active=true;

	bool result = false;
	int32_t RecNo;
	if( FDTableRecordings->FindLast() )
	{
		RecNo = FDTableRecordings->FieldByName( "RecNo" )->AsInteger;
		result = true;
	}
	else
	{
		result = false;
	}

	// access entire table again
	FDTableRecordings->Active=false;
	FDTableRecordings->Filtered=false;
	FDTableRecordings->Filter="";
	FDTableRecordings->Active=true;

	if( result )
	{
		LocateRecordingNo( RecNo );
	}
	return result;
}

//---------------------------------------------------------------------------
//
// Description:
// Edit the fields of a recording and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesRecording() in order to apply the changes.
//
bool	__fastcall TCIISDBModule::SetRecordingRec( CIISRecordingRec *RecordingRec )
{
 FDTableRecordings->Edit();
 return PostRecordingRec(RecordingRec);
}

bool	__fastcall TCIISDBModule::PostRecordingRec( CIISRecordingRec *RecordingRec )
{
 FDTableRecordings->FieldValues[ "RecNo" ] = RecordingRec->RecNo;
 WriteDateTime(FDTableRecordings,"RecStart",RecordingRec->RecStart);
 WriteDateTime(FDTableRecordings,"RecStop",RecordingRec->RecStop);
 FDTableRecordings->FieldValues[ "RecType" ] = RecordingRec->RecType;
 FDTableRecordings->FieldValues[ "DecaySampInterval" ] = RecordingRec->DecaySampInterval;
 FDTableRecordings->FieldValues[ "DecayDuration" ] = RecordingRec->DecayDuration;
 FDTableRecordings->FieldValues[ "LPRRange" ] = RecordingRec->LPRRange;
 FDTableRecordings->FieldValues[ "LPRStep" ] = RecordingRec->LPRStep;
 FDTableRecordings->FieldValues[ "LPRDelay1" ] = RecordingRec->LPRDelay1;
 FDTableRecordings->FieldValues[ "LPRDelay2" ] = RecordingRec->LPRDelay2;
 FDTableRecordings->FieldValues[ "SampInterval" ] = RecordingRec->SampInterval;
 FDTableRecordings->FieldValues[ "LPRMode" ] = RecordingRec->LPRMode;
 FDTableRecordings->FieldValues[ "ZoneNo" ] = RecordingRec->ZoneNo;

 FDTableRecordings->FieldValues[ "DecayDelay" ] = RecordingRec->DecayDelay;
 FDTableRecordings->FieldValues[ "DecaySampInterval2" ] = RecordingRec->DecaySampInterval2;
 FDTableRecordings->FieldValues[ "DecayDuration2" ] = RecordingRec->DecayDuration2;

 bool PostOk;
 try
  {
   FDTableRecordings->Post();
   PostOk=true;
  }
 catch (const Exception &E)
  {
   PostOk = false;
   Debug(Name + "::PostRecordingRec() " + FDTableRecordings->TableName + ":" + E.Message);
   FDTableRecordings->Cancel();
  }
 return PostOk;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new recording record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesRecording() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendRecordingRec( CIISRecordingRec *RecordingRec )
{
 bool NewRecUpdated;

 try
  {
   FDTableRecordings->Append();
   NewRecUpdated = SetRecordingRec( RecordingRec );
  }
 catch (const Exception &E)
  {
   NewRecUpdated = false;
   Debug(Name + "::AppendRecordingRec() " + FDTableRecordings->TableName + ":" + E.Message);
  }
 return NewRecUpdated;
}
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a recording
//
// Comment: Note !! Moves to the next recording in db before return
// Returns true if next recording available.
// Returns false if end of recordings
//
bool __fastcall TCIISDBModule::GetRecordingRec( CIISRecordingRec *RecordingRec, bool AutoInc)
{

 RecordingRec->RecNo = FDTableRecordings->FieldByName( "RecNo" )->AsInteger;
 RecordingRec->RecStart = ReadDateTime(FDTableRecordings,"RecStart");
 RecordingRec->RecStop = ReadDateTime(FDTableRecordings,"RecStop");
 RecordingRec->RecType = FDTableRecordings->FieldByName( "RecType" )->AsInteger;
 RecordingRec->DecaySampInterval = FDTableRecordings->FieldByName( "DecaySampInterval" )->AsInteger;
 RecordingRec->DecayDuration = FDTableRecordings->FieldByName( "DecayDuration" )->AsInteger;
 RecordingRec->LPRRange = FDTableRecordings->FieldByName( "LPRRange" )->AsInteger;
 RecordingRec->LPRStep = FDTableRecordings->FieldByName( "LPRStep" )->AsInteger;
 RecordingRec->LPRDelay1 = FDTableRecordings->FieldByName( "LPRDelay1" )->AsInteger;
 RecordingRec->LPRDelay2 = FDTableRecordings->FieldByName( "LPRDelay2" )->AsInteger;
 RecordingRec->SampInterval = FDTableRecordings->FieldByName( "SampInterval" )->AsInteger;
 RecordingRec->LPRMode = FDTableRecordings->FieldByName( "LPRMode" )->AsInteger;

 RecordingRec->DecayDelay = FDTableRecordings->FieldByName( "DecayDelay" )->AsInteger;
 RecordingRec->DecayDuration2 = FDTableRecordings->FieldByName( "DecayDuration2" )->AsInteger;
 RecordingRec->DecaySampInterval2 = FDTableRecordings->FieldByName( "DecaySampInterval2" )->AsInteger;

 RecordingRec->ZoneNo = FDTableRecordings->FieldByName( "ZoneNo" )->AsInteger;

 if( AutoInc ) return FDTableRecordings->FindNext();
 else return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to recordings table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesRecording(void)
{
 int32_t NoOfErrors=FDTableRecordings->ApplyUpdates();
 FDTableRecordings->CommitUpdates();

 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesRecording() " + FDTableRecordings->TableName + ":No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesMonitor table
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetMonitorValuesRecCount()
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SELECT COUNT(*) FROM valuesmonitor;");
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}
//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesMonitor table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetMonitorValuesRecCountAfter(CIISMonitorValueRec *MValRec)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append(String("SELECT COUNT(*) FROM valuesmonitor WHERE DateTimeStamp > ")
					+ DateTimeToSQLStr(MValRec->DateTimeStamp) + String(";"));

 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the very first record in the ValuesMonitor table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! A fixed number of records (400) is retrieved.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//
bool __fastcall TCIISDBModule::FindFirstMonitorValue()
{
 FDQueryValues->Active=false;
 FDQueryValues->SQL->Clear();
 FDQueryValues->SQL->Append("SELECT * FROM valuesmonitor LIMIT 400;");
 FDQueryValues->Active=true;

 return FDQueryValues->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record in the ValuesMonitor table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//
bool __fastcall TCIISDBModule::FindLastMonitorValue()
{
 FDQueryValues->Active=false;
 FDQueryValues->SQL->Clear();
 FDQueryValues->SQL->Append("SELECT * FROM valuesmonitor ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValues->Active=true;

	// Only one record in dataset (LIMIT 1) . FindLast() or FindFirst() works Ok
	// However, if the query is changed to return more than one record FindFirst()
	// must be used as the "last" record will appear first in the resulting set
	// when the sort order is reversed
 return FDQueryValues->FindLast();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record belonging to a specified recording in the ValuesMonitor table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//
bool __fastcall TCIISDBModule::FindLastMonitorValueInRecording(int32_t RecNo)
{
 FDQueryValues->Active=false;
 FDQueryValues->SQL->Clear();
 FDQueryValues->SQL->Append("SELECT * FROM valuesmonitor WHERE RecNo = " + IntToStr(RecNo) + " ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValues->Active=true;

 return FDQueryValues->FindLast();
}

//---------------------------------------------------------------------------
//
// Description:
// Format a query string that will select records with a combined timestamp
// (DateTimeStamp + Fraction) or greater
//
String __fastcall TCIISDBModule::SQLSelectFromTimeStamp(String TableName,
								TDateTime DateTimeStamp, int32_t Fraction)
{
 String QueryString;

 String DateTimeSQLStr = DateTimeToSQLStr(DateTimeStamp);
 QueryString.sprintf(L"SELECT * FROM %s WHERE ((DateTimeStamp > %s) OR ((DateTimeStamp = %s) AND (Fraction >= %d)))",TableName.c_str(),DateTimeSQLStr.c_str(),DateTimeSQLStr.c_str(),Fraction);
 return QueryString;
}
//---------------------------------------------------------------------------
//
// Description:
// Locate a record in the ValuesMonitor table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// A fixed number of records will be retrieved to the local dataset starting at
// the second defined by the timestamp.
// After that the exact record (depending on Fraction) will be located.
//
// The server will read the following records one by one
// by calling GetMonitorValueRec (reads values and steps to the next record )
//
// Comment: Note! A fixed number of records (400) is retrieved to the local dataset.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//
bool __fastcall TCIISDBModule::LocateMonitorValue( TDateTime DateTimeStamp, uint32_t RecNo, uint32_t SensorSerialNo , String ValueType  )
{
 int32_t Fraction;
 TDateTime DateTimeNoms = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
		// MySQL
 FDQueryValues->Active=false;
 FDQueryValues->SQL->Clear();
 FDQueryValues->SQL->Append(SQLSelectFromTimeStamp("valuesmonitor", DateTimeNoms, Fraction) + String(" LIMIT 400;"));
 FDQueryValues->Active=true;

 Variant Args[5];
 Args[0] = DateTimeNoms;
 Args[1] = Fraction;
 Args[2] = RecNo;
 Args[3] = SensorSerialNo;
 Args[4] = ValueType;

 return FDQueryValues->Locate("DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", VarArrayOf( Args, 4 ), NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Step to the next record in the local dataset retrieved from the ValuesMonitor table
//
// Comment: Returns true when at the end of the local dataset.
// This means that there may be more records in the entire table.
// These records will be found after next LocateMonitorValue()
//
bool __fastcall TCIISDBModule::FindNextMonitorValue()
{
  return FDQueryValues->FindNext();
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a record in the ValuesMonitor table
//
// Comment: Note !! Moves to the next recording in the local dataset before return.
// Returns true if next value is available in the local dataset
// else returns false.
// This means that there may be more records in the entire table.
// These records will be found after next LocateMonitorValue()
//
 bool __fastcall TCIISDBModule::GetMonitorValueRec( CIISMonitorValueRec *MValRec, bool AutoInc )
{
 MValRec->DateTimeStamp = ReadDateTime(FDQueryValues,"DateTimeStamp") +
			TDateTime( One_mS * FDQueryValues->FieldByName("Fraction")->AsInteger );
 MValRec->RecNo = FDQueryValues->FieldByName( "RecNo" )->AsInteger;
 MValRec->SensorSerialNo = FDQueryValues->FieldByName( "SensorSerialNo" )->AsInteger;
 MValRec->ValueType = FDQueryValues->FieldByName( "ValueType" )->AsString;
 MValRec->ValueUnit = FDQueryValues->FieldByName( "ValueUnit" )->AsString;
 MValRec->Value = FDQueryValues->FieldByName("Value")->AsFloat;
 MValRec->RawValue = FDQueryValues->FieldByName("RawValue")->AsFloat;
 MValRec->SampleDateTime = ReadDateTime(FDQueryValues,"SampleDateTime");

 if( AutoInc ) return FDQueryValues->FindNext();
 else return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new monitor value record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesMonitorValue() in order to apply the changes.
//

bool __fastcall TCIISDBModule::AppendMonitorValueRec( CIISMonitorValueRec *MValRec)
{
	TDateTime ValueTimeStamp;

	ValueTimeStamp = MValRec->DateTimeStamp;
	if (	ValueTimeStamp <= LastValueMonitorTimeStamp )	ValueTimeStamp = LastValueMonitorTimeStamp + (TDateTime)One_mS ;

	LastValueMonitorTimeStamp = ValueTimeStamp;

	int32_t Fraction;
	TDateTime TimestampWithoutMilliSecs = DateTimeNoMilliSecs(ValueTimeStamp,&Fraction);

	TabValuesAppend->Append();
	WriteDateTime(TabValuesAppend,"DateTimeStamp",TimestampWithoutMilliSecs);
	TabValuesAppend->FieldValues["Fraction"] =  Fraction;
	TabValuesAppend->FieldValues["RecNo"] =  MValRec->RecNo;
	TabValuesAppend->FieldValues["SensorSerialNo"] =  MValRec->SensorSerialNo;
	TabValuesAppend->FieldValues["ValueType"] =  MValRec->ValueType;
	TabValuesAppend->FieldValues["ValueUnit"] =  MValRec->ValueUnit;
	TabValuesAppend->FieldValues["Value"] =  MValRec->Value;
	TabValuesAppend->FieldValues["RawValue"] =  MValRec->RawValue;
	WriteDateTime(TabValuesAppend,"SampleDateTime",MValRec->SampleDateTime);
	bool PostOk;
	try
	{
		TabValuesAppend->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendMonitorValueRec() " + E.Message);
		TabValuesAppend->Cancel();
	}

	return PostOk;
}

bool __fastcall TCIISDBModule::AppendMonitorValueRec_NoTimeAdj( CIISMonitorValueRec *MValRec)
{
 int32_t Fraction;
 TDateTime TimestampWithoutMilliSecs = DateTimeNoMilliSecs(MValRec->DateTimeStamp,&Fraction);

 TabValuesAppend->Append();
 WriteDateTime(TabValuesAppend,"DateTimeStamp",TimestampWithoutMilliSecs);
 TabValuesAppend->FieldValues["Fraction"] =  Fraction;
 TabValuesAppend->FieldValues["RecNo"] =  MValRec->RecNo;
 TabValuesAppend->FieldValues["SensorSerialNo"] =  MValRec->SensorSerialNo;
 TabValuesAppend->FieldValues["ValueType"] =  MValRec->ValueType;
 TabValuesAppend->FieldValues["ValueUnit"] =  MValRec->ValueUnit;
 TabValuesAppend->FieldValues["Value"] =  MValRec->Value;
 TabValuesAppend->FieldValues["RawValue"] =  MValRec->RawValue;
 WriteDateTime(TabValuesAppend,"SampleDateTime",MValRec->SampleDateTime);
 bool PostOk;
 try
  {
   TabValuesAppend->Post();
   PostOk=true;
  }
 catch (const Exception &E)
  {
   PostOk = false;
   Debug(Name + "::AppendMonitorValueRec() " + E.Message);
   TabValuesAppend->Cancel();
  }
 return PostOk;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending inserts (Client Data Set) into MySQL-table
//
// Comment: returns false if at least one pending insert failed
//
bool	__fastcall TCIISDBModule::SQLApplyInserts(String TableName,TClientDataSet *DataSet)
{
  if (DataSet->RecordCount)
  {

    String SQLString="INSERT INTO " + TableName + " ";

		    // Field names
    SQLString += "(";
    for (int32_t fi = 0; fi < DataSet->FieldCount; fi++)
    {
      if (fi)
      {
        SQLString += ",";
      }
      SQLString += DataSet->FieldDefs->Items[fi]->Name;

    }
    SQLString += ")";

    SQLString +=" VALUES";

		    // Field values
    int32_t Count=0;
    while (DataSet->RecordCount)
    {
      if (Count)
      {
        SQLString += ",";
      }
      DataSet->First();
      SQLString += "(";
      for (int32_t fi = 0; fi < DataSet->FieldCount; fi++)
      {
        if (fi)
        {
          SQLString += ",";
        }
        SQLString += FieldToSQLStr(DataSet, fi);
      }
      SQLString += ")";

      Count++;

      DataSet->Delete();
    }
    SQLString += ";";

//Example
//INSERT INTO itx.valuesmonitor (DateTimeStamp,Fraction,RecNo,SensorSerialNo,ValueType,Value,RawValue) VALUES('2012-04-02 13:40:02',1,3,720897,'XX','??',1.23,4.56);
//
            // ExecuteDirect returns the number of affected rows which should
            // the same as the no of rows to insert
    if (FDConnection->ExecSQL(SQLString) == Count)
      return true;
    else
      return false;
  }
  else
    return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to monitor values table
//
// Comment: returns false if at least one pending update failed
//
#define ValuesMonitorAppendCloseReopenPeriod 100
bool	__fastcall TCIISDBModule::ApplyUpdatesMonitorValue(void)
{
/*
 int32_t NoOfErrors=TabValuesAppend->ApplyUpdates(-1);
// TabValues->Refresh();
 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesMonitorValue() " + "No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  {
   TDateTime TimeNow=Now();

   if (((double)(TimeNow - ValuesMonitorAppendStartDateTime) > (1/24.0)) || ForceNewValuesMonitorAppendStartDateTime)
	{  	// Refresh query if older than 1 hour
		// in order to not to waste memory
	 TabValuesAppend->Active=false;
//	 SQLDataSetValuesAppend->Active=false;

	 TabValuesAppend->DataSet->CommandText =
	 String("SELECT * FROM valuesmonitor WHERE DateTimeStamp > ") + TodayThisSecSQLStr()
			+ String(";");
	 ValuesMonitorAppendStartDateTime=TimeNow;

//	 SQLDataSetValuesAppend->Active=true;
	 TabValuesAppend->Active=true;
	 ForceNewValuesMonitorAppendStartDateTime = false;
	}
   return true;
  }
  */

    bool Result = SQLApplyInserts("valuesmonitor",TabValuesAppend);

    return Result;
}

//		LPR table
//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesLPR table
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//

uint32_t __fastcall TCIISDBModule::GetLPRValuesRecCount(void)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SELECT COUNT(*) FROM valueslpr;");
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;

}
//		LPR table
//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesLPR table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetLPRValuesRecCountAfter(CIISLPRValueRec *LPRValRec)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append(String("SELECT COUNT(*) FROM valueslpr WHERE DateTimeStamp > ")
					+ DateTimeToSQLStr(LPRValRec->DateTimeStamp) + String(";"));
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;

}

//---------------------------------------------------------------------------
//
// Description:
// Locate the very first record in the ValuesLPR table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! A fixed number of records (400) is retrieved.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//
bool __fastcall TCIISDBModule::FindFirstLPRValue(void)
{
 FDQueryValuesLPR->Active=false;
 FDQueryValuesLPR->SQL->Clear();
 FDQueryValuesLPR->SQL->Append("SELECT * FROM valuesLPR LIMIT 400;");
 FDQueryValuesLPR->Active=true;

 return FDQueryValuesLPR->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record in the ValuesLPR table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//
bool __fastcall TCIISDBModule::FindLastLPRValue(void)
{
 FDQueryValuesLPR->Active=false;
 FDQueryValuesLPR->SQL->Clear();
 FDQueryValuesLPR->SQL->Append("SELECT * FROM valueslpr ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValuesLPR->Active=true;

	// Only one record in dataset (LIMIT 1) . FindLast() or FindFirst() works Ok
	// However, if the query is changed to return more than one record FindFirst()
	// must be used as the "last" record will appear first in the resulting set
	// when the sort order is reversed
 return FDQueryValuesLPR->FindLast();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record belonging to a specified recording in the ValuesLPR table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//

bool __fastcall TCIISDBModule::FindLastLPRValueInRecording(int32_t RecNo)
{
 FDQueryValuesLPR->Active=false;
 FDQueryValuesLPR->SQL->Clear();
 FDQueryValuesLPR->SQL->Append("SELECT * FROM valueslpr WHERE RecNo = " + IntToStr(RecNo) + " ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValuesLPR->Active=true;

 return FDQueryValuesLPR->FindLast();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate a record in the ValuesLPR table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// A fixed number of records will be retrieved to the local dataset starting at
// the second defined by the timestamp.
// After that the exact record (depending on Fraction) will be located.
//
// The server will read the following records one by one
// by calling GetLPRValueRec (reads values and steps to the next record )
//
// Comment: Note! A fixed number of records (400) is retrieved to the local dataset.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//

bool __fastcall TCIISDBModule::LocateLPRValue( TDateTime DateTimeStamp )
{
 int32_t Fraction;
 TDateTime DateTimeNoms = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
		// MySQL
 FDQueryValuesLPR->Active=false;
 FDQueryValuesLPR->SQL->Clear();
 FDQueryValuesLPR->SQL->Append(SQLSelectFromTimeStamp("valueslpr", DateTimeNoms, Fraction) + String(" LIMIT 400;"));
 FDQueryValuesLPR->Active=true;

 Variant Args[2];
 Args[0] = DateTimeNoms;
 Args[1] = Fraction;

 return FDQueryValuesLPR->Locate("DateTimeStamp;Fraction", VarArrayOf( Args, 1 ), NoOpts );
}

//---------------------------------------------------------------------------
//
// Description:
// Step to the next record in the local dataset retrieved from the ValuesLPR table
//
// Comment: Returns true when at the end of the local dataset.
// This means that there may be more records in the entire table.
// These records will be found after next LocateLPRValue()
//
bool __fastcall TCIISDBModule::FindNextLPRValue(void)
{
 return FDQueryValuesLPR->FindNext();
}

//---------------------------------------------------------------------------
//
// Description:
// Query the SQL database in order to retrieve the matching records to the local
// dataset.
//
// Comment:
//
void __fastcall TCIISDBModule::SetLPRQuery(TDateTime RecStart, uint32_t RecNo,
									int32_t SensorSerialNo,String ValueType)

{
 FDQueryValuesLPR->Active=false;
 FDQueryValuesLPR->SQL->Clear();
 FDQueryValuesLPR->SQL->Append(L"SELECT * FROM valueslpr WHERE DateTimeStamp >= " + DateTimeToSQLStr(RecStart) + L" AND RecNo = " + IntToStr((int32_t)RecNo) +
									L" AND SensorSerialNo = " + IntToStr(SensorSerialNo) +
									L" AND ValueType = '" + ValueType + L"';");
 FDQueryValuesLPR->Active=true;
}

//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesLPR local ClientDataSet
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetLPRLocalValuesRecCount(void)
{
 return FDQueryValuesLPR->RecordCount;
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the first record in the ValuesLPR local ClientDataSet
//
//
bool	__fastcall TCIISDBModule::FindFirstLPRLocalValue(void)
{
 return FDQueryValuesLPR->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a record in the ValuesLPR table
//
// Comment: Note !! Moves to the next recording in the local dataset before return.
// Returns true if next value is available in the local dataset
// else returns false.
// This means that there may be more records in the entire table.
// These records will be found after next LocateLPRValue()
//
 bool __fastcall TCIISDBModule::GetLPRValueRec( CIISLPRValueRec *LPRValRec, bool AutoInc )
{
 LPRValRec->DateTimeStamp = ReadDateTime(FDQueryValuesLPR,"DateTimeStamp") +
			TDateTime( One_mS * FDQueryValuesLPR->FieldByName("Fraction")->AsInteger );
 LPRValRec->RecNo = FDQueryValuesLPR->FieldByName( "RecNo" )->AsInteger;
 LPRValRec->SensorSerialNo = FDQueryValuesLPR->FieldByName( "SensorSerialNo" )->AsInteger;
 LPRValRec->ValueType = FDQueryValuesLPR->FieldByName( "ValueType" )->AsString;
 LPRValRec->ValueV = FDQueryValuesLPR->FieldByName("ValueV")->AsFloat;
 LPRValRec->ValueI = FDQueryValuesLPR->FieldByName("ValueI")->AsFloat;

 if( AutoInc )
  return FDQueryValuesLPR->FindNext();
 else
  return true;
}

bool __fastcall TCIISDBModule::AppendLPRValueRec( CIISLPRValueRec *LPRValRec )
{
	TDateTime ValueTimeStamp, TimestampWithoutMilliSecs;
	int32_t Fraction;

	ValueTimeStamp = LPRValRec->DateTimeStamp;
	if (	ValueTimeStamp <= LastValueLPRTimeStamp ) ValueTimeStamp = LastValueLPRTimeStamp + (TDateTime)One_mS;
	LastValueLPRTimeStamp = ValueTimeStamp;

	TimestampWithoutMilliSecs = DateTimeNoMilliSecs(ValueTimeStamp,&Fraction);

	TabValuesLPRAppend->Append();
	WriteDateTime(TabValuesLPRAppend,"DateTimeStamp",TimestampWithoutMilliSecs);
	TabValuesLPRAppend->FieldValues["Fraction"] = Fraction;
	TabValuesLPRAppend->FieldValues["RecNo"] = LPRValRec->RecNo;
	TabValuesLPRAppend->FieldValues["SensorSerialNo"] = LPRValRec->SensorSerialNo;
	TabValuesLPRAppend->FieldValues["ValueType"] = LPRValRec->ValueType; // 2010-11-12 ver 3.6.0
	TabValuesLPRAppend->FieldValues["ValueV"] = LPRValRec->ValueV;
	TabValuesLPRAppend->FieldValues["ValueI"] = LPRValRec->ValueI;
	bool PostOk;
	try
	{
		TabValuesLPRAppend->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendLPRValueRec() " + E.Message);
		TabValuesLPRAppend->Cancel();
	}
	return PostOk;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to LPR values table
//
// Comment: returns false if at least one pending update failed
//
#define ValuesLPRAppendCloseReopenPeriod 100
bool	__fastcall TCIISDBModule::ApplyUpdatesLPRValue(void)
{
	bool Result = SQLApplyInserts("valueslpr",TabValuesLPRAppend);
	return Result;
}

//		Decay table
//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesDecay table
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//

uint32_t __fastcall TCIISDBModule::GetDecayValuesRecCount()
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SELECT COUNT(*) FROM valuesdecay;");
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}
//		Decay table
//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesDecay table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetDecayValuesRecCountAfter(CIISDecayValueRec *DecValRec)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append(String("SELECT COUNT(*) FROM valuesdecay WHERE DateTimeStamp > ")
					+ DateTimeToSQLStr(DecValRec->DateTimeStamp) + String(";"));
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;

}

TDateTime __fastcall TCIISDBModule::GetLastMonitorTimestamp(void)
{

 if (FindLastMonitorValue())
 {
    return AssembleDateTime(FDQueryValues->FieldByName("DateTimeStamp")->AsDateTime,
														FDQueryValues->FieldByName("Fraction")->AsInteger );
 }
 else return TDateTime(0);
}

TDateTime __fastcall TCIISDBModule::GetLastLPRTimestamp(void)
{
	if (FindLastLPRValue())
	{
		return AssembleDateTime(FDQueryValuesLPR->FieldByName("DateTimeStamp")->AsDateTime,
														FDQueryValuesLPR->FieldByName("Fraction")->AsInteger );
	}
	else return TDateTime(0);
}

TDateTime __fastcall TCIISDBModule::GetLastCalcTimestamp(void)
{
	if (FindLastCalcValue())
	{
		return AssembleDateTime(FDTableValuesCalc->FieldByName("DateTimeStamp")->AsDateTime,
														FDTableValuesCalc->FieldByName("Fraction")->AsInteger );
	}
	else return TDateTime(0);
}

TDateTime __fastcall TCIISDBModule::GetLastDecayTimestamp(void)
{
	if (FindLastDecayValue())
	{
		return AssembleDateTime(FDQueryValuesDecay->FieldByName("DateTimeStamp")->AsDateTime,
														FDQueryValuesDecay->FieldByName("Fraction")->AsInteger );
	}
	else return TDateTime(0);
}

TDateTime __fastcall TCIISDBModule::GetLastMiscTimestamp(void)
{
	if (FDTableValuesMisc->FindLast())
	{
		return AssembleDateTime(FDTableValuesMisc->FieldByName("DateTimeStamp")->AsDateTime,
														FDTableValuesMisc->FieldByName("Fraction")->AsInteger );
	}
	else return TDateTime(0);
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the very first record in the ValuesDecay table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! A fixed number of records (400) is retrieved.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//

bool __fastcall TCIISDBModule::FindFirstDecayValue()
{
 FDQueryValuesDecay->Active=false;
 FDQueryValuesDecay->SQL->Clear();
 FDQueryValuesDecay->SQL->Append("SELECT * FROM valuesdecay LIMIT 400;");
 FDQueryValuesDecay->Active=true;
 return FDQueryValuesDecay->FindFirst();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record in the ValuesDecay table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//
bool __fastcall TCIISDBModule::FindLastDecayValue()
{
 FDQueryValuesDecay->Active=false;
 FDQueryValuesDecay->SQL->Clear();
 FDQueryValuesDecay->SQL->Append("SELECT * FROM valuesdecay ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValuesDecay->Active=true;

	// Only one record in dataset (LIMIT 1) . FindLast() or FindFirst() works Ok
	// However, if the query is changed to return more than one record FindFirst()
	// must be used as the "last" record will appear first in the resulting set
	// when the sort order is reversed
 return FDQueryValuesDecay->FindLast();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the very last record belonging to a specified recording in the ValuesDecay table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment: Note! Only one record is retrieved.
//
bool __fastcall TCIISDBModule::FindLastDecayValueInRecording(int32_t RecNo)
{
 FDQueryValuesDecay->Active=false;
 FDQueryValuesDecay->SQL->Clear();
 FDQueryValuesDecay->SQL->Append("SELECT * FROM valuesdecay WHERE RecNo = " + IntToStr(RecNo) + " ORDER BY DateTimeStamp DESC,Fraction DESC,RecNo DESC,SensorSerialNo DESC,ValueType DESC LIMIT 1;");
 FDQueryValuesDecay->Active=true;

 return FDQueryValuesDecay->FindLast();
}
//---------------------------------------------------------------------------
//
// Description:
// Locate a record in the ValuesDecay table
//
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// A fixed number of records will be retrieved to the local dataset starting at
// the second defined by the timestamp.
// After that the exact record (depending on Fraction) will be located.
//
// The server will read the following records one by one
// by calling GetDecayValueRec (reads values and steps to the next record )
//
// Comment: Note! A fixed number of records (400) is retrieved to the local dataset.
// The reason is that the server will normally proceed with sending
// a package with approx. 200-300 values and it is efficient to have them
// available.
//

bool __fastcall TCIISDBModule::LocateDecayValue( TDateTime DateTimeStamp, uint32_t RecNo, uint32_t SensorSerialNo, String ValueType  )
{
 int32_t Fraction;
 TDateTime DateTimeNoms = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
		// MySQL
 FDQueryValuesDecay->Active=false;
 FDQueryValuesDecay->SQL->Clear();
 FDQueryValuesDecay->SQL->Append(SQLSelectFromTimeStamp("valuesdecay", DateTimeNoms, Fraction) + String(" LIMIT 400;"));
 FDQueryValuesDecay->Active=true;

 Variant Args[5];
 Args[0] = DateTimeNoms;
 Args[1] = Fraction;
 Args[2] = RecNo;
 Args[3] = SensorSerialNo;
 Args[4] = ValueType;

 return FDQueryValuesDecay->Locate("DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", VarArrayOf( Args, 4 ), NoOpts );
}


//---------------------------------------------------------------------------
//
// Description:
// Step to the next record in the local dataset retrieved from the ValuesDecay table
//
// Comment: Returns true when at the end of the local dataset.
// This means that there may be more records in the entire table.
// These records will be found after next LocateDecayValue()
//
bool __fastcall TCIISDBModule::FindNextDecayValue()
{
 return FDQueryValuesDecay->FindNext();
}

//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a record in the ValuesDecay table
//
// Comment: Note !! Moves to the next recording in the local dataset before return.
// Returns true if next value is available in the local dataset
// else returns false.
// This means that there may be more records in the entire table.
// These records will be found after next LocateDecayValue()
//
 bool __fastcall TCIISDBModule::GetDecayValueRec( CIISDecayValueRec *DecValRec, bool AutoInc )
{
  DecValRec->DateTimeStamp = ReadDateTime(FDQueryValuesDecay,"DateTimeStamp") +
							 TDateTime( One_mS * FDQueryValuesDecay->FieldByName("Fraction")->AsInteger );
  DecValRec->RecNo = FDQueryValuesDecay->FieldByName( "RecNo" )->AsInteger;
  DecValRec->SensorSerialNo = FDQueryValuesDecay->FieldByName( "SensorSerialNo" )->AsInteger;
  DecValRec->ValueType = FDQueryValuesDecay->FieldByName( "ValueType" )->AsString;
  DecValRec->Value = FDQueryValuesDecay->FieldByName("Value")->AsFloat;
  DecValRec->SampleDateTime = ReadDateTime(FDQueryValuesDecay,"SampleDateTime");

  if( AutoInc ) return FDQueryValuesDecay->FindNext();
  else return true;
}
//---------------------------------------------------------------------------
//
// Description:
// Append a new decay value record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesDecayValue() in order to apply the changes.
//
bool __fastcall TCIISDBModule::AppendDecayValueRec( CIISDecayValueRec *DecValRec )
{
	TDateTime ValueTimeStamp;

	ValueTimeStamp = DecValRec->DateTimeStamp;
	if (	ValueTimeStamp <= LastValueDecayTimeStamp )	ValueTimeStamp = LastValueDecayTimeStamp + (TDateTime)One_mS;
	LastValueDecayTimeStamp = ValueTimeStamp;

	int32_t Fraction;
	TDateTime TimestampWithoutMilliSecs = DateTimeNoMilliSecs(ValueTimeStamp,&Fraction);

	TabValuesDecayAppend->Append();
	WriteDateTime(TabValuesDecayAppend,"DateTimeStamp",TimestampWithoutMilliSecs);
	TabValuesDecayAppend->FieldValues["Fraction"] = Fraction;
	TabValuesDecayAppend->FieldValues["RecNo"] = DecValRec->RecNo;
	TabValuesDecayAppend->FieldValues["SensorSerialNo"] = DecValRec->SensorSerialNo;
	TabValuesDecayAppend->FieldValues["ValueType"] = DecValRec->ValueType;
	TabValuesDecayAppend->FieldValues["Value"] = DecValRec->Value;
	WriteDateTime(TabValuesDecayAppend,"SampleDateTime",DecValRec->SampleDateTime);
	bool PostOk;
	try
	{
		TabValuesDecayAppend->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendDecayValueRec() " + E.Message);
		TabValuesDecayAppend->Cancel();
	}
	return PostOk;
}

bool __fastcall TCIISDBModule::AppendDecayValueRec_NoTimeAdj( CIISDecayValueRec *DecValRec )
{
	int32_t Fraction;
	TDateTime TimestampWithoutMilliSecs;

	TimestampWithoutMilliSecs = DateTimeNoMilliSecs(DecValRec->DateTimeStamp,&Fraction);

	TabValuesDecayAppend->Append();
	WriteDateTime(TabValuesDecayAppend,"DateTimeStamp",TimestampWithoutMilliSecs);
	TabValuesDecayAppend->FieldValues["Fraction"] = Fraction;
	TabValuesDecayAppend->FieldValues["RecNo"] = DecValRec->RecNo;
	TabValuesDecayAppend->FieldValues["SensorSerialNo"] = DecValRec->SensorSerialNo;
	TabValuesDecayAppend->FieldValues["ValueType"] = DecValRec->ValueType;
	TabValuesDecayAppend->FieldValues["Value"] = DecValRec->Value;
	WriteDateTime(TabValuesDecayAppend,"SampleDateTime",DecValRec->SampleDateTime);
	bool PostOk;
	try
	{
		TabValuesDecayAppend->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendDecayValueRec() " + E.Message);
		TabValuesDecayAppend->Cancel();
	}
	return PostOk;
}


//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesMisc table
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetMiscValuesRecCount()
{
 if (MiscValueInserted)
 {
    FDTableValuesMisc->Refresh();
    MiscValueInserted=false;
 }
 return FDTableValuesMisc->RecordCount;
}
//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesMisc table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetMiscValuesRecCountAfter(CIISMiscValueRec *MValRec)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append(String("SELECT COUNT(*) FROM valuesmisc WHERE DateTimeStamp > ")
					+ DateTimeToSQLStr(MValRec->DateTimeStamp) + String(";"));

 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}
//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesMisc table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::PeekMiscValuesRecCount()
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SELECT COUNT(*) FROM valuesmisc;");

 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}
//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesMisc table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read.
//
// Comment:
//
void __fastcall TCIISDBModule::DeleteOldMiscValues()
{
   FDConnection->ExecSQL("DELETE FROM valuesmisc LIMIT 2000;");
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the first miscellaneous value in table
//
// Comment:
//
bool __fastcall TCIISDBModule::FindFirstMiscValue()
{
  return FDTableValuesMisc->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the last miscellaneous value in table
//
// Comment:
//
bool __fastcall TCIISDBModule::FindLastMiscValue()
{
  if (MiscValueInserted)
  {
    MiscValueInserted = false;
    FDTableValuesMisc->Refresh();
  }
  return FDTableValuesMisc->FindLast();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate a record in the ValuesMisc table
//
//
bool __fastcall TCIISDBModule::LocateMiscValue( TDateTime DateTimeStamp, uint32_t SensorSerialNo , String ValueType  )
{
 int32_t Fraction;

 Variant Args[4];
 Args[0] = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
 Args[1] = Fraction;
 Args[2] = SensorSerialNo;
 Args[3] = ValueType;

 if (MiscValueInserted)
 {
   MiscValueInserted = false;
   FDTableValuesMisc->Refresh();
 }

 return FDTableValuesMisc->Locate("DateTimeStamp;Fraction;SensorSerialNo;ValueType", VarArrayOf( Args, 3 ), NoOpts );
}
//---------------------------------------------------------------------------
//
// Description:
// Move cursor to the next misc value record
//
bool __fastcall TCIISDBModule::FindNextMiscValue()
{
 return FDTableValuesMisc->FindNext();
}
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a record in the ValuesMisc table
//
// Comment: Note !! The argument AutoInc will move cursor to the next recording
// in the local dataset before return.
// Returns true if next value is available in the local dataset
// else returns false.
//
 bool __fastcall TCIISDBModule::GetMiscValueRec( CIISMiscValueRec *MValRec, bool AutoInc )
{
 MValRec->DateTimeStamp = ReadDateTime(FDTableValuesMisc,"DateTimeStamp") +
		  TDateTime( One_mS * FDTableValuesMisc->FieldByName("Fraction")->AsInteger );
 MValRec->SensorSerialNo = FDTableValuesMisc->FieldByName( "SensorSerialNo" )->AsInteger;
 MValRec->ValueType = FDTableValuesMisc->FieldByName( "ValueType" )->AsString;
 MValRec->ValueUnit = FDTableValuesMisc->FieldByName( "ValueUnit" )->AsString;
 MValRec->Value = FDTableValuesMisc->FieldByName("Value")->AsFloat;

 if( AutoInc ) return FDTableValuesMisc->FindNext();
 else return true;
}

//---------------------------------------------------------------------------
//
// Description:
// Append a new misc value record and edit the fields and post the changes
// Returns true if post was successful.
//
// Comment: Note !! It is normally(for SQL db) also necessary to apply the updates
// to the database in order to save them permanently in the database.
//
// Call ApplyUpdatesMiscValue() in order to apply the changes.
//
bool __fastcall TCIISDBModule::AppendMiscValueRec( CIISMiscValueRec *MValRec)
{
 TDateTime ValueTimeStamp=MValRec->DateTimeStamp;

 if (	ValueTimeStamp <= LastValueMiscTimeStamp )
	ValueTimeStamp = LastValueMiscTimeStamp + (TDateTime)One_mS;

 LastValueMiscTimeStamp = ValueTimeStamp;

 int32_t Fraction;
 TDateTime TimestampWithoutMilliSecs = DateTimeNoMilliSecs(ValueTimeStamp,&Fraction);

 ClientDataSetValuesMisc->Append();
 WriteDateTime(ClientDataSetValuesMisc,"DateTimeStamp",TimestampWithoutMilliSecs);
 ClientDataSetValuesMisc->FieldValues["Fraction"] =  Fraction;
 ClientDataSetValuesMisc->FieldValues["SensorSerialNo"] =  MValRec->SensorSerialNo;
 ClientDataSetValuesMisc->FieldValues["ValueType"] =  MValRec->ValueType;
 ClientDataSetValuesMisc->FieldValues["ValueUnit"] =  MValRec->ValueUnit;
 ClientDataSetValuesMisc->FieldValues["Value"] =  MValRec->Value;
 bool PostOk;
 try
  {
   ClientDataSetValuesMisc->Post();
   PostOk=true;
  }
 catch (const Exception &E)
  {
   PostOk = false;
   Debug(Name + "::AppendMiscValueRec() " + E.Message);
   ClientDataSetValuesMisc->Cancel();
  }

 return PostOk;
}
//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to miscellaneous values table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesMiscValue(int32_t ValuesMiscSize)
{
/*
  int32_t NoOfErrors=FDTableValuesMisc->ApplyUpdates();
  FDTableValuesMisc->CommitUpdates();
*/
  InsertedMiscValues += ClientDataSetValuesMisc->RecordCount;
  MiscValueInserted = true;
  bool Result = SQLApplyInserts("valuesmisc",ClientDataSetValuesMisc);

  if (Result)
  {
	// No need to check table size each scan
	if (InsertedMiscValues > 1000)
	{
	  // Delete the oldest values when table is too large
	  FDTableValuesMisc->Refresh();
	  // Table size only checked once in 1000 rows
	  if ( FDTableValuesMisc->RecordCount >= (ValuesMiscSize - 1000) )
	  {
		 // Delete 2000 rows with one SQL-command
		DeleteOldMiscValues();
		FDTableValuesMisc->Refresh();
	  }
	  InsertedMiscValues = 0;
    }

    return true;
  }
 else
  {
    Debug(Name + "::ApplyUpdatesMiscValue() failed");
    return false;
  }

}

void __fastcall TCIISDBModule::ClearSensorMiscValues(uint32_t SensorSerialNo)
{
    FDTableValuesMisc->Filter="SensorSerialNo = " + IntToStr((int32_t)SensorSerialNo);
    FDTableValuesMisc->Filtered =true;

    while (FDTableValuesMisc->RecordCount)
    {
        FDTableValuesMisc->First();
        FDTableValuesMisc->Delete();
    }

    FDTableValuesMisc->ApplyUpdates();
    FDTableValuesMisc->CommitUpdates();

    FDTableValuesMisc->Filter="";
    FDTableValuesMisc->Filtered =false;
}

//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to decay values table
//
// Comment: returns false if at least one pending update failed
//
bool	__fastcall TCIISDBModule::ApplyUpdatesDecayValue(void)
{
/*
 int32_t NoOfErrors=TabValuesDecayAppend->ApplyUpdates(-1);
// TabValuesDecay->Refresh();
 if (NoOfErrors)
  {
   Debug(Name + "::ApplyUpdatesDecayValue() " + "No of errors=" + IntToStr(NoOfErrors) );
   return false;
  }
 else
  {
   TDateTime TimeNow=Now();

   if (((double)(TimeNow - ValuesDecayAppendStartDateTime) > (1/24.0)) || ForceNewValuesDecayAppendStartDateTime)
	{  	// Refresh query if older than 1 hour
		// in order to not to waste memory
	 TabValuesDecayAppend->Active=false;

	 TabValuesDecayAppend->DataSet->CommandText =
	 String("select * from valuesdecay where DateTimeStamp > ") + TodayThisSecSQLStr()
			+ String(";");
	 ValuesDecayAppendStartDateTime=TimeNow;

	 TabValuesDecayAppend->Active=true;
	 ForceNewValuesDecayAppendStartDateTime = false;
	}

   return true;
  }
  */

    bool Result = SQLApplyInserts("valuesdecay",TabValuesDecayAppend);

    return Result;

}

//---------------------------------------------------------------------------
//
// Description:
// Get the total number of records in the ValuesCalc table
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetCalcValuesRecCount()
{
 return FDTableValuesCalc->RecordCount;
}
//---------------------------------------------------------------------------
//
// Description:
// Get the number of records in the ValuesCalc table after a specified value
// The method used here is efficient on large tables and does not require
// the entire table to be read to a local dataset.
//
// Comment:
//
uint32_t __fastcall TCIISDBModule::GetCalcValuesRecCountAfter(CIISCalcValueRec *MValRec)
{
 uint32_t Count=0;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append(String("SELECT COUNT(*) FROM valuescalc WHERE DateTimeStamp > ")
					+ DateTimeToSQLStr(MValRec->DateTimeStamp) + String(";"));

 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Count=FDQuery->FieldByName("COUNT(*)")->AsInteger;
  }
 FDQuery->Active=false;
 return Count;
}
//---------------------------------------------------------------------------
//
// Description:
// Locate the first miscellaneous value in table
//
// Comment:
//
bool __fastcall TCIISDBModule::FindFirstCalcValue()
{
  return FDTableValuesCalc->FindFirst();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate the last miscellaneous value in table
//
// Comment:
//
bool __fastcall TCIISDBModule::FindLastCalcValue()
{
  return FDTableValuesCalc->FindLast();
}

//---------------------------------------------------------------------------
//
// Description:
// Locate a record in the ValuesCalc table
//
//

	bool __fastcall TCIISDBModule::LocateCalcValue( TDateTime DateTimeStamp, uint32_t RecNo,
											uint32_t SensorSerialNo , String ValueType  )
	{
	 int32_t Fraction;

	 Variant Args[5];
	 Args[0] = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
	 Args[1] = Fraction;
	 Args[2] = RecNo;
	 Args[3] = SensorSerialNo;
	 Args[4] = ValueType;

	 return FDTableValuesCalc->Locate("DateTimeStamp;Fraction;RecNo;SensorSerialNo;ValueType", VarArrayOf( Args, 4 ), NoOpts );
	}

/*
bool __fastcall TCIISDBModule::LocateCalcValue( TDateTime DateTimeStamp, )
{
 int32_t Fraction;

 Variant Args[2];
 Args[0] = DateTimeNoMilliSecs(DateTimeStamp,&Fraction);
 Args[1] = Fraction;

 return FDTableValuesCalc->Locate("DateTimeStamp;Fraction", VarArrayOf( Args, 1 ), NoOpts );
}
*/
//---------------------------------------------------------------------------
//
// Description:
// Move cursor to the next calc value record
//
bool __fastcall TCIISDBModule::FindNextCalcValue()
{
 return FDTableValuesCalc->FindNext();
}
//---------------------------------------------------------------------------
//
// Description:
// Read all properties(fields) of a record in the ValuesCalc table
//
// Comment: Note !! The argument AutoInc will move cursor to the next recording
// in the local dataset before return.
// Returns true if next value is available in the local dataset
// else returns false.
//
 bool __fastcall TCIISDBModule::GetCalcValueRec( CIISCalcValueRec *MValRec, bool AutoInc )
{
 MValRec->DateTimeStamp = ReadDateTime(FDTableValuesCalc,"DateTimeStamp") +
		  TDateTime( One_mS * FDTableValuesCalc->FieldByName("Fraction")->AsInteger );
 MValRec->RecNo = FDTableValuesCalc->FieldByName( "RecNo" )->AsInteger;
 MValRec->SensorSerialNo = FDTableValuesCalc->FieldByName( "SensorSerialNo" )->AsInteger;
 MValRec->ValueType = FDTableValuesCalc->FieldByName( "ValueType" )->AsString;
 MValRec->ValueUnit = FDTableValuesCalc->FieldByName( "ValueUnit" )->AsString;
 MValRec->Value = FDTableValuesCalc->FieldByName("Value")->AsFloat;
 MValRec->Code = FDTableValuesCalc->FieldByName("Code")->AsInteger;

 if( AutoInc ) return FDTableValuesCalc->FindNext();
 else return true;
}

bool __fastcall TCIISDBModule::AppendCalcValueRec( CIISCalcValueRec *CalcValRec)
{
	TDateTime ValueTimeStamp, TimestampWithoutMilliSecs;
	int32_t Fraction;

	ValueTimeStamp = CalcValRec->DateTimeStamp;
	if (	ValueTimeStamp <= LastValueCalcTimeStamp ) ValueTimeStamp = LastValueCalcTimeStamp + (TDateTime)One_mS;
	LastValueCalcTimeStamp = ValueTimeStamp;

	TimestampWithoutMilliSecs = DateTimeNoMilliSecs(ValueTimeStamp,&Fraction);

	FDTableValuesCalc->Append();
	WriteDateTime(FDTableValuesCalc,"DateTimeStamp",TimestampWithoutMilliSecs);
	FDTableValuesCalc->FieldValues["Fraction"] =  Fraction;
	FDTableValuesCalc->FieldValues["RecNo"] =  CalcValRec->RecNo;
	FDTableValuesCalc->FieldValues["SensorSerialNo"] =  CalcValRec->SensorSerialNo;
	FDTableValuesCalc->FieldValues["ValueType"] =  CalcValRec->ValueType;
	FDTableValuesCalc->FieldValues["ValueUnit"] =  CalcValRec->ValueUnit;
	FDTableValuesCalc->FieldValues["Value"] =  CalcValRec->Value;
	FDTableValuesCalc->FieldValues["Code"] =  CalcValRec->Code;
	bool PostOk;
	try
	{
		FDTableValuesCalc->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendCalcValueRec() " + E.Message);
		FDTableValuesCalc->Cancel();
	}
	return PostOk;
}

bool __fastcall TCIISDBModule::AppendCalcValueRec_NoTimeAdj( CIISCalcValueRec *CalcValRec)
{
	int32_t Fraction;
	TDateTime TimestampWithoutMilliSecs;

	TimestampWithoutMilliSecs = DateTimeNoMilliSecs(CalcValRec->DateTimeStamp,&Fraction);

	FDTableValuesCalc->Append();
	WriteDateTime(FDTableValuesCalc,"DateTimeStamp",TimestampWithoutMilliSecs);
	FDTableValuesCalc->FieldValues["Fraction"] =  Fraction;
	FDTableValuesCalc->FieldValues["RecNo"] =  CalcValRec->RecNo;
	FDTableValuesCalc->FieldValues["SensorSerialNo"] =  CalcValRec->SensorSerialNo;
	FDTableValuesCalc->FieldValues["ValueType"] =  CalcValRec->ValueType;
	FDTableValuesCalc->FieldValues["ValueUnit"] =  CalcValRec->ValueUnit;
	FDTableValuesCalc->FieldValues["Value"] =  CalcValRec->Value;
	FDTableValuesCalc->FieldValues["Code"] =  CalcValRec->Code;
	bool PostOk;
	try
	{
		FDTableValuesCalc->Post();
		PostOk=true;
	}
	catch (const Exception &E)
	{
		PostOk = false;
		Debug(Name + "::AppendCalcValueRec() " + E.Message);
		FDTableValuesCalc->Cancel();
	}
	return PostOk;
}



//---------------------------------------------------------------------------
//
// Description:
// Apply pending updates to miscellaneous values table
//
// Comment: returns false if at least one pending update failed
//
#define CalcValueCloseReopenPeriod 100
bool	__fastcall TCIISDBModule::ApplyUpdatesCalcValue(void)
{
 int32_t NoOfErrors=FDTableValuesCalc->ApplyUpdates();
 FDTableValuesCalc->CommitUpdates();

 if (NoOfErrors)
	{
	 Debug(Name + "::ApplyUpdatesCalcValue() " + "No of errors=" + IntToStr(NoOfErrors) );
	 return false;
	}
 else
	{
	 return true;
	}
}



//
// Translate ClientDataSet field type to SQL Field Type string
//
//---------------------------------------------------------------------------
String __fastcall TCIISDBModule::SQLFieldTypeString(enum TFieldType FieldType,int32_t Size)
{
 String FieldTypeString="";
 switch (FieldType)
  {
	 case ftString:
		FieldTypeString = "VARCHAR(" + IntToStr(Size) + ")";
		break;
	 case ftSmallint:
		FieldTypeString = "SMALLINT";
		break;
	 case ftInteger:
		FieldTypeString = "INTEGER";
		break;
	 case ftWord:
		FieldTypeString = "SMALLINT UNSIGNED";
		break;
	 case ftBoolean:
		FieldTypeString = "BOOLEAN";
		break;
	 case ftFloat:
		FieldTypeString = "REAL";
		break;
	 case ftDate:
		FieldTypeString = "DATE";
		break;
	 case ftTime:
		FieldTypeString = "TIME";
		break;
	 case ftDateTime:
		FieldTypeString = "TIMESTAMP DEFAULT '1980-01-01 00:00:00'" ;
		break;
	 case ftMemo:
		FieldTypeString = "TEXT";
		break;

	 default:
		ShowMessage("Unknown datatype:"+IntToStr(FieldType));
	   ;
  }
 return FieldTypeString;
}
//
// Create SQL-string to use when creating a MySQL-table
//
//---------------------------------------------------------------------------
String __fastcall TCIISDBModule::GenerateSQLCreateTable(String DBName,String TableName,
					TClientDataSet *TableSrc)
{
 String SQLString = "CREATE TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` (";

 for (int32_t i=0;i < TableSrc->FieldDefs->Count;i++)
  {
   if (i > 0)
	{
	 SQLString += ", ";
	}
   String FieldStr = "`" + TableSrc->FieldDefs->Items[i]->Name + "` ";
   FieldStr += SQLFieldTypeString(TableSrc->FieldDefs->Items[i]->DataType,TableSrc->FieldDefs->Items[i]->Size);

   SQLString += FieldStr;
  }

 TableSrc->IndexDefs->Update();
 if (TableSrc->IndexDefs->Count)
  {
   AnsiString IndexFields=TableSrc->IndexDefs->Items[0]->Fields;

   int32_t SemiColonPos=IndexFields.Pos(";");
   while (SemiColonPos)
	{
	 IndexFields.Delete(SemiColonPos,1);
	 IndexFields.Insert("`,`",SemiColonPos);
	 SemiColonPos=IndexFields.Pos(";");
	}

   SQLString += ", PRIMARY KEY (`" + IndexFields + "`)";
  }

 SQLString += ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";

 return SQLString;
}
//
// Create SQL-string to use when creating a MySQL-table
//
//---------------------------------------------------------------------------
String __fastcall TCIISDBModule::GenerateSQLCreateTable(String DBName,String TableName,
					TFDDataSet *TableSrc)
{
 String SQLString = "CREATE TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` (";

 for (int32_t i=0;i < TableSrc->FieldDefs->Count;i++)
  {
   if (i > 0)
	{
	 SQLString += ", ";
	}
   String FieldStr = "`" + TableSrc->FieldDefs->Items[i]->Name + "` ";
   FieldStr += SQLFieldTypeString(TableSrc->FieldDefs->Items[i]->DataType,TableSrc->FieldDefs->Items[i]->Size);

   SQLString += FieldStr;
  }

 AnsiString IndexFields=TableSrc->IndexFieldNames;
 if (IndexFields.Length())
  {
   int32_t SemiColonPos=IndexFields.Pos(";");
   while (SemiColonPos)
	{
	 IndexFields.Delete(SemiColonPos,1);
	 IndexFields.Insert("`,`",SemiColonPos);
	 SemiColonPos=IndexFields.Pos(";");
	}

   SQLString += ", PRIMARY KEY (`" + IndexFields + "`)";
  }

 SQLString += ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";

 return SQLString;
}
//---------------------------------------------------------------------------
//
// Delete SQL-table
//---------------------------------------------------------------------------
bool __fastcall TCIISDBModule::DeleteSQLTable(TFDConnection *SQLConnection,String DBName,String TableName)
{
 String SQLString = "DROP TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "`;";

 if (SQLConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}
//---------------------------------------------------------------------------
//
// SQL-table exists?
//
bool __fastcall TCIISDBModule::SQLTableExists(String DBName,String TableName)
{
 bool Exists=false;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SHOW TABLES IN " + DBName.LowerCase() + " LIKE '" + TableName.LowerCase() + "';");
 FDQuery->Active=true;
 if (FDQuery->RecordCount == 1)
  {
   Exists=true;
  }
 FDQuery->Active=false;
 return Exists;
}

//---------------------------------------------------------------------------
//
// SQL-field exists?
//
// E.g. SELECT `DecayDuration` FROM `ciislogger`.`controllers` Limit 1;
// in order to check if there is a field called DecayDuration
//
bool __fastcall TCIISDBModule::SQLFieldExists(String DBName,String TableName,String FieldName)
{
 bool Exists=true;
 FDQuery->Active=false;
 FDQuery->SQL->Clear();
 FDQuery->SQL->Append("SELECT `" + FieldName + "` FROM `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` Limit 1;");
 try
  {
   FDQuery->Active=true;
  }
 catch (...)
  {
   Exists=false;
  }

 FDQuery->Active=false;

 return Exists;
}
//---------------------------------------------------------------------------
//
// SQL- Add new field
//
// e.g. ALTER TABLE `ciislogger`.`controllers` ADD `DecayDelay` INTEGER;
//
bool __fastcall TCIISDBModule::SQLAddField(String DBName,String TableName,String FieldName,enum TFieldType FieldType,String DefaultExpr,int32_t Size,String AfterField)
{
 String FieldTypeString=SQLFieldTypeString(FieldType,Size);
 AnsiString DefaultStr=DefaultExpr.Length() ? (" DEFAULT " + DefaultExpr) : String("");
 String SQLString = "ALTER TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` ADD `" + FieldName + "` " + FieldTypeString + DefaultStr;

 if (AfterField.Length() > 0)
  {
   SQLString += " AFTER " + AfterField + ";";
  }
 else
  SQLString += ";";

 if (FDConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}

bool __fastcall TCIISDBModule::SQLUpdateField(String DBName,String TableName, String DestFieldName , String OrgFieldName)
{
 String SQLString = "UPDATE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` SET `" + DestFieldName + "`=`" + OrgFieldName;

 if (FDConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}


//---------------------------------------------------------------------------
//
// SQL - Drop Primary key
//
// e.g. ALTER TABLE `ciislogger`.`controllers` DROP PRIMARY KEY;
//
bool __fastcall TCIISDBModule::SQLDropPrimaryKey(String DBName,String TableName)
{
 String SQLString = "ALTER TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` DROP PRIMARY KEY;";

 if (FDConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}
//---------------------------------------------------------------------------
//
// SQL - Add Primary key
//
// e.g. ALTER TABLE `ciislogger`.`controllers` ADD PRIMARY KEY (KeyFields);
//
bool __fastcall TCIISDBModule::SQLAddPrimaryKey(String DBName,String TableName,String KeyFields)
{
 String SQLString = "ALTER TABLE `" + DBName.LowerCase() + "`.`" + TableName.LowerCase() + "` ADD PRIMARY KEY (" + KeyFields + ");";

 if (FDConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}

//---------------------------------------------------------------------------
//
// Description:
// Create SQL Database
//
// Comment: returns true if db created Ok
//
//
bool __fastcall TCIISDBModule::CreateSQLDatabase(TFDConnection *SQLConnection,String DBName)
{
 String SQLString = "CREATE DATABASE IF NOT EXISTS `" + DBName.LowerCase() + "`;";

 if (SQLConnection->ExecSQL(SQLString) == 0)
  return true;
 else
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TCIISDBModule::Debug(String Message)
{
 #if DebugDB == 1
 if ( DW != NULL )
   DW->Log(Message);
 #endif
}
//---------------------------------------------------------------------------
void __fastcall TCIISDBModule::Debug_AlwOn(String Message)
{

 if ( DW != NULL )
   DW->Log(Message);
}
//---------------------------------------------------------------------------
// Read/Write Date / DateTime
//---------------------------------------------------------------------------
void __fastcall TCIISDBModule::WriteDate(TDataSet *ClientDataSet,String FieldName,TDate Value)
{
 try
  {
   ClientDataSet->FieldValues[FieldName]=Value;
  }
 catch (const Exception &E)
  {
   Debug(Name + "::WriteDate() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
  }
}
//---------------------------------------------------------------------------
void __fastcall TCIISDBModule::WriteDateTime(TDataSet *ClientDataSet,String FieldName,TDateTime Value)
{
 try
  {
   ClientDataSet->FieldValues[FieldName]=Value;
  }
 catch (const Exception &E)
  {
   Debug(Name + "::WriteDateTime() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
  }
}

//---------------------------------------------------------------------------
TDate __fastcall TCIISDBModule::ReadDate(TDataSet *ClientDataSet,String FieldName)
{
 TDate Value;
 try
  {
   Value = ClientDataSet->FieldByName(FieldName)->AsDateTime;
  }
 catch (const Exception &E)
  {
   Value=TDateTime(1980,1,1);
   Debug(Name + "::ReadDate() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
  }
 return Value;
}
//---------------------------------------------------------------------------
TDateTime __fastcall TCIISDBModule::ReadDateTime(TDataSet *ClientDataSet,String FieldName)
{
 TDateTime Value;
 try
  {
   Value = ClientDataSet->FieldByName(FieldName)->AsDateTime;
  }
 catch (const Exception &E)
  {
   Value=TDateTime(1980,1,1,0,0,0,0);
   Debug(Name + "::ReadDateTime() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
  }
 return Value;
}

//---------------------------------------------------------------------------
// Read/Write Bool
//---------------------------------------------------------------------------
void __fastcall TCIISDBModule::WriteBool(TDataSet *ClientDataSet,String FieldName,bool Value)
{
 bool WriteOK=false;

/*  Write bool as integer. True == -1, False == 0
 try
  {
   ClientDataSet->FieldByName(FieldName)->AsBoolean=Value;
   WriteOK=true;
  }
 catch (const Exception &E)
  {
   Debug(Name + "::WriteBool() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
   WriteOK=false;
  }
 */
 if (!WriteOK)
  {
   try
   {
      ClientDataSet->FieldByName(FieldName)->AsInteger= Value ? -1:0;
   }
   catch (const Exception &E)
   {
      Debug(Name + "::WriteBool() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
   }
  }
}
//---------------------------------------------------------------------------
bool __fastcall TCIISDBModule::ReadBool(TDataSet *ClientDataSet,String FieldName)
{
 bool Value;
 bool ValueOK=false;
 /*     Write bool as integer. True == -1, False == 0
 try
 {
	 Value = ClientDataSet->FieldByName(FieldName)->AsBoolean;
	 ValueOK = true;
 }
 catch (const Exception &E)
 {
	 Debug(Name + "::ReadBool() " + ClientDataSet->Name + " Field:" + FieldName + " " + E.Message);
	 ValueOK=false;
	 Debug(Name + "::ReadBool() will read bool as int32_t");
 }
   */
 if (!ValueOK)
  {
   int32_t IntValue=ClientDataSet->FieldByName(FieldName)->AsInteger;
   if (IntValue != 0)
	Value=true;
   else
	Value=false;
  }
 return Value;
}
//---------------------------------------------------------------------------
//
// Description: Set new debug window
//
void __fastcall TCIISDBModule::SetDebugWin(TDebugWin *SetDebugWin)
{
 DW = SetDebugWin;
}
//---------------------------------------------------------------------------
//
// Returns a timestamp with the millisecs cleared.
// The arg Fraction is updated with the millisecond
//
//
TDateTime __fastcall TCIISDBModule::DateTimeNoMilliSecs(TDateTime DateTime,int32_t *Fraction)
{
		// MySQL
 uint16_t Year,Month,Day;
 uint16_t Hour,Minute,Second,MilliSecond;
 DateTime.DecodeDate(&Year,&Month,&Day);
 DateTime.DecodeTime(&Hour,&Minute,&Second,&MilliSecond);

 *Fraction=MilliSecond;
 return TDateTime(Year,Month,Day,Hour,Minute,Second,0);
}
//---------------------------------------------------------------------------
//
// Assemble a timestamp using a timestamp without millisecs and a fraction (millisecs)
//
//
TDateTime __fastcall TCIISDBModule::AssembleDateTime(TDateTime DateTime,int32_t Fraction)
{
		// MySQL
 uint16_t Year,Month,Day;
 uint16_t Hour,Minute,Second,MilliSecond;
 DateTime.DecodeDate(&Year,&Month,&Day);
 DateTime.DecodeTime(&Hour,&Minute,&Second,&MilliSecond);

 return TDateTime(Year,Month,Day,Hour,Minute,Second,Fraction);
}
//---------------------------------------------------------------------------
//
// Formats a timestamp to a syntax acceptable by MySQL
//
//

String __fastcall TCIISDBModule::DateTimeToSQLStr( TDateTime t )
{
 Word Year, Month, Day, Hour, Min, Sec, MSec;
 String s;

 DecodeDate(t, Year, Month, Day);
 DecodeTime(t, Hour, Min, Sec, MSec);
 s.sprintf(L"'%4d-%02d-%02d %02d:%02d:%02d'",
			Year, Month, Day, Hour, Min, Sec);

 return s;
}
//---------------------------------------------------------------------------
//
// Formats a timestamp to a syntax acceptable by MySQL
//
//

String __fastcall TCIISDBModule::FloatToSQLStr( String FloatAsString )
{
        // In case exponential format
 String s="'" + FloatAsString + "'";
			// Replace any ',' with '.'
 int32_t decimalPos=s.Pos(",");
 if (decimalPos > 0)
 {
 	s[decimalPos] = '.';
 }
 return s;
}
//---------------------------------------------------------------------------
//
// Clears minutes,seconds in timestamp and
// formats to a syntax acceptable by MySQL
//
//

String __fastcall TCIISDBModule::TodayThisHourSQLStr(void)
{
 uint16_t Year, Month, Day, Hour, Dummy;
 String s;

 TDateTime t=Now();

 t.DecodeDate(&Year, &Month, &Day);
 t.DecodeTime(&Hour,&Dummy,&Dummy,&Dummy);
 s.sprintf(L"'%4d-%02d-%02d %02d:00:00'",
			(int32_t) Year,(int32_t) Month,(int32_t) Day,(int32_t) Hour);

 return s;
}
//---------------------------------------------------------------------------
//
// Formats to a syntax acceptable by MySQL
//
//

String __fastcall TCIISDBModule::TodayThisSecSQLStr(void)
{
 uint16_t Year, Month, Day, Hour, Minute, Second, Dummy;
 String s;

 TDateTime t=Now();

 t.DecodeDate(&Year, &Month, &Day);
 t.DecodeTime(&Hour,&Minute,&Second,&Dummy);
 s.sprintf(L"'%4d-%02d-%02d %02d:%02d:%02d'",
			(int32_t) Year,(int32_t) Month,(int32_t) Day,(int32_t) Hour, (int32_t) Minute, (int32_t) Second);

 return s;
}

//---------------------------------------------------------------------------

String __fastcall TCIISDBModule::FieldToSQLStr(TClientDataSet *ds, int32_t fi)
{
	String s;
	switch (ds->FieldDefs->Items[fi]->DataType)
	{
		case ftDateTime:
			s = DateTimeToSQLStr(ReadDateTime(ds,ds->FieldDefs->Items[fi]->Name));
			break;
		case ftFloat:
			s = FloatToSQLStr(ds->Fields->FieldByNumber(fi+1)->AsString);
			break;
		case ftString:
			s = "'" + ds->Fields->FieldByNumber(fi+1)->AsString + "'";
			break;
	default:
			s = ds->Fields->FieldByNumber(fi+1)->AsString;
	}
	return s;
}


//---------------------------------------------------------------------------
// Keep DB connection open with a "dummy" query
//
void __fastcall TCIISDBModule::KeepDB(void)
{
/*
    SimpleDataSet->DataSet->CommandText= "SELECT COUNT(*) FROM project;";
    SimpleDataSet->Active=true;
    SimpleDataSet->Active=false;
*/
	   int32_t ConfigVer = FDTableProject->FieldValues["ConfigVer"];
	   FDTableProject->Edit();
	   FDTableProject->FieldValues["ConfigVer"] = ConfigVer + 1;
	   FDTableProject->Post();
	   FDTableProject->ApplyUpdates();
      FDTableProject->CommitUpdates();

	   FDTableProject->Edit();
	   FDTableProject->FieldValues["ConfigVer"] = ConfigVer;
	   FDTableProject->Post();
	   FDTableProject->ApplyUpdates();
      FDTableProject->CommitUpdates();

}

