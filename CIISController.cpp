//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISController.h"
#include "CIISProject.h"
#include "CIISWLink.h"
#include "CIISBIChInfo.h"
#include "System.IniFiles.hpp"
#include "CIISBusInterface.h"


//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------



__fastcall TCIISController::TCIISController(TCIISDBModule *SetDB,
																						TDebugWin *SetDebugWin,
																						CIISCtrlRec *SetCtrlRec,
																						TObject *SetCIISParent )
						:TCIISObj( SetDB, NULL, SetDebugWin, SetCIISParent )
{
	String IniFilePath;
	TIniFile *pIniFile;

	TCIISZone *Zone;
	CIISZoneRec *ZR;
	TCIISWLink *WLink;
	CIISWLinkRec *WLR;
	bool MoreRecords;

	CIISObjType = CIISController;

	P_Prj = (TCIISProject*)CIISParent;
	P_Ctrl = NULL;
	P_Zone = NULL;

	RestartCIISBusRunning = false;
	StartScheduleRunning = false;
	ResetCIISBusRunning = false;

	FApplyNewRecording = false;
	FApplyNewDecaySample = false;
	FApplyNewMonitorSample = false;
	FApplyNewZone = false;

	OnSlowClockTick = 20;

 	IniFilePath=GetCommonAppFolderPath() + ChangeFileExt(ExtractFileName(Application->ExeName),".Ini");
	pIniFile = new TIniFile(IniFilePath);
	if (pIniFile)
	{
		DecayZoneDelay = pIniFile->ReadInteger("Measurements", "DecayZoneDelay", 0 );
		DisableEthernet = pIniFile->ReadBool("Communication", "DisableEthernet", false );
		MonitorStartDelay = pIniFile->ReadInteger("Schedule", "MonitorStartDelay", 0 );
		MonitorStartDelay = min( MonitorStartDelay, 120);
		delete pIniFile;
	}



	CtrlRec = SetCtrlRec;
	CIISBusInt = new TCIISBusInterface( SetDB, SetDebugWin, this, &IncomingCIIBusMessage, DisableEthernet );
	ChildList->Add( CIISBusInt );


	BPIn = new TCIISBusPacket;

	DB->SetZoneFilter( CtrlRec );
	if( DB->FindFirstZone() )
	{
		do
		{
			ZR = new CIISZoneRec;
			MoreRecords = DB->GetZoneRec( ZR, true );
			//ZR->ZoneCanAdr = CIISBusInt->GetZonCanAdr();    ZonCanAdr is set att in ResetCIISBus
			if( ZR->uCtrl ) Zone = new TCIISZone_uCtrl(SetDB, CIISBusInt, SetDebugWin, ZR, this, false );
			else
			{
				Zone = new TCIISZone_Online(SetDB, CIISBusInt, SetDebugWin, ZR, this, false );
        Zone->MonitorStartDelay = MonitorStartDelay;
			}
			ChildList->Add( Zone );
		} while ( MoreRecords );
	}
	else
	{
		ZR = NULL;
	}
  DB->ClearZoneFilter();

	DB->SetWLinkFilter( CtrlRec );
  if( DB->FindFirstWLink() )
  {
		do
		{
			WLR = new CIISWLinkRec;
			MoreRecords = DB->GetWLinkRec( WLR, true );

			WLR->WLinkCanAdr = 0;
			WLR->WLinkStatus = 1;
			WLR->WLinkConnected = false;

			WLink = new TCIISWLink(SetDB, CIISBusInt, SetDebugWin, WLR, this );
			ChildList->Add( WLink );
		} while ( MoreRecords );
	}
	else
	{
		WLR = NULL;
  }
	DB->ClearWLinkFilter();

}

__fastcall TCIISController::~TCIISController()
{
	TCIISObj *CIISObj;
	TCIISZone *Zone;

	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Zone = (TCIISZone*)CIISObj;
			delete Zone;
			ChildList->Delete(i);
		}
	}
  delete BPIn;
  delete CIISBusInt;
  delete CtrlRec;
}

void __fastcall TCIISController::TestBI()
{
		CIISBusInt->TestBI();
}

void __fastcall TCIISController::PrepareShutdown()
{
  TCIISObj *CIISObj;
	TCIISZone *Zone;

	CIISBusInt->SetWatchDogEnable( false );

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Zone = (TCIISZone*) CIISObj;
			Zone->PrepareShutdown();
		}
  }
}


void __fastcall TCIISController::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	TCIISObj *Obj;

	#if DebugMsg == 1
	Debug( "Ctrl: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
	{
	case CIISRead:
		if( P->UserLevel != ULNone )
		{
			ReadRec( P, O );
		}
		else
		{
			O->MessageCode = CIISMsg_UserLevelError;
		}
		break;

	case CIISWrite:
		if( ThisRec( P ) )
		{
			if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
			{
			WriteRec( P, O );
			}
			else
			{
			O->MessageCode = CIISMsg_UserLevelError;
			}
		}
		break;

	case CIISAppend:
		O->MessageCode = 112;
		P->CommandMode = CIISCmdReady;
		break;

	case CIISDelete:
		if( ThisRec( P ) )
		{
			if( P->UserLevel == ULErase )
			{
				if( ChildList->Count == 0 && CtrlRec->USBCANStatus == 0 &&  DB->DeleteCtrlRec( CtrlRec ) )
				{
					DB->ApplyUpdatesCtrl();
					P_Prj->Log( ClientCom, CIIEventCode_CtrlTabChanged, CIIEventLevel_High, "", 0, 0 );
					P->CommandMode = CIISDeleteCtrl;
					P->CIISObj = this;
					O->MessageCode = CIISMsg_Ok;
				}
				else O->MessageCode = CIISMsg_DeleteError;
			}
			else
			{
				O->MessageCode = CIISMsg_UserLevelError;
			}
		}
		break;

	case CIISLogin:
		break;

	case CIISLogout:
		break;

	case CIISSelectProject:
		break;

	case CIISExitProject:
		break;

	case CIISReqStatus:
		break;

	case CIISReqDBVer:
		break;

	case CIISReqPrgVer:
		break;

	case CIISReqTime:
		break;

	case CIISSetTime:
		break;
  }
}

void __fastcall TCIISController::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
  //if( P->MessageTable == CIISZones  && P->MessageCommand == CIISAppend )
	//P->CommandMode = CIISAddZone;

	#if DebugMsg == 1
	Debug( "Ctrl: ParseCommand_End" );
	#endif

  switch( P->CommandMode )
  {
	case CIISSensorMoved:
	AddSensor( (TCIISSensor*) P->CIISObj );
	P->CommandMode = CIISCmdReady;
	break;

	case CIISPSMoved:
	AddPS( (TCIISPowerSupply*) P->CIISObj );
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAddZone:
	AddZone( P, O );
	break;

	case CIISDeleteZone:
	DeleteZone( P );
	break;

	case CIISDeleteWLink:
	DeleteWLink( P );
	break;

	default:
	break;
  }
}

bool __fastcall TCIISController::SetPrjName( String PrjName )
{
  bool result;
  String CurrentName;

  result = false;
  if( DB->LocateCtrlRec( CtrlRec ) )
  {
	CurrentName = CtrlRec->CtrlName;
	CtrlRec->PrjName = PrjName;
	if( DB->SetCtrlRec( CtrlRec ) )
	{
	  result = true;
	  DB->ApplyUpdatesCtrl();
    }
	else CtrlRec->PrjName = CurrentName;
  }
  return result;
}

bool __fastcall TCIISController::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CtrlRec->CtrlName;
}

void __fastcall TCIISController::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
		if( P->ArgInc( 200 ) )
		{
			if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetCtrl" ) P->CommandMode = CIISRecAdd;
		}
		else P->CommandMode = CIISRecAdd;
  }

  if( P->CommandMode == CIISRecSearch )
  {
		if( ThisRec( P ) )
		{
			P->CommandMode = CIISRecAdd;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_RecNotFound;
  }

	else if( P->CommandMode == CIISRecAdd )
  {
		O->MessageData = O->MessageData +
		"1=" + CtrlRec->CtrlName + "\r\n" +
		"2=" + CIISBoolToStr( CtrlRec->NoIR ) + "\r\n" +
		"3=" + CIISDateTimeToStr( CtrlRec->ScheduleDateTime ) + "\r\n" +
		"4=" + IntToStr( CtrlRec->SchedulePeriod ) + "\r\n" +
		"5=" + IntToStr( CtrlRec->DecaySampInterval ) + "\r\n" +
		"6=" + IntToStr( CtrlRec->DecayDuration ) + "\r\n" +
		"7=" + IntToStr( CtrlRec->LPRRange ) + "\r\n" +
		"8=" + IntToStr( CtrlRec->LPRStep ) + "\r\n" +
		"9=" + IntToStr( CtrlRec->LPRDelay1 ) + "\r\n" +
		"10=" + IntToStr( CtrlRec->LPRDelay2 ) + "\r\n" +
		"11=" + IntToStr( CtrlRec->SampInterval1 ) + "\r\n" +
		"12=" + IntToStr( CtrlRec->SampInterval2 ) + "\r\n" +
		"13=" + IntToStr( CtrlRec->SampInterval3 ) + "\r\n" +
		"14=" + IntToStr( CtrlRec->SampInterval4 ) + "\r\n" +
		"15=" + IntToStr( CtrlRec->SampInterval5 ) + "\r\n" +
		"16=" + IntToStr( CtrlRec->NextCanAdr ) + "\r\n" +
		"17=" + CIISFloatToStr( CtrlRec->DefaultSensorLow ) + "\r\n" +
		"18=" + CIISFloatToStr( CtrlRec->DefaultSensorHigh ) + "\r\n" +
		"19=" + CIISBoolToStr( CtrlRec->DefaultAlarmEnable ) + "\r\n" +
		"20=" + IntToStr( CtrlRec->CtrlAlarmStatus ) + "\r\n" +
		"21=" + IntToStr( CtrlRec->USBCANStatus ) + "\r\n" +
		"22=" + IntToStr( CtrlRec->NodeCount ) + "\r\n" +
		"23=" + IntToStr( CtrlRec->MonitorCount ) + "\r\n" +
		"24=" + IntToStr( CtrlRec->DecayCount ) + "\r\n" +
		"25=" + IntToStr( CtrlRec->LPRCount ) + "\r\n" +
		"26=" + IntToStr( CtrlRec->SchedulePeriodUnit ) + "\r\n" +
		"27=" + CIISBoolToStr( CtrlRec->ScheduleDecay ) + "\r\n" +
		"28=" + CIISBoolToStr( CtrlRec->ScheduleLPR ) + "\r\n" +
		"29=" + IntToStr( CtrlRec->CtrlScheduleStatus ) + "\r\n" +
		"30=" + IntToStr( CtrlRec->LPRMode ) + "\r\n" +
		"31=" + IntToStr( CtrlRec->DecayDelay ) + "\r\n" +
		"32=" + IntToStr( CtrlRec->DecaySampInterval2 ) + "\r\n" +
		"33=" + IntToStr( CtrlRec->DecayDuration2 ) + "\r\n" +

		"34=" + CIISBoolToStr( CtrlRec->WatchDogEnable ) + "\r\n" +
		"35=" + IntToStr( CtrlRec->BusIntSerialNo ) + "\r\n" +
		"36=" + IntToStr( CtrlRec->BusIntVerMajor ) + "\r\n" +
		"37=" + IntToStr( CtrlRec->BusIntVerMinor ) + "\r\n" +
		"38=" + IntToStr( CtrlRec->MonitorExtCount ) + "\r\n" +

		"39=" + CIISBoolToStr( CtrlRec->ScheduleZRA ) + "\r\n" +
		"40=" + CIISBoolToStr( CtrlRec->ScheduleResMes ) + "\r\n" +

		"50=" + CtrlRec->PrjName + "\r\n";

		O->MessageCode = CIISMsg_Ok;

		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
  else
  {
		 O->MessageCode = CIISMsg_UnknownCommand;
		 P->CommandMode = CIISCmdReady;
  }
}

void __fastcall TCIISController::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  TCIISObj *CIISObj;
  TCIISZone *Zone;

  if( DB->LocateCtrlRec( CtrlRec ) )
  {
		if( P->ArgInc(1) )
		{
			CtrlRec->CtrlName = P->GetArg(1);

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( this->CIISObjIs( CIISZones ) )
			{
				Zone = (TCIISZone*)CIISObj;
				Zone->SetCtrlName( P->GetArg(1));

			}
			}
		}
		if( P->ArgInc(2) ) CtrlRec->NoIR = CIISStrToBool( P->GetArg(2) );
		if( P->ArgInc(3) )
		{
			CtrlRec->ScheduleDateTime = CIISStrToDateTime( P->GetArg(3) );
			if( CtrlRec->ScheduleDateTime > Now() ) CtrlRec->CtrlScheduleStatus = 1;
			else CtrlRec->CtrlScheduleStatus = 0;
		}
		if( P->ArgInc(4) ) CtrlRec->SchedulePeriod = StrToInt( P->GetArg(4) );
		if( P->ArgInc(5) ) CtrlRec->DecaySampInterval = StrToInt( P->GetArg(5) );
		if( P->ArgInc(6) ) CtrlRec->DecayDuration = StrToInt( P->GetArg(6) );
		if( P->ArgInc(7) ) CtrlRec->LPRRange = StrToInt( P->GetArg(7) );
		if( P->ArgInc(8) ) CtrlRec->LPRStep = StrToInt( P->GetArg(8) );
		if( P->ArgInc(9) ) CtrlRec->LPRDelay1 = StrToInt( P->GetArg(9) );
		if( P->ArgInc(10) ) CtrlRec->LPRDelay2 = StrToInt( P->GetArg(10) );
		if( P->ArgInc(11) ) CtrlRec->SampInterval1 = StrToInt( P->GetArg(11) );
		if( P->ArgInc(12) ) CtrlRec->SampInterval2 = StrToInt( P->GetArg(12) );
		if( P->ArgInc(13) ) CtrlRec->SampInterval3 = StrToInt( P->GetArg(13) );
		if( P->ArgInc(14) ) CtrlRec->SampInterval4 = StrToInt( P->GetArg(14) );
		if( P->ArgInc(15) ) CtrlRec->SampInterval5 = StrToInt( P->GetArg(15) );
		if( P->ArgInc(16) ) CtrlRec->DefaultSensorLow = CIISStrToFloat( P->GetArg(16) );
		if( P->ArgInc(17) ) CtrlRec->DefaultSensorHigh = CIISStrToFloat( P->GetArg(17) );
		if( P->ArgInc(18) ) CtrlRec->DefaultAlarmEnable = CIISStrToBool( P->GetArg(18) );
		if( P->ArgInc(19) ) CtrlRec->SchedulePeriodUnit = StrToInt( P->GetArg(19) );
		if( P->ArgInc(20) ) CtrlRec->ScheduleDecay = CIISStrToBool( P->GetArg(20) );
		if( P->ArgInc(21) ) CtrlRec->ScheduleLPR = CIISStrToBool( P->GetArg(21) );
		if( P->ArgInc(22) ) CtrlRec->LPRMode = StrToInt( P->GetArg(22) );
		if( P->ArgInc(23) ) CtrlRec->DecayDelay = StrToInt( P->GetArg(23) );
		if( P->ArgInc(24) ) CtrlRec->DecaySampInterval2 = StrToInt( P->GetArg(24) );
		if( P->ArgInc(25) ) CtrlRec->DecayDuration2 = StrToInt( P->GetArg(25) );

		if( P->ArgInc(26) ) CtrlRec->WatchDogEnable = CIISStrToBool( P->GetArg(26) );
		if( P->ArgInc(27) ) CtrlRec->BusIntSerialNo = StrToInt( P->GetArg(27) );
		if( P->ArgInc(28) ) CtrlRec->BusIntVerMajor = StrToInt( P->GetArg(28) );
		if( P->ArgInc(29) ) CtrlRec->BusIntVerMinor = StrToInt( P->GetArg(29) );
		if( P->ArgInc(30) ) CtrlRec->CtrlScheduleStatus = StrToInt( P->GetArg(30) );

		if( P->ArgInc(31) ) CtrlRec->ScheduleZRA = CIISStrToBool( P->GetArg(31) );
		if( P->ArgInc(32) ) CtrlRec->ScheduleResMes = CIISStrToBool( P->GetArg(32) );

		DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
		P_Prj->Log( ClientCom, CIIEventCode_CtrlTabChanged, CIIEventLevel_High, "", 0, 0 );
		O->MessageCode = CIISMsg_Ok;

		//if( P->ArgInc(3) ) NetInt->UpdateScheduleDateTime();

		if( P->ArgInc(26) )
		{
		 //CIISBusInt->SetWatchDogTime( WatchDogTime );
		 CIISBusInt->SetWatchDogEnable( CtrlRec->WatchDogEnable );
		}

		if( P->ArgInc(200) )
		{
			//if( P->GetArg(200) == "Reset CANNet" ) NetInt->ResetCanNet();
			//if( P->GetArg(200) == "Update LastValue" ) NetInt->UpdateLastValue( 0, 0 );
			if( P->GetArg(200) == "Update LastValue" ) this->UpdateLastValues();
			else if( P->GetArg(200) == "Reset CAN Alarms" ) this->ResetCANAlarm();
		}
	}
  else O->MessageCode = CIISMsg_RecNotFound;
  P->CommandMode = CIISCmdReady;
}

void __fastcall TCIISController::AddNode( TCIISNodeIni *Node )
{
  TCIISObj *CIISObj;
  TCIISZone *Z;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISZones ) )
	{
	  Z = (TCIISZone*)ChildList->Items[i];
	  if( Z->ZoneNo == 1 )
	  {
			Z->AddChild( Node );
			break;
	  }
	}
  }
}

void __fastcall TCIISController::AddSensor( TCIISSensor *Sensor )
{
	TCIISObj *CIISObj;
  TCIISZone *Z;
  int32_t SensorZoneNo;
  bool ZoneFound;

  ZoneFound = false;
  SensorZoneNo = Sensor->ZoneNo;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Z = (TCIISZone*)ChildList->Items[i];
			if( Z->ZoneNo == SensorZoneNo )
			{
				Z->AddChild( Sensor );
				ZoneFound = true;
				break;
			}
		}
	}
  if( !ZoneFound )
	{
		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISZones ) )
			{
				Z = (TCIISZone*)ChildList->Items[i];
				if( Z->ZoneNo == 1 )
				{
					Z->AddChild( Sensor );
					break;
				}
			}
		}
  }
}

void __fastcall TCIISController::AddPS( TCIISPowerSupply *PS )
{
  TCIISObj *CIISObj;
  TCIISZone *Z;
  int32_t PSZoneNo;
  bool ZoneFound;

  ZoneFound = false;
  PSZoneNo = PS->ZoneNo;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISZones ) )
	{
	  Z = (TCIISZone*)ChildList->Items[i];
	  if( Z->ZoneNo == PSZoneNo )
	  {
		Z->AddChild( PS );
		ZoneFound = true;
		break;
	  }
	}
  }
  if( !ZoneFound )
  {
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
	  CIISObj = (TCIISObj*) ChildList->Items[i];
	  if( CIISObj->CIISObjIs( CIISZones ) )
	  {
		Z = (TCIISZone*)ChildList->Items[i];
		if( Z->ZoneNo == 1 )
		{
		  Z->AddChild( PS );
		  break;
		}
	  }
	}
	}
}

void __fastcall TCIISController::AddZone( TCamurPacket *P, TCamurPacket *O )
{
	TCIISZone *Zone;
	CIISZoneRec *ZR;

	ZR = new CIISZoneRec;

	ZR->ZoneName = "New Zone";
	ZR->AnodeArea = 0;
	ZR->CathodeArea = 0;
	ZR->Comment = " ";
	ZR->ZoneSampInterval = 3;
	ZR->RecType = 0;
	ZR->RecNo = 0;
	ZR->ZoneAlarmStatus = 0;
	ZR->ZoneScheduleStatus = 0;
	ZR->CtrlName = "Controller 1";
	ZR->IncludeInSchedule = true;
	ZR->uCtrl = false;
	ZR->uCtrlConnected = false;
	ZR->PSOffOnAlarm = false;
	ZR->PSOffHold = false;

	ZR->ZoneCanAdr = CIISBusInt->GetZonCanAdr();

	if( P->ArgInc(2) ) ZR->ZoneName = P->GetArg(2);
	if( P->ArgInc(3) ) ZR->AnodeArea = CIISStrToFloat(P->GetArg(3));
	if( P->ArgInc(4) ) ZR->CathodeArea = CIISStrToFloat(P->GetArg(4));
	if( P->ArgInc(5) ) ZR->Comment = P->GetArg(5);
	if( P->ArgInc(6) ) ZR->ZoneSampInterval = StrToInt(P->GetArg(6));
	if( P->ArgInc(7) ) ZR->ZoneScheduleStatus = StrToInt(P->GetArg(7));
	if( P->ArgInc(50) ) ZR->CtrlName = P->GetArg(50);

	if( DB->AppendZoneRec( ZR, true ))
	{
		DB->ApplyUpdatesZone();
		P_Prj->Log( ClientCom, CIIEventCode_ZoneTabChanged, CIIEventLevel_High, "", 0, 0 );
		O->MessageCode = CIISMsg_Ok;
		Zone = new TCIISZone_Online( DB, CIISBusInt, DW, ZR, this, true );
		ChildList->Add( Zone );
	}
	else O->MessageCode = CIISDBError;
	P->CommandMode = CIISCmdReady;
}

void __fastcall TCIISController::DeleteZone( TCamurPacket *P )
{
  TCIISObj *CIISObj;
  TCIISZone *ZonDelete;

  ZonDelete = (TCIISZone*) P->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISZones ) )
	{
	  if( ZonDelete == CIISObj )
	  {
		ChildList->Delete(i);
		delete ZonDelete;
		break;
	  }
	}
  }
}

void __fastcall TCIISController::DeleteWLink( TCamurPacket *P )
{
  TCIISObj *CIISObj;
  TCIISWLink *WLinkDelete;

  WLinkDelete = (TCIISWLink*) P->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISWLink ) )
	{
	  if( WLinkDelete == CIISObj )
	  {
		ChildList->Delete(i);
		delete WLinkDelete;
		break;
	  }
	}
  }
}

// CIIBus

void __fastcall TCIISController::IncomingCIIBusMessage(const Byte *CIIBusMsg, PVOID BIChannel)
{
	BPIn->Byte0 = CIIBusMsg[0];
  BPIn->Byte1 = CIIBusMsg[1];
  BPIn->Byte2 = CIIBusMsg[2];
  BPIn->Byte3 = CIIBusMsg[3];
  BPIn->Byte4 = CIIBusMsg[4];
  BPIn->Byte5 = CIIBusMsg[5];
  BPIn->Byte6 = CIIBusMsg[6];
  BPIn->Byte7 = CIIBusMsg[7];

  BPIn->ParseMode = CIISParseCont;

  BPIn->SetBIChannel( BIChannel );

	#if DebugCIISBusInt == 1

		TCIISBIChannel * BICh;
		BICh = (TCIISBIChannel*) BIChannel;

	if( BPIn->MessageCommand == MSG_TX_IDENTIFY )
	{

		Debug( "IDENTIFY from SNR: " + IntToStr( BPIn->SerNo ) + " BI: " + IntToStr( BICh->BusInterfaceSerialNo ));
	}
	else
	{
//		Debug( "Incoming BusMessage / MCommand:" + IntToStr( BPIn->MessageCommand ) + " / CANAdr:" + IntToStr( BPIn->CANAdr ) +
//			 " / Tag:" + IntToStr( BPIn->Tag ) );

		Debug( "RX BI" + IntToStr( BICh->BusInterfaceSerialNo ) +
					 "/" + IntToStr( BPIn->CANAdr ) +
					 " [" + IntToStr( CIIBusMsg[0]) +
					 ", " + IntToStr( CIIBusMsg[1]) +
					 ", " + IntToStr( CIIBusMsg[2]) +
					 ", " + IntToStr( CIIBusMsg[3]) +
					 ", " + IntToStr( CIIBusMsg[4]) +
					 ", " + IntToStr( CIIBusMsg[5]) +
					 ", " + IntToStr( CIIBusMsg[6]) +
					 ", " + IntToStr( CIIBusMsg[7]) +
					 " Cmd:" + IntToStr( BPIn->MessageCommand ) +
					 " / Tag:" + IntToStr( BPIn->Tag ) );

	}
	#endif


  this->ParseCIIBusMessage( BPIn );
}

void __fastcall TCIISController::OnSysClockTick( TDateTime TickTime )
{
	if( ResetCIISBusRunning ) SM_ResetCIISBus();
	else if( RestartCIISBusRunning ) SM_RestartCIISBus();
	else
	{
		if( StartScheduleRunning ) SM_StartSchedule();

		OnSlowClockTick--;
		if( OnSlowClockTick < 0 )
		{
			OnSlowClockTick = 20;
			if( !StartScheduleRunning && CtrlRec->CtrlScheduleStatus > 0 && TickTime > CtrlRec->ScheduleDateTime ) StartSchedule();
		}
	}
}

void __fastcall TCIISController::AfterSysClockTick( TDateTime TickTime )
{
	if( FApplyNewDecaySample )
	{
		DB->ApplyUpdatesDecayValue();

		#if DebugCIIApplyUppdate == 1
		Debug( "DecayValues Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesSensor();

		#if DebugCIIApplyUppdate == 1
		Debug( "Sensors Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesPS();

		#if DebugCIIApplyUppdate == 1
		Debug( "PS Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);

		#if DebugCIIApplyUppdate == 1
		Debug( "MiscValues Updated" + DateTimeToStr( TickTime ));
		#endif

		 DB->ApplyUpdatesPrj();

		#if DebugCIIApplyUppdate == 1
		Debug( "Project Updated" + DateTimeToStr( TickTime ));
		#endif

		FApplyNewDecaySample = false;
	}

	if( FApplyNewMonitorSample )
	{

		DB->ApplyUpdatesMonitorValue();
		#if DebugCIIApplyUppdate == 1
		Debug( "MonitorValues Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesSensor();
		#if DebugCIIApplyUppdate == 1
		Debug( "Sensors Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		#if DebugCIIApplyUppdate == 1
		Debug( "MiscValues Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesPS();
		#if DebugCIIApplyUppdate == 1
		Debug( "PS Updated" + DateTimeToStr( TickTime ));
		#endif

		DB->ApplyUpdatesPrj();
		#if DebugCIIApplyUppdate == 1
		Debug( "Project Updated" + DateTimeToStr( TickTime ));
		#endif
    FApplyNewMonitorSample = false;
	}

	if( FApplyNewZone )
	{


		DB->ApplyUpdatesZone();
		FApplyNewZone = false;

		#if DebugCIIApplyUppdate == 1
		Debug( "ApplyNewZone in AfterSysClockTick" + DateTimeToStr( TickTime ));
		#endif
	}

	if( FApplyNewRecording )
	{
		DB->ApplyUpdatesRecording();
		FApplyNewRecording = false;

		#if DebugCIIApplyUppdate == 1
		Debug( "ApplyNewRec in AfterSysClockTick" + DateTimeToStr( TickTime ));
		#endif
	}

}

void __fastcall TCIISController::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
  switch (BPIn->MessageCommand)
  {
	case MSG_TX_IDENTIFY:
	  BPIn->MsgMode = CIISBus_AddNode;
		BPIn->ParseMode = CIISParseContAndFinalize;
		break;
	case MSG_TX_BIMode:
		if( BPIn->BIMode == 1 )
		{
		  P_Prj->ShowuCtrlInfo = true;
      RBState = RB_RestartZones;
		}
		BPIn->ParseMode = CIISParseContAndFinalize;
		break;

	default:
	break;
  }
}

void __fastcall TCIISController::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{
  switch (BPIn->MessageCommand)
  {
	case MSG_TX_IDENTIFY:
	  if( BPIn->MsgMode == CIISBus_AddNode ) AddNewNode( BPIn );
	  break;

	case MSG_TX_CAPABILITY:
		if( BPIn->MsgMode == CIISBus_UpdateNode )
	  {
			UpdateNode( BPIn );
			BPIn->MsgMode = CIISBus_MsgReady;
			BPIn->ParseMode = CIISParseReady;
	  }
		break;

	case MSG_TX_BIMode:
		if( BPIn->BIMode == 1 )
		{
			TCIISZone_uCtrl *Zone;
			CIISZoneRec *ZR;
			TCIISBIChannel * BICh;
			BICh = (TCIISBIChannel*)BPIn->GetBIChannel();

			ZR = new CIISZoneRec;

			ZR->ZoneName = IntToStr( BICh->BusInterfaceSerialNo );
			ZR->BIChSerNo = BICh->BusInterfaceSerialNo;
			ZR->AnodeArea = 0;
			ZR->CathodeArea = 0;
			ZR->Comment = " ";
			ZR->ZoneSampInterval = 3;
			ZR->RecType = 0;
			ZR->RecNo = 0;
			ZR->ZoneAlarmStatus = 0;
			ZR->ZoneScheduleStatus = 0;
			ZR->CtrlName = CtrlRec->CtrlName;
			ZR->ZoneCanAdr = 0;
			ZR->IncludeInSchedule = false;
			ZR->uCtrl = true;

			if( DB->AppendZoneRec( ZR, true ))
			{
				DB->ApplyUpdatesZone();
				P_Prj->Log( ClientCom, CIIEventCode_ZoneTabChanged, CIIEventLevel_High, "", 0, 0 );
				Zone = new TCIISZone_uCtrl( DB, CIISBusInt, DW, ZR, this, true );
				Zone->uCtrlConnected = true;
				Zone->CIISBICh = BICh;
				ChildList->Add( Zone );
			}
		}
		break;

		default:
		break;
	}
}

void __fastcall TCIISController::AddNewNode( TCIISBusPacket *BPIn )
{
	CIISNodeRec *NodeRec;
	TCIISNodeIni *Node;

	NodeRec = new CIISNodeRec;
	NodeRec->NodeSerialNo = BPIn->SerNo;
	Node = new TCIISNodeIni( DB, CIISBusInt, DW, NodeRec, this );

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	AddNode( Node );

	P_Prj->ConfigChanged();
}

void __fastcall TCIISController::UpdateNode( TCIISBusPacket *BPIn )
{
  TCIISWLink *WLink;
  TCIISNodeIni *NI;
  TCIISObj *UpdateObj;
	CIISWLinkRec *WLR;
	TCIISBIChannel *BICh;
	int32_t IniErr;


  UpdateObj = (TCIISObj*)BPIn->CIISObj;

  if( UpdateObj->CIISObjIs( CIISWLink ) )
  {
		WLink = (TCIISWLink*)UpdateObj;
		WLink->IniRunning = true;
  }
  else if( UpdateObj->CIISObjIs( CIISNode ) )
	{
		NI = (TCIISNodeIni*)UpdateObj;
		if( NI->NodCapability == Camur_II_WLink  )
		{
			WLR = new CIISWLinkRec;
			WLR->WLinkSerialNo = NI->SerialNo;
			WLR->WLinkCanAdr = NI->CanAdr;

			BICh = NI->CIISBICh;
			IniErr = NI->IniErrorCount;

			WLR->WLinkType = NI->NodCapability;
			WLR->CtrlName = CtrlRec->CtrlName;

			DB->AppendWLinkRec( WLR ); DB->ApplyUpdatesWLink();

			delete NI;

			WLink = new TCIISWLink(DB, CIISBusInt, DW, WLR, this );
			WLink->SetDefaultValues();
			WLink->CIISBICh = BICh;
			WLink->IniErrorCount = IniErr;

			ChildList->Add( WLink );
			WLink->IniRunning = true;

		}
  }
}

void __fastcall TCIISController::ResetCIISBus()
{
	RestartCIISBusRunning = false;
	StartScheduleRunning = false;

	ResetBusState = 100;
	T = 0;
	TNextEvent = 0;
	ResetCIISBusRunning = true;
}

void __fastcall TCIISController::SM_ResetCIISBus()
{
	if( T >= TNextEvent )
	{
		TCIISObj *CIISObj;
		TCIISZone *Z;
		TCIISWLink *W;
		TCIISBusInterface *BI;


		if( T >= 20000 * NoOffIniRetrys ) ResetBusState = 999;

		switch( ResetBusState )
		{
			case 100:
			CtrlRec->NodeCount = 0;
			CtrlRec->DetectedNodeCount = 0;
			CtrlRec->IniErrorCount = 0;
			CtrlRec->MonitorCount = 0;
			CtrlRec->MonitorExtCount = 0;
			CtrlRec->DecayCount = 0;
			CtrlRec->LPRCount = 0;
			CtrlRec->USBCANStatus = 0;

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
        CIISObj = (TCIISObj*) ChildList->Items[i];
				if( CIISObj->CIISObjIs( CIISBusInterface ))
				{
					BI = (TCIISBusInterface*)CIISObj;
					BI->ResetCIISBus();
				}
			}

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
				CIISObj = (TCIISObj*) ChildList->Items[i];
				if( CIISObj->CIISObjIs( CIISWLink ))
				{
					W = (TCIISWLink*)CIISObj;
					W->ResetCIISBus();
				}
				else if( CIISObj->CIISObjIs( CIISZones ))
				{
					Z = (TCIISZone*)CIISObj;
					Z->ResetCIISBus();
				}
			}

			DB->ApplyUpdatesWLink();
			ResetBusState = 200;
			TNextEvent += 500;
			break;

		case 200:
			if( !CIISBusInt->ResetRunning )  ResetBusState = 900;
			TNextEvent += 500;
			break;

		case 900:
			ResetCIISBusRunning = false;
			RestartCIISBus();
			break;

		case 999:
			ResetCIISBusRunning = false;
			break;
		}
	}
	T += SysClock;
}

void __fastcall TCIISController::RestartCIISBus()
{
	RBState = RB_RestartBusInterface;
	T = 0;
	TNextEvent = 0;
	RestartCIISBusRunning = true;

	// Delay schedule if schedule start time is past.
	if( Now() > CtrlRec->ScheduleDateTime )
	{
		if( CtrlRec->CtrlScheduleStatus > 0 )	IncrementScheduleDateTime();
	}
}

void __fastcall TCIISController::SM_RestartCIISBus()
{
	TCIISObj *CIISObj;
	TCIISZone *Z;

	if( T >= TNextEvent )
	{
		//TCIISObj *CIISObj;
		//TCIISZone *Z;

		if( T >= 65000 * NoOffIniRetrys ) RBState = RB_TimeOut;

		switch( RBState )
		{
			case RB_RestartBusInterface:
			CIISBusInt->RestartCIISBus();
			RBState = RB_WaitForBusIni;
			TNextEvent += 4000;
			break;

			case RB_WaitForBusIni:
			if( NodeIniReady() )
			{
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesAlarm();
				UpdateNodeCountInZones();
				UpdateIniErrorCountInZones();

				CtrlRec->BusIntSerialNo = CIISBusInt->HighestUSBSerialNo;
				CtrlRec->BusIntVerMajor = CIISBusInt->HighestUSBVerMajor;
				CtrlRec->BusIntVerMinor = CIISBusInt->HighestUSBVerMinor;

				if( DB->LocateCtrlRec( CtrlRec ) )
				{
					DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
				}
				RBState = RB_ReqBIMode;
				TNextEvent += 500;
			}
			else TNextEvent += 1000;
			break;

			case RB_ReqBIMode:
			CIISBusInt->RequestBIMode();
			RBState = RB_RestartWatchDog;
			TNextEvent += 1000;
			break;

			case RB_RestartWatchDog:
			if( CtrlRec->WatchDogEnable )
			{
				//CIISBusInt->SetWatchDogTime( WatchDogTime );
				CIISBusInt->SetWatchDogEnable( true );
			}
			else
			{
				CIISBusInt->SetWatchDogEnable( false );
			}
			RBState = RB_CancelDecay;
			TNextEvent += 1000;
			break;


			case RB_CancelDecay:
			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
				CIISObj = (TCIISObj*) ChildList->Items[i];
				if( CIISObj->CIISObjIs( CIISZones ))
				{
					Z = (TCIISZone*)CIISObj;
					Z->CancelDecay();
				}
			}

			RBState = RB_RestartZones;
			TNextEvent += 1000;
			break;


			case RB_RestartZones:
			RestartZones();
			RBState = RB_Ready;
			TNextEvent += 1000;
			break;

			case RB_Ready :
			RestartCIISBusRunning = false;
			P_Prj->Log( SystemEvent, CIIEventCode_ServerReStarted, CIIEventLevel_High, "ServerStartOK", 0, 0 );
			break;

			case RB_TimeOut :
			RestartCIISBusRunning = false;
			P_Prj->Log( SystemEvent, CIIEventCode_ServerReStarted, CIIEventLevel_High, "ServerStartTimeout", 0, 0 );
			break;

			default:
			break;
	  }
	}
  T += SysClock;
}


bool __fastcall TCIISController::NodeIniReady()
{
	TCIISObj *CIISObj;
	TCIISBusInterface *BI;
	TCIISWLink *WL;
	TCIISZone *Z;
	bool NIR;

	NIR = true;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISBusInterface ) )
		{
			BI = (TCIISBusInterface*)CIISObj;
			NIR = NIR && BI->NodeIniReady();
		}
		else if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Z = (TCIISZone*)CIISObj;
			NIR = NIR && Z->NodeIniReady();
		}
		else if( CIISObj->CIISObjIs( CIISWLink )  )
		{
			WL = (TCIISWLink*)CIISObj;
			NIR = NIR && WL->NodeIniReady();
		}

		if( !NIR ) break;
	}
	return NIR;
}

void __fastcall TCIISController::UpdateNodeCountInZones()
{
	TCIISObj *CIISObj;
	TCIISWLink *WL;
	TCIISZone *Z;
	int32_t NC;

	NC = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Z = (TCIISZone*)CIISObj;
			NC = NC + Z->GetNodeCount();
		}
		else if( CIISObj->CIISObjIs( CIISWLink )  )
		{
			WL = (TCIISWLink*)CIISObj;
			if( WL->WLinkStatus == 0 ) NC++;
		}
	}
	CtrlRec->NodeCount = NC;
	return;
}

void __fastcall TCIISController::UpdateIniErrorCountInZones()
{
	TCIISObj *CIISObj;
	TCIISWLink *WL;
  TCIISZone *Z;
  int32_t EC;

  EC = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Z = (TCIISZone*)CIISObj;
			EC = EC + Z->GetIniErrorCount();
		}
		else if( CIISObj->CIISObjIs( CIISWLink )  )
		{
			WL = (TCIISWLink*)CIISObj;
			EC = EC + WL->IniErrorCount;
		}
	}
	CtrlRec->IniErrorCount = EC;
	return;
}

int32_t __fastcall TCIISController::GetBIChCount()
{
  return CIISBusInt->BIChCount;
}

void __fastcall TCIISController::RestartZones()
{
	TCIISObj *CIISObj;
	TCIISZone *Z;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Z = (TCIISZone*)CIISObj;
			Z->RestartZone();
		}
	}
}

void __fastcall TCIISController::StartSchedule()
{
	TCIISObj *CIISObj;
	TCIISZone *Z;

	IncrementScheduleDateTime();

	if( DecayZoneDelay == 0 )
	{
		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISZones ) )
			{
				Z = (TCIISZone*)CIISObj;
				if( Z->ZoneNo != 1 ) Z->StartSchedule();
			}
		}
	}
	else
	{
		T2 = DecayZoneDelay * 1000;
		T2NextEvent = DecayZoneDelay * 1000;
		SheduleNextZone = 1;   // Zone 0 = Avaliable
		StartScheduleRunning = true;
	}


	#if DebugCIISObj == 1
	Debug( "Scheduling started new scheduling time set" );
	#endif
}

void __fastcall TCIISController::IncrementScheduleDateTime()
{
	int32_t T, Y;
	Word Year, Month, Day, Hour, Min, Sec, MSec;
	TDateTime DT;

	T = max( 1, CtrlRec->SchedulePeriod );
	DT = CtrlRec->ScheduleDateTime;

	while( DT < Now() )
	{
		switch( CtrlRec->SchedulePeriodUnit )
		{
			case 0: DT = DT + TDateTime( T * OneMinute ); // minutes
			break;
			case 1: DT = DT + TDateTime( T * OneHour ); // hours
			break;
			case 2: DT = DT + TDateTime( T * OneDay ); // days
			break;
			case 3:  // months
			DecodeDate(DT, Year, Month, Day);
			DecodeTime(DT, Hour, Min, Sec, MSec);
			Y = T / 12;
			T = T - Y * 12;
			Month += T;
			Year += Y;
			if( Month > 12 )
			{
				Year++;
				Month -=12;
			}
			try
			{
				DT = EncodeDate( Year, Month, Day );
			}
			catch(...)
			{
				try
				{
					DT = EncodeDate( Year, Month, 30 );
				}
				catch(...)
				{
					try
					{
						DT = EncodeDate( Year, Month, 29 );
					}
					catch(...)
					{
						DT = EncodeDate( Year, Month, 28 );
					}
				}
			}
			DT = DT + EncodeTime( Hour, Min, Sec, MSec );
			break;
		}
	}

	CtrlRec->ScheduleDateTime = DT;
	if( DB->LocateCtrlRec( CtrlRec ) )
	{
		DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
	}
}

void __fastcall TCIISController::SM_StartSchedule()
{
	TCIISObj *CIISObj;
	TCIISZone *Z;

  if( T2 >= T2NextEvent)
  {
		while( SheduleNextZone < ChildList->Count )
		{
			CIISObj = (TCIISObj*) ChildList->Items[SheduleNextZone];
			if( CIISObj->CIISObjIs( CIISZones ) )
			{
				Z = (TCIISZone*)CIISObj;
				Z->StartSchedule();
				T2 = 0;
				SheduleNextZone++;
				break;
			}
			else SheduleNextZone++;
		}
		if( SheduleNextZone == ChildList->Count ) StartScheduleRunning = false;
	}
  T2 += SysClock;
}


void __fastcall TCIISController::UpdateLastValues()
{
  TCIISObj *CIISObj;
  TCIISZone *Z;
  TCIISWLink *W;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISZones ) )
	{
	  Z = (TCIISZone*)CIISObj;
	  Z->UpdateLastValues();
	}
	else if( CIISObj->CIISObjIs( CIISWLink ) )
	{
	  W = (TCIISWLink*)CIISObj;
	  W->UpdateLastValues();
	}
  }

}

bool __fastcall TCIISController::UpdateAlarmStatus()
{
	TCIISObj *CIISObj;
	TCIISZone *Zone;
	bool Alarm, AlarmOnZone;

	Alarm = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Zone = (TCIISZone*)CIISObj;
			AlarmOnZone = Zone->UpdateAlarmStatus();
			Alarm = Alarm || AlarmOnZone;
		}
	}
  if( Alarm ) CtrlRec->CtrlAlarmStatus = 1;
  else CtrlRec->CtrlAlarmStatus = 0;
	if( DB->LocateCtrlRec( CtrlRec ) )
	{
		DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
	}
  return Alarm;
}

void __fastcall TCIISController::ResetCANAlarm()
{
  TCIISObj *CIISObj;
	TCIISZone *Zone;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			Zone = (TCIISZone*)CIISObj;
			Zone->ResetCANAlarm();
		}
	}
}


int32_t __fastcall TCIISController::GetSampelIntervall( int32_t i )
{
  int32_t SIntv;

  switch( i )
  {
	case 1: SIntv = CtrlRec->SampInterval1;
	  break;
	case 2: SIntv = CtrlRec->SampInterval2;
	  break;
	case 3: SIntv = CtrlRec->SampInterval3;
	  break;
	case 4: SIntv = CtrlRec->SampInterval4;
	  break;
	case 5: SIntv = CtrlRec->SampInterval5;
	  break;
  }
  return SIntv;
}

int32_t __fastcall TCIISController::GetSetSampelIntervall( int32_t uCtrlSampInt )
{
	if( uCtrlSampInt == CtrlRec->SampInterval1 ) return 1;
	else if ( uCtrlSampInt == CtrlRec->SampInterval2 ) return 2;
	else if ( uCtrlSampInt == CtrlRec->SampInterval3 ) return 3;
	else if ( uCtrlSampInt == CtrlRec->SampInterval4 ) return 4;
	else if ( uCtrlSampInt == CtrlRec->SampInterval5 ) return 5;
	else
	{
		CtrlRec->SampInterval5 = uCtrlSampInt;
		if( DB->LocateCtrlRec( CtrlRec ) )
		{
			DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
		}
		return 5;
	}
}

void __fastcall TCIISController::IncRecordingCount( int32_t RecType )
{
  switch( RecType )
  {
	case RT_Monitor:
		CtrlRec->MonitorCount = CtrlRec->MonitorCount + 1;
		break;
	case RT_MonitorExt:
		CtrlRec->MonitorExtCount = CtrlRec->MonitorExtCount + 1;
		break;
	case RT_LPR:
	  CtrlRec->LPRCount = CtrlRec->LPRCount + 1;
	  break;
	case RT_Decay:
	  CtrlRec->DecayCount = CtrlRec->DecayCount + 1;
	  break;
	case RT_CTNonStat:
	  CtrlRec->LPRCount = CtrlRec->LPRCount + 1;
	  break;
	case RT_CTStat:
	  CtrlRec->DecayCount = CtrlRec->DecayCount + 1;
	  break;
	case RT_RExt:
	  CtrlRec->DecayCount = CtrlRec->DecayCount + 1;
	  break;
  }
  if( DB->LocateCtrlRec( CtrlRec ) )
  {
		DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
  }

}

void __fastcall TCIISController::DecRecordingCount( int32_t RecType )
{
  switch( RecType )
  {
	case RT_Monitor:
		CtrlRec->MonitorCount = CtrlRec->MonitorCount - 1;
		break;
	case RT_MonitorExt:
		CtrlRec->MonitorExtCount = CtrlRec->MonitorExtCount - 1;
		break;
	case RT_LPR:
	  CtrlRec->LPRCount = CtrlRec->LPRCount - 1;
	  break;
	case RT_Decay:
	  CtrlRec->DecayCount = CtrlRec->DecayCount - 1;
	  break;
	case RT_CTNonStat:
	  CtrlRec->LPRCount = CtrlRec->LPRCount - 1;
	  break;
	case RT_CTStat:
	  CtrlRec->DecayCount = CtrlRec->DecayCount - 1;
	  break;
	case RT_RExt:
	  CtrlRec->DecayCount = CtrlRec->DecayCount - 1;
	  break;
  }
  if( DB->LocateCtrlRec( CtrlRec ) )
  {
		DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();
  }
}

void __fastcall TCIISController::IncDetectedNodeCount()
{
  CtrlRec->DetectedNodeCount++;
}

void __fastcall TCIISController::DecDetectedNodeCount()
{
  CtrlRec->DetectedNodeCount--;
}

// Calib

TCIISZone* __fastcall TCIISController::GetZone( int32_t Zone_No )
{
	TCIISObj *CIISObj;
	TCIISZone *Zone;
	int32_t ZoneCnt;

	ZoneCnt = 0;
	Zone = NULL;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISZones ) )
		{
			ZoneCnt++;
			if (ZoneCnt == Zone_No)
			{
				Zone = (TCIISZone*) CIISObj;
				break;
			}
		}
	}
	return Zone;
}

TCIISBusInterface* __fastcall TCIISController::GetBusInt()
{
	return CIISBusInt;
}






