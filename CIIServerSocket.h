//---------------------------------------------------------------------------

#ifndef CIIServerSocketH
#define CIIServerSocketH

#include "DebugWinU.h"
#include "CIISCommon.h"
#include <System.Win.ScktComp.hpp>

//---------------------------------------------------------------------------

class TCIIServerSocket : public TObject
{
private:
  TDebugWin *DW;
  TList *ReceivedMessageList, *SendMessageList;
  TServerSocket *ServSocket;
	TCustomWinSocket *CWSocket;
	MessInQueueFP MInQ;
	bool FClientLoggedIn, FClientAktiv, TmpClientAktiv;
	CIIUserLevel FUserLevel;
	bool FPrjSelected, FPrjSelOnline;

  char SocketInBuf[SocketInBufSize];
  unsigned char SocketOutBuf[SocketOutBufSize];
  unsigned char SocketInputQueue[SocketInputQueueSize];
  int SocketInputIdx, PortNo;
  Word ResponseCount;
  bool FClientConnected;

  void __fastcall Debug(String Message);
  void __fastcall AddSocketInputQueue(char* Buf, int N);
  
	bool __fastcall ParseMessages();

	bool __fastcall GetClientAktiv();

protected:

public:
        __fastcall TCIIServerSocket(MessInQueueFP SetMInQ, int SetPortNo, TDebugWin * SetDebugWin);
        __fastcall ~TCIIServerSocket();
    void __fastcall SetDebugWin(TDebugWin * SetDebugWin);
/*
      void __fastcall TimerSReadEvent();
*/
    void __fastcall SocketAccept(TObject * Sender, TCustomWinSocket * Socket);
    void __fastcall ClientConnect(TObject * Sender, TCustomWinSocket * Socket);
		void __fastcall ClientDisconnect(TObject * Sender, TCustomWinSocket * Socket);
		void __fastcall ClientOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode);
    void __fastcall ClientRead(TObject * Sender, TCustomWinSocket * Socket);
    TCamurPacket* __fastcall GetMessage();
		void __fastcall PutMessage( TCamurPacket * o );
		void __fastcall CancelSendMessage();
    void __fastcall SendMessage();
		void __fastcall ConnectClient();
		void __fastcall DisconnectClient();

__published:
		__property bool ClientConnected  = { read=FClientConnected };
		__property bool ClientLoggedIn = { read=FClientLoggedIn, write=FClientLoggedIn };
		__property CIIUserLevel UserLevel = { read=FUserLevel, write=FUserLevel };
		__property bool PrjSelected = { read=FPrjSelected, write=FPrjSelected };
		__property bool PrjSelOnline = { read=FPrjSelOnline, write=FPrjSelOnline };
		__property bool ClientAktiv = { read=GetClientAktiv };

};







#endif
