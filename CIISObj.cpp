//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISObj.h"
#include "CIISBusInterface.h"
#include "CIISBusInterface.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

__fastcall TCIISObj::TCIISObj(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, TObject *SetCIISParent )
{
  DB = SetDB;
  CIISBusInt = SetCIISBusInt;
  DW = SetDebugWin;
  CIISParent = SetCIISParent;
  ChildList = new TList;
  CIISObjType = CIISUndefinedTab;
}

//---------------------------------------------------------------------------

__fastcall TCIISObj::~TCIISObj()
{

//

}

void __fastcall TCIISObj::SetDebugWin(TDebugWin * SetDebugWin)
{
  TCIISObj *Obj;

  DW = SetDebugWin;
  for( int32_t i = 0; i < ChildList->Count ; i++ )
  {
	Obj = (TCIISObj*)ChildList->Items[i];
	Obj->SetDebugWin( SetDebugWin );
  }
}

void __fastcall TCIISObj::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

void __fastcall TCIISObj::ParseCommand( TCamurPacket *P, TCamurPacket *O )
{
	TCIISObj *Obj;

	#if DebugMsg == 1
	Debug( "CIISObj: ParseCommand" );
	#endif

  if( P->MessageTable == this->CIISObjType )
  {
		ParseCommand_Begin( P, O );
  }
	else // Not this type of object, try sub objects
  {
		for( int32_t i = 0; i < ChildList->Count ; i++ )
		{
			Obj = (TCIISObj*)ChildList->Items[i];
			Obj->ParseCommand( P, O );
			if( P->CommandMode == CIISCmdReady ||
				P->CommandMode == CIISDeleteCtrl ||
				P->CommandMode == CIISDeleteZone ||
				P->CommandMode == CIISAddZone ||
				P->CommandMode == CIISSensorMoved ||
				P->CommandMode == CIISDeleteSensor ||
				P->CommandMode == CIISAlarmMoved ||
				P->CommandMode == CIISDeleteAlarm ||
				P->CommandMode == CIISPSMoved ||
				P->CommandMode == CIISDeletePS ||
				P->CommandMode == CIISDeleteWLink
			) break;
		}
  }
  ParseCommand_End( P, O );
}

bool  __fastcall TCIISObj::CIISObjIs( CIISTable CIISObjType )
{
  return CIISObjType == this->CIISObjType;
}


// Camur II Bus Interface

 void __fastcall TCIISObj::ParseClockTick( TDateTime TickTime )
{
  TCIISObj *Obj;
  this->OnSysClockTick( TickTime );
  for( int32_t i = 0; i < ChildList->Count ; i++ )
  {
		Obj = (TCIISObj*)ChildList->Items[i];
		Obj->ParseClockTick( TickTime );
	}
	this->AfterSysClockTick( TickTime );
}

void __fastcall TCIISObj::ParseCIIBusMessage( TCIISBusPacket *BPIn )
{
  TCIISObj *Obj;

  ParseCIISBusMessage_Begin( BPIn );

  for( int32_t i = 0; i < ChildList->Count ; i++ )
  {
		Obj = (TCIISObj*)ChildList->Items[i];
		Obj->ParseCIIBusMessage( BPIn );
		if( BPIn->ParseMode < CIISParseCont) break;
  }

  if( BPIn->ParseMode == CIISParseFinalize ||
	  BPIn->ParseMode == CIISParseContAndFinalize ) ParseCIISBusMessage_End( BPIn );
}
