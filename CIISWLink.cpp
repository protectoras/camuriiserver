//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISWLink.h"

#include "CIISProject.h"
#include "CIISController.h"
#include "CIISBusInterface.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

/*
If one BI in system with many BI is removed and system restarted:
Check CIISBICh != NULL in function that can be cald for not connected WLink
if( WLinkRec->WLinkConnected ) or if( CIISBICh != NULL )
*/

__fastcall TCIISWLink::TCIISWLink(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISWLinkRec *SetWLinkRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{

  CIISObjType = CIISWLink;

  P_Ctrl = (TCIISController*)CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

  CIISBICh = NULL;

	WLinkRec = SetWLinkRec;
	WLinkSetStateRunning = false;
	NodeDetected = false;

	WLinkIniRunning = false;
	FIniErrorCount = 0;
	NIState = NI_ReqVersion;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;

}

__fastcall TCIISWLink::~TCIISWLink()
{
 delete WLinkRec;
}

void __fastcall TCIISWLink::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "WLink: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	  {
		WriteRec( P, O );
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase )
	  {
		if( DB->DeleteWLinkRec( WLinkRec ) )
		{
		  DB->ApplyUpdatesWLink();
		  P_Prj->Log( ClientCom, CIIEventCode_WLinkTabChanged, CIIEventLevel_High, "", 0, 0 );
		  P->CommandMode = CIISDeleteWLink;
		  P->CIISObj = this;
		  O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_DeleteError;
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISWLink::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "WLink: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISWLink::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == WLinkRec->WLinkSerialNo;
}

void __fastcall TCIISWLink::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
	if( P->ArgInc( 200 ) )
	{
	  if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
	  else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
	}
  }

  if( P->CommandMode == CIISRecSearch )
  {
	if( ThisRec( P ) )
	{
	  if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
	  O->MessageCode = CIISMsg_Ok;
	}
	else O->MessageCode = CIISMsg_RecNotFound;
  }
  else if( P->CommandMode == CIISRecAdd )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( WLinkRec->WLinkSerialNo ) + "\r\n" +
	"2=" + IntToStr( WLinkRec->WLinkCanAdr ) + "\r\n" +
	"3=" + IntToStr( WLinkRec->WLinkStatus ) + "\r\n" +
	"4=" + WLinkRec->WLinkName + "\r\n" +
	"5=" + IntToStr( WLinkRec->WLinkType ) + "\r\n" +
	"6=" + IntToStr( WLinkRec->WLinkRequestStatus ) + "\r\n" +
	"7=" + CIISBoolToStr( WLinkRec->WLinkConnected ) + "\r\n" +
	"8=" + IntToStr( WLinkRec->WLinkVerMajor ) + "\r\n" +
	"9=" + IntToStr( WLinkRec->WLinkVerMinor ) + "\r\n" +
	"10=" + IntToStr( WLinkRec->WLinkDH ) + "\r\n" +
	"11=" + IntToStr( WLinkRec->WLinkDL ) + "\r\n" +
	"12=" + IntToStr( WLinkRec->WLinkMY ) + "\r\n" +
	"13=" + IntToStr( WLinkRec->WLinkCH ) + "\r\n" +
	"14=" + IntToStr( WLinkRec->WLinkID ) + "\r\n" +
	"15=" + IntToStr( WLinkRec->WLinkPL ) + "\r\n" +
	"16=" + IntToStr( WLinkRec->WLinkSignal ) + "\r\n" +
	"17=" + IntToStr( WLinkRec->WLinkMode ) + "\r\n" +
	"18=" + CIISBoolToStr( WLinkRec->VerNotSupported ) + "\r\n" +
	"50=" + WLinkRec->CtrlName + "\r\n";

	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetWLinkRecCount() ) + "\r\n";;
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISWLink::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->ArgInc(200) )
  {
	if( P->GetArg(200) == "Update LastValue" )
	{
	  this->UpdateLastValues();
	  O->MessageCode = CIISMsg_Ok;
	}
	else if( P->GetArg(200) == "Permanent" )
	{
	  this->Permanent();
	  O->MessageCode = CIISMsg_Ok;
	}
  }
  else if( DB->LocateWLinkRec( WLinkRec ) )
  {
	if( P->ArgInc(2) ) WLinkRec->WLinkName = P->GetArg(2);

	if( P->ArgInc(3) ) WLinkRec->WLinkDH = StrToInt(P->GetArg(3));
	if( P->ArgInc(4) ) WLinkRec->WLinkDL = StrToInt(P->GetArg(4));
	if( P->ArgInc(5) ) WLinkRec->WLinkMY = StrToInt(P->GetArg(5));
	if( P->ArgInc(6) ) WLinkRec->WLinkCH = StrToInt(P->GetArg(6));
	if( P->ArgInc(7) ) WLinkRec->WLinkID = StrToInt(P->GetArg(7));
	if( P->ArgInc(8) ) WLinkRec->WLinkPL = StrToInt(P->GetArg(8));
	if( P->ArgInc(9) ) WLinkRec->WLinkMode = StrToInt(P->GetArg(9));

	if( DB->SetWLinkRec( WLinkRec ) )
	{
	  DB->ApplyUpdatesWLink();
	  P_Prj->Log( ClientCom, CIIEventCode_WLinkTabChanged, CIIEventLevel_High, "", 0, 0 );
	  O->MessageCode = CIISMsg_Ok;
	}
	else O->MessageCode = CIISDBError;
  }
  else O->MessageCode = CIISMsg_RecNotFound;

  P->CommandMode = CIISCmdReady;

  if( P->ArgInc(3) || P->ArgInc(4) || P->ArgInc(5) || P->ArgInc(6) || P->ArgInc(7) ||
	  P->ArgInc(8) || P->ArgInc(9) )
  {
	if( DB->LocateWLinkRec( WLinkRec ) ) this->WLinkSetState( P );
  }

}
#pragma argsused
void __fastcall TCIISWLink::OnSysClockTick( TDateTime TickTime )
{
  if( WLinkIniRunning ) SM_WLinkIni();
  else if( WLinkSetStateRunning ) SM_WLinkSetState();
}

  //CIISBusInt

void __fastcall TCIISWLink::ResetCIISBus()
{
  CIISBICh = NULL;
  WLinkRec->WLinkCanAdr = 0;
  WLinkRec->WLinkStatus = 1;
  WLinkRec->WLinkConnected = false;
	NodeDetected = false;
	FIniErrorCount = 0;
  if( DB->LocateWLinkRec( WLinkRec ) )
  {
		DB->SetWLinkRec( WLinkRec ); // DB->ApplyUpdatesWLink();
  }
}

bool __fastcall TCIISWLink::NodeIniReady()
{
  return !WLinkIniRunning;
}

void __fastcall TCIISWLink::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

  if(( BPIn->MessageCommand == MSG_TX_IDENTIFY ) && ( BPIn->SerNo == WLinkRec->WLinkSerialNo )) WLinkIni( BPIn );
  else if(( CIISBICh == BPIn->GetBIChannel() ) && ( BPIn->CANAdr == WLinkRec->WLinkCanAdr ))
  {
		switch (BPIn->MessageCommand)
		{
			case MSG_TX_WLinkPar:
			WLinkIniPar( BPIn );
			break;

			case MSG_TX_WLinkMode:
			WLinkIniMode( BPIn );
			break;

			case MSG_TX_VERSION:
			WLinkIniVersion( BPIn );
			break;

			default:
			break;
		}
	}
}
#pragma argsused
void __fastcall TCIISWLink::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISWLink::WLinkIni( TCIISBusPacket *BPIn )
{
	NIState = NI_Accept;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;
	WLinkIniRunning = true;

  CIISBICh = (TCIISBIChannel*) BPIn->GetBIChannel();

  BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	FIniErrorCount = 0;

	SM_WLinkIni();
}

void __fastcall TCIISWLink::SM_WLinkIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept:
			if( !NodeDetected )
			{
				WLinkRec->WLinkCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( WLinkRec->WLinkSerialNo, WLinkRec->WLinkCanAdr );
			TNextEvent = TNextEvent + CanAdrToReqWLinkPar + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

		case NI_ReqVersion:
			CIISBICh->RequestVer( WLinkRec->WLinkCanAdr );
			VerRecived = false;
			TNextEvent = TNextEvent + FastCmdDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer:
			if( VerRecived )
			{
				if( WLinkRec->WLinkVerMajor > WLink_SupportedVer )
				{
					WLinkRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					WLinkRec->VerNotSupported = false;

					NIState = NI_ReqWLinkDH;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;


			case NI_ReqWLinkDH:
			WLinkPar = 7; // DH
			CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
			WLinkParRecived = false;
			TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_WaitForWLinkDH;
			break;

			case NI_WaitForWLinkDH:
			if( WLinkParRecived )
			{
				WLinkPar = 8; // DL
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkDL;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkDL:
			if( WLinkParRecived )
			{
				WLinkPar = 9; // MY
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkMY;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkMY:
			if( WLinkParRecived )
			{
				WLinkPar = 10; // CH
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkCH;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkCH:
			if( WLinkParRecived )
			{
				WLinkPar = 11; // ID
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkID;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkID:
			if( WLinkParRecived )
			{
				WLinkPar = 12; // PL
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkPL;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkPL:
			if( WLinkParRecived )
			{
				WLinkPar = 6; // Signal
				CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkSignal;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkSignal:
			if( WLinkParRecived )
			{
				CIISBICh->RequestWLinkMode( WLinkRec->WLinkCanAdr );
				WLinkParRecived = false;
				TNextEvent = TNextEvent + WaitForWLinkParDelay + ( WLinkRec->WLinkCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_WaitForWLinkMode;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForWLinkMode :
			if( WLinkParRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqWLinkDH;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			WLinkIniRunning = false;
			break;

			case NI_TimeOut :
			WLinkRec->WLinkConnected = false;
			WLinkIniRunning = false;
			break;

			default:
			break;
		}
	}
  T += SysClock;
}

void __fastcall TCIISWLink::WLinkIniPar( TCIISBusPacket *BPIn )
{
  int32_t ParVal;

  WLinkParRecived = true;

  ParVal = BPIn->WLinkPar;

  switch ( WLinkPar )
  {
	case 7:
	  WLinkRec->WLinkDH = ParVal;
	  break;
	case 8:
	  WLinkRec->WLinkDL = ParVal;
	  break;
	case 9:
	  WLinkRec->WLinkMY = ParVal;
	  break;
	case 10:
	  WLinkRec->WLinkCH = ParVal;
	  break;
	case 11:
	  WLinkRec->WLinkID = ParVal;
	  break;
	case 12:
	  WLinkRec->WLinkPL = ParVal;
	  break;
	case 6:
	  WLinkRec->WLinkSignal = ParVal;
	  break;
	default:
	  //WLinkSetPar  0-5
	  break;
  }

  if( DB->LocateWLinkRec( WLinkRec ) )
  {
	DB->SetWLinkRec( WLinkRec ); DB->ApplyUpdatesWLink();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "WLinkParameter recived WLink: " + IntToStr( WLinkRec->WLinkSerialNo ) + " / " + IntToStr( WLinkRec->WLinkCanAdr ) + ": " + IntToStr( WLinkPar) + " = " + IntToStr( ParVal ));
  #endif
}

void __fastcall TCIISWLink::WLinkIniMode( TCIISBusPacket *BPIn )
{
  WLinkParRecived = true;

	WLinkRec->WLinkMode = BPIn->Byte3;

  if( DB->LocateWLinkRec( WLinkRec ) )
  {
	DB->SetWLinkRec( WLinkRec ); DB->ApplyUpdatesWLink();
  }
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug( "WLinkMode recived WLink: " + IntToStr( WLinkRec->WLinkSerialNo ) + " / " + IntToStr( WLinkRec->WLinkCanAdr ) + ": Mode = " + IntToStr( WLinkRec->WLinkMode));
  #endif

}

void __fastcall TCIISWLink::WLinkIniVersion( TCIISBusPacket *BPIn )
{
  VerRecived = true;
	//WLinkIniRunning = false;
  WLinkRec->WLinkVerMinor = BPIn->Byte4;
	WLinkRec->WLinkVerMajor = BPIn->Byte3;
  WLinkRec->WLinkConnected = true;
  WLinkRec->WLinkStatus = 0;
  if( DB->LocateWLinkRec( WLinkRec ) )
  {
	DB->SetWLinkRec( WLinkRec ); DB->ApplyUpdatesWLink();
  }
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  #if DebugCIISBusInt == 1
	Debug( "WLinkVersion recived WLink: " + IntToStr( WLinkRec->WLinkSerialNo ) + " / " + IntToStr( WLinkRec->WLinkCanAdr ) + " = " + IntToStr( WLinkRec->WLinkVerMajor) + "." + IntToStr( WLinkRec->WLinkVerMinor));
  #endif
}

void __fastcall TCIISWLink::SetDefaultValues()
{
  WLinkRec->WLinkName = IntToStr( WLinkRec->WLinkSerialNo );
  WLinkRec->WLinkStatus = 0;
  WLinkRec->WLinkRequestStatus= -1;
  WLinkRec->WLinkConnected = true;
	WLinkRec->WLinkVerMajor = 0;
	WLinkRec->WLinkVerMinor = 0;
  WLinkRec->VerNotSupported = true;

	DB->SetWLinkRec( WLinkRec );
}

void __fastcall TCIISWLink::UpdateLastValues()
{
  WLinkPar = 6;
  if( CIISBICh != NULL ) CIISBICh->RequestWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar );
}

void __fastcall TCIISWLink::Permanent()
{
  WLinkPar = 13;
  if( CIISBICh != NULL ) CIISBICh->WLinkParPermanent( WLinkRec->WLinkCanAdr );
}

void __fastcall TCIISWLink::WLinkSetState( TCamurPacket *P )
{
  if( CIISBICh != NULL )
  {
	SetPL = P->ArgInc(8);
	SetDH = P->ArgInc(3);
	SetDL = P->ArgInc(4);
	SetMY = P->ArgInc(5);
	SetCH = P->ArgInc(6);
	SetID = P->ArgInc(7);
	SetMode = P->ArgInc(9);

	WLSetState = NI_WLinkSetPL;
	WLinkSetStateRunning = true;

	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;
  }
}

void __fastcall TCIISWLink::SM_WLinkSetState()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) WLSetState = NI_TimeOut;

	switch( WLSetState )
	{
	  case NI_WLinkSetPL:
		if( !SetPL ) WLSetState = NI_WLinkSetMode;
		else
		{
		  WLinkPar = 5; // PL
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkPL );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkPL;
		}
		break;

	  case NI_WaitForWLinkPL:
		if( WLinkParRecived )
		{
		  WLSetState = NI_WLinkSetMode;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;

	  case NI_WLinkSetMode:
		if( !SetMode ) WLSetState = NI_WLinkSetDH;
		else
		{
		  CIISBICh->SetWLinkMode( WLinkRec->WLinkCanAdr, WLinkRec->WLinkMode );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkParDelay;
		  WLSetState = NI_WLinkSetDH;
		}
		break;

	  case NI_WLinkSetDH:
		if( !SetDH ) WLSetState = NI_WLinkSetDL;
		else
		{
		  WLinkPar = 0; // DH
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkDH );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkDH;
		}
		break;

	  case NI_WaitForWLinkDH:
		if( WLinkParRecived )
		{
		  WLSetState = NI_WLinkSetDL;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;


	  case NI_WLinkSetDL:
		if( !SetDL ) WLSetState = NI_WLinkSetMY;
		else
		{
		  WLinkPar = 1; // DL
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkDL );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkDL;
		}
		break;

	  case NI_WaitForWLinkDL:
		if( WLinkParRecived )
		{
		  WLSetState = NI_WLinkSetMY;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;

	  case NI_WLinkSetMY:
		if( !SetMY ) WLSetState = NI_WLinkSetCH;
		else
		{
		  WLinkPar = 2; // MY
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkMY );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkMY;
		}
		break;

	  case NI_WaitForWLinkMY:
		if( WLinkParRecived )
		{
		  WLSetState = NI_WLinkSetCH;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;

	  case NI_WLinkSetCH:
		if( !SetCH ) WLSetState = NI_WLinkSetID;
		else
		{
		  WLinkPar = 3; // CH
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkCH );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkCH;
		}
		break;

	  case NI_WaitForWLinkCH:
		if( WLinkParRecived )
		{
		  WLSetState = NI_WLinkSetID;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;

	  case NI_WLinkSetID:
		if( !SetID ) WLSetState = NI_Ready;
		else
		{
		  WLinkPar = 4; // ID
		  CIISBICh->SetWLinkPar( WLinkRec->WLinkCanAdr, WLinkPar, WLinkRec->WLinkID );
		  WLinkParRecived = false;
		  TNextEvent += WaitForWLinkSetParDelay;
		  WLSetState = NI_WaitForWLinkID;
		}
		break;

	  case NI_WaitForWLinkID:
		if( WLinkParRecived )
		{
		  WLSetState = NI_Ready;
		}
		else // retry
		{
		  if( IniRetrys-- <= 0 ) WLSetState = NI_TimeOut;
		  TNextEvent += WaitForWLinkSetParDelay;
		}
	  break;

	  case NI_Ready :
		WLinkSetStateRunning = false;
		break;

	  case NI_TimeOut :
		WLinkRec->WLinkConnected = false;
		WLinkSetStateRunning = false;
		break;

	  default:
		break;
	}
  }

  T += SysClock;

}
