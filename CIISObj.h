//---------------------------------------------------------------------------

#ifndef CIISObjH
#define CIISObjH
//---------------------------------------------------------------------------

#include "system.hpp"
#include "CIISCommon.h"
#include "CIISDB.h"
#include "DebugWinU.h"
//#include "CIISBusInterface.h"

class TCIISProject;
class TCIISController;
class TCIISZone;
class TCIISBusInterface;
class TCIISBIChannel;
class TCIISObj : public TObject
{
private:


protected:
  CIISTable CIISObjType;
  TCIISDBModule *DB;
  TDebugWin *DW;

  TList *ChildList;
  TCIISProject *P_Prj;
  TCIISController *P_Ctrl;
	TCIISZone *P_Zone;
  TCIISBusInterface *P_BI;



	//TUSBCANInterface *CAN;
	TCIISBusInterface *CIISBusInt;


public:
  TObject *CIISParent;
	TCIISBIChannel *CIISBICh;

  __fastcall TCIISObj(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, TObject *SetCIISParent );
  __fastcall ~TCIISObj();

  virtual void __fastcall SetDebugWin(TDebugWin * SetDebugWin);
  void __fastcall Debug(String Message);
  //CIISClientInt
  void  __fastcall ParseCommand( TCamurPacket *P, TCamurPacket *O );
  virtual void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O ) = 0;
  virtual void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O ) = 0;
  bool  __fastcall CIISObjIs( CIISTable CIISObjType );

  // CIISBusInt
  void  __fastcall ParseClockTick( TDateTime TickTime );
	virtual void __fastcall OnSysClockTick( TDateTime TickTime ) = 0;
  virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) = 0;
  void __fastcall ParseCIIBusMessage( TCIISBusPacket *BPIn );
  virtual void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn) = 0;
  virtual void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn ) = 0;

__published:

};











#endif
