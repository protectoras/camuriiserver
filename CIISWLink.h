//---------------------------------------------------------------------------

#ifndef CIISWLinkH
#define CIISWLinkH
//---------------------------------------------------------------------------

#include "CIISObj.h"
#include "CIISNodeIni.h"


class TCIISWLink : public TCIISObj
{
private:
  CIISWLinkRec *WLinkRec;
  Word WLinkPar;
  bool WLinkParRecived;
  TCamurPacket *IniP;
  NodeIniState NIState, WLSetState;
  bool SetPL, SetDH, SetDL, SetMY, SetCH, SetID, SetMode;
  int32_t FIniErrorCount;

  //CIISClientInt

  bool __fastcall ThisRec( TCamurPacket *P );
  void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
  void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt

  void __fastcall WLinkIni( TCIISBusPacket *BPIn );
  void __fastcall SM_WLinkIni();
  void __fastcall WLinkIniPar( TCIISBusPacket *BPIn );
  void __fastcall WLinkIniMode( TCIISBusPacket *BPIn );
  void __fastcall WLinkIniVersion( TCIISBusPacket *BPIn );


  int32_t __fastcall GetSerialNo() { return WLinkRec->WLinkSerialNo; }
  int32_t __fastcall GetCanAdr() { return WLinkRec->WLinkCanAdr; }
  int32_t __fastcall GetWLinkVerMajor() { return WLinkRec->WLinkVerMajor; }
	int32_t __fastcall GetWLinkVerMinor() { return WLinkRec->WLinkVerMinor; }
	int32_t __fastcall GetWLinkStatus() { return WLinkRec->WLinkStatus; }

  void __fastcall Permanent();
  void __fastcall WLinkSetState( TCamurPacket *P );
  void __fastcall SM_WLinkSetState();

protected:
  //CIISClientInt

  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

	//CIISBusInt
	//TCIISBIChannel *CIISBICh;
	int32_t IniRetrys;
  bool WLinkIniRunning, WLinkSetStateRunning;
  int32_t T, TNextEvent;
  bool NodeDetected;
  bool DHRecived;
  bool DLRecived;
  bool MYRecived;
  bool CHRecived;
  bool IDRecived;
  bool PLRecived;
  bool SignalRecived;
  bool ModeRecived;

  bool VerRecived;

	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
  __fastcall TCIISWLink(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISWLinkRec *SetWLinkRec, TObject *SetCIISParent );
  __fastcall ~TCIISWLink();
  void __fastcall ResetCIISBus();
  bool __fastcall NodeIniReady();
  void __fastcall SetDefaultValues();
  void __fastcall UpdateLastValues();

__published:

  __property int32_t SerialNo = { read = GetSerialNo };
  __property int32_t CanAdr = { read = GetCanAdr };
	__property bool IniRunning = { read = WLinkIniRunning, write = WLinkIniRunning };

	__property int32_t WLinkStatus = { read = GetWLinkStatus };

//	__property TCIISBIChannel* BIChannel = { read = CIISBICh, write = CIISBICh };

  __property int32_t VerMajor = { read = GetWLinkVerMajor };
	__property int32_t VerMinor = { read = GetWLinkVerMinor };
	__property int32_t IniErrorCount = { read = FIniErrorCount, write = FIniErrorCount };

};












#endif
