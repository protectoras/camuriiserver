//---------------------------------------------------------------------------

#include <vcl.h>
#include <dir.h>
#pragma hdrstop

#include "DebugWinU.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TDebugWin *DebugWin;
//---------------------------------------------------------------------------
__fastcall TDebugWin::TDebugWin(TComponent* Owner, DebugStateFP SetDS)
        : TForm(Owner)
{
	ChangeDS = SetDS;
	WriteToDebugFile = false;
}
//---------------------------------------------------------------------------

#pragma argsused
void __fastcall TDebugWin::FormCreate(TObject *Sender)
{
NextRow = 0;
StrGridDebug->ColWidths[0] = 150;
StrGridDebug->ColWidths[1] = 800;
}
//---------------------------------------------------------------------------


void __fastcall TDebugWin::Log(String Message)
{
StrGridDebug->Cells[0][NextRow] = CIISDateTimeToStr( Now() );
StrGridDebug->Cells[1][NextRow] = Message;

if( WriteToDebugFile )
{
		FileWrite(iFileHandle, StrGridDebug->Cells[0][NextRow].w_str(), StrGridDebug->Cells[0][NextRow].Length()*sizeof(wchar_t));
		FileWrite(iFileHandle, L"; ", 2*sizeof(wchar_t));
		FileWrite(iFileHandle, StrGridDebug->Cells[1][NextRow].w_str(), StrGridDebug->Cells[1][NextRow].Length()*sizeof(wchar_t));
		FileWrite(iFileHandle, L"\r\n", 2*sizeof(wchar_t));
}

NextRow++;
if( NextRow >= StrGridDebug->RowCount ) NextRow = 0;
StrGridDebug->Cells[0][NextRow] = "";
StrGridDebug->Cells[1][NextRow] = "-->";
}

#pragma argsused
void __fastcall TDebugWin::BitBtnClearClick(TObject *Sender)
{
for( int32_t i = 0; i < StrGridDebug->RowCount; i++)
{
	StrGridDebug->Cells[0][i] = "";
	StrGridDebug->Cells[1][i] = "";
	NextRow = 0;
	StrGridDebug->Cells[0][NextRow] = CIISDateTimeToStr( Now() );
	StrGridDebug->Cells[1][NextRow] = "Log cleard";
}
}
//---------------------------------------------------------------------------


#pragma argsused
void __fastcall TDebugWin::FormClose(TObject *Sender, TCloseAction &Action)
{
	ChangeDS(0);
}
//---------------------------------------------------------------------------

#pragma argsused
void __fastcall TDebugWin::BBSaveToFileClick(TObject *Sender)
{
	wchar_t szFileName[MAXFILE+4];

	//int32_t iLength;
	//int32_t count;
	if (SaveDialog1->Execute())
	{
	if (FileExists(SaveDialog1->FileName))
		{
			_wfnsplit(SaveDialog1->FileName.c_str(), 0, 0, szFileName, 0);


			wcscat(szFileName, L".BAK");
			//Removing previously created .BAK files, if found
			if (FileExists(szFileName))
			{
				DeleteFileW(szFileName);
			}
			RenameFile(SaveDialog1->FileName, szFileName);
		}

	iFileHandle = FileCreate(SaveDialog1->FileName);

	WriteToDebugFile = true;


	// Write out the number of rows and columns in the grid.
	/*count = StrGridDebug->ColCount;
	FileWrite(iFileHandle, &count, sizeof(count));
	count = StrGridDebug->RowCount;
	FileWrite(iFileHandle, &count, sizeof(count));

	for ( int32_t x=0;x<StrGridDebug->RowCount;x++)
	{
		// Write out the length of each string, followed by the string itself.
		//iLength = StrGridDebug->Cells[x][y].Length();
		//FileWrite(iFileHandle, (wchar_t *)&iLength, sizeof(iLength));
		FileWrite(iFileHandle, StrGridDebug->Cells[0][x].w_str(), StrGridDebug->Cells[x][0].Length()*sizeof(wchar_t));
		FileWrite(iFileHandle, " : " , 3);
		FileWrite(iFileHandle, StrGridDebug->Cells[1][x].w_str(), StrGridDebug->Cells[x][1].Length()*sizeof(wchar_t));

		FileWrite(iFileHandle, "eol \r\n" , 5);
	}
		FileClose(iFileHandle);     */
  }
}

//---------------------------------------------------------------------------

#pragma argsused
void __fastcall TDebugWin::BitBtn1Click(TObject *Sender)
{
	WriteToDebugFile = false;
	FileClose(iFileHandle);
}
//---------------------------------------------------------------------------

