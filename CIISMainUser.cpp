//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
//  2012-10-16  Ini-file now located in CommonAppData\Camur II
//                                              Bo H

#include <vcl.h>
#include <dir.h>
#include <shlobj.h>
#pragma hdrstop

#include "CIISMainUser.h"
#include "CIISOnTopMessage.h"
#include "UnitMySQLLogin.h"

#define ReduceWorkingSetPeriod 7200L

#define KeepDBPeriod 3600L

#define SystemSyncPeriod 10L

// 1 - Link Midas.Lib in your project
// 2 - Declare the external method DllGetDataSnapClassObject:
//
extern "C" __stdcall int32_t DllGetDataSnapClassObject (REFCLSID rclsid, REFIID riid, void** ppv);
// 3 - In startup application, form create or other point execute register Midas dll througt RegisterMidasLib:
//

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
String DBHostName ="localhost";
String DBName = "CIISUser";
String DBUserName = "root";
String DBPassword = "camur";

String CIIPMPort = "5018";
String CIIMonitorPort = "5019";
String CIILoggerPort = "5020";

TCIISMainFrm *CIISMainFrm;
//---------------------------------------------------------------------------
__fastcall TCIISMainFrm::TCIISMainFrm(TComponent* Owner)
		: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TCIISMainFrm::FormCreate(TObject *Sender)
{

  // Register MidasLib in order to NOT use MIDAS.DLL
  // Ver 3.2.7
  RegisterMidasLib(DllGetDataSnapClassObject);

  DB = NULL;
  PrjRec = NULL;
  SerInt = NULL;
  TimerSysCheck->Enabled = false;
  BBReset->Enabled = false;
  InitFaild = false;
  ReduceWorkingSetCounter = 0;
  SystemSyncCounter = 0;
  KeepDBCounter = 0;

  // Path to IniFile
  IniFilePath=GetCommonAppFolderPath() + ChangeFileExt(ExtractFileName(Application->ExeName),".Ini");


  #if Startmode == 1

  StartDebugWin = false;
  BBDebugWin->Visible = false;
  GBLoggerIP->Visible = false;
  CIISMainFrm->WindowState = wsMinimized;
  BBStartCIISUser->Visible = false;
  BBStartCIISUserClick( this );

  #elif Startmode == 2

  StartDebugWin = true;
  BBDebugWin->Visible = true;
  GBLoggerIP->Visible = false;
  BBStartCIISUser->Visible = false;
  BBStartCIISUserClick( this );

  #elif Startmode == 3

  StartDebugWin = true;
  BBDebugWin->Visible = true;
  GBLoggerIP->Visible = true;
  BBStartCIISUser->Visible = true;

  #endif

  TSysClock = 0;

  OnSlowClockTick = 10;
  if( !InitFaild ) TimerSysCheck->Enabled = true;

}
//---------------------------------------------------------------------------
void __fastcall TCIISMainFrm::FormDestroy(TObject *Sender)
{
  TimerSysCheck->Enabled = false;
  TSync = Now() + TDateTime( 10 * OneSecond );

  if( DB != NULL)
  {
	DB->CloseCIISDB();
	delete DB;
  }

  if( PrjRec != NULL ) delete PrjRec;
  delete DebWin;
  delete SerInt;
  delete ClientInt;
}

void __fastcall TCIISMainFrm::Debug(String Message)
{
  if( DebWin != NULL ) DebWin->Log(Message);
}

void __fastcall TCIISMainFrm::TimerSysCheckTimer(TObject *Sender)
{
  TimerSysCheck->Enabled = false;
  TimerStart = Now();

  ClientInt->TimerSMEvent( this );
  SerInt->TimerSMEvent( this );

  // 100 ms interval --------------------------------------------------

  if( Now() >= SClockNextEvent )
  {
		SClockEvent++;
		NextEvent_ms = SysClock * (__int64)SClockEvent;
		if( SClockEvent == INT_MAX )
		{
			SClockStart += NextEvent_ms * One_mS;
			SClockEvent = 0;
			NextEvent_ms = 0;
		}
		SClockNextEvent = SClockStart + TDateTime( NextEvent_ms * One_mS );

		//CIIMonitorServerSocket->TimerSReadEvent();
		//CIIPMServerSocket->TimerSReadEvent();
		//CIISLoggerClientSocket->TimerSReadEvent();
		SerInt->TimerSMCancelEvent( this );

		TAfterSysClock = Now();
		TSysClock += ( TAfterSysClock - TimerStart );

		// 100 ms interval --------------------------------------------------

		// 1000 ms interval---------------------------------------------------
		OnSlowClockTick--;
		if( OnSlowClockTick < 0 )
		{
			OnSlowClockTick = 10;

			LTSysValue->Caption = IntToStr( (int32_t)((double)TSysClock * 24 * 60 * 60 * 100 ));
			TSysClock = 0;

			if( ClientInt->LocalClientConnected ) ShMonitorConnected->Brush->Color = clLime;
			else ShMonitorConnected->Brush->Color = clBtnFace;

			if( ClientInt->PMConnected ) ShPMConnected->Brush->Color = clLime;
			else ShPMConnected->Brush->Color = clBtnFace;

			if( InitFaild ) CIISMainFrm->Close();

			if( SerInt->Connected() )
			{
				ShLoggerConnected->Brush->Color = clLime;

				SystemSyncCounter++;
				if ((SystemSyncCounter % SystemSyncPeriod) == 0)
				{
					SerInt->SystemSyncIni();
				}
			}
			else
			{
				ShLoggerConnected->Brush->Color = clBtnFace;
			}

			ReduceWorkingSetCounter++;
			if ((ReduceWorkingSetCounter % ReduceWorkingSetPeriod) == 0)
			{
					TrimAppMemorySize();
			}

			KeepDBCounter++;
			if ((KeepDBCounter % KeepDBPeriod) == 0)
			{
					DB->KeepDB();
			}
		}
	// 1000 ms interval---------------------------------------------------
  }

  TimerSysCheck->Enabled = true;
}

void __fastcall TCIISMainFrm::ChangeDebugState(int32_t DState)
{
  if( DState == 0 )
  {
	DebWin = NULL;
	if(SerInt != NULL ) SerInt->SetDebugWin( NULL );
	if( ClientInt != NULL ) ClientInt->SetDebugWin( NULL );
  }
}


void __fastcall TCIISMainFrm::TrimAppMemorySize(void)
{
 SetProcessWorkingSetSize(GetCurrentProcess(), -1, -1);
}



// Returns the path to the Common Application Data folder if available.
// Otherwise the path to the Application exe-file

String __fastcall TCIISMainFrm::GetCommonAppFolderPath(void)
{
  String CommonAppFolderPath;
  char szPath[MAXPATH];
  if(SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA|CSIDL_FLAG_CREATE,NULL,0,szPath) == 0)
  {
	CommonAppFolderPath=String(szPath);
	CommonAppFolderPath+= "\\CamurII\\";
	ForceDirectories(CommonAppFolderPath);
  }
  else
  {
	CommonAppFolderPath=ExtractFilePath(Application->ExeName);
  }
  return CommonAppFolderPath;
}

void __fastcall TCIISMainFrm::BBStartCIISUserClick(TObject *Sender)
{
  int8_t Answer;

  TimerSysCheck->Enabled = false;
  BBReset->Enabled = false;

  if( StartDebugWin )
  {
		DebWin = new TDebugWin(this, &ChangeDebugState);
		DebWin->Top = Top + Height;
		DebWin->Left = Left;
		DebWin->Show();
  }

  DB = new TCIISDBModule(this);
  PrjRec = new CIISPrjRec;

  TIniFile *pIniFile = new TIniFile(IniFilePath);
  if (pIniFile)
  {
		DBHostName = pIniFile->ReadString("SQL", "HostName", DBHostName);
		DBName = pIniFile->ReadString("SQL", "Database", DBName);
		DBUserName = pIniFile->ReadString("SQL", "User_Name", DBUserName);
		DBPassword = pIniFile->ReadString("SQL", "Password", DBPassword);
		delete pIniFile;
  }

  // Create the form that stays on top with a message in case we will need it below
  TFormOnTopMessage *OnTopMessage=new TFormOnTopMessage(NULL);

   // Only check MySQL-service if local MySQL - database
  if (DBHostName.Pos("localhost") || DBHostName.Pos("127.0.0.1"))
  {
		while (!DB->IsMySQLServiceStarted())
		{
			OnTopMessage->Show();
			Sleep(2000);
		}
		OnTopMessage->Hide();
		delete OnTopMessage;
	}

  TFormMySQLLogin *FormMySQLLogin=new TFormMySQLLogin(this);

  bool OkToContinue=false;

  while (!OkToContinue)
	{
		if (DB->IsLoginPossible(DBHostName,DBUserName,DBPassword))
		{
			if (DB->DatabaseExists(DBHostName,DBUserName,DBPassword,DBName))
				OkToContinue = true;
			else
			{
				String DBNotFoundMessage="Database:" + DBName + " not found!\nCreate this database? [Yes] \nTry another database name[No]";
				if (Application->MessageBox(DBNotFoundMessage.c_str(),Caption.c_str(),MB_YESNO|MB_ICONQUESTION) == IDYES) // InitCIISDB will create the database
				OkToContinue=true;
			}
		}

		if (!OkToContinue)
		{
			FormMySQLLogin->LabeledEditHostName->Text=DBHostName;
			FormMySQLLogin->LabeledEditUserName->Text=DBUserName;
			FormMySQLLogin->LabeledEditPassword->Text=DBPassword;
			FormMySQLLogin->LabeledEditDbName->Text=DBName;

			Answer = FormMySQLLogin->ShowModal();
			if (Answer == mrYes)
			{
				DBHostName = FormMySQLLogin->LabeledEditHostName->Text;
				DBUserName = FormMySQLLogin->LabeledEditUserName->Text;
				DBPassword = FormMySQLLogin->LabeledEditPassword->Text;
				DBName = FormMySQLLogin->LabeledEditDbName->Text;
			}
			else // mrNo
			{
			InitFaild = true;
			return;
			}
		}
  }
  delete FormMySQLLogin;

  // Save credentials and database-name when ok to continue
  pIniFile = new TIniFile(IniFilePath);
  if (pIniFile)
  {
		pIniFile->WriteString("SQL", "HostName",DBHostName);
		pIniFile->WriteString("SQL", "User_Name",DBUserName);
		pIniFile->WriteString("SQL", "Password",DBPassword);
		pIniFile->WriteString("SQL", "Database",DBName);

		CIIPMPort =  pIniFile->ReadString("Communication", "CIIPMPort", CIIPMPort);
		CIIMonitorPort =  pIniFile->ReadString("Communication", "CIIMonitorPort", CIIMonitorPort);
		CIILoggerPort =  pIniFile->ReadString("Communication", "CIILoggerPort", CIILoggerPort);

		delete pIniFile;
  }

  if( DB->InitCIISDB(DBHostName, DBName, CIISRemoteServer, DBUserName, DBPassword, DebWin ))
  {
		SerInt = new TCIISServerInterface( DB, DebWin, EdLocalIP->Text , StrToInt( CIILoggerPort ));
		CIISLoggerClientSocket = SerInt->GetClientSocket();

		ClientInt = new TCIISClientInterfaceUser( DB, DebWin, SerInt, StrToInt( CIIMonitorPort ), StrToInt( CIIPMPort ));
		CIIMonitorServerSocket = ClientInt->GetMSocket();
		CIIPMServerSocket = ClientInt->GetPMSocket();

		BBReset->Enabled = false;
		DB->GetPrjRec( PrjRec );
		PrjRec->ServerStatus = CIISDisconnected;
		DB->SetPrjRec( PrjRec ); DB->ApplyUpdatesPrj();

		DB->LogEvent( SystemEvent, CIIEventCode_ServerStart, CIIEventLevel_High, "ServerStart", 0, 0, PrjRec->EventLogSize );
		TimerSysCheck->Enabled = true;
  }
  else InitFaild = true;
}


void __fastcall TCIISMainFrm::BBDebugWinClick(TObject *Sender)
{

  DebWin = new TDebugWin(this, &ChangeDebugState);
  DebWin->Top = Top + Height;
  DebWin->Left = Left;
  DebWin->Show();

  if(SerInt != NULL ) SerInt->SetDebugWin( DebWin );
  if( ClientInt != NULL ) ClientInt->SetDebugWin( DebWin );

}
//---------------------------------------------------------------------------
void __fastcall TCIISMainFrm::FormShow(TObject *Sender)
{
  CIISMainFrm->Caption = "Camur II Server Ver " + GetProgVersion();
}
//---------------------------------------------------------------------------









//---------------------------------------------------------------------------


