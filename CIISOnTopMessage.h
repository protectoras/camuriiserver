//---------------------------------------------------------------------------

#ifndef CIISOnTopMessageH
#define CIISOnTopMessageH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormOnTopMessage : public TForm
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TFormOnTopMessage(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormOnTopMessage *FormOnTopMessage;
//---------------------------------------------------------------------------
#endif
