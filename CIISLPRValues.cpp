//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISLPRValues.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



__fastcall TCIISLPRValues::TCIISLPRValues(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISLPRValueRec *SetLPRValRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISValuesLPR;
  LPRValRec = SetLPRValRec;
}

__fastcall TCIISLPRValues::~TCIISLPRValues()
{
  delete LPRValRec;
}

void __fastcall TCIISLPRValues::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "LPRValues: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;	
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISLPRValues::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "LPRValues: ParseCommand_End" );
	#endif
}


bool __fastcall TCIISLPRValues::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( LPRValRec->DateTimeStamp );
}

void __fastcall TCIISLPRValues::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
		if( P->GetArg( 200 ) == "GetFirstToEnd" )
			{
			if( DB->FindFirstLPRValue() )P->CommandMode = CIISRecAdd;
			else
			{
				O->MessageCode = CIISMsg_RecNotFound;
				P->CommandMode = CIISCmdReady;
			}
		}
		else if( P->GetArg( 200 ) == "GetNextToEnd" )
		{
			if( P->ArgInc(1) )
			{
				if( DB->LocateLPRValue( CIISStrToDateTime( P->GetArg(1) ) ) )
				{
					if( DB->FindNextLPRValue() ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_Ok;
						P->CommandMode = CIISCmdReady;
					}
				}
				else if( DB->FindFirstLPRValue() )
				{
					DB->GetLPRValueRec( LPRValRec, false );
					if( CIISStrToDateTime( P->GetArg(1) ) < LPRValRec->DateTimeStamp ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_RecNotFound;
						P->CommandMode = CIISCmdReady;
					}
				}
				else
				{
					O->MessageCode = CIISMsg_RecNotFound;
					P->CommandMode = CIISCmdReady;
				}
			}
			else if( P->ArgInc(99) )
			{
				O->MessageCode = CIISMsg_NoLongerSupported;
			}
		}
		else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
	}

  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {
	do
	{
	  MoreRecords = DB->GetLPRValueRec( LPRValRec, true );
	  O->MessageData = O->MessageData +
	  "1=" + CIISDateTimeToStr(LPRValRec->DateTimeStamp) + "\r\n" +
	  "2=" + IntToStr( LPRValRec->RecNo ) + "\r\n" +
	  "3=" + IntToStr( LPRValRec->SensorSerialNo ) + "\r\n" +
	  "4=" + CIISFloatToStr( LPRValRec->ValueV ) + "\r\n" +
	  "5=" + CIISFloatToStr( LPRValRec->ValueI ) + "\r\n" +
	  "6=" + LPRValRec->ValueType + "\r\n";
	} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );

	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	if( P->ArgInc(1) )
	{
	   LPRValRec->DateTimeStamp = CIISStrToDateTime( P->GetArg(1) );
	   O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetLPRValuesRecCountAfter( LPRValRec ) ) + "\r\n";
	}
	else
	{
	  O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetLPRValuesRecCount() ) + "\r\n";
	}
	
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISLPRValues::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISLPRValues::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISLPRValues::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}
