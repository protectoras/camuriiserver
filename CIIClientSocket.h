//---------------------------------------------------------------------------

#ifndef CIIClientSocketH
#define CIIClientSocketH
#include "DebugWinU.h"
#include <System.Win.scktcomp.hpp>

//---------------------------------------------------------------------------

class TCIIClientSocket : public TObject
{
private:
  TDebugWin *DW;
	TList *MessageList;
	TClientSocket *ClientSocket;
	TCustomWinSocket *CWSocket;
	MessInQueueFP MInQ;
	int ECode;
	int FT_Timeout;
	bool FComTimeout;
  TDateTime FTOut;

	char SocketInBuf[SocketInBufSize];
	char SocketOutBuf[SocketOutBufSize];
	char SocketInputQueue[SocketInputQueueSize];

  String IPAdr;
	int SocketInputIdx, PortNo;
  Word ResponseCount;
	bool FServerConnected;

	void __fastcall Debug(String Message);
	void __fastcall AddSocketInputQueue(char* Buf, int N);
  bool __fastcall ParseMessages();

protected:

public:
  __fastcall TCIIClientSocket(MessInQueueFP SetMInQ, String SetIP, int SetPortNo, TDebugWin * SetDebugWin);
  __fastcall ~TCIIClientSocket();


  TCamurPacket* __fastcall GetReply(int RequestNo);
	void __fastcall SendMessage( TCamurPacket *Request );

	void __fastcall ServerConnect(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerLookup(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerDisconnect(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode);
	void __fastcall ServerRead(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall SetDebugWin(TDebugWin * SetDebugWin);
	void __fastcall OpenCom(String SetIP);
	void __fastcall CloseCom();
	void __fastcall SetComTimeout( int SetT_Timeout );


__published:
	__property bool ServerConnected  = { read=FServerConnected };
	__property bool ComTimeout = { read = FComTimeout };

};
#endif
