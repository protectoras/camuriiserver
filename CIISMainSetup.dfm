object CIISMainLoggerFrm: TCIISMainLoggerFrm
  Left = 1247
  Top = 53
  Caption = 'Camur II Server'
  ClientHeight = 336
  ClientWidth = 491
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object BBReset: TBitBtn
    Left = 372
    Top = 295
    Width = 92
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Reset'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 0
    OnClick = BBResetClick
  end
  object BBDebugWin: TBitBtn
    Left = 276
    Top = 295
    Width = 92
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Debug'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555500000000
      0555555F7777777775F55500FFFFFFFFF0555577F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF777FFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF7F777FF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF77F7FFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7FF5F5F5F57F550F00F0F0F0F
      005557F77F7F7F7F77555055070707070555575F7F7F7F7F7F55550507070707
      0555557575757575755555505050505055555557575757575555}
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BBDebugWinClick
  end
  object GBServerPortsConnected: TGroupBox
    Left = 23
    Top = 10
    Width = 189
    Height = 85
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Active Client'
    TabOrder = 2
    object LCIIMonitor: TLabel
      Left = 55
      Top = 25
      Width = 33
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Local'
    end
    object LCIIUser: TLabel
      Left = 55
      Top = 48
      Width = 48
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Remote'
    end
    object ShMonitorConnected: TShape
      Left = 20
      Top = 27
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object ShUserConnected: TShape
      Left = 20
      Top = 47
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
  end
  object GBCamurIILogger: TGroupBox
    Left = 276
    Top = 103
    Width = 188
    Height = 146
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Camur II Bus'
    TabOrder = 3
    object LCIIBusInt: TLabel
      Left = 60
      Top = 25
      Width = 84
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Bus Interfaces'
    end
    object LNodes: TLabel
      Left = 60
      Top = 64
      Width = 109
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Connected Nodes'
    end
    object LNoOfNodes: TLabel
      Left = 25
      Top = 64
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LNoOfBusInt: TLabel
      Left = 25
      Top = 25
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LNoOfDetecedNodes: TLabel
      Left = 25
      Top = 44
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LDetectedNodes: TLabel
      Left = 60
      Top = 44
      Width = 99
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Detected Nodes'
    end
    object LNodeIniErrors: TLabel
      Left = 60
      Top = 84
      Width = 89
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Node Ini Errors'
    end
    object LNoOfNodIniErrors: TLabel
      Left = 25
      Top = 84
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LTCANRxValue: TLabel
      Left = 25
      Top = 105
      Width = 7
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = '0'
    end
    object LTCANRx: TLabel
      Left = 60
      Top = 105
      Width = 111
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'T CAN Rx ( mS/S )'
    end
    object LTSysValue: TLabel
      Left = 25
      Top = 124
      Width = 7
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = '0'
    end
    object LTSys: TLabel
      Left = 60
      Top = 124
      Width = 81
      Height = 16
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = 'T Sys (mS/S)'
    end
  end
  object GBRecordings: TGroupBox
    Left = 276
    Top = 10
    Width = 188
    Height = 85
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Recordings'
    TabOrder = 4
    object LNoOfMonitors: TLabel
      Left = 25
      Top = 25
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LMonitors: TLabel
      Left = 60
      Top = 25
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Monitor Rec'
    end
    object LNoOfDecays: TLabel
      Left = 25
      Top = 44
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LDecays: TLabel
      Left = 60
      Top = 44
      Width = 68
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Decay Rec'
    end
    object NoOfLPRs: TLabel
      Left = 25
      Top = 64
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
      Color = clBtnFace
      ParentColor = False
    end
    object LLPRs: TLabel
      Left = 60
      Top = 64
      Width = 54
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'LPR Rec'
    end
  end
  object BBStartCIISLoggger: TBitBtn
    Left = 276
    Top = 257
    Width = 188
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Start CIISLogger'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 5
    OnClick = BBStartCIISLogggerClick
  end
  object BBSetupNode: TBitBtn
    Left = 19
    Top = 295
    Width = 107
    Height = 33
    Caption = 'Setup node'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
      033333777777777773333330777777703333333773F333773333333330888033
      33333FFFF7FFF7FFFFFF0000000000000003777777777777777F0FFFFFFFFFF9
      FF037F3333333337337F0F78888888887F037F33FFFFFFFFF37F0F7000000000
      8F037F3777777777F37F0F70AAAAAAA08F037F37F3333337F37F0F70ADDDDDA0
      8F037F37F3333337F37F0F70A99A99A08F037F37F3333337F37F0F70A99A99A0
      8F037F37F3333337F37F0F70AAAAAAA08F037F37FFFFFFF7F37F0F7000000000
      8F037F3777777777337F0F77777777777F037F3333333333337F0FFFFFFFFFFF
      FF037FFFFFFFFFFFFF7F00000000000000037777777777777773}
    NumGlyphs = 2
    TabOrder = 6
    OnClick = BBSetupNodeClick
  end
  object GBSystem: TGroupBox
    Left = 24
    Top = 216
    Width = 185
    Height = 57
    Caption = 'System'
    TabOrder = 7
    object LCtrlMode: TLabel
      Left = 17
      Top = 24
      Width = 95
      Height = 16
      Caption = 'Controller Mode'
    end
  end
  object TimerSysCheck: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerSysCheckTimer
    Left = 224
    Top = 32
  end
end
