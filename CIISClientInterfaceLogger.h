//---------------------------------------------------------------------------

#ifndef CIISClientInterfaceLoggerH
#define CIISClientInterfaceLoggerH

#include "CIISDB.h"
#include "CIISProject.h"
#include "DebugWinU.h"
#include <System.Win.ScktComp.hpp>
#include "CIIServerSocket.h"

//---------------------------------------------------------------------------

class TCIISClientInterfaceLogger : public TObject
{
private:
  TCIISProject *Prj;
  TDebugWin *DW;
  int PortNoLocal, PortNoRemote;
  TCIIServerSocket *LSocket, *RSocket, *CurrSocket;

  String UserLevel, Password, MonUserLevel, MonPassword;

  void __fastcall Debug(String Message);
  bool __fastcall ParseCommands();
  void __fastcall GetLocalMessage();
  void __fastcall GetRemoteMessage();
  bool __fastcall GetLocalClientConnected();
	bool __fastcall GetRemoteClientConnected();
	bool __fastcall GetRemoteClientAktiv();


protected:

public:
  __fastcall TCIISClientInterfaceLogger( TCIISProject *SetPrj,
										 TDebugWin * SetDebugWin,
										 int SetPortNoLocal,
										 int SetPortNoRemote );
	__fastcall ~TCIISClientInterfaceLogger();

	TCIIServerSocket* __fastcall GetLSocket();
	TCIIServerSocket* __fastcall GetRSocket();

  void __fastcall DisconnectRemoteClient();

  void __fastcall SetDebugWin(TDebugWin * SetDebugWin);

__published:
  __property bool LocalClientConnected  = { read=GetLocalClientConnected };
	__property bool RemoteClientConnected  = { read=GetRemoteClientConnected };
	__property bool RemoteClientAkltiv = { read=GetRemoteClientAktiv };

};


#endif
