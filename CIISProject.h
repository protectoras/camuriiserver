//---------------------------------------------------------------------------

#ifndef CIISProjectH
#define CIISProjectH
//---------------------------------------------------------------------------

#include "CIISCommon.h"
#include "CIISObj.h"
#include "CIISController.h"
#include "CIISEvents.h"
#include "CIISRecordings.h"
#include "CIISMonitorValues.h"
#include "CIISLPRValues.h"
#include "CIISCalcValues.h"
#include "CIISDecayValues.h"
#include "CIISMiscValues.h"
#include <Classes.hpp>
#include "limits.h"

class TCIISProject : public TCIISObj
{
private:
//CIISClientInt
  CIISPrjRec *PR;

  CIISEventRec *ER;
  CIISRecordingRec *RR;
  CIISMonitorValueRec *MR;
	CIISLPRValueRec *LR;
  CIISCalcValueRec *CalcR;
  CIISDecayValueRec *DR;
  CIISMiscValueRec *MiscR;

  TCIISEvents *Events;
  TCIISRecordings *Recordings;
  TCIISMonitorValues *MonitorValues;
	TCIISLPRValues *LPRValues;
	TCIISCalcValues *CalcValues;
  TCIISDecayValues *DecayValues;
  TCIISMiscValues *MiscValues;

  int32_t FServerStatus;

	//CIISBusInt

	TList *BusIntList;
	TCIISBusInterface *CIISBusInterface;
	int32_t PrjUpdateAlarmDelay;
  bool FAlarmStatusChanged;
  int32_t OnKeepMySQLAlive;
	int32_t OnSlowClockTick;

	//Reset req from CII Monitor

	bool FReqReset;

  //CIISShutDown

  bool FPowerShutdown, FShutdownReady, ShutdownNow;
  int32_t ShutDownDelay;
	int32_t T, TNextEvent, ShutDownState;
	void __fastcall SM_Shutdown();
	bool __fastcall CII_Controller_Shutdown();

	// uCtrl

	int32_t  FuCtrlBytesRead;
	bool FuCtrlRecordingOn, FuCtrlReadingData, FuCtrlPowerIn,
			 FuCtrlPowerOn, FuCtrlPowerOk, FuCtrlSyncTime, FuCtrlSyncData,
			 FShowuCtrlInfo, FuCtrlStoringData;

	TDateTime FuCtrlTime, FuCtrlSampleTimeStamp;

  //CIISClientInt
  CIIServerStatus __fastcall Get_ServerStatus() {return PR->ServerStatus;}
  void __fastcall Set_ServerStatus( CIIServerStatus SStat );
  CIISServerMode  __fastcall Get_ServerMode() { return PR->ServerMode;}
  void  __fastcall Set_ServerMode ( CIISServerMode SMode );

  bool __fastcall ThisRec( TCamurPacket *P );
  void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O );
  void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );
  void __fastcall Login( TCamurPacket *P, TCamurPacket *O );
  void __fastcall Logout( TCamurPacket *P, TCamurPacket *O );
  void __fastcall DeleteCtrl( TCamurPacket *P );
  void __fastcall SetAlarmStatusChanged( bool AlarmStatusChanged );
  void __fastcall UpdateAlarmStatus();

  int32_t __fastcall GetMiscSize() { return PR->ValuesMiscSize; }

  //CIISBusInt

  int32_t __fastcall GetNodeCount();
  int32_t __fastcall GetDetectedNodeCount();
  int32_t __fastcall GetIniErrorCount();
	int32_t __fastcall GetMonitorCount();
	int32_t __fastcall GetMonitorExtCount();
  int32_t __fastcall GetDecayCount();
  int32_t __fastcall GetLPRCount();
	int32_t __fastcall GetUSBCANCount();
	bool __fastcall GetAlarmStatus() { return PR->PrjAlarmStatus; }

	// uCtrl



protected:
  //CIISClientInt
  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt

	void __fastcall OnSysClockTick( TDateTime TickTime );
	void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
  //CIISClientInt
	__fastcall TCIISProject( TCIISDBModule *SetDB, TDebugWin *SetDebugWin );
	__fastcall ~TCIISProject();

	void __fastcall SysClockEvent();

bool  __fastcall ResetCIISBus();
void  __fastcall LastValChanged();
void  __fastcall DataChanged();
void  __fastcall ConfigChanged();
void __fastcall PrepareShutdown();

void  __fastcall Log( CIIEventType EvType, CIIEventCode EvCode,
						CIIEventLevel EvLevel, String EvString,
						int32_t, double EvFloat);

	TCIISController* __fastcall GetCtrl( int32_t Ctrl_No );

	TList* __fastcall GetBusIntList();

void __fastcall TestBI();


__published:

	__property CIIServerStatus ServerStatus  = { read = Get_ServerStatus , write = Set_ServerStatus };
	__property CIISServerMode ServerMode  = { read = Get_ServerMode, write = Set_ServerMode };

	__property int32_t NodeCount = { read = GetNodeCount };
	__property int32_t DetectedNodeCount = { read = GetDetectedNodeCount };
	__property int32_t IniErrorCount = { read = GetIniErrorCount };
	__property int32_t MonitorCount = { read = GetMonitorCount };
	__property int32_t MonitorExtCount = { read = GetMonitorExtCount };
	__property int32_t DecayCount = { read = GetDecayCount };
	__property int32_t LPRCount = { read = GetLPRCount };
	__property int32_t USBCANCount = { read = GetUSBCANCount };
	__property int32_t MiscSize = { read = GetMiscSize };

	__property bool AlarmStatusChanged = { write = SetAlarmStatusChanged };
	__property bool AlarmStatus = { read = GetAlarmStatus };
	__property bool PowerShutdown = { read = FPowerShutdown, write = FPowerShutdown};
	__property bool ShutdownReady = { read = FShutdownReady, write = FShutdownReady};

  __property bool ReqReset = { read = FReqReset, write = FReqReset };

	__property bool ShowuCtrlInfo = { read = FShowuCtrlInfo, write = FShowuCtrlInfo };
	__property bool uCtrlRecordingOn = { read = FuCtrlRecordingOn, write = FuCtrlRecordingOn };
	__property bool uCtrlReadingData = { read = FuCtrlReadingData, write = FuCtrlReadingData };
	__property bool uCtrlStoringData = { read = FuCtrlStoringData, write = FuCtrlStoringData };
	__property TDateTime uCtrlTime = { read = FuCtrlTime, write = FuCtrlTime };
	__property bool uCtrlSyncTime = { read = FuCtrlSyncTime, write = FuCtrlSyncTime};
	__property bool uCtrlSyncData = { read = FuCtrlSyncData, write = FuCtrlSyncData};
	__property TDateTime uCtrlSampleTimeStamp = { read = FuCtrlSampleTimeStamp, write = FuCtrlSampleTimeStamp};
	__property int32_t uCtrlBytesRead = { read = FuCtrlBytesRead, write = FuCtrlBytesRead };
	__property bool uCtrlPowerIn = { read = FuCtrlPowerIn, write = FuCtrlPowerIn};
	__property bool uCtrlPowerOn = { read = FuCtrlPowerOn, write = FuCtrlPowerOn};
	__property bool uCtrlPowerOk = { read = FuCtrlPowerOk, write = FuCtrlPowerOk};

	};

#endif
