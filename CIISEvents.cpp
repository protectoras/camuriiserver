//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISEvents.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISEvents::TCIISEvents(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISEventRec *SetEventRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISEvent;
  EventRec = SetEventRec;

  CIISObjType = CIISEvent;
}

__fastcall TCIISEvents::~TCIISEvents()
{
  delete EventRec;
}

void __fastcall TCIISEvents::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Events: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISEvents::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Events: ParseCommand_Begin" );
	#endif
}

bool __fastcall TCIISEvents::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( EventRec->EventDateTime );
}

void __fastcall TCIISEvents::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
	if( P->GetArg( 200 ) == "GetFirstToEnd" )
	{
	  if( DB->FindFirstEvent() )P->CommandMode = CIISRecAdd;
	  else
	  {
		O->MessageCode = CIISMsg_RecNotFound;
		P->CommandMode = CIISCmdReady;
      }
	}
	else if( P->GetArg( 200 ) == "GetNextToEnd" )
	{
	  if( P->ArgInc(1) && P->ArgInc(5) )
	  {
		if( DB->LocateEvent( CIISStrToDateTime( P->GetArg(1) ), StrToInt( P->GetArg(5) ) ) )
		{
		  if( DB->FindNextEvent() ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_Ok;
			P->CommandMode = CIISCmdReady;
          }
		}
		else
		{
		  O->MessageCode = CIISMsg_RecNotFound;
		  P->CommandMode = CIISCmdReady;
		}
	  }
	}
	else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
  }
  
  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {
	do
	{
	  MoreRecords = DB->GetEventRec( EventRec, true );
	  O->MessageData = O->MessageData +
	  "1=" + CIISDateTimeToStr( EventRec->EventDateTime ) + "\r\n" +
	  "2=" + IntToStr( EventRec->EventType ) + "\r\n" +
	  "3=" + IntToStr( EventRec->EventCode ) + "\r\n" +
	  "4=" + IntToStr( EventRec->EventLevel ) + "\r\n" +
	  "5=" + IntToStr( EventRec->EventNo ) + "\r\n" +
	  "6=" + EventRec->EventString + "\r\n" +
	  "7=" + IntToStr( EventRec->EventInt ) + "\r\n" +
	  "8=" + CIISFloatToStr( EventRec->EventFloat ) + "\r\n";
	} while( O->MessageData.Length() < OutMessageLimit && MoreRecords );
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetEventLogRecCount() ) + "\r\n";;
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISEvents::OnSysClockTick( TDateTime TickTime )
{

}

#pragma argsused
void __fastcall TCIISEvents::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}

#pragma argsused
void __fastcall TCIISEvents::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void  __fastcall TCIISEvents::Log( CIIEventType EvType, CIIEventCode EvCode,
								   CIIEventLevel EvLevel, String EvString,
								   int32_t EvInt, double EvFloat, int32_t EvLogSize)
{
  DB->LogEvent( EvType, EvCode, EvLevel, EvString, EvInt, EvFloat, EvLogSize);
}								   
