//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop















































USEFORM("CIISOnTopMessage.cpp", FormOnTopMessage);
USEFORM("DebugWinU.cpp", DebugWin);
USEFORM("CIISZoneSetup.cpp", FrmZoneSetup);
USEFORM("UnitMySQLLogin.cpp", FormMySQLLogin);
USEFORM("CIISSetupSelect.cpp", FrmCalibSelect);
USEFORM("CIISControllerSetup.cpp", FrmControllerSetup);
USEFORM("CIISMainSetup.cpp", CIISMainLoggerFrm);
USEFORM("CIISDB.cpp", CIISDBModule); /* TDataModule: File Type */
USEFORM("CIISDBInit.cpp", CIISDBInitFrm);
USEFORM("CIISCTx8Setup.cpp", FrmCTx8SetUp);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Camur II Server";
		Application->CreateForm(__classid(TCIISMainLoggerFrm), &CIISMainLoggerFrm);
		Application->CreateForm(__classid(TCIISDBInitFrm), &CIISDBInitFrm);
		Application->CreateForm(__classid(TFormOnTopMessage), &FormOnTopMessage);
		Application->CreateForm(__classid(TFormMySQLLogin), &FormMySQLLogin);
		Application->CreateForm(__classid(TFrmControllerSetup), &FrmControllerSetup);
		Application->CreateForm(__classid(TFrmCalibSelect), &FrmCalibSelect);
		Application->CreateForm(__classid(TFrmZoneSetup), &FrmZoneSetup);
		Application->CreateForm(__classid(TFrmCTx8SetUp), &FrmCTx8SetUp);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
