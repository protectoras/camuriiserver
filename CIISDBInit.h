//---------------------------------------------------------------------------

#ifndef CIISDBInitH
#define CIISDBInitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TCIISDBInitFrm : public TForm
{
__published:	// IDE-managed Components
	TMemo *MemoDB;
private:	// User declarations
public:
	void __fastcall Msg( String msg );
	void __fastcall MsgCl( TColor cl );
			 __fastcall TCIISDBInitFrm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCIISDBInitFrm *CIISDBInitFrm;
//---------------------------------------------------------------------------
#endif
