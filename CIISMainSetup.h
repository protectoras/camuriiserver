//---------------------------------------------------------------------------

#ifndef CIISMainSetupH
#define CIISMainSetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CIISDB.h"
#include "DebugWinU.h"
#include "CIISClientInterfaceLogger.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ScktComp.hpp>
#include <ComCtrls.hpp>
#include <Mask.hpp>

#include "CIISProject.h"

#include "CIISCorrRate.h"

#define MYSQL_SERVICE_START_TIMEOUT 10 // seconds

#define Startmode 2

// 1 : LocalServer ( Logger )
// 2 : LocalServer + DebugWin
// 3 : LocalServer + DebugWin + Selectable port no.

//---------------------------------------------------------------------------
class TCIISMainLoggerFrm : public TForm
{
__published:	// IDE-managed Components
		TBitBtn *BBReset;
        TTimer *TimerSysCheck;
        TBitBtn *BBDebugWin;
	TGroupBox *GBServerPortsConnected;
	TLabel *LCIIMonitor;
	TLabel *LCIIUser;
	TGroupBox *GBCamurIILogger;
	TLabel *LCIIBusInt;
	TLabel *LNodes;
	TLabel *LNoOfNodes;
	TLabel *LNoOfBusInt;
	TGroupBox *GBRecordings;
	TLabel *LNoOfMonitors;
	TLabel *LMonitors;
	TLabel *LNoOfDecays;
	TLabel *LDecays;
	TLabel *NoOfLPRs;
	TLabel *LLPRs;
	TShape *ShMonitorConnected;
	TShape *ShUserConnected;
	TBitBtn *BBStartCIISLoggger;
	TLabel *LNoOfDetecedNodes;
	TLabel *LDetectedNodes;
	TLabel *LNodeIniErrors;
	TLabel *LNoOfNodIniErrors;
	TBitBtn *BBSetupNode;
	TGroupBox *GBSystem;
	TLabel *LCtrlMode;
	TLabel *LTCANRxValue;
	TLabel *LTCANRx;
	TLabel *LTSysValue;
	TLabel *LTSys;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall BBResetClick(TObject *Sender);
        void __fastcall TimerSysCheckTimer(TObject *Sender);
        void __fastcall ChangeDebugState(int State);
        void __fastcall BBDebugWinClick(TObject *Sender);
		void __fastcall FormShow(TObject *Sender);
	void __fastcall BBStartCIISLogggerClick(TObject *Sender);
	void __fastcall BBSetupNodeClick(TObject *Sender);

private:	// User declarations
  TDebugWin *DebWin;
	TCIISDBModule *DB;
	TCIISClientInterfaceLogger *ClientInt;
	bool StartDebugWin, InitFaild, DatabaseCreated, ShutDownServerAfterNewDb;
	TDateTime TSync;
	String __fastcall GetCommonAppFolderPath(void);
	void __fastcall TrimAppMemorySize(void);

	String IniFilePath;

	TDateTime TimerStart, TAfterReadCAN, TAfterSysClock, TReadCAN, TSysClock;
	double TFix;
  void __fastcall Debug(String Message);

  // Test CIISObj
	TCIISProject *Prj;
	TList *BusIntList;
	TCIISBusInterface *CIISBusInterface;
	TCIIServerSocket *LSocket, *RSocket;
	__int64 NextEvent_ms;
	TDateTime  SClockNextEvent, SClockStart;
	int SClockEvent;
	int OnSlowClockTick;
	int ReduceWorkingSetCounter;
	bool SysClockEnabled;

public:		// User declarations
  __fastcall TCIISMainLoggerFrm(TComponent* Owner);



};
//---------------------------------------------------------------------------
extern PACKAGE TCIISMainLoggerFrm *CIISMainLoggerFrm;
//---------------------------------------------------------------------------
#endif


