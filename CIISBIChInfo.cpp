//---------------------------------------------------------------------------

#pragma hdrstop

#include "CIISProject.h"
#include "CIISBIChInfo.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

__fastcall TCIISBIChInfo::TCIISBIChInfo(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
																				TDebugWin *SetDebugWin, CIISBIChannelRec *SetBIChRec,
																				TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
	CIISObjType = CIISBIChannel;
	BIChRec = SetBIChRec;

  P_Ctrl = (TCIISController*)CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	#if DebugCIISStart == 1

	Debug( "BIChInfo created: " + IntToStr( BIChRec->IP ) + "/" + IntToStr( BIChRec->SerialNo ));

	#endif

}

__fastcall TCIISBIChInfo::~TCIISBIChInfo()
{
	delete BIChRec;
}

void __fastcall TCIISBIChInfo::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
  switch( P->MessageCommand )
  {
	case CIISRead:
		if( P->UserLevel != ULNone )
		{
			ReadRec( P, O );
		}
		else
		{
			O->MessageCode = CIISMsg_UserLevelError;
		}
		break;

	case CIISWrite:
		if( ThisRec( P ) )
		{
			if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
			{
				WriteRec( P, O );
			}
			else
			{
				O->MessageCode = CIISMsg_UserLevelError;
			}
		}
		break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISBIChInfo::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{

}

bool __fastcall TCIISBIChInfo::ThisRec( TCamurPacket *P )
{
	return P->GetArg(1) == BIChRec->SerialNo;
}

void __fastcall TCIISBIChInfo::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
		if( P->ArgInc( 200 ) )
		{
			if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
			else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
		}
	}

	if( P->CommandMode == CIISRecSearch )
	{
		if( ThisRec( P ) )
		{
			if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_RecNotFound;
	}
	else if( P->CommandMode == CIISRecAdd )
	{
		O->MessageData = O->MessageData +
		"1=" + IntToStr( BIChRec->SerialNo ) + "\r\n" +
		"2=" + IntToStr( BIChRec->Type ) + "\r\n"
		"3=" + BIChRec->Name + "\r\n" +
		"4=" + CIISBoolToStr( BIChRec->Included ) + "\r\n" +
		"5=" + CIISBoolToStr( BIChRec->Active ) + "\r\n" +
		"6=" + CIISBoolToStr( BIChRec->WatchDog ) + "\r\n" +
		"7=" + IntToStr( BIChRec->SerialNo ) + "\r\n" +
		"8=" + IntToStr( BIChRec->Mode ) + "\r\n" +
		"9=" + BIChRec->IP + "\r\n" +
		"10=" + BIChRec->StaticIP + "\r\n" +
		"11=" + BIChRec->Gateway + "\r\n" +
		"12=" + BIChRec->Netmask + "\r\n" +
		"13=" + BIChRec->MACAddress + "\r\n" +
		"14=" + IntToStr( BIChRec->VerMajor ) + "\r\n"
		"15=" + IntToStr( BIChRec->VerMinor ) + "\r\n" ;

		O->MessageCode = CIISMsg_Ok;

		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
	}

  else if( P->CommandMode == CIISRecCount )
  {
		O->MessageData = O->MessageData + "100=" + IntToStr((int) DB->GetBIChannelsRecCount() ) + "\r\n";;
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
	else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISBIChInfo::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->ArgInc(200) )
	{
		O->MessageCode = CIISMsg_UnknownCommand;
	}
	else if( DB->LocateBIChannelRec( BIChRec ))
	{
		if( P->ArgInc(2) ) BIChRec->Name = P->GetArg(2);
		if( P->ArgInc(3) ) BIChRec->Included = CIISStrToBool( P->GetArg(3));
		if( P->ArgInc(4) ) BIChRec->WatchDog = CIISStrToBool( P->GetArg(4));
		if( P->ArgInc(5) ) BIChRec->WDTime = StrToInt( P->GetArg(5) );
		if( P->ArgInc(6) ) BIChRec->Mode = StrToInt( P->GetArg(6) );
		if( P->ArgInc(7) ) BIChRec->StaticIP = P->GetArg(7);
		if( P->ArgInc(8) ) BIChRec->Gateway = P->GetArg(8);
		if( P->ArgInc(9) ) BIChRec->Netmask = P->GetArg(9);


		if( DB->SetBIChannelRec( BIChRec ) )
		{
			DB->ApplyUpdatesBIChannels();
			P_Prj->Log( ClientCom, CIIEventCode_BIChTabChanged, CIIEventLevel_High, "", 0, 0 );
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;
  }
  else O->MessageCode = CIISMsg_RecNotFound;

  P->CommandMode = CIISCmdReady;

	if( P->ArgInc(5) || P->ArgInc(6) )
	{
		// s�tt nya v�rden
  }

}

#pragma argsused
void __fastcall TCIISBIChInfo::OnSysClockTick( TDateTime TickTime )
{

}

#pragma argsused
void __fastcall TCIISBIChInfo::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}

#pragma argsused
void __fastcall TCIISBIChInfo::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}