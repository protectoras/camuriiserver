//---------------------------------------------------------------------------

#ifndef CIISControllerH
#define CIISControllerH
//---------------------------------------------------------------------------

#include "CIISObj.h"
#include "CIISZone.h"
#include "CIISNodeIni.h"

	enum RestartCtrlState
	{
		RB_RestartBusInterface,
		RB_WaitForBusIni,
		RB_ReqBIMode,
		RB_RestartPowerSupplys,
		RB_RestartRecordings,
		RB_RestartWatchDog,
    RB_CancelDecay,
		RB_RestartZones,
		RB_Ready,
		RB_TimeOut
	};



class TCIISController : public TCIISObj
{
private:

	int32_t NoOfZones;
	TCIISBusPacket *BPIn;
	bool StartScheduleRunning;
	bool ResetCIISBusRunning;
	int32_t ResetBusState;

	int32_t T2, T2NextEvent, SheduleNextZone;
  int32_t DecayZoneDelay, MonitorStartDelay;
	int32_t OnSlowClockTick;
	bool DisableEthernet;
	bool FApplyNewRecording, FApplyNewDecaySample, FApplyNewMonitorSample, FApplyNewZone;

  bool __fastcall ThisRec( TCamurPacket *P );
  void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O );
  void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );
  void __fastcall AddNode( TCIISNodeIni *Node );
  void __fastcall AddSensor( TCIISSensor *Sensor );
  void __fastcall AddPS( TCIISPowerSupply *PS );
  void __fastcall AddZone( TCamurPacket *P, TCamurPacket *O  );
  void __fastcall DeleteZone( TCamurPacket *P );
  void __fastcall DeleteWLink( TCamurPacket *P );
  void __fastcall AddNewNode( TCIISBusPacket *BPIn );
	void __fastcall UpdateNode( TCIISBusPacket *BPIn );

	void __fastcall SM_ResetCIISBus();
	void __fastcall SM_RestartCIISBus();
	void __fastcall StartSchedule();
	void __fastcall IncrementScheduleDateTime();
	void __fastcall SM_StartSchedule();
	bool __fastcall GetNoIR() { return CtrlRec->NoIR; }
  int32_t __fastcall GetLPRRange() { return CtrlRec->LPRRange; }
  int32_t __fastcall GetLPRStep() { return CtrlRec->LPRStep; }
  int32_t __fastcall GetLPRDelay1() { return CtrlRec->LPRDelay1; }
  int32_t __fastcall GetLPRDelay2() { return CtrlRec->LPRDelay2; }
  int32_t __fastcall GetLPRMode() { return CtrlRec->LPRMode; }
  int32_t __fastcall GetDecaySampInterval() { return CtrlRec->DecaySampInterval; }
  int32_t __fastcall GetDecaySampInterval2() { return CtrlRec->DecaySampInterval2; }
  int32_t __fastcall GetDecayDuration() { return CtrlRec->DecayDuration; }
  int32_t __fastcall GetDecayDuration2() { return CtrlRec->DecayDuration2; }
  int32_t __fastcall GetDecayDelay() { return CtrlRec->DecayDelay; }
	int32_t __fastcall GetNodeCount() { return CtrlRec->NodeCount; }
	int32_t __fastcall GetDetectedNodeCount() { return CtrlRec->DetectedNodeCount; }
  int32_t __fastcall GetIniErrorCount () { return CtrlRec->IniErrorCount; }
	int32_t __fastcall GetMonitorCount() { return CtrlRec->MonitorCount; }
	int32_t __fastcall GetMonitorExtCount() { return CtrlRec->MonitorExtCount; }
  int32_t __fastcall GetDecayCount() { return CtrlRec->DecayCount; }
  int32_t __fastcall GetLPRCount() { return CtrlRec->LPRCount; }
  int32_t __fastcall GetUSBCANStatus() { return CtrlRec->USBCANStatus; }
  int32_t __fastcall GetScheduleDecay() { return CtrlRec->ScheduleDecay ; }
	int32_t __fastcall GetScheduleLPR() { return CtrlRec->ScheduleLPR ; }
	int32_t __fastcall GetScheduleZRA() { return CtrlRec->ScheduleZRA ; }
	int32_t __fastcall GetScheduleResMes() { return CtrlRec->ScheduleResMes ; }

	int32_t __fastcall GetBIChCount();

	String __fastcall GetCtrlName() { return CtrlRec->CtrlName ; }
	bool __fastcall GetAlarmStatus() { return CtrlRec->CtrlAlarmStatus; }
	bool __fastcall GetWatchDogEnable() { return CtrlRec->WatchDogEnable; }
	void __fastcall ResetCANAlarm();

protected:

	CIISCtrlRec *CtrlRec;
	RestartCtrlState RBState;
	int32_t T, TNextEvent;
	bool __fastcall NodeIniReady();
	bool RestartCIISBusRunning;
	void __fastcall RestartZones();

	//CIISClientInt
  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
	void __fastcall AfterSysClockTick( TDateTime TickTime );
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );



public:
	__fastcall TCIISController(TCIISDBModule *SetDB, TDebugWin *SetDebugWin, CIISCtrlRec *SetCtrlRec, TObject *SetCIISParent );
  __fastcall ~TCIISController();

  bool __fastcall SetPrjName( String PrjName );
	void __fastcall ResetCIISBus();
	void __fastcall RestartCIISBus();
  void __fastcall PrepareShutdown();
  void __fastcall UpdateLastValues();
	bool __fastcall UpdateAlarmStatus();

  void __fastcall IncomingCIIBusMessage(const Byte *CIIBusMsg, PVOID BIChannel);

	int32_t __fastcall GetSampelIntervall( int32_t i );
	int32_t __fastcall GetSetSampelIntervall( int32_t uCtrlSampInt );
	void __fastcall IncRecordingCount( int32_t RecType );
  void __fastcall DecRecordingCount( int32_t RecType );
  void __fastcall IncDetectedNodeCount();
	void __fastcall DecDetectedNodeCount();

	void __fastcall UpdateNodeCountInZones();
	void __fastcall UpdateIniErrorCountInZones();

	TCIISZone* __fastcall GetZone( int32_t Zone_No );
	TCIISBusInterface* __fastcall GetBusInt();

	void __fastcall TestBI();

__published:

  __property bool NoIR = { read = GetNoIR };
  __property int32_t  LPRRange = { read = GetLPRRange };
  __property int32_t  LPRStep = { read = GetLPRStep };
  __property int32_t  LPRDelay1 = { read = GetLPRDelay1 };
  __property int32_t  LPRDelay2 = { read = GetLPRDelay2 };
  __property int32_t  LPRMode = { read = GetLPRMode };
  __property int32_t  DecaySampInterval = { read = GetDecaySampInterval };
  __property int32_t  DecaySampInterval2 = { read = GetDecaySampInterval2 };
  __property int32_t  DecayDuration = { read = GetDecayDuration };
  __property int32_t  DecayDuration2 = { read = GetDecayDuration2 };
  __property int32_t  DecayDelay = { read = GetDecayDelay };
  __property int32_t  NodeCount = { read = GetNodeCount };
  __property int32_t  DetectedNodeCount = { read = GetDetectedNodeCount };
	__property int32_t  IniErrorCount = { read = GetIniErrorCount };
	__property int32_t  MonitorCount = { read = GetMonitorCount };
	__property int32_t  MonitorExtCount = { read = GetMonitorExtCount };
  __property int32_t  DecayCount = { read = GetDecayCount };
  __property int32_t  LPRCount = { read = GetLPRCount };
  __property int32_t  USBCANStatus = { read = GetUSBCANStatus };
	__property bool ScheduleDecay = { read = GetScheduleDecay };
	__property bool ScheduleLPR = { read = GetScheduleLPR };
	__property bool ScheduleZRA = { read = GetScheduleZRA };
	__property bool ScheduleResMes = { read = GetScheduleResMes };
	__property int32_t  BIChCount = { read = GetBIChCount };
	__property String CtrlName = { read = GetCtrlName };
	__property bool RestartRunning = { read = RestartCIISBusRunning };
	__property bool AlarmStatus = { read = GetAlarmStatus };
	__property bool WatchDogEnable  = { read = GetWatchDogEnable };

	__property bool ApplyNewRecording = { read = FApplyNewRecording, write = FApplyNewRecording };
	__property bool ApplyNewDecaySample = { write = FApplyNewDecaySample };
	__property bool ApplyNewMonitorSample = { write = FApplyNewMonitorSample };
	__property bool ApplyNewZone = { read = FApplyNewZone, write = FApplyNewZone };


};
#endif
