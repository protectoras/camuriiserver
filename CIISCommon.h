#ifndef CIISCommonH
#define CIISCommonH

//---------------------------------------------------------------------------
// History
//
// Date			Comment										Sign
// 2015-02-08   Converted to Xe7,
//				Declared new CIIEventCode AlarmMoved        Bo H
// 				Renamed enums
//

/*
#define MSG_TX_UNHANDLED         255
#define MSG_TX_IDENTIFY          0
#define MSG_TX_ANALOG            1
#define MSG_TX_LPR               2
#define MSG_TX_CALOFS            3
#define MSG_TX_CALGAIN           4
#define MSG_TX_HELO              5
#define MSG_TX_CAPABILITY        6
#define MSG_TX_EEDATA            7
#define MSG_TX_ADREGDATA         8
#define MSG_TX_IRRESULT          9
#define MSG_TX_LPREND            10
#define MSG_TX_SHUNTVALUE        11
#define MSG_TX_SAMPLE            12
#define MSG_TX_VERSION           13
#define MSG_TX_TEMP				 14
#define MSG_TX_SAMPLEEXTCH       15
#define MSG_RX_UIQ_SAMPLEEXTCH   16

#define MSG_TX_SHUNTS			 19
*/

#include <Classes.hpp>


//Supporterd Node Versions

const int32_t BIChEthSupportedVer = 1;
const int32_t BIChUSBSupportedVer = 3;
const int32_t PS_PI_SupportedVer = 5;
const int32_t PS_FV8_SupportedVer = 5;
const int32_t PS_FV3_SupportedVer = 5;
const int32_t PS_FV01_SupportedVer = 5;
const int32_t PS_FV1_SupportedVer = 5;
const int32_t PS_FVHV1_SupportedVer = 5;
const int32_t S_IO_SupportedVer = 3;
const int32_t S_LPRExt_SupportedVer = 2;
const int32_t S_HiRes_SupportedVer = 2;
const int32_t S_HUM_SupportedVer = 5;
const int32_t S_CrackWatch_SupportedVer = 1;
const int32_t S_P_SupportedVer = 5;
const int32_t S_P4_SupportedVer = 5;
const int32_t S_LPR_SupportedVer = 5;
const int32_t S_ZRA_SupportedVer = 5;
const int32_t S_R_SupportedVer = 5;
const int32_t S_PT_SupportedVer = 5;
const int32_t S_CTx8_SupportedVer = 5;
const int32_t WLink_SupportedVer = 5;
const int32_t Alarm_SupportedVer = 1;



const int32_t DefaultEventTabSize = 10000;       // Ver 3.8.0
const int32_t DefaultEventLevel = 1;

const float FixVoltOverflow = 99.999;
const float FixVoltUnderflow = -0.001;



const float One_mS = 1.0/( 24 * 60 * 60 * 1000 ); // Coonvert TTime to millisec.
const float OneSecond = 1.0/( 24 * 60 * 60 ); // Coonvert TTime to sec.
const float OneMinute = 1.0/( 24 * 60 ); // Coonvert TTime to minutes.
const float OneHour = 1.0/( 24 ); // Coonvert TTime to hours.
const float OneDay = 1.0; // Coonvert TTime to days.

const int32_t MessageHeaderLength = 7;  // Client / LServer interface
const int32_t OutMessageLimit = 12288;
const int32_t SocketInBufSize = 16384;
const int32_t SocketOutBufSize = 16384;
const int32_t SocketInputQueueSize = 32768;

// NetInterface
const int32_t UpdateAlarmDelay = 10000;
const int32_t CapabilityReqDelay = 10000;  // Time form first MSG_TX_IDENTIFY to first capability req. =Min start time for all types off nodes
const int32_t SetCANGroupDelay = 2000;
const int32_t IShuntReqDelay = 4000;
const int32_t VerReqDelay = 6000;
const int32_t RestartDelay = 24000;
const int32_t UpdatePSDelay = 100;
const uint32_t SysClock = 100; // Timer interupt = 110 mS for Sampel clock. TTimer interval is a mul of 55mS
//const uint32_t SysClockDT = SysClock * One_mS;
const int32_t Sec_To_mS = 1000;  // Convert S to mS ( all timers are set in mS in NetInterface code )

//CIISShutDown
const int32_t ShutDownDelayTime  = 30 * Sec_To_mS;

//SenseGuard
const int32_t SenseGuardInterval = 5 * Sec_To_mS;
const int32_t SenseGuardDelayTime = 45 * Sec_To_mS;
const int32_t SenseGuardVDiff = 1; // 1 Volt

// Alarms

const int32_t SetAlarmInterval = 20;

//Watch Dog

const int32_t WatchDogTime = 120; // 120 * Seconds = 120 Seconds
const int32_t ResetWatchDogTime = 200; // 200 * 100mS = 20 Seconds

// New NetInterface

const int32_t CANStartAdrNode = 400;
const int32_t CANStartAdrZone = 300;

// Delays in SM_SensorIni(), SM_PSIni()

const int32_t NodToNodDelay = 20;
const int32_t AcceptDelay = 1500;
const int32_t OffsetDelay = 1500;
const int32_t DACalibDelay = 3000;
const int32_t FastCmdDelay = 500;

const int32_t NoOffIniRetrys = 3;
const int32_t NodeIniTimeOut = 60000 * NoOffIniRetrys;



/*
	const int32_t AcceptToReqCapabilityDelay = 2500;
	const int32_t AcceptToZoneAdrDelay = 1500;//4000;
	const int32_t WaitForCapabilityDelay = 1500;
	const int32_t ZoneCanAdrToOffset = 1500;
	const int32_t ZoneCanAdrToReqCalib = 1500;//2000;
	const int32_t WaitForCalibValuesDelay = 1500;//5000;
	const int32_t WaitForBitValDelay = 800;
	const int32_t WaitForVerDelay = 1500;//2000;
	const int32_t WaitForSelFreq = 1000;
	const int32_t WaitForDACalib = 3000;
	const int32_t WaitForSelMode = 2000;
	const int32_t WaitForSelRange = 1000;

*/



const int32_t CanAdrToReqWLinkPar = 1500;
const int32_t WaitForWLinkParDelay = 1500;
const int32_t WaitForWLinkSetParDelay = 2000;



enum NodeIniState { NI_Accept,
					NI_ReqCapability,
					NI_WaitForCapability,
					NI_ZoneAdr,
					NI_OffsetAdj,
					NI_ReqVersion,
					NI_ReqCalibValues,
					NI_SetCamurIIMode,
					NI_ClrCIIIRecPar,
					NI_ClrCIIIAlarm,
          NI_SetInvertOutputOn,
					NI_ReqCalibValues_Ver3,
          NI_ReqCalibValues_Ver4,
					NI_WaitForCalibValues,
					NI_WaitForCalibValues_Ver3,
					NI_WaitForCh1BitValue,
					NI_WaitForCh2BitValue,
					NI_WaitForCh3BitValue,
					NI_WaitForCh4BitValue,
					NI_WaitForCh5BitValue,
					NI_WaitForCh6BitValue,
					NI_WaitForVer,
					NI_SelRange,
					NI_SelMode,
					NI_DACalib,
					NI_SetFallback,
					NI_Ready,
					NI_TimeOut,
					NI_ReqWLinkDH,
					NI_WaitForWLinkDH,
					NI_WaitForWLinkDL,
					NI_WaitForWLinkMY,
					NI_WaitForWLinkCH,
					NI_WaitForWLinkID,
					NI_WaitForWLinkPL,
					NI_WaitForWLinkSignal,
					NI_WaitForWLinkMode,
					NI_WLinkSetPL,
					NI_WLinkSetMode,
					NI_WLinkSetDH,
					NI_WLinkSetDL,
					NI_WLinkSetMY,
					NI_WLinkSetCH,
					NI_WLinkSetID
				  };

enum CIISPSIniState
					{
						PSStateVOut,
						PSStateIOut,
						PSStateRemote,
						PSStateSetInvertOutputOn,
						PSStateOutputOn,
						PSStateRampUpVout,
						PSStateReady
					};

enum CIISNodeCapability {
					Camur_II_WLink	= 9,
					Camur_II_LPR		= 10,
					Camur_II_PowerInterface = 11,
					Camur_II_IO		= 12,
					Camur_II_Alarm = 13,
					Camur_II_P		= 14,
					Camur_II_HUM		= 15,
					Camur_II_HiRes	= 16,
					Camur_II_R		= 17,
					Camur_II_ZRA		= 18,
					Camur_II_LPRExt	= 20,
					Camur_II_CT		= 21,
					Camur_II_MRE		= 22,
					Camur_II_CW		= 23,
					Camur_II_P4		= 24,
					Camur_II_Ladder	= 25,
					Camur_II_CrackWatch = 26,
					Camur_II_PT   = 27,
          Camur_II_Wenner = 28,
					Camur_II_FixVolt	= 31,
					Camur_II_FixVolt3A = 32,
					Camur_II_FixVolt01A = 33,
					Camur_II_FixVolt1A = 34,
					Camur_II_FixVoltHV1A = 35
					};

enum CIISBusMsg {
					 MSG_TX_IDENTIFY         = 0,
					 MSG_TX_ANALOG           = 1,
					 MSG_TX_LPR              = 2,
					 MSG_TX_CALOFS           = 3,
					 MSG_TX_CALGAIN          = 4,
					 MSG_TX_HELO             = 5,
					 MSG_TX_CAPABILITY       = 6,
					 MSG_TX_EEDATA           = 7,
					 MSG_TX_ADREGDATA        = 8,
					 MSG_TX_IRRESULT         = 9,
					 MSG_TX_LPREND           = 10,
					 MSG_TX_RANGE		     = 11,
					 MSG_TX_SAMPLE           = 12,
					 MSG_TX_VERSION          = 13,
					 MSG_TX_PSTEMP			 = 14,
					 MSG_TX_SAMPLEEXTCH      = 15,
					 MSG_RX_UIQ_SAMPLEEXTCH  = 16,
					 MSG_TX_SHUNTS			 = 19,
					 MSG_TX_TEMP			 = 20,
					 MSG_TX_WLinkPar         = 22,
					 MSG_TX_WLinkMode        = 23,
					 MSG_TX_uCtrlTime		 = 31,
					 MSG_TX_uCtrlPowerSettings = 32,
					 MSG_TX_uCtrlDataLenght	 = 33,
					 MSG_TX_uCtrlData		 = 34,
					 MSG_TX_BIMode			 = 35,
					 MSG_TX_uCtrlNodeInfo	 = 36,
					 MSG_TX_uCtrlNodeCount	 = 37,
					 MSG_TX_uCtrlRecStatus	 = 38,
					 MSG_TX_LPREND_CIII      = 113,
					 MSG_TX_LPR_U            = 114,
					 MSG_TX_LPR_I            = 115,
           MSG_TX_Debug            = 253,
					 MSG_TX_UNHANDLED        = 255,
				};

enum CIISBusMsgMode {
					  CIISBus_MsgReady = 10,
					  CIISBus_AddNode = 20,
					  CIISBus_UpdateNode = 25,
				   };

enum CIISParseMode { CIISParseReady = 10, CIISParseFinalize = 11, CIISParseCont = 20, CIISParseContAndFinalize = 30 };

enum CIIEventType { TypeUDef, SystemEvent, LevAlarm, ClientCom, CANCom };
enum CIIEventCode { CIIEventCode_CodeUDef,
					CIIEventCode_ETabCreated = 1,
					CIIEventCode_ServerStart = 2,
					CIIEventCode_ServerStop = 3,
					CIIEventCode_SensorLow = 4,
					CIIEventCode_SensorHigh = 5,
					CIIEventCode_SensorNormal = 6,
					CIIEventCode_PSLowU = 7,
					CIIEventCode_PSHighU = 8,
					CIIEventCode_PSLowI = 9,
					CIIEventCode_PSHighI = 10,
					CIIEventCode_PSNormalU = 11,
					CIIEventCode_PSNormalI = 12,
					CIIEventCode_LogIn = 13,
					CIIEventCode_LogOut = 14,
					CIIEventCode_AlarmEnable = 15,
					CIIEventCode_AlarmDisable = 16,
					CIIEventCode_PSVOutEnable = 17,
					CIIEventCode_PSVOutDisable = 18,
					CIIEventCode_PSRemote = 19,
					CIIEventCode_PSLocal = 20,
					CIIEventCode_SensorMoved = 21,
					CIIEventCode_PSMoved = 22,
					CIIEventCode_PrjTabChanged = 23,
					CIIEventCode_CtrlTabChanged = 24,
					CIIEventCode_ZoneTabChanged = 25,
					CIIEventCode_SensorTabChanged = 26,
					CIIEventCode_PSTabChanged = 27,
					CIIEventCode_RecStart = 28,
					CIIEventCode_RecStop = 29,
					CIIEventCode_ResetCan = 30,
					CIIEventCode_RestartCan = 31,
					CIIEventCode_TabProjectUpdated = 32,
					CIIEventCode_TabControllersUpdated = 33,
					CIIEventCode_TabZonesUpdated = 34,
					CIIEventCode_TabNodesUpdated = 35,
					CIIEventCode_TabSensorsUpdated = 36,
					CIIEventCode_TabPowerSupplyUpdated = 37,
					CIIEventCode_TabRecordingsUpdated = 38,
					CIIEventCode_TabValuesUpdated = 39,
					CIIEventCode_TabValuesLPRUpdated = 40,
					CIIEventCode_TabValuesDecayUpdated = 41,
					CIIEventCode_TabEventLogUpdated = 42,
					CIIEventCode_LoggInError = 43,
					CIIEventCode_TimeSet = 44,
					CIIEventCode_SensorLow2 = 45,
					CIIEventCode_SensorHigh2 = 46,
					CIIEventCode_SensorNormal2 = 47,
					CIIEventCode_WLinkTabChanged = 48,
					CIIEventCode_CANDupTag = 49,
					CIIEventCode_CANMissingTag = 50,
					CIIEventCode_CIIShutdown = 51,
					CIIEventCode_CIIShutdownFaild = 52,
					CIIEventCode_SenseGuardPSOff = 53,
					CIIEventCode_SensorLow3 = 54,
					CIIEventCode_SensorHigh3 = 55,
					CIIEventCode_SensorNormal3 = 56,
					CIIEventCode_SensorLow4 = 57,
					CIIEventCode_SensorHigh4 = 58,
					CIIEventCode_SensorNormal4 = 59,
					CIIEventCode_DupSNR = 60,
					CIIEventCode_AlarmTabChanged = 61,
					CIIEventCode_AlarmMoved = 62,
					CIIEventCode_BIChTabChanged = 63,
					CIIEventCode_ServerReStarted = 64

				 };

enum CIIUserLevel { ULNone, ULReadOnly, ULModify, ULErase };

enum CIIEventLevel { CIIEventLevel_UDef, CIIEventLevel_Low, CIIEventLevel_Medium,
						CIIEventLevel_High };

enum CIIMonitorState { MStart, PSOff, MSample, PSOn, MStop };

enum CIIServerStatus { CIISDisconnected, CIISConnected, CIISystemSync, CIIDataSync };

enum CIISServerMode { CIISNotInitiated, CIISLocalServer, CIISRemoteServer };

enum CIISCommand { CIISRead = 10, CIISWrite = 11, CIISAppend = 12,
									 CIISDelete = 13, CIISLogin = 20, CIISLogout = 21,
									 CIISSelectProject = 30, CIISExitProject = 31,
									 CIISReqStatus = 40, CIISReqDBVer = 41, CIISReqPrgVer = 42,
									 CIISReqTime = 43, CIISSetTime = 44, CIISServerType = 50};

enum CIISCommandMode { CIISUndef = 0,
					   CIISRecSearch = 1,
					   CIISRecAdd = 2, CIISRecAddLastVal = 3, CIISRecAddAlarm = 4,
					   CIISRecCount = 5,
					   CIISDeleteCtrl = 20,
					   CIISSensorMoved = 30,
						 CIISDeleteSensor = 31,
						 CIISAlarmMoved = 32,
						 CIISDeleteAlarm = 33,
						 CIISDeleteWLink = 35,
					   CIISPSMoved = 60,
						 CIISDeletePS = 61,
					   CIISAddZone = 40,
					   CIISDeleteZone = 41,
					   CIISRecordingStop = 59,
					   CIISCmdReady = 90 };



enum CIISTable { CIISUndefinedTab = 0, CIISNode = 1,
				 CIISProject = 10, CIISBusInterface = 14, CIISBIChannel = 15, CIISController = 20, CIISSensors = 30, CIISAlarm = 32,
				 CIISWLink = 35, CIISZones = 40, CIISValues = 50, CIISValuesLPR = 51,
				 CIISValuesDecay = 52, CIISValuesCalc = 53, CIISValuesMisc= 54, CIISRecordings = 55,
         CIISPowerSupply = 60, CIISEvent = 70 };

enum CIISMsgType { CIISRequest = 10, CIISResponse = 20 };

enum CIIMsgCode { CIISMsg_Ok = 0, CIIMsg_SysSyncStarted = 1, CIISMsg_DataSyncStarted = 2,
									CIISMsg_Idle = 20, CIISMsg_Busy = 21,
									CIISMsg_DeleteError = 103,
									CIISMsg_RecNotFound = 110,
									CIISMsg_UnknownCommand = 111, CIISMsg_NoLongerSupported = 112, CIISMsg_LServerNotConn = 210,
									CIISMsg_LoggInError = 250, CIISMsg_UserLevelError = 251,
									CIISMsg_LoggInTimeOut = 240, CIISMsg_LoggOutTimeOut = 241,
									CIISMsg_ExitPrjTimeOut = 242, CIISMsg_CanSyncTimeOut = 243,
									CIISDBError = 244, CIISMsg_SelPrjTimeOut = 245,
									CIISUnDefErr = 255 };

enum CIISCheckSyncState { CIISCheckSyncStart, CIISCheckSyncRequest,
                          CIISCheckSyncRead, CIISCheckSyncCtrlDBUppdate, CIISCheckSyncReady,
                          CIISCheckSyncError, CIISCheckSyncTimeout };

enum CIISLastValSyncState { CIISLastValSyncStart,
                            CIISLastValSyncSensorRequestFirst, CIISLastValSyncSensorRequest, CIISLastValSyncSensorRead,
                            CIISLastValSyncPSRequestFirst, CIISLastValSyncPSRequest, CIISLastValSyncPSRead,
                            CIISLastValSyncReady,
                            CIISLastValSyncError, CIISLastValSyncTimeout };

enum CIISDataSyncState { CIISDataSyncStart, CIISDataSyncValues,
                         CIISDataSyncLPR, CIISDataSyncCV,  CIISDataSyncDecay,
                         CIISDataSyncReady, CIISDataSyncError, CIISDataSyncTimeout };

enum CIISSysSyncState { CIISSysSyncMiscValIni,
												CIISSysSyncStart, CIISSysSyncCheck,
												CIISSysSyncLastVal, CIISSysSyncMiscVal,
												CIISSysCheckProject,
                        CIISSysSyncEvent, CIISSysSyncProject, CIISSysSyncBIChannels,
                        CIISSysSyncCtrl, CIISSysSyncZone,
												CIISSysSyncSensor, CIISSysSyncAlarm, CIISSysSyncPS, CIISSysSyncWLink,
												CIISSysSyncBIChannel, CIISSysSyncRec, CIISSysSyncReadyAfterCheck,
                        CIISSysSyncReady, CIISSysSyncError, CIISSysSyncTimeout };

enum CIISProjectSyncState{ CIISPrjSyncStart, CIISPrjSyncRequest, CIISPrjSyncReadRec,
                           CIISPrjSyncReady };                         


enum CIISEventSyncState { CIISEventSyncStart,
                          CIISEventSyncRequestFirst, CIISEventSyncRequestNext,
                          CIISEventSyncReadRec, CIISEventSyncReady };

enum CIISCtrlSyncState{ CIISCtrlSyncStart, CIISCtrlSyncRequest, CIISCtrlSyncReadRec,
                           CIISCtrlSyncReady };

enum CIISZoneSyncState{ CIISZoneSyncStart, CIISZoneSyncRequestFirst, CIISZoneSyncRequest,
                        CIISZoneSyncReadRec,CIISZoneSyncReady };

enum CIISSensorSyncState{ CIISSensSyncStart, CIISSensSyncRequestFirst, CIISSensSyncRequest,
													CIISSensSyncReadRec, CIISSensSyncReady };

enum CIISAlarmSyncState{ CIISAlarmSyncStart, CIISAlarmSyncRequestFirst, CIISAlarmSyncRequest,
													CIISAlarmSyncReadRec, CIISAlarmSyncReady };

enum CIISBIChannelsSyncState{ CIISBIChannelsSyncStart, CIISBIChannelsSyncRequestFirst, CIISBIChannelsSyncRequest,
													CIISBIChannelsSyncReadRec, CIISBIChannelsSyncReady };

enum CIISPSSyncState{ CIISPSSyncStart, CIISPSSyncRequestFirst, CIISPSSyncRequest,
					  CIISPSSyncReadRec, CIISPSSyncReady };

enum CIISWLinkSyncState{ CIISWLinkSyncStart, CIISWLinkSyncRequestFirst, CIISWLinkSyncRequest,
						CIISWLinkSyncReadRec, CIISWLinkSyncReady };

enum CIISBIChannelSyncState{ CIISBIChannelSyncStart, CIISBIChannelSyncRequestFirst, CIISBIChannelSyncRequest,
						CIISBIChannelSyncReadRec, CIISBIChannelSyncReady };

enum CIISRecSyncState{ CIISRecSyncStart, CIISRecSyncRequestStopDate, CIISRecSyncReadStopDate,
                       CIISRecSyncRequestFirst, CIISRecSyncRequest,
                       CIISRecSyncReadRec, CIISRecSyncReady };

enum CIISValSyncState{ CIISValSyncStart, CIISValSyncRequestRecCount, CIISValSyncReadRecCount,
											 CIISValSyncRequestFirst, CIISValSyncRequest,
											 CIISValSyncReadRec, CIISValSyncReady };

enum CIISLPRSyncState{ CIISLPRSyncStart, CIISLPRSyncRequestRecCount,CIISLPRSyncReadRecCount,
											 CIISLPRSyncRequestFirst, CIISLPRSyncRequest,
											 CIISLPRSyncReadRec, CIISLPRSyncReady };

enum CIISCVSyncState{ CIISCVSyncStart, CIISCVSyncRequestRecCount,CIISCVSyncReadRecCount,
											 CIISCVSyncRequestFirst, CIISCVSyncRequest,
											 CIISCVSyncReadRec, CIISCVSyncReady };

enum CIISDecaySyncState{ CIISDecaySyncStart, CIISDecaySyncRequestRecCount,CIISDecaySyncReadRecCount,
                         CIISDecaySyncRequestFirst, CIISDecaySyncRequest,
												 CIISDecaySyncReadRec, CIISDecaySyncReady };

enum CIISLoggInState{ CIISLoggInStart, CIISLoggInOpenCom, CIISLoggInRead, CIISLoggInCancel, CIISLoggInReady };

enum CIISOpenComState{ CIISOpenCom, CIISOpenComCheck, CIISOpenComReady, CIISOpenComCancel };

enum CIISCloseComState{ CIISCloseCom, CIISCloseComCheck, CIISCloseComReady, CIISCloseComCancel };

enum CIISCancelSyncState{ CIISCancelSync, CIISCanselSyncWait, CIISCanselSyncClose };

enum CIISSelPrjState { CIISSelPrjLoggIn, CIISSelPrjCheckLoggIn, CIISSelPrjSystemSync, CIISSelPrjCheckSync,
											 CIISSelPrjConnClient, CIISSelPrjTimeOut, CIISSelPrjReady };

enum	CIISPMLoggInState { CIISPMLoggInCancelSync, CIISPMLoggInCheckCancelSync, CIISPMLoggInCheckCloseCom,
													CIISPMLoggInOk, CIISPMLoggInTimeOut, CIISPMLoggInReady };

enum	CIISPMLoggOutState { CIISPMLoggOutCancelSync, CIISPMLoggOutCheckCancelSync, CIISPMLoggOutCheckCloseCom,
                           CIISPMLoggOutOk, CIISPMLoggOutTimeOut, CIISPMLoggOutReady };

enum	CIISPMExitPrjState { CIISExitPrjCancelSync, CIISExitPrjCheckCancelSync, CIISExitPrjCheckCloseCom,
													 CIISPMExitPrjOk, CIISPMExitPrjTimeOut, CIISExitPrjReady };

enum	CIISMonLoggInState { CIISMonLoggInCancelSync, CIISMonLoggInCheckCancelSync, CIISMonLoggInCheckCloseCom,
													 CIISMonLoggIn, CIISMonCheckLoggIn, CIISMonSystemSync, CIISMonCheckSystemSync,
													 CIISMonLoggInOk, CIISMonLoggInTimeOut, CIISMonLoggInReady	};

enum	CIISMonLoggOutState {CIISMonLoggOutCancelSync, CIISMonLoggOutCheckCancelSync, CIISMonLoggOutCheckCloseCom,
   												 CIISMonLoggOutOk, CIISMonLoggOutTimeOut, CIISMonLoggOutReady	};

enum	CIISMonCancelSyncState {CIISMonCancelSync, CIISMonCheckCancelSync, CIISMonCancelSyncOk, CIISMonCanselSyncTimeOut,
															CIISMonCancelSyncReady };


// Recordings ( Monitor, Decay, LPR )

const int32_t NoIRDelay = 1000;
const int32_t AfterSampleDelay = 1000;
const int32_t NoIRMinSampTime = NoIRDelay + AfterSampleDelay + 1000;
const int32_t TWarmUpMin = NoIRDelay + 1000;
const int32_t TWarmUpEnabled = 5000;
enum MonitorState { MSStart, MSWarmUp, MSPowerOff, MSSample_1, MSSample_2, MSSampleTemp, MSApplyUpd, MSWaitForMultiStepNodes, MSAfterSample };


const int32_t DecayDelay1 = 1000;
const int32_t ApplyUpdDelay = 1000;
const int32_t ApplyUpdDelayResMes = 3000;

//const int32_t DecayDelay = 500;
const int32_t IniSampMaxInterval = 2000;
//const int32_t NoOfIniSamp = 10;
enum DecayState { DecayIni, DecayStart, DecayPowerOff, DecayInstantOffSample,
		 DecayIniSample1_1, DecayIniSample1_2, DistDecayOff, DecayIniSample2_1,
		 DecayIniSample2_2, DecayIniApplyUpd, DecaySample, DecayApplyUpd, DecayEnd };

enum { RT_StandBy = 0, RT_Monitor = 1, RT_LPR = 2, RT_Decay = 3, RT_MonitorExt = 4, RT_ScheduledValues = 5,
					 RT_CTNonStat = 10, RT_CTStat = 11, RT_RExt = 12 };

//int32_t CIISRecType;

//enum CIISLoggInState { CIISLoggInCancelSync, CIISLoggIn };



/*
enum CIIS___SyncState{ CIIS___SyncStart, CIIS___SyncRequest, CIIS___SyncReadRec,
                           CIIS___SyncReady };
*/
typedef void __fastcall ( __closure *ProcMessFP)(const Byte*, PVOID);
typedef void __fastcall ( __closure *DebugStateFP)(int32_t);
typedef void __fastcall ( __closure *MessInQueueFP)();

uint32_t __fastcall HexToInt( String Hex );

TDateTime __fastcall Now_ms_cleard();
TDateTime __fastcall Now_ms();
String __fastcall CIISBoolToStr( bool b );
bool __fastcall CIISStrToBool( String s );
String __fastcall CIISFloatToStr( double d );
double __fastcall CIISStrToFloat( String s );
TDateTime __fastcall CIISStrToDateTime( String s );
String __fastcall CIISDateTimeToStr( TDateTime t );
String __fastcall CIISDateTimeToStrNomS( TDateTime t );
Word __fastcall CIISBcdToInt( Word ival );
Word __fastcall CIISIntToBcd( Word ival );

void __fastcall StringToIPBytes( String IPAdr, Word IPAdrBytes[4] );



bool __fastcall CIISSetSysTime( TDateTime t );
AnsiString __fastcall GetProgVersion();
String __fastcall GetCommonAppFolderPath(void);

__int64 __fastcall CIIDiskFree( String Path);

template <class T> T max( T t1, T t2 )
{
	return ( t1 < t2 ) ? t2 : t1;
}

template <class T> T min( T t1, T t2 )
{
	return ( t1 > t2 ) ? t2 : t1;
}

//---------------------------------------------------------------------------



struct CIISPrjRec
{
  String PrjName;
  String PrjNo;
  String Client;
  String Consultant;
  String Manager;
  String ContractDescription;
  String MMResponsibility;
  TDateTime Commissioning;
  String Drawings;
  String Criteria;
  int32_t ConnType;
  String ConnRemote;
  String ConnServerIP;
  int32_t PrjAlarmStatus;
  int32_t EventLogSize;
  int16_t EventLevel;
  CIIServerStatus ServerStatus;
  CIISServerMode ServerMode;
  int32_t ConfigVer;
  int32_t DataVer;
  int32_t LastValVer;
  int32_t DBVer;
  String PWReadOnly;
  String PWModify;
  String PWErase;
  int32_t PrgVer;
  TDateTime DTAdj;
  TDateTime DTNew;
  int32_t ValuesMiscSize;
  TDateTime CIISStart; // Last start time for CIIS
};



struct CIISCtrlRec
{
  String CtrlName;
  Boolean NoIR;
	TDateTime ScheduleDateTime;
  int32_t SchedulePeriod;
  int32_t DecaySampInterval;
  int32_t DecayDuration;
  int32_t LPRRange;
  int32_t LPRStep;
  int32_t LPRDelay1;
  int32_t LPRDelay2;
  int32_t SampInterval1;
  int32_t SampInterval2;
  int32_t SampInterval3;
  int32_t SampInterval4;
  int32_t SampInterval5;
  int16_t NextCanAdr;
  Double DefaultSensorLow;
  Double DefaultSensorHigh;
  Boolean DefaultAlarmEnable;
  int32_t CtrlAlarmStatus;
  int32_t USBCANStatus;
  int32_t NodeCount;
	int32_t MonitorCount;
	int32_t MonitorExtCount;
  int32_t DecayCount;
  int32_t LPRCount;
  int32_t SchedulePeriodUnit;
  Boolean ScheduleDecay;
	Boolean ScheduleLPR;
	Boolean ScheduleZRA;
	Boolean ScheduleResMes;
  int32_t CtrlScheduleStatus;
  int32_t LPRMode;
  int32_t DecayDelay;
  int32_t DecaySampInterval2;
  int32_t DecayDuration2;


  Boolean WatchDogEnable;
  int32_t BusIntSerialNo;
  int32_t BusIntVerMajor;
  int32_t BusIntVerMinor;

  String PrjName;
  int32_t DetectedNodeCount;
  int32_t IniErrorCount;
};

struct CIISWLinkRec
{
  int32_t WLinkSerialNo;
  int16_t WLinkCanAdr;
  int32_t WLinkStatus;
  String WLinkName;
  int16_t WLinkType;
  int16_t WLinkRequestStatus;
  Boolean WLinkConnected;
  int32_t WLinkVerMajor;
  int32_t WLinkVerMinor;
  int32_t WLinkDH;
  int32_t WLinkDL;
  int32_t WLinkMY;
  int16_t WLinkCH;
  int32_t WLinkID;
  int16_t WLinkPL;
  int16_t WLinkSignal;
  int16_t WLinkMode;
  String CtrlName;
	int32_t BIChannel;
	Boolean VerNotSupported;
};

struct CIISZoneRec
{
  int32_t ZoneNo;
  String ZoneName;
  Double AnodeArea;
  Double CathodeArea;
  String Comment;
  int16_t ZoneSampInterval;
  int16_t RecType;
  int32_t RecNo;
  int16_t ZoneCanAdr;
  int32_t ZoneAlarmStatus;
	int32_t ZoneScheduleStatus;
	int32_t BIChSerNo;
	Boolean IncludeInSchedule;
	int16_t RecTypeBeforeSchedule;
	Boolean uCtrl;
	Boolean uCtrlConnected;
	Boolean PSOffOnAlarm;
	Boolean PSOffHold;
  String CtrlName;
};

struct CIISNodeRec
{
  int32_t NodeSerialNo;
  int16_t NodeCanAdr;
  int16_t NodeType;
  int32_t NodeStatus;
  Boolean NodeConnected;
  int32_t ZoneNo;
  int32_t ZoneCanAdr;
  int32_t BIChannel;
};

struct CIISSensorRec
{
  int32_t SensorSerialNo;
  int16_t SensorCanAdr;
  int32_t SensorStatus;
  String SensorName;
  int16_t SensorType;
  Double SensorLastValue;
  Double PreCommOff;
  Double PreCommLPR;
  Double SensorArea;
  Double SensorGain;
  Double SensorOffset;
  String SensorUnit;
  Double SensorLow;
  Double SensorHigh;
  Boolean SensorAlarmEnabled;
  int32_t SensorAlarmStatus;
  Double SensorXPos;
  Double SensorYPos;
  Double SensorZPos;
  int32_t SensorSectionNo;
  int16_t RequestStatus;
  Double SensorIShunt;
  int32_t SensorLPRStep;
  Boolean SensorConnected;
  int32_t SensorWarmUp;
  Boolean SensorTemp;
  Double SensorLastTemp;
  int32_t SensorVerMajor;
  int32_t SensorVerMinor;
  Double SensorIShunt1;
  Double SensorIShunt2;
  Double SensorIShunt3;
  Double SensorLastValue2;
  Double SensorGain2;
  Double SensorOffset2;
  String SensorUnit2;
  Double SensorLow2;
  Double SensorHigh2;
  int32_t SensorAlarmStatus2;
  Boolean PowerShutdownEnabled;
  int32_t ZoneNo;
  int32_t ZoneCanAdr;
  Double Ch1BitVal;
  Double Ch2BitVal;
  Double Ch3BitVal;
  Double Ch4BitVal;
  Double Ch5BitVal;
  Double Ch6BitVal;
  int32_t BIChannel;
  Double SensorIShunt4;
  Double SensorIShunt5;
  Double SensorIShunt6;
  Double SensorIShunt7;
  Double SensorIShunt8;
  Double SensorIShunt9;
  Double SensorIShunt10;
  Double SensorIShunt11;
  Double SensorIShunt12;
  Double SensorIShunt13;
  Double SensorIShunt14;
  int32_t SensorChannelCount;
  String SensorName2;
  String SensorName3;
  String SensorName4;
  Double SensorGain3;
  Double SensorOffset3;
  String SensorUnit3;
  Double SensorLow3;
  Double SensorHigh3;
  Double SensorLastValue3;
  int32_t SensorAlarmStatus3;
  Double SensorGain4;
  Double SensorOffset4;
  String SensorUnit4;
  Double SensorLow4;
  Double SensorHigh4;
  Double SensorLastValue4;
	int32_t SensorAlarmStatus4;
	int32_t SensorLPRIRange;
	Boolean VerNotSupported;
	int32_t DisabledChannels;
	Double PreCommOff2;
	Double PreCommOff3;
	Double PreCommOff4;
	Boolean CANAlarmEnabled;
	int32_t CANAlarmStatus;
	Double SensorIShunt15;
	Double SensorIShunt16;
	Double SensorIShunt17;
	Double SensorIShunt18;
	Double SensorIShunt19;
	Double SensorIShunt20;
	Double SensorIShunt21;
	Double SensorIShunt22;
	Double SensorIShunt23;
};

struct CIISAlarmRec
{
	int32_t AlarmSerialNo;
	int16_t AlarmCanAdr;
	int32_t AlarmStatus;
	String AlarmName;
	int16_t AlarmType;
	int16_t RequestStatus;
	Boolean AlarmConnected;
	int32_t AlarmVerMajor;
	int32_t AlarmVerMinor;
	Boolean DO1;
	Boolean DO2;
	Boolean DO3;
	Boolean DO4;
	int32_t DO1AlarmType;
	int32_t DO2AlarmType;
	int32_t DO3AlarmType;
	int32_t DO4AlarmType;
	Boolean InvDO1;
	Boolean InvDO2;
	Boolean InvDO3;
	Boolean InvDO4;
	int32_t ZoneNo;
	int32_t ZoneCanAdr;
	int32_t BIChannel;
	Boolean VerNotSupported;
	Boolean CANAlarmEnabled;
	int32_t CANAlarmStatus;
};

struct CIISBIChannelRec
{
	int32_t SerialNo;
	int16_t Type;
	String Name;
	Boolean Included;
	Boolean Active;
	Boolean WatchDog;
	int32_t WDTime;
	int16_t Mode;
	String IP;
	String StaticIP;
	String Gateway;
  String Netmask;
	String MACAddress;
	int32_t VerMajor;
	int32_t VerMinor;
  Boolean TXError;
	Boolean ReConnect;
	Boolean VerNotSupported;
};

struct CIISPowerSupplyRec
{
  int32_t PSSerialNo;
  int16_t PSCanAdr;
  int32_t PSStatus;
  String PSName;
  String PSSerialID;
  int16_t PSType;
  Double PSLastValueU;
  Double PSLastValueI;
  Double PSLastValueCh3;
  Double PSLastValueCh4;
  int16_t PSMode;
  Double PSSetVoltage;
  Double PSSetCurrent;
  Double PSOutGain;
  Double PSOutOffset;
  Double PSLowU;
  Double PSLowI;
  Double PSHighU;
  Double PSHighI;
	Boolean PSAlarmEnabled;
  int32_t PSAlarmStatusU;
  int32_t PSAlarmStatusI;
  Boolean PSVOutEnabled;
  Boolean PSRemote;
  int16_t RequestStatus;
  Boolean PSConnected;
  Double Ch1Gain;
  Double Ch1Offset;
  String Ch1Unit;
  Double Ch2Gain;
  Double Ch2Offset;
  String Ch2Unit;
  Double Ch3Gain;
  Double Ch3Offset;
  String Ch3Unit;
  Double Ch4Gain;
  Double Ch4Offset;
  String Ch4Unit;
  Double Ch1BitVal;
  Double Ch2BitVal;
  Double Ch3BitVal;
  Double Ch4BitVal;
  int32_t PSVerMajor;
  int32_t PSVerMinor;
  Boolean SenseGuardEnabled;
  Boolean Fallback;
  Boolean PSTemp;
  int32_t ZoneNo;
  int32_t ZoneCanAdr;
	int32_t BIChannel;
	Boolean VerNotSupported;
	int32_t DisabledChannels;
	Boolean InvDO1;
	Boolean InvDO2;
	Boolean CANAlarmEnabled;
  int32_t CANAlarmStatus;
};

struct CIISRecordingRec
{
  int32_t RecNo;
  TDateTime RecStart;
  TDateTime RecStop;
  int16_t RecType;
  int32_t DecaySampInterval;
  int32_t DecayDuration;
  int32_t LPRRange;
  int32_t LPRStep;
  int32_t LPRDelay1;
  int32_t LPRDelay2;
  int32_t SampInterval;
  int32_t ZoneNo;
  int32_t LPRMode;
  int32_t DecayDelay;
  int32_t DecaySampInterval2;
  int32_t DecayDuration2;
};

struct CIISEventRec
{
  TDateTime EventDateTime;
  int32_t EventType;
  int32_t EventCode;
  int16_t EventLevel;
  int32_t EventNo;
  String EventString;
  int32_t EventInt;
  Double EventFloat;
};

struct CIISMonitorValueRec
{
	TDateTime DateTimeStamp;
  int32_t RecNo;
  int32_t SensorSerialNo;
  String ValueType;
  String ValueUnit;
  Double Value;
	Double RawValue;
	TDateTime SampleDateTime;
};

struct CIISLPRValueRec
{
  TDateTime DateTimeStamp;
  int32_t RecNo;
  int32_t SensorSerialNo;
  String ValueType;
  Double ValueV;
  Double ValueI;
};

struct CIISCorrRateValueRec
{
  Double ValueV;
  Double ValueI;
  Double ValueVSqr;
  Double ValueVI;
};

struct CIISDecayValueRec
{
  TDateTime DateTimeStamp;
  int32_t RecNo;
  int32_t SensorSerialNo;
  String ValueType;
	Double Value;
	TDateTime SampleDateTime;
};

	// 2011-01-19, Ver 3.8.0
struct CIISMiscValueRec
{
  TDateTime DateTimeStamp;
  int32_t SensorSerialNo;
  String ValueType;
  String ValueUnit;
  Double Value;
};

	// 2011-03-07, Ver 3.9.*
struct CIISCalcValueRec
{
  TDateTime DateTimeStamp;
  int32_t RecNo;
  int32_t SensorSerialNo;
  String ValueType;
  String ValueUnit;
  Double Value;
  int32_t Code;
};

class TDataArg : public TCollectionItem
{
private:
	int32_t FNum;
	String FVal;

protected:
public:
__published:
	__property int32_t Num  = { read=FNum, write=FNum };
	__property String Val  = { read=FVal, write=FVal };

};

//---------------------------------------------------------------------------

class TCamurPacket : public TObject
{
private:
		Byte FMessageType;
		Byte FMessageNumber;
		Byte FMessageCode;
		Byte FMessageCommand;
		Byte FMessageTable;
		CIIUserLevel FUserLevel;
		String FMessageData;
		TCollection *FArgs;
		int32_t FPortNo;
		Byte FMessageOrgNumber;
		CIISCommandMode FCommandMode;
		

protected:

public:
		__fastcall TCamurPacket();
		__fastcall ~TCamurPacket();
		String __fastcall GetArg(int32_t n);
		String __fastcall GetDelArg( int32_t n );
		bool __fastcall ArgInc(int32_t n);
		void *CIISObj;

__published:
		__property Byte MessageType  = { read=FMessageType, write=FMessageType };
		__property Byte MessageNumber  = { read=FMessageNumber, write=FMessageNumber };
		__property Byte MessageCode  = { read=FMessageCode, write=FMessageCode };
		__property Byte MessageCommand  = { read=FMessageCommand, write=FMessageCommand };
		__property Byte MessageTable  = { read=FMessageTable, write=FMessageTable };
		__property CIIUserLevel UserLevel  = { read=FUserLevel, write=FUserLevel };
		__property String MessageData  = { read=FMessageData, write=FMessageData };
		__property TCollection *Args  = { read=FArgs, write=FArgs };
		__property int32_t PortNo  = { read=FPortNo, write=FPortNo };
		__property Byte MessageOrgNumber  = { read=FMessageOrgNumber, write=FMessageOrgNumber };
		__property CIISCommandMode CommandMode = { read=FCommandMode, write=FCommandMode };

};

typedef void __fastcall ( __closure *TransferReplyFP)( TCamurPacket* );

class TCIISBusPacket : public TObject
{
private:
  Byte FByte0;
  Byte FByte1;
  Byte FByte2;
  Byte FByte3;
  Byte FByte4;
  Byte FByte5;
  Byte FByte6;
  Byte FByte7;
  CIISParseMode FParseMode;
  CIISBusMsgMode FMsgMode;
	int32_t FRequestedTag, FRecType, FRecordingNo;
	TDateTime FSampleTimeStamp;
	bool FMonitorExtended;
	int32_t FMonitorExtendedRecNo;
  PVOID FBIChannel;

	CIISBusMsg __fastcall GetMCommand(){ return (CIISBusMsg)FByte0; }
	int32_t __fastcall GetSerNo(){ return FByte1 + FByte2*256 + FByte3*65536; }
	int32_t __fastcall GetCANAdr(){ return FByte1 + FByte2*256; }
	CIISNodeCapability __fastcall GetNodeCap() { return (CIISNodeCapability)FByte3; }
	int32_t __fastcall GetWLinkPar();
	int32_t __fastcall ByteToHex( Byte B);


protected:

public:
	__fastcall TCIISBusPacket();
	__fastcall ~TCIISBusPacket();
	void *CIISObj;

	PVOID __fastcall GetBIChannel();
	void __fastcall SetBIChannel( PVOID BIChannel );


__published:
	__property Byte Byte0 = {read=FByte0, write = FByte0 };
	__property Byte Byte1 = {read=FByte1, write = FByte1 };
	__property Byte Byte2 = {read=FByte2, write = FByte2 };
	__property Byte Byte3 = {read=FByte3, write = FByte3 };
	__property Byte Byte4 = {read=FByte4, write = FByte4 };
	__property Byte Byte5 = {read=FByte5, write = FByte5 };
	__property Byte Byte6 = {read=FByte6, write = FByte6 };
	__property Byte Byte7 = {read=FByte7, write = FByte7 };
	__property CIISBusMsg MessageCommand = { read = GetMCommand };
	__property CIISParseMode ParseMode = { read=FParseMode, write=FParseMode };
	__property CIISBusMsgMode MsgMode = { read=FMsgMode, write=FMsgMode };
	__property int32_t SerNo = { read = GetSerNo };
	__property int32_t CANAdr = { read = GetCANAdr };
	__property CIISNodeCapability NodeCap = { read = GetNodeCap };
	__property Byte Tag = {read=FByte3, write = FByte3 };
	__property int32_t RequestedTag = {read=FRequestedTag, write=FRequestedTag};
	__property TDateTime SampleTimeStamp = {read=FSampleTimeStamp, write=FSampleTimeStamp};
	__property int32_t RecType = {read=FRecType, write = FRecType };
	__property bool MonitorExtended = {read=FMonitorExtended, write=FMonitorExtended};
	__property int32_t MonitorExtendedRecNo = {read=FMonitorExtendedRecNo, write=FMonitorExtendedRecNo};
	__property int32_t RecordingNo = {read=FRecordingNo, write=FRecordingNo};
	__property int32_t WLinkPar = {read = GetWLinkPar };
	__property Byte BIMode = { read = FByte2 };


  //__property FT_HANDLE BIChannel = {read = FBIChannel, write = FBIChannel };

};



#endif

