//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "CIISBusInterface.h"
#include "CIISProject.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISBusInterface::TCIISBusInterface( TCIISDBModule *SetDB,
																								 TDebugWin *SetDebugWin,
																								 TObject *SetCIISParent,
																								 ProcMessFP SetPM,
																								 bool DisableEthernet )
					 :TCIISObj( SetDB, NULL, SetDebugWin, SetCIISParent )
{
	DB = SetDB;
	PM = SetPM;

	CIISObjType = CIISBusInterface;

	P_Ctrl = (TCIISController*)CIISParent;
	P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	CanAdr = CANStartAdrZone;
	BIChRecs = new TList;

	AddNewBIChannels = false;
	ResetCIISBusRunning = false;
	RestartCIISBusRunning = false;

	if( DisableEthernet ) UDPSrv = NULL;
	else UDPSrv = new TIdUDPServer;
}

__fastcall TCIISBusInterface::~TCIISBusInterface()
{
	TCIISBIChannel *BICh;

	delete UDPSrv;

	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		delete BICh;
		ChildList->Delete(i);
	}

	for( int32_t i = BIChRecs->Count - 1; i >= 0; i-- )
	{
		BIChRecs->Delete(i);
	}

	delete ChildList;
	delete BIChRecs;
}


void __fastcall TCIISBusInterface::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "BusInterface: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
		break;

	case CIISWrite:
		break;

	case CIISAppend:
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISBusInterface::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "BusInterface: ParseCommand_End" );
	#endif
}

#pragma argsused
void __fastcall TCIISBusInterface::OnSysClockTick( TDateTime TickTime )
{
	if( ResetCIISBusRunning ) SM_ResetCIISBus();
	else if( RestartCIISBusRunning ) SM_RestartCIISBus();
}

#pragma argsused
void __fastcall TCIISBusInterface::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}

#pragma argsused
void __fastcall TCIISBusInterface::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISBusInterface::TestBI()
{
	//GetUSBCanVersion();
}

void __fastcall TCIISBusInterface::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

void __fastcall TCIISBusInterface::ResetCIISBus()
{
	RestartCIISBusRunning = false;
  CanAdr = CANStartAdrZone;
	ResetBusState = 100;
	T = 0;
	TNextEvent = 0;
	ResetCIISBusRunning = true;
}

void __fastcall TCIISBusInterface::SM_ResetCIISBus()
{
	if( T >= TNextEvent )
	{
		if( T >= 5000 * NoOffIniRetrys ) ResetBusState = 999;

		switch( ResetBusState )
		{
		case 100:
			TCIISObj *CIISObj;
			TCIISBIChannel *BICh;

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
				CIISObj = (TCIISObj*) ChildList->Items[i];
				if( CIISObj->CIISObjIs( CIISBIChannel ) )
				{
					BICh = (TCIISBIChannel*)CIISObj;
					BICh->ResetCIISBus();
				}
			}

			ResetBusState = 200;
			TNextEvent += 1000;
			break;

		case 200:
			if( !BIChResetRunning() )
			{
				UnInstallBIChannels();
				ResetBusState = 900;
			}
			break;

		case 900:
			ResetCIISBusRunning = false;
			P_Prj->Log( SystemEvent, CIIEventCode_ResetCan, CIIEventLevel_High, "", 0, 0 );
			break;

		case 999:
			ResetCIISBusRunning = false;
			break;
		}
	}
	T += SysClock;
}

bool __fastcall TCIISBusInterface::BIChResetRunning()
{
	TCIISObj *CIISObj;
	TCIISBIChannel *BICh;

	bool RR;

	RR = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISBIChannel ) )
		{
			BICh = (TCIISBIChannel*)CIISObj;
			RR = RR || BICh->BIChResetRunning;
		}
		if( RR ) break;
	}
	return RR;
}

void __fastcall TCIISBusInterface::RestartCIISBus()
{
	CIISBIChannelRec *BIChRec;
	bool MoreRecords;

	if( DB->FindFirstBIChannel() )
	{
		do
		{
			BIChRec = new CIISBIChannelRec;
			MoreRecords = DB->GetBIChannelRec( BIChRec, true );
			BIChRec->Active = false;

			BIChRecs->Add( BIChRec );
		} while ( MoreRecords );
	}

	RBIState = BI_FindBusInterfaces;
	T = 0;
	TNextEvent = 0;
	RestartCIISBusRunning = true;
}

void __fastcall TCIISBusInterface::SM_RestartCIISBus()
{
	if( T >= TNextEvent )
	{
		if( T >= 45000 * NoOffIniRetrys ) RBIState = BI_TimeOut;

		switch( RBIState )
		{
			case BI_FindBusInterfaces:
			AddNewBIChannels = true;
			if( UDPSrv != NULL ) FindEthInterfaces();
			FindUSBInterfaces();
			RBIState = BI_InstallInterfaces;  //BI_GetEthIntSnr;
			TNextEvent += 4000;
			break;

			case BI_InstallInterfaces:
			AddNewBIChannels = false;
			InstallUSBInterfaces();
			InstallEthInterfaces();
			RBIState = BI_OpenBusInterfaces;
			TNextEvent += 500;
			break;

			case BI_OpenBusInterfaces:
			BIOpenCom();
			RBIState = BI_IniBIChannels;
			TNextEvent += 1000;
			break;

			case BI_IniBIChannels:
			TCIISObj *CIISObj;
			TCIISBIChannel *BICh;

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISBIChannel ) )
			{
				BICh = (TCIISBIChannel*)CIISObj;
				if( BICh->Included )BICh->BIChannelIni();
			}
			RBIState = BI_WaitForBusIni;
			TNextEvent += 1000;
			}
			break;

			case BI_WaitForBusIni:
			if( !BIChIniRunning() )
			{
			DB->ApplyUpdatesBIChannels();
			RBIState = BI_Ready;
			}
			TNextEvent += 500;
			break;

			case BI_Ready :
			RestartCIISBusRunning = false;
			P_Prj->Log( SystemEvent, CIIEventCode_RestartCan, CIIEventLevel_High, "", 0, 0 );
			break;

			case BI_TimeOut :
			RestartCIISBusRunning = false;
			P_Prj->Log( SystemEvent, CIIEventCode_RestartCan, CIIEventLevel_High, "", 0, 0 );
			break;

			default:
			break;
	  }
	}
	T += SysClock;
}

bool __fastcall TCIISBusInterface::NodeIniReady()
{
	return !RestartCIISBusRunning;
}

bool __fastcall TCIISBusInterface::BIChIniRunning()
{
	TCIISObj *CIISObj;
	TCIISBIChannel *BICh;

	bool RR;

	RR = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISBIChannel ) )
		{
			BICh = (TCIISBIChannel*)CIISObj;
			RR = RR || BICh->BIChIniRunning;
		}
		if( RR ) break;
	}
	return RR;
}

void __fastcall TCIISBusInterface::UnInstallBIChannels()
{
	int32_t SerNo;
	TCIISBIChannel *BICh;

  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		delete BICh;
		ChildList->Delete(i);
	}

	for( int32_t i = BIChRecs->Count - 1; i >= 0; i-- )
	{
		BIChRecs->Delete(i);
	}
}

void __fastcall TCIISBusInterface::FindEthInterfaces()
{

	UDPSrv->DefaultPort = 5021;
	UDPSrv->Active = true;
	UDPSrv->OnUDPRead = UDPRead;

	UDPSrv->Broadcast("CamurII",5021);
	UDPSrv->Broadcast("CamurEth",5021);

	#if DebugCIISBusInt == 1
	Debug( "Track EthInterfaces" );
	#endif

}

#pragma argsused
void __fastcall TCIISBusInterface::UDPRead(TIdUDPListenerThread *AThread,
					const TIdBytes AData, TIdSocketHandle *ABinding)
{
	CIISBIChannelRec *BIChRec;
	int32_t SerNo;
	bool InList;

	UDPData = BytesToString(AData);
	UDPEthIP = ABinding->PeerIP;
	if(( UDPData.SubString(0,9) == "CIIEthInt" ) || ( UDPData.SubString(0,10) == "CamurEthOK" ))  //CIIEthIntxxxxyy
	{
		if( UDPData.SubString(0,9) == "CIIEthInt" )
		{
			SerNo = 1 * HexToInt( UDPData.SubString(12,2) ) +
							256 * HexToInt( UDPData.SubString(10,2) ) +
							65536 * HexToInt( UDPData.SubString(14,2) );
		}
		else
		{
			SerNo = 2000;
		}

		#if DebugCIISBusInt == 1
		Debug( "Ethernet Interface found at IP: " + UDPEthIP + " S/N: " + IntToStr( SerNo ) );
		#endif

		if( AddNewBIChannels )
		{
			InList = false;
			for ( int32_t i = 0; i < BIChRecs->Count; i++ )
			{
				BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
				if( BIChRec->SerialNo == SerNo )
				{
					BIChRec->Active = true;
					BIChRec->TXError = false;
					BIChRec->ReConnect = false;
					BIChRec->IP = UDPEthIP;
					InList = true;
					break;
				}
			}
			if( !InList )
			{
				BIChRec = new CIISBIChannelRec;
				BIChRec->SerialNo = SerNo;
				BIChRec->Type = Ethernet;
				BIChRec->Name = IntToStr( SerNo );
				BIChRec->Included = false;
				BIChRec->Active = true;
				BIChRec->IP = UDPEthIP;
				BIChRec->WatchDog = false;
				BIChRec->WDTime = WatchDogTime;
				BIChRec->TXError = false;
				BIChRec->ReConnect = false;
				BIChRecs->Add( BIChRec );
				DB->AppendBIChannelRec(BIChRec);

				#if DebugCIISBusInt == 1
				Debug( "New Eternet Interface S/N: " + IntToStr( BIChRec->SerialNo ) + " IP: "  + BIChRec->IP );
				#endif
			}
		}
		else
		{
			for ( int32_t i = 0; i < BIChRecs->Count; i++ )
			{
				BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
				if( BIChRec->SerialNo == SerNo )
				{

					#if DebugCIISBusInt == 1
					Debug( "Eternet Interface found again IP: " + UDPEthIP + " S/N: " + IntToStr( SerNo ) );
					#endif

					if( BIChRec->Included )BIChRec->ReConnect = true;
					break;
				}
			}
		}
	}
}

void __fastcall TCIISBusInterface::InstallEthInterfaces()
{
	TCIISBIEthernet *EthCh;
	CIISBIChannelRec *BIChRec;


	for ( int32_t i = 0; i < BIChRecs->Count; i++ )
	{
		BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
		if(( BIChRec->Type == Ethernet ) && BIChRec->Active )//&& BIChRec->Included )
		{
			EthCh = new TCIISBIEthernet( DB, DW, BIChRec, this, PM );
			ChildList->Add( EthCh );
			if( BIChRec->Included ) EthCh->OpenCom();
			FBIChCount = ChildList->Count;
			#if DebugCIISBusInt == 1
			Debug( "Install Eth Interface Snr = " + IntToStr( BIChRec->SerialNo ) );
			#endif
		}
	}
}

void __fastcall TCIISBusInterface::FindUSBInterfaces()
{

	DWORD NoOfDevs, DevIndex;
	String S1, S2;
	FT_STATUS ftStatus;
	char USBCANSerNo[9];
	int32_t SerNo;
	TCIISBIChannel *BICh;
	CIISBIChannelRec *BIChRec;
	bool InList;

	FHighestUSBSerialNo = 0;
	SerNo = 0;

	CurrentFTHandle = INVALID_HANDLE_VALUE;

	ftStatus = FT_ListDevices(&NoOfDevs,NULL,FT_LIST_NUMBER_ONLY);
	if (ftStatus == FT_OK)
	{
		for( DevIndex = 0; DevIndex < NoOfDevs; DevIndex++ )
		{
			ftStatus = FT_ListDevices((PVOID)DevIndex,USBCANSerNo,FT_LIST_BY_INDEX|FT_OPEN_BY_SERIAL_NUMBER);
			if (ftStatus == FT_OK)
			{
				S1 = USBCANSerNo;
				if( S1.Pos("PRCII") == 1 )
				{
					 SerNo = StrToInt( S1.SubString( 6, S1.Length() - 5 ));


					if( AddNewBIChannels )
					{
						InList = false;
						for ( int32_t i = 0; i < BIChRecs->Count; i++ )
						{
							BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
							if( BIChRec->SerialNo == SerNo )
							{
								BIChRec->Active = true;
								BIChRec->TXError = false;
								BIChRec->ReConnect = false;
								InList = true;

								#if DebugCIISBusInt == 1
								Debug( "USB Interface found S/N: " + IntToStr( BIChRec->SerialNo ) );
								#endif

								break;
							}
						}
						if( !InList )
						{
							BIChRec = new CIISBIChannelRec;
							BIChRec->SerialNo = SerNo;
							BIChRec->Type = USB;
							BIChRec->Name = IntToStr( SerNo );
							BIChRec->Included = true;
							BIChRec->Active = true;
							BIChRec->WatchDog = true;
							BIChRec->WDTime = WatchDogTime;
							BIChRec->TXError = false;
							BIChRec->ReConnect = false;
							BIChRecs->Add( BIChRec );
							DB->AppendBIChannelRec(BIChRec);

							#if DebugCIISBusInt == 1
							Debug( "New USB Interface S/N: " + IntToStr( BIChRec->SerialNo ) );
							#endif
						}
					}
					else
					{
						for ( int32_t i = 0; i < BIChRecs->Count; i++ )
						{
							BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
							if( BIChRec->SerialNo == SerNo )
							{

								#if DebugCIISBusInt == 1
								Debug( "USB Interface found again S/N: " + IntToStr( SerNo ) );
								#endif

								BIChRec->ReConnect = true;
								break;
							}
						}
					}
				}
			}
		}
	}
}

void __fastcall TCIISBusInterface::InstallUSBInterfaces()
{
	CIISBIChannelRec *BIChRec;
	TCIISBIUSB *USBCh;

	FHighestUSBSerialNo = 0;

	for ( int32_t i = 0; i < BIChRecs->Count; i++ )
	{
		BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
		if(( BIChRec->Type == USB ) && BIChRec->Active )//&& BIChRec->Included )
		{
			USBCh = new TCIISBIUSB(  DB, DW, BIChRec, this, PM );
			ChildList->Add( USBCh );
			if( BIChRec->Included )
			{
				if( BIChRec->SerialNo > FHighestUSBSerialNo ) FHighestUSBSerialNo = BIChRec->SerialNo;
			}
			FBIChCount = ChildList->Count;
			#if DebugCIISBusInt == 1
			Debug( " USB Interface " + IntToStr( BIChRec->SerialNo ) + " installed" );
			#endif
		}
	}
}

void __fastcall TCIISBusInterface::BIOpenCom()
{
	TCIISObj *CIISObj;
	TCIISBIChannel *BICh;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISBIChannel ) )
		{
			BICh = (TCIISBIChannel*)CIISObj;
			if( BICh->Included ) BICh->OpenCom();
		}
	}
}

bool __fastcall TCIISBusInterface::BIReadCANMsg(bool Status)
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->BIChReadCANMsg( Status );
		rslt = rslt && fnk;
  }
  return rslt;
}

#pragma argsused
void __fastcall TCIISBusInterface::TimerCanRxEvent(TObject * Sender)
{
  TCIISBIChannel *BICh;

	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) BICh->ReadCanRx();
  }
}

int32_t __fastcall TCIISBusInterface::GetZonCanAdr()
{
  return CanAdr++;
}

bool __fastcall TCIISBusInterface::SetUSBCanAddress(Word USBCANAddress)
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->SetUSBCanAddress( USBCANAddress );
		rslt = rslt && fnk;
  }
	return rslt;
}

int32_t __fastcall  TCIISBusInterface::GetHighestUSBSerialNo()
{
	return FHighestUSBSerialNo;
}

int32_t __fastcall  TCIISBusInterface::GetHighestUSBVerMajor()
{
	CIISBIChannelRec *BIChRec;

	for ( int32_t i = 0; i < BIChRecs->Count; i++ )
	{
		BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
		if( BIChRec->SerialNo == FHighestUSBSerialNo )
		{
			break;
		}
	}
	return BIChRec->VerMajor;
}

int32_t __fastcall  TCIISBusInterface::GetHighestUSBVerMinor()
{
	CIISBIChannelRec *BIChRec;

	for ( int32_t i = 0; i < BIChRecs->Count; i++ )
	{
		BIChRec = (CIISBIChannelRec*)BIChRecs->Items[i];
		if( BIChRec->SerialNo == FHighestUSBSerialNo )
		{
			break;
		}
	}
	return BIChRec->VerMinor;
}

void __fastcall TCIISBusInterface::SetWatchDogEnable( bool WDEnable )
{
  TCIISBIChannel *BICh;
	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) BICh->SetWatchDogEnable( WDEnable );
  }
}

bool __fastcall TCIISBusInterface::RequestSample( Word CanAdr, int32_t Tag )
{
	bool rslt, fnk;
	TCIISBIChannel *BICh;
  rslt = true;
	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestSample( CanAdr, Tag );
		rslt = rslt && fnk;
	}
	return rslt;
}

bool __fastcall TCIISBusInterface::RequestTemp( Word CanAdr )
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestTemp( CanAdr );
		rslt = rslt && fnk;
	}
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestDecayIni( Word CanAdr, Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestDecayIni( CanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
		rslt = rslt && fnk;
	}
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestDecayStart( Word CanAdr )
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestDecayStart( CanAdr );
		rslt = rslt && fnk;
  }
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestDecayCancel( Word CanAdr )
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestDecayCancel( CanAdr );
		rslt = rslt && fnk;
  }
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestLPRStart( Word CanAdr, Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode )
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestLPRStart( CanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, LPRMode );
		rslt = rslt && fnk;
  }
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestLPRStop( Word CanAdr )
{
  bool rslt, fnk;
	TCIISBIChannel *BICh;
	rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestLPRStop( CanAdr );
		rslt = rslt && fnk;
  }
  return rslt;
}

bool __fastcall TCIISBusInterface::RequestBIMode()
{
  bool rslt, fnk;
  TCIISBIChannel *BICh;
  rslt = true;
  for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
  {
		BICh = (TCIISBIChannel*) ChildList->Items[i];
		if( BICh->Included ) fnk = BICh->RequestBIMode();
		rslt = rslt && fnk;
	}
  return rslt;
}

PVOID __fastcall TCIISBusInterface::GetBIChannel( int32_t ChNo )
{
	TCIISBIChannel *BICh;

	if( ChNo < ChildList->Count ) BICh = (TCIISBIChannel*) ChildList->Items[ ChNo ];
	else BICh = NULL;

	return BICh;
}
//------------------------------------------------------------------------------

__fastcall TCIISBIChannel::TCIISBIChannel( TCIISDBModule *SetDB,
																					 TDebugWin *SetDebugWin,
																					 CIISBIChannelRec *SetBIChRec,
																					 TObject *SetCIISParent,
																					 ProcMessFP SetPM
																					)
					 :TCIISObj( SetDB, NULL, SetDebugWin, SetCIISParent )

{
	PM = SetPM;
  PMBI = NULL;
	BIChRec = SetBIChRec;

	CIISObjType = CIISBIChannel;

	P_BI = (TCIISBusInterface*)CIISParent;
	P_Ctrl = (TCIISController*)P_BI->CIISParent;
	P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	CANInBufReadPnt = 0;
	CANInBufWritePnt = 0;
	CanAdr = CANStartAdrNode;
	CanPMActive = false;
	ResetRunning = false;
	IniRunning = false;
	WDRunning = false;
	ReConnectRunning = false;
	OnSlowClockTick = 20;
	OnResetWD = ResetWatchDogTime;

	TLastSend = Now();
}

__fastcall TCIISBIChannel::~TCIISBIChannel()
{
  //
}

void __fastcall TCIISBIChannel::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "BIChannel: ParseCommand_Begin" );
	#endif

 switch( P->MessageCommand )
  {
	case CIISRead:
		if( P->UserLevel != ULNone )
		{
			ReadRec( P, O );
		}
		else
		{
			O->MessageCode = CIISMsg_UserLevelError;
		}
		break;

	case CIISWrite:
		if( ThisRec( P ) )
		{
			if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
			{
				WriteRec( P, O );
			}
			else
			{
				O->MessageCode = CIISMsg_UserLevelError;
			}
		}
		break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
	}
}

#pragma argsused
void __fastcall TCIISBIChannel::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "BIChannel: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISBIChannel::ThisRec( TCamurPacket *P )
{
	return P->GetArg(1) == BIChRec->SerialNo;
}

void __fastcall TCIISBIChannel::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
		if( P->ArgInc( 200 ) )
		{
			if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
			else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
		}
	}

	if( P->CommandMode == CIISRecSearch )
	{
		if( ThisRec( P ) )
		{
			if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_RecNotFound;
	}
	else if( P->CommandMode == CIISRecAdd )
	{
		O->MessageData = O->MessageData +
		"1=" + IntToStr( BIChRec->SerialNo ) + "\r\n" +
		"2=" + IntToStr( BIChRec->Type ) + "\r\n"
		"3=" + BIChRec->Name + "\r\n" +
		"4=" + CIISBoolToStr( BIChRec->Included ) + "\r\n" +
		"5=" + CIISBoolToStr( BIChRec->Active ) + "\r\n" +
		"6=" + CIISBoolToStr( BIChRec->WatchDog ) + "\r\n" +
		"7=" + IntToStr( BIChRec->WDTime ) + "\r\n" +
		"8=" + IntToStr( BIChRec->Mode ) + "\r\n" +
		"9=" + BIChRec->IP + "\r\n" +
		"10=" + BIChRec->StaticIP + "\r\n" +
		"11=" + BIChRec->Gateway + "\r\n" +
		"12=" + BIChRec->Netmask + "\r\n" +
		"13=" + BIChRec->MACAddress + "\r\n" +
		"14=" + IntToStr( BIChRec->VerMajor ) + "\r\n"
		"15=" + IntToStr( BIChRec->VerMinor ) + "\r\n"
		"16=" + CIISBoolToStr( BIChRec->VerNotSupported ) + "\r\n" ;


		O->MessageCode = CIISMsg_Ok;

		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
	}

  else if( P->CommandMode == CIISRecCount )
  {
		O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetBIChannelsRecCount() ) + "\r\n";;
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
	else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISBIChannel::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->ArgInc(200) )
	{
		O->MessageCode = CIISMsg_UnknownCommand;
	}
	else if( DB->LocateBIChannelRec( BIChRec ))
	{
		if( P->ArgInc(2) ) BIChRec->Name = P->GetArg(2);
		if( P->ArgInc(3) ) BIChRec->Included = CIISStrToBool( P->GetArg(3));
		if( P->ArgInc(4) ) BIChRec->WatchDog = CIISStrToBool( P->GetArg(4));
		if( P->ArgInc(5) ) BIChRec->WDTime = StrToInt( P->GetArg(5) );
		if( P->ArgInc(6) ) BIChRec->Mode = StrToInt( P->GetArg(6) );
		if( P->ArgInc(7) ) BIChRec->StaticIP = P->GetArg(7);
		if( P->ArgInc(8) ) BIChRec->Gateway = P->GetArg(8);
		if( P->ArgInc(9) ) BIChRec->Netmask = P->GetArg(9);


		if( DB->SetBIChannelRec( BIChRec ) )
		{
			DB->ApplyUpdatesBIChannels();
			P_Prj->Log( ClientCom, CIIEventCode_BIChTabChanged, CIIEventLevel_High, "", 0, 0 );
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;
	}
  else O->MessageCode = CIISMsg_RecNotFound;

  P->CommandMode = CIISCmdReady;

	if( P->ArgInc(4) )
  {
		SetWatchDogEnable( BIChRec->WatchDog && P_Ctrl->WatchDogEnable );
	}
	if( P->ArgInc(6) || P->ArgInc(7) || P->ArgInc(8) || P->ArgInc(9) )
	{
		//  Set Mode:Static=0/DHCP=1, StaticIP, GATEWAY, NETMASK
		SetIP_Par( BIChRec->Mode, BIChRec->StaticIP, BIChRec->Gateway, BIChRec->Netmask );
	}
}

#pragma argsused
void __fastcall TCIISBIChannel::OnSysClockTick( TDateTime TickTime )
{
	if( ResetRunning ) SM_ResetCIISBus();
	else if( IniRunning ) SM_BIChannelIni();
	else if( ReConnectRunning ) SM_ReConnectBIChannel();
	else
	{
		OnSlowClockTick--;
		if( OnSlowClockTick < 0 )
		{
			OnSlowClockTick = 20;

			if( BIChRec->TXError )
			{
				BIChClose();
				BIChOpen();
				BIChRec->TXError = false;

				#if DebugCIISBusInt == 1
				Debug( "TXError found on Eternet Interface S/N: " + IntToStr( BIChRec->SerialNo ));
				#endif
			}

			if( BIChRec->ReConnect ) ReConnectBIChannel();
		}

		if( WDRunning )
		{
			OnResetWD--;
			if( OnResetWD < 0 )
			{
				OnResetWD = ResetWatchDogTime;
				ResetWatchDog();
			}
		}
	}
}

void __fastcall TCIISBIChannel::ResetCIISBus()
{
	IniRunning = false;
	ReConnectRunning = false;

	ResetBusState = 100;
	T = 0;
	TNextEvent = 0;
	ResetRunning = true;
	TLastSend = Now();
}

void __fastcall TCIISBIChannel::SM_ResetCIISBus()
{
	if( T >= TNextEvent )
	{
		if( T >= 4000 * NoOffIniRetrys ) ResetBusState = 999;

		switch( ResetBusState )
		{
			case 100:
			BIChReadCANMsg( false );
			ResetBusState = 200;
			TNextEvent += 500;
			break;

			case 200:
			BIChClose();

			CanAdr = CANStartAdrNode;
			CanPMActive = false;
			BIChRec->ReConnect = false;
			BIChRec->TXError = false;
			CANInBufReadPnt = 0;
			CANInBufWritePnt = 0;

			ResetBusState = 900;
			TNextEvent += 500;
			break;

		case 900:
			ResetRunning = false;
			break;

		case 999:
			ResetRunning = false;
			break;
		}
	}
	T += SysClock;
}

#pragma argsused
void __fastcall TCIISBIChannel::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}

#pragma argsused
void __fastcall TCIISBIChannel::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISBIChannel::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}


void __fastcall TCIISBIChannel::ReadCanRx()
{

}

void __fastcall TCIISBIChannel::ParseMessages()
{
	int32_t MOffset, MStart, MEnd, MLength;

	#if DebugClientSocketBI == 1
	if( CANInBufReadPnt != CANInBufWritePnt) Debug( "Start Parse " + IntToStr( CANInBufReadPnt ) + "/" + IntToStr( CANInBufWritePnt ) );
	#endif

	while( CANInBufReadPnt < CANInBufWritePnt )   // Search for meassage
	{

	#if DebugClientSocketBI == 1
	Debug( "Parse " + IntToStr( CANInBufReadPnt ) + "/" + IntToStr( CANInBufWritePnt ) );
	#endif

		if( CANInBuf[CANInBufReadPnt & BufMask] == 1 )
		{
			MStart = CANInBufReadPnt;
			MLength = CANInBuf[(MStart + 2) & BufMask];
			MEnd = MStart + MLength + 3;
			if( MEnd >=  CANInBufWritePnt )
			{

				#if DebugClientSocketBI == 1
				Debug( "Parse no end " + IntToStr( MEnd ) + "/" + IntToStr( CANInBufWritePnt ) );
				#endif

				break;
			}
			else
			{
				if( CANInBuf[MEnd & BufMask] == 4 )  // accept as meassage
				{
				#if DebugClientSocketBI == 1
				Debug( "Accept messsage " + IntToStr( CANInBufReadPnt ) + "/" + IntToStr( MEnd ) );
				#endif

					if( CANInBuf[(MStart + 1) & BufMask] == 20 )  // meassge from CANBUSS
					{
						for( int32_t i = 0; i < MLength - 1; i++ ) // copy meassage to CANInBuf
							CANInMsg[i] = CANInBuf[(MStart + USBHeaderLength + i) & BufMask];

						// handle meassage
						CANInBufReadPnt = MEnd + 1;
						CANInFlg = canMSG_STD;

						//CanRx(nil);    <<<<---------------------------------------------

						 /* Request error counter ECAN
						flag = USBCanMSG;
						msg[0] = 1;
						msg[1] = 21;
						msg[2] = 0;
						msg[3] = 4;
						SendMsg( 0, msg, 4, flag );
						 */

						if( CanPMActive ) PM( CANInMsg, this );
					} //if( CANInBuf[MStart + 1] == 20

					else if( CANInBuf[(MStart + 1) & BufMask] == 21 )  // BI Address
					{
						FBusInterfaceAddress =  CANInBuf[(MStart + 3) & BufMask];
						CANInBufReadPnt = MEnd + 1;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 22 )  // BI Version
					{
						FBusInterfaceVerMajor = CANInBuf[(MStart + 3) & BufMask];
						FBusInterfaceVerMinor = CANInBuf[(MStart + 4) & BufMask];

						BIChRec->VerMajor = FBusInterfaceVerMajor;
						BIChRec->VerMinor = FBusInterfaceVerMinor;


						CANInBufReadPnt = MEnd + 1;

						#if DebugCIISBusInt == 1
						Debug( "RX BI" + IntToStr(BIChRec->SerialNo) + " BI Version = " +
						IntToStr(FBusInterfaceVerMajor) + "." + IntToStr(FBusInterfaceVerMinor) )
						#endif
						;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 23 )  // BI Error
					{
						#if DebugCIISBusIntRx == 1
						Debug( "Error =  " + IntToStr(CANInBuf[(MStart + 3) & BufMask]));
						#endif
						CANInBufReadPnt = MEnd + 1;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 24 )  // BI Watch Dog Status
					{
						FBusInterfaceWDStatus = CANInBuf[(MStart + 3) & BufMask];
						CANInBufReadPnt = MEnd + 1;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 30 )  // BI Watch Dog Status
					{
						Debug( "Error count Tx =  " + IntToStr(CANInBuf[(MStart + 3) & BufMask]) + " Rx = " + IntToStr(CANInBuf[(MStart + 4) & BufMask]));
						CANInBufReadPnt = MEnd + 1;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 35 )  //  uCtrl/BI Mode MSG_TX_BIMode
					{
						for( int32_t i = 0; i < MLength + 3; i++ ) // copy meassage to CANInMsg
							CANInMsg[i] = CANInBuf[(MStart + 1 + i) & BufMask];

						CANInBufReadPnt = MEnd + 1;
						if( CanPMActive ) PM( CANInMsg, this );
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 43 )  //  MSG_GET_IP_GATEWAY_NETMASK
					{
						BIChRec->StaticIP = IntToStr( CANInBuf[(MStart + 3) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 4) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 5) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 6) & BufMask ]);
						BIChRec->Gateway =  IntToStr( CANInBuf[(MStart + 7) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 8) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 9) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 10) & BufMask ]);
						BIChRec->Netmask =  IntToStr( CANInBuf[(MStart + 11) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 12) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 13) & BufMask ]) + "." +
																IntToStr( CANInBuf[(MStart + 14) & BufMask ]);

						BIChRec->Mode = CANInBuf[(MStart + 15) & BufMask ];

						CANInBufReadPnt = MEnd + 1;

						#if DebugCIISBusInt == 1
						Debug( "RX BI" + IntToStr(BIChRec->SerialNo) + " MSG_GET_IP_GATEWAY_NETMASK " )
						#endif
						;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 44 )  //  MSG_GET_MAC_ADDRESS
					{

						BIChRec->MACAddress =
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 3) & BufMask ],2) + "-" +
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 4) & BufMask ],2) + "-" +
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 5) & BufMask ],2) + "-" +
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 6) & BufMask ],2) + "-" +
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 7) & BufMask ],2) + "-" +
						 BIChRec->MACAddress.IntToHex( CANInBuf[(MStart + 8) & BufMask ],2);

						CANInBufReadPnt = MEnd + 1;

						#if DebugCIISBusInt == 1
						Debug( "RX BI" + IntToStr(BIChRec->SerialNo) + " MSG_GET_MAC_ADDRESS " )
						#endif
						;
					}
					else if( CANInBuf[(MStart + 1) & BufMask] == 53 )  // CIIICTrl Ack
					{
						//FAck = CANInBuf[(MStart + 4) & BufMask];
						CANInBufReadPnt = MEnd + 1;
					}
					else  // message from BI / uCtrl
					{
						for( int32_t i = 0; i < MLength + 3; i++ ) // copy meassage to BusIntMsg
							BusIntMsg[i] = CANInBuf[(MStart + 1 + i) & BufMask];

						CANInBufReadPnt = MEnd + 1;
						if( PMBI != NULL ) PMBI( BusIntMsg, this );
					}
				} // if( CANInBuf[MEnd] == 4
				else
				{
					CANInBufReadPnt++;

					#if DebugClientSocketBIEOT == 1
					Debug( " NOT EOT " + IntToStr( CANInBufReadPnt ) + "/" + IntToStr( CANInBufWritePnt ) );
					Debug( " MStart =  " + IntToStr( MStart & BufMask ) + " Length = " + IntToStr( MLength ) + " MEnd = " + IntToStr( MEnd & BufMask  ));
					for (int32_t i = CANInBufReadPnt - 1; i <= CANInBufWritePnt; i++)
					{
						Debug( "[" + IntToStr( i ) + "] = " + IntToStr( CANInBuf[i & BufMask]) );
					}
					#endif
				}
			} // if( MEnd <=  CANInBufWritePnt
		} // if( CANInBuf[i] == 1 )
		else
		{
			 CANInBufReadPnt++;

				#if DebugClientSocketBI == 1
				Debug( " NOT SOH " + IntToStr( CANInBufReadPnt ) + "/" + IntToStr( CANInBufWritePnt ) );
				#endif
		}
	} // while CANInBufReadPnt < CANInBufWritePnt

	CANInBufReadPnt = CANInBufReadPnt & BufMask;
	CANInBufWritePnt = CANInBufWritePnt & BufMask;
	if( CANInBufWritePnt < CANInBufReadPnt ) CANInBufWritePnt = CANInBufWritePnt + BufMask + 1;
}

#pragma argsused
int32_t __fastcall TCIISBIChannel::SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags)
{
	return 0;
}

bool __fastcall TCIISBIChannel::BIChClose()
{
	return false;
}

bool __fastcall TCIISBIChannel::BIChOpen()
{
	return false;
}

void __fastcall TCIISBIChannel::OpenCom()
{

}

void __fastcall TCIISBIChannel::BIChannelIni()
{
	if( BIChRec->Included )
	{
		RBIState = BI_GetBIVersions;
		T = 0;
		TNextEvent = 0;
		IniRunning = true;
		SM_BIChannelIni();
  }
	else IniRunning = false;
}

void __fastcall TCIISBIChannel::SM_BIChannelIni()
{
}

void __fastcall TCIISBIChannel::ReConnectBIChannel()
{
	ReConnectState = 100;
	T = 0;
	TNextEvent = 9000;
	ReConnectRunning = true;
}

void __fastcall TCIISBIChannel::SM_ReConnectBIChannel()
{
}

bool __fastcall TCIISBIChannel::SetUSBCanAddress(Word USBCANAddress)
{
	Byte msg[8];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
  msg[0] = 1;
  msg[1] = 11;
  msg[2] = 2;
	msg[3] = USBCANAddress & 255;
  msg[4] = USBCANAddress >> 8;
  msg[5] = 4;

	if( SendMsg( 0, msg, 6, flag ) == 6 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set BI address = " + IntToStr( USBCANAddress ))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting BI address")
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::BIChCANPower(bool Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = USBCanMSG;
  msg[0] = 1;
  msg[1] = 15;
  msg[2] = 1;
  if( Status ) msg[3] = 1;
  else msg[3] = 0;
  msg[4] = 4;

  if( SendMsg( 0, msg, 5, flag ) == 5 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) +" : Set CANPower = " + IntToStr( msg[3] ))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting CANPower" )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::BIChReadCANMsg(bool Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
  msg[1] = 16;
  msg[2] = 1;
  if( Status ) msg[3] = 1;
  else msg[3] = 0;
  msg[4] = 4;


  if( SendMsg( 0, msg, 5, flag ) == 5 )
  {
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : BIChReadCANMsg = " + IntToStr( msg[3] ))
		#endif
		;
  }
  else
  {
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting BIChReadCANMsg")
		#endif
		;
	}

	CANInBufReadPnt = 0;
	CANInBufWritePnt = 0;

	CanPMActive = Status;

  return SendOK;
}

bool __fastcall TCIISBIChannel::GetBIChVersion()
{
	Byte msg[8];
	Cardinal flag;
  bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
  msg[1] = 12;
	msg[2] = 0;
  msg[3] = 4;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request BI Version")
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting BI Version")
	#endif
	;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::GetBIChIPSt_GW_NM()
{
	Byte msg[8];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[1] = 43;
	msg[2] = 0;
  msg[3] = 4;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request IPStat Gateway Netmask")
	#endif
	;
	}
  else
	{
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting IPStat Gateway Netmask")
	#endif
	;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::GetBIChMac()
{
	Byte msg[8];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[1] = 44;
	msg[2] = 0;
  msg[3] = 4;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request Mac-address")
	#endif
	;
	}
  else
	{
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting Mac-address")
	#endif
	;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::SetAOut(Word CanAdr, int32_t Value)
{
  Byte msg[8];
  Cardinal flag;
  double DVal;
  int32_t IntVal;
  bool SendOK;

  flag = canMSG_STD;

  DVal = Value; DVal = DVal * 65535.0 / 4570.0;
  IntVal = DVal;
  IntVal = max( min( IntVal, 65535 ), 0 );

  msg[0] = 2;
  msg[1] = IntVal & 255;
  msg[2] = IntVal >> 8;

  /* TODO 1 : Check resulting output for AOUT */

  if( SendMsg( CanAdr, msg, 3, flag ) == 3 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " AOut = " + IntToStr(Value) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending AOut to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPSVOut(Word CanAdr, int32_t Value, int32_t MaxOut)
{
  Byte msg[8];
  Cardinal flag;
  int32_t IntVal;
  bool SendOK;

  flag = canMSG_STD;
  IntVal = max( min( Value, MaxOut ), 0 );

  msg[0] = 26;
  msg[1] = IntVal & 255;
  msg[2] = IntVal >> 8;
  msg[3] = 1;

  if( SendMsg( CanAdr, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " PSVOut = " + IntToStr(Value) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PSVOut to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPSIOut(Word CanAdr, int32_t Value, int32_t MaxOut)
{
  Byte msg[8];
  Cardinal flag;
  int32_t IntVal;
  bool SendOK;

  flag = canMSG_STD;

	IntVal = max( min( Value, MaxOut ), 0 );

  msg[0] = 26;
  msg[1] = IntVal & 255;
  msg[2] = IntVal >> 8;
  msg[3] = 0;

  if( SendMsg( CanAdr, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Set PSIOut = " + IntToStr(Value) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PSIOut to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPI_IOut(Word CanAdr, int32_t Value, int32_t MaxOut)   // in uA for Camur III Power Interface
{
  Byte msg[8];
	Cardinal flag;
  int32_t IntVal;
  bool SendOK;

  flag = canMSG_STD;

	IntVal = max( min( Value, MaxOut ), 0 );

	IntVal = IntVal / 2;

  msg[0] = 26;
  msg[1] = IntVal & 255;
  msg[2] = IntVal >> 8;
  msg[3] = 0;

  if( SendMsg( CanAdr, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Set PSIOut = " + IntToStr(Value) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PSIOut to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetDigOut(Word CanAdr, Word Channel, bool Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 5;
  msg[1] = Channel;
  if( Status == true ) msg[2] = 1;
  else msg[2] = 0;

  if( SendMsg( CanAdr, msg, 3, flag ) == 3 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " Set DigOut " + IntToStr(Channel) + " to " + IntToStr(msg[2]))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending DigOut to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPSOutputOn(Word CanAdr, bool Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
	msg[0] = 20;
	if( Status == true ) msg[1] = 0;
	else msg[1] = 1;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " PS Output On = " + CIISBoolToStr( Status ))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PS Output On " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPSMode(Word CanAdr, int32_t Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 23;
  if( Status == 1 ) msg[1] = 1;
  else msg[1] = 0;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " PSMode = " + IntToStr(msg[1]))
	#endif
	;
	}
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PSMode to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetPSFallback(Word CanAdr, int32_t Status)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 40;
  if( Status == 1 ) msg[1] = 1;
  else msg[1] = 0;

	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
	{
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " PSFallback = " + IntToStr(msg[1]))
	#endif
	;
	}
	else
	{
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending PSFallback to " + IntToStr(CanAdr) )
	#endif
	;
	}
	return SendOK;
}

int32_t __fastcall TCIISBIChannel::GetCanAdr()
{
  return CanAdr++;
}

bool __fastcall TCIISBIChannel::SendAccept(int32_t SerNo, Word CANAdr)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 0;
  msg[1] = SerNo & 255;
  msg[2] = ( SerNo & 65535 ) >> 8;
  msg[3] = SerNo >> 16;
  msg[4] = CANAdr & 255;
  msg[5] = CANAdr >> 8;
  if( SendMsg( 200, msg, 6, flag ) == 6 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CANAdr)  + " Accept to " + IntToStr(SerNo) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending Accept to " + IntToStr(SerNo) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestCapability(Word CanAdr)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 4;
  msg[1] = 12;
	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug("TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + "Capability req" )
	#endif
	;
	}
	else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending Capability req on CanAdr" + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetGroup(Word CanAdr, Word CanGroup)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;
  flag = canMSG_STD;
  msg[0] = 6;
  msg[1] = CanGroup & 255;
  msg[2] = CanGroup >> 8;

  if( SendMsg( CanAdr, msg, 3, flag ) == 3 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) +  " Assigned to group " + IntToStr(CanGroup) )
	#endif
	;
	}
	else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while Assigning CanAdr" + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestIShunt(Word CanAdr)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 11;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " IShunt req")
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending IShunt req on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestRShunt(Word CanAdr)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 28;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " RShunt req" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending RShunt req on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestBitVal(Word CanAdr, Word Channel)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 4;
  msg[1] = 13;
  msg[2] = Channel;

  if( SendMsg( CanAdr, msg, 3, flag ) == 3 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Scale req" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending Scale req on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestVer(Word CanAdr)
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 13;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
	{
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Version req" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending IShunt req on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetWatchDogTime( Word WDTime )
{
	Byte msg[8];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[1] = 17;
	msg[2] = 1;
	msg[3] = WDTime & 255;
	msg[4] = 4 ;

	if( SendMsg( 0, msg, 5, flag ) == 5 )
	{
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set WDTime = " + IntToStr( WDTime ))
	#endif
	;
	}
	else
	{
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting WDTime")
	#endif
	;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::SetWatchDogEnable( bool WDEnable )
{
  Byte msg[8];
  Cardinal flag;
	bool SendOK;

	if( !BIChRec->WatchDog ) WDEnable = false;

  flag = USBCanMSG;
  msg[0] = 1;
  msg[1] = 18;
	msg[2] = 1;
  if( WDEnable ) msg[3] = 1;
  else msg[3] = 0;
  msg[4] = 4 ;

  if( SendMsg( 0, msg, 5, flag ) == 5 )
  {
		SendOK = true;
		WDRunning = WDEnable;
		OnResetWD = 5;

		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set WDEnable = " + IntToStr( msg[3] ))
		#endif
		;
  }
  else
  {
		SendOK = false;

		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting WDEnable")
		#endif
		;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask )
{
	return true;
}

bool __fastcall TCIISBIChannel::ResetWatchDog()
{
	Byte msg[8];
	Cardinal flag;
  bool SendOK;

  flag = USBCanMSG;
  msg[0] = 1;
  msg[1] = 19;
  msg[2] = 0;
	msg[3] = 4 ;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Reset WD ")
	#endif
	;
	}
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while resetting WD")
	#endif
	;
	}
	return SendOK;
}


void __fastcall TCIISBIChannel::OffsetAdj( Word CanAdr )
{
	Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 19;

	if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
	{
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Offset req")
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while req Offset adj Ch " + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) )
	#endif
	;
	}
}

void __fastcall TCIISBIChannel::SetCamurIIMode( Word CanAdr )
{
  Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 95;
	msg[1] = 1;

	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
	{
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + " Camur II Mode")
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while req Camur II Mode Ch " + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) )
	#endif
	;
	}
}

void __fastcall TCIISBIChannel::ClearCIIIRecPar( Word CanAdr )
{
	Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 112;

	if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
	{
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + "Clear CIII Rec Par")
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while req Clear CIII Rec Par Ch " + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) )
	#endif
	;
	}
}

void __fastcall TCIISBIChannel::ClearCIIIAlarm( Word CanAdr )
{
	Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 70;
	msg[1] = 0;
	msg[2] = 1;
	msg[3] = 0;
	msg[4] = 0;

	if( SendMsg( CanAdr, msg, 5, flag ) == 5 )
	{
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + "Clear CIII Alarm")
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while req Clear CIII Alarm Ch " + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) )
	#endif
	;
	}
}

void __fastcall TCIISBIChannel::InvertOutputOn( Word CanAdr, bool Invert )
{
	Byte msg[2];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 125;
	if( Invert ) msg[1] = 1;
	else msg[1] = 0;

	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
	{
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  + "Set Invert Output On " + BoolToStr( Invert ))
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while Set Invert Output On Ch " + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) )
	#endif
	;
	}
}

bool __fastcall TCIISBIChannel::RequestSample( Word CanAdr, int32_t Tag )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 14;
  msg[1] = Tag;

	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) + " Request sample " + IntToStr(Tag) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting sample " + IntToStr(Tag) + " on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestTemp( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 15;

  if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) + " Request temp " )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting temp on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestDecayIni( Word CanAdr, Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 43; // MSG_RX_DecayIni
  msg[1] = StartDelay;
  msg[2] = POffDelay;
  msg[3] = NoOfIniSamples;
  msg[4] = IniInterval;


  if( SendMsg( CanAdr, msg, 5, flag ) == 5 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + "/" +IntToStr(StartDelay)  + "/" +IntToStr(POffDelay)  + "/" +IntToStr(NoOfIniSamples)  + "/" +IntToStr(IniInterval)  + " Request Decay Ini" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting Decay Ini " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestDecayStart( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 44; // MSG_RX_DecayStart
  msg[1] = 1;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " Request Start Decay" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting Start Decay " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestDecayCancel( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 44; // MSG_RX_DecayCancel
  msg[1] = 0;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " Request Cancel Decay" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting Cancel Decay " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}



bool __fastcall TCIISBIChannel::RequestLPRStart( Word CanAdr, Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 1; // MSG_RX_UIQ_REQLPR
  msg[1] = LPRRange;
  msg[3] = LPRTOn;
  msg[2] = LPRTOff;
  msg[4] = LPRStep;
  msg[5] = LPRMode;

  if( SendMsg( CanAdr, msg, 6, flag ) == 6 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " Reques LPR Start" )
	#endif
	;
	}
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting LPR Start on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestLPRStop( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 3;  // MSG_RX_UIQ_STOPLPR

  if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " Request LPR Stop" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting LPR Stop on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestWLinkPar( Word CanAdr, Word WLinkPar )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 33;
  msg[1] = WLinkPar;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " Request WLinkPar " + IntToStr( WLinkPar ) )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting WLinkPar " + IntToStr( WLinkPar ) + " on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestWLinkMode( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 35;  //WLink Mode

  if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + "Request WLink Mode" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting WLink Mode on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetWLinkPar( Word CanAdr, Word WLinkPar, int32_t Val )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

	flag = canMSG_STD;
	msg[0] = 33;
	msg[1] = WLinkPar;
	msg[2] = Val & 255;
	msg[3] = Val >> 8;

  if( SendMsg( CanAdr, msg, 4, flag ) == 4 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr) + "Set WLinkPar " + IntToStr( WLinkPar ) + " = " + IntToStr( Val ))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while setting WLinkPar " + IntToStr( WLinkPar ) + " on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SetWLinkMode( Word CanAdr, int32_t Mode )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 34;  //WLink Mode
  msg[1] = Mode;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + "Set WLink Mode = " + IntToStr( Mode ))
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while settting WLink Mode on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::WLinkParPermanent( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 33;
  msg[1] = 13;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr) + " WLink Par Permanent" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting WLink Par Permanent on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::SendClrLPR( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;
  bool SendOK;

  flag = canMSG_STD;
  msg[0] = 12;

  if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
  {
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " LPR accept" )
	#endif
	;
  }
  else
  {
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending LPR accept to " + IntToStr(CanAdr) )
	#endif
	;
  }
  return SendOK;
}

bool __fastcall TCIISBIChannel::RequestCANBusReset()
{
  Byte msg[8];
  Cardinal flag;
  bool BusResetOk;

  flag = canMSG_STD;
  msg[0] = 1;
  if( SendMsg( 200, msg, 1, flag ) == 1 )
  {
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Forcing network reset" );
	#endif
	BusResetOk = true;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : TX ERR while forcing network reset " );
	#endif
	BusResetOk = true;
  }

  return BusResetOk;
}

void __fastcall TCIISBIChannel::CTSelectMode( Word CanAdr, int32_t Mode )
{
  Byte msg[8];
  Cardinal flag;

  flag = canMSG_STD;
  msg[0] = 33;
  msg[1] = Mode;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " CT Select Mode " + IntToStr(Mode))
	#endif
	;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Select Mode " + IntToStr( Mode ) + " on CAN Adr " + IntToStr(CanAdr) )
	#endif
	;
  }
}

void __fastcall TCIISBIChannel::CTSelectRange( Word CanAdr, int32_t Range )
{
	Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
  msg[0] = 29;
  msg[1] = Range;

	if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
	{
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " CT Select Range " + IntToStr(Range))
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Select Range " + IntToStr( Range ) + " on CAN Adr " + IntToStr(CanAdr) )
	#endif
	;
	}
}


void __fastcall TCIISBIChannel::SetCTModeRangeFreq( Byte Mode, bool Apply,
																		Byte ZRARange, Byte ResMesRange, Byte ResMesFreq )
{

	Byte msg[8];
	Cardinal flag;

	flag = canMSG_STD;
	msg[0] = 119;
	msg[1] = Mode;
	if( Apply ) msg[2] = 1;
	else msg[2] = 0;
	msg[3] = ZRARange;
	msg[4] = ResMesRange;
	msg[5] = ResMesFreq;

	if( SendMsg( CanAdr, msg, 6, flag ) == 6 )
	{
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" + IntToStr(CanAdr)  +
										" CT Select Mode " + IntToStr(Mode) +
										" CT Select Apply " + IntToStr(Apply) +
										" CT Select ZRARange " + IntToStr(ZRARange) +
										" CT Select ResMesRange " + IntToStr(ResMesRange) +
										" CT Select ResMesFreq " + IntToStr(ResMesFreq)  )
	#endif
	;
	}
	else
	{
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Select Range on CAN Adr " + IntToStr(CanAdr) )
	#endif
	;
	}
}

void __fastcall TCIISBIChannel::CTSelectRFreq( Word CanAdr, int32_t FMode )
{
  Byte msg[8];
  Cardinal flag;

  flag = canMSG_STD;
  msg[0] = 31;
  msg[1] = FMode;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + "CT Select Freq " + IntToStr( FMode ))
	#endif
	;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Select Freq " + IntToStr( FMode ) + " on CAN Adr " + IntToStr(CanAdr) )
	#endif
	;
  }
}

void __fastcall TCIISBIChannel::CTSelectCh( Word CanAdr, int32_t Ch )
{
  Byte msg[8];
  Cardinal flag;

  flag = canMSG_STD;
  msg[0] = 34;
  msg[1] = Ch;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + "CT Select ch " + IntToStr(Ch))
	#endif
	;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Select ch " + IntToStr(Ch) + " on CAN Adr " + IntToStr(CanAdr) )
	#endif
	;
  }
}

void __fastcall TCIISBIChannel::CTDACalib( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;

  flag = canMSG_STD;
  msg[0] = 4;
  msg[1] = 2;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + " CT DA Calib" )
	#endif
	;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT DA Calib to " + IntToStr(CanAdr) )
	#endif
	;
  }
}

void __fastcall TCIISBIChannel::CTOffsetAdj( Word CanAdr )
{
  Byte msg[8];
  Cardinal flag;

  flag = canMSG_STD;
  msg[0] = 19;

  if( SendMsg( CanAdr, msg, 1, flag ) == 1 )
  {
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + "CT Offset adj" )
	#endif
	;
  }
  else
  {
	#if DebugCIISBusInt == 1
	Debug( "TX ERR while sending CT Offset Adj  to " + IntToStr(CanAdr) )
	#endif
	;
  }
}

void __fastcall TCIISBIChannel::CTRequestTemp( Word CanAdr, int32_t Tag )
{
  Byte msg[8];
  Cardinal flag;


  flag = canMSG_STD;
  msg[0] = 30;
  msg[1] = Tag;

  if( SendMsg( CanAdr, msg, 2, flag ) == 2 )
  {

	#if DebugCIISBusInt == 1
	Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + "/" +IntToStr(CanAdr)  + "Request temp " + IntToStr(Tag))
	#endif
	;
  }
  else
  {

	#if DebugCIISBusInt == 1
	Debug( "TX ERR while requesting temp " + IntToStr(Tag) + " on CanAdr " + IntToStr(CanAdr) )
	#endif
	;
  }

}

// uCtrl

void __fastcall TCIISBIChannel::SetPMBI( ProcMessFP SetPMBI )
{
	PMBI = SetPMBI;
}

ProcMessFP __fastcall TCIISBIChannel::GetPMBI()
{
	return PMBI;
}

bool __fastcall TCIISBIChannel::RequestBIMode()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4 ;

	msg[1] = 30;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request BIMode ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting BIMode")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::SetBIMode( Byte uCtrlMode )
{
	Byte msg[5];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 1;
	msg[4] = 4 ;

	msg[1] = 22;
	msg[3] = uCtrlMode ;

	if( SendMsg( 0, msg, 5, flag ) == 5 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set BIMode = " + IntToStr(msg[3]) )
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting BIMode")
		#endif
		;
	}
	return SendOK;

}

bool __fastcall TCIISBIChannel::SetBITime( Word Year, Word Month, Word Day, Word Hour, Word Minute, Word Sec )
{
	Byte msg[10];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 6;
	msg[9] = 4;

	msg[1] = 23;

	msg[3] = Year;
	msg[4] = Month;
	msg[5] = Day;
	msg[6] = Hour;
	msg[7] = Minute;
	msg[8] = Sec;


	if( SendMsg( 0, msg, 10, flag ) == 10 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set BITime = "
																												+ IntToStr( msg[3] ) + '-'
																												+ IntToStr( msg[4] ) + '-'
																												+ IntToStr( msg[5] ) + ' '
																												+ IntToStr( msg[6] ) + ':'
																												+ IntToStr( msg[7] ) )
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting BITime" )
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestBITime()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4 ;

	msg[1] = 24;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request BITime ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting BITime")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestPowerSetting()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 26;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request BI Power Settings ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while requesting BI Power Settings")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::SetPower( int32_t Voltage, int32_t Current, Byte PowerMode, bool OutputOn, int32_t Interval )
{
	Byte msg[12];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 8;
	msg[11] = 4;

	msg[1] = 25;


	msg[3] = Voltage & 255;
	msg[4] = Voltage >> 8;
	msg[5] = Current & 255;
	msg[6] = Current >> 8;

	if( PowerMode == 1 ) msg[7] = 1;
	else msg[7] = 0;

	if( OutputOn == true ) msg[8] = 0;
	else msg[8] = 1;

	/* old for CIISSetup
	if( PowerMode == 0 ) msg[7] = 0;
	else msg[7] = 1;

	if( OutputOn == 0 ) msg[8] = 0;
	else msg[8] = 1;
	*/

	msg[9] = Interval & 255;
	msg[10] = Interval >> 8;

	if( SendMsg( 0, msg, 12, flag ) == 12 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : uCtrl Par = "
																												+ IntToStr( msg[3] + msg[4]*256 ) + "V "
																												+ IntToStr( msg[5] + msg[6]*256 ) + "A "
																												+ IntToStr( msg[9] + msg[10]*256 ) + "S / Mode = "
																												+ IntToStr( msg[7] ) + "Power On = "
																												+ IntToStr( msg[8] ) )
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting uCtrl Parameters" )
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::StartRecording()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 27;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Start Recording ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while starting Recording")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::StopRecording()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 28;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Stop Recording ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Stoping Recording")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestNodeCount()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 32;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request Node Count ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Requesting Node Count")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestNodeInfo( Byte Channel )
{
	Byte msg[5];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 1;
	msg[4] = 4;


	msg[1] = 31;
	msg[3] = Channel;

	if( SendMsg( 0, msg, 5, flag ) == 5 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request Node Info ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Requesting Node Info")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestData( int32_t Start )
{
	Byte msg[8];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 4;
	msg[7] = 4;

	msg[3] = Start & 255;
	Start = Start >> 8;

	msg[4] = Start & 255;
	Start = Start >> 8;

	msg[5] = Start & 255;
	Start = Start >> 8;

	msg[6] = Start & 255;
	Start = Start >> 8;

	msg[1] = 29;

	if( SendMsg( 0, msg, 8, flag ) == 8 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request Data ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Requesting Data")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestRecStatus()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 33;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request Rec Status ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Requesting Rec Status")
		#endif
		;
	}
	return SendOK;
}

bool __fastcall TCIISBIChannel::RequestLastValueUpdate()
{
	Byte msg[4];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[2] = 0;
	msg[3] = 4;


	msg[1] = 34;

	if( SendMsg( 0, msg, 4, flag ) == 4 )
	{
		SendOK = true;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Request LastValueUpdate ")
		#endif
		;
	}
	else
	{
		SendOK = false;
		#if DebugCIISBusInt == 1
		Debug(  "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while Requesting LastValueUpdate")
		#endif
		;
	}
	return SendOK;
}


// TCIISBIUSB


__fastcall TCIISBIUSB::TCIISBIUSB( TCIISDBModule *SetDB,
																	 TDebugWin *SetDebugWin,
																	 CIISBIChannelRec *SetBIChRec,
																	 TObject *SetCIISParent,
																	 ProcMessFP SetPM
																	)
					 :TCIISBIChannel( SetDB, SetDebugWin, SetBIChRec, SetCIISParent, SetPM )
{

}

__fastcall TCIISBIUSB::~TCIISBIUSB()
{

}

void __fastcall TCIISBIUSB::OpenCom()
{
	String S1;
	int32_t L;


	BIChRec->TXError = false;
	BIChRec->ReConnect = false;

	CurrentFTHandle = INVALID_HANDLE_VALUE;
	S1 = "PRCII";
	S1 = S1 + IntToStr( BIChRec->SerialNo );
	L = S1.Length();

	for( int32_t i = 0 ; i < L; i++ )
	{
		USBCANSerNo[i] = AnsiChar( S1[i+1] );
	}

	USBCANSerNo[L] = 0;

	CurrentFTHandle = FT_W32_CreateFile(
										 USBCANSerNo,
										 GENERIC_READ | GENERIC_WRITE,
										 0,
										 NULL,
										 OPEN_EXISTING,
										 FILE_ATTRIBUTE_NORMAL | FT_OPEN_BY_SERIAL_NUMBER,
										 NULL );

	if( CurrentFTHandle == INVALID_HANDLE_VALUE )
	{
		#if DebugCIISBusInt == 1
		Debug( " Faild to install USB Interface " + IntToStr( BIChRec->SerialNo ))
		#endif
		;
	}
	else
	{
		;
	}
}

void __fastcall TCIISBIUSB::SM_BIChannelIni()
{
	if( T >= TNextEvent )
	{
		if( T >= 20000 * NoOffIniRetrys ) RBIState = BI_TimeOut;

		switch( RBIState )
		{
			case BI_GetBIVersions:
			GetBIChVersion();
			RBIState = BI_SetWDTime;
			TNextEvent += 500;
			break;

			case BI_SetWDTime:
			if( FBusInterfaceVerMajor > BIChUSBSupportedVer ) BIChRec->VerNotSupported = true;
			else BIChRec->VerNotSupported = false;
			SetWatchDogTime( WatchDogTime );
			RBIState = BI_SetWDEnable;
			TNextEvent += 500;
			break;

			case BI_SetWDEnable:
			SetWatchDogEnable( false );   // Watch dog enabled is set in CIISController
			RBIState = BI_BICANPower;
			TNextEvent += 500;
			break;

			case BI_BICANPower:
			BIChCANPower( true );
			RBIState = BI_BICANForceReset;
			TNextEvent += 100;
			break;

			case BI_BICANForceReset:
			RequestCANBusReset();
			RBIState = BI_BIReadCANMsg;
			TNextEvent += 1500;
			break;

			case BI_BIReadCANMsg:
			BIChReadCANMsg( true );
			RBIState = BI_OpenBusInterfaces;
			TNextEvent += 100;
			break;

			case BI_OpenBusInterfaces:
			SetUSBCanAddress( 200 );
			RBIState = BI_Ready;
			TNextEvent += 500;
			break;

			case BI_Ready :
			if( DB->LocateBIChannelRec( BIChRec ))
			{
				DB->SetBIChannelRec( BIChRec );

				#if DebugCIISBusInt == 1
				Debug( "Update TabBIChannels: " + IntToStr( BIChRec->SerialNo ) );
				#endif
			}

			IniRunning = false;
			break;

			case BI_TimeOut :
			IniRunning = false;
			break;

			default:
			break;
	  }
	}
	T += SysClock;
}

void __fastcall TCIISBIUSB::SM_ReConnectBIChannel()
{

	if( T >= TNextEvent )
	{
		if( T >= 10000 * NoOffIniRetrys ) ReConnectState = 999;

		switch( ReConnectState )
		{
			case 100:
			BIChClose();
			BIChOpen();
			ReConnectState = 200;
			TNextEvent += 1000;
			break;

			case 200:
			BIChReadCANMsg(true);
			SetUSBCanAddress( 200 );
			BIChRec->ReConnect = false;
			ReConnectState = 900;
			TNextEvent += 500;
			break;

		case 900 :
			ReConnectRunning = false;
			break;

		case 999 :
			ReConnectRunning = false;
			break;
		}
	}
	T += SysClock;
}

void __fastcall TCIISBIUSB::ReadCanRx()
{
	unsigned long NoOfBytes, NoOfBytesRead;

	/*
	#if DebugCIISBusIntRx == 1
	Debug( "BI-CanRxEvent" );
	#endif
	*/

	if( FT_GetQueueStatus(CurrentFTHandle, &NoOfBytes) != FT_OK ) NoOfBytes = 0;

  /*
  #if DebugCIISBusIntRx == 1
	Debug( "BI-CanRxEvent Bytes in Queue = " + IntToStr( NoOfBytes ));
  #endif
  */

	NoOfBytes = min( NoOfBytes, (unsigned long)(TempCANBufSize - MaxUSBMeassageLength) );
	if( NoOfBytes > 0 )
	{

		#if DebugCIISBusIntRx == 1
		Debug( "BI-CanRxEvent Bytes requested = " + IntToStr( (int32_t)NoOfBytes ));
		#endif


		// Copy to CANInBuf
		FT_W32_ReadFile( CurrentFTHandle, TempCANBuf, NoOfBytes, &NoOfBytesRead, NULL );

		#if DebugCIISBusIntRx == 1
		Debug( "BI-CanRxEvent Bytes read = " + IntToStr( (int32_t)NoOfBytesRead ));
		#endif

		for( unsigned long i = 0; i < NoOfBytesRead; i++ )
		CANInBuf[CANInBufWritePnt++ & BufMask] = TempCANBuf[i];

		ParseMessages();

	} // if( NoOfBytes > 0 )
}

int32_t __fastcall TCIISBIUSB::SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags)
{
  unsigned long  NoOfBytes;
  Byte StdMsg[16];
	TDateTime TNextSend;

	#if DebugCIISBusIntTx == 1
  Debug( "BI-SendMsg Length = " + IntToStr( (Byte)dlc ));
  #endif

	if( FBusInterfaceVerMajor == 3 && FBusInterfaceVerMinor <= 3)
  {
		TNextSend = TLastSend + TDateTime( 2 * One_mS );
		do
		{
			TLastSend = Now();
		}
		while( TLastSend < TNextSend );
	}

	if( flags == USBCanMSG ) FT_W32_WriteFile( CurrentFTHandle, msg, dlc, &NoOfBytes, NULL );
	else if( flags == canMSG_STD )
	{
		StdMsg[0] = 1;
		StdMsg[1] = 10;
		StdMsg[2] = dlc + 2;
		StdMsg[3] = id & 255;
		StdMsg[4] = id >> 8;
		Move(msg, StdMsg + 5, dlc);
		StdMsg[5 + dlc] = 4;
		FT_W32_WriteFile( CurrentFTHandle, StdMsg, dlc + 6, &NoOfBytes, NULL );
		NoOfBytes -= 6;

		#if DebugCIISBusIntTx == 1
		Debug( "BI-SendMsg Sent Length = " + IntToStr( (int32_t)NoOfBytes ));
		#endif
  }

	/* Request error counter ECAN
  flag = USBCanMSG;
  msg[0] = 1;
  msg[1] = 21;
  msg[2] = 0;
  msg[3] = 4;
  SendMsg( 0, msg, 4, flag );
	*/

	if( dlc != NoOfBytes )
	{
		BIChRec->ReConnect = true;

		#if DebugCIISBusInt == 1
		Debug( "USBInt TXError Detected" + IntToStr( BIChRec->SerialNo ));
		#endif
  }


  return NoOfBytes;
}

bool __fastcall TCIISBIUSB::BIChClose()
{
	bool rslt;

	rslt = FT_W32_CloseHandle( CurrentFTHandle );

	#if DebugCIISBusInt == 1
	Debug( "USBInt " + IntToStr( BIChRec->SerialNo ) + " CloseCom " );
	#endif

	return rslt;
}

bool __fastcall TCIISBIUSB::BIChOpen()
{
	CurrentFTHandle = FT_W32_CreateFile(
										 USBCANSerNo,
										 GENERIC_READ | GENERIC_WRITE,
										 0,
										 NULL,
										 OPEN_EXISTING,
										 FILE_ATTRIBUTE_NORMAL | FT_OPEN_BY_SERIAL_NUMBER,
										 NULL );

	#if DebugCIISBusInt == 1
	Debug( "USBInt " + IntToStr( BIChRec->SerialNo ) +" OpenCom " );
	#endif


	return true;
}

bool __fastcall TCIISBIUSB::SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask )
{
	return true;
}

//TCIISBIEthernet

__fastcall TCIISBIEthernet::TCIISBIEthernet( TCIISDBModule *SetDB,
											 TDebugWin *SetDebugWin,
											 CIISBIChannelRec *SetBIChRec,
											 TObject *SetCIISParent,
											 ProcMessFP SetPM )
		   :TCIISBIChannel( SetDB, SetDebugWin, SetBIChRec, SetCIISParent, SetPM )
{
	SocketInputIdx = 0;
	ClientSocket = new TClientSocket( NULL );
	SocketOpen = false;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt Created for S/N: " + IntToStr( BIChRec->SerialNo ) );
	#endif
}

__fastcall TCIISBIEthernet::~TCIISBIEthernet()
{
	delete ClientSocket;
}

void __fastcall TCIISBIEthernet::OpenCom()
{
	int32_t p;

	IPAdr = BIChRec->IP + "::" + 5022;
	p = IPAdr.Pos("::");
	if( p > 0 )
	{
		PortNo = StrToInt( IPAdr.SubString( p+2, p+6));
		IPAdr = IPAdr.SubString( 0, p-1 );
	}

	ClientSocket->OnConnect = ServerConnect;
	ClientSocket->OnLookup = ServerLookup;
	ClientSocket->OnDisconnect = ServerDisconnect;
	ClientSocket->OnError = ServerOnError;
	ClientSocket->OnRead = ServerRead;
	ClientSocket->Host = IPAdr;
	ClientSocket->Port = PortNo;
	ClientSocket->Tag = 0;

	BIChRec->TXError = false;
	BIChRec->ReConnect = false;

	ClientSocket->Open();

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) +" OpenCom: " + IPAdr + "::" + IntToStr( PortNo ));
	#endif
}

void __fastcall TCIISBIEthernet::SM_BIChannelIni()
{
	if( T >= TNextEvent )
	{
		if( T >= 20000 * NoOffIniRetrys ) RBIState = BI_TimeOut;

		switch( RBIState )
		{
			case BI_GetBIVersions:
			GetBIChVersion();
			RBIState = BI_GetBIIPStatGatwayNetmask;
			TNextEvent += 500;
			break;

			case BI_GetBIIPStatGatwayNetmask:
			if( FBusInterfaceVerMajor > BIChEthSupportedVer ) BIChRec->VerNotSupported = true;
			else BIChRec->VerNotSupported = false;
			GetBIChIPSt_GW_NM();
			RBIState = BI_GetBIMac;
			TNextEvent += 500;
			break;

			case BI_GetBIMac:
			GetBIChMac();
			RBIState = BI_SetWDTime;
			TNextEvent += 500;
			break;

			case BI_SetWDTime:
			SetWatchDogTime( WatchDogTime );
			RBIState = BI_SetWDEnable;
			TNextEvent += 500;
			break;

			case BI_SetWDEnable:
			SetWatchDogEnable( false );   // Watch dog enabled is set in CIISController
			RBIState = BI_BICANPower;
			TNextEvent += 500;
			break;

			case BI_BICANPower:
			BIChCANPower( true );
			RBIState = BI_BICANForceReset;
			TNextEvent += 100;
			break;

			case BI_BICANForceReset:
			RequestCANBusReset();
			RBIState = BI_BIReadCANMsg;
			TNextEvent += 1500;
			break;

			case BI_BIReadCANMsg:
			BIChReadCANMsg( true );
			RBIState = BI_OpenBusInterfaces;
			TNextEvent += 100;
			break;

			case BI_OpenBusInterfaces:
			SetUSBCanAddress( 200 );
			RBIState = BI_Ready;
			TNextEvent += 500;
			break;

			case BI_Ready :
			if( DB->LocateBIChannelRec( BIChRec ))
			{
				DB->SetBIChannelRec( BIChRec );

				#if DebugCIISBusInt == 1
				Debug( "Update TabBIChannels: " + IntToStr( BIChRec->SerialNo ) );
				#endif
			}

			IniRunning = false;
			break;

			case BI_TimeOut :
			IniRunning = false;
			break;

			default:
			break;
	  }
	}
	T += SysClock;
}

void __fastcall TCIISBIEthernet::SM_ReConnectBIChannel()
{

	if( T >= TNextEvent )
	{
		if( T >= 10000 * NoOffIniRetrys ) ReConnectState = 999;

		switch( ReConnectState )
		{
			case 100:
			BIChClose();
			OpenCom();
			ReConnectState = 200;
			TNextEvent += 1000;
			break;

			case 200:
			BIChReadCANMsg(true);
			SetUSBCanAddress( 200 );
			BIChRec->ReConnect = false;
			ReConnectState = 900;
			TNextEvent += 500;
			break;

		case 900 :
			ReConnectRunning = false;
			break;

		case 999 :
			ReConnectRunning = false;
			break;
		}
	}
	T += SysClock;
}

void __fastcall TCIISBIEthernet::AddSocketInputQueue(char* Buf, int32_t N)
{
	Move( Buf, SocketInputQueue + SocketInputIdx, N );
	SocketInputIdx += N;
	#if DebugClientSocketBI == 1
	Debug( "EthernetInt SocketInputQueue: Adding " + IntToStr( N ) + ", L = " + IntToStr( SocketInputIdx ) + " bytes" );
	#endif
}

void __fastcall TCIISBIEthernet::ReadCanRx()
{
	unsigned long NoOfBytes, NoOfBytesRead, CANInBufSpace;

	NoOfBytes =  SocketInputIdx;
	CANInBufSpace = CANInBufReadPnt - CANInBufWritePnt;
	if ( CANInBufSpace <= 0 ) CANInBufSpace += BufMask;

	NoOfBytes = min( NoOfBytes, CANInBufSpace );
	if( NoOfBytes > 0 )
		for( unsigned long i = 0; i < NoOfBytes; i++ )
			CANInBuf[CANInBufWritePnt++ & BufMask] = SocketInputQueue[i];

	SocketInputIdx -= NoOfBytes;
	Move( SocketInputQueue + NoOfBytes, SocketInputQueue, NoOfBytes );

	ParseMessages();
}

int32_t __fastcall TCIISBIEthernet::SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags)
{
	unsigned long  NoOfBytes;
	Byte StdMsg[16];
	TDateTime TNextSend;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) + " SendMsg: " + IPAdr + "::" + IntToStr( PortNo ));
	#endif

	#if DebugCIISBusIntTx == 1
	Debug( "EthernetInt SendMsg Length = " + IntToStr( (Byte)dlc ));
	#endif

	if( SocketOpen == true )
	{
		if( flags == USBCanMSG ) NoOfBytes = CWSocket->SendBuf(msg, dlc);
		else if( flags == canMSG_STD )
		{
			StdMsg[0] = 1;
			StdMsg[1] = 10;
			StdMsg[2] = dlc + 2;
			StdMsg[3] = id & 255;
			StdMsg[4] = id >> 8;

			Move(msg, StdMsg + 5, dlc);
			StdMsg[5 + dlc] = 4;
			NoOfBytes = CWSocket->SendBuf( StdMsg, dlc + 6 );
			NoOfBytes -= 6;
			#if DebugCIISBusIntTx == 1
			Debug( "BI-SendMsg Sent Length = " + IntToStr( (int32_t)NoOfBytes ));
			#endif
		}
	}
	return NoOfBytes;
}

bool __fastcall TCIISBIEthernet::BIChClose()
{
	ClientSocket->Close();

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) +" CloseCom " + IPAdr );
	#endif

	return true;
}

bool __fastcall TCIISBIEthernet::BIChOpen()
{
	ClientSocket->Open();

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) +" OpenCom " + IPAdr );
	#endif

	return true;
}

bool __fastcall TCIISBIEthernet::SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask )
{
	Word ip[4], gw[4], nm[4];

	StringToIPBytes( StaticIP, ip );
	StringToIPBytes( Gateway, gw );
	StringToIPBytes( Netmask, nm );


	Byte msg[17];
	Cardinal flag;
	bool SendOK;

	flag = USBCanMSG;
	msg[0] = 1;
	msg[1] = 42;
	msg[2] = 13;
	msg[16] = 4 ;

	msg[3] = ip[0];  // IP 1
	msg[4] = ip[1];  // IP 2
	msg[5] = ip[2];  // IP 3
	msg[6] = ip[3];  // IP 4

	msg[7] = gw[0];  // Gateway 1
	msg[8] = gw[1];  // Gateway 2
	msg[9] = gw[2];  // Gateway 3
	msg[10] = gw[3]; // Gateway 4

	msg[11] = nm[0]; // N�tmask 1
	msg[12] = nm[1]; // N�tmask 2
	msg[13] = nm[2]; // N�tmask 3
	msg[14] = nm[3]; // N�tmask 4

	msg[15] = Mode;


	if( SendMsg( 0, msg, 17, flag ) == 17 )
	{
	SendOK = true;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + " : Set IP Parameters = " + StaticIP )
	#endif
	;
	}
	else
	{
	SendOK = false;
	#if DebugCIISBusInt == 1
	Debug( "TX BI" + IntToStr( BIChRec->SerialNo ) + " : ERR while setting IP Parameters" )
	#endif
	;
	}
	return SendOK;
}

//External calls form ClientSocket

#pragma argsused
void __fastcall TCIISBIEthernet::ServerLookup(TObject * Sender, TCustomWinSocket * Socket)
{
	#if DebugCIISBusInt == 1
	Debug( "EthernetInt Lookup connection to " + IPAdr );
	#endif
	;
}

#pragma argsused
void __fastcall TCIISBIEthernet::ServerConnect(TObject * Sender, TCustomWinSocket * Socket)
{
	SocketInputIdx = 0;
	CWSocket = Socket;
	SocketOpen = true;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) + " connected to " + Socket->RemoteAddress );
  #endif
}

#pragma argsused
void __fastcall TCIISBIEthernet::ServerOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int32_t &ErrorCode)
{
  int32_t ECode;

	ECode = ErrorCode;
	ErrorCode = 0;

	BIChRec->TXError = true;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) + " ServerOnError. Error = " + IntToStr(ECode) );
	#endif
}

#pragma argsused
void __fastcall TCIISBIEthernet::ServerDisconnect(TObject * Sender, TCustomWinSocket * Socket)
{
	SocketOpen = false;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) + " disconnected from " + Socket->RemoteAddress );
	#endif
}

#pragma argsused
void __fastcall TCIISBIEthernet::ServerRead(TObject * Sender, TCustomWinSocket * Socket)
{
	int32_t b;

	#if DebugCIISBusInt == 1
	Debug( "EthernetInt " + IntToStr( BIChRec->SerialNo ) + " ServerRead: " + IPAdr + "::" + IntToStr( PortNo ));
	#endif

	b = Socket->ReceiveBuf( SocketInBuf, EthSocketInBufSize );
	while( b > 0 )
	{
		#if DebugClientSocketBI == 1
		Debug( "EthernetInt ServerRead: " + IntToStr( b ));

		for (int32_t i = 0; i < b; i++)
		{
			Debug( "[" + IntToStr( i ) + "] = " + IntToStr( SocketInBuf[i]) );
		}
		#endif

		if( b > 0 ) AddSocketInputQueue( SocketInBuf, b );
		b = Socket->ReceiveBuf( SocketInBuf, EthSocketInBufSize );
	}
}


