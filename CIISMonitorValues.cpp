//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISMonitorValues.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISMonitorValues::TCIISMonitorValues(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISMonitorValueRec *SetMValRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISValues;
  MValRec = SetMValRec;

  CIISObjType = CIISValues;
}

__fastcall TCIISMonitorValues::~TCIISMonitorValues()
{
  delete MValRec;
}

void __fastcall TCIISMonitorValues::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "MonitorValues: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISMonitorValues::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "MonitorValues: ParseCommand_End" );
	#endif
}


bool __fastcall TCIISMonitorValues::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( MValRec->DateTimeStamp ) &&
		 P->GetArg(2) == IntToStr( MValRec->RecNo ) &&
		 P->GetArg(3) == IntToStr( MValRec->SensorSerialNo ) &&
		 P->GetArg(4) == MValRec->ValueType;
}

void __fastcall TCIISMonitorValues::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
		if( P->GetArg( 200 ) == "GetFirstToEnd" )
			{
			if( DB->FindFirstMonitorValue() )P->CommandMode = CIISRecAdd;
			else
			{
				O->MessageCode = CIISMsg_RecNotFound;
				P->CommandMode = CIISCmdReady;
			}
		}
		else if( P->GetArg( 200 ) == "GetNextToEnd" )
		{
			if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) && P->ArgInc(4))
			{
				if( DB->LocateMonitorValue( CIISStrToDateTime( P->GetArg(1) ), StrToInt( P->GetArg(2) ), StrToInt( P->GetArg(3) ), P->GetArg(4)) )
				{
					if( DB->FindNextMonitorValue() ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_Ok;
						P->CommandMode = CIISCmdReady;
					}
				}
				else if( DB->FindFirstMonitorValue() )
				{
					DB->GetMonitorValueRec( MValRec, false );
					if( CIISStrToDateTime( P->GetArg(1) ) < MValRec->DateTimeStamp ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_RecNotFound;
						P->CommandMode = CIISCmdReady;
					}
				}
				else
				{
					O->MessageCode = CIISMsg_RecNotFound;
					P->CommandMode = CIISCmdReady;
				}
			}
			else if( P->ArgInc(99) )
			{
				O->MessageCode = CIISMsg_NoLongerSupported;
			}
		}
		else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
		}

		// Execute request
		if( P->CommandMode == CIISRecAdd )
		{

		#if DebugCIIDataTable == 1
		Debug( "Dataread started"  );
		#endif

		do
		{
			MoreRecords = DB->GetMonitorValueRec( MValRec, true );
			O->MessageData = O->MessageData +
			"1=" + CIISDateTimeToStr( MValRec->DateTimeStamp ) + "\r\n" +
			"2=" + IntToStr( MValRec->RecNo ) + "\r\n" +
			"3=" + IntToStr( MValRec->SensorSerialNo ) + "\r\n" +
			"4=" + MValRec->ValueType + "\r\n" +
			"5=" + MValRec->ValueUnit + "\r\n" +
			"6=" + CIISFloatToStr( MValRec->Value ) + "\r\n" +
			"7=" + CIISFloatToStr( MValRec->RawValue ) + "\r\n" +
			"8=" + CIISDateTimeToStr( MValRec->SampleDateTime ) + "\r\n";
		} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );

		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;

		#if DebugCIIDataTable == 1
		Debug( "Dataread ready"  );
		#endif

		}
		else if( P->CommandMode == CIISRecCount )
		{
		if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) && P->ArgInc(4))
		{
			MValRec->DateTimeStamp = CIISStrToDateTime( P->GetArg(1) );
			MValRec->RecNo = StrToInt( P->GetArg(2) );
			MValRec->SensorSerialNo = StrToInt( P->GetArg(3) );
			MValRec->ValueType = P->GetArg(4);

			O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetMonitorValuesRecCountAfter( MValRec ) ) + "\r\n";
		}
		else
		{
			O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetMonitorValuesRecCount() ) + "\r\n";
		}

	
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
	}
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISMonitorValues::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISMonitorValues::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISMonitorValues::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}
