//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CIISServerInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
__fastcall TCIISServerInterface::TCIISServerInterface( TCIISDBModule * SetDB,
                                                             TDebugWin * SetDebugWin,
                                                             String SetIP,
															 int32_t SetPortNo )
{
  DB = SetDB;
  DW = SetDebugWin;

  IPAdr = SetIP;
  PortNo = SetPortNo;

  TransferReply = NULL;
	FLoggedIn = false;

	TransferMessageList = new TList;
  TransferResponseList = new TList;

  MRec = new CIISMonitorValueRec;
	LPRRec = new CIISLPRValueRec;
	CVRec = new CIISCalcValueRec;
  DRec = new CIISDecayValueRec;


  Zones = new TList;
  Sensors = new TList;
  PSs = new TList;
	WLinks = new TList;
	Alarms = new TList;
  BIChannels = new TList;

  MessageNumber = 0;

  FSysSyncRunning = false;
	FDataSyncRunning = false;
	FTransferMessageRunning = false;
	FLoggInRunning = false;
  FOpenComRunning = false;
  FCloseComRunning = false;
	FCancelSyncRunning = false;

	SysSyncCancel = false;
  DataSyncCancel = false;
	SendTransferMessageCancel = false;
  LoggInCancel = false;
  OpenComCancel = false;
	CloseComCancel = false;

	ValuesTabSyncCancel = false;
  ValuesLPRTabSyncCancel = false;
  ValuesDecayTabSyncCancel = false;

	CheckSyncCancel = false;
	LastValSyncCancel = false;
	SyncMiscValCancel = false;
	CheckProjectCancel = false;
	ProjectTabSyncCancel = false;
	CtrlTabSyncCancel = false;
	EventTabSyncCancel = false;
	ZoneTabSyncCancel = false;
  SensorTabSyncCancel = false;
  PSTabSyncCancel = false;
  RecordingTabSyncCancel = false;
	WLinkTabSyncCancel = false;
	BIChannelTabSyncCancel = false;
	AlarmTabSyncCancel = false;

	TimerSMEnabled = false;
	TimerSMCancelEnabled = false;
	StopReEntrancy = false;


  EvTabStatusSet.Clear();

  if( IPAdr == "" ) IPAdr = "127.0.0.1";

	ClientSocket = new TCIIClientSocket( &MessageRecived, IPAdr, PortNo, DW );

	MiscValueGetLoggerLastRec = true;
  MiscValueLoggerEmpty = false;
  MiscRec = new CIISMiscValueRec;

	#if DebugNetIntRemote == 1
  Debug("CIISClientInterface Created");
  #endif
}

__fastcall TCIISServerInterface::~TCIISServerInterface()
{
	delete TransferMessageList;
  delete TransferResponseList;
  delete ClientSocket;
  delete MiscRec;
}

TCIIClientSocket* __fastcall TCIISServerInterface::GetClientSocket()
{
	return ClientSocket;
}

void __fastcall TCIISServerInterface::SetTransferReply( TransferReplyFP SetTP )
{
  TransferReply = SetTP;
}

bool __fastcall TCIISServerInterface::DataSyncIni()
{
	if( !ClientSocket->ServerConnected ) return false;
	DataSyncCancel = false;
  DataSyncState = CIISDataSyncStart;
	FDataSyncRunning = true;
	TimerSMEnabled = true;
  PrjRecDS = new CIISPrjRec;
  return true;
}

void __fastcall TCIISServerInterface::SM_DataSync()
{
	if( ClientSocket->ComTimeout ) DataSyncCancel = true;
	if( DataSyncCancel )
  {
		switch( DataSyncState )
		{
			case CIISDataSyncStart: DataSyncState = CIISDataSyncTimeout;
				break;

			case CIISDataSyncValues:
				ValuesTabSyncCancel = true;
				if( ValueTabSync() ) DataSyncState = CIISDataSyncTimeout;
				break;

			case CIISDataSyncLPR:
				ValuesLPRTabSyncCancel = true;
				if( ValueLPRTabSync() ) DataSyncState = CIISDataSyncTimeout;
				break;

			case CIISDataSyncCV:
				ValuesCVTabSyncCancel = true;
				if( ValueCVTabSync() ) DataSyncState = CIISDataSyncTimeout;
				break;

			case CIISDataSyncDecay:
				ValuesDecayTabSyncCancel = true;
				if( ValueDecayTabSync() ) DataSyncState = CIISDataSyncTimeout;
				break;

			case CIISDataSyncReady:
				FDataSyncRunning = false;
				DB->GetPrjRec( PrjRecDS );
				PrjRecDS->ServerStatus = CIIDataSync;
				DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
				delete PrjRecDS;

				#if DebugNetIntRemote == 1
				Debug( "Ser Int: DataSync ready" );
				#endif
				break;

			case CIISDataSyncError:
				FDataSyncRunning = false;
				DB->GetPrjRec( PrjRecDS );
				PrjRecDS->ServerStatus = CIISConnected;
				DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
				delete PrjRecDS;

				#if DebugNetIntRemote == 1
				Debug( "Ser Int: DataSync faild: Error" );
				#endif
				break;

			case CIISDataSyncTimeout:
				FDataSyncRunning = false;
				DB->GetPrjRec( PrjRecDS );
				PrjRecDS->ServerStatus = CIISDisconnected;
				DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
				delete PrjRecDS;

				#if DebugNetIntRemote == 1
				Debug( "Ser Int: DataSync faild: Com TimeOut" );
				#endif
				break;
		}
		return;
	} //  DataSyncCancel

  switch ( DataSyncState )
  {
		case CIISDataSyncStart:
			ValueTabSyncIni();
			DataSyncState = CIISDataSyncValues;
			break;

		case CIISDataSyncValues:
			if( ValueTabSync() )
			{
				ValueLPRTabSyncIni();
				DataSyncState = CIISDataSyncLPR;
			}
			break;

		case CIISDataSyncLPR:
			if( ValueLPRTabSync() )
			{
				ValueCVTabSyncIni();
				DataSyncState = CIISDataSyncCV;
			}
			break;

		case CIISDataSyncCV:
			if( ValueCVTabSync() )
			{
				ValueDecayTabSyncIni();
				DataSyncState = CIISDataSyncDecay;
			}
			break;

		case CIISDataSyncDecay:
			if( ValueDecayTabSync() )
			{
				DataSyncState = CIISDataSyncReady;
			}
			break;

		case CIISDataSyncReady:
			FDataSyncRunning = false;
			DB->GetPrjRec( PrjRecDS );
			PrjRecDS->ServerStatus = CIIDataSync;
			DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
			delete PrjRecDS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: DataSync ready" );
			#endif
			break;

		case CIISDataSyncError:
			FDataSyncRunning = false;
			DB->GetPrjRec( PrjRecDS );
			PrjRecDS->ServerStatus = CIISConnected;
			DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
			delete PrjRecDS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: DataSync faild: Error" );
			#endif
			break;

		case CIISDataSyncTimeout:
			FDataSyncRunning = false;
			DB->GetPrjRec( PrjRecDS );
			PrjRecDS->ServerStatus = CIISDisconnected;
			DB->SetPrjRec( PrjRecDS ); DB->ApplyUpdatesPrj();
			delete PrjRecDS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: DataSync faild: Com TimeOut" );
			#endif
			break;
  }
  return;
}

bool __fastcall TCIISServerInterface::SystemSyncIni()
{
	if( SM_Running()  ) return false;

	SysSyncCancel = false;
	PrjRecSS = new CIISPrjRec;
  SysSyncState = CIISSysSyncStart;
  FSysSyncRunning = true;
	TimerSMEnabled = true;

	if( MiscValueGetLoggerLastRec )
  {
		MiscValSyncIni();
		SysSyncState = CIISSysSyncMiscValIni;
  }
	else SysSyncState = CIISSysSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: SystemSyncIni ");
  #endif

  return true;
}

void __fastcall TCIISServerInterface::SM_SystemSync()
{
	if( SysSyncCancel )
	{
		switch( SysSyncState )
		{
			case CIISSysSyncStart:
			case CIISSysSyncMiscValIni:
			case CIISSysSyncReadyAfterCheck:
			 SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncCtrl:
			CtrlTabSyncCancel = true;
			if( SM_CtrlTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncProject:
			ProjectTabSyncCancel = true;
			if( SM_ProjectTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysCheckProject:
			CheckProjectCancel = true;
			if( SM_ProjectTabCheck() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncMiscVal:
			SyncMiscValCancel = true;
			if( SM_MiscValSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncLastVal:
			LastValSyncCancel = true;
			if( SM_LastValSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncCheck:
			CheckSyncCancel = true;
			if( SM_CheckSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncEvent:
			EventTabSyncCancel = true;
			if( EventTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncZone:
			ZoneTabSyncCancel = true;
			if( ZoneTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncSensor:
			SensorTabSyncCancel = true;
			if( SensorTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncAlarm:
			AlarmTabSyncCancel = true;
			if( AlarmTabSync() ) SysSyncState = CIISSysSyncTimeout;

			case CIISSysSyncPS:
			PSTabSyncCancel = true;
			if( PSTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncWLink:
			WLinkTabSyncCancel = true;
			if( WLinkTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncBIChannel:
			BIChannelTabSyncCancel = true;
			if( BIChannelTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncRec:
			RecordingTabSyncCancel = true;
			if( RecordingTabSync() ) SysSyncState = CIISSysSyncTimeout;
			break;

			case CIISSysSyncReady:
			FSysSyncRunning = false;
			SysSyncCancel = false;
			DB->GetPrjRec( PrjRecSS );
			PrjRecSS->ServerStatus = CIISystemSync;
			DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();

			if( PrjRecSS != NULL ) delete PrjRecSS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: SystemSync ready" );
			#endif
			break;

			case CIISSysSyncError:
			FSysSyncRunning = false;
			SysSyncCancel = false;
			DB->GetPrjRec( PrjRecSS );
			PrjRecSS->ServerStatus = CIISConnected;
			DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();

			if( PrjRecSS != NULL ) delete PrjRecSS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: SystemSync faild" );
			#endif
			break;

			case CIISSysSyncTimeout:
			FSysSyncRunning = false;
			SysSyncCancel = false;
			DB->GetPrjRec( PrjRecSS );
			PrjRecSS->ServerStatus = CIISDisconnected;
			DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();

			if( PrjRecSS != NULL ) delete PrjRecSS;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: SystemSync faild: Com TimeOut" );
			#endif
			break;

			default: break;
		}
		return;
	} // SysSyncCancel


	switch ( SysSyncState )
  {
	case CIISSysSyncMiscValIni:
		if( SM_MiscValSync() )
	  {
			SysSyncState = CIISSysSyncStart;

			#if DebugNetIntRemote == 1
			Debug( "Ser Int: SystemSyncStart with MiscValIni ");
			#endif
	  }
	  break;

	case CIISSysSyncStart:
		CheckSyncIni();
		SysSyncState = CIISSysSyncCheck;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SystemSyncStart ");
	  #endif
	  break;

	case CIISSysSyncCheck:
		if( SM_CheckSync() )
		{
			if( PrjTabStatusSet.Contains( PTSDataCh ))
			{
				DB->GetPrjRec( PrjRecSS );
				PrjRecSS->ServerStatus = CIISystemSync;
				DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();
				SysSyncState = CIISSysSyncReady;
			}

			if( PrjTabStatusSet.Contains( PTSConfigCh ))
			{
				DB->GetPrjRec( PrjRecSS );
				PrjRecSS->ServerStatus = CIISConnected;
				DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();
				ProjectTabCeckIni();
				SysSyncState = CIISSysCheckProject;
			}
			else if( PrjTabStatusSet.Contains( PTSLastValCh ))
			{
				DB->GetPrjRec( PrjRecSS );
				if( PrjRecSS->DBVer >= 3800 )
				{
					MiscValSyncIni();
					SysSyncState = CIISSysSyncMiscVal;
				}
				else
				{
					LastValSyncIni();
					SysSyncState = CIISSysSyncLastVal;
				}

			}
			else SysSyncState = CIISSysSyncReadyAfterCheck;
	  }
	  break;

	case CIISSysSyncReadyAfterCheck:
	  FSysSyncRunning = false;
		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SystemSync ready after check" );
	  #endif
	  TransferResponse();
	  break;

	case CIISSysSyncLastVal:
		if( SM_LastValSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: LastValSync Ready ");
			#endif
			SysSyncState = CIISSysSyncReady;
	  }
	  break;

	case CIISSysSyncMiscVal:
		if( SM_MiscValSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: MiscValSync Ready ");
			#endif
			SysSyncState = CIISSysSyncReady;
	  }
	  break;

	case CIISSysCheckProject:
		if( SM_ProjectTabCheck() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: ProjectTab Checked ");
			#endif
			EventTabSyncIni();
			SysSyncState = CIISSysSyncEvent;
	  }
		break;

	case CIISSysSyncEvent:
		if( EventTabSync() )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: EventTabSync Ready ");
			#endif

			DB->GetPrjRec( PrjRecSS );
			if( PrjRecSS->ServerMode == CIISNotInitiated )
			{
				EvTabStatusSet << ETSPrjCh;
				EvTabStatusSet << ETSCtrlCh;
				EvTabStatusSet << ETSZoneCh;
				EvTabStatusSet << ETSSensorCh;
				EvTabStatusSet << ETSPSCh;
				EvTabStatusSet << ETSWLinkCh;
				EvTabStatusSet << ETSAlarmCh;
				EvTabStatusSet << ETSBIChannelCh;
				EvTabStatusSet << ETSRecCh;
			}

			// if( FConfigChanged || FDataChanged || FLastValChanged ) EvTabStatusSet << ETSPrjCh;

			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncProject:
		if( SM_ProjectTabSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: ProjectTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSPrjCh;
			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncCtrl:
		if( SM_CtrlTabSync() )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: CtrlTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSCtrlCh;
			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncZone:
	  if( ZoneTabSync() )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: ZoneTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSZoneCh;
			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncSensor:
	  if( SensorTabSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: SensorTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSSensorCh;
			SystemSyncSelectStep();
	  }
		break;

	case CIISSysSyncAlarm:
		if( AlarmTabSync() )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: AlarmTabSync Ready " );
			#endif

			EvTabStatusSet >> ETSAlarmCh;
			SystemSyncSelectStep();
		}
		break;

	case CIISSysSyncPS:
	  if( PSTabSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: PSTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSPSCh;
			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncWLink:
		if( WLinkTabSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: WLinkTabSync Ready ");
			#endif

			EvTabStatusSet >> ETSWLinkCh;
			SystemSyncSelectStep();
		}
		break;

	case CIISSysSyncBIChannel:
		if( BIChannelTabSync() )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: BIChannelTabSync Ready " );
			#endif

			EvTabStatusSet >> ETSBIChannelCh;
			SystemSyncSelectStep();
		}
		break;

	case CIISSysSyncRec:
	  if( RecordingTabSync() )
	  {
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: RecordingTabSync Ready ");
			#endif
			EvTabStatusSet >> ETSRecCh;
			SystemSyncSelectStep();
	  }
	  break;

	case CIISSysSyncReady:
	  FSysSyncRunning = false;
	  DB->GetPrjRec( PrjRecSS );
		PrjRecSS->ServerStatus = CIISystemSync;
	  DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();
	  TransferResponse();

	  if( PrjRecSS != NULL ) delete PrjRecSS;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SystemSync ready" );
	  #endif
	 
	  break;

	case CIISSysSyncError:
	  FSysSyncRunning = false;
	  DB->GetPrjRec( PrjRecSS );
		PrjRecSS->ServerStatus = CIISConnected;
	  DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();
	  TransferResponse();

	  if( PrjRecSS != NULL ) delete PrjRecSS;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SystemSync faild" );
	  #endif
	  break;

	case CIISSysSyncTimeout:
	  FSysSyncRunning = false;
	  DB->GetPrjRec( PrjRecSS );
		PrjRecSS->ServerStatus = CIISDisconnected;
	  DB->SetPrjRec( PrjRecSS ); DB->ApplyUpdatesPrj();
	  TransferResponse();

	  if( PrjRecSS != NULL ) delete PrjRecSS;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SystemSync faild: Com TimeOut" );
	  #endif
	  break;

	  default: break;
  }
	return;
}

void __fastcall TCIISServerInterface::SystemSyncSelectStep()
{
	if( EvTabStatusSet.Contains( ETSPrjCh ))
  {
   ProjectTabSyncIni();
   SysSyncState = CIISSysSyncProject;
  }
  else if( EvTabStatusSet.Contains( ETSCtrlCh ) )
  {
   CtrlTabSyncIni();
   SysSyncState = CIISSysSyncCtrl;
  }
  else if( EvTabStatusSet.Contains( ETSZoneCh ) )
  {
   ZoneTabSyncIni();
   SysSyncState = CIISSysSyncZone;
  }
  else if( EvTabStatusSet.Contains( ETSSensorCh ) )
  {
   SensorTabSyncIni();
   SysSyncState = CIISSysSyncSensor;
	}
	else if( EvTabStatusSet.Contains( ETSAlarmCh  ))
	{
		AlarmTabSyncIni();
		SysSyncState = CIISSysSyncAlarm;
  }
	else if( EvTabStatusSet.Contains( ETSPSCh ) )
  {
	 PSTabSyncIni();
   SysSyncState = CIISSysSyncPS;
  }
	else if( EvTabStatusSet.Contains( ETSWLinkCh ) )
	{
	 WLinkTabSyncIni();
	 SysSyncState = CIISSysSyncWLink;
	}
	else if( EvTabStatusSet.Contains( ETSBIChannelCh ) )
	{
	 BIChannelTabSyncIni();
	 SysSyncState = CIISSysSyncBIChannel;
	}
  else if( EvTabStatusSet.Contains( ETSRecCh ) )
  {
   RecordingTabSyncIni();
   SysSyncState = CIISSysSyncRec;
  }
  else SysSyncState = CIISSysSyncReady;
}

void __fastcall TCIISServerInterface::CheckSyncIni()
{
	PrjRecChS = new CIISPrjRec;
	CheckSyncState = CIISCheckSyncStart;
	CheckSyncCancel = false;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: CheckSyncIni ");
  #endif
}

bool __fastcall TCIISServerInterface::SM_CheckSync()
{
  TCamurPacket *Reply;
	String Data;
  bool Ready, Changed;

	Ready = false;

	if( CheckSyncCancel  )
	{
		CheckSyncState = CIISCheckSyncReady;
	}

	switch( CheckSyncState )
  {
		case CIISCheckSyncStart:
	  PrjTabStatusSet.Clear();
	  CheckSyncState = CIISCheckSyncRequest;
	  break;

    case CIISCheckSyncRequest:
		Data = "200=GetConfigDataVer\r\n";
	  RequestNo = Request( CIISRead, CIISProject, Data );
	  CheckSyncState = CIISCheckSyncRead;
	  break;

    case CIISCheckSyncRead:
		Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				DB->GetPrjRec( PrjRecChS );
				Changed = false;
				if( PrjRecChS->DataVer != StrToInt( Reply->GetArg(2) ) )
				{
					PrjTabStatusSet << PTSDataCh;
					Changed = true;
				}
				if( PrjRecChS->ConfigVer != StrToInt( Reply->GetArg(1) ) )
				{
					PrjTabStatusSet << PTSConfigCh;
					Changed = true;
				}
				if( PrjRecChS->LastValVer != StrToInt( Reply->GetArg(3) ) )
				{
					PrjTabStatusSet << PTSLastValCh;
					Changed = true;
				}
				//CheckSyncState = CIISCheckSyncReady;

				if( Changed )
				{
					PrjRecChS->ConfigVer = StrToInt( Reply->GetArg(1));
					PrjRecChS->DataVer = StrToInt( Reply->GetArg(2));
					PrjRecChS->LastValVer = StrToInt( Reply->GetArg(3));
					DB->SetPrjRec( PrjRecChS ); DB->ApplyUpdatesPrj();

					ApplyUpdateDelay = Now() + 1.5 * OneSecond;
					CheckSyncState = CIISCheckSyncCtrlDBUppdate;

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: Wait for DB update on Ctrl");
					Debug( "Ser Int: Config/Data/Last Ver Updated");
					#endif
				}
				else CheckSyncState = CIISCheckSyncReady;
			}
			else CheckSyncState = CIISCheckSyncError;
			delete Reply;
		} // Reply
		break;

	case CIISCheckSyncCtrlDBUppdate:   // Wait for DB ApplyUpdate in CIILogger on Ctrl PC
		if( Now() > ApplyUpdateDelay ) CheckSyncState = CIISCheckSyncReady;
		break;

	case CIISCheckSyncError:
	  DB->GetPrjRec( PrjRecChS );
	  PrjRecChS->ServerStatus = CIISDisconnected;
	  DB->SetPrjRec( PrjRecChS ); DB->ApplyUpdatesPrj();
		CheckSyncState = CIISCheckSyncReady;
	  break;

	case CIISCheckSyncReady:
	  delete PrjRecChS;
	  Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CheckSync Ready");
	  #endif
	  break;

	  default: break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::LastValSyncIni()
{
  LastValSyncState = CIISLastValSyncStart;
	SRec = new CIISSensorRec;
	PSRec = new CIISPowerSupplyRec;
	MiscRec = new CIISMiscValueRec;
	LastValSyncCancel = false;
  return;
}

bool __fastcall TCIISServerInterface::SM_LastValSync()
{
  TCamurPacket *Reply;
  String Data, Data1, Data2, Data3;
  bool Ready;

	Ready = false;

	if( LastValSyncCancel )
	{
	 LastValSyncState = CIISLastValSyncReady;
	}

  switch (LastValSyncState)
	{
	case CIISLastValSyncStart:
		LastValSyncState = CIISLastValSyncSensorRequestFirst;
		break;

	case CIISLastValSyncSensorRequestFirst:
		Data = "200=GetLastValFirstToEnd\r\n";
	  RequestNo = Request( CIISRead, CIISSensors, Data );
	  LastValSyncState = CIISLastValSyncSensorRead;
		break;

	case CIISLastValSyncSensorRequest:
		Data = "1=" + LastSensor + "\r\n" +
			 "200=GetLastValNextToEnd" + "\r\n";
	  RequestNo = Request( CIISRead, CIISSensors, Data );
	  LastValSyncState = CIISLastValSyncSensorRead;
		break;

	case CIISLastValSyncSensorRead:
		Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
	  {
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					LastValSyncState = CIISLastValSyncPSRequestFirst;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						Data1 = Reply->GetDelArg(1);
						Data2 = Reply->GetDelArg(6);
						Data3 = Reply->GetDelArg(27);
						LastSensor = Data1;

						SRec->SensorSerialNo = StrToInt( Data1 );
						if( DB->LocateSensorRec( SRec ) )
						{
							DB->GetSensorRec( SRec, false );
							if(Data2 != "" )
							{
								MiscRec->Value = CIISStrToFloat( Data2 );
								MiscRec->DateTimeStamp = TDate(1980,1,1);
								MiscRec->SensorSerialNo = SRec->SensorSerialNo;
								MiscRec->ValueType = "U";
								MiscRec->ValueUnit = SRec->SensorUnit;
							}
							DB->AppendMiscValueRec( MiscRec );

							if(Data3 != "" )
							{
								MiscRec->Value = CIISStrToFloat( Data3 );
								MiscRec->ValueType = "C";
								MiscRec->ValueUnit = SRec->SensorUnit2;
							}
							DB->AppendMiscValueRec( MiscRec );
						}
					}
					DB->ApplyUpdatesMiscValue( ValuesMiscSize );
					LastValSyncState = CIISLastValSyncSensorRequest;
				}
			} // CIIMsgOk
			else
			{
				SensTabStatusSet << STSLocalError;
				LastValSyncState = CIISLastValSyncReady;
			}
			delete Reply;
		} // Reply
		break;

	case CIISLastValSyncPSRequestFirst:
		Data = "200=GetLastValFirstToEnd\r\n";
	  RequestNo = Request( CIISRead, CIISPowerSupply, Data );
	  LastValSyncState = CIISLastValSyncPSRead;
		break;

	case CIISLastValSyncPSRequest:
		Data = "1=" + LastPS + "\r\n" +
			 "200=GetLastValNextToEnd" + "\r\n";
	  RequestNo = Request( CIISRead, CIISPowerSupply, Data );
	  LastValSyncState = CIISLastValSyncPSRead;
		break;

	case CIISLastValSyncPSRead:
		Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					LastValSyncState = CIISLastValSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						Data1 = Reply->GetDelArg(1);
						Data2 = Reply->GetDelArg(7);
						Data3 = Reply->GetDelArg(8);
						LastPS = Data1;

						PSRec->PSSerialNo = StrToInt( Data1 );
						if( DB->LocatePSRec( PSRec ) )
						{
						DB->GetPSRec( PSRec, false );
						MiscRec->Value = CIISStrToFloat( Data2 );
						MiscRec->DateTimeStamp = TDate(1980,1,1);
						MiscRec->SensorSerialNo = PSRec->PSSerialNo;
						MiscRec->ValueType = "PSU";
						MiscRec->ValueUnit = PSRec->Ch1Unit;

						DB->AppendMiscValueRec( MiscRec );

						MiscRec->Value = CIISStrToFloat( Data3 );
						MiscRec->ValueType = "PSI";
						MiscRec->ValueUnit = PSRec->Ch2Unit;

						DB->AppendMiscValueRec( MiscRec );
						}
					}
					DB->ApplyUpdatesMiscValue( ValuesMiscSize );

					LastValSyncState = CIISLastValSyncPSRequest;
				}
			} // CIIMsgOk
			else
			{
				SensTabStatusSet << STSLocalError;
				LastValSyncState = CIISLastValSyncReady;
			}
			delete Reply;
	  } // Reply
		break;

	case CIISLastValSyncReady:
		delete PSRec;
	  delete SRec;
	  delete MiscRec;
	  Ready = true;
		break;

	default: break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::MiscValSyncIni()
{
  MiscValSyncState = MV_StartSync;
	SyncMiscValCancel = false;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: MiscValSyncIni ");
	#endif

	return;
}

bool __fastcall TCIISServerInterface::SM_MiscValSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

	Ready = false;

	if( SyncMiscValCancel )
	{
		MiscValSyncState = MV_SyncReady;
	}

  switch (MiscValSyncState)
  {
	case MV_StartSync:
		if( MiscValueGetLoggerLastRec ) MiscValSyncState = MV_RequestNewLastRec;
	  else if( MiscValueLoggerEmpty ) MiscValSyncState = MV_RequestFirstRec;
	  else MiscValSyncState = MV_RequestNextRec;
		break;

	case MV_RequestNewLastRec:   //MiscRec is init on connect to logger
		Data = "200=GetLast\r\n";
	  RequestNo = Request( CIISRead, CIISValuesMisc, Data );
	  MiscValSyncState = MV_ReadLastRec;
		break;

	case MV_ReadLastRec:
		Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					MiscValueLoggerEmpty = true;
					MiscValueGetLoggerLastRec = false;
					MiscValSyncState = MV_SyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						MiscRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
						MiscRec->SensorSerialNo = StrToInt( Reply->GetDelArg(2) );
						MiscRec->ValueType = Reply->GetDelArg(3);
						MiscRec->ValueUnit = Reply->GetDelArg(4);
						MiscRec->Value = CIISStrToFloat( Reply->GetDelArg(5) );
					}
					MiscValueLoggerEmpty = false;
					MiscValueGetLoggerLastRec = false;
					MiscValSyncState = MV_SyncReady;
				}
			} // CIIMsgOk
			else
			{
				SensTabStatusSet << STSLocalError;
				MiscValSyncState = MV_SyncReady;
			}
			delete Reply;
	  } // Reply
		break;

	case MV_RequestFirstRec:
		Data = "200=GetFirstToEnd\r\n";
	  RequestNo = Request( CIISRead, CIISValuesMisc, Data );
	  MiscValSyncState = MV_ReadRec;
		break;

	case MV_RequestNextRec:
		Data = "1=" + CIISDateTimeToStr( MiscRec->DateTimeStamp ) + "\r\n" +
			 "2=" + IntToStr( MiscRec->SensorSerialNo ) + "\r\n" +
			 "3=" + MiscRec->ValueType + "\r\n" +
			 "200=GetNextToEnd" + "\r\n";
	  RequestNo = Request( CIISRead, CIISValuesMisc, Data );
	  MiscValSyncState = MV_ReadRec;
		break;

	case MV_ReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					MiscValSyncState = MV_SyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						MiscRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
						MiscRec->SensorSerialNo = StrToInt( Reply->GetDelArg(2) );
						MiscRec->ValueType = Reply->GetDelArg(3);
						MiscRec->ValueUnit = Reply->GetDelArg(4);
						MiscRec->Value = CIISStrToFloat( Reply->GetDelArg(5) );

						DB->AppendMiscValueRec( MiscRec );
					}
					DB->ApplyUpdatesMiscValue( ValuesMiscSize );
					MiscValSyncState = MV_RequestNextRec;

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: MV_ReadRec to " + DateTimeToStr( MiscRec->DateTimeStamp ));
					#endif
				}
			} // CIIMsgOk
			else
			{
				SensTabStatusSet << STSLocalError;
				MiscValSyncState = MV_SyncReady;
			}
			delete Reply;
	  } // Reply
		break;

	case MV_SyncReady:
		Ready = true;
		break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::ProjectTabCeckIni()
{
	PrjRecPC = new CIISPrjRec;
	CheckProjectCancel = false;
	ProjectSyncState = CIISPrjSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ProjectTabCeckIni ");
	#endif

}

bool __fastcall TCIISServerInterface::SM_ProjectTabCheck()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

	Ready = false;

	if( CheckProjectCancel )
	{
		ProjectSyncState = CIISPrjSyncReady;
	}

  switch (ProjectSyncState)
  {
	case CIISPrjSyncStart:
	  PrjTabStatusSet.Clear();
	  ProjectSyncState = CIISPrjSyncRequest;
	  break;

	case CIISPrjSyncRequest:
	  Data = "200=GetProjectNo\r\n";
	  RequestNo = Request( CIISRead, CIISProject, Data );
	  ProjectSyncState = CIISPrjSyncReadRec;
	  break;

	case CIISPrjSyncReadRec:
	  Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
	  {
		if( Reply->MessageCode != CIISMsg_Ok ) PrjTabStatusSet << PTSLocalError;
		else
		{
		  if( Reply->MessageData == "" ) PrjTabStatusSet << PTSMissingRec;
		  else
		  {
			DB->GetPrjRec( PrjRecPC  );
			if( PrjRecPC->PrjNo != Reply->GetArg(2) ) PrjTabStatusSet << PTSPrjNoDontMatch;
		  }
		}
		ProjectSyncState = CIISPrjSyncReady;
		delete Reply;
	  }
	  break;

	case CIISPrjSyncReady:
	  delete PrjRecPC;
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: ProjectTabCeckIni Ready ");
		#endif

	  break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::EventTabSyncIni()
{
  EvRec = new CIISEventRec;
	EventSyncState = CIISEventSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: EventTabSyncIni ");
	#endif

}

bool __fastcall TCIISServerInterface::EventTabSync()
{
  TCamurPacket *Reply;
  String Data;
  TDateTime FromDate;
  int32_t FromEvNo;
  bool Ready, MoreToRead;

  Ready = false;

	if( EventTabSyncCancel )
  {
		switch( EventSyncState )
		{
		case CIISEventSyncStart:
		case CIISEventSyncReadRec:
		case CIISEventSyncRequestFirst:
		case CIISEventSyncReady:
			break;
		case CIISEventSyncRequestNext:
			DB->ApplyUpdatesEvent();
			break;
		}
		EventTabSyncCancel = false;
		EventSyncState = CIISEventSyncReady;
	} // EventTabSyncCancel

	switch( EventSyncState )
  {
	case CIISEventSyncStart:
		EvTabStatusSet.Clear();
		EventSyncState = CIISEventSyncRequestFirst;
		break;

  case CIISEventSyncRequestFirst:
		if( DB->FindLastEvent() )
		{
			DB->GetEventRec( EvRec, false );
			Data = "1=" + CIISDateTimeToStr( EvRec->EventDateTime ) + "\r\n" +
				 "5=" + IntToStr( EvRec->EventNo ) + "\r\n" +
				 "200=GetNextToEnd" + "\r\n";
		}
		else // No records in User EvTab
		{
			EvTabStatusSet << ETSMissingRec;
			Data = "200=GetFirstToEnd\r\n";
		}
		RequestNo = Request( CIISRead, CIISEvent, Data );
		EventSyncState = CIISEventSyncReadRec;
		break;

  case CIISEventSyncRequestNext:
		Data = "1=" + CIISDateTimeToStr( EvRec->EventDateTime ) + "\r\n" +
				 "5=" + IntToStr( EvRec->EventNo ) + "\r\n" +
				 "200=GetNextToEnd" + "\r\n";

		RequestNo = Request( CIISRead, CIISEvent, Data );
		DB->ApplyUpdatesEvent();
		EventSyncState = CIISEventSyncReadRec;
		break;

	case CIISEventSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					DB->ApplyUpdatesEvent();
					EventSyncState = CIISEventSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						EvRec->EventDateTime = CIISStrToDateTime(Reply->GetDelArg(1));
						EvRec->EventType = StrToInt(Reply->GetDelArg(2));
						EvRec->EventCode = StrToInt(Reply->GetDelArg(3));
						EvRec->EventLevel = StrToInt(Reply->GetDelArg(4));
						EvRec->EventNo = StrToInt(Reply->GetDelArg(5));
						EvRec->EventString = Reply->GetDelArg(6);
						EvRec->EventInt = StrToInt(Reply->GetDelArg(7));
						EvRec->EventFloat = CIISStrToFloat(Reply->GetDelArg(8));

						SetEvTabStatus( EvRec->EventCode );

						DB->AppendEventRec( EvRec );
					}
					EventSyncState = CIISEventSyncRequestNext;
				}
			}
			else if( Reply->MessageCode == CIISMsg_RecNotFound )// requested rec is no longer available on logger server;
			{
				EvTabStatusSet << ETSMissingRec;
				Data = "200=GetFirstToEnd\r\n";
				RequestNo = Request( CIISRead, CIISEvent, Data );
				EventSyncState = CIISEventSyncReadRec;
			}
			delete Reply;
		}
		break;

	case CIISEventSyncReady:
		delete EvRec;
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: EventTabSync Ready ");
		#endif
		break;
  }
	return Ready;
}

void __fastcall TCIISServerInterface::ProjectTabSyncIni()
{
	PrjRec = new CIISPrjRec;
	ProjectTabSyncCancel = false;
  ProjectSyncState = CIISPrjSyncStart;
}

bool __fastcall TCIISServerInterface::SM_ProjectTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

	Ready = false;

	if( ProjectTabSyncCancel )
	{
		ProjectSyncState = CIISPrjSyncReady;
	}

  switch (ProjectSyncState)
  {
    case CIISPrjSyncStart:
	  PrjTabStatusSet.Clear();
	  ProjectSyncState = CIISPrjSyncRequest;
	  break;

    case CIISPrjSyncRequest:
	  Data = "200=GetProject\r\n";
      RequestNo = Request( CIISRead, CIISProject, Data );
	  ProjectSyncState = CIISPrjSyncReadRec;
	  break;

    case CIISPrjSyncReadRec:
	  Reply = ClientSocket->GetReply( RequestNo );
      if( Reply != NULL )
	  {
		if( Reply->MessageCode == CIISMsg_Ok )
		{
		  if( Reply->MessageData != "" )
		  {
			DB->GetPrjRec( PrjRec );
			PrjRec->PrjName = Reply->GetArg(1);
			PrjRec->PrjNo = Reply->GetArg(2);
			PrjRec->Client = Reply->GetArg(3);
			PrjRec->Consultant = Reply->GetArg(4);
			PrjRec->Manager = Reply->GetArg(5);
			PrjRec->ContractDescription = Reply->GetArg(6);
			PrjRec->MMResponsibility = Reply->GetArg(7);
			if( Reply->GetArg(8) != "" )
			  PrjRec->Commissioning = CIISStrToDateTime(Reply->GetArg(8));
			PrjRec->Drawings = Reply->GetArg(9);
			PrjRec->Criteria = Reply->GetArg(10);
			PrjRec->ConnType = StrToInt(Reply->GetArg(11));
			PrjRec->ConnRemote = Reply->GetArg(12);
			//DB->TabProject->FieldValues["ConnServerIP"] = Reply->GetArg(13); // ???????????
			PrjRec->PrjAlarmStatus = StrToInt(Reply->GetArg(14));
			PrjRec->EventLogSize = StrToInt(Reply->GetArg(15));
			PrjRec->EventLevel = StrToInt(Reply->GetArg(16));
			PrjRec->ServerStatus = CIISConnected;
			PrjRec->ServerMode = CIISRemoteServer;
			PrjRec->ConfigVer = StrToInt(Reply->GetArg(19));
			PrjRec->DataVer = StrToInt(Reply->GetArg(20));
			PrjRec->LastValVer = StrToInt(Reply->GetArg(21));
			if( Reply->ArgInc(22) ) PrjRec->DBVer = StrToInt(Reply->GetArg(22));
			else PrjRec->DBVer = 0;
			if( Reply->ArgInc(24) ) PrjRec->PWReadOnly = Reply->GetArg(24);
			if( Reply->ArgInc(25) ) PrjRec->PWModify = Reply->GetArg(25);
			if( Reply->ArgInc(26) ) PrjRec->PWErase = Reply->GetArg(26);
			if( Reply->ArgInc(27) ) PrjRec->PrgVer = StrToInt( Reply->GetArg(27) );
			if( Reply->ArgInc(28) ) PrjRec->DTAdj = CIISStrToDateTime(Reply->GetArg(28));
			if( Reply->ArgInc(29) ) PrjRec->DTNew = CIISStrToDateTime(Reply->GetArg(29));

		   DB->SetPrjRec( PrjRec ); DB->ApplyUpdatesPrj();

		   ProjectSyncState = CIISPrjSyncReady;
          }
          else
          {
			PrjTabStatusSet << PTSMissingRec;
            ProjectSyncState = CIISPrjSyncReady;
          }
        } // CIIMsgOk
        else
        {
            PrjTabStatusSet << PTSLocalError;
            ProjectSyncState = CIISPrjSyncReady;
        }
        delete Reply;
	  } // Reply
	  break;

	case CIISPrjSyncReady:
	  delete PrjRec;
	  Ready = true;
	  break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::CtrlTabSyncIni()
{
	CtrlTabSyncCancel = false;
  CtrlRec = new CIISCtrlRec;
	CtrlSyncState = CIISCtrlSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: CtrlTabSyncIni ");
	#endif
}

bool __fastcall TCIISServerInterface::SM_CtrlTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

	if( CtrlTabSyncCancel )
	{
		CtrlSyncState = CIISCtrlSyncReady;
	}
	Ready = false;

  switch (CtrlSyncState)
  {
	case CIISCtrlSyncStart:
	  CtrlTabStatusSet.Clear();
		CtrlSyncState = CIISCtrlSyncRequest;
	  break;

    case CIISCtrlSyncRequest:
	  Data = "200=GetCtrl\r\n";
	  RequestNo = Request( CIISRead, CIISController, Data );
	  CtrlSyncState = CIISCtrlSyncReadRec;
	  break;

	case CIISCtrlSyncReadRec:
	  Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData != "" )
				{
				 DB->FindFirstCtrl(); DB->GetCtrlRec( CtrlRec, false );

				 CtrlRec->CtrlName = Reply->GetArg(1);
				 CtrlRec->NoIR = CIISStrToBool( Reply->GetArg(2) );
				 if( Reply->GetArg(3) != "" ) CtrlRec->ScheduleDateTime = CIISStrToDateTime(Reply->GetArg(3));
				 if( Reply->GetArg(4) != "" ) CtrlRec->SchedulePeriod = StrToInt(Reply->GetArg(4));
				 CtrlRec->DecaySampInterval = StrToInt(Reply->GetArg(5));
				 CtrlRec->DecayDuration = StrToInt(Reply->GetArg(6));
				 CtrlRec->LPRRange = StrToInt(Reply->GetArg(7));
				 CtrlRec->LPRStep = StrToInt(Reply->GetArg(8));
				 CtrlRec->LPRDelay1 = StrToInt(Reply->GetArg(9));
				 CtrlRec->LPRDelay2 = StrToInt(Reply->GetArg(10));
				 CtrlRec->SampInterval1 = StrToInt(Reply->GetArg(11));
				 CtrlRec->SampInterval2 = StrToInt(Reply->GetArg(12));
				 CtrlRec->SampInterval3 = StrToInt(Reply->GetArg(13));
				 CtrlRec->SampInterval4 = StrToInt(Reply->GetArg(14));
				 CtrlRec->SampInterval5 = StrToInt(Reply->GetArg(15));
				 CtrlRec->NextCanAdr = StrToInt(Reply->GetArg(16));
				 CtrlRec->DefaultSensorLow = CIISStrToFloat(Reply->GetArg(17));
				 CtrlRec->DefaultSensorHigh = CIISStrToFloat(Reply->GetArg(18));
				 CtrlRec->DefaultAlarmEnable = CIISStrToBool( Reply->GetArg(19) );
				 CtrlRec->CtrlAlarmStatus = StrToInt(Reply->GetArg(20));
				 CtrlRec->USBCANStatus = StrToInt(Reply->GetArg(21));
				 CtrlRec->NodeCount = StrToInt(Reply->GetArg(22));
				 CtrlRec->MonitorCount = StrToInt(Reply->GetArg(23));
				 CtrlRec->DecayCount = StrToInt(Reply->GetArg(24));
				 CtrlRec->LPRCount = StrToInt(Reply->GetArg(25));
				 if( Reply->ArgInc(26) ) CtrlRec->SchedulePeriodUnit = StrToInt(Reply->GetArg(26));
				 if( Reply->ArgInc(27) ) CtrlRec->ScheduleDecay = CIISStrToBool( Reply->GetArg(27) );
				 if( Reply->ArgInc(28) ) CtrlRec->ScheduleLPR = CIISStrToBool( Reply->GetArg(28) );
				 if( Reply->ArgInc(29) ) CtrlRec->CtrlScheduleStatus = StrToInt(Reply->GetArg(29));
				 if( Reply->ArgInc(30) ) CtrlRec->LPRMode = StrToInt(Reply->GetArg(30));
				 if( Reply->ArgInc(31) ) CtrlRec->DecayDelay = StrToInt(Reply->GetArg(31));
				 if( Reply->ArgInc(32) ) CtrlRec->DecaySampInterval2 = StrToInt(Reply->GetArg(32));
				 if( Reply->ArgInc(33) ) CtrlRec->DecayDuration2 = StrToInt(Reply->GetArg(33));

				 if( Reply->ArgInc(34) ) CtrlRec->WatchDogEnable = CIISStrToBool( Reply->GetArg(34) );
				 if( Reply->ArgInc(35) ) CtrlRec->BusIntSerialNo = StrToInt(Reply->GetArg(35));
				 if( Reply->ArgInc(36) ) CtrlRec->BusIntVerMajor = StrToInt(Reply->GetArg(36));
				 if( Reply->ArgInc(37) ) CtrlRec->BusIntVerMinor = StrToInt(Reply->GetArg(37));
				 if( Reply->ArgInc(38) ) CtrlRec->MonitorExtCount = StrToInt(Reply->GetArg(38));

				 if( Reply->ArgInc(39) ) CtrlRec->ScheduleZRA = CIISStrToBool( Reply->GetArg(39) );
				 if( Reply->ArgInc(40) ) CtrlRec->ScheduleResMes = CIISStrToBool( Reply->GetArg(40) );

				 CtrlRec->PrjName = Reply->GetArg(50);

				 DB->SetCtrlRec( CtrlRec ); DB->ApplyUpdatesCtrl();

				 CtrlSyncState = CIISCtrlSyncReady;
				}
				else
				{
				CtrlTabStatusSet << CTSMissingRec;
				CtrlSyncState = CIISCtrlSyncReady;
				}
			} // CIIMsgOk
			else
			{
				CtrlTabStatusSet << CTSLocalError;
				CtrlSyncState = CIISCtrlSyncReady;
			}
			delete Reply;
		} // Reply
	  break;

	case CIISCtrlSyncReady:
		delete CtrlRec;
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CtrlTabSync Ready");
		#endif

		break;
  }
	return Ready;
}

void __fastcall TCIISServerInterface::InitZoneRec(CIISZoneRec *ZRec)
{
        // Note! It is not possible to use memset(0) or similar because of embedded String's
    ZRec->ZoneNo=0;
    ZRec->ZoneName="";
    ZRec->AnodeArea=0;
    ZRec->CathodeArea=0;
    ZRec->Comment="";
    ZRec->ZoneSampInterval=0;
    ZRec->RecType=0;
    ZRec->RecNo=0;
    ZRec->ZoneCanAdr=0;
    ZRec->ZoneAlarmStatus=0;
    ZRec->ZoneScheduleStatus=0;
    ZRec->BIChSerNo=0;
		ZRec->IncludeInSchedule=false;
		ZRec->RecTypeBeforeSchedule=0;
		ZRec->uCtrl=false;
		ZRec->PSOffOnAlarm=false;
		ZRec->PSOffHold=false;
    ZRec->CtrlName="";
}

void __fastcall TCIISServerInterface::ZoneTabSyncIni()
{
	ZoneSyncState = CIISZoneSyncStart;
	ZoneTabSyncCancel = false;
	ZRecUser = new CIISZoneRec;
	for( int32_t i = Zones->Count - 1; i >= 0; i-- )
	{
		ZRec = (CIISZoneRec*) Zones->Items[i];
		delete ZRec;
		Zones->Delete(i);
	}

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ZoneTabSyncIni ");
	#endif

}

bool __fastcall TCIISServerInterface::ZoneTabSync()
{
	TCamurPacket *Reply;
	String Data;
	bool Ready;

	Ready = false;

	if( ZoneTabSyncCancel )
  {
		ZoneSyncState = CIISZoneSyncReady;
	} // ZoneTabSyncCancel

  switch (ZoneSyncState)
  {
  case CIISZoneSyncStart:
		ZoneTabStatusSet.Clear();
		ZoneSyncState = CIISZoneSyncRequestFirst;
		break;

  case CIISZoneSyncRequestFirst:
		Data = "200=GetFirstToEnd\r\n";
		RequestNo = Request( CIISRead, CIISZones, Data );
		ZoneSyncState = CIISZoneSyncReadRec;
		break;

  case CIISZoneSyncRequest:
		Data = "1=" + IntToStr( ZRec->ZoneNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISZones, Data );
		ZoneSyncState = CIISZoneSyncReadRec;
		break;

  case CIISZoneSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					bool MoreRec, ZRecFound;

					// remove unused zones from user db
					if( DB->FindFirstZone() )
					{
						do
						{
							MoreRec = DB->GetZoneRec( ZRecUser, true );
							ZRecFound = false;
							for( int32_t i = 0; i < Zones->Count; i++ )
							{
								ZRecLogger = (CIISZoneRec*) Zones->Items[i];
								if( ZRecUser->ZoneNo == ZRecLogger->ZoneNo )
								{
									ZRecFound = true;
									break;
								}
							}
							if( !ZRecFound )
							{
								DB->LocateZoneRec( ZRecUser );
								DB->DeleteZoneRec( ZRecUser );
							}
						} while( MoreRec );
					}
					// update used zones and add new zones to user db
					for( int32_t i = 0; i < Zones->Count; i++ )
					{
						ZRec = (CIISZoneRec*) Zones->Items[i];
						if( DB->LocateZoneRec( ZRec ) ) DB->SetZoneRec( ZRec );
						else DB->AppendZoneRec( ZRec, false );
					}
					DB->ApplyUpdatesZone();

					ZoneSyncState = CIISZoneSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						ZRec = new CIISZoneRec;

						// Ver 3.16.0.3
						// Init record (in case logger server is old and not supplying all values)
						InitZoneRec(ZRec);

						ZRec->ZoneNo = StrToInt( Reply->GetDelArg(1) );
						ZRec->ZoneName = Reply->GetDelArg(2);
						ZRec->AnodeArea = CIISStrToFloat( Reply->GetDelArg(3) );
						ZRec->CathodeArea = CIISStrToFloat( Reply->GetDelArg(4) );
						ZRec->Comment = Reply->GetDelArg(5);
						ZRec->ZoneSampInterval = StrToInt(Reply->GetDelArg(6));
						ZRec->RecType = StrToInt(Reply->GetDelArg(7));
						ZRec->RecNo = StrToInt(Reply->GetDelArg(8));
						ZRec->ZoneCanAdr = StrToInt( Reply->GetDelArg(9) );
						ZRec->ZoneAlarmStatus = StrToInt(Reply->GetDelArg(10));
						if(Reply->ArgInc(11) ) ZRec->ZoneScheduleStatus = StrToInt(Reply->GetDelArg(11));
						if(Reply->ArgInc(12) ) ZRec->IncludeInSchedule = CIISStrToBool(Reply->GetDelArg(12));
						if(Reply->ArgInc(13) ) ZRec->RecTypeBeforeSchedule = StrToInt( Reply->GetDelArg(13) );
						if(Reply->ArgInc(14) ) ZRec->PSOffOnAlarm = CIISStrToBool(Reply->GetDelArg(14));
						if(Reply->ArgInc(15) ) ZRec->PSOffHold = CIISStrToBool(Reply->GetDelArg(15));
            if(Reply->ArgInc(16) ) ZRec->uCtrl = CIISStrToBool(Reply->GetDelArg(16));
						ZRec->CtrlName = Reply->GetDelArg(50);
						Zones->Add( ZRec );
					}
					ZoneSyncState = CIISZoneSyncRequest;
				}
			} // CIIMsgOk
			else
			{
				ZoneTabStatusSet << ZTSLocalError;
				ZoneSyncState = CIISZoneSyncReady;
			}
			delete Reply;
		} // Reply
		break;

  case CIISZoneSyncReady:
		delete ZRecUser;
		for( int32_t i = Zones->Count - 1; i >= 0; i-- )
		{
			ZRec = (CIISZoneRec*) Zones->Items[i];
			delete ZRec;
			Zones->Delete(i);
		}
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: ZoneTabSync Ready");
		#endif
		break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::SensorTabSyncIni()
{
	SensSyncState = CIISSensSyncStart;
	SensorTabSyncCancel = false;
	SRecUser = new CIISSensorRec;
  for( int32_t i = Sensors->Count - 1; i >= 0; i-- )
  {
		SRec = (CIISSensorRec*) Sensors->Items[i];
		delete SRec;
		Sensors->Delete(i);
	}

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: SensorTabSyncIni ");
	#endif
}

void __fastcall TCIISServerInterface::InitSensorRec(CIISSensorRec *SRec)
{
	// Note! It is not possible to use memset(0) or similar because of embedded String's
	SRec->SensorSerialNo=0;
	SRec->SensorCanAdr=0;
	SRec->SensorStatus=0;
	SRec->SensorName="";
	SRec->SensorType=0;
	SRec->SensorLastValue=0;
	SRec->PreCommOff=0;
	SRec->PreCommLPR=0;
	SRec->SensorArea=0;
	SRec->SensorGain=0;
	SRec->SensorOffset=0;
	SRec->SensorUnit="";
	SRec->SensorLow=0;
	SRec->SensorHigh=0;
	SRec->SensorAlarmEnabled=false;
	SRec->SensorAlarmStatus=0;
	SRec->SensorXPos=0;
	SRec->SensorYPos=0;
	SRec->SensorZPos=0;
	SRec->SensorSectionNo=0;
	SRec->RequestStatus=0;
	SRec->SensorIShunt=0;
	SRec->SensorLPRStep=0;
	SRec->SensorConnected=false;
	SRec->SensorWarmUp=0;
	SRec->SensorTemp=false;
	SRec->SensorLastTemp=0;
	SRec->SensorVerMajor=0;
	SRec->SensorVerMinor=0;
	SRec->SensorIShunt1=0;
	SRec->SensorIShunt2=0;
	SRec->SensorIShunt3=0;
	SRec->SensorLastValue2=0;
	SRec->SensorGain2=0;
	SRec->SensorOffset2=0;
	SRec->SensorUnit2="";
	SRec->SensorLow2=0;
	SRec->SensorHigh2=0;
	SRec->SensorAlarmStatus2=0;
	SRec->PowerShutdownEnabled=false;
	SRec->ZoneNo=0;
	SRec->ZoneCanAdr=0;
	SRec->Ch1BitVal=0;
	SRec->Ch2BitVal=0;
	SRec->Ch3BitVal=0;
	SRec->Ch4BitVal=0;
	SRec->Ch5BitVal=0;
	SRec->Ch6BitVal=0;
	SRec->BIChannel=0;
	SRec->SensorIShunt4=0;
	SRec->SensorIShunt5=0;
	SRec->SensorIShunt6=0;
	SRec->SensorIShunt7=0;
	SRec->SensorIShunt8=0;
	SRec->SensorIShunt9=0;
	SRec->SensorIShunt10=0;
	SRec->SensorIShunt11=0;
	SRec->SensorChannelCount=0;
	SRec->SensorName2="";
	SRec->SensorName3="";
	SRec->SensorName4="";
	SRec->SensorGain3=0;
	SRec->SensorOffset3=0;
	SRec->SensorUnit3="";
	SRec->SensorLow3=0;
	SRec->SensorHigh3=0;
	SRec->SensorLastValue3=0;
	SRec->SensorAlarmStatus3=0;
	SRec->SensorGain4=0;
	SRec->SensorOffset4=0;
	SRec->SensorUnit4="";
	SRec->SensorLow4=0;
	SRec->SensorHigh4=0;
	SRec->SensorLastValue4=0;
	SRec->SensorAlarmStatus4=0;
	SRec->SensorLPRIRange=2;
	SRec->VerNotSupported=false;
	SRec->DisabledChannels=0;
	SRec->PreCommOff2=0;
	SRec->PreCommOff3=0;
	SRec->PreCommOff4=0;
	SRec->CANAlarmEnabled = true;
	SRec->CANAlarmStatus = 0;
}

bool __fastcall TCIISServerInterface::SensorTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

  Ready = false;

	if( SensorTabSyncCancel  )
	{
		SensSyncState = CIISSensSyncReady;
	} // SensorTabSyncCancel


  switch (SensSyncState)
  {
  case CIISSensSyncStart:
	SensTabStatusSet.Clear();
	SensSyncState = CIISSensSyncRequestFirst;
	break;

  case CIISSensSyncRequestFirst:
  {
	Data = "200=GetFirstToEnd\r\n";
	RequestNo = Request( CIISRead, CIISSensors, Data );
	SensSyncState = CIISSensSyncReadRec;
  };
  break;

  case CIISSensSyncRequest:
	Data = "1=" + IntToStr( SRec->SensorSerialNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
	RequestNo = Request( CIISRead, CIISSensors, Data );
	SensSyncState = CIISSensSyncReadRec;
	break;

  case CIISSensSyncReadRec:
	Reply = ClientSocket->GetReply( RequestNo );
	if( Reply != NULL )
	{
	  if( Reply->MessageCode == CIISMsg_Ok )
	  {
		if( Reply->MessageData == "" )
		{
		  bool MoreRec, SRecFound;

		  // remove unused Sensors from user db
		  if( DB->FindFirstSensor() )
		  {
			do
			{
			  MoreRec = DB->GetSensorRec( SRecUser, true );
			  SRecFound = false;
			  for( int32_t i = 0; i < Sensors->Count; i++ )
			  {
				SRecLogger = (CIISSensorRec*) Sensors->Items[i];
				if( SRecUser->SensorSerialNo == SRecLogger->SensorSerialNo )
				{
				  SRecFound = true;
				  break;
				}
			  }
			  if( !SRecFound )
			  {
				DB->LocateSensorRec( SRecUser );
				DB->DeleteSensorRec( SRecUser );
			  }
			} while( MoreRec );
		  }
		  // update used Sensors and add new Sensors to user db
		  for( int32_t i = 0; i < Sensors->Count; i++ )
		  {
			SRec = (CIISSensorRec*) Sensors->Items[i];
			if( DB->LocateSensorRec( SRec ) ) DB->SetSensorRec( SRec );
			else DB->AppendSensorRec( SRec );
		  }
		  DB->ApplyUpdatesSensor();

		  SensSyncState = CIISSensSyncReady;
		}
		else
		{
			while( Reply->ArgInc( 1 ) )
		  {
			SRec = new CIISSensorRec;

                    // Ver 3.16.0.3
                    // Init record (in case logger server is old and not supplying all values)
			InitSensorRec(SRec);

			SRec->SensorSerialNo = StrToInt(Reply->GetDelArg(1));
			Data = Reply->GetDelArg(2);
			if(Data != "" ) SRec->SensorCanAdr = StrToInt( Data );
			SRec->SensorStatus = StrToInt(Reply->GetDelArg(3));
			SRec->SensorName = Reply->GetDelArg(4);
			SRec->SensorType = StrToInt(Reply->GetDelArg(5));
			Data = Reply->GetDelArg(6);
			if(Data != "" ) SRec->SensorLastValue = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(7);
			if(Data != "" ) SRec->PreCommOff = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(8);
			if(Data != "" ) SRec->PreCommLPR = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(9);
			if(Data != "" ) SRec->SensorArea = CIISStrToFloat( Data );
			SRec->SensorGain = CIISStrToFloat(Reply->GetDelArg(10));
			SRec->SensorOffset = CIISStrToFloat(Reply->GetDelArg(11));
			SRec->SensorUnit = Reply->GetDelArg(12);
			Data = Reply->GetDelArg(13);
			if(Data != "" ) SRec->SensorLow = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(14);
			if(Data != "" ) SRec->SensorHigh = CIISStrToFloat( Data );
			SRec->SensorAlarmEnabled = CIISStrToBool( Reply->GetDelArg(15) );
			SRec->SensorAlarmStatus = StrToInt(Reply->GetDelArg(16));
			Data = Reply->GetDelArg(17);
			if(Data != "" ) SRec->SensorXPos = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(18);
			if(Data != "" ) SRec->SensorYPos = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(19);
			if(Data != "" ) SRec->SensorZPos = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(20);
			if(Data != "" ) SRec->SensorSectionNo = StrToInt( Data );
			SRec->RequestStatus = StrToInt(Reply->GetDelArg(21));
			Data = Reply->GetDelArg(22);
			if(Data != "" ) SRec->SensorIShunt = CIISStrToFloat( Data );
			SRec->SensorLPRStep = StrToInt(Reply->GetDelArg(23));
			SRec->SensorConnected = CIISStrToBool( Reply->GetDelArg(24) );
			Data = Reply->GetDelArg(25);
			if(Data != "" ) SRec->SensorWarmUp = StrToInt( Data );
			Data = Reply->GetDelArg(26);
			if(Data != "" ) SRec->SensorTemp = CIISStrToBool( Data );
			Data = Reply->GetDelArg(27);
			if(Data != "" ) SRec->SensorLastTemp = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(28);
			if(Data != "" ) SRec->SensorVerMajor = StrToInt( Data );
			Data = Reply->GetDelArg(29);
			if(Data != "" ) SRec->SensorVerMinor = StrToInt( Data );
			Data = Reply->GetDelArg(30);
			if(Data != "" ) SRec->SensorIShunt1 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(31);
			if(Data != "" ) SRec->SensorIShunt2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(32);
			if(Data != "" ) SRec->SensorIShunt3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(33);
			if(Data != "" ) SRec->SensorLastValue2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(34);
			if(Data != "" ) SRec->SensorGain2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(35);
			if(Data != "" ) SRec->SensorOffset2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(36);
			if(Data != "" ) SRec->SensorUnit2 = Data;
			Data = Reply->GetDelArg(37);
			if(Data != "" ) SRec->SensorLow2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(38);
			if(Data != "" ) SRec->SensorHigh2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(39);
			if(Data != "" ) SRec->SensorAlarmStatus2 = StrToInt( Data );
			Data = Reply->GetDelArg(40);
			if(Data != "" ) SRec->PowerShutdownEnabled = CIISStrToBool( Data );
			else SRec->PowerShutdownEnabled = false;
			Data = Reply->GetDelArg(41);
			if(Data != "" ) SRec->SensorChannelCount = StrToInt( Data );
			SRec->ZoneNo = StrToInt(Reply->GetDelArg(50));

			Data = Reply->GetDelArg(51);
			if(Data != "" ) SRec->SensorName2 = Data;
			Data = Reply->GetDelArg(52);
			if(Data != "" ) SRec->SensorName3 = Data;
			Data = Reply->GetDelArg(53);
			if(Data != "" ) SRec->SensorName4 = Data;

			Data = Reply->GetDelArg(54);
			if(Data != "" ) SRec->SensorGain3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(55);
			if(Data != "" ) SRec->SensorOffset3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(56);
			if(Data != "" ) SRec->SensorUnit3 = Data;
			Data = Reply->GetDelArg(57);
			if(Data != "" ) SRec->SensorLow3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(58);
			if(Data != "" ) SRec->SensorHigh3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(59);
			if(Data != "" ) SRec->SensorAlarmStatus3 = StrToInt( Data );

			Data = Reply->GetDelArg(60);
			if(Data != "" ) SRec->SensorGain4 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(61);
			if(Data != "" ) SRec->SensorOffset4 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(62);
			if(Data != "" ) SRec->SensorUnit4 = Data;
			Data = Reply->GetDelArg(63);
			if(Data != "" ) SRec->SensorLow4 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(64);
			if(Data != "" ) SRec->SensorHigh4 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(65);
			if(Data != "" ) SRec->SensorAlarmStatus4 = StrToInt( Data );
			Data = Reply->GetDelArg(66);
			if(Data != "" ) SRec->SensorLPRIRange = StrToInt( Data );
			Data = Reply->GetDelArg(67);
			if(Data != "" ) SRec->VerNotSupported = CIISStrToBool( Data );
			Data = Reply->GetDelArg(68);
			if(Data != "" ) SRec->DisabledChannels = StrToInt( Data );

			Data = Reply->GetDelArg(69);
			if(Data != "" ) SRec->PreCommOff2 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(70);
			if(Data != "" ) SRec->PreCommOff3 = CIISStrToFloat( Data );
			Data = Reply->GetDelArg(71);
			if(Data != "" ) SRec->PreCommOff4 = CIISStrToFloat( Data );

			Data = Reply->GetDelArg(72);
			if(Data != "" ) SRec->CANAlarmEnabled = CIISStrToBool( Data );
			Data = Reply->GetDelArg(73);
			if(Data != "" ) SRec->CANAlarmStatus = StrToInt( Data );



			Sensors->Add( SRec );
			}
		  SensSyncState = CIISSensSyncRequest;
		}
	  } // CIIMsgOk
	  else
	  {
		  SensTabStatusSet << STSLocalError;
		  SensSyncState = CIISSensSyncReady;
	  }
	  delete Reply;
	} // Reply
	break;

  case CIISSensSyncReady:
		delete SRecUser;
		for( int32_t i = Sensors->Count - 1; i >= 0; i-- )
		{
			SRec = (CIISSensorRec*) Sensors->Items[i];
			delete SRec;
			Sensors->Delete(i);
		}
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: SensorTabSync Ready");
		#endif

		break;
	}
  return Ready;
}

void __fastcall TCIISServerInterface::InitAlarmRec(CIISAlarmRec *AlarmRec)
{
	AlarmRec->InvDO1 = false;
	AlarmRec->InvDO2 = false;
	AlarmRec->InvDO3 = false;
	AlarmRec->InvDO4 = false;
	AlarmRec->VerNotSupported = false;
	AlarmRec->CANAlarmEnabled = true;
	AlarmRec->CANAlarmStatus = 0;

}

void __fastcall TCIISServerInterface::AlarmTabSyncIni()
{
	AlarmSyncState = CIISAlarmSyncStart;
	AlarmTabSyncCancel = false;
	AlarmRecUser = new CIISAlarmRec;
	for( int32_t i = Alarms->Count - 1; i >= 0; i-- )
	{
		AlarmRec = (CIISAlarmRec*) Alarms->Items[i];
		delete AlarmRec;
		Alarms->Delete(i);
	}
}

bool __fastcall TCIISServerInterface::AlarmTabSync()
{
	TCamurPacket *Reply;
	String Data;
	bool Ready;

	Ready = false;

	if( AlarmTabSyncCancel )
  {
		AlarmSyncState = CIISAlarmSyncReady;
	}


  switch (AlarmSyncState)
	{
  case CIISAlarmSyncStart:
		AlarmTabStatusSet.Clear();
		AlarmSyncState = CIISAlarmSyncRequestFirst;
		break;

	case CIISAlarmSyncRequestFirst:
		Data = "200=GetFirstToEnd\r\n";
		RequestNo = Request( CIISRead, CIISAlarm, Data );
		AlarmSyncState = CIISAlarmSyncReadRec;
		break;

  case CIISAlarmSyncRequest:
		Data = "1=" + IntToStr( AlarmRec->AlarmSerialNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISAlarm, Data );
		AlarmSyncState = CIISAlarmSyncReadRec;
		break;

  case CIISAlarmSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					bool MoreRec, AlarmRecFound;

					// remove unused Alarms from user db
					if( DB->FindFirstAlarm() )
					{
						do
						{
							MoreRec = DB->GetAlarmRec( AlarmRecUser, true );
							AlarmRecFound = false;
							for( int32_t i = 0; i < Alarms->Count; i++ )
							{
								AlarmRecLogger = (CIISAlarmRec*) Alarms->Items[i];
								if( AlarmRecUser->AlarmSerialNo == AlarmRecLogger->AlarmSerialNo )
								{
									AlarmRecFound = true;
									break;
								}
							}
							if( !AlarmRecFound )
							{
								DB->LocateAlarmRec( AlarmRecUser );
								DB->DeleteAlarmRec( AlarmRecUser );
							}
						} while( MoreRec );
					}
					// update used Alarms and add new Alarms to user db
					for( int32_t i = 0; i < Alarms->Count; i++ )
					{
						AlarmRec = (CIISAlarmRec*) Alarms->Items[i];
						if( DB->LocateAlarmRec( AlarmRec ) ) DB->SetAlarmRec( AlarmRec );
						else DB->AppendAlarmRec( AlarmRec );
					}
					DB->ApplyUpdatesAlarm();

					AlarmSyncState = CIISAlarmSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						AlarmRec = new CIISAlarmRec;
						InitAlarmRec(AlarmRec);

						AlarmRec->AlarmSerialNo = StrToInt(Reply->GetDelArg(1));
						AlarmRec->AlarmCanAdr = StrToInt( Reply->GetDelArg(2) );
						AlarmRec->AlarmStatus = StrToInt( Reply->GetDelArg(3) );
						AlarmRec->AlarmName = Reply->GetDelArg(4);
						AlarmRec->AlarmType = StrToInt( Reply->GetDelArg(5) );
						AlarmRec->RequestStatus = StrToInt( Reply->GetDelArg(6) );
						AlarmRec->AlarmConnected = CIISStrToBool( Reply->GetDelArg(7) );
						AlarmRec->AlarmVerMajor = StrToInt( Reply->GetDelArg(8) );
						AlarmRec->AlarmVerMinor = StrToInt( Reply->GetDelArg(9) );

						AlarmRec->DO1 = CIISStrToBool( Reply->GetDelArg(10) );
						AlarmRec->DO2 = CIISStrToBool( Reply->GetDelArg(11) );
						AlarmRec->DO3 = CIISStrToBool( Reply->GetDelArg(12) );
						AlarmRec->DO4 = CIISStrToBool( Reply->GetDelArg(13) );
						AlarmRec->DO1AlarmType = StrToInt( Reply->GetDelArg(14) );
						AlarmRec->DO2AlarmType = StrToInt( Reply->GetDelArg(15) );
						AlarmRec->DO3AlarmType = StrToInt( Reply->GetDelArg(16) );
						AlarmRec->DO4AlarmType = StrToInt( Reply->GetDelArg(17) );
						if( Reply->ArgInc(18) ) AlarmRec->InvDO1 = CIISStrToBool( Reply->GetDelArg(18) );
						if( Reply->ArgInc(19) ) AlarmRec->InvDO2 = CIISStrToBool( Reply->GetDelArg(19) );
						if( Reply->ArgInc(20) ) AlarmRec->InvDO3 = CIISStrToBool( Reply->GetDelArg(20) );
						if( Reply->ArgInc(21) ) AlarmRec->InvDO4 = CIISStrToBool( Reply->GetDelArg(21) );
						if( Reply->ArgInc(22) ) AlarmRec->VerNotSupported = CIISStrToBool( Reply->GetDelArg(22) );
						if( Reply->ArgInc(23) ) AlarmRec->CANAlarmEnabled = CIISStrToBool( Reply->GetDelArg(23) );
						if( Reply->ArgInc(24) )AlarmRec->CANAlarmStatus = StrToInt( Reply->GetDelArg(24) );

						AlarmRec->ZoneNo = StrToInt( Reply->GetDelArg(50) );
						Alarms->Add( AlarmRec );
					}
					AlarmSyncState = CIISAlarmSyncRequest;
				}
			} // CIIMsgOk
			else
			{
				AlarmTabStatusSet << AlarmTSLocalError;
				AlarmSyncState = CIISAlarmSyncReady;
			}
			delete Reply;
		} // Reply
		break;

  case CIISAlarmSyncReady:
		delete AlarmRecUser;
		for( int32_t i = Alarms->Count - 1; i >= 0; i-- )
		{
			AlarmRec = (CIISAlarmRec*) Alarms->Items[i];
			delete AlarmRec;
			Alarms->Delete(i);
		}
		Ready = true;
		break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::InitPowerSupplyRec(CIISPowerSupplyRec *PSRec)
{
        // Note! It is not possible to use memset(0) or similar because of embedded String's

  PSRec->PSSerialNo=0;
  PSRec->PSCanAdr=0;
  PSRec->PSStatus=0;
  PSRec->PSName="";
  PSRec->PSSerialID="";
  PSRec->PSType=0;
  PSRec->PSLastValueU=0;
  PSRec->PSLastValueI=0;
  PSRec->PSLastValueCh3=0;
  PSRec->PSLastValueCh4=0;
  PSRec->PSMode=0;
  PSRec->PSSetVoltage=0;
  PSRec->PSSetCurrent=0;
  PSRec->PSOutGain=0;
  PSRec->PSOutOffset=0;
  PSRec->PSLowU=0;
  PSRec->PSLowI=0;
  PSRec->PSHighU=0;
  PSRec->PSHighI=0;
  PSRec->PSAlarmEnabled=false;
  PSRec->PSAlarmStatusU=0;
  PSRec->PSAlarmStatusI=0;
  PSRec->PSVOutEnabled=false;
  PSRec->PSRemote=false;
  PSRec->RequestStatus=0;
  PSRec->PSConnected=false;
  PSRec->Ch1Gain=0;
  PSRec->Ch1Offset=0;
  PSRec->Ch1Unit="";
  PSRec->Ch2Gain=0;
  PSRec->Ch2Offset=0;
  PSRec->Ch2Unit="";
  PSRec->Ch3Gain=0;
  PSRec->Ch3Offset=0;
  PSRec->Ch3Unit="";
  PSRec->Ch4Gain=0;
  PSRec->Ch4Offset=0;
  PSRec->Ch4Unit="";
  PSRec->Ch1BitVal=0;
  PSRec->Ch2BitVal=0;
  PSRec->Ch3BitVal=0;
  PSRec->Ch4BitVal=0;
  PSRec->PSVerMajor=0;
  PSRec->PSVerMinor=0;
  PSRec->SenseGuardEnabled=false;
	PSRec->Fallback=false;
	PSRec->PSTemp=false;
	PSRec->VerNotSupported=false;
  PSRec->ZoneNo=0;
  PSRec->ZoneCanAdr=0;
	PSRec->BIChannel=0;
	PSRec->DisabledChannels=0;
	PSRec->InvDO1 = false;
	PSRec->InvDO2 = false;
	PSRec->CANAlarmEnabled = true;
	PSRec->CANAlarmStatus = 0;

}

void __fastcall TCIISServerInterface::PSTabSyncIni()
{
	PSSyncState = CIISPSSyncStart;
	PSTabSyncCancel = false;
	PSRecUser = new CIISPowerSupplyRec;
	for( int32_t i = PSs->Count - 1; i >= 0; i-- )
	{
		PSRec = (CIISPowerSupplyRec*) PSs->Items[i];
		delete PSRec;
		PSs->Delete(i);
	}

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: PSTabSyncIni ");
	#endif
}

bool __fastcall TCIISServerInterface::PSTabSync()
{
	TCamurPacket *Reply;
	String Data;
	bool Ready;

	Ready = false;
	if( PSTabSyncCancel )
	{
		PSSyncState = CIISPSSyncReady;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: if( PSTabSyncCancel ) ");
		#endif
	} // PSTabSyncCancel

  switch (PSSyncState)
  {
  case CIISPSSyncStart:
		PSTabStatusSet.Clear();
		PSSyncState = CIISPSSyncRequestFirst;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISPSSyncStart");
		#endif
		break;

  case CIISPSSyncRequestFirst:
		Data = "200=GetFirstToEnd\r\n";
		RequestNo = Request( CIISRead, CIISPowerSupply, Data );
		PSSyncState = CIISPSSyncReadRec;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISPSSyncRequestFirst");
		#endif
		break;

  case CIISPSSyncRequest:
		Data = "1=" + IntToStr( PSRec->PSSerialNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISPowerSupply, Data );
		PSSyncState = CIISPSSyncReadRec;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISPSSyncRequest");
		#endif
		break;

	case CIISPSSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: CIISPSSyncReadRec/if( Reply != NULL )");
			#endif
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				#if DebugNetIntRemote == 1
				Debug( "Ser Int: CIISPSSyncReadRec/if( Reply->MessageCode == CIISMsg_Ok )");
				#endif
				if( Reply->MessageData == "" )
				{
					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISPSSyncReadRec/if( Reply->MessageData == "" ) )");
					#endif

					bool MoreRec, PSRecFound;

					// remove unused PSs from user db
					MoreRec = false;
					if( DB->FindFirstPS() )
					{
						do
						{
							MoreRec = DB->GetPSRec( PSRecUser, true );
							PSRecFound = false;
							for( int32_t i = 0; i < PSs->Count; i++ )
							{
							PSRecLogger = (CIISPowerSupplyRec*) PSs->Items[i];
							if( PSRecUser->PSSerialNo == PSRecLogger->PSSerialNo )
							{
								PSRecFound = true;
								break;
							}
							}
							if( !PSRecFound )
							{
							DB->LocatePSRec( PSRecUser );
							DB->DeletePSRec( PSRecUser );
							}
						} while( MoreRec );
					}
					// update used PSs and add new PSs to user db
					for( int32_t i = 0; i < PSs->Count; i++ )
					{
						PSRec = (CIISPowerSupplyRec*) PSs->Items[i];
						if( DB->LocatePSRec( PSRec ) ) DB->SetPSRec( PSRec );
						else DB->AppendPSRec( PSRec );
					}
					DB->ApplyUpdatesPS();
					PSSyncState = CIISPSSyncReady;
				}
				else
				{
					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISPSSyncReadRec/if( Reply->MessageData != "" ) )");
					#endif
					while( Reply->ArgInc( 1 ) )
					{

						PSRec = new CIISPowerSupplyRec;

						// Ver 3.16.0.3
						// Init record (in case logger server is old and not supplying all values)
						InitPowerSupplyRec(PSRec);

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/new rec");
						#endif

						PSRec->PSSerialNo = StrToInt( Reply->GetDelArg(1) );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg1"  +  IntToStr( PSRec->PSSerialNo ));
						#endif

						Data = Reply->GetDelArg(2);
						if(Data != "" ) PSRec->PSCanAdr = StrToInt( Data );
						PSRec->PSStatus = StrToInt( Reply->GetDelArg(3));
						PSRec->PSName = Reply->GetDelArg(4);
						PSRec->PSSerialID = Reply->GetDelArg(5);
						PSRec->PSType = StrToInt( Reply->GetDelArg(6) );
						Data = Reply->GetDelArg(7);
						if(Data != "" ) PSRec->PSLastValueU = CIISStrToFloat( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg7"  +  Data );
						#endif

						Data = Reply->GetDelArg(8);
						if(Data != "" ) PSRec->PSLastValueI = CIISStrToFloat( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg8"  +  Data );
						#endif

						PSRec->PSMode = StrToInt( Reply->GetDelArg(9) );
						PSRec->PSSetVoltage = CIISStrToFloat( Reply->GetDelArg(10) );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg10"  +  Data );
						#endif

						PSRec->PSSetCurrent = CIISStrToFloat( Reply->GetDelArg(11) );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg11"  +  Data );
						#endif

						PSRec->PSOutGain = CIISStrToFloat( Reply->GetDelArg(12) );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg12"  +  Data );
						#endif

						PSRec->PSOutOffset = CIISStrToFloat( Reply->GetDelArg(13) );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg13"  +  Data );
						#endif

						Data = Reply->GetDelArg(14);
						if(Data != "" ) PSRec->PSLowU = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(15);
						if(Data != "" ) PSRec->PSLowI = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(16);
						if(Data != "" ) PSRec->PSHighU = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(17);
						if(Data != "" ) PSRec->PSHighI = CIISStrToFloat( Data );
						PSRec->PSAlarmEnabled = CIISStrToBool( Reply->GetDelArg(18) );
						PSRec->PSAlarmStatusU = StrToInt( Reply->GetDelArg(19) );
						PSRec->PSAlarmStatusI = StrToInt( Reply->GetDelArg(20) );
						PSRec->PSVOutEnabled = CIISStrToBool( Reply->GetDelArg(21) );
						PSRec->PSRemote = CIISStrToBool( Reply->GetDelArg(22) );
						PSRec->RequestStatus = StrToInt( Reply->GetDelArg(23) );
						PSRec->PSConnected = CIISStrToBool( Reply->GetDelArg(24) );
						Data = Reply->GetDelArg(25);
						if(Data != "" ) PSRec->Ch1Gain = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(26);
						if(Data != "" ) PSRec->Ch1Offset = CIISStrToFloat( Data );
						PSRec->Ch1Unit = Reply->GetDelArg(27);
						Data = Reply->GetDelArg(28);
						if(Data != "" ) PSRec->Ch2Gain = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(29);
						if(Data != "" ) PSRec->Ch2Offset = CIISStrToFloat( Data );
						PSRec->Ch2Unit = Reply->GetDelArg(30);
						Data = Reply->GetDelArg(31);
						if(Data != "" ) PSRec->Ch3Gain = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(32);
						if(Data != "" ) PSRec->Ch3Offset = CIISStrToFloat( Data );
						PSRec->Ch3Unit = Reply->GetDelArg(33);
						Data = Reply->GetDelArg(34);
						if(Data != "" ) PSRec->Ch4Gain = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(35);
						if(Data != "" ) PSRec->Ch4Offset = CIISStrToFloat( Data );
						PSRec->Ch4Unit = Reply->GetDelArg(36);
						Data = Reply->GetDelArg(37);
						if(Data != "" ) PSRec->Ch1BitVal = CIISStrToFloat( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg37"  +  Data );
						#endif

						Data = Reply->GetDelArg(38);
						if(Data != "" ) PSRec->Ch2BitVal = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(39);
						if(Data != "" ) PSRec->Ch3BitVal = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(40);
						if(Data != "" ) PSRec->Ch4BitVal = CIISStrToFloat( Data );
						Data = Reply->GetDelArg(41);
						if(Data != "" ) PSRec->PSLastValueCh3 = CIISStrToFloat( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg41"  +  Data );
						#endif

						Data = Reply->GetDelArg(42);
						if(Data != "" ) PSRec->PSLastValueCh4 = CIISStrToFloat( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg42"  +  Data );
						#endif

						Data = Reply->GetDelArg(43);
						if(Data != "" ) PSRec->PSVerMajor = StrToInt( Data );
						Data = Reply->GetDelArg(44);
						if(Data != "" ) PSRec->PSVerMinor = StrToInt( Data );
						Data = Reply->GetDelArg(45);
						if(Data != "" ) PSRec->SenseGuardEnabled = CIISStrToBool( Data );
						else PSRec->SenseGuardEnabled = false;
						Data = Reply->GetDelArg(46);
						if(Data != "" ) PSRec->Fallback = CIISStrToBool( Data );
						else PSRec->Fallback = false;
						Data = Reply->GetDelArg(47);
						if(Data != "" ) PSRec->PSTemp = CIISStrToBool( Data );
						else PSRec->PSTemp = false;
						Data = Reply->GetDelArg(48);
						if(Data != "" ) PSRec->VerNotSupported = CIISStrToBool( Data );
						Data = Reply->GetDelArg(49);
						if(Data != "" ) PSRec->DisabledChannels = StrToInt( Data );

						PSRec->ZoneNo = StrToInt( Reply->GetDelArg(50) );

						Data = Reply->GetDelArg(51);
						if(Data != "" ) PSRec->InvDO1 = CIISStrToBool( Data );
						else PSRec->InvDO1 = false;
						Data = Reply->GetDelArg(52);
						if(Data != "" ) PSRec->InvDO2 = CIISStrToBool( Data );
						else PSRec->InvDO2 = false;

						Data = Reply->GetDelArg(53);
						if(Data != "" ) PSRec->CANAlarmEnabled = CIISStrToBool( Data );
						else PSRec->CANAlarmEnabled = true;
						Data = Reply->GetDelArg(54);
						if(Data != "" ) PSRec->CANAlarmStatus = StrToInt( Data );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: CIISPSSyncReadRec/GetDelArg50"  +  StrToInt( PSRec->ZoneNo ) );
						#endif

						PSs->Add( PSRec );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: Ready Reading PS snr: " + IntToStr( PSRec->PSSerialNo ));
						#endif

					}
					PSSyncState = CIISPSSyncRequest;
				}
			} // CIIMsgOk
			else
			{
				PSTabStatusSet << PSTSLocalError;
				PSSyncState = CIISPSSyncReady;

				#if DebugNetIntRemote == 1
				Debug( "Ser Int: CIISPSSyncReadRec/Reply->MessageCode != CIISMsg_Ok");
				#endif
			}
			delete Reply;
		} // Reply

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISPSSyncReadRec");
		#endif

		break;

  case CIISPSSyncReady:
		delete PSRecUser;
		for( int32_t i = PSs->Count - 1; i >= 0; i-- )
		{
			PSRec = (CIISPowerSupplyRec*) PSs->Items[i];
			delete PSRec;
			PSs->Delete(i);
		}
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: PSTabSync Ready");
		#endif

		break;
	}
		return Ready;
}

void __fastcall TCIISServerInterface::InitWLinkRec(CIISWLinkRec *WLinkRec)
{
	WLinkRec->VerNotSupported = false;
}

void __fastcall TCIISServerInterface::WLinkTabSyncIni()
{
	WLinkSyncState = CIISWLinkSyncStart;
	WLinkTabSyncCancel = false;
	WRecUser = new CIISWLinkRec;
	for( int32_t i = WLinks->Count - 1; i >= 0; i-- )
	{
		WRec = (CIISWLinkRec*) WLinks->Items[i];
		delete WRec;
		WLinks->Delete(i);
	}

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: WLinkTabSyncIni ");
	#endif
}

bool __fastcall TCIISServerInterface::WLinkTabSync()
{
	TCamurPacket *Reply;
	String Data;
	bool Ready;

	Ready = false;

	if( WLinkTabSyncCancel )
	{
		WLinkSyncState = CIISWLinkSyncReady;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: if( WLinkTabSyncCancel ) ");
		#endif
	}


  switch (WLinkSyncState)
  {
	case CIISWLinkSyncStart:
		WLinkTabStatusSet.Clear();
		WLinkSyncState = CIISWLinkSyncRequestFirst;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISWLinkSyncStart");
		#endif
		break;

	case CIISWLinkSyncRequestFirst:
		Data = "200=GetFirstToEnd\r\n";
		RequestNo = Request( CIISRead, CIISWLink, Data );
		WLinkSyncState = CIISWLinkSyncReadRec;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISWLinkSyncRequestFirst");
		#endif
		break;

	case CIISWLinkSyncRequest:
		Data = "1=" + IntToStr( WRec->WLinkSerialNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISWLink, Data );
		WLinkSyncState = CIISWLinkSyncReadRec;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISWLinkSyncRequest");
		#endif
		break;

	case CIISWLinkSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: CIISWLinkSyncReadRec/if( Reply != NULL )");
			#endif

			if( Reply->MessageCode == CIISMsg_Ok )
			{
				#if DebugNetIntRemote == 1
				Debug( "Ser Int: CIISWLinkSyncReadRec/if( Reply->MessageCode == CIISMsg_Ok )");
				#endif

				if( Reply->MessageData == "" )
				{
					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/if( Reply->MessageData == "" ) )");
					#endif

					bool MoreRec, WRecFound;

					// remove unused WLinks from user db
					if( DB->FindFirstWLink() )
					{
						do
						{
							MoreRec = DB->GetWLinkRec( WRecUser, true );
							WRecFound = false;
							for( int32_t i = 0; i < WLinks->Count; i++ )
							{
								WRecLogger = (CIISWLinkRec*) WLinks->Items[i];
								if( WRecUser->WLinkSerialNo == WRecLogger->WLinkSerialNo )
								{
									WRecFound = true;
									break;
								}
							}
							if( !WRecFound )
							{
								DB->LocateWLinkRec( WRecUser );
								DB->DeleteWLinkRec( WRecUser );
							}
						} while( MoreRec );
					}
					// update used WLinks and add new WLinks to user db
					for( int32_t i = 0; i < WLinks->Count; i++ )
					{
						WRec = (CIISWLinkRec*) WLinks->Items[i];
						if( DB->LocateWLinkRec( WRec ) ) DB->SetWLinkRec( WRec );
						else DB->AppendWLinkRec( WRec );
					}
					DB->ApplyUpdatesWLink();

					WLinkSyncState = CIISWLinkSyncReady;
				}
				else
				{
					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/if( Reply->MessageData != "" ) )");
					#endif

					while( Reply->ArgInc( 1 ) )
					{
						WRec = new CIISWLinkRec;
						InitWLinkRec( WRec );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/new rec");
					#endif

						WRec->WLinkSerialNo = StrToInt(Reply->GetDelArg(1));


					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg1"  +  IntToStr( WRec->WLinkSerialNo ));
					#endif

						Data = Reply->GetDelArg(2);
						if(Data != "" ) WRec->WLinkCanAdr = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg2"  +  Data );
					#endif

						Data = Reply->GetDelArg(3);
						if(Data != "" ) WRec->WLinkStatus = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg3"  +  Data );
					#endif

						WRec->WLinkName = Reply->GetDelArg(4);

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg4"  +   WRec->WLinkName );
					#endif

						Data = Reply->GetDelArg(5);
						if(Data != "" ) WRec->WLinkType = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg5"  +  Data );
					#endif

						Data = Reply->GetDelArg(6);
						if(Data != "" ) WRec->WLinkRequestStatus = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg6"  +  Data );
					#endif


						WRec->WLinkConnected = CIISStrToBool( Reply->GetDelArg(7) );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg7"  +  WRec->WLinkConnected );
					#endif

						Data = Reply->GetDelArg(8);
						if(Data != "" ) WRec->WLinkVerMajor = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg8"  +  Data );
					#endif

						Data = Reply->GetDelArg(9);
						if(Data != "" ) WRec->WLinkVerMinor = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg9"  +  Data );
					#endif

						Data = Reply->GetDelArg(10);
						if( Data !="" ) WRec->WLinkDH = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg10"  +  Data );
					#endif

						Data = Reply->GetDelArg(11);
						if( Data !="" ) WRec->WLinkDL = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg11"  +  Data );
					#endif

						Data = Reply->GetDelArg(12);
						if( Data !="" ) WRec->WLinkMY = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg12"  +  Data );
					#endif

						Data = Reply->GetDelArg(13);
						if( Data !="" ) WRec->WLinkCH = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg13"  +  Data );
					#endif

						Data = Reply->GetDelArg(14);
						if( Data !="" ) WRec->WLinkID = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg14"  +  Data );
					#endif

						Data = Reply->GetDelArg(15);
						if( Data !="" ) WRec->WLinkPL = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg15"  +  Data );
					#endif

						Data = Reply->GetDelArg(16);
						if( Data !="" ) WRec->WLinkSignal = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg16"  +  Data );
					#endif

						Data = Reply->GetDelArg(17);
						if( Data !="" ) WRec->WLinkMode = StrToInt( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg17"  +  Data );
					#endif

						Data = Reply->GetDelArg(18);
						if( Data !="" ) WRec->VerNotSupported = CIISStrToBool( Data );

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg18"  +  Data );
					#endif


						WRec->CtrlName = Reply->GetDelArg(50);

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/GetDelArg50"  +  WRec->CtrlName );
					#endif

						WLinks->Add( WRec );

						#if DebugNetIntRemote == 1
						Debug( "Ser Int: Ready Reading WLink snr: " + IntToStr( WRec->WLinkSerialNo ));
						#endif

					}
					WLinkSyncState = CIISWLinkSyncRequest;

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISWLinkSyncReadRec/while( Reply->ArgInc( 1 ) ) ready)");
					#endif
				}
			} // CIIMsgOk
			else
			{
				WLinkTabStatusSet << WLinkTSLocalError;
				WLinkSyncState = CIISWLinkSyncReady;

				#if DebugNetIntRemote == 1
				Debug( "Ser Int: CIISWLinkSyncReadRec/Reply->MessageCode != CIISMsg_Ok");
				#endif
			}
			delete Reply;
		} // Reply

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISWLinkSyncReadRec");
		#endif
		break;

	case CIISWLinkSyncReady:
		delete WRecUser;
		for( int32_t i = WLinks->Count - 1; i >= 0; i-- )
		{
			WRec = (CIISWLinkRec*) WLinks->Items[i];
			delete WRec;
			WLinks->Delete(i);
		}
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISWLinkSyncReady");
		#endif
		break;
  }
  return Ready;
}

void __fastcall TCIISServerInterface::BIChannelTabSyncIni()
{
	BIChannelSyncState = CIISBIChannelSyncStart;
	BIChannelTabSyncCancel = false;

	BIChRecUser = new CIISBIChannelRec;
  for( int32_t i = BIChannels->Count - 1; i >= 0; i-- )
  {
		BIChRec = (CIISBIChannelRec*) BIChannels->Items[i];
		delete BIChRec;
		BIChannels->Delete(i);
  }
}

void __fastcall TCIISServerInterface::InitBIChannelRec(CIISBIChannelRec *BIChRec)
{
	BIChRec->VerNotSupported = false;
}

bool __fastcall TCIISServerInterface::BIChannelTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

  Ready = false;

	if( BIChannelTabSyncCancel )
  {
		BIChannelSyncState = CIISBIChannelSyncReady;
	} // BIChannelTabSyncCancel


  switch (BIChannelSyncState)
  {
		case CIISBIChannelSyncStart:
		BIChTabStatusSet.Clear();
		BIChannelSyncState = CIISBIChannelSyncRequestFirst;
		break;

		case CIISBIChannelSyncRequestFirst:
		{
		Data = "200=GetFirstToEnd\r\n";
		RequestNo = Request( CIISRead, CIISBIChannel, Data );
		BIChannelSyncState = CIISBIChannelSyncReadRec;
		};
		break;

		case CIISBIChannelSyncRequest:
		Data = "1=" + IntToStr( BIChRec->SerialNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISBIChannel, Data );
		BIChannelSyncState = CIISBIChannelSyncReadRec;
		break;

		case CIISBIChannelSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					bool MoreRec, BIChRecFound;

					// remove unused BIChannels from user db
					if( DB->FindFirstBIChannel() )
					{
						do
						{
							MoreRec = DB->GetBIChannelRec( BIChRecUser, true );
							BIChRecFound = false;
							for( int32_t i = 0; i < BIChannels->Count; i++ )
							{
								BIChRecLogger = (CIISBIChannelRec*) BIChannels->Items[i];
								if( BIChRecUser->SerialNo == BIChRecLogger->SerialNo )
								{
									BIChRecFound = true;
									break;
								}
							}
							if( !BIChRecFound )
							{
								DB->LocateBIChannelRec( BIChRecUser );
								DB->DeleteBIChannelRec( BIChRecUser );
							}
						} while( MoreRec );
					}
					// update used BIChannels and add new BIChannels to user db
					for( int32_t i = 0; i < BIChannels->Count; i++ )
					{
						BIChRec = (CIISBIChannelRec*) BIChannels->Items[i];
						if( DB->LocateBIChannelRec( BIChRec ) ) DB->SetBIChannelRec( BIChRec );
						else DB->AppendBIChannelRec( BIChRec );
					}
					DB->ApplyUpdatesBIChannels();

					BIChannelSyncState = CIISBIChannelSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						BIChRec = new CIISBIChannelRec;
						InitBIChannelRec( BIChRec );

						BIChRec->SerialNo = StrToInt(Reply->GetDelArg(1));
						BIChRec->Type = StrToInt( Reply->GetDelArg(2));
						Data = Reply->GetDelArg(3);
						if(Data != "" ) BIChRec->Name = StrToInt( Data );
						BIChRec->Included = CIISStrToBool( Reply->GetDelArg(4));
						BIChRec->Active = CIISStrToBool( Reply->GetDelArg(5));
						BIChRec->WatchDog = CIISStrToBool( Reply->GetDelArg(6));
						BIChRec->WDTime = StrToInt(Reply->GetDelArg(7));
						Data = Reply->GetDelArg(8);
						if(Data != "" ) BIChRec->Mode = StrToInt( Data );
						Data = Reply->GetDelArg(9);
						if(Data != "" ) BIChRec->IP = Data;
						Data = Reply->GetDelArg(10);
						if(Data != "" ) BIChRec->StaticIP = Data;
						Data = Reply->GetDelArg(11);
						if(Data != "" ) BIChRec->Gateway = Data;
						Data = Reply->GetDelArg(12);
						if(Data != "" ) BIChRec->Netmask = Data;
						Data = Reply->GetDelArg(13);
						if(Data != "" ) BIChRec->MACAddress = Data;
						Data = Reply->GetDelArg(14);
						if(Data != "" ) BIChRec->VerMajor = StrToInt( Data );
						Data = Reply->GetDelArg(15);
						if(Data != "" ) BIChRec->VerMinor = StrToInt( Data );
						Data = Reply->GetDelArg(16);
						if(Data != "" ) BIChRec->VerNotSupported = CIISStrToBool( Reply->GetDelArg(16));

						BIChannels->Add( BIChRec );
					}
					BIChannelSyncState = CIISBIChannelSyncRequest;
				}
			} // CIIMsgOk
			else
			{
				BIChTabStatusSet << BIChTSLocalError;
				BIChannelSyncState = CIISBIChannelSyncReady;
			}
			delete Reply;
		} // Reply
		break;

		case CIISBIChannelSyncReady:
		delete BIChRecUser;
		for( int32_t i = BIChannels->Count - 1; i >= 0; i-- )
		{
			BIChRec = (CIISBIChannelRec*) BIChannels->Items[i];
			delete BIChRec;
			BIChannels->Delete(i);
		}
		Ready = true;
		break;
	}
	return Ready;
}

void __fastcall TCIISServerInterface::RecordingTabSyncIni()
{
  RRec = new CIISRecordingRec;
	RecSyncState = CIISRecSyncStart;
	RecordingTabSyncCancel = false;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: RecordingTabSyncIni ");
	#endif
}

bool __fastcall TCIISServerInterface::RecordingTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

  Ready = false;

	if( RecordingTabSyncCancel )
  {
		switch (RecSyncState)
		{
			case CIISRecSyncStart:
			case CIISRecSyncRequestStopDate:
			case CIISRecSyncReadStopDate:
			case CIISRecSyncRequestFirst:
			case CIISRecSyncReadRec:
			case CIISRecSyncReady:
			break;
			case CIISRecSyncRequest:
			DB->ApplyUpdatesRecording();
			break;
		}
		RecSyncState = CIISRecSyncReady;
	} // RecordingTabSyncCancel


  switch (RecSyncState)
  {
		case CIISRecSyncStart:
		RecTabStatusSet.Clear();

		if( DB->FindFirstRecording() ) RecSyncState = CIISRecSyncRequestStopDate;
		else RecSyncState = CIISRecSyncRequestFirst;// No records User Rec table
		break;

  case CIISRecSyncRequestStopDate:
		RecSyncState = CIISRecSyncRequestFirst;
		do
		{
			MoreRec = DB->GetRecordingRec( RRec, true );
			if( RRec->RecStop < RRec->RecStart )      // <= before ver 3.11.0.1
			{
			Data = "1=" + IntToStr( RRec->RecNo ) + "\r\n" + "200=GetRecStop" + "\r\n";
			RequestNo = Request( CIISRead, CIISRecordings, Data );
			RecSyncState = CIISRecSyncReadStopDate;
			break;
			}
		} while( MoreRec );
		break;

  case CIISRecSyncReadStopDate:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
			Data = Reply->GetDelArg(1);
			if( Data != "" )
			{
				RRec->RecStop = CIISStrToDateTime( Data );
				if( DB->LocateRecordingNo( RRec->RecNo ) )
				{
				DB->SetRecordingRec( RRec ); DB->ApplyUpdatesRecording();
				}
			}
			if( DB->FindNextRecording() ) RecSyncState = CIISRecSyncRequestStopDate;
			else RecSyncState = CIISRecSyncRequestFirst;
			}
			else
			{
			RecSyncState = CIISRecSyncRequestFirst;
			}
			delete Reply;
		} // Reply
		break;

  case CIISRecSyncRequestFirst:
		if( DB->FindLastRecording())
		{
			DB->GetRecordingRec( RRec, false );
			Data = "1=" + IntToStr( RRec->RecNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		}
		else Data = "200=GetFirstToEnd\r\n";// No records in User Rec tab
		RequestNo = Request( CIISRead, CIISRecordings, Data );
		RecSyncState = CIISRecSyncReadRec;
		break;

  case CIISRecSyncRequest:
		Data = "1=" + IntToStr( RRec->RecNo ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISRecordings, Data );
		DB->ApplyUpdatesRecording();
		RecSyncState = CIISRecSyncReadRec;
		break;

  case CIISRecSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					DB->ApplyUpdatesRecording();
					RecSyncState = CIISRecSyncReady;
				}
				else
				{
					while( Reply->ArgInc( 1 ) )
					{
						RRec->RecNo = StrToInt( Reply->GetDelArg(1) );
						RRec->RecStart = CIISStrToDateTime( Reply->GetDelArg(2) );
						Data = Reply->GetDelArg(3);
						if(Data != "" ) RRec->RecStop = CIISStrToDateTime( Data );
						RRec->RecType = StrToInt( Reply->GetDelArg(4) );
						Data = Reply->GetDelArg(5);
						if(Data != "" ) RRec->DecaySampInterval = StrToInt( Data );
						Data = Reply->GetDelArg(6);
						if(Data != "" ) RRec->DecayDuration = StrToInt( Data );
						Data = Reply->GetDelArg(7);
						if(Data != "" ) RRec->LPRRange = StrToInt( Data );
						Data = Reply->GetDelArg(8);
						if(Data != "" ) RRec->LPRStep = StrToInt( Data );
						Data = Reply->GetDelArg(9);
						if(Data != "" ) RRec->LPRDelay1 = StrToInt( Data );
						Data = Reply->GetDelArg(10);
						if(Data != "" ) RRec->LPRDelay2 = StrToInt( Data );
						Data = Reply->GetDelArg(11);
						if(Data != "" ) RRec->SampInterval = StrToInt( Data );
						Data = Reply->GetDelArg(12);
						if(Data != "" ) RRec->LPRMode = StrToInt( Data );

						Data = Reply->GetDelArg(13);
						if(Data != "" ) RRec->DecayDelay = StrToInt( Data );
						Data = Reply->GetDelArg(14);
						if(Data != "" ) RRec->DecayDuration2 = StrToInt( Data );
						Data = Reply->GetDelArg(15);
						if(Data != "" ) RRec->DecaySampInterval2 = StrToInt( Data );

						RRec->ZoneNo = StrToInt( Reply->GetDelArg(50) );

						DB->AppendRecordingRec( RRec );
					}
					RecSyncState = CIISRecSyncRequest;
				}
			} // CIIMsgOk
			else
			{
				RecTabStatusSet << RecTSLocalError;
				RecSyncState = CIISRecSyncReady;
			}
			delete Reply;
		} // Reply
		break;

	case CIISRecSyncReady:
		delete RRec;
		Ready = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: RecordingTabSyncIni Ready ");
		#endif
		break;
	}
  return Ready;
}

void __fastcall TCIISServerInterface::ValueTabSyncIni()
{
	ValuesTabSyncCancel = false;
	ValSyncState = CIISValSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ValueTabSyncIni ");
	#endif
}

bool __fastcall TCIISServerInterface::ValueTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

  Ready = false;

	if( ValuesTabSyncCancel )
	{
		switch (ValSyncState)
		{
			case CIISValSyncStart:
			case CIISValSyncRequestRecCount:
			case CIISValSyncReadRecCount:
			case CIISValSyncRequestFirst:
			case CIISValSyncReady:
				break;

			case CIISValSyncRequest:
			case CIISValSyncReadRec:
				DB->ApplyUpdatesMonitorValue();
				break;
		}
		return true;
	} // ValuTabSyncCancel


	switch (ValSyncState)
  {
		case CIISValSyncStart:
		ValTabStatusSet.Clear();
		ValSyncState = CIISValSyncRequestRecCount;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncStart");
		#endif
		break;

		case CIISValSyncRequestRecCount:
		if( DB->FindLastMonitorValue() )
		{
			DB->GetMonitorValueRec( MRec, false );
			Data =  "1=" + CIISDateTimeToStr( MRec->DateTimeStamp ) + "\r\n" +
					"2=" + IntToStr( MRec->RecNo ) + "\r\n" +
					"3=" + IntToStr( MRec->SensorSerialNo ) + "\r\n" +
					"4=" + MRec->ValueType + "\r\n" +
					"200=GetRecordCount" + "\r\n";
		}
		else
		{
			Data = "200=GetRecordCount\r\n";
		}

		RequestNo = Request( CIISRead, CIISValues, Data );
		ValSyncState = CIISValSyncReadRecCount;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncRequestRecCount");
		#endif
		break;

		case CIISValSyncReadRecCount:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->ArgInc(100))
				{
					ValueLastSyncRecord = StrToInt(Reply->GetArg(100));
				}
			} // CIIMsgOk
			ValSyncState = CIISValSyncRequestFirst;
			delete Reply;
		} // Reply

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncReadRecCount");
		#endif
		break;

		case CIISValSyncRequestFirst:
		if( DB->FindLastMonitorValue() )
		{
			DB->GetMonitorValueRec( MRec, false );
			Data =  "1=" + CIISDateTimeToStr( MRec->DateTimeStamp ) + "\r\n" +
					"2=" + IntToStr( MRec->RecNo ) + "\r\n" +
					"3=" + IntToStr( MRec->SensorSerialNo ) + "\r\n" +
					"4=" + MRec->ValueType + "\r\n" +
					"200=GetNextToEnd" + "\r\n";
		}
		else // No records in User value tab
		{
			Data = "200=GetFirstToEnd\r\n";
		}
		RequestNo = Request( CIISRead, CIISValues, Data );
		ValueFirstRead = true;
		ValSyncState = CIISValSyncReadRec;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncRequestFirst");
		Debug( Data );
		#endif
		break;

		case CIISValSyncRequest:
			Data =  "1=" + CIISDateTimeToStr( MRec->DateTimeStamp ) + "\r\n" +
					"2=" + IntToStr( MRec->RecNo ) + "\r\n" +
					"3=" + IntToStr( MRec->SensorSerialNo ) + "\r\n" +
					"4=" + MRec->ValueType + "\r\n" +
					"200=GetNextToEnd" + "\r\n";
		RequestNo = Request( CIISRead, CIISValues, Data );
		DB->ApplyUpdatesMonitorValue();
		ValSyncState = CIISValSyncReadRec;


		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncRequest");
		#endif
		break;

		case CIISValSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					ValSyncState = CIISValSyncReady;
				}
				else
				{
					if( Reply->ArgInc(99))
					{
						Data = Reply->GetDelArg(99);
						ValueCurrSyncRecord = StrToInt( Data );
						if( ValueFirstRead )
						{
							ValueFirstRead = false;
							ValueFirstSyncRecord = ValueCurrSyncRecord;
						}
						if( ValueCurrSyncRecord > 0 )
						{
							Data = "99=" + Data + "\r\n" +
								 "200=GetNextToEnd" + "\r\n";
							RequestNo = Request( CIISRead, CIISValues, Data );
							ValSyncState = CIISValSyncReadRec;
						}
						else ValSyncState = CIISValSyncReady;
					}
					else
					{
						if( ValueFirstRead )
						{
							ValueFirstRead = false;
							ValueCurrSyncRecord = 0;
							ValueFirstSyncRecord = ValueCurrSyncRecord;
						}
						ValSyncState = CIISValSyncRequest;
					}
					while( Reply->ArgInc( 1 ) )
					{
						MRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
						MRec->RecNo = StrToInt( Reply->GetDelArg(2) );
						MRec->SensorSerialNo = StrToInt( Reply->GetDelArg(3) );
						MRec->ValueType = Reply->GetDelArg(4);
						MRec->ValueUnit = Reply->GetDelArg(5);
						MRec->Value = CIISStrToFloat( Reply->GetDelArg(6) );
						MRec->RawValue = CIISStrToFloat( Reply->GetDelArg(7) );
						Data = Reply->GetDelArg(8);
						if(Data != "" ) MRec->SampleDateTime = CIISStrToDateTime( Data );
						else MRec->SampleDateTime = MRec->DateTimeStamp;
						DB->AppendMonitorValueRec_NoTimeAdj( MRec );
						ValueCurrSyncRecord++;
					}
					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISValSyncReadRec to" + DateTimeToStr( MRec->SampleDateTime ));
					#endif
				}
			} // CIIMsgOk
			else
			{
				ValTabStatusSet << ValTSLocalError;
				ValSyncState = CIISValSyncReady;
			}
			delete Reply;
		} // Reply
		break;

		case CIISValSyncReady:
		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncReady");
		#endif

		DB->ApplyUpdatesMonitorValue();

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: CIISValSyncReady after ApplyUpdate");
		#endif

		Ready = true;
		break;
	}
  return Ready;
}

void __fastcall TCIISServerInterface::ValueLPRTabSyncIni()
{
	ValuesLPRTabSyncCancel = false;
	LPRSyncState = CIISLPRSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ValueLPRTabSyncIni ");
	#endif

}

bool __fastcall TCIISServerInterface::ValueLPRTabSync()
{
  TCamurPacket *Reply;
  String Data;
	bool Ready;
	int CurrRecNo, CurrStep, CurrSensor;

  Ready = false;

	if( ValuesLPRTabSyncCancel )
  {
		switch (LPRSyncState)
		{
		case CIISLPRSyncStart:
		case CIISLPRSyncRequestRecCount:
		case CIISLPRSyncReadRecCount:
		case CIISLPRSyncRequestFirst:
		case CIISLPRSyncReady:
			break;

		case CIISLPRSyncRequest:
		case CIISLPRSyncReadRec:
			DB->ApplyUpdatesLPRValue();
			break;
		}
	return true;
	} // ValuesLPRTabSyncCancel

  switch (LPRSyncState)
  {
	case CIISLPRSyncStart:
		LPRTabStatusSet.Clear();
		LPRSyncState = CIISLPRSyncRequestRecCount;
	  break;

	case CIISLPRSyncRequestRecCount:
	  if( DB->FindLastLPRValue() )
	  {
		DB->GetLPRValueRec( LPRRec, false );
		Data = "1=" + CIISDateTimeToStr( LPRRec->DateTimeStamp ) + "\r\n" + "200=GetRecordCount" + "\r\n";
	  }
	  else
	  {
		Data = "200=GetRecordCount\r\n";
	  }
	  
	  RequestNo = Request( CIISRead, CIISValuesLPR, Data );
	  LPRSyncState = CIISLPRSyncReadRecCount;
	  break;

	case CIISLPRSyncReadRecCount:
	  Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
	  {
		if( Reply->MessageCode == CIISMsg_Ok )
		{
		  if( Reply->ArgInc(100))
		  {
				LPRLastSyncRecord = StrToInt(Reply->GetArg(100));
		  }
		} // CIIMsgOk
		LPRSyncState = CIISLPRSyncRequestFirst;
		delete Reply;
	  } // Reply
	  break;

	case CIISLPRSyncRequestFirst:
	  if( DB->FindLastLPRValue() )
	  {
		DB->GetLPRValueRec( LPRRec, false );
		Data = "1=" + CIISDateTimeToStr( LPRRec->DateTimeStamp ) + "\r\n" + "200=GetNextToEnd" + "\r\n";
	  }
		else // No records in User LPR tab
	  {
		Data = "200=GetFirstToEnd\r\n";
	  }
	  RequestNo = Request( CIISRead, CIISValuesLPR, Data );
	  LPRFirstRead = true;
	  LPRSyncState = CIISLPRSyncReadRec;
	  break;

	case CIISLPRSyncRequest:
	  Data = "1=" + CIISDateTimeToStr( LPRRec->DateTimeStamp )  + "\r\n" + "200=GetNextToEnd" + "\r\n";
	  RequestNo = Request( CIISRead, CIISValuesLPR, Data );
	  DB->ApplyUpdatesLPRValue();
	  LPRSyncState = CIISLPRSyncReadRec;
	  break;

	case CIISLPRSyncReadRec:
	  Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					LPRSyncState = CIISLPRSyncReady;
				}
				else // Read rec
				{
					if( Reply->ArgInc(99))
					{
						Data = Reply->GetDelArg(99);
						LPRCurrSyncRecord = StrToInt( Data );
						if( LPRFirstRead )
						{
							LPRFirstRead = false;
							LPRFirstSyncRecord = LPRCurrSyncRecord;
						}
						if( LPRCurrSyncRecord > 0 )
						{
							Data = "99=" + Data + "\r\n" + "200=GetNextToEnd" + "\r\n";
							RequestNo = Request( CIISRead, CIISValuesLPR, Data );
							LPRSyncState = CIISLPRSyncReadRec;
						}
						else LPRSyncState = CIISLPRSyncReady;
					}
					else
					{
						if( LPRFirstRead )
						{
							LPRFirstRead = false;
							LPRCurrSyncRecord = 0;
							LPRFirstSyncRecord = LPRCurrSyncRecord;
						}
						LPRSyncState = CIISLPRSyncRequest;
					}
					while( Reply->ArgInc( 1 ) )
					{
						LPRRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
						LPRRec->RecNo = StrToInt( Reply->GetDelArg(2) );
						LPRRec->SensorSerialNo = StrToInt( Reply->GetDelArg(3) );
						LPRRec->ValueV = CIISStrToFloat( Reply->GetDelArg(4) );
						LPRRec->ValueI = CIISStrToFloat( Reply->GetDelArg(5) );
						if( Reply->ArgInc(6) ) LPRRec->ValueType = Reply->GetDelArg(6);
						else LPRRec->ValueType = "C1";
						DB->AppendLPRValueRec( LPRRec );
						LPRCurrSyncRecord++;
					} // while( Reply->ArgInc( 1 ) )

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISLPRSyncReadRec to " + DateTimeToStr( LPRRec->DateTimeStamp ));
					#endif

				} // else // Read rec
			} // if( Reply->MessageCode == CIISMsg_Ok )
			else
			{
				LPRTabStatusSet << LPRTSLocalError;
				LPRSyncState = CIISLPRSyncReady;
			}
			delete Reply;
	  } // Reply
	  break;

	case CIISLPRSyncReady:
	  DB->ApplyUpdatesLPRValue();
	  Ready = true;
	  break;
  }
	return Ready;
}

void __fastcall TCIISServerInterface::ValueCVTabSyncIni()
{
	ValuesCVTabSyncCancel = false;
	CVSyncState = CIISCVSyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ValueCVTabSyncIni ");
	#endif

}

bool __fastcall TCIISServerInterface::ValueCVTabSync()
{
  TCamurPacket *Reply;
  String Data;
	bool Ready;
	int CurrRecNo, CurrStep, CurrSensor;

  Ready = false;

	if( ValuesCVTabSyncCancel )
  {
		switch (CVSyncState)
		{
		case CIISCVSyncStart:
			case CIISCVSyncRequestRecCount:
			case CIISCVSyncReadRecCount:
			case CIISCVSyncRequestFirst:
			case CIISCVSyncReady:
			break;

		case CIISCVSyncRequest:
		case CIISCVSyncReadRec:
			DB->ApplyUpdatesCalcValue();
			break;
		}
		return true;
	} // ValuesCVTabSyncCancel

	switch (CVSyncState)
	{
	case CIISCVSyncStart:
		CVTabStatusSet.Clear();
		CVSyncState = CIISCVSyncRequestRecCount;
	  break;

	case CIISCVSyncRequestRecCount:
		if( DB->FindLastCalcValue() )
	  {
			DB->GetCalcValueRec( CVRec, false );
			Data = "1=" + CIISDateTimeToStr( CVRec->DateTimeStamp ) + "\r\n" + "200=GetRecordCount" + "\r\n";
	  }
	  else
	  {
			Data = "200=GetRecordCount\r\n";
	  }

		RequestNo = Request( CIISRead, CIISValuesCalc, Data );
	  CVSyncState = CIISCVSyncReadRecCount;
	  break;

	case CIISCVSyncReadRecCount:
	  Reply = ClientSocket->GetReply( RequestNo );
	  if( Reply != NULL )
	  {
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->ArgInc(100))
				{
					CVLastSyncRecord = StrToInt(Reply->GetArg(100));
				}
			} // CIIMsgOk
			CVSyncState = CIISCVSyncRequestFirst;
			delete Reply;
		} // Reply
	  break;

	case CIISCVSyncRequestFirst:
		if( DB->FindLastCalcValue() )
	  {
			DB->GetCalcValueRec( CVRec, false );
			Data = 	"1=" + CIISDateTimeToStr( CVRec->DateTimeStamp ) + "\r\n" +
					"2=" + IntToStr( CVRec->RecNo ) + "\r\n" +
					"3=" + IntToStr( CVRec->SensorSerialNo )  + "\r\n" +
					"4=" + CVRec->ValueType + "\r\n" +
					"200=GetNextToEnd" + "\r\n";

		}
		else // No records in User CV tab
	  {
			Data = "200=GetFirstToEnd\r\n";
	  }
		RequestNo = Request( CIISRead, CIISValuesCalc, Data );
		CVFirstRead = true;
		CVSyncState = CIISCVSyncReadRec;
		break;

	case CIISCVSyncRequest:
		Data = 	"1=" + CIISDateTimeToStr( CVRec->DateTimeStamp ) + "\r\n" +
				"2=" + IntToStr( CVRec->RecNo ) + "\r\n" +
				"3=" + IntToStr( CVRec->SensorSerialNo )  + "\r\n" +
				"4=" + CVRec->ValueType + "\r\n" +
				"200=GetNextToEnd" + "\r\n";

		RequestNo = Request( CIISRead, CIISValuesCalc, Data );
		DB->ApplyUpdatesCalcValue();
		CVSyncState = CIISCVSyncReadRec;
		break;

	case CIISCVSyncReadRec:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok )
			{
				if( Reply->MessageData == "" )
				{
					CVSyncState = CIISCVSyncReady;
				}
				else // Read rec
				{
					if( Reply->ArgInc(99))
					{
						Data = Reply->GetDelArg(99);
						CVCurrSyncRecord = StrToInt( Data );
						if( CVFirstRead )
						{
							CVFirstRead = false;
							CVFirstSyncRecord = CVCurrSyncRecord;
						}
						if( CVCurrSyncRecord > 0 )
						{
							Data = "99=" + Data + "\r\n" + "200=GetNextToEnd" + "\r\n";
							RequestNo = Request( CIISRead, CIISValuesCalc, Data );
							CVSyncState = CIISCVSyncReadRec;
						}
						else CVSyncState = CIISCVSyncReady;
					}
					else
					{
						if( CVFirstRead )
						{
							CVFirstRead = false;
							CVCurrSyncRecord = 0;
							CVFirstSyncRecord = CVCurrSyncRecord;
						}
						CVSyncState = CIISCVSyncRequest;
					}

					while( Reply->ArgInc( 1 ) )
					{
						CVRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
						CVRec->RecNo = StrToInt( Reply->GetDelArg(2) );
						CVRec->SensorSerialNo = StrToInt( Reply->GetDelArg(3) );
						CVRec->ValueType = Reply->GetDelArg(4);
						CVRec->ValueUnit = Reply->GetDelArg(5);
						CVRec->Value = CIISStrToFloat( Reply->GetDelArg(6) );
						CVRec->Code = StrToInt( Reply->GetDelArg(7) );
						DB->AppendCalcValueRec_NoTimeAdj( CVRec );
						CVCurrSyncRecord++;
					} // while( Reply->ArgInc( 1 ) )

					#if DebugNetIntRemote == 1
					Debug( "Ser Int: CIISCVSyncReadRec to " + DateTimeToStr( CVRec->DateTimeStamp ));
					#endif

				} // else // Read rec
			} // if( Reply->MessageCode == CIISMsg_Ok )
			else
			{
				CVTabStatusSet << CVTSLocalError;
				CVSyncState = CIISCVSyncReady;
			}
			delete Reply;
		} // Reply
		break;

	case CIISCVSyncReady:
		DB->ApplyUpdatesCalcValue();
		Ready = true;
		break;
	}
	return Ready;
}

void __fastcall TCIISServerInterface::ValueDecayTabSyncIni()
{
	ValuesDecayTabSyncCancel = false;
	DecaySyncState = CIISDecaySyncStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: ValueDecayTabSyncIni ");
	#endif

}

bool __fastcall TCIISServerInterface::ValueDecayTabSync()
{
  TCamurPacket *Reply;
  String Data;
  bool Ready;

  Ready = false;

	if( ValuesDecayTabSyncCancel )
	{
		switch (DecaySyncState)
		{
			case CIISDecaySyncStart:
			case CIISDecaySyncRequestRecCount:
			case CIISDecaySyncReadRecCount:
			case CIISDecaySyncRequestFirst:
			case CIISDecaySyncReady:
			break;

			case CIISDecaySyncRequest:
			case CIISDecaySyncReadRec:
			DB->ApplyUpdatesDecayValue();
			break;
		}
		return true;
	} // ValuesDecayTabSyncCancel

  switch (DecaySyncState)
	{
		case CIISDecaySyncStart:
			DecayTabStatusSet.Clear();
			DecaySyncState = CIISDecaySyncRequestRecCount;
			ValuesDecayCheckFirstReply = true;
			break;

		case CIISDecaySyncRequestRecCount:
			if( DB->FindLastDecayValue() )
			{
				DB->GetDecayValueRec( DRec, false );
				Data = 	"1=" + CIISDateTimeToStr( DRec->DateTimeStamp ) + "\r\n" +
						"2=" + IntToStr( DRec->RecNo ) + "\r\n" +
						"3=" + IntToStr( DRec->SensorSerialNo )  + "\r\n" +
						"4=" + DRec->ValueType + "\r\n" +
						"200=GetRecordCount" + "\r\n";
			}
			else
			{
				Data = "200=GetRecordCount\r\n";
			}

			RequestNo = Request( CIISRead, CIISValuesDecay, Data );
			DecaySyncState = CIISDecaySyncReadRecCount;
			break;

		case CIISDecaySyncReadRecCount:
			Reply = ClientSocket->GetReply( RequestNo );
			if( Reply != NULL )
			{
				if( Reply->MessageCode == CIISMsg_Ok )
				{
					if( Reply->ArgInc(100))
					{
						DecayLastSyncRecord = StrToInt(Reply->GetArg(100));
					}
				} // CIIMsgOk
				DecaySyncState = CIISDecaySyncRequestFirst;
				delete Reply;
			} // Reply
			break;

		case CIISDecaySyncRequestFirst:
			if( DB->FindLastDecayValue() )
			{
				DB->GetDecayValueRec( DRec, false );
				Data = 	"1=" + CIISDateTimeToStr( DRec->DateTimeStamp ) + "\r\n" +
						"2=" + IntToStr( DRec->RecNo ) + "\r\n" +
						"3=" + IntToStr( DRec->SensorSerialNo )  + "\r\n" +
						"4=" + DRec->ValueType + "\r\n" +
						"200=GetNextToEnd" + "\r\n";

				ValuesDecayCheckFirstReply = true;
			}
			else // No records in User Decay tab
			{
				Data = "200=GetFirstToEnd\r\n";
			}
			RequestNo = Request( CIISRead, CIISValuesDecay, Data );
			DecayFirstRead = true;
			DecaySyncState = CIISDecaySyncReadRec;
			break;

		case CIISDecaySyncRequest:
			Data = 	"1=" + CIISDateTimeToStr( DRec->DateTimeStamp ) + "\r\n" +
					"2=" + IntToStr( DRec->RecNo ) + "\r\n" +
					"3=" + IntToStr( DRec->SensorSerialNo )  + "\r\n" +
					"4=" + DRec->ValueType + "\r\n" +
					"200=GetNextToEnd" + "\r\n";

			ValuesDecayCheckFirstReply = true;
			RequestNo = Request( CIISRead, CIISValuesDecay, Data );
			DB->ApplyUpdatesDecayValue();
			DecaySyncState = CIISDecaySyncReadRec;
			break;

		case CIISDecaySyncReadRec:
			Reply = ClientSocket->GetReply( RequestNo );
			if( Reply != NULL )
			{
				if( Reply->MessageCode == CIISMsg_Ok )
				{
					if( Reply->MessageData == "" )
					{
						DecaySyncState = CIISDecaySyncReady;
					}
					else
					{
						if( Reply->ArgInc(99))
						{
							Data = Reply->GetDelArg(99);
							DecayCurrSyncRecord = StrToInt( Data );
							if( DecayFirstRead )
							{
								DecayFirstRead = false;
								DecayFirstSyncRecord = DecayCurrSyncRecord;
							}
							if( DecayCurrSyncRecord > 0 )
							{
								Data = "99=" + Data + "\r\n" + "200=GetNextToEnd" + "\r\n";
								RequestNo = Request( CIISRead, CIISValuesDecay, Data );
								DecaySyncState = CIISDecaySyncReadRec;
							}
							else DecaySyncState = CIISDecaySyncReady;
						}
						else
						{
							if( DecayFirstRead )
							{
								DecayFirstRead = false;
								DecayCurrSyncRecord = 0;
								DecayFirstSyncRecord = DecayCurrSyncRecord;
							}
							DecaySyncState = CIISDecaySyncRequest;
						}

						while( Reply->ArgInc( 1 ) )
						{
							DRec->DateTimeStamp = CIISStrToDateTime( Reply->GetDelArg(1) );
							DRec->RecNo = StrToInt( Reply->GetDelArg(2) );
							DRec->SensorSerialNo = StrToInt( Reply->GetDelArg(3) );
							DRec->Value = CIISStrToFloat( Reply->GetDelArg(4) );
							if( Reply->ArgInc(5) ) DRec->ValueType = Reply->GetDelArg(5);
							else DRec->ValueType = "U";
							Data = Reply->GetDelArg(6);
							if(Data != "" ) DRec->SampleDateTime = CIISStrToDateTime( Data );
							else DRec->SampleDateTime = DRec->DateTimeStamp;
							if( !ValuesDecayCheckFirstReply )	DB->AppendDecayValueRec_NoTimeAdj( DRec );
							else
							{
								if( !DB->LocateDecayValue( DRec->DateTimeStamp, DRec->RecNo, DRec->SensorSerialNo, DRec->ValueType )) DB->AppendDecayValueRec_NoTimeAdj( DRec );
								ValuesDecayCheckFirstReply = false;
							}
							DecayCurrSyncRecord++;
						}

							#if DebugNetIntRemote == 1
							Debug( "Ser Int: CIISDecaySyncReadRec to " + DateTimeToStr( DRec->SampleDateTime ));
							#endif

					}
				} // CIIMsgOk
				else
				{
				DecayTabStatusSet << DecayTSLocalError;
				DecaySyncState = CIISDecaySyncReady;
				}
				delete Reply;
			} // Reply
			break;

		case CIISDecaySyncReady:
			DB->ApplyUpdatesDecayValue();
			Ready = true;
			break;
  }
	return Ready;
}

void __fastcall TCIISServerInterface::SetEvTabStatus(int32_t EvCode)
{
	switch (EvCode)
  {
	case CIIEventCode_SensorLow: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorHigh: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorNormal: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorLow2: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorHigh2: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorNormal2: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorLow3: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorHigh3: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorNormal3: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorLow4: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorHigh4: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_SensorNormal4: EvTabStatusSet << ETSSensorCh;
	break;
	case CIIEventCode_PSLowU: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSHighU: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSLowI: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSHighI: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSNormalU: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSNormalI: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_AlarmEnable: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_AlarmDisable: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSVOutEnable: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSVOutDisable: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSRemote: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PSLocal: EvTabStatusSet << ETSPSCh;
    break;
	case CIIEventCode_PrjTabChanged: EvTabStatusSet << ETSPrjCh;
		break;
	case CIIEventCode_CtrlTabChanged: EvTabStatusSet << ETSCtrlCh;
		break;
	case CIIEventCode_ZoneTabChanged: EvTabStatusSet << ETSZoneCh;
		break;
	case CIIEventCode_SensorTabChanged: EvTabStatusSet << ETSSensorCh;
		break;
	case CIIEventCode_AlarmTabChanged: EvTabStatusSet << ETSAlarmCh;
		break;
	case CIIEventCode_WLinkTabChanged: EvTabStatusSet << ETSWLinkCh;
		break;
	case CIIEventCode_BIChTabChanged: EvTabStatusSet << ETSBIChannelCh;
		break;
	case CIIEventCode_PSTabChanged: EvTabStatusSet << ETSPSCh;
		break;
	case CIIEventCode_RecStart:
		EvTabStatusSet << ETSRecCh;
		EvTabStatusSet << ETSZoneCh;
    break;
	case CIIEventCode_RecStop:
		EvTabStatusSet << ETSRecCh;
		EvTabStatusSet << ETSZoneCh;
    break;
	case CIIEventCode_ResetCan:
		EvTabStatusSet << ETSSensorCh;
		EvTabStatusSet << ETSPSCh;
		EvTabStatusSet << ETSWLinkCh;
		EvTabStatusSet << ETSAlarmCh;
		EvTabStatusSet << ETSBIChannelCh;
		EvTabStatusSet << ETSRecCh;
    break;
	case CIIEventCode_ServerReStarted:
		EvTabStatusSet << ETSSensorCh;
		EvTabStatusSet << ETSPSCh;
		EvTabStatusSet << ETSWLinkCh;
		EvTabStatusSet << ETSAlarmCh;
		EvTabStatusSet << ETSBIChannelCh;
		EvTabStatusSet << ETSRecCh;
    break;
	}
}

bool __fastcall TCIISServerInterface::SM_Running()
{
	return ( FSysSyncRunning || FDataSyncRunning|| FLoggInRunning ||
			 FCancelSyncRunning || FOpenComRunning || FCloseComRunning );
}

#pragma argsused
void __fastcall TCIISServerInterface::TimerSMEvent(TObject * Sender)
{
	if( !TimerSMEnabled || StopReEntrancy ) return;

	StopReEntrancy = true;

	if( FSysSyncRunning ) SM_SystemSync();
	else if( FDataSyncRunning ) SM_DataSync();
	else if( FTransferMessageRunning ) SM_SendTransferMessage();
	else if( FLoggInRunning ) SM_LoggIn();
	else if( FOpenComRunning ) SM_OpenCom();
	else if( FCloseComRunning ) SM_CloseCom();
	else TimerSMEnabled = false;

	StopReEntrancy = false;
}

#pragma argsused
void __fastcall TCIISServerInterface::TimerSMCancelEvent(TObject * Sender)
{
	if( !TimerSMCancelEnabled ) return;

	if( FCancelSyncRunning ) SM_CancelSync();
	else TimerSMCancelEnabled = false;
}

bool __fastcall TCIISServerInterface::TransferMessage( TCamurPacket *p )
{
  bool SC;

  SC = ClientSocket->ServerConnected;

  if( SC )
  {
    p->MessageOrgNumber = p->MessageNumber;
		TransferMessageList->Add( p );

		FTransferMessageRunning = true;
		TimerSMEnabled = true;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: MSG " + IntToStr( p->MessageNumber ) + " moved to TransferMessageList " );
		#endif
	}
	return SC;
}

void __fastcall TCIISServerInterface::SM_SendTransferMessage()
{
  TCamurPacket *p;

	for( int32_t i = 0; i < TransferMessageList->Count; i++ )
  {
		p = (TCamurPacket*)TransferMessageList->Items[i];
    p->MessageNumber = MessageNumber++;
		#if DebugNetIntRemote == 1
		Debug( "Ser Int: MSG org no " + IntToStr( p->MessageOrgNumber ) + " renum to " + IntToStr( p->MessageNumber ) );
    #endif
		ClientSocket->SendMessage( p );
		if( SendTransferMessageCancel )
		{
			SendTransferMessageCancel = false;
			break;
		}
  }

	FTransferMessageRunning = false;
}

bool __fastcall TCIISServerInterface::TransferResponse()
{
	TCamurPacket *o;

  if( TransferReply != NULL  )
  {
    for( int32_t i = 0; i < TransferResponseList->Count; i++ )
    {
      o = (TCamurPacket*)TransferResponseList->Items[i];

      TransferReply( o );
      TransferResponseList->Delete(i);
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: TransferResponse MSG: " + IntToStr( o->MessageNumber ));
      #endif
    }
  } // TransferReply != NULL

  return true;
}

void __fastcall TCIISServerInterface::MessageRecived()
{
	while( ParseCommands() );
}

bool __fastcall TCIISServerInterface::ParseCommands()
{
  TCamurPacket *r, *p;
  bool TransferMessageFound;

  TransferMessageFound = false;

	for( int32_t j = 0; j < TransferMessageList->Count; j++ )
  {
		p = (TCamurPacket*)TransferMessageList->Items[j];
		r = ClientSocket->GetReply( p->MessageNumber );

		if( r == NULL ) // F�r snabbare Sync, anropa TimerSMEvent f�r k�rning av sync sys, data, ...
		{
			TimerSMEvent( this );
		}
		else
		{
			#if DebugNetIntRemote == 1
			Debug( "Ser Int: MSG " + IntToStr( p->MessageNumber ) + " renum to org no" + IntToStr( p->MessageOrgNumber ) );
			#endif
			TransferMessageFound = true;
			r->MessageNumber = p->MessageOrgNumber;
			TransferResponseList->Add(r);
			TransferMessageList->Delete(j);
			break;
		}
	}


	if( TransferMessageFound ) SystemSyncIni();

  return false;
}

int32_t __fastcall TCIISServerInterface::Request( CIISCommand Cmd, CIISTable Tab, String Data)
{
  TCamurPacket *o;
  Byte ReqNo;

  ReqNo = MessageNumber++;

  o = new TCamurPacket;
  o->MessageType = CIISRequest;
	o->MessageNumber = ReqNo;
  o->MessageOrgNumber = ReqNo;
  o->MessageCode = CIISMsg_Ok;
  o->MessageCommand = Cmd;
  o->MessageTable = Tab;
	o->MessageData = Data;

	ClientSocket->SendMessage( o );

  delete o;

	return ReqNo;
}

bool __fastcall TCIISServerInterface::Connected()
{
  return ClientSocket->ServerConnected;
}

void __fastcall TCIISServerInterface::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

void __fastcall TCIISServerInterface::SetDebugWin(TDebugWin * SetDebugWin)
{
  DW = SetDebugWin;
  ClientSocket->SetDebugWin( SetDebugWin );
}

bool __fastcall TCIISServerInterface::OpenComIni()
{
	if( SM_Running() ) return false;

	PrjRecOC = new CIISPrjRec;
	FOpenComRunning = true;
	TimerSMEnabled = true;
	OpenComState = CIISOpenCom;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: OpenComIni ");
	#endif

	return true;
}

void __fastcall TCIISServerInterface::SM_OpenCom()
{
  if (OpenComCancel) OpenComState = CIISOpenComCancel;

  switch (OpenComState)
	{
	case CIISOpenCom :
	  DB->GetPrjRec( PrjRecOC );
	  IPAdr = PrjRecOC->ConnServerIP;
	  ClientSocket->OpenCom( IPAdr );
	  OpenComState = CIISOpenComCheck;
	  break;

	case CIISOpenComCheck :
		if( ClientSocket->ServerConnected ) OpenComState = CIISOpenComReady;
	  break;

	case CIISOpenComCancel:
	  OpenComCancel = false;
	  OpenComState = CIISOpenComReady;
	  break;

	case CIISOpenComReady :
	  delete PrjRecOC;
	  FOpenComRunning = false;
	  break;
  }
}

bool __fastcall TCIISServerInterface::CloseComIni()
{
	if( SM_Running() ) return false;

	FCloseComRunning = true;
	TimerSMEnabled = true;
	CloseComState = CIISCloseCom;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: CloseComIni ");
	#endif

	return true;
}

void __fastcall TCIISServerInterface::SM_CloseCom()
{
	if (CloseComCancel) CloseComState = CIISCloseComCancel;

	switch (CloseComState)
	{
		case CIISCloseCom :
		{
			ClientSocket->CloseCom();
			CloseComState = CIISCloseComCheck;
		}
		break;

		case CIISCloseComCheck :
		{
			if( !ClientSocket->ServerConnected ) CloseComState = CIISCloseComReady;
		}
		break;

		case CIISCloseComReady :
		{
			FCloseComRunning = false;
		}
		break;

		case CIISCloseComCancel :
		{
			CloseComCancel = false;
			FCloseComRunning = false;
		}
		break;
	}
}

bool __fastcall TCIISServerInterface::LoggInIni( String SetUserLevel, String SetPassword )
{
	if( SM_Running() ) return false;

	PrjRecLI = new CIISPrjRec;
	FLoggedIn = false;
	UserLevel = SetUserLevel;
	Password = SetPassword;

	FLoggInRunning = true;
	TimerSMEnabled = true;
	LoggInState = CIISLoggInStart;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: LogInIni ");
	#endif

	return true;
}

void __fastcall TCIISServerInterface::SM_LoggIn()
{
  TCamurPacket *Reply;
  String Data;

	if( LoggInCancel ) LoggInState = CIISLoggInCancel;

  switch( LoggInState )
	{
	case CIISLoggInStart:
		DB->GetPrjRec( PrjRecLI );
		IPAdr = PrjRecLI->ConnServerIP;
		ValuesMiscSize = PrjRecLI->ValuesMiscSize;
		MiscValueGetLoggerLastRec = true;
		MiscValueLoggerEmpty = false;
		ClientSocket->OpenCom( IPAdr );
		LoggInState = CIISLoggInOpenCom;
		break;

  case CIISLoggInOpenCom:
		if( ClientSocket->ServerConnected )
		{
			Data = "1=" + UserLevel + "\r\n" + "2=" + Password + "\r\n";
			RequestNo = Request( CIISLogin, CIISProject, Data );
			LoggInState = CIISLoggInRead;
		}
		break;

  case CIISLoggInRead:
		Reply = ClientSocket->GetReply( RequestNo );
		if( Reply != NULL )
		{
			if( Reply->MessageCode == CIISMsg_Ok ) FLoggedIn = true;
			else FLoggedIn = false;
			delete Reply;
			LoggInState = CIISLoggInReady;
		}
		break;

  case CIISLoggInCancel:
		ClientSocket->CloseCom();
		FLoggedIn = false;
		LoggInCancel = false;
		LoggInState = CIISLoggInReady;
		break;

  case CIISLoggInReady:
		delete PrjRecLI;
		FLoggInRunning = false;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: LogInReady ");
		#endif

		break;
	}
		return;
}

bool __fastcall TCIISServerInterface::CancelSyncIni()
{
	if( FCancelSyncRunning ) return false;

	FCancelSyncRunning = true;
	TimerSMCancelEnabled = true;
	CancelSyncState = CIISCancelSync;

	#if DebugNetIntRemote == 1
	Debug( "Ser Int: CancelSyncIni ");
  #endif

  return true;
}

void __fastcall TCIISServerInterface::SM_CancelSync()
{
  TCamurPacket *p;

  switch( CancelSyncState )
	{
	case CIISCancelSync:
		if( FSysSyncRunning ) SysSyncCancel = true;
		if( FDataSyncRunning ) DataSyncCancel = true;
		if( FTransferMessageRunning ) SendTransferMessageCancel = true;
		if( FLoggInRunning ) LoggInCancel = true;
		if( FOpenComRunning ) OpenComCancel = true;
		if( FCloseComRunning ) CloseComCancel = true;
		CancelSyncState = CIISCanselSyncWait;
		break;

	case CIISCanselSyncWait:
		if( !( FSysSyncRunning || FDataSyncRunning || FTransferMessageRunning ||
			 FLoggInRunning  || FOpenComRunning  || FCloseComRunning ))
		CancelSyncState = CIISCanselSyncClose;
	  break;

	case CIISCanselSyncClose:
		for( int32_t i = 0; i < TransferMessageList->Count; i++ )
	  {
			p = (TCamurPacket*)TransferMessageList->Items[i];
			delete p;
			TransferMessageList->Delete(i);
	  }
	  for( int32_t i = 0; i < TransferResponseList->Count; i++ )
	  {
			p = (TCamurPacket*)TransferResponseList->Items[i];
			delete p;
			TransferResponseList->Delete(i);
	  }
		EvTabStatusSet.Clear();

		PrjRecCS = new CIISPrjRec;
		DB->GetPrjRec( PrjRecCS );
		PrjRecCS->ServerMode = CIISNotInitiated;
		PrjRecCS->ConfigVer--;
		PrjRecCS->DataVer--;
		PrjRecCS->LastValVer--;
		DB->SetPrjRec( PrjRecCS );
		DB->ApplyUpdatesPrj();

		delete PrjRecCS;
		FCancelSyncRunning = false;

		#if DebugNetIntRemote == 1
		Debug( "Ser Int: Cancel Sync Ready");
		#endif
		break;
	}
}

String __fastcall TCIISServerInterface::StatusMessage()
{
	String SM;
	int32_t FRec, CRec, LRec, TRec, RRec;

	SM = "";

	if( FSysSyncRunning )
	{
		SM = "1=SysSync \r\n";
		switch ( SysSyncState )
		{
			case CIISSysSyncEvent: SM = SM + "2=EventLog" + "\r\n";
			break;
			case CIISSysSyncProject: SM = SM + "2=Project" + "\r\n";
			break;
			case CIISSysSyncCtrl: SM = SM + "2=Controllers" + "\r\n";
			break;
			case CIISSysSyncZone: SM = SM + "2=Zones" + "\r\n";
			break;
			case CIISSysSyncSensor: SM = SM + "2=Sensors" + "\r\n";
			break;
			case CIISSysSyncPS: SM = SM + "2=PowerSupplys" + "\r\n";
			break;
			case CIISSysSyncWLink: SM = SM + "2=WLinks" + "\r\n";
			break;
			case CIISSysSyncRec: SM = SM + "2=Recordings" + "\r\n";
			break;

			default: break;
		}
	}
	else if( FDataSyncRunning )
	{
		SM = "1=DataSync \r\n";
		switch ( DataSyncState )
		{
			case CIISDataSyncValues:
				SM = SM + "2=Values" + "\r\n";
				FRec = ValueFirstSyncRecord;
				CRec = ValueCurrSyncRecord;
				LRec = ValueLastSyncRecord;
			break;
			case CIISDataSyncLPR:
				SM = SM + "2=ValuesLPR" + "\r\n";
				FRec = LPRFirstSyncRecord;
				CRec = LPRCurrSyncRecord;
				LRec = LPRLastSyncRecord;
			break;
			case CIISDataSyncCV:
				SM = SM + "2=ValuesCV" + "\r\n";
				FRec = CVFirstSyncRecord;
				CRec = CVCurrSyncRecord;
				LRec = CVLastSyncRecord;
			break;
			case CIISDataSyncDecay:
				SM = SM + "2=ValuesDecay" + "\r\n";
				FRec = DecayFirstSyncRecord;
				CRec = DecayCurrSyncRecord;
				LRec = DecayLastSyncRecord;
			break;

            default: break;
		}

		TRec = LRec - FRec;
		if( TRec > 0 )
		{
			RRec = ( CRec - FRec ) * 100 / TRec;
			SM = SM + "3=" + IntToStr(RRec) + "\r\n" +
								"4=" + IntToStr(CRec) + "\r\n" +
								"5=" + IntToStr(LRec) +  "\r\n";
		}
	}
	else if( FTransferMessageRunning ) SM = "1=TransMessage \r\n";
	else if( FLoggInRunning ) SM = "1=RemoteLogin \r\n";
	else if( FOpenComRunning ) SM = "1=OpenCom \r\n";
	else if( FCloseComRunning )SM = "1=CloseCom \r\n";
	else if( FCancelSyncRunning ) SM = "1=Canceling \r\n";
	else SM = "1=Waiting \r\n";

/*
  	SM = SM + "6=" + IntToStr( MessageNumber ) + "\r\n";

*/
		#if DebugNetIntRemote == 1
		Debug( "Ser Int: "  + SM );
		#endif

  return SM;
}


