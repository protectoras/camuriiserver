//---------------------------------------------------------------------------

#ifndef CIISControllerSetupH
#define CIISControllerSetupH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "CIISCommon.h"
#include "CIISDB.h"
#include "CIISProject.h"
#include "CIISBusInterface.h"

//---------------------------------------------------------------------------
class TFrmControllerSetup : public TForm
{
__published:	// IDE-managed Components
	TButton *BGetSerial;
	TLabel *LSerialNo;
	TButton *BGetBIMode;
	TLabel *LBIMode;
	TButton *BGetBIVer;
	TLabel *LBIVer;
	void __fastcall BGetSerialClick(TObject *Sender);
	void __fastcall BGetBIModeClick(TObject *Sender);
	void __fastcall BGetBIVerClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations

	TCIISDBModule *DB;
  TDebugWin *DW;

	ProcMessFP CtrlIncommingBIChMessage;

  TList *ChildList;
	TCIISProject *P_Prj;
	TCIISController *P_Ctrl;
	TCIISZone *P_Zone;
	TCIISBusInterface *P_BI;
	TCIISBIChannel *BICh;
	void __fastcall IncomingBusIntMessage(const Byte *BusIntMsg, PVOID BIChannel);

public:		// User declarations
	__fastcall TFrmControllerSetup(TComponent* Owner,
																 TCIISController* Ctrl);


};
//---------------------------------------------------------------------------
extern PACKAGE TFrmControllerSetup *FrmControllerSetup;
//---------------------------------------------------------------------------
#endif
