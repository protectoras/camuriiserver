//---------------------------------------------------------------------------

#ifndef CIISCTx8SetupH
#define CIISCTx8SetupH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "CIISSensor.h"
#include "CIISBusInterface.h"
//---------------------------------------------------------------------------
class TFrmCTx8SetUp : public TForm
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall TFrmCTx8SetUp(TComponent* Owner, TCIISSensor* SetSensor );
};
//---------------------------------------------------------------------------
extern PACKAGE TFrmCTx8SetUp *FrmCTx8SetUp;
//---------------------------------------------------------------------------
#endif
