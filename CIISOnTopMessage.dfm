object FormOnTopMessage: TFormOnTopMessage
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Waiting for MySQL Service to start...'
  ClientHeight = 11
  ClientWidth = 210
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
end
