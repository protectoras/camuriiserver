//---------------------------------------------------------------------------

#ifndef CIISMainUserH
#define CIISMainUserH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CIISDB.h"
#include "DebugWinU.h"
#include "CIISClientInterfaceUser.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <System.Win.ScktComp.hpp>
#include <ComCtrls.hpp>
#include <Mask.hpp>

#include "CIISProject.h"

#define Startmode 1


// 1 : RemoteServer ( User )
// 2 : RemoteServer + DebugWin
// 3 : RemoteServer + DebugWin + Selectable port no.

//---------------------------------------------------------------------------
class TCIISMainFrm : public TForm
{
__published:	// IDE-managed Components
  TBitBtn *BBReset;
  TTimer *TimerSysCheck;
  TBitBtn *BBDebugWin;
  TGroupBox *GBLoggerIP;
  TEdit *EdLocalIP;
	TGroupBox *GBServerPortsConnected;
	TLabel *LCIIPM;
  TLabel *LCIIMonitor;
  TGroupBox *GBClientPorts;
  TLabel *LCIILogger;
  TShape *ShPMConnected;
  TShape *ShMonitorConnected;
  TShape *ShLoggerConnected;
  TBitBtn *BBStartCIISUser;
	TGroupBox *GBSysInfo;
	TLabel *LTSysValue;
	TLabel *LTSys;
  void __fastcall FormCreate(TObject *Sender);
  void __fastcall FormDestroy(TObject *Sender);
  void __fastcall TimerSysCheckTimer(TObject *Sender);
  void __fastcall ChangeDebugState( int32_t State );
  void __fastcall BBDebugWinClick(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall BBStartCIISUserClick(TObject *Sender);

private:	// User declarations
  TDebugWin *DebWin;
  TCIISDBModule *DB;
  TCIISServerInterface *SerInt;
  TCIISClientInterfaceUser *ClientInt;
  bool StartDebugWin, InitFaild;
  TDateTime TSync;
  void __fastcall Debug(String Message);
	String __fastcall GetCommonAppFolderPath(void);
	String IniFilePath;

  void __fastcall TrimAppMemorySize(void);

	TDateTime TimerStart, TAfterSysClock, TSysClock;
	int32_t OnSlowClockTick;
	__int64 NextEvent_ms;
	TDateTime  SClockNextEvent, SClockStart;
	int32_t SClockEvent;
	int32_t ReduceWorkingSetCounter, SystemSyncCounter, KeepDBCounter;



  TCIISProject *Prj;
	CIISPrjRec *PrjRec;

	TCIIServerSocket *CIIMonitorServerSocket, *CIIPMServerSocket;
	TCIIClientSocket *CIISLoggerClientSocket;

  //------------

public:		// User declarations
  __fastcall TCIISMainFrm(TComponent* Owner);



};
//---------------------------------------------------------------------------
extern PACKAGE TCIISMainFrm *CIISMainFrm;
//---------------------------------------------------------------------------
#endif


