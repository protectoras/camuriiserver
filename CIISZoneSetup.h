//---------------------------------------------------------------------------

#ifndef CIISZoneSetupH
#define CIISZoneSetupH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "CIISZone.h"
#include "CIISBusInterface.h"
#include "DebugWinU.h"

#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFrmZoneSetup : public TForm
{
__published:	// IDE-managed Components
	TButton *BGetBIChSerial;
	TLabel *LBIChSerialNo;
	TButton *BGetBIChMode;
	TLabel *LBIChMode;
	TButton *BStdMode;
	TButton *BuCtrlMode;
	TButton *BGetTime;
	TButton *BSetTime;
	TLabel *LuCtrlTime;
	TDateTimePicker *DTPSetuCtrlDate;
	TDateTimePicker *DTPSetuCtrlTime;
	TButton *BNow;
	TRadioGroup *RGuCtrlPOut;
	TRadioGroup *RGuCtrlPMode;
	TEdit *EduCtrlVOut;
	TEdit *EduCtrlIOut;
	TEdit *EduCtrlInterval;
	TLabel *Label34;
	TLabel *Label35;
	TLabel *Label39;
	TButton *BReadFixVolt;
	TButton *BSetFixVolt;
	TButton *BStartRec;
	TButton *BStopRec;
	TButton *BGetNodeCount;
	TButton *BGetNodeInfo1;
	TLabel *LNodeCount;
	TLabel *Label1;
	TShape *ShPowerIn;
	TShape *ShPowerOn;
	TShape *ShPowerOk;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *LFixVoltSnr;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *LVout;
	TLabel *LIOut;
	TLabel *LVIn;
	TLabel *LVTerm;
	TLabel *Label9;
	TLabel *LP4Snr;
	TLabel *Label11;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *LV1;
	TLabel *LV2;
	TLabel *LV3;
	TLabel *LV4;
	TButton *BGetNodeInfo2;
	TButton *BGetData;
	TLabel *Label10;
	TLabel *LStoredValues;
	TLabel *Label15;
	TLabel *LTimeStamp;
	TButton *BRecStatus;
	TLabel *LRecStatus;
	TLabel *Label16;
	TLabel *Label17;
	TLabel *Label18;
	TCheckBox *ChBStoreData;
	TLabel *LBIChVer;
	void __fastcall BGetBIChSerialClick(TObject *Sender);
	void __fastcall BGetBIChModeClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BGetTimeClick(TObject *Sender);
	void __fastcall BNowClick(TObject *Sender);
	void __fastcall BStdModeClick(TObject *Sender);
	void __fastcall BuCtrlModeClick(TObject *Sender);
	void __fastcall BSetTimeClick(TObject *Sender);
	void __fastcall BReadFixVoltClick(TObject *Sender);
	void __fastcall BSetFixVoltClick(TObject *Sender);
	void __fastcall BStartRecClick(TObject *Sender);
	void __fastcall BStopRecClick(TObject *Sender);
	void __fastcall BGetNodeCountClick(TObject *Sender);
	void __fastcall BGetNodeInfo1Click(TObject *Sender);
	void __fastcall BGetNodeInfo2Click(TObject *Sender);
	void __fastcall BGetDataClick(TObject *Sender);

	void __fastcall ChangeDebugState(int State);
	void __fastcall BRecStatusClick(TObject *Sender);

private:	// User declarations

	TDebugWin *DebWin;

	TCIISZone *Zone;
	TCIISBIChannel *BICh;
	ProcMessFP ZoneIncommingBIChMessage;

	double PS_Ch1BitVal, PS_Ch2BitVal, PS_Ch3BitVal, PS_Ch4BitVal;
	double P4_Ch1BitVal, P4_Ch2BitVal, P4_Ch3BitVal, P4_Ch4BitVal;
	int PSIndex, P4Index, RequestIndex;

	Byte RecordingMem[128*1024];

	int InPointer, TotLenght, DataLenght;

	String BIChVersion;


	void __fastcall IncomingBIChMessage(const Byte *BusIntMsg, PVOID BIChannel);

	Word __fastcall CIISIntToBcd( Word ival );
	Word __fastcall CIISBcdToInt( Word ival );

	void __fastcall Debug(String Message);

public:		// User declarations
	__fastcall TFrmZoneSetup(TComponent* Owner, TCIISZone* SetZone );
};
//---------------------------------------------------------------------------
extern PACKAGE TFrmZoneSetup *FrmZoneSetup;
//---------------------------------------------------------------------------
#endif
