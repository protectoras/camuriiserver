//---------------------------------------------------------------------------

#ifndef CIISMainLoggerH
#define CIISMainLoggerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CIISDB.h"
#include "DebugWinU.h"
#include "CIISClientInterfaceLogger.h"
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <System.Win.ScktComp.hpp>
#include <ComCtrls.hpp>
#include <Mask.hpp>

#include "CIISProject.h"

#include "CIISCorrRate.h"

#define MYSQL_SERVICE_START_TIMEOUT 60 // seconds

#define KeepDBPeriod 3600L

#define Startmode 1



// 1 : LocalServer ( Logger )
// 2 : LocalServer + DebugWin
// 3 : LocalServer + DebugWin + Selectable port no.

//---------------------------------------------------------------------------
class TCIISMainLoggerFrm : public TForm
{
__published:	// IDE-managed Components
		TBitBtn *BBReset;
	TTimer *TimerSysCheck;
        TBitBtn *BBDebugWin;
	TGroupBox *GBServerPortsConnected;
	TLabel *LCIIMonitor;
	TLabel *LCIIUser;
	TGroupBox *GBCamurIILogger;
	TLabel *LCIIBusInt;
	TLabel *LNodes;
	TLabel *LNoOfNodes;
	TLabel *LNoOfBusInt;
	TGroupBox *GBRecordings;
	TLabel *LNoOfMonitors;
	TLabel *LMonitors;
	TLabel *LNoOfDecays;
	TLabel *LDecays;
	TLabel *NoOfLPRs;
	TLabel *LLPRs;
	TShape *ShMonitorConnected;
	TShape *ShUserConnected;
	TBitBtn *BBStartCIISLoggger;
	TLabel *LNoOfDetecedNodes;
	TLabel *LDetectedNodes;
	TLabel *LNodeIniErrors;
	TLabel *LNoOfNodIniErrors;
	TLabel *LTCANRx;
	TLabel *LTSys;
	TLabel *LTCANRxValue;
	TLabel *LTSysValue;
	TLabel *LNoOfMonitorsExt;
	TLabel *LMonitorsExt;
	TGroupBox *GBuCtrl;
	TShape *ShuCtrlRecordingOn;
	TLabel *LuCtrlRecordingOn;
	TShape *ShuCtrlReadingData;
	TLabel *LuCtrlReadingData;
	TLabel *LuCtrlSampleTimeStamp;
	TLabel *LuCtrlBytesRead;
	TLabel *Label2;
	TShape *ShuCtrlPowerIn;
	TShape *ShuCtrlPowerOn;
	TLabel *Label3;
	TShape *ShuCtrlPowerOk;
	TLabel *Label4;
	TBitBtn *BBuCtrlSync;
	TLabel *LuCtrlTime;
	TShape *ShuCtrlStoringData;
	TLabel *Label1;
	TBitBtn *BBuCtrlREadData;
	TShape *ShRunning;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall BBResetClick(TObject *Sender);
        void __fastcall TimerSystemClockEvent(TObject *Sender);
        void __fastcall ChangeDebugState( int32_t State);
        void __fastcall BBDebugWinClick(TObject *Sender);
		void __fastcall FormShow(TObject *Sender);
	void __fastcall BBStartCIISLogggerClick(TObject *Sender);
	void __fastcall BBuCtrlSyncClick(TObject *Sender);
	void __fastcall BBuCtrlREadDataClick(TObject *Sender);

private:	// User declarations
  TDebugWin *DebWin;
	TCIISDBModule *DB;
	TCIISClientInterfaceLogger *ClientInt;
	bool StartDebugWin, InitFaild, DatabaseCreated;
	void __fastcall TrimAppMemorySize(void);

		String IniFilePath;

	TDateTime TimerStart, TAfterReadCAN, TAfterSysClock, TReadCAN, TSysClock;
	double TFix;
	void __fastcall Debug(String Message);

	TCIISProject *Prj;
	TList *BusIntList;
	TCIISBusInterface *CIISBusInterface;
	TCIIServerSocket *LSocket, *RSocket;
	uint64_t NextEvent_ms;
	TDateTime  SClockNextEvent, SClockStart;
	uint32_t SClockEvent;
	uint32_t OnSlowClockTick;
	uint32_t ReduceWorkingSetCounter, KeepDBCounter;
	bool SysClockEnabled;
	uint32_t RemoteClientUnaktive;

public:		// User declarations
  __fastcall TCIISMainLoggerFrm(TComponent* Owner);



};
//---------------------------------------------------------------------------
extern PACKAGE TCIISMainLoggerFrm *CIISMainLoggerFrm;
//---------------------------------------------------------------------------
#endif


