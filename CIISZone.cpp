//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISZone.h"
#include "CIISProject.h"
#include "CIISController.h"
#include "CIISBusInterface.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------



__fastcall TCIISZone::TCIISZone(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
																TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
																TObject *SetCIISParent, bool NewZone )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{

  TCIISSensor *Sensor;
  TCIISAlarm *Alarm;
  TCIISPowerSupply *PS;
  CIISSensorRec *SR;
  CIISAlarmRec *AR;
  CIISPowerSupplyRec *PSR;
  bool MoreRecords;

  CIISObjType = CIISZones;


  InstalledPS = NULL;

  P_Zone = NULL;
	P_Ctrl = (TCIISController*)CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

  SMRunning_RestartZone = false;
  OnSlowClockTick = 200;
  ForceStopRec = false;
  FMonitorStartDelay = 0;


  ZoneRec = SetZoneRec;
  RR = NULL;

  if( !NewZone )
  {
	DB->SetSensorFilter(ZoneRec);
	if( DB->FindFirstSensor())
	{
	  do
	  {
		SR = new CIISSensorRec;
		MoreRecords = DB->GetSensorRec( SR, true );

		SR->SensorCanAdr = 0;
		SR->SensorStatus = 1;
		SR->SensorConnected = false;
		SR->ZoneCanAdr = ZoneRec->ZoneCanAdr;   // Needs Apply Uppdate ???

		SR->SensorAlarmStatus = 0;
		SR->SensorAlarmStatus2 = 0;
		SR->SensorAlarmStatus3 = 0;
		SR->SensorAlarmStatus4 = 0;


		if( SR->SensorType == Camur_II_LPR ) Sensor = new TCIISSensor_LPR(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_IO ) Sensor = new TCIISSensor_IO(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_P ) Sensor = new TCIISSensor_P(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_P4 ) Sensor = new TCIISSensor_P4(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_LPRExt ) Sensor = new TCIISSensor_LPRExt(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_HUM ) Sensor = new TCIISSensor_HUM(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_HiRes ) Sensor = new TCIISSensor_HiRes(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_R ) Sensor = new TCIISSensor_R(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_Wenner ) Sensor = new TCIISSensor_Wenner(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_ZRA) Sensor = new TCIISSensor_ZRA(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_CrackWatch) Sensor = new TCIISSensor_CrackWatch(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_PT) Sensor = new TCIISSensor_PT(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_CT) Sensor = new TCIISSensor_CT(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_MRE) Sensor = new TCIISSensor_MRE(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_CW) Sensor = new TCIISSensor_CW(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else if( SR->SensorType == Camur_II_Ladder) Sensor = new TCIISSensor_Ladder(SetDB, SetCIISBusInt, SetDebugWin, SR, this );
		else  Sensor = new TCIISSensor(SetDB, SetCIISBusInt, SetDebugWin, SR, this );

		ChildList->Add( Sensor );
	  } while ( MoreRecords );

	  DB->ApplyUpdatesSensor();

	}
	else
	{
	  SR = NULL;
	}

	DB->ClearSensorFilter();

	// Read PowerSupplys

	DB->SetPSFilter(ZoneRec);
	if( DB->FindFirstPS())
	{
	  do
	  {
		PSR = new CIISPowerSupplyRec;
		MoreRecords = DB->GetPSRec( PSR, true );

		PSR->PSCanAdr = 0;
		PSR->PSStatus = 1;
		PSR->PSConnected = false;
		PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;  // Needs Apply Update

		PSR->PSAlarmStatusU = 0;
		PSR->PSAlarmStatusI = 0;

		if( PSR->PSType == Camur_II_PowerInterface ) PS = new TCIISPowerSupply_PI(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else if( PSR->PSType == Camur_II_FixVolt ) PS = new TCIISPowerSupply_FixVolt(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else if( PSR->PSType == Camur_II_FixVolt3A ) PS = new TCIISPowerSupply_FixVolt3A(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else if( PSR->PSType == Camur_II_FixVolt01A ) PS = new TCIISPowerSupply_FixVolt01A(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else if( PSR->PSType == Camur_II_FixVolt1A ) PS = new TCIISPowerSupply_FixVolt1A(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else if( PSR->PSType == Camur_II_FixVoltHV1A ) PS = new TCIISPowerSupply_FixVoltHV1A(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		else PS = new TCIISPowerSupply(SetDB, SetCIISBusInt, SetDebugWin, PSR, this );
		ChildList->Add( PS );
	  } while ( MoreRecords );

	  DB->ApplyUpdatesPS();

	}
	else
	{
	  PSR = NULL;
	}

	DB->ClearPSFilter();


	// Read Alarms

	DB->SetAlarmFilter(ZoneRec);
	if( DB->FindFirstAlarm())
	{
	  do
	  {
		AR = new CIISAlarmRec;
		MoreRecords = DB->GetAlarmRec( AR, true );

		AR->AlarmCanAdr = 0;
		AR->AlarmStatus = 1;
		AR->AlarmConnected = false;
		AR->ZoneCanAdr = ZoneRec->ZoneCanAdr;  // Needs Apply Update
		Alarm = new TCIISAlarm(SetDB, SetCIISBusInt, SetDebugWin, AR, this );
		ChildList->Add( Alarm );
	  } while ( MoreRecords );

	  DB->ApplyUpdatesAlarm();

	}
	else
	{
		AR = NULL;
	}

	DB->ClearAlarmFilter();
  }

  RR = new CIISRecordingRec;
  if( DB->LocateRecordingNo( ZoneRec->RecNo ) ) DB->GetRecordingRec( RR, false );

}

__fastcall TCIISZone::~TCIISZone()
{
	TCIISObj *CIISObj;
	TCIISSensor *Sensor;
	TCIISPowerSupply *PS;
	TCIISAlarm* Alarm;
	TCIISNodeIni *NI;

	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
		Sensor = (TCIISSensor*)CIISObj;
		delete Sensor;
		ChildList->Delete(i);
	}
	else if( CIISObj->CIISObjIs( CIISAlarm ) )
	{
		Alarm = (TCIISAlarm*)CIISObj;
		delete Alarm;
		ChildList->Delete(i);
	}
	else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
	{
		PS = (TCIISPowerSupply*)CIISObj;
		delete PS;
		ChildList->Delete(i);
	}
	else if( CIISObj->CIISObjIs( CIISNode ) )
	{
		NI = (TCIISNodeIni*)CIISObj;
		delete NI;
		ChildList->Delete(i);
	}
	}
	delete RR;
	delete ZoneRec;
}

void __fastcall TCIISZone::CancelDecay()
{
	CIISBusInt->RequestDecayCancel( ZoneRec->ZoneCanAdr );
}

#pragma argsused
bool __fastcall TCIISZone::ResetCIISBus()
{
	return false;
}

#pragma argsused
void __fastcall TCIISZone::OnSysClockTick( TDateTime TickTime )
{

}

void __fastcall TCIISZone::PrepareShutdown()
{

}

#pragma argsused
void __fastcall TCIISZone::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
  TCIISObj *Obj;

	#if DebugMsg == 1
	Debug( "Zone: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	  {
		WriteRec( P, O );
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISAppend:
	if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	{
	  P->CommandMode = CIISAddZone;
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	  P->CommandMode = CIISCmdReady;
	}

	break;

	case CIISDelete:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase )
	  {
		if( ChildList->Count == 0 && ZoneRec->ZoneNo != 1 &&  DB->DeleteZoneRec( ZoneRec ) )
		{
			DB->ApplyUpdatesZone();
		  P_Prj->Log( ClientCom, CIIEventCode_ZoneTabChanged, CIIEventLevel_High, "", 0, 0 );
		  P->CommandMode = CIISDeleteZone;
		  P->CIISObj = this;
		  O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_DeleteError;
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;


	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}

#pragma argsused
void __fastcall TCIISZone::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Zone: ParseCommand_End" );
	#endif

  switch( P->CommandMode )
  {
	case CIISSensorMoved:
	RemoveSensor( P );
	break;

	case CIISPSMoved:
	RemovePS( P );
	break;

	case CIISDeleteSensor:
	DeleteSensor( (TCIISSensor*) P->CIISObj );
	break;

	case CIISDeletePS:
	DeletePS( (TCIISPowerSupply*) P->CIISObj );
	break;

	case CIISDeleteAlarm:
	DeleteAlarm( (TCIISAlarm*) P->CIISObj );
	break;

	default:
	break;
  }
}

bool __fastcall TCIISZone::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == ZoneRec->ZoneNo;
}

void __fastcall TCIISZone::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
		if( P->ArgInc( 200 ) )
		{
			if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
			else if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
		}
  }

  if( P->CommandMode == CIISRecSearch )
	{
		if( ThisRec( P ) )
		{
			P->CommandMode = CIISRecAdd;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_RecNotFound;
  }

	else if( P->CommandMode == CIISRecAdd )
	{
		O->MessageData = O->MessageData +
		"1=" + IntToStr( ZoneRec->ZoneNo ) + "\r\n" +
		"2=" + ZoneRec->ZoneName + "\r\n" +
		"3=" + CIISFloatToStr( ZoneRec->AnodeArea ) + "\r\n" +
		"4=" + CIISFloatToStr( ZoneRec->CathodeArea ) + "\r\n" +
		"5=" + ZoneRec->Comment + "\r\n" +
		"6=" + IntToStr( ZoneRec->ZoneSampInterval ) + "\r\n" +
		"7=" + IntToStr( ZoneRec->RecType ) + "\r\n" +
		"8=" + IntToStr( ZoneRec->RecNo ) + "\r\n" +
		"9=" + IntToStr( ZoneRec->ZoneCanAdr ) + "\r\n" +
		"10=" + IntToStr( ZoneRec->ZoneAlarmStatus ) + "\r\n" +
		"11=" + IntToStr( ZoneRec->ZoneScheduleStatus ) + "\r\n" +
		"12=" + CIISBoolToStr( ZoneRec->IncludeInSchedule ) + "\r\n" +
		"13=" + IntToStr( ZoneRec->RecTypeBeforeSchedule ) + "\r\n" +
		"14=" + CIISBoolToStr( ZoneRec->PSOffOnAlarm ) + "\r\n" +
		"15=" + CIISBoolToStr( ZoneRec->PSOffHold ) + "\r\n" +
    "16=" + CIISBoolToStr( ZoneRec->uCtrl ) + "\r\n" +
		"50=" + ZoneRec->CtrlName + "\r\n";

		O->MessageCode = CIISMsg_Ok;

		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
	else if( P->CommandMode == CIISRecCount )
  {
		O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetZoneRecCount() ) + "\r\n";;
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
	}
  else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISZone::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  String Command;

	if( P->ArgInc(200) )
	{
		Command = P->GetArg(200);
		if( ( Command == "Stop" ) || ( Command == "0" ) )
		{
			 ForceStopRec = true;
			 if( !StopRecording() ) O->MessageCode = 206;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( ( Command == "Start Monitor" ) || ( Command == "1" ) )
		{
			 if( !StartMonitor() ) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( ( Command == "Start LPR" ) || ( Command == "2" ))
		{
			 if( !StartLPR() ) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( ( Command == "Start Decay" ) || ( Command == "3" ) )
		{
			 if( !StartDecay()) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( Command == "4" )
		{
			 if( !StartMonitorExtended() )
			 {
				 O->MessageCode = 205;
			 }
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( Command == "10" )
		{
			 if( !StartCTNonStat()) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( Command == "11" )
		{
			 if( !StartCTStat()) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( Command == "12" )
		{
			 if( !StartRExt()) O->MessageCode = 205;
			 else O->MessageCode = CIISMsg_Ok;
		}
		else if( Command == "Update LastValue" )
		{
			this->UpdateLastValues();// NetInt->UpdateLastValue( ZoneNo, 0 );
			O->MessageCode = CIISMsg_Ok;
		}
	}  // ArgInc(200)
	else if( DB->LocateZoneRec( ZoneRec ) )
	{
		if( P->ArgInc(2) ) ZoneRec->ZoneName = P->GetArg(2);
		if( P->ArgInc(3) ) ZoneRec->AnodeArea = CIISStrToFloat(P->GetArg(3));
		if( P->ArgInc(4) ) ZoneRec->CathodeArea = CIISStrToFloat(P->GetArg(4));
		if( P->ArgInc(5) ) ZoneRec->Comment = P->GetArg(5);
		if( P->ArgInc(6) ) ZoneRec->ZoneSampInterval = StrToInt(P->GetArg(6));
		if( P->ArgInc(8) ) ZoneRec->IncludeInSchedule = CIISStrToBool(P->GetArg(8));
		if( P->ArgInc(9) ) ZoneRec->PSOffOnAlarm = CIISStrToBool(P->GetArg(9));
		//if( P->ArgInc(50) ) ZoneRec->CtrlName = P->GetArg(50);

		if( DB->SetZoneRec( ZoneRec ) )
		{
			DB->ApplyUpdatesZone();
			P_Prj->Log( ClientCom, CIIEventCode_ZoneTabChanged, CIIEventLevel_High, "", 0, 0 );
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;

		if( ZoneRec->uCtrl && P->ArgInc(6) ) InstalledPS->PSIniState();

	}
  else
  {
		O->MessageCode = CIISMsg_RecNotFound;
	}
  P->CommandMode = CIISCmdReady;
}

void __fastcall TCIISZone::RemoveSensor( TCamurPacket *P )
{
  TCIISObj *CIISObj;
  TCIISSensor *SMoved;

  SMoved = (TCIISSensor*) P->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  if( SMoved == CIISObj )
	  {
		ChildList->Delete(i);
		break;
	  }
	}
  }
}

void __fastcall TCIISZone::RemovePS( TCamurPacket *P )
{
  TCIISObj *CIISObj;
  TCIISPowerSupply *PSMoved;

  PSMoved = (TCIISPowerSupply*) P->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISPowerSupply ) )
	{
	  if( PSMoved == CIISObj )
	  {
		ChildList->Delete(i);
		break;
	  }
	}
  }
}

void __fastcall TCIISZone::RemoveNI( TCIISBusPacket *BPIn )
{
  TCIISObj *CIISObj;
  TCIISNodeIni *NIMoved;

  NIMoved = (TCIISNodeIni*) BPIn->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISNode ) )
		{
			if( NIMoved == CIISObj )
			{
				ChildList->Delete(i);
				break;
			}
		}
	}
}


void __fastcall TCIISZone::AddChild( TCIISObj *Child )
{
	TCIISSensor *Sensor;
	TCIISAlarm *Alarm;
	TCIISPowerSupply *PS;
  TCIISNodeIni *NI;

  if( Child->CIISObjIs( CIISSensors ) )
  {
		Sensor = (TCIISSensor*)Child;
		Sensor->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		DB->ApplyUpdatesSensor();
	}
	else if ( Child->CIISObjIs( CIISAlarm ) )
	{
		Alarm = (TCIISAlarm*)Child;
		Alarm->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		DB->ApplyUpdatesAlarm();
	}
	else if ( Child->CIISObjIs( CIISPowerSupply ) )
	{
		PS = (TCIISPowerSupply*)Child;
		PS->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		DB->ApplyUpdatesPS();
	}
  else if ( Child->CIISObjIs( CIISNode ) )
  {
		NI = (TCIISNodeIni*)Child;
		NI->ZoneCanAdr = ZoneRec->ZoneCanAdr;
  }

  ChildList->Add( Child );

  P_Prj->AlarmStatusChanged = true;
}

bool __fastcall TCIISZone::SetCtrlName( String CtrlName )
{
  bool result;
  String CurrentName;

  result = false;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		CurrentName = ZoneRec->CtrlName;
		ZoneRec->CtrlName = CtrlName;
		if( DB->SetZoneRec( ZoneRec ) )
		{
			result = true;
			DB->ApplyUpdatesZone();
		}
		else ZoneRec->CtrlName = CurrentName;
	}
	return result;
}

void __fastcall TCIISZone::DeleteSensor( TCIISSensor *Sensor )
{
	TCIISObj *CIISObj;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			if( Sensor == CIISObj )
			{
			ChildList->Delete(i);
			delete Sensor;
			break;
			}
		}
	}
}

void __fastcall TCIISZone::DeleteAlarm( TCIISAlarm *Alarm )
{
	TCIISObj *CIISObj;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			if( Alarm == CIISObj )
			{
			ChildList->Delete(i);
			delete Alarm;
			break;
			}
		}
	}
}

void __fastcall TCIISZone::DeletePS( TCIISPowerSupply *PS )
{
  TCIISObj *CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			if( PS == CIISObj )
			{
			ChildList->Delete(i);
			delete PS;
			break;
			}
		}
	}
}

void __fastcall TCIISZone::DeleteNI( TCIISNodeIni *NI )
{
  TCIISObj *CIISObj;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISNode ) )
		{
			if( NI == CIISObj )
			{
			ChildList->Delete(i);
			delete NI;
			break;
			}
		}
  }
}

#pragma argsused
void __fastcall TCIISZone::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}

#pragma argsused
void __fastcall TCIISZone::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISZone::UpdateNode( TCIISBusPacket *BPIn )
{
  TCIISSensor *Sensor;
  TCIISAlarm *Alarm;
  TCIISPowerSupply *PS;
  TCIISNodeIni *NI;
  CIISSensorRec *SR;
  CIISAlarmRec *AR;
  CIISPowerSupplyRec *PSR;
  CIISNodeRec *NR;
  TCIISObj *UpdateObj;
	TCIISBIChannel *BICh;
  int32_t IniErr;

  UpdateObj = (TCIISObj*)BPIn->CIISObj;

  if( UpdateObj->CIISObjIs( CIISSensors ) )
  {
	Sensor = (TCIISSensor*)UpdateObj;
	SR = new CIISSensorRec;
	SR->SensorSerialNo = Sensor->SerialNo;
	DB->LocateSensorRec( SR );
	DB->GetSensorRec( SR, false );

	SR->SensorType = Sensor->SensorType;
	SR->SensorCanAdr = Sensor->CanAdr;
	BICh = Sensor->CIISBICh;
	IniErr = Sensor->IniErrorCount;
	SR->ZoneNo = ZoneRec->ZoneNo;
	SR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	DeleteSensor( Sensor );


	if( SR->SensorType == Camur_II_LPR ) Sensor = new TCIISSensor_LPR(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_IO ) Sensor = new TCIISSensor_IO(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_P ) Sensor = new TCIISSensor_P(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_P4 ) Sensor = new TCIISSensor_P4(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_LPRExt ) Sensor = new TCIISSensor_LPRExt(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_HUM ) Sensor = new TCIISSensor_HUM(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_HiRes ) Sensor = new TCIISSensor_HiRes(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_R ) Sensor = new TCIISSensor_R(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_Wenner ) Sensor = new TCIISSensor_Wenner(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_ZRA) Sensor = new TCIISSensor_ZRA(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_CrackWatch) Sensor = new TCIISSensor_CrackWatch(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_PT ) Sensor = new TCIISSensor_PT(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_CT) Sensor = new TCIISSensor_CT(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_MRE) Sensor = new TCIISSensor_MRE(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_CW) Sensor = new TCIISSensor_CW(DB, CIISBusInt, DW, SR, this );
	else if( SR->SensorType == Camur_II_Ladder) Sensor = new TCIISSensor_Ladder(DB, CIISBusInt, DW, SR, this );
	else  Sensor = new TCIISSensor(DB, CIISBusInt, DW, SR, this );

	Sensor->CIISBICh = BICh;
	Sensor->IniErrorCount = IniErr;
	ChildList->Add( Sensor );
	Sensor->IniRunning = true;
  }
  else if( UpdateObj->CIISObjIs( CIISAlarm ) )
  {
	Alarm = (TCIISAlarm*)UpdateObj;
	AR = new CIISAlarmRec;
	DB->LocateAlarmRec( AR );
	DB->GetAlarmRec( AR, false );

	AR->AlarmType = Alarm->AlarmType;
	AR->AlarmSerialNo = Alarm->SerialNo;
	AR->AlarmCanAdr = Alarm->CanAdr;
	BICh = Alarm->CIISBICh;
	IniErr = Alarm->IniErrorCount;
	AR->ZoneNo = ZoneRec->ZoneNo;
	AR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	DeleteAlarm( Alarm );

	Alarm = new TCIISAlarm(DB, CIISBusInt, DW, AR, this);
	Alarm->CIISBICh = BICh;
	Alarm->IniErrorCount = IniErr;
	ChildList->Add( Alarm );
	Alarm->IniRunning = true;
  }
  else if( UpdateObj->CIISObjIs( CIISPowerSupply ) )
  {
	PS = (TCIISPowerSupply*)UpdateObj;
	PSR = new CIISPowerSupplyRec;
	DB->LocatePSRec( PSR );
	DB->GetPSRec( PSR, false );

	PSR->PSType = PS->PSType;
	PSR->PSSerialNo = PS->SerialNo;
	PSR->PSCanAdr = PS->CanAdr;
	BICh = PS->CIISBICh;
	IniErr = PS->IniErrorCount;
	PSR->ZoneNo = ZoneRec->ZoneNo;
	PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	DeletePS( PS );

	if( PSR->PSType == Camur_II_PowerInterface ) PS = new TCIISPowerSupply_PI(DB, CIISBusInt, DW, PSR, this );
	else if( PSR->PSType == Camur_II_FixVolt ) PS = new TCIISPowerSupply_FixVolt(DB, CIISBusInt, DW, PSR, this );
	else if( PSR->PSType == Camur_II_FixVolt3A ) PS = new TCIISPowerSupply_FixVolt3A(DB, CIISBusInt, DW, PSR, this );
	else if( PSR->PSType == Camur_II_FixVolt01A ) PS = new TCIISPowerSupply_FixVolt01A(DB, CIISBusInt, DW, PSR, this );
	else if( PSR->PSType == Camur_II_FixVolt1A ) PS = new TCIISPowerSupply_FixVolt1A(DB, CIISBusInt, DW, PSR, this );
	else if( PSR->PSType == Camur_II_FixVoltHV1A ) PS = new TCIISPowerSupply_FixVoltHV1A(DB, CIISBusInt, DW, PSR, this );
	else PS = new TCIISPowerSupply(DB, CIISBusInt, DW, PSR, this );

	PS->CIISBICh = BICh;
	PS->IniErrorCount = IniErr;
	ChildList->Add( PS );
	PS->IniRunning = true;
  }
  else if( UpdateObj->CIISObjIs( CIISNode ) )
  {
	NI = (TCIISNodeIni*)UpdateObj;
	if( NI->NodCapability == Camur_II_LPR ||
		NI->NodCapability == Camur_II_IO ||
		NI->NodCapability == Camur_II_P ||
		NI->NodCapability == Camur_II_P4 ||
		NI->NodCapability == Camur_II_LPRExt ||
		NI->NodCapability == Camur_II_HUM ||
		NI->NodCapability == Camur_II_HiRes ||
		NI->NodCapability == Camur_II_R ||
		NI->NodCapability == Camur_II_Wenner ||
		NI->NodCapability == Camur_II_ZRA ||
		NI->NodCapability == Camur_II_CrackWatch ||
		NI->NodCapability == Camur_II_PT ||
		NI->NodCapability == Camur_II_CT ||
		NI->NodCapability == Camur_II_MRE ||
		NI->NodCapability == Camur_II_CW ||
		NI->NodCapability == Camur_II_Ladder )
	{
	  SR = new CIISSensorRec;
	  SR->SensorSerialNo = NI->SerialNo;
	  SR->SensorCanAdr = NI->CanAdr;
		BICh = NI->CIISBICh;
	  IniErr = NI->IniErrorCount;
	  SR->SensorType = NI->NodCapability;
	  SR->ZoneNo = ZoneRec->ZoneNo;
	  SR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	  DB->AppendSensorRec( SR ); //DB->ApplyUpdatesSensor();

	  DeleteNI( NI );

	  if( SR->SensorType == Camur_II_LPR ) Sensor = new TCIISSensor_LPR(DB, CIISBusInt, DW, SR, this );
	  else if( SR->SensorType == Camur_II_IO ) Sensor = new TCIISSensor_IO(DB, CIISBusInt, DW, SR, this );
	  else if( SR->SensorType == Camur_II_P ) Sensor = new TCIISSensor_P(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_P4 ) Sensor = new TCIISSensor_P4(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_LPRExt ) Sensor = new TCIISSensor_LPRExt(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_HUM ) Sensor = new TCIISSensor_HUM(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_HiRes ) Sensor = new TCIISSensor_HiRes(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_R ) Sensor = new TCIISSensor_R(DB, CIISBusInt, DW, SR, this );
    else if( SR->SensorType == Camur_II_Wenner ) Sensor = new TCIISSensor_Wenner(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_ZRA) Sensor = new TCIISSensor_ZRA(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_CrackWatch) Sensor = new TCIISSensor_CrackWatch(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_PT ) Sensor = new TCIISSensor_PT(DB, CIISBusInt, DW, SR, this );
		else if( SR->SensorType == Camur_II_CT) Sensor = new TCIISSensor_CT(DB, CIISBusInt, DW, SR, this );
	  else if( SR->SensorType == Camur_II_MRE) Sensor = new TCIISSensor_MRE(DB, CIISBusInt, DW, SR, this );
	  else if( SR->SensorType == Camur_II_CW) Sensor = new TCIISSensor_CW(DB, CIISBusInt, DW, SR, this );
	  else if( SR->SensorType == Camur_II_Ladder) Sensor = new TCIISSensor_Ladder(DB, CIISBusInt, DW, SR, this );
	  else  Sensor = new TCIISSensor(DB, CIISBusInt, DW, SR, this );

		Sensor->SetDefaultValues();

		Sensor->CIISBICh = BICh;
	  Sensor->IniErrorCount = IniErr;
	  ChildList->Add( Sensor );
	  Sensor->IniRunning = true;
	}
	else if( NI->NodCapability == Camur_II_PowerInterface ||
			 NI->NodCapability == Camur_II_FixVolt ||
			 NI->NodCapability == Camur_II_FixVolt3A ||
			 NI->NodCapability == Camur_II_FixVolt01A ||
			 NI->NodCapability == Camur_II_FixVolt1A ||
			 NI->NodCapability == Camur_II_FixVoltHV1A )
	{
	  PSR = new CIISPowerSupplyRec;
	  PSR->PSSerialNo = NI->SerialNo;
	  PSR->PSCanAdr = NI->CanAdr;
		BICh = NI->CIISBICh;
	  IniErr = NI->IniErrorCount;
	  PSR->PSType = NI->NodCapability;
	  PSR->ZoneNo = ZoneRec->ZoneNo;
	  PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	  DB->AppendPSRec( PSR ); //DB->ApplyUpdatesPS();

	  DeleteNI( NI );

	  if( PSR->PSType == Camur_II_PowerInterface ) PS = new TCIISPowerSupply_PI(DB, CIISBusInt, DW, PSR, this );
	  else if( PSR->PSType == Camur_II_FixVolt ) PS = new TCIISPowerSupply_FixVolt(DB, CIISBusInt, DW, PSR, this );
	  else if( PSR->PSType == Camur_II_FixVolt3A ) PS = new TCIISPowerSupply_FixVolt3A(DB, CIISBusInt, DW, PSR, this );
	  else if( PSR->PSType == Camur_II_FixVolt01A ) PS = new TCIISPowerSupply_FixVolt01A(DB, CIISBusInt, DW, PSR, this );
	  else if( PSR->PSType == Camur_II_FixVolt1A ) PS = new TCIISPowerSupply_FixVolt1A(DB, CIISBusInt, DW, PSR, this );
	  else if( PSR->PSType == Camur_II_FixVoltHV1A ) PS = new TCIISPowerSupply_FixVoltHV1A(DB, CIISBusInt, DW, PSR, this );
	  else PS = new TCIISPowerSupply(DB, CIISBusInt, DW, PSR, this );

		PS->SetDefaultValues();

		PS->CIISBICh = BICh;
	  PS->IniErrorCount = IniErr;
	  ChildList->Add( PS );
	  PS->IniRunning = true;
	}
	else if ( NI->NodCapability == Camur_II_Alarm)
	{
	  AR = new CIISAlarmRec;
	  AR->AlarmSerialNo = NI->SerialNo;
	  AR->AlarmCanAdr = NI->CanAdr;
		BICh = NI->CIISBICh;
	  IniErr = NI->IniErrorCount;
	  AR->AlarmType = NI->NodCapability;
	  AR->ZoneNo = ZoneRec->ZoneNo;
	  AR->ZoneCanAdr = ZoneRec->ZoneCanAdr;

	  DB->AppendAlarmRec( AR ); //DB->ApplyUpdatesAlarm();

	  DeleteNI( NI );

	  Alarm = new TCIISAlarm(DB, CIISBusInt, DW, AR, this );
		Alarm->SetDefaultValues();
		Alarm->CIISBICh = BICh;
	  Alarm->IniErrorCount = IniErr;
	  ChildList->Add( Alarm );
	  Alarm->IniRunning = true;
	}
  }
}

bool __fastcall TCIISZone::StartMonitor()
{
	return false;
}

bool __fastcall TCIISZone::StartDecay()
{
	return false;

}

bool __fastcall TCIISZone::StartLPR()
{
	return false;

}

bool __fastcall TCIISZone::StartMonitorExtended()
{
	return false;

}

bool __fastcall TCIISZone::StartScheduledValues()
{
	return false;

}

bool __fastcall TCIISZone::StartCTNonStat()
{
	return false;

}

bool __fastcall TCIISZone::StartCTStat()
{
	return false;

}

bool __fastcall TCIISZone::StartRExt()
{
	return false;

}

void __fastcall TCIISZone::RestartZone()
{
	return;

}

bool __fastcall TCIISZone::StopRecording()
{
	return false;

}

void __fastcall TCIISZone::StartSchedule()
{
	return;

}

bool __fastcall TCIISZone::NodeIniReady()
{
  TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISAlarm *Alarm;
	TCIISPowerSupply *PS;
	TCIISNodeIni *NI;
  bool NIR;

  NIR = true;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			NIR = NIR && S->NodeIniReady();
		}
		else if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			Alarm = (TCIISAlarm*)CIISObj;
			NIR = NIR && Alarm->NodeIniReady();
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			NIR = NIR && PS->NodeIniReady();
		}
		else if( CIISObj->CIISObjIs( CIISNode ) )
		{
			NI =(TCIISNodeIni*)CIISObj;
			NIR = NIR && NI->NodeIniReady();
    }

		if( !NIR ) break;
  }
  return NIR;
}

int32_t __fastcall TCIISZone::GetNodeCount()
{
      return 0;
}

int32_t __fastcall TCIISZone::GetIniErrorCount()
{
  TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISAlarm *A;
	TCIISPowerSupply *PS;
  int32_t EC;

  EC = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			EC = EC + S->IniErrorCount;
		}
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			A = (TCIISAlarm*)CIISObj;
			EC = EC + A->IniErrorCount;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			EC = EC + PS->IniErrorCount;
		}
  }
  return EC;
}

void __fastcall TCIISZone::UpdateLastValues()
{

}


bool __fastcall TCIISZone::UpdateAlarmStatus()
{
	TCIISObj *CIISObj;
	TCIISSensor *Sensor;
	TCIISPowerSupply *PS;
	//TCIISAlarm *Alarm;
	bool AlarmStatus, AlarmOnNode;

	AlarmStatus = false;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			Sensor = (TCIISSensor*)CIISObj;
			AlarmOnNode = Sensor->AlarmStatus;
			AlarmStatus = AlarmStatus || AlarmOnNode;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			AlarmOnNode = PS->AlarmStatus;
			AlarmStatus = AlarmStatus || AlarmOnNode;
		}
/*
			else if( CIISObj->CIISObjIs( CIISAlarm ) )
			{
				Alarm = (TCIISAlarm*)CIISObj;
				AlarmOnNode = Alarm->AlarmStatus;
				AlarmStatus = AlarmStatus || AlarmOnNode;
			}
*/
	}

	if( AlarmStatus )
	{
		ZoneRec->ZoneAlarmStatus = 1;
		if( ZoneRec->PSOffOnAlarm )
		{
			ZoneRec->PSOffHold = true;
			PSOnAlarm();
		}
	}
  else ZoneRec->ZoneAlarmStatus = 0;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
  }
  return AlarmStatus;
}

void __fastcall TCIISZone::ResetCANAlarm()
{
  TCIISObj *CIISObj;
	TCIISSensor *Sensor;
	TCIISPowerSupply *PS;
	TCIISAlarm *Alarm;
	bool AlarmStatus, AlarmOnNode;

	AlarmStatus = false;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			Sensor = (TCIISSensor*)CIISObj;
			Sensor->ResetCANAlarm();
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->ResetCANAlarm();
		}
		else if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			Alarm = (TCIISAlarm*)CIISObj;
			Alarm->ResetCANAlarm();
		}

	}
}

void __fastcall TCIISZone::PSOnAlarm()
{

}

// Setup

	TCIISSensor* __fastcall TCIISZone::GetSensor( int32_t Sensor_No )
	{
		TCIISObj *CIISObj;
		TCIISSensor *Sensor;
		int32_t SensorCnt;

		SensorCnt = 0;
		Sensor = NULL;
		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISSensors ) )
			{
				SensorCnt++;
				if (SensorCnt == Sensor_No)
				{
					Sensor = (TCIISSensor*) CIISObj;
					break;
				}
			}
		}
		return Sensor;
	}

/*
  TCIISBIChannel* __fastcall TCIISZone::GetBIChannel()
  {
		return BICh;
	}
*/


	TCIISPowerSupply* __fastcall TCIISZone::GetPS( int32_t PS_No )
	{
		TCIISObj *CIISObj;
		TCIISPowerSupply *PS;
		int32_t PSCnt;

		PSCnt = 0;
		PS = NULL;
		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISPowerSupply ) )
			{
				PSCnt++;
				if (PSCnt == PS_No)
				{
					PS = (TCIISPowerSupply*) CIISObj;
					break;
				}
			}
		}
		return PS;
  }

	// Zone_online

__fastcall TCIISZone_Online::TCIISZone_Online(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
																TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
																TObject *SetCIISParent, bool NewZone )
						:TCIISZone( SetDB, SetCIISBusInt, SetDebugWin, SetZoneRec, SetCIISParent, NewZone )
{

	SMRunning_Schedule = false;
	Sample_MRE = false;
	Sample_CW = false;
	Sample_Ladder = false;
	Sample_PSTemp = false;
	Sample_CTx1Temp = false;
	SMRunning_MRE = false;
	SMRunning_LPR_CW = false;
	SMRunning_LPR_Ladder = false;
	SMRunning_LPR = false;
	SMRunning_PSTemp = false;
	SMRunning_CTx1Temp = false;
	UpdateLastValueRunning = false;
	MonitorExtended = false;
	MonitorFirstSample = false;

}

__fastcall TCIISZone_Online::~TCIISZone_Online()
{

}

bool __fastcall TCIISZone_Online::ResetCIISBus()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
	TCIISPowerSupply *PS;
	TCIISAlarm *Alarm;

	ZoneRec->ZoneCanAdr = CIISBusInt->GetZonCanAdr();
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	}

	// Save status for recording and schedule for restart functions.

	RestartRecType = ZoneRec->RecType;
	ZoneRec->RecType = RT_StandBy;
	RestartRecNo = ZoneRec->RecNo;
	RestartScheduleStatus = ZoneRec->ZoneScheduleStatus;

	SMRunning_RestartZone = false;
	SMRunning_Schedule = false;

	Sample_MRE = false;
	Sample_CW = false;
	Sample_Ladder = false;
	Sample_PSTemp = false;
	Sample_CTx1Temp = false;
	SMRunning_MRE = false;
	SMRunning_LPR_CW = false;
	SMRunning_LPR_Ladder = false;
	SMRunning_Ladder = false;
	SMRunning_PSTemp = false;
	SMRunning_CTx1Temp = false;
  OnSlowClockTick = 200;

	UpdateLastValueRunning = false;
	ForceStopRec = false;

  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->ResetCIISBus();
			S->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
		else if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			Alarm = (TCIISAlarm*)CIISObj;
			Alarm->ResetCIISBus();
			Alarm->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->ResetCIISBus();
			PS->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
	}

	DB->ApplyUpdatesSensor();
	DB->ApplyUpdatesPS();

  return true;
}

void __fastcall TCIISZone_Online::RestartZone()
{
  if( DB->LocateRecordingNo( RestartRecNo ) ) DB->GetRecordingRec( RR, false );
  else
  {
		PSIniState();
		return;
	}


  State_RestartZone = 10;
  SMRunning_RestartZone = true;
  TRestart = 0;
  TNextEvent_Restart = 0;

  if( RestartScheduleStatus == 3 ) RestartScheduleStatus = 2;
  else if( RestartScheduleStatus == 5 ) RestartScheduleStatus = 4;

  TDecayDurationLeft = RR->DecayDuration - (int32_t)( (double)( Now() - RR->RecStart ) / OneSecond );
  TDecayDurationLeft = max( 0, TDecayDurationLeft );
}

void __fastcall TCIISZone_Online::SM_RestartZone()
{
	if( TRestart >= TNextEvent_Restart )
	{
		switch( State_RestartZone )
		{
			case 10:
			if( RestartScheduleStatus > 0 )
			{
				if( TDecayDurationLeft > 0 )
				{
					switch( RestartScheduleStatus )
					{
						case 1:
						if( P_Ctrl->ScheduleDecay ) RestartScheduleStatus = 2;
						else if( P_Ctrl->ScheduleLPR ) RestartScheduleStatus = 4;
						else  RestartScheduleStatus = 6;
						#if DebugCIISObj == 1
						Debug( "Scheduling restarted on Zone " + IntToStr( ZoneRec->ZoneNo ) );
						#endif
						break;

						case 2:
						ResumeDecay( RestartRecNo, TDecayDurationLeft );
						ZoneRec->ZoneScheduleStatus = 3;
						SMRunning_Schedule = true;
						State_RestartZone = 100;
						#if DebugCIISObj == 1
						Debug( "Rescheduled decay on Zone " + IntToStr( ZoneRec->ZoneNo ) );
						#endif
						break;

						case 3:
						break;

						case 4:
						StartLPR();
						ZoneRec->ZoneScheduleStatus = 5;
						SMRunning_Schedule = true;
						State_RestartZone = 100;
						#if DebugCIISObj == 1
						Debug( "Rescheduled LPR on Zone " + IntToStr( ZoneRec->ZoneNo ) );
						#endif
						break;

						case 5:
						break;

						case 6:
						ZoneRec->ZoneScheduleStatus = 6;
						SMRunning_Schedule = true;
						State_RestartZone = 100;
						#if DebugCIISObj == 1
						Debug( "Rescheduling ready on Zone " + IntToStr( ZoneRec->ZoneNo ) );
						#endif
						break;
					}
				}
				else
				{
					ZoneRec->ZoneScheduleStatus = 0;
					SMRunning_Schedule = false;
					if(ZoneRec->RecTypeBeforeSchedule == RT_Monitor ) StartMonitor();
					State_RestartZone = 30;
				}
				TNextEvent_Restart += 2000;
			}
			else
			{
				State_RestartZone = 20;
				TNextEvent_Restart += 1;
			}
			break;

			case 20:
			if( RestartRecType == RT_StandBy )
			{
				State_RestartZone = 30;
				TNextEvent_Restart += 1;
			}
			else if( RestartRecType == RT_Monitor )
			{
				StartMonitor();
				State_RestartZone = 30;
				TNextEvent_Restart += 2000;
			}
			else if( RestartRecType == RT_MonitorExt )
			{
				StartMonitorExtended();
				State_RestartZone = 30;
				TNextEvent_Restart += 2000;
			}
			else if( RestartRecType == RT_Decay )
			{
				ResumeDecay( RestartRecNo, TDecayDurationLeft );
				State_RestartZone = 100;
				TNextEvent_Restart += 1;
			}
			else if( RestartRecType == RT_LPR )
			{
				State_RestartZone = 30;
				TNextEvent_Restart += 1;
			}
			break;

			case 30:
			PSIniState();
			State_RestartZone = 100;
			TNextEvent_Restart += 10;
			break;

			case 100:
			SMRunning_RestartZone = false;;
			break;
		}
	}
	TRestart += SysClock;
}

void __fastcall TCIISZone_Online::PSIniState()
{
	TCIISObj *CIISObj;
  TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->PSIniState();
		}
	}
}

#pragma argsused
void __fastcall TCIISZone_Online::OnSysClockTick( TDateTime TickTime )
{

	if( SMRunning_RestartZone ) SM_RestartZone();

	if(( ZoneRec->RecType == RT_Monitor ) || MonitorExtended || ( ZoneRec->RecType == RT_ScheduledValues )) SM_Monitor();
	else if( ZoneRec->RecType == RT_Decay ) SM_Decay();
	else if( ZoneRec->RecType == RT_CTNonStat ) SM_CTNonStat();
	else if( ZoneRec->RecType == RT_CTStat ) SM_CTStat();
	else if( ZoneRec->RecType == RT_RExt ) SM_RExt();
	else if( SMRunning_LPR_CW ) SM_LPR_CW();
	else if( SMRunning_LPR_Ladder ) SM_LPR_Ladder();

	if( SMRunning_CW ) SM_Sample_CW();
	if( SMRunning_MRE ) SM_Sample_MRE();
	if( SMRunning_Ladder ) SM_Sample_Ladder();
	if( SMRunning_PSTemp ) SM_Sample_PSTemp();
	if( SMRunning_CTx1Temp ) SM_Sample_CTx1Temp();
	if( SMRunning_LPR ) SM_LPR();

	if( UpdateLastValueRunning ) SM_UpdateLastValue();

	OnSlowClockTick--;
	if( OnSlowClockTick < 0 )
	{
		OnSlowClockTick = 20;
		if( SMRunning_Schedule ) SM_Schedule();
	}
}

void __fastcall TCIISZone_Online::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
  switch (BPIn->MessageCommand)
  {
	case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )
	  BPIn->RecordingNo =  ZoneRec->RecNo;
	  BPIn->RecType =  ZoneRec->RecType;
	  BPIn->RequestedTag =  ExpectedTag;
	  BPIn->SampleTimeStamp = SampleTimeStamp;
	break;

	case MSG_TX_SAMPLEEXTCH: // Reqest extended Sample ( Monitor, LastValue or Decay )
	  BPIn->RecordingNo =  ZoneRec->RecNo;
	  BPIn->RecType =  ZoneRec->RecType;
	  BPIn->RequestedTag =  ExpectedTag;
	  BPIn->SampleTimeStamp = SampleTimeStamp;
	break;

	case MSG_TX_PSTEMP: // Reqest extended Sample ( Monitor, MonitorExt )
		BPIn->RecordingNo =  ZoneRec->RecNo;
	  BPIn->RecType =  ZoneRec->RecType;
	  BPIn->RequestedTag = ExpectedTag;
	  BPIn->SampleTimeStamp = SampleTimeStamp;
	break;

	case MSG_TX_TEMP: // Reqest extended Sample ( CTNonStat, CTStat, Monitor, MonitorExt )
	  BPIn->RecordingNo =  ZoneRec->RecNo;
	  BPIn->RecType =  ZoneRec->RecType;
	  BPIn->RequestedTag = ExpectedTag;
	  BPIn->SampleTimeStamp = SampleTimeStamp;
	break;

	case MSG_TX_LPR:
	  BPIn->RecordingNo =  ZoneRec->RecNo;
	  SMRunning_LPR = true;
	break;

	case MSG_TX_LPREND:
	  BPIn->RecordingNo =  ZoneRec->RecNo;
	  BPIn->RecType =  ZoneRec->RecType;
	  BPIn->MonitorExtended = MonitorExtended;
	  BPIn->SampleTimeStamp = SampleTimeStamp;
	  BPIn->MonitorExtendedRecNo = MonitorExtendedRecNo;
	  break;

	default:
	  break;
  }
}

void __fastcall TCIISZone_Online::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{
  switch (BPIn->MessageCommand)
  {
	case MSG_TX_CAPABILITY:
		if( BPIn->MsgMode == CIISBus_UpdateNode )
		{

			if( ((TCIISObj*)BPIn->CIISObj)->CIISObjIs( CIISNode ) )
			{
				if( ((TCIISNodeIni*)BPIn->CIISObj)->NodCapability == Camur_II_WLink)
				{
					RemoveNI( BPIn );
				}
				else
				{
					UpdateNode( BPIn );
					BPIn->MsgMode = CIISBus_MsgReady;
					BPIn->ParseMode = CIISParseReady;
        }
			}
			else
			{
				UpdateNode( BPIn );
				BPIn->MsgMode = CIISBus_MsgReady;
				BPIn->ParseMode = CIISParseReady;
			}
		}
		break;

	case MSG_TX_LPREND:

		if( LPREnd() ) StopRecording();
		BPIn->MsgMode = CIISBus_MsgReady;
		BPIn->ParseMode = CIISParseReady;
	  break;

	default:
	  break;
  }
}

void __fastcall TCIISZone_Online::StartSchedule()
{
	if(( ZoneRec->IncludeInSchedule ) && ( ZoneRec->ZoneScheduleStatus == 0 ))
	{
		OnSlowClockTick = 0;
		SMRunning_Schedule = true;
		ZoneRec->ZoneScheduleStatus = 1;
	}
}

int32_t __fastcall TCIISZone_Online::GetNodeCount()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
	TCIISPowerSupply *PS;
	TCIISAlarm *Alarm;
  int32_t NC;

	NC = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorStatus == 0 ) NC++;
		}
		else if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			Alarm = (TCIISAlarm*)CIISObj;
			if( Alarm->AlarmStatus == 0 ) NC++;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->PSStatus == 0 ) NC++;
		}
	}

	return NC;
}

void __fastcall TCIISZone_Online::UpdateLastValues()
{
  if( !UpdateLastValueRunning )
  {
		StartSampleTimeStamp = Now();
		CIISBusInt->RequestSample( ZoneRec->ZoneCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
		UpdateLastValueRunning = true;

		TimerLastVal = 0;
		TimeLastValEvent = ApplyUpdDelay;

		if( !( SMRunning_CW || SMRunning_LPR_CW )) CWUpdateLastValues();
		if( !( SMRunning_Ladder || SMRunning_LPR_Ladder )) LadderUpdateLastValues();
		if( !SMRunning_MRE ) MREUpdateLastValues();
		if( !SMRunning_PSTemp ) PSUpdateLastTempValues();
		if( !SMRunning_CTx1Temp ) CTx1UpdateLastTempValues();
	}

}

void __fastcall TCIISZone_Online::SM_UpdateLastValue()
{
	if( TimerLastVal >= TimeLastValEvent)
	{
		DB->ApplyUpdatesSensor();
		DB->ApplyUpdatesPS();
		DB->ApplyUpdatesPrj();
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		UpdateLastValueRunning = false;
	}
	TimerLastVal += SysClock;


}

bool __fastcall TCIISZone_Online::StartMonitor()
{
	if( ZoneRec->RecType != RT_StandBy ) return false;

	MaxCWChCount = GetMaxCWChCount();

	TInterval = P_Ctrl->GetSampelIntervall( ZoneRec->ZoneSampInterval ) * Sec_To_mS;
	if( P_Ctrl->NoIR ) TInterval = ( TInterval > NoIRMinSampTime ) ? TInterval :  NoIRMinSampTime;
	if( TInterval < 2000 )
	{
		FastApplyUppdate = true;
		MonitorFirstSample = true;
	}
	else
	{
		FastApplyUppdate = false;
		MonitorFirstSample = false;
	}

	AppUpdDelayAdjusted = (( TInterval - 400 ) < 5000 ) ? ( TInterval - 400 ) : 5000;

 if( DB->FindLastRecording() )
	{
		DB->GetRecordingRec( RR, false );
		RR->RecNo = RR->RecNo + 1;
	}
	else RR->RecNo = 1;

	RR->RecStart = Now();
	RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	RR->RecType = RT_Monitor;
	RR->SampInterval= TInterval / Sec_To_mS;
	RR->ZoneNo = ZoneRec->ZoneNo;

	RR->DecaySampInterval = 0;
	RR->DecayDuration = 0;
	RR->LPRRange = 0;
	RR->LPRStep = 0;
	RR->LPRDelay1 = 0;
	RR->LPRDelay2 = 0;
	RR->LPRMode = 0;

	DB->AppendRecordingRec( RR ); //DB->ApplyUpdatesRecording();
	P_Ctrl->ApplyNewRecording = true;

	ZoneRec->RecType = RT_Monitor;
	MonitorExtended = false;
	ZoneRec->RecNo = RR->RecNo;
	if( DB->LocateZoneRec( ZoneRec ) )
	{
		DB->SetZoneRec( ZoneRec ); //DB->ApplyUpdatesZone();
		P_Ctrl->ApplyNewZone = true;
	}



	// Handel SensorWarmUP
	TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
	if( TWarmUp > 0 )
	{
		if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;
		if(( TWarmUp > 0 ) && ( TInterval - TWarmUp >= TWarmUpEnabled )) WUEnabled = true;
		else WUEnabled = false;
	}
	else
	{
		WUEnabled = false;
	}

	if(( TWarmUp > 0 ) && !WUEnabled ) SensorWarmUp( true );

	ResetLastStoredTag();
	RequestTag = 1;
	T = 0;
	TNextEvent = 1000;
	MState = MSStart;
/*
  	if ( WUEnabled ) MState = MSWarmUp;
  	else if ( P_Ctrl->NoIR ) MState = MSPowerOff;
		else MState = MSSample_1;
*/

	if( CWInZone() )
	{
		T_CW = 0;
		TNextEvent_CW = 500;
		State_CW = 10;
		Sample_CW = true;
	}
	else
	{
		Sample_CW = false;
	}

	if( MREInZone() )
	{
		T_CW = 0;
		TNextEvent_MRE = 500;
		State_MRE = 10;
		Sample_MRE = true;
	}
	else
	{
		Sample_MRE = false;
	}

	if( LadderInZone() )
	{
		T_Ladder = 0;
		TNextEvent_Ladder = 500;
		State_Ladder = 10;
		Sample_Ladder = true;
	}
	else
	{
		Sample_Ladder = false;
	}

	if( PSTempInZone() )
	{
		T_PSTemp = 0;
		TNextEvent_PSTemp = 500;
		State_PSTemp = 10;
		Sample_PSTemp = true;
	}
	else
	{
		 Sample_PSTemp = false;
	}

	if( CTx1TempInZone() )
	{
		T_CTx1Temp = 0;
		TNextEvent_CTx1Temp = 500;
		State_CTx1Temp = 10;
		Sample_CTx1Temp = true;
	}
	else
	{
		 Sample_CTx1Temp = false;
	}



	SMRunning_MRE = false;
	SMRunning_LPR_CW = false;
	SMRunning_LPR_Ladder = false;
	SMRunning_Ladder = false;

	P_Ctrl->IncRecordingCount( RT_Monitor );
	P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "Monitor", RR->RecNo, 0 );

	return true;
}

bool __fastcall TCIISZone_Online::StartMonitorExtended()
{
  if( ZoneRec->RecType != RT_StandBy ) return false;

  MaxCWChCount = GetMaxCWChCount();

  TInterval = P_Ctrl->GetSampelIntervall( ZoneRec->ZoneSampInterval ) * Sec_To_mS;
  //if( P_Ctrl->NoIR ) TInterval = max( TInterval, NoIRMinSampTime );
  if( P_Ctrl->NoIR ) TInterval = ( TInterval > NoIRMinSampTime ) ? TInterval: NoIRMinSampTime;

  //AppUpdDelayAdjusted = min( TInterval - 400, 5000 );
  AppUpdDelayAdjusted = (( TInterval - 400 ) < 5000 ) ? ( TInterval - 400 ) : 5000;

	if( DB->FindLastRecording() )
  {
		DB->GetRecordingRec( RR, false );
		RR->RecNo = RR->RecNo + 1;
  }
	else RR->RecNo = 1;

  RR->RecStart = Now();
  RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
  RR->RecType = RT_MonitorExt;
  RR->SampInterval= TInterval / Sec_To_mS;
  RR->ZoneNo = ZoneRec->ZoneNo;

  RR->DecaySampInterval = 0;
  RR->DecayDuration = 0;
  RR->LPRRange = 0;
  RR->LPRStep = 0;
  RR->LPRDelay1 = 0;
  RR->LPRDelay2 = 0;
  RR->LPRMode = 0;

  DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

  ZoneRec->RecType = RT_MonitorExt;
  MonitorExtended = true;
  ForceStopRec = false;
  ZoneRec->RecNo = RR->RecNo;
  MonitorExtendedRecNo = RR->RecNo;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
  }

  // Handel SensorWarmUP
  TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
  if( TWarmUp > 0 )
  {
		if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;
		if(( TWarmUp > 0 ) && ( TInterval - TWarmUp >= TWarmUpEnabled )) WUEnabled = true;
		else WUEnabled = false;
  }
  else
  {
		WUEnabled = false;
  }

  if(( TWarmUp > 0 ) && !WUEnabled ) SensorWarmUp( true );

  ResetLastStoredTag();
  RequestTag = 1;
  T = 0;
  TNextEvent = 1000;
  if ( WUEnabled ) MState = MSWarmUp;
  else if ( P_Ctrl->NoIR ) MState = MSPowerOff;
  else MState = MSSample_1;

	if( CWInZone() )
	{
		T_CW = 0;
		TNextEvent_CW = 500;
		State_CW = 10;
		Sample_CW = true;
	}
	else
	{
		Sample_CW = false;
  }

  if( MREInZone() )
  {
		T_CW = 0;
		TNextEvent_MRE = 500;
		State_MRE = 10;
		Sample_MRE = true;
  }
  else
  {
		Sample_MRE = false;
  }

  if( LadderInZone() )
  {
		T_Ladder = 0;
		TNextEvent_Ladder = 500;
		State_Ladder = 10;
		Sample_Ladder = true;
  }
  else
  {
		Sample_Ladder = false;
	}

	if( PSTempInZone() )
	{
		T_PSTemp = 0;
		TNextEvent_PSTemp = 500;
		State_PSTemp = 10;
		Sample_PSTemp = true;
	}
	else
	{
		 Sample_PSTemp = false;
	}

	if( CTx1TempInZone() )
	{
		T_CTx1Temp = 0;
		TNextEvent_CTx1Temp = 500;
		State_CTx1Temp = 10;
		Sample_CTx1Temp = true;
	}
	else
	{
		 Sample_CTx1Temp = false;
	}

  SMRunning_MRE = false;
	SMRunning_LPR_CW = false;
  SMRunning_LPR_Ladder = false;
  SMRunning_Ladder = false;

	P_Ctrl->IncRecordingCount( RT_MonitorExt );
  P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "MonitorExt", RR->RecNo, 0 );

  return true;
}

bool __fastcall TCIISZone_Online::StartScheduledValues()
{
	if( ZoneRec->RecType != RT_StandBy ) return false;

	MaxCWChCount = GetMaxCWChCount();

	TInterval = P_Ctrl->GetSampelIntervall( ZoneRec->ZoneSampInterval ) * Sec_To_mS;
	//if( P_Ctrl->NoIR ) TInterval = max( TInterval, NoIRMinSampTime );
	if( P_Ctrl->NoIR ) TInterval = ( TInterval > NoIRMinSampTime ) ? TInterval: NoIRMinSampTime;

	//AppUpdDelayAdjusted = min( TInterval - 400, 5000 );
	AppUpdDelayAdjusted = (( TInterval - 400 ) < 5000 ) ? ( TInterval - 400 ) : 5000;

	if( DB->FindLastRecording() )
	{
		DB->GetRecordingRec( RR, false );
		RR->RecNo = RR->RecNo + 1;
	}
	else RR->RecNo = 1;

	RR->RecStart = Now();
	RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	RR->RecType = RT_ScheduledValues;
	RR->SampInterval= TInterval / Sec_To_mS;
	RR->ZoneNo = ZoneRec->ZoneNo;

	RR->DecaySampInterval = 0;
	RR->DecayDuration = 0;
	RR->LPRRange = 0;
	RR->LPRStep = 0;
	RR->LPRDelay1 = 0;
	RR->LPRDelay2 = 0;
	RR->LPRMode = 0;

	DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

	ZoneRec->RecType = RT_ScheduledValues;
	MonitorExtended = false;
	ZoneRec->RecNo = RR->RecNo;
	if( DB->LocateZoneRec( ZoneRec ) )
	{
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	}

	// Handel SensorWarmUP
	TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
	if( TWarmUp > 0 )
	{
		if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;
		if(( TWarmUp > 0 ) && ( TInterval - TWarmUp >= TWarmUpEnabled )) WUEnabled = true;
		else WUEnabled = false;
	}
	else
	{
		WUEnabled = false;
	}

	if(( TWarmUp > 0 ) && !WUEnabled ) SensorWarmUp( true );

	ResetLastStoredTag();
	RequestTag = 1;
	T = 0;
	TNextEvent = 1000;
	if ( WUEnabled ) MState = MSWarmUp;
	else if ( P_Ctrl->NoIR ) MState = MSPowerOff;
	else MState = MSSample_1;

	if( CWInZone() )
	{
		T_CW = 0;
		TNextEvent_CW = 500;
		State_CW = 10;
		Sample_CW = true;
	}
	else
	{
		Sample_CW = false;
	}

	if( MREInZone() )
	{
		T_CW = 0;
		TNextEvent_MRE = 500;
		State_MRE = 10;
		Sample_MRE = true;
	}
	else
	{
		Sample_MRE = false;
	}

	if( LadderInZone() )
	{
		T_Ladder = 0;
		TNextEvent_Ladder = 500;
		State_Ladder = 10;
		Sample_Ladder = true;
	}
	else
	{
		Sample_Ladder = false;
	}

	if( PSTempInZone() )
	{
		T_PSTemp = 0;
		TNextEvent_PSTemp = 500;
		State_PSTemp = 10;
		Sample_PSTemp = true;
	}
	else
	{
		 Sample_PSTemp = false;
	}

	if( CTx1TempInZone() )
	{
		T_CTx1Temp = 0;
		TNextEvent_CTx1Temp = 500;
		State_CTx1Temp = 10;
		Sample_CTx1Temp = true;
	}
	else
	{
		 Sample_CTx1Temp = false;
	}



	SMRunning_MRE = false;
	SMRunning_LPR_CW = false;
	SMRunning_LPR_Ladder = false;
	SMRunning_Ladder = false;

	P_Ctrl->IncRecordingCount( RT_Monitor );
	P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "Monitor", RR->RecNo, 0 );

	return true;
}

bool __fastcall TCIISZone_Online::StartDecay()
{
	if( ZoneRec->RecType != RT_StandBy ) return false;

	TDecayInstantOffDelay = P_Ctrl->DecayDelay;
	TIniInterval = P_Ctrl->DecaySampInterval2 * Sec_To_mS;
	TDecayIniDuration = P_Ctrl->DecayDuration2;
	TDistDecayIniDuration =  min( TDecayIniDuration, 20 );  // Distributed ini duration limited by timeout in early nodes.
	TInterval = P_Ctrl->GetSampelIntervall( P_Ctrl->DecaySampInterval ) * Sec_To_mS;
	TDecayTotDuration = P_Ctrl->DecayDuration;
	AppUpdDelayAdjusted = (( TInterval - 400 ) < 5000 ) ? ( TInterval - 400 ) : 5000;

	if( DB->FindLastRecording() )
	{
		DB->GetRecordingRec( RR, false );
		RR->RecNo = RR->RecNo + 1;
	}
	else RR->RecNo = 1;

	RR->RecStart = Now();
	RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	RR->RecType = RT_Decay;
	RR->SampInterval = 0;
	RR->ZoneNo = ZoneRec->ZoneNo;
	RR->DecayDelay = TDecayInstantOffDelay;
	RR->DecaySampInterval =  TInterval / Sec_To_mS;
	RR->DecaySampInterval2 =  TIniInterval / Sec_To_mS;
	RR->DecayDuration = P_Ctrl->DecayDuration;
	RR->DecayDuration2 = P_Ctrl->DecayDuration2;
	RR->LPRRange = 0;
	RR->LPRStep = 0;
	RR->LPRDelay1 = 0;
	RR->LPRDelay2 = 0;
	RR->LPRMode = 0;
	DB->AppendRecordingRec( RR ); //DB->ApplyUpdatesRecording();
	P_Ctrl->ApplyNewRecording = true;

	ZoneRec->RecType = RT_Decay;
	ZoneRec->RecNo = RR->RecNo;
	if( DB->LocateZoneRec( ZoneRec ) )
	{
		DB->SetZoneRec( ZoneRec ); //DB->ApplyUpdatesZone();
		P_Ctrl->ApplyNewZone = true;
	}

	ResetLastStoredTag();
	RequestTag = 1;
	T = 0;
	TNextEvent = 27000;
	TDuration = TIniInterval / Sec_To_mS;
	DState = DecayIni;
	RequestDecayIni( 1, P_Ctrl->DecayDelay / 10, TDistDecayIniDuration / P_Ctrl->DecaySampInterval2, P_Ctrl->DecaySampInterval2 ); // Only checks if all nodes has Decay function. Sets OnlyDistDecay tue/false
	// Use distributed decay only if all nodes in a zone support it
	SetRunDistDecay( OnlyDistDecay ); // set RunDistDecay true/false for all nodes in Zone
	// if( OnlyDistDecay )	CIISBusInt->RequestDecayIni(ZoneRec->ZoneCanAdr, 1, P_Ctrl->DecayDelay / 10, TDistDecayIniDuration / P_Ctrl->DecaySampInterval2, P_Ctrl->DecaySampInterval2 );  // DecayIni send to zon address insted.
	P_Ctrl->IncRecordingCount( RT_Decay );
	P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "Decay", RR->RecNo, 0 );

	return true;
}

void __fastcall TCIISZone_Online::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;


	OnlyDistDecay = true;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			OnlyDistDecay = S->RequestDecayIni( StartDelay, POffDelay, NoOfIniSamples, IniInterval ) && OnlyDistDecay;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			OnlyDistDecay = PS->RequestDecayIni( StartDelay, POffDelay, NoOfIniSamples, IniInterval ) && OnlyDistDecay;
		}
	}
}

void __fastcall TCIISZone_Online::SetPSOutputTmpOffFlag()
{
	TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->SetOutputTmpOffFlag();
		}
	}
}


bool __fastcall TCIISZone_Online::StartLPR()
{
	LPRVersion = GetLPRVersionInZone(); // 0 = No LPRSensor in this Zone

	if( ZoneRec->RecType != RT_StandBy || LPRVersion == 0 ) return false;

	//ReadTempDelay = 0;

	SMRunning_LPR = false;
	T_LPR = 0;
	TNextEvent_LPR = 500;
	State_LPR = 10;

	if( LPRVersion == 3 )   //CW nodes
	{
		LPR_CWSelectedCh = 1;
		T_LPR_CW = 0;
		TNextEvent_LPR_CW = 200;
		State_LPR_CW = 20;

		LPR_LadderSelectedCh = 1;
		T_LPR_Ladder = 0;
		TNextEvent_LPR_Ladder = 200;
		State_LPR_Ladder = 20;

		if( CWInZone() ) SMRunning_LPR_CW = true;
		if( LadderInZone() ) SMRunning_LPR_Ladder = true;
		ForceStopRec = false;
		MaxCWChCount = GetMaxCWChCount();
	}
	else
	{
		if( DB->FindLastRecording() )
		{
			DB->GetRecordingRec( RR, false );
			RR->RecNo = RR->RecNo + 1;
		}
		else RR->RecNo = 1;

		RR->RecStart = Now();
		RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
		RR->RecType = RT_LPR;
		RR->SampInterval= 0;
		RR->ZoneNo = ZoneRec->ZoneNo;
		RR->DecayDelay = 0;
		RR->DecaySampInterval = 0;
		RR->DecaySampInterval2 = 0;
		RR->DecayDuration = 0;
		RR->DecayDuration2 = 0;
		RR->LPRRange = P_Ctrl->LPRRange;
		RR->LPRStep = P_Ctrl->LPRStep;
		RR->LPRDelay1 = P_Ctrl->LPRDelay1;
		RR->LPRDelay2 = P_Ctrl->LPRDelay2;
		RR->LPRMode = P_Ctrl->LPRMode;

		DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

		ZoneRec->RecType = RT_LPR;
		ZoneRec->RecNo = RR->RecNo;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		if( LPRVersion == 1 )
		{

			LPRRange = max( 1, RR->LPRRange );
			LPRTOn = max( 1, RR->LPRDelay1 );
			LPRTOff = max( 1, RR->LPRDelay2 );
			LPRStep = RR->LPRStep;
			LPRMode = 0;
		}
		else // LPRVersion == 2 or 3
		{
			LPRRange = max( 1, RR->LPRRange / 10 );
			LPRTOn = max( 1, RR->LPRDelay1 / 5 );
			LPRTOff = max( 0, RR->LPRDelay2 / 5 );
			LPRStep = RR->LPRStep / 2;
			LPRMode = RR->LPRMode;

			switch (LPRMode)
			{
			case 0:
				LPRStep = abs( LPRStep );
				LPRStep = max(1, min(127, LPRStep));
				break;
			case 1:
			case 2:
				 if( LPRStep >= 0 ) LPRStep = max(1, min(127, LPRStep));
				 else LPRStep = max( -127, LPRStep );
				break;
			case 3:
				 if( LPRStep >= 0 ) LPRStep = 1;
				 else LPRStep = -1;
				 break;
			}
		}
    LPRSelectRange( -1 );
		LPRIni();


		//CIISBusInt->RequestLPRStart( ZoneRec->ZoneCanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode  );
		//Dont start MRE nodes...

		TCIISObj *CIISObj;
		TCIISSensor *S;

		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISSensors ) )
			{
				S = (TCIISSensor*)CIISObj;
				if( S->SensorType == Camur_II_LPR )
				{
					CIISBusInt->RequestLPRStart( S->CanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode  );
				}
			}
		}


		P_Ctrl->IncRecordingCount( RT_LPR );
		P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "LPR", RR->RecNo, 0 );
	}
	return true;
}

bool __fastcall TCIISZone_Online::StartCTNonStat()
{
  if( ZoneRec->RecType != RT_StandBy ) return false;

  if( DB->FindLastRecording() )
  {
	DB->GetRecordingRec( RR, false );
	RR->RecNo = RR->RecNo + 1;
  }
  else RR->RecNo = 1;

	RR->RecStart = Now();
	RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
  RR->RecType = RT_CTNonStat;
  RR->SampInterval = 0;
  RR->ZoneNo = ZoneRec->ZoneNo;
  RR->DecayDelay = TDecayInstantOffDelay;
  RR->DecaySampInterval =  TInterval / Sec_To_mS;
  RR->DecaySampInterval2 =  TIniInterval / Sec_To_mS;
  RR->DecayDuration = P_Ctrl->DecayDuration;
  RR->DecayDuration2 = P_Ctrl->DecayDuration2;
  RR->LPRRange = 0;
  RR->LPRStep = 0;
  RR->LPRDelay1 = 0;
  RR->LPRDelay2 = 0;
  RR->LPRMode = 0;
	DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

  ZoneRec->RecType = RT_CTNonStat;
	ZoneRec->RecNo = RR->RecNo;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
	DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
  }

    // Handel SensorWarmUP
  TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
  if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;


  ResetLastStoredTag();
  RequestTag = 1;
  T = 0;
  TNextEvent = 1000;
	TDuration = 0;
  CTState = 10;

  P_Ctrl->IncRecordingCount( RT_CTNonStat );
  P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "CT NonStat", RR->RecNo, 0 );

  return true;

}

bool __fastcall TCIISZone_Online::StartCTStat()
{
  if( ZoneRec->RecType != RT_StandBy ) return false;

  if( DB->FindLastRecording() )
  {
	DB->GetRecordingRec( RR, false );
	RR->RecNo = RR->RecNo + 1;
  }
  else RR->RecNo = 1;

  RR->RecStart = Now();
  RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
  RR->RecType = RT_CTStat;
  RR->SampInterval = 0;
  RR->ZoneNo = ZoneRec->ZoneNo;
  RR->DecayDelay = TDecayInstantOffDelay;
  RR->DecaySampInterval =  TInterval / Sec_To_mS;
  RR->DecaySampInterval2 =  TIniInterval / Sec_To_mS;
  RR->DecayDuration = P_Ctrl->DecayDuration;
  RR->DecayDuration2 = P_Ctrl->DecayDuration2;
  RR->LPRRange = 0;
  RR->LPRStep = 0;
  RR->LPRDelay1 = 0;
  RR->LPRDelay2 = 0;
  RR->LPRMode = 0;
	DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

  ZoneRec->RecType = RT_CTStat;
	ZoneRec->RecNo = RR->RecNo;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
	DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
  }

    // Handel SensorWarmUP
  TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
  if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;


  ResetLastStoredTag();
  RequestTag = 1;
  T = 0;
	TNextEvent = 1000;
	TDuration = 0;
  CTState = 10;

  P_Ctrl->IncRecordingCount( RT_CTStat );
	P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "CT Stat", RR->RecNo, 0 );

  return true;

}

bool __fastcall TCIISZone_Online::StartRExt()
{
  if( ZoneRec->RecType != RT_StandBy ) return false;

  if( DB->FindLastRecording() )
  {
		DB->GetRecordingRec( RR, false );
		RR->RecNo = RR->RecNo + 1;
  }
  else RR->RecNo = 1;

  RR->RecStart = Now();
  RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	RR->RecType = RT_RExt;
  RR->SampInterval = 0;
  RR->ZoneNo = ZoneRec->ZoneNo;
  RR->DecayDelay = TDecayInstantOffDelay;
  RR->DecaySampInterval =  TInterval / Sec_To_mS;
  RR->DecaySampInterval2 =  TIniInterval / Sec_To_mS;
  RR->DecayDuration = P_Ctrl->DecayDuration;
  RR->DecayDuration2 = P_Ctrl->DecayDuration2;
  RR->LPRRange = 0;
  RR->LPRStep = 0;
  RR->LPRDelay1 = 0;
  RR->LPRDelay2 = 0;
  RR->LPRMode = 0;
	DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

  ZoneRec->RecType = RT_RExt;
	ZoneRec->RecNo = RR->RecNo;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
  }

    // Handel SensorWarmUP
  TWarmUp = MaxSensorWarmUp() * Sec_To_mS;
  if( TWarmUp < TWarmUpMin ) TWarmUp = TWarmUpMin;


  ResetLastStoredTag();
  RequestTag = 1;
  T = 0;
  TNextEvent = 1000;
	TDuration = 0;
  CTState = 10;

  P_Ctrl->IncRecordingCount( RT_RExt );
  P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "R Ext", RR->RecNo, 0 );

  return true;

}

bool __fastcall TCIISZone_Online::StopRecording()
{
	if( ZoneRec->RecType == 0 ) return false;

	//ReadTempDelay = 1000;

	switch( ZoneRec->RecType )
	{
	case RT_Monitor:
		if( TWarmUp > 0 ) SensorWarmUp( false );
		if( P_Ctrl->NoIR ) PSIniState();

		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); //DB->ApplyUpdatesZone();
			P_Ctrl->ApplyNewZone = true;
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); //DB->ApplyUpdatesRecording();
      P_Ctrl->ApplyNewRecording = true;
		}

		P_Ctrl->DecRecordingCount( RT_Monitor );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "Monitor", RR->RecNo, 0 );
		break;

	case RT_MonitorExt:
		if( TWarmUp > 0 ) SensorWarmUp( false );
		if( P_Ctrl->NoIR ) PSIniState();

		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
		}

		State_Ladder = 900;
		State_CW = 900;

		MonitorExtended = false;

		P_Ctrl->DecRecordingCount( RT_MonitorExt );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "MonitorExt", RR->RecNo, 0 );
		break;

	case RT_ScheduledValues:
		if( TWarmUp > 0 ) SensorWarmUp( false );
		if( P_Ctrl->NoIR ) PSIniState();

		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
		}

		P_Ctrl->DecRecordingCount( RT_Monitor );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "Monitor", RR->RecNo, 0 );
		break;

	case RT_LPR:

		if( MonitorExtended )
		{
			ZoneRec->RecType = RT_StandBy;
			if( DB->LocateZoneRec( ZoneRec ) )
			{
				DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
			}

			if( ForceStopRec )
			{
				if( ( State_CW > 100 ) && ( State_CW < 200 ) )    // Monitor Extended Recording
				{
					State_CW = 900;
					CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );

					RR->RecStop = Now();
					if( DB->LocateRecordingNo( RR->RecNo ) )
					{
						DB->SetRecordingRec( RR );
						DB->ApplyUpdatesRecording();
					}
					P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );
					P_Ctrl->DecRecordingCount( RT_LPR );
				}

				if( ( State_Ladder > 100 ) && ( State_CW < 200 ) )  // Monitor Extended Recording
				{
					State_Ladder = 900;
					CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );

					RR->RecStop = Now();
					if( DB->LocateRecordingNo( RR->RecNo ) )
					{
						DB->SetRecordingRec( RR );
						DB->ApplyUpdatesRecording();
					}

					P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );
					P_Ctrl->DecRecordingCount( RT_LPR );
				}

				if( TWarmUp > 0 ) SensorWarmUp( false );
				if( P_Ctrl->NoIR ) PSIniState();

				if( DB->LocateRecordingNo( MonitorExtendedRecNo ) )
				{
					DB->GetRecordingRec( RR, false );
					RR->RecStop = Now();
					DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
				}


				MonitorExtended = false;

				P_Ctrl->DecRecordingCount( RT_MonitorExt );
				P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "MonitorExt", RR->RecNo, 0 );
			}
		}
		else
		{
			if( SMRunning_LPR_CW  )
			{
				ZoneRec->RecType = RT_StandBy;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				if( ForceStopRec ) State_LPR_CW = 990;  // Terminate SM_LPR_CW  LPR Recording
			}

			if( SMRunning_LPR_Ladder  )
			{
				ZoneRec->RecType = RT_StandBy;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				if( ForceStopRec ) State_LPR_Ladder = 990;  // Terminate SM_LPR_Ladder  LPR Recording
			}
			else if( !( SMRunning_LPR_CW || SMRunning_LPR_Ladder ))  // Std LPR
			{
				ZoneRec->RecType = RT_StandBy;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				RR->RecStop = Now();
				if( DB->LocateRecordingNo( RR->RecNo ) )
				{
					DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
				}
				CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );
				P_Ctrl->DecRecordingCount( RT_LPR );
				P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );
			}
		}
		break;

	case RT_Decay:
		CIISBusInt->RequestDecayCancel( ZoneRec->ZoneCanAdr );
		if(( ZoneRec->ZoneScheduleStatus == 0 ) && ( RestartScheduleStatus == 0 )) PSIniState(); // if not scheduled LPR will follow
		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); P_Ctrl->ApplyNewZone = true; //DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); P_Ctrl->ApplyNewRecording = true; //DB->ApplyUpdatesRecording();
		}

		SetIniDecaySample( false ); // If stop during inisamples the flag must be cleard to get samples in next decay
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "Decay", RR->RecNo, 0 );
		P_Ctrl->DecRecordingCount( RT_Decay );
		break;

	case RT_CTNonStat:
		if( TWarmUp > 0 ) SensorWarmUp( false );

		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
		}

		P_Ctrl->DecRecordingCount( RT_CTNonStat );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "CTNonStat", RR->RecNo, 0 );
		break;

	case RT_CTStat:
		if( TWarmUp > 0 ) SensorWarmUp( false );

		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
		}

		P_Ctrl->DecRecordingCount( RT_CTStat );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "CTStat", RR->RecNo, 0 );
		break;

	case RT_RExt:
		if( TWarmUp > 0 ) SensorWarmUp( false );
		ZoneRec->RecType = RT_StandBy;
		if( DB->LocateZoneRec( ZoneRec ) )
		{
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
		}

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
		{
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
		}

		P_Ctrl->DecRecordingCount( RT_RExt );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "RExt", RR->RecNo, 0 );
		break;
	}
	return true;
}

void __fastcall TCIISZone_Online::PrepareShutdown()
{
	RestartRecType = ZoneRec->RecType;
	StopRecording();
	ZoneRec->RecType = RestartRecType;
	DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
}

void __fastcall TCIISZone_Online::SM_Schedule()
{
	switch( ZoneRec->ZoneScheduleStatus )
	{
		case 1:
			if( MonitorExtended ) ZoneRec->RecTypeBeforeSchedule = RT_MonitorExt;
			else ZoneRec->RecTypeBeforeSchedule = ZoneRec->RecType;
			ForceStopRec = true;
			StopRecording();
			if( P_Ctrl->ScheduleDecay ) ZoneRec->ZoneScheduleStatus = 2;
			else if( P_Ctrl->ScheduleLPR ) ZoneRec->ZoneScheduleStatus = 4;
			else if( P_Ctrl->ScheduleZRA  || P_Ctrl->ScheduleResMes ) ZoneRec->ZoneScheduleStatus = 6;
			else ZoneRec->ZoneScheduleStatus = 8;
			MSDCounter = 0;
			#if DebugCIISObj == 1
			Debug( "Scheduling started on Zone " + IntToStr( ZoneRec->ZoneNo ) );
			#endif
			break;

		case 2:
			StartDecay();
			ZoneRec->ZoneScheduleStatus = 3;
			#if DebugCIISObj == 1
			Debug( "Starting scheduled decay on Zone " + IntToStr( ZoneRec->ZoneNo ) );
			#endif
			break;

		case 3:
			if( ZoneRec->RecType == RT_StandBy )
			{
				if( P_Ctrl->ScheduleLPR ) ZoneRec->ZoneScheduleStatus = 4;
				else if( P_Ctrl->ScheduleZRA  || P_Ctrl->ScheduleResMes ) ZoneRec->ZoneScheduleStatus = 6;
				else ZoneRec->ZoneScheduleStatus = 8;
				MSDCounter = 0;
			}
			break;

		case 4:
			StartLPR();
			ZoneRec->ZoneScheduleStatus = 5;
			#if DebugCIISObj == 1
			Debug( "Starting scheduled LPR on Zone " + IntToStr( ZoneRec->ZoneNo ) );
			#endif
			break;

		case 5:
			if(( ZoneRec->RecType == RT_StandBy ) && ( SMRunning_LPR_CW == false ) && ( SMRunning_LPR_Ladder == false ))
			{
				if( P_Ctrl->ScheduleZRA  || P_Ctrl->ScheduleResMes ) ZoneRec->ZoneScheduleStatus = 6;
				else ZoneRec->ZoneScheduleStatus = 8;
				MSDCounter = 0;
			}
			break;

		case 6:
			StartScheduledValues();
			ZoneRec->ZoneScheduleStatus = 7;
			#if DebugCIISObj == 1
			Debug( "Starting scheduled values " + IntToStr( ZoneRec->ZoneNo ) );
			#endif
			break;

		case 7:
			if( ZoneRec->RecType == RT_StandBy ) ZoneRec->ZoneScheduleStatus = 8;
			MSDCounter = 0;
			break;

		case 8:
			PSIniState();
			if( MSDCounter >= FMonitorStartDelay )
			{
				if( ZoneRec->RecTypeBeforeSchedule == RT_Monitor ) StartMonitor();
				else if( ZoneRec->RecTypeBeforeSchedule == RT_MonitorExt ) StartMonitorExtended();
				ZoneRec->ZoneScheduleStatus = 0;
				SMRunning_Schedule = false;
			}
			else ZoneRec->ZoneScheduleStatus = 9;
			MSDCounter += 2;
			break;

		case 9:
			if( MSDCounter >= FMonitorStartDelay )
			{
				if( ZoneRec->RecTypeBeforeSchedule == RT_Monitor ) StartMonitor();
				else if( ZoneRec->RecTypeBeforeSchedule == RT_MonitorExt ) StartMonitorExtended();
				ZoneRec->ZoneScheduleStatus = 0;
				SMRunning_Schedule = false;
			}
			else MSDCounter += 2;
			#if DebugCIISObj == 1
			Debug( "Scheduling ready on Zone " + IntToStr( ZoneRec->ZoneNo ) );
			#endif
			break;
	}
}

void __fastcall TCIISZone_Online::SM_Monitor()
{
	if( T >= TNextEvent)
	{
		switch (MState)
		{
			case MSStart:
			if( P_Ctrl->ApplyNewRecording || P_Ctrl->ApplyNewZone )
			{
				T = 0;
				TNextEvent = 0;

				#if DebugCIIApplyUppdate == 1
				Debug( "Apply recording not ready " + ZoneRec->ZoneName );
				#endif

				break;
			}

			#if DebugCIIApplyUppdate == 1
			Debug( "Apply recording ready " + ZoneRec->ZoneName );
			#endif

			T = 0;
			TNextEvent = 0;
			if ( WUEnabled ) MState = MSWarmUp;
			else if ( P_Ctrl->NoIR ) MState = MSPowerOff;
			else MState = MSSample_1;
			break;

			case MSWarmUp:
			SensorWarmUp( true );
			if( P_Ctrl->NoIR )
			{
				TNextEvent += TWarmUp - NoIRDelay;
				MState = MSPowerOff;
			}
			else
			{
				TNextEvent += TWarmUp;
				MState = MSSample_1;
			}
			T += SysClock;
			break;

			case MSPowerOff:
			PSSetOutputOff();
			TNextEvent += NoIRDelay;
			MState = MSSample_1;
			T += SysClock;
			break;


			case MSSample_1:
			StartSampleTimeStamp = Now();
			RequestSample( StartSampleTimeStamp );
			MState = MSSample_2;
			TNextEvent = 0;  // Continue with next zon before start multi channel nodes and fast apply update.
			T = SysClock;
			break;

			case MSSample_2:
			// Start multi ch nodes
			if( Sample_MRE )
			{
				State_MRE = 10;
				TNextEvent_MRE = 500;
				T_MRE = 0;
				SMRunning_MRE = true;
			}
			if( Sample_CW )
			{
				State_CW = 10;
				TNextEvent_CW = 500;
				T_CW = 0;
				SMRunning_CW = true;
			}
			if( Sample_Ladder )
			{
				State_Ladder = 10;
				TNextEvent_Ladder = 500;
				T_Ladder = 0;
				SMRunning_Ladder = true;
			}
			if( Sample_PSTemp )
			{
				State_PSTemp = 10;
				TNextEvent_PSTemp = 700;
				T_PSTemp = 0;
				SMRunning_PSTemp = true;
			}

			if( Sample_CTx1Temp )
			{
				State_CTx1Temp = 10;
				TNextEvent_CTx1Temp = 700;
				T_CTx1Temp = 0;
				SMRunning_CTx1Temp = true;
			}

			if( FastApplyUppdate && MonitorFirstSample )   // In FastApplyUpdate there is no data to store on the first sample request.
			{
				MonitorFirstSample = false;

				if( SMRunning_MRE || SMRunning_CW || SMRunning_Ladder || SMRunning_PSTemp || SMRunning_CTx1Temp )  // Wait for Multistep MRE/CW nodes to finnish previus samp. Apply updaate made in SMRunning_XXX
				{
					TNextEvent = 1000;
					MState = MSWaitForMultiStepNodes;
				}
				else if( P_Ctrl->NoIR || WUEnabled )     // see: NoIRMinSampTime and TWarmUpEnabled that limits min TInterval
				{
					TNextEvent = AfterSampleDelay;
					MState = MSAfterSample;
				}
				else
				{
					TNextEvent = TInterval;
					MState = MSSample_1;
				}
			}
			else if( FastApplyUppdate ) // Short TInterval (1000ms) Only when NoIR or Warmup not is selected. Update is made one samp interval after req.
			{

				PrevTag = RequestTag - 1; if( PrevTag == 0 ) PrevTag = 200;

				#if DebugCIIApplyUppdate == 1
				Debug( "Check Sample Recived: " + ZoneRec->ZoneName );
				#endif
				CheckSampleRecived( PrevTag );

				#if DebugCIIApplyUppdate == 1
				Debug( "Start Fast Apply Update: " + ZoneRec->ZoneName );
				#endif

				P_Ctrl->ApplyNewMonitorSample = true;

				if( SMRunning_MRE || SMRunning_CW || SMRunning_Ladder || SMRunning_PSTemp || SMRunning_CTx1Temp )  // Wait for Multistep MRE/CW nodes to finnish previus samp. Apply updaate made in SMRunning_XXX
				{
					TNextEvent = 1000;
					MState = MSWaitForMultiStepNodes;
				}
				else if( P_Ctrl->NoIR || WUEnabled )     // see: NoIRMinSampTime and TWarmUpEnabled that limits min TInterval
				{
					TNextEvent = AfterSampleDelay;
					MState = MSAfterSample;
				}
				else
				{
					TNextEvent = TInterval;
					MState = MSSample_1;
				}
			}
			else
			{
				TNextEvent = 1000;
				MState = MSAfterSample;
			}
			break;

			case MSWaitForMultiStepNodes:
			if( SMRunning_MRE || SMRunning_CW || SMRunning_Ladder || SMRunning_PSTemp || SMRunning_CTx1Temp ) TNextEvent += 1000; // Wait for Multichannel MRE/CW nodes to finnish previus samp
			else
			{
				if( P_Ctrl->NoIR || WUEnabled )
				{
					TNextEvent += AfterSampleDelay;
					MState = MSAfterSample;
				}
				else
				{
					TNextEvent = TInterval;
					MState = MSSample_1;
				}
				if ( ZoneRec->RecType == RT_ScheduledValues ) StopRecording();
			}
			T += SysClock;
			break;

			case MSAfterSample:
			if( P_Ctrl->NoIR )
			{
				PSSetOutputState();
				MState = MSPowerOff;
				TNextEvent = TInterval - NoIRDelay; //AfterSampleDelay - NoIRDelay - AppUpdDelayAdjusted;
			}
			if( WUEnabled )
			{
				SensorWarmUp( false );
				MState = MSWarmUp;
				TNextEvent = TInterval - TWarmUp; //AfterSampleDelay - TWarmUp - AppUpdDelayAdjusted;
			}

			#if DebugCIIApplyUppdate == 1
			Debug( "Check Sample Recived: " + ZoneRec->ZoneName  );
			#endif

			CheckSampleRecived( RequestTag );

			#if DebugCIIApplyUppdate == 1
			Debug( "Start Normal Apply Update: " + ZoneRec->ZoneName );
			#endif

			P_Ctrl->ApplyNewMonitorSample = true;

			TNextEvent = TInterval;
			MState = MSSample_1;

			T += SysClock;
			break;

			default:
			break;
		}
  }
  else T += SysClock;
}

void __fastcall TCIISZone_Online::SM_Sample_CW()
{
  if( T_CW >= TNextEvent_CW)
  {
		switch (State_CW)
		{
		case 10: // P-Mode
			TNextEvent_CW += 500;
			if( !CWUpdatingLastValue() ) State_CW = 20 ;
			T_CW += SysClock;
			break;

		case 20: // P-Mode
			CWSelectMode( 2 );
			TNextEvent_CW += 1000;
			State_CW = 21 ;
			T_CW += SysClock;
			break;

		case 21: // Range = -1 => user selected range see SensorRec->SensorLPRIRange
			CWSelectRange( -1 );
			TNextEvent_CW += 1000;
			State_CW = 22 ;
			T_CW += SysClock;
			break;

		case 22: // R Meassuring freq. = 1000 Hz
			CWSelectRFreq(1000);
			TNextEvent_CW += 1000;
			State_CW = 23 ;
			T_CW += SysClock;
			break;

		case 23: // Channel = 0;
			CWSelectCh( 0 );
			TNextEvent_CW += 2000;
			State_CW = 24 ;
			T_CW += SysClock;
			break;

		case 24: // Adj. Offset
			CWOffsetAdj();
			TNextEvent_CW += 2000;
			State_CW = 25 ;
			T_CW += SysClock;
			break;

		case 25: // Calibrate DA
			CWDACalib();
			TNextEvent_CW += 3000;
			State_CW = 30 ;
			T_CW += SysClock;
			break;

		case 30: // Request temp
			CWRequestTemp( 211, StartSampleTimeStamp );
			TNextEvent_CW += 3000;
			State_CW = 39 ;
			T_CW += SysClock;
			break;

		case 39: // Apply uppdate T
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CWSelectedCh = 1;
			TNextEvent_CW += 1000;
			State_CW = 40 ;
			T_CW += SysClock;
			break;

		case 40: // Channel = 1-6
			CWSelectCh( CWSelectedCh );
			TNextEvent_CW += 2000;
			State_CW = 45 ;
			T_CW += SysClock;
			break;

		case 45: // Request sample ch 1-6
			CWRequestSample( 200 + CWSelectedCh, StartSampleTimeStamp );
			State_CW = 49 ;
			TNextEvent_CW += 1000;
			T_CW += SysClock;
			break;

		case 49: // Apply uppdate P
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_CW += 500;
			CWSelectedCh++;
			if( CWSelectedCh <= MaxCWChCount ) State_CW = 40 ;
			else if( MonitorExtended ) State_CW = 100;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleZRA ) State_CW = 500;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleResMes ) State_CW = 600;
			else State_CW = 900;
			
			T_CW += SysClock;
			break;

		// Monitor Extended (LPR, ZRA, R added)

		case 100: // Start recording
			if( DB->FindLastRecording() )
			{
				DB->GetRecordingRec( RR, false );
				RR->RecNo = RR->RecNo + 1;
			}
			else RR->RecNo = 1;

			RR->RecStart = StartSampleTimeStamp;
			RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
			RR->RecType = RT_LPR;
			RR->SampInterval= 0;
			RR->ZoneNo = ZoneRec->ZoneNo;
			RR->DecayDelay = 0;
			RR->DecaySampInterval = 0;
			RR->DecaySampInterval2 = 0;
			RR->DecayDuration = 0;
			RR->DecayDuration2 = 0;
			RR->LPRRange = P_Ctrl->LPRRange;
			RR->LPRStep = P_Ctrl->LPRStep;
			RR->LPRDelay1 = P_Ctrl->LPRDelay1;
			RR->LPRDelay2 = P_Ctrl->LPRDelay2;
			RR->LPRMode = P_Ctrl->LPRMode;

			DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

			SMRunning_LPR = false;
			T_LPR = 0;
			TNextEvent_LPR = 500;
			State_LPR = 10;


			ZoneRec->RecType = RT_LPR;
			ZoneRec->RecNo = RR->RecNo;
			if( DB->LocateZoneRec( ZoneRec ) )
			{
				DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
			}

			P_Ctrl->IncRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

			CWSelectedCh = 1;

			TNextEvent_CW += 1000;
			State_CW = 110 ;
			T_CW += SysClock;
			break;

		case 110: // Channel = 1-7
			CWSelectCh( CWSelectedCh );
			TNextEvent_CW += 1000;
			State_CW = 120 ;
			T_CW += SysClock;
			break;

		case 120:
			LPRRange = max( 1, RR->LPRRange / 10 );
			LPRTOn = max( 1, RR->LPRDelay1 / 5 );
			LPRTOff = max( 0, RR->LPRDelay2 / 5 );
			LPRStep = RR->LPRStep / 2;
			LPRMode = RR->LPRMode;

			switch (LPRMode)
			{
			case 0:
				LPRStep = abs( LPRStep );
				LPRStep = max(1, min(127, LPRStep));
				break;

			case 1:
			case 2:
				if( LPRStep >= 0 ) LPRStep = max(1, min(127, LPRStep));
				else LPRStep = max( -127, LPRStep );
				break;

			case 3:
				if( LPRStep >= 0 ) LPRStep = 1;
				else LPRStep = -1;
				break;
			}

			if( LPRIni() )
			{
				CWRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode );
				ZoneRec->RecType = RT_LPR;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				TNextEvent_CW += 2000;
				State_CW = 130 ;
				T_CW += SysClock;
			}
			else
			{
				TNextEvent_CW += 2000;
				State_CW = 140 ;
				T_CW += SysClock;
			}
			break;

		case 130:
			if( ZoneRec->RecType == RT_StandBy )
			{
				CWSelectedCh++;
				if( CWSelectedCh <= MaxCWChCount ) State_CW = 110 ;
				else State_CW = 140;

				DB->ApplyUpdatesMonitorValue();
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				DB->ApplyUpdatesPrj();
			}
			TNextEvent_CW += 1000;
			T_CW += SysClock;
			break;

		case 140:
			CWSelectCh( 1 );
			TNextEvent_CW += 500;
			State_CW = 199 ;
			T_CW += SysClock;
			break;

		case 199:
			RR->RecStop = Now();
			if( DB->LocateRecordingNo( RR->RecNo ) )
			{
				DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
			}
			CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );
			P_Ctrl->DecRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

			if( DB->LocateRecordingNo( MonitorExtendedRecNo ) )
			{
				DB->GetRecordingRec( RR, false );

				ZoneRec->RecType = RT_MonitorExt;
				ZoneRec->RecNo = RR->RecNo;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				TNextEvent_CW += 500;
				State_CW = 500 ;
				T_CW += SysClock;
			}
			else
			{
				TNextEvent_CW += 500;
				State_CW = 900 ;
				T_CW += SysClock;
			}
			break;

			// Sample ZRA channels

		case 500: // ZRA-Mode
			CWSelectMode( 1 );
			TNextEvent_CW += 1000;
			State_CW = 521 ;
			T_CW += SysClock;
			break;

		case 521: // Range = -1 => user selected range see SensorRec->SensorLPRIRange
			CWSelectRange( -1 );
			TNextEvent_CW += 1000;
			State_CW = 523 ;
			T_CW += SysClock;
			break;

		case 523: // Channel = 0;
			CWSelectCh( 0 );
			TNextEvent_CW += 1000;
			State_CW = 524 ;
			T_CW += SysClock;
			break;

		case 524: // Adj. Offset
			CWOffsetAdj();
			TNextEvent_CW += 2000;
			State_CW = 525 ;
			T_CW += SysClock;
			break;

		case 525: // Calibrate DA
			CWDACalib();
			CWSelectedCh = 1;
			TNextEvent_CW += 3000;
			State_CW = 540 ;
			T_CW += SysClock;
			break;

		case 540: // Channel = 1-7
			CWSelectCh( CWSelectedCh );
			TNextEvent_CW += 10000;
			State_CW = 545 ;
			T_CW += SysClock;
			break;

		case 545: // Request sample ch 1-7
			CWRequestSample( 210 + CWSelectedCh, StartSampleTimeStamp );
			TNextEvent_CW += 1000;
			State_CW = 549 ;
			T_CW += SysClock;
			break;

		case 549: // Apply Uppdate ZRA
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_CW += 500;
			CWSelectedCh++;
			if( CWSelectedCh <= MaxCWChCount ) State_CW = 540 ;
			else if( MonitorExtended ) State_CW = 600;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleResMes ) State_CW = 600;
			else State_CW = 900;
			
			T_CW += SysClock;
			break;

			// Sample R channels

		case 600: // R-Mode
			CWSelectMode( 0 );
			TNextEvent_CW += 1000;
			State_CW = 621 ;
			T_CW += SysClock;
			break;

		case 621: // Range = 0
			CWSelectRange( 0 );
			TNextEvent_CW += 1000;
			State_CW = 622 ;
			T_CW += SysClock;
			break;

		case 622: // 1000 HZ
			CWSelectRFreq(1000);
			TNextEvent_CW += 1000;
			CWSelectedCh = 1;
			State_CW = 640 ;
			T_CW += SysClock;
			break;

		case 640: // Channel = 1-7
			CWSelectCh( CWSelectedCh );
			TNextEvent_CW += 1000;
			State_CW = 645 ;
			T_CW += SysClock;
			break;

		case 645: // Request sample ch 1-7
			CWRequestSample( 210 + CWSelectedCh, StartSampleTimeStamp );
			State_CW = 649 ;
			TNextEvent_CW += 3000;
			T_CW += SysClock;
			break;

		case 649: // Apply Update R
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_CW += 500;
			CWSelectedCh++;
			if( CWSelectedCh <= MaxCWChCount ) State_CW = 640 ;
			else State_CW = 900;
			T_CW += SysClock;
			break;

		case 900: // P-Mode
			CWSelectMode( 2 );
			TNextEvent_CW += 1000;
			State_CW = 921 ;
			T_CW += SysClock;
			break;

		case 921: // Range = -1 => user selected range see SensorRec->SensorLPRIRange
			CWSelectRange( -1 );
			TNextEvent_CW += 1000;
			State_CW = 922 ;
			T_CW += SysClock;
			break;

		case 922: // R Meassuring freq. = 1000 Hz
			CWSelectRFreq(1000);
			TNextEvent_CW += 1000;
			State_CW = 923 ;
			T_CW += SysClock;
			break;

		case 923: // Channel = 1;
			CWSelectCh( 1 );
			TNextEvent_CW += 1000;
			State_CW = 999 ;
			T_CW += SysClock;
			break;

		case 999:
			SMRunning_CW = false;
			break;
		}
  }
  else T_CW += SysClock;
}

void __fastcall TCIISZone_Online::SM_Sample_MRE()
{
	if( T_MRE >= TNextEvent_MRE)
	{
	switch (State_MRE)
	{
	case 10: // P-Mode
	  TNextEvent_MRE += 500;
		if( !MREUpdatingLastValue() ) State_MRE = 20 ;
	  T_MRE += SysClock;
		break;

	case 20: // P-Mode
	  MRESelectMode( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 21 ;
	  T_MRE += SysClock;
	  break;

	case 21: // Range = 2
	  MRESelectRange( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 22 ;
	  T_MRE += SysClock;
	  break;

	case 22: // R Meassuring freq. = 1000 Hz
		MRESelectRFreq(1000);
	  TNextEvent_MRE += 500;
	  State_MRE = 23 ;
	  T_MRE += SysClock;
	  break;

	case 23: // Channel = 0;
	  MRESelectCh( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 24 ;
	  T_MRE += SysClock;
		break;

	case 24: // Adj. Offset
		MREOffsetAdj();
		TNextEvent_MRE += 2000;
		State_MRE = 25 ;
		T_MRE += SysClock;
		break;

	case 25: // Calibrate DA
		MREDACalib();
		TNextEvent_MRE += 3000;
		State_MRE = 26 ;
		T_MRE += SysClock;
		break;

	case 26: // Request temp
		MRERequestTemp( 211, StartSampleTimeStamp );
		TNextEvent_MRE += 3000;
		State_MRE = 50 ;
		T_MRE += SysClock;
		break;


	case 50: // R-Mode
		MRESelectMode( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 51 ;
	  T_MRE += SysClock;
	  break;

	case 51: // Range = 0
	  MRESelectRange( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 52 ;
	  T_MRE += SysClock;
	  break;

	case 52: // Channel = 0;
	  MRESelectCh( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 53 ;
	  T_MRE += SysClock;
	  break;

	case 53: // 1000 HZ
	  MRESelectRFreq(1000);
	  TNextEvent_MRE += 500;
	  MRESelectedCh = 1;
	  State_MRE = 55 ;
	  T_MRE += SysClock;
	  break;

	case 55: // Channel = 1-8
	  MRESelectCh( MRESelectedCh );
		TNextEvent_MRE = 500;
	  State_MRE = 56 ;
	  T_MRE = SysClock;
	  break;

	case 56: // Request sample ch 1-7
		MRERequestSample( 200 + MRESelectedCh, StartSampleTimeStamp );
		MRESelectedCh++;
		if( MRESelectedCh < 8 ) State_MRE = 55 ;
		else State_MRE = 900;

		DB->ApplyUpdatesMonitorValue();
	  DB->ApplyUpdatesSensor();
	  DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();

		TNextEvent_MRE += 3000;
	  T_MRE += SysClock;
	  break;

	case 900:
		T_MRE = SysClock;
		TNextEvent_MRE = AppUpdDelayAdjusted;
		State_MRE = 910;
		break;

	case 910:
		DB->ApplyUpdatesMonitorValue();
		DB->ApplyUpdatesSensor();
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();

		T_MRE += SysClock;
		TNextEvent_MRE += 200;
		State_MRE = 950;
		break;

	case 950: // P-Mode
	  MRESelectMode( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 951 ;
	  T_MRE += SysClock;
	  break;

	case 951: // Range = 2
	  MRESelectRange( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 952 ;
	  T_MRE += SysClock;
	  break;

	case 952: // R Meassuring freq. = 1000 Hz
	  MRESelectRFreq(1000);
	  TNextEvent_MRE += 500;
	  State_MRE = 953 ;
	  T_MRE += SysClock;
	  break;

	case 953: // Channel = 1;
	  MRESelectCh( 1 );
	  TNextEvent_MRE += 500;
	  State_MRE = 999 ;
	  T_MRE += SysClock;
	  break;

	case 999:
		SMRunning_MRE = false;
		break;
		}
  }
  else T_MRE += SysClock;
}

void __fastcall TCIISZone_Online::SM_Sample_Ladder()
{
  if( T_Ladder >= TNextEvent_Ladder)
  {
		switch (State_Ladder)
		{
		case 10: // Wait if last value update is running
			TNextEvent_Ladder += 500;
			if( !LadderUpdatingLastValue() ) State_Ladder = 20 ;
			T_Ladder += SysClock;
			break;

		case 20: // P-Mode
			LadderSelectMode( 2 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 21 ;
			T_Ladder += SysClock;
			break;

		case 21: // Range -1 => user selected range see SensorRec->SensorLPRIRange
			LadderSelectRange( -1 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 22 ;
			T_Ladder += SysClock;
			break;

		case 22: // R Meassuring freq. = 1000 Hz
			LadderSelectRFreq(1000);
			TNextEvent_Ladder += 1000;
			State_Ladder = 23 ;
			T_Ladder += SysClock;
			break;

		case 23: // Channel = 0;
			LadderSelectCh( 0 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 24 ;
			T_Ladder += SysClock;
			break;

		case 24: // Adj. Offset
			LadderOffsetAdj();
			TNextEvent_Ladder += 2000;
			LadderSelectedCh = 1;
			State_Ladder = 25 ;
			T_Ladder += SysClock;
			break;

		case 25: // Calibrate DA
			LadderDACalib();
			TNextEvent_Ladder += 3000;
			State_Ladder = 30 ;
			T_Ladder += SysClock;
			break;

		case 30: // Request Temp
			LadderRequestTemp( 211, StartSampleTimeStamp );
			TNextEvent_Ladder += 3000;
			State_Ladder = 39 ;
			T_Ladder += SysClock;

			#if DebugCIISBusInt == 1
			Debug("Zone Temp requested ");
			#endif

			break;

		case 39: // Apply update T
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			LadderSelectedCh = 1;
			TNextEvent_Ladder += 500;
			State_Ladder = 40 ;
			T_Ladder += SysClock;

			#if DebugCIISBusInt == 1
			Debug("Zone Apply update T ");
			#endif



			break;

			// Sample P channels

		case 40: // Channel = 1-8

			LadderSelectCh( LadderSelectedCh );
			TNextEvent_Ladder += 2000;
			State_Ladder = 45 ;
			T_Ladder += SysClock;
			break;

		case 45: // Request sample ch 1-7
			LadderRequestSample( 210 + LadderSelectedCh, StartSampleTimeStamp );
			TNextEvent_Ladder += 1000;
			State_Ladder = 49 ;
			T_Ladder += SysClock;
			break;

		case 49: // Apply update P, Extended ?
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_Ladder += 500;
			LadderSelectedCh++;
			if( LadderSelectedCh < 8 ) State_Ladder = 40 ;
			else if( MonitorExtended ) State_Ladder = 100;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleZRA ) State_Ladder = 500;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleResMes ) State_Ladder = 600;
			else State_Ladder = 900;

			T_Ladder += SysClock;
			break;


			// Monitor Extended (LPR added)


		case 100: // Start recording
			if( DB->FindLastRecording() )
			{
				DB->GetRecordingRec( RR, false );
				RR->RecNo = RR->RecNo + 1;
			}
			else RR->RecNo = 1;

			RR->RecStart = StartSampleTimeStamp;
			RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
			RR->RecType = RT_LPR;
			RR->SampInterval= 0;
			RR->ZoneNo = ZoneRec->ZoneNo;
			RR->DecayDelay = 0;
			RR->DecaySampInterval = 0;
			RR->DecaySampInterval2 = 0;
			RR->DecayDuration = 0;
			RR->DecayDuration2 = 0;
			RR->LPRRange = P_Ctrl->LPRRange;
			RR->LPRStep = P_Ctrl->LPRStep;
			RR->LPRDelay1 = P_Ctrl->LPRDelay1;
			RR->LPRDelay2 = P_Ctrl->LPRDelay2;
			RR->LPRMode = P_Ctrl->LPRMode;

			DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

			SMRunning_LPR = false;
			T_LPR = 0;
			TNextEvent_LPR = 500;
			State_LPR = 10;


			ZoneRec->RecType = RT_LPR;
			ZoneRec->RecNo = RR->RecNo;
			if( DB->LocateZoneRec( ZoneRec ) )
			{
				DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
			}

			P_Ctrl->IncRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

			LadderSelectedCh = 1;

			TNextEvent_Ladder += 1000;
			State_Ladder = 110 ;
			T_Ladder += SysClock;
			break;

		case 110: // Channel = 1-7
			LadderSelectCh( LadderSelectedCh );
			TNextEvent_Ladder += 1000;
			State_Ladder = 120 ;
			T_Ladder += SysClock;
			break;

		case 120:

			LPRRange = max( 1, RR->LPRRange / 10 );
			LPRTOn = max( 1, RR->LPRDelay1 / 5 );
			LPRTOff = max( 0, RR->LPRDelay2 / 5 );
			LPRStep = RR->LPRStep / 2;
			LPRMode = RR->LPRMode;

			switch (LPRMode)
			{
			case 0:
				LPRStep = abs( LPRStep );
				LPRStep = max(1, min(127, LPRStep));
				break;
			case 1:
			case 2:
				 if( LPRStep >= 0 ) LPRStep = max(1, min(127, LPRStep));
				 else LPRStep = max( -127, LPRStep );
				break;
			case 3:
				 if( LPRStep >= 0 ) LPRStep = 1;
				 else LPRStep = -1;
				 break;
			}

			if( LPRIni() )
			{
				LadderRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode );
				ZoneRec->RecType = RT_LPR;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				TNextEvent_Ladder += 2000;
				State_Ladder = 130 ;
				T_Ladder += SysClock;
			}
			else
			{
				TNextEvent_Ladder += 2000;
				State_Ladder = 140 ;
				T_Ladder += SysClock;
			}

			break;

		case 130:
			if( ZoneRec->RecType == RT_StandBy )
			{
				LadderSelectedCh++;
				if( LadderSelectedCh < 8 ) State_Ladder = 110 ;
				else State_Ladder = 140;

				DB->ApplyUpdatesMonitorValue();
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				DB->ApplyUpdatesPrj();

			}
			TNextEvent_Ladder += 1000;
			T_Ladder += SysClock;
			break;

		case 140:
			LadderSelectCh( 1 );
			TNextEvent_Ladder += 500;
			State_Ladder = 199 ;
			T_Ladder += SysClock;
			break;

		case 199:

			RR->RecStop = Now();
			if( DB->LocateRecordingNo( RR->RecNo ) )
			{
				DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
			}
			CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );
			P_Ctrl->DecRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );


			if( DB->LocateRecordingNo( MonitorExtendedRecNo ) )
			{
				DB->GetRecordingRec( RR, false );

				ZoneRec->RecType = RT_MonitorExt;
				ZoneRec->RecNo = RR->RecNo;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				TNextEvent_Ladder += 500;
				State_Ladder = 500 ;
				T_Ladder += SysClock;
			}
			else
			{
				TNextEvent_Ladder += 500;
				State_Ladder = 900 ;
				T_Ladder += SysClock;
			}
			break;

			// Sample ZRA channels

		case 500: // ZRA-Mode
			LadderSelectMode( 1 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 521 ;
			T_Ladder += SysClock;
			break;

		case 521: // Range = -1 => user selected range see SensorRec->SensorLPRIRange
			LadderSelectRange( -1 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 523 ;
			T_Ladder += SysClock;
			break;

		case 523: // Channel = 0;
			LadderSelectCh( 0 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 524 ;
			T_Ladder += SysClock;
			break;

		case 524: // Adj. Offset
			LadderOffsetAdj();
			TNextEvent_Ladder += 2000;
			State_Ladder = 525 ;
			T_Ladder += SysClock;
			break;

		case 525: // Calibrate DA
			LadderDACalib();
			LadderSelectedCh = 1;
			TNextEvent_Ladder += 3000;
			State_Ladder = 540 ;
			T_Ladder += SysClock;
			break;

		case 540: // Channel = 1-7
			LadderSelectCh( LadderSelectedCh );
			TNextEvent_Ladder += 10000;
			State_Ladder = 545 ;
			T_Ladder += SysClock;
			break;

		case 545: // Request sample ch 1-7
			LadderRequestSample( 210 + LadderSelectedCh, StartSampleTimeStamp );
			TNextEvent_Ladder += 1000;
			State_Ladder = 549 ;
			T_Ladder += SysClock;
			break;

		case 549: // Apply Uppdate ZRA
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_Ladder += 500;
			LadderSelectedCh++;
			if( LadderSelectedCh < 8 ) State_Ladder = 540 ;
			else if( MonitorExtended ) State_Ladder = 600;
			else if (( ZoneRec->RecType == RT_ScheduledValues ) && P_Ctrl->ScheduleResMes ) State_Ladder = 600;
			else State_Ladder = 900;
			
			T_Ladder += SysClock;
			break;

			// Sample R channels

		case 600: // R-Mode
			LadderSelectMode( 0 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 621 ;
			T_Ladder += SysClock;
			break;

		case 621: // Range = 0
			LadderSelectRange( 0 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 622 ;
			T_Ladder += SysClock;
			break;

		case 622: // 1000 HZ
			LadderSelectRFreq(1000);
			TNextEvent_Ladder += 1000;
			LadderSelectedCh = 1;
			State_Ladder = 640 ;
			T_Ladder += SysClock;
			break;

		case 640: // Channel = 1-7
			LadderSelectCh( LadderSelectedCh );
			TNextEvent_Ladder += 1000;
			State_Ladder = 645 ;
			T_Ladder += SysClock;
			break;

		case 645: // Request sample ch 1-7
			LadderRequestSample( 210 + LadderSelectedCh, StartSampleTimeStamp );
			State_Ladder = 649 ;
			TNextEvent_Ladder += 5000;
			T_Ladder += SysClock;
			break;

		case 649: // Apply Update R
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			TNextEvent_Ladder += 500;
			LadderSelectedCh++;
			if( LadderSelectedCh < 8 ) State_Ladder = 640 ;
			else State_Ladder = 900;
			T_Ladder += SysClock;
			break;

		case 900: // P-Mode
			LadderSelectMode( 2 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 921 ;
			T_Ladder += SysClock;
			break;

		case 921: // Range = -1 => user selected range see SensorRec->SensorLPRIRange
			LadderSelectRange( -1 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 922 ;
			T_Ladder += SysClock;
			break;

		case 922: // R Meassuring freq. = 1000 Hz
			LadderSelectRFreq(1000);
			TNextEvent_Ladder += 1000;
			State_Ladder = 923 ;
			T_Ladder += SysClock;
			break;

		case 923: // Channel = 1;
			LadderSelectCh( 1 );
			TNextEvent_Ladder += 1000;
			State_Ladder = 999 ;
			T_Ladder += SysClock;
			break;

		case 999:
			SMRunning_Ladder = false;
			break;
		}
	}
	else T_Ladder += SysClock;
}

void __fastcall TCIISZone_Online::SM_Sample_PSTemp()
{
	if( T_PSTemp >= TNextEvent_PSTemp)
	{
			switch (State_PSTemp)
			{
			case 10:
				if( !PSUpdatingLastValue() ) State_PSTemp = 20;
				TNextEvent_PSTemp += 100;
				break;

			case 20:
				PSRequestTemp( StartSampleTimeStamp );
				TNextEvent_PSTemp += 1000;
				State_PSTemp = 30;
				break;

			case 30:
				DB->ApplyUpdatesMonitorValue();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				DB->ApplyUpdatesPrj();

				SMRunning_PSTemp = false;
				break;
			}
	}
	T_PSTemp += SysClock;
}

void __fastcall TCIISZone_Online::SM_Sample_CTx1Temp()
{
	if( T_CTx1Temp >= TNextEvent_CTx1Temp)
	{
			switch (State_CTx1Temp)
			{
			case 10:
				if( !CTx1UpdatingLastValue() ) State_CTx1Temp = 20;
				TNextEvent_CTx1Temp += 100;
				break;

			case 20:
				CTx1RequestTemp( RequestTag, StartSampleTimeStamp );
				TNextEvent_CTx1Temp += 500;
				State_CTx1Temp = 25;
				break;

			case 25:
				if( !CTx1UpdatingTemp() ) State_CTx1Temp = 30;
				TNextEvent_CTx1Temp += 500;
				break;

			case 30:
				DB->ApplyUpdatesMonitorValue();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				DB->ApplyUpdatesPrj();

				SMRunning_CTx1Temp = false;
				break;
			}
	}
	T_CTx1Temp += SysClock;
}

void __fastcall TCIISZone_Online::SM_LPR()
{
	if( T_LPR >= TNextEvent_LPR)
	{
		switch (State_LPR)
		{
		case 10: // P-Mode
			TNextEvent_LPR += 3000;
			State_LPR = 900 ;
			T_LPR += SysClock;
			break;

		case 900:
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: Start");
			#endif

			DB->ApplyUpdatesLPRValue();
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: LPRValues ");
			#endif

			DB->ApplyUpdatesSensor();
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: Sensors ");
			#endif

			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: MiscValues ");
			#endif

			P_Prj->DataChanged();
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: Prj DataChanged");
			#endif

			DB->ApplyUpdatesPrj();
			#if DebugLPR == 1
			Debug(  "LPRAppUpd: Project ");
			#endif

			SMRunning_LPR = false;
			T_LPR = 0;
			TNextEvent_LPR = 500;
			State_LPR = 10;
			break;
		}
	}
	else T_LPR += SysClock;
}

void __fastcall TCIISZone_Online::SM_LPR_CW()
{
  if( T_LPR_CW >= TNextEvent_LPR_CW)
  {
	switch (State_LPR_CW)
	{

	case 20: // P-Mode
	  CWSelectMode( 2 );
	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 21 ;
	  T_LPR_CW += SysClock;
	  break;

	case 21: // Range = User selected
		CWSelectRange( -1 );  // -1 => user selected range see SensorRec->SensorLPRIRange
	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 22 ;
	  T_LPR_CW += SysClock;
	  break;

	case 22: // R Meassuring freq. = 1000 Hz
	  CWSelectRFreq(1000);
	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 23 ;
	  T_LPR_CW += SysClock;
	  break;

	case 23: // Channel = 0;
	  CWSelectCh( 0 );
	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 24 ;
	  T_LPR_CW += SysClock;
		break;

	case 24: // Adj. Offset
		CWOffsetAdj();
		TNextEvent_LPR_CW += 2000;
		State_LPR_CW = 25 ;
		T_LPR_CW += SysClock;
		break;

	case 25: // Calibrate DA
		CWDACalib();
		TNextEvent_LPR_CW += 3000;
		State_LPR_CW = 50 ;
		T_LPR_CW += SysClock;
		break;

	case 50: // Start recording
	  if( DB->FindLastRecording() )
	  {
			DB->GetRecordingRec( RR, false );
			RR->RecNo = RR->RecNo + 1;
	  }
		else RR->RecNo = 1;

	  RR->RecStart = Now();
		RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
	  RR->RecType = RT_LPR;
	  RR->SampInterval= 0;
	  RR->ZoneNo = ZoneRec->ZoneNo;
		RR->DecayDelay = 0;
	  RR->DecaySampInterval = 0;
	  RR->DecaySampInterval2 = 0;
	  RR->DecayDuration = 0;
	  RR->DecayDuration2 = 0;
	  RR->LPRRange = P_Ctrl->LPRRange;
	  RR->LPRStep = P_Ctrl->LPRStep;
	  RR->LPRDelay1 = P_Ctrl->LPRDelay1;
	  RR->LPRDelay2 = P_Ctrl->LPRDelay2;
	  RR->LPRMode = P_Ctrl->LPRMode;

	  DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

		SMRunning_LPR = false;
		T_LPR = 0;
		TNextEvent_LPR = 500;
		State_LPR = 10;

		ZoneRec->RecType = RT_LPR;
		ZoneRec->RecNo = RR->RecNo;
	  if( DB->LocateZoneRec( ZoneRec ) )
	  {
			DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	  }

	  P_Ctrl->IncRecordingCount( RT_LPR );
	  P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

	  LPR_CWSelectedCh = 1;

	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 55 ;
	  T_LPR_CW += SysClock;
	  break;

	case 55: // Channel = 1-6
		CWSelectCh( LPR_CWSelectedCh );
	  TNextEvent_LPR_CW += 1000;
	  State_LPR_CW = 900 ;
	  T_LPR_CW += SysClock;
	  break;

	case 900:

	  LPRRange = max( 1, RR->LPRRange / 10 );
	  LPRTOn = max( 1, RR->LPRDelay1 / 5 );
	  LPRTOff = max( 0, RR->LPRDelay2 / 5 );
	  LPRStep = RR->LPRStep / 2;
	  LPRMode = RR->LPRMode;

	  switch (LPRMode)
	  {
		case 0:
		  LPRStep = abs( LPRStep );
		  LPRStep = max(1, min(127, LPRStep));
		  break;
		case 1:
		case 2:
		   if( LPRStep >= 0 ) LPRStep = max(1, min(127, LPRStep));
		   else LPRStep = max( -127, LPRStep );
			break;
		case 3:
		   if( LPRStep >= 0 ) LPRStep = 1;
		   else LPRStep = -1;
			 break;
	  }

		if( LPRIni() )
		{
			CWRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode );
			ZoneRec->RecType = RT_LPR;
			if( DB->LocateZoneRec( ZoneRec ) )
			{
				DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
			}

			TNextEvent_LPR_CW += 2000;
			State_LPR_CW = 901 ;
			T_LPR_CW += SysClock;
		}
		else
		{
			TNextEvent_LPR_CW += 2000;
			State_LPR_CW = 990 ;
			T_LPR_CW += SysClock;
    }

	  break;

	case 901:
		if( ZoneRec->RecType == RT_StandBy )
		{
			LPR_CWSelectedCh++;
			if( LPR_CWSelectedCh <= MaxCWChCount ) State_LPR_CW = 55 ;
			else State_LPR_CW = 990;
	  }
		TNextEvent_LPR_CW += 1000;
		T_LPR_CW += SysClock;
	  break;

	case 990:
		CWSelectCh( 1 );
		TNextEvent_LPR_CW += 500;
	  State_LPR_CW = 999 ;
	  T_LPR_CW += SysClock;
	  break;

	case 999:

		RR->RecStop = Now();
		if( DB->LocateRecordingNo( RR->RecNo ) )
	  {
			DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
	  }
		CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );
		P_Ctrl->DecRecordingCount( RT_LPR );
		P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

		SMRunning_LPR_CW = false;
		break;
	}
  }
  else T_LPR_CW += SysClock;
}

void __fastcall TCIISZone_Online::SM_LPR_Ladder()
{
  if( T_LPR_Ladder >= TNextEvent_LPR_Ladder)
  {
	switch (State_LPR_Ladder)
	{
		case 20: // P-Mode
			LadderSelectMode( 2 );
			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 21 ;
			T_LPR_Ladder += SysClock;
			break;

		case 21: // Range = User selected
			LadderSelectRange( -1 ); // -1 => user selected range see SensorRec->SensorLPRIRange
			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 22 ;
			T_LPR_Ladder += SysClock;
			break;

		case 22: // R Meassuring freq. = 1000 Hz
			LadderSelectRFreq(1000);
			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 23 ;
			T_LPR_Ladder += SysClock;
			break;

		case 23: // Channel = 0;
			LadderSelectCh( 0 );
			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 24 ;
			T_LPR_Ladder += SysClock;
			break;

		case 24: // Adj. Offset
			LadderOffsetAdj();
			TNextEvent_LPR_Ladder += 2000;
			State_LPR_Ladder = 25 ;
			T_LPR_Ladder += SysClock;
			break;

		case 25: // Calibrate DA
			LadderDACalib();
			TNextEvent_LPR_Ladder += 3000;
			State_LPR_Ladder = 50 ;
			T_LPR_Ladder += SysClock;
			break;

		case 50: // Start recording
			if( DB->FindLastRecording() )
			{
				DB->GetRecordingRec( RR, false );
				RR->RecNo = RR->RecNo + 1;
			}
			else RR->RecNo = 1;

			RR->RecStart = Now();
			RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
			RR->RecType = RT_LPR;
			RR->SampInterval= 0;
			RR->ZoneNo = ZoneRec->ZoneNo;
			RR->DecayDelay = 0;
			RR->DecaySampInterval = 0;
			RR->DecaySampInterval2 = 0;
			RR->DecayDuration = 0;
			RR->DecayDuration2 = 0;
			RR->LPRRange = P_Ctrl->LPRRange;
			RR->LPRStep = P_Ctrl->LPRStep;
			RR->LPRDelay1 = P_Ctrl->LPRDelay1;
			RR->LPRDelay2 = P_Ctrl->LPRDelay2;
			RR->LPRMode = P_Ctrl->LPRMode;

			DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();

			SMRunning_LPR = false;
			T_LPR = 0;
			TNextEvent_LPR = 500;
			State_LPR = 10;

			ZoneRec->RecType = RT_LPR;
			ZoneRec->RecNo = RR->RecNo;
			if( DB->LocateZoneRec( ZoneRec ) )
			{
				DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
			}

			P_Ctrl->IncRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

			LPR_LadderSelectedCh = 1;

			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 55 ;
			T_LPR_Ladder += SysClock;
			break;

		case 55: // Channel = 1-6
			LadderSelectCh( LPR_LadderSelectedCh );
			TNextEvent_LPR_Ladder += 1000;
			State_LPR_Ladder = 900 ;
			T_LPR_Ladder += SysClock;
			break;

		case 900:

			LPRRange = max( 1, RR->LPRRange / 10 );
			LPRTOn = max( 1, RR->LPRDelay1 / 5 );
			LPRTOff = max( 0, RR->LPRDelay2 / 5 );
			LPRStep = RR->LPRStep / 2;
			LPRMode = RR->LPRMode;

			switch (LPRMode)
			{
			case 0:
				LPRStep = abs( LPRStep );
				LPRStep = max(1, min(127, LPRStep));
				break;
			case 1:
			case 2:
				 if( LPRStep >= 0 ) LPRStep = max(1, min(127, LPRStep));
				 else LPRStep = max( -127, LPRStep );
				break;
			case 3:
				 if( LPRStep >= 0 ) LPRStep = 1;
				 else LPRStep = -1;
				 break;
			}

			if( LPRIni() )
			{
				LadderRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode );
				ZoneRec->RecType = RT_LPR;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

				TNextEvent_LPR_Ladder += 2000;
				State_LPR_Ladder = 901 ;
				T_LPR_Ladder += SysClock;
			}
			else
			{
				TNextEvent_LPR_Ladder += 2000;
				State_LPR_Ladder = 990 ;
				T_LPR_Ladder += SysClock;
			}

			break;

		case 901:
			if( ZoneRec->RecType == RT_StandBy )
			{
				LPR_LadderSelectedCh++;
				if( LPR_LadderSelectedCh <= 7 ) State_LPR_Ladder = 55 ;
				else State_LPR_Ladder = 990;
			}
			TNextEvent_LPR_Ladder += 1000;
			T_LPR_Ladder += SysClock;
			break;

		case 990:
			LadderSelectCh( 1 );
			TNextEvent_LPR_Ladder += 500;
			State_LPR_Ladder = 999 ;
			T_LPR_Ladder += SysClock;
			break;

		case 999:

			RR->RecStop = Now();
			if( DB->LocateRecordingNo( RR->RecNo ) )
			{
				DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
			}
			CIISBusInt->RequestLPRStop( ZoneRec->ZoneCanAdr );
			P_Ctrl->DecRecordingCount( RT_LPR );
			P_Prj->Log( ClientCom, CIIEventCode_RecStop, CIIEventLevel_High, "LPR", RR->RecNo, 0 );

			SMRunning_LPR_Ladder = false;
			break;
		}
  }
	else T_LPR_Ladder += SysClock;
}

void __fastcall TCIISZone_Online::LPRSelectRange( int32_t Range )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_LPR)
			{
				S->LPRSelectRange( Range );
			}
		}
	}
}

bool __fastcall TCIISZone_Online::LPRIni()
{
  TCIISObj *CIISObj;
	TCIISSensor *S;
	bool SensorStarted, AnySensorStarted;

	AnySensorStarted = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			SensorStarted = S->SensorLPRIni();
			AnySensorStarted = AnySensorStarted || SensorStarted;
		}
	}
	return AnySensorStarted;
}

bool __fastcall TCIISZone_Online::LPREnd()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool AllSensorsReady;

  AllSensorsReady = true;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  AllSensorsReady = AllSensorsReady && S->LPREnd;
	  if( !AllSensorsReady ) break;
	}
  }
  return AllSensorsReady;
}

bool __fastcall TCIISZone_Online::ResumeDecay(int32_t ResumeRecNo, int32_t TDecayDurationLeft)
{
  if( ZoneRec->RecType != RT_StandBy ) return false;

  if( DB->LocateRecordingNo( ResumeRecNo ) ) DB->GetRecordingRec( RR, false );
  else return false;

  if( RR->RecType != RT_Decay ) return false;

  RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
  DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();

  TDecayInstantOffDelay = P_Ctrl->DecayDelay;
	TIniInterval = P_Ctrl->DecaySampInterval2 * Sec_To_mS;
	TDecayIniDuration = P_Ctrl->DecayDuration2;
	TDistDecayIniDuration =  min( TDecayIniDuration, 20 );
  TInterval = P_Ctrl->GetSampelIntervall( P_Ctrl->DecaySampInterval ) * Sec_To_mS;
	TDecayTotDuration = TDecayDurationLeft;

  ZoneRec->RecType = RT_Decay;
  ZoneRec->RecNo = RR->RecNo;
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	}


  ResetLastStoredTag();
  RequestTag = 1;
  T = 0;
  TNextEvent = TDecayInstantOffDelay;
	TDuration = TIniInterval / Sec_To_mS;
  DState = DecaySample;
	PSSetOutputOff();

  P_Ctrl->IncRecordingCount( RT_Decay );
  P_Prj->Log( ClientCom, CIIEventCode_RecStart, CIIEventLevel_High, "Decay", RR->RecNo, 0 );

  return true;
}

void __fastcall TCIISZone_Online::SM_Decay()
{
	if( T >= TNextEvent)    // else T += SysClock; SysClock = 100; Timer interupt = 110 mS for Sampel clock. TTimer interval is a mul of 55mS
  {
		switch (DState)
		{
			case DecayIni:
				if( P_Ctrl->ApplyNewRecording || P_Ctrl->ApplyNewZone )
				{
					T = 0;
					TNextEvent = 0;

					#if DebugCIIApplyUppdate == 1
					Debug( "Apply recording not ready " + ZoneRec->ZoneName );
					#endif

					break;
				}

				#if DebugCIIApplyUppdate == 1
				Debug( "Apply recording ready " + ZoneRec->ZoneName );
				#endif

				if( OnlyDistDecay )	CIISBusInt->RequestDecayIni(ZoneRec->ZoneCanAdr, 1, P_Ctrl->DecayDelay / 10, TDistDecayIniDuration / P_Ctrl->DecaySampInterval2, P_Ctrl->DecaySampInterval2 );  // DecayIni send to zon address insted.

				TNextEvent += DecayDelay1;  // =1s     Set in CIISCommon.h
				DState = DecayStart;
				T += SysClock;
				break;

			case DecayStart:   //0s
				SetIniDecaySample( true );
				if( OnlyDistDecay )
				{
					SampleTimeStamp = Now();  //Else SampleTimeStamp is set in RequestSample
					CIISBusInt->RequestDecayStart( ZoneRec->ZoneCanAdr );
				}
				else RequestSample( Now() );

				TNextEvent += DecayDelay1;  // =1s     Set in CIISCommon.h
				DState = DecayPowerOff;
				T += SysClock;
				break;

			case DecayPowerOff: //1s
				if( OnlyDistDecay ) SetPSOutputTmpOffFlag();  //Else OutputTmpOffFlag is set in PSSetOutputOff
				else PSSetOutputOff();
				TNextEvent += TDecayInstantOffDelay;  // User selectable normaly 500ms
				DState = DecayInstantOffSample;
				T += SysClock;
				break;

			case DecayInstantOffSample://1,5s
				if( !OnlyDistDecay ) RequestSample( Now() );
				T = SysClock;
				TNextEvent = TIniInterval; // User selectable normaly 2s
				DState = DecayIniSample1_1;
				break;

			case DecayIniSample1_1: //3,5a
				if( !OnlyDistDecay ) RequestSample( Now() );
				T = SysClock;
				TNextEvent = 0;  // Continue with next zon before start fast apply update.
				DState = DecayIniSample1_2;
				break;

			case DecayIniSample1_2:
				#if DebugCIIApplyUppdate == 1
				Debug( "Start Fast Apply Update 1: " + ZoneRec->ZoneName );
				#endif

				P_Ctrl->ApplyNewDecaySample = true;

				if( TDuration < TDistDecayIniDuration )
				{
					TNextEvent = TIniInterval;
					TDuration += TIniInterval / Sec_To_mS;
					DState = DecayIniSample1_1;
				}
				else if( TDuration < TDecayIniDuration )  // TDecayIniDuration >= max value for dist decay
				{
					TNextEvent = 800;
					DState = DistDecayOff;
				}
				else
				{
					TNextEvent = AppUpdDelayAdjusted;
					DState = DecayIniApplyUpd;
				}
				T += SysClock;
				break;

			case DistDecayOff:                         // TDecayIniDuration >= max value for dist decay
				CIISBusInt->RequestDecayCancel( ZoneRec->ZoneCanAdr );
				SetIniDecaySample( false );
				TNextEvent = TIniInterval;
				TDuration += TIniInterval / Sec_To_mS;
				DState = DecayIniSample2_1;
				T += SysClock;
				break;

			case DecayIniSample2_1:                    // TDecayIniDuration >= max value for dist decay
				RequestSample( Now() );
				T = SysClock;
				TNextEvent = 0;  // Continue with next zon before start fast apply update.
				DState = DecayIniSample2_2;
				break;

			case DecayIniSample2_2:
				#if DebugCIIApplyUppdate == 1
				Debug( "Start Fast Apply Update 2: " + ZoneRec->ZoneName );
				#endif

				P_Ctrl->ApplyNewDecaySample = true;

				DB->ApplyUpdatesDecayValue();

				if( TDuration < TDecayIniDuration )
				{
					TNextEvent = TIniInterval;
					TDuration += TIniInterval / Sec_To_mS;
					DState = DecayIniSample2_1;
				}
				else
				{
					TNextEvent = AppUpdDelayAdjusted;
					DState = DecayIniApplyUpd;
				}
        T += SysClock;
				break;

			case DecayIniApplyUpd:

				CIISBusInt->RequestDecayCancel( ZoneRec->ZoneCanAdr );

				#if DebugCIIApplyUppdate == 1
				Debug( "Start Apply Update 3: " + ZoneRec->ZoneName );
				#endif

				P_Ctrl->ApplyNewDecaySample = true;

				T += SysClock;
				TNextEvent = TInterval;
				//TDuration += TInterval / Sec_To_mS;
				DState = DecaySample;
				break;

			case DecaySample:
				SetIniDecaySample( false );
				RequestSample( Now() );
				T = SysClock;
				TNextEvent = AppUpdDelayAdjusted;
				DState = DecayApplyUpd;
				break;

			case DecayApplyUpd:

				P_Ctrl->ApplyNewDecaySample = true;

				#if DebugCIIApplyUppdate == 1
				Debug( "Check Sample Recived"  );
				#endif

				CheckSampleRecived( RequestTag );

				#if DebugCIIApplyUppdate == 1
				Debug( "Start Apply Update 4: " + ZoneRec->ZoneName );
				#endif

				T += SysClock;
				TNextEvent = TInterval;
				TDuration += TInterval / Sec_To_mS;
				DState = DecaySample;
				if( TDuration >= TDecayTotDuration )
				{
					TNextEvent = DecayDelay1;
					DState = DecayEnd;
				}
				break;

		case DecayEnd:
			StopRecording();
			break;
		}
  }
	else T += SysClock;
}

void __fastcall TCIISZone_Online::SM_CTNonStat()
{
  if( T >= TNextEvent)
  {
	switch (CTState)
		{
		case 10: // MSWarmUp
			SensorWarmUp( true );
			TNextEvent += TWarmUp;
			CTState = 20;
			T += SysClock;
			break;

		case 20: // P-Mode
			CTSelectMode( 2 );
			TNextEvent += 1000;
			CTState = 21 ;
			T += SysClock;
			break;

		case 21: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 22 ;
			T += SysClock;
			break;

		case 22: // R Meassuring freq. = 1000 Hz
			CTSelectRFreq(1000);
			TNextEvent += 1000;
			CTState = 23 ;
			T += SysClock;
			break;

		case 23: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 24 ;
			T += SysClock;
			break;

		case 24: // Adj. Offset
			CTOffsetAdj();
			TNextEvent += 2000;
			CTSelectedCh = 1;
			CTState = 25 ;
			T += SysClock;
			break;

		case 25: // Calibrate DA
			CTDACalib();
			TNextEvent += 3000;
			CTState = 26 ;
			T += SysClock;
			break;

		case 26: // Request HUM
			HUMRequestSample( 1, Now() );
			TNextEvent += 2000;
			CTState = 27 ;
			T += SysClock;
			break;

		case 27: // Request Temp
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTRequestTemp( 1, Now() );
			TNextEvent += 3000;
			CTSelectedCh = 1;
			CTState = 30 ;
			T += SysClock;
			break;

			// P-mode

		case 30: // Channel = 1-8
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTSelectCh( CTSelectedCh );
			TNextEvent += 1000;
			CTState = 31 ;
			T += SysClock;
			break;

		case 31: // Request sample ch 1-8
			CTRequestSample( 17 + CTSelectedCh, Now() );
			TNextEvent += 1000;
			CTSelectedCh++;
			if( CTSelectedCh < 9 ) CTState = 30 ;
			else CTState = 40;
			T += SysClock;
			break;

			// ZRA-mode

		case 40: // ZRA-Mode
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTSelectMode( 1 );
			TNextEvent += 1000;
			CTState = 41 ;
			T += SysClock;
			break;

		case 41: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 42 ;
			T += SysClock;
			break;

		case 42: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 43 ;
			T += SysClock;
			break;

		case 43: // Adj. Offset
			CTOffsetAdj();
			TNextEvent += 1000;
			CTSelectedCh = 1;
			CTState = 45 ;
			T += SysClock;
			break;

		case 45: // Channel = 1-8
			CTSelectCh( CTSelectedCh );
			TNextEvent = 1000;
			CTState = 46 ;
			T = SysClock;
			break;

		case 46: // Request sample ch 1-8
			CTRequestSample( 10 + CTSelectedCh, Now() );

			if( T < 10000 ) TNextEvent += 5000;
			else TNextEvent += 10000;
			if( T < 20000 ) CTState = 46 ;
			else
			{
				CTSelectedCh++;
				T = SysClock;
				TNextEvent = 1000;
				if( CTSelectedCh < 8 ) CTState = 45 ;
				else CTState = 50;
			}
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			T += SysClock;
			break;

			// R-mode

		case 50: // R-Mode
			CTSelectMode( 0 );
			TNextEvent += 1000;
			CTState = 51 ;
			T += SysClock;
			break;

		case 51: // Range = 0 (Auto)
			CTSelectRange( 0 );
			TNextEvent += 1000;
			CTState = 52 ;
			T += SysClock;
			break;

		case 52: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 53 ;
			T += SysClock;
			break;

		case 53: // 100 HZ
			CTSelectRFreq(100);
			TNextEvent += 1000;
			CTSelectedCh = 1;
			CTState = 55 ;
			T += SysClock;
			break;

		case 55: // Channel = 1-8
			CTSelectCh( CTSelectedCh );
			TNextEvent = 1000;
			CTState = 56 ;
			T = SysClock;
			break;

		case 56: // Request sample ch 1-8
			CTRequestSample( 3 + CTSelectedCh, Now() );
			{
				CTSelectedCh++;
				if( CTSelectedCh < 8 ) CTState = 55 ;
				else CTState = 900;
			}
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 900:
			T = SysClock;
			TNextEvent = ApplyUpdDelay;
			CTState = 910;
			break;

		case 910:
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			T += SysClock;
			TNextEvent += 200;
			CTState = 920;
			break;

		case 920:
			SensorWarmUp( false );
			MState = MSWarmUp;
			T += SysClock;
			TNextEvent += 200;
			CTState = 950;
			break;

		case 950: // P-Mode
			CTSelectMode( 2 );
			TNextEvent += 1000;
			CTState = 951 ;
			T += SysClock;
			break;

		case 951: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 952 ;
			T += SysClock;
			break;

		case 952: // R Meassuring freq. = 1000 Hz
			CTSelectRFreq(1000);
			TNextEvent += 1000;
			CTState = 953 ;
			T += SysClock;
			break;

		case 953: // Channel = 1;
			CTSelectCh( 1 );
			TNextEvent += 1000;
			CTState = 999 ;
			T += SysClock;
			break;

		case 999:
			StopRecording();
			break;
		}
	}
	else T += SysClock;
}

void __fastcall TCIISZone_Online::SM_RExt()
{
  if( T >= TNextEvent)
  {
	switch (CTState)
	{
			// R-mode

		case 10: // R-Mode
			CTSelectMode( 0 );
			TNextEvent += 1000;
			CTState = 51 ;
			T += SysClock;
			break;

		case 51: // Range = 0
			CTSelectRange( 0 );
			TNextEvent += 1000;
			CTState = 52 ;
			T += SysClock;
			break;

		case 52: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 53 ;
			T += SysClock;
			break;

		case 53: // 100 HZ
			CTSelectRFreq(100);
			TNextEvent += 1000;
			CTSelectedCh = 1;
			CTState = 55 ;
			T += SysClock;
			break;

		case 55: // Channel = 1-3
			CTSelectCh( CTSelectedCh );
			TNextEvent = 1000;
			CTState = 56 ;
			T = SysClock;
			break;

		case 56: // Request sample ch 1-8
			CTRequestSample( 28 + CTSelectedCh, Now() );
			{
				CTSelectedCh++;
				if( CTSelectedCh < 3 ) CTState = 55 ;
				else CTState = 900;
			}
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 900:
			T = SysClock;
			TNextEvent = ApplyUpdDelay;
			CTState = 910;
			break;

		case 910:
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			T += SysClock;
			TNextEvent += 200;
			CTState = 950;
			break;

		case 950: // P-Mode
			CTSelectMode( 2 );
			TNextEvent += 1000;
			CTState = 951 ;
			T += SysClock;
			break;

		case 951: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 952 ;
			T += SysClock;
			break;

		case 952: // R Meassuring freq. = 1000 Hz
			CTSelectRFreq(1000);
			TNextEvent += 1000;
			CTState = 953 ;
			T += SysClock;
			break;

		case 953: // Channel = 1;
			CTSelectCh( 1 );
			TNextEvent += 1000;
			CTState = 999 ;
			T += SysClock;
			break;

		case 999:
			StopRecording();
			break;
		}
  }
  else T += SysClock;
}

void __fastcall TCIISZone_Online::SM_CTStat()
{
  if( T >= TNextEvent)
  {
	switch (CTState)
	{
		case 10: // MSWarmUp
			SensorWarmUp( true );
			TNextEvent += TWarmUp;
			CTState = 20;
			T += SysClock;
			break;

		case 20: // P-Mode
			CTSelectMode( 2 );
			TNextEvent += 1000;
			CTState = 21 ;
			T += SysClock;
			break;

		case 21: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 22 ;
			T += SysClock;
			break;

		case 22: // R Meassuring freq. = 1000 Hz
			CTSelectRFreq(1000);
			TNextEvent += 1000;
			CTState = 23 ;
			T += SysClock;
			break;

		case 23: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 24 ;
			T += SysClock;
			break;

		case 24: // Adj. Offset
			CTOffsetAdj();
			TNextEvent += 2000;
			CTSelectedCh = 1;
			CTState = 25 ;
			T += SysClock;
			break;

		case 25: // Calibrate DA
			CTDACalib();
			TNextEvent += 3000;
			CTState = 26 ;
			T += SysClock;
			break;

		case 26: // Request HUM
			HUMRequestSample( 1, Now() );
			TNextEvent += 2000;
			CTState = 27 ;
			T += SysClock;
			break;

		case 27: // Request Temp
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTRequestTemp( 1, Now() );
			TNextEvent += 3000;
			CTSelectedCh = 1;
			CTState = 30 ;
			T += SysClock;
			break;

			// P-mode

		case 30: // Channel = 1-8
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTSelectCh( CTSelectedCh );
			TNextEvent += 1000;
			CTState = 31 ;
			T += SysClock;
			break;

		case 31: // Request sample ch 1-8
			CTRequestSample( 17 + CTSelectedCh, Now() );
			TNextEvent += 1000;
			CTSelectedCh++;
			if( CTSelectedCh < 9 ) CTState = 30 ;
			else CTState = 40;
			T += SysClock;
			break;

			// ZRA-mode

		case 40: // ZRA-Mode
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			CTSelectMode( 1 );
			TNextEvent += 1000;
			CTState = 41 ;
			T += SysClock;
			break;

		case 41: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 1000;
			CTState = 42 ;
			T += SysClock;
			break;

		case 42: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 1000;
			CTState = 43 ;
			T += SysClock;
			break;

		case 43: // Adj. Offset
			CTOffsetAdj();
			TNextEvent += 1000;
			CTSelectedCh = 1;
			CTState = 45 ;
			T += SysClock;
			break;

		case 45: // Channel = 1-8
			CTSelectCh( CTSelectedCh );
			TNextEvent = 1000;
			CTState = 46 ;
			T = SysClock;
			break;

		case 46: // Request sample ch 1-8
			CTRequestSample( 10 + CTSelectedCh, Now() );
			TNextEvent += 2000;
			CTState = 47;
			T += SysClock;
			break;

		case 47:
			if( T < 10000 ) TNextEvent += 3000;
			else if( T < 30000 ) TNextEvent += 8000;
			else if( T < 60000 ) TNextEvent += 28000;
			else  TNextEvent += 58000;
			if( T < 120000 ) CTState = 46;
			else
			{
			if( CTZRAChange() >= 0.05 ) CTState = 46;
			else
			{
				CTSelectedCh++;
				T = SysClock;
				TNextEvent = 1000;
				if( CTSelectedCh < 8 ) CTState = 45;
				else CTState = 50;
			}

			}
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			T += SysClock;
			break;

			// R-mode

		case 50: // R-Mode
			CTSelectMode( 0 );
			TNextEvent += 500;
			CTState = 51 ;
			T += SysClock;
			break;

		case 51: // Range = 0 Autorange
			CTSelectRange( 0 );
			TNextEvent += 500;
			CTState = 52 ;
			T += SysClock;
			break;

		case 52: // Channel = 0;
			CTSelectCh( 0 );
			TNextEvent += 500;
			CTState = 53 ;
			T += SysClock;
			break;

		case 53: // 100 HZ
			CTSelectRFreq(100);
			TNextEvent += 500;
			CTSelectedCh = 1;
			CTState = 55 ;
			T += SysClock;
			break;

		case 55: // Channel = 1-8
			CTSelectCh( CTSelectedCh );
			TNextEvent = 200;
			CTState = 56 ;
			T = SysClock;
			break;

		case 56: // Request sample ch 1-8
			CTRequestSample( 3 + CTSelectedCh, Now() );
			{
			CTSelectedCh++;
			if( CTSelectedCh < 8 ) CTState = 55 ;
			else CTState = 900;
			}
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 900:
			T = SysClock;
			TNextEvent = ApplyUpdDelay;
			CTState = 910;
			break;

		case 910:
			DB->ApplyUpdatesMonitorValue();
			DB->ApplyUpdatesSensor();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			T += SysClock;
			TNextEvent += 200;
			CTState = 920;
			break;

		case 920:
			SensorWarmUp( false );
			MState = MSWarmUp;
				T += SysClock;
			TNextEvent += 200;
			CTState = 950;
			break;

		case 950: // P-Mode
			CTSelectMode( 2 );
			TNextEvent += 500;
			CTState = 951 ;
			T += SysClock;
			break;

		case 951: // Range = -1 -> Selected by user
			CTSelectRange( -1 );
			TNextEvent += 500;
			CTState = 952 ;
			T += SysClock;
			break;

		case 952: // R Meassuring freq. = 1000 Hz
			CTSelectRFreq(1000);
			TNextEvent += 500;
			CTState = 953 ;
			T += SysClock;
			break;

		case 953: // Channel = 1;
			CTSelectCh( 1 );
			TNextEvent += 500;
			CTState = 999 ;
			T += SysClock;
			break;

		case 999:
			StopRecording();
			break;
		}
  }
  else T += SysClock;
}

int32_t __fastcall TCIISZone_Online::MaxSensorWarmUp()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  int32_t MSWU;

  MSWU = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  MSWU = max( MSWU, S->SensorWarmUpTime );
	}
  }
  return MSWU;
}

void __fastcall TCIISZone_Online::SensorWarmUp( bool On )
 {
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  S->SensorWarmUp(On);
	  //if( S->SensorWarmUp != 0 ) CIISBusInt->SetDigOut( S->CanAdr, 0, On );
	}
  }
 }

void __fastcall TCIISZone_Online::RequestSample( TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	RequestTag++;
  if( RequestTag > 200 ) RequestTag = 1;
  ExpectedTag = RequestTag;
	SampleTimeStamp = STime;
	CIISBusInt->RequestSample( ZoneRec->ZoneCanAdr, RequestTag );

  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->SampReqRunnig = true;
		}
	}

}

void __fastcall TCIISZone_Online::SetIniDecaySample( bool IniDecaySample )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->IniDecaySample = IniDecaySample;

		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->IniDecaySample = IniDecaySample;
		}
	}
}

//RunDistDecay


void __fastcall TCIISZone_Online::SetRunDistDecay( bool RDD )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->RunDistDecay = RDD;

		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->RunDistDecay = RDD;
		}
	}
}

int32_t __fastcall TCIISZone_Online::GetLPRVersionInZone()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	int32_t Ver;

	Ver = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_LPR || S->SensorType == Camur_II_LPRExt )
			{
				if( S->VerMajor <= 1 && S->VerMinor <= 8 ) Ver = 1;
				else Ver = 2;
				break;
			}
			else if(( S->SensorType == Camur_II_CW ) || ( S->SensorType == Camur_II_Ladder ))
			{
				Ver = 3;
				break;
			}
		}
	}
	return Ver;
}

void __fastcall TCIISZone_Online::PSSetOutputOff()
{
  TCIISObj *CIISObj;
  TCIISPowerSupply *PS;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->PSSetOutputOff();
		}
  }
}

void __fastcall TCIISZone_Online::PSSetOutputState()
{
	TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->PSSetOutputState();
		}
	}
}

void __fastcall TCIISZone_Online::PSOnAlarm()
{
  TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->PSOnAlarm();
		}
	}
}

//CT-Nodes
void __fastcall TCIISZone_Online::CTSelectMode( int32_t Mode )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CT )
			{
				S->CTSelectMode( Mode );
			}
		}
  }
}

void __fastcall TCIISZone_Online::CTSelectRange( int32_t Range )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  if( S->SensorType == Camur_II_CT )
	  {
		S->CTSelectRange( Range );
	  }
	}
  }
}

void __fastcall TCIISZone_Online::CTSelectRFreq( int32_t Freq )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  if( S->SensorType == Camur_II_CT )
	  {
		S->CTSelectRFreq( Freq );
	  }
	}
  }
}

void __fastcall TCIISZone_Online::CTSelectCh( int32_t Ch )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  if( S->SensorType == Camur_II_CT )
	  {
		S->CTSelectCh( Ch );
	  }
	}
  }
}

void __fastcall TCIISZone_Online::CTDACalib()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  if( S->SensorType == Camur_II_CT )
	  {
		S->CTDACalib();
	  }
	}
  }
}

void __fastcall TCIISZone_Online::CTOffsetAdj()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISSensors ) )
	{
	  S = (TCIISSensor*)CIISObj;
	  if( S->SensorType == Camur_II_CT )
	  {
		S->CTOffsetAdj();
	  }
	}
  }
}

void __fastcall TCIISZone_Online::CTRequestTemp( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CT )
			{
				S->CTRequestTemp( CTTag );
			}
		}
	}
}

void __fastcall TCIISZone_Online::CTRequestSample( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CT )
			{
				S->CTRequestSample( CTTag );
			}
		}
  }
}

double __fastcall TCIISZone_Online::CTZRAChange()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  double Change, MaxChange;

  MaxChange = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CT )
			{
				Change = S->CTZRAChange();
				if( Change > MaxChange) MaxChange = Change;
			}
		}
  }
  return MaxChange;
}

void __fastcall TCIISZone_Online::CheckSampleRecived( int32_t RequestedTag )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->CheckSampleRecived( RequestedTag );
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->CheckSampleRecived( RequestedTag );
		}
	}
}

void __fastcall TCIISZone_Online::ResetLastStoredTag()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  TCIISPowerSupply *PS;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->ResetLastStoredTag();
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->ResetLastStoredTag();
		}
	}
}

//HUM-Nodes

void __fastcall TCIISZone_Online::HUMRequestSample( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_HUM )
			{
				S->HUMRequestSample( CTTag );
			}
		}
	}
}

//CW-Nodes

bool __fastcall TCIISZone_Online::CWInZone()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool InZone;

  InZone = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
			InZone = true;
			break;
			}
		}
	}
  return InZone;
}

int32_t __fastcall TCIISZone_Online::GetMaxCWChCount()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	int32_t Count;

	Count = 1;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				Count = max( Count, S->SensorChannelCount );
			}
		}
	}
	return Count;
}

bool __fastcall TCIISZone_Online::CWUpdatingLastValue()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool InZone;

  InZone = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				InZone = S->UpdatingLastValue;
				if( InZone ) break;
			}
		}
	}
  return InZone;
}

void __fastcall TCIISZone_Online::CWSelectMode( int32_t Mode )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTSelectMode( Mode );
			}
		}
  }
}

void __fastcall TCIISZone_Online::CWSelectRange( int32_t Range )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTSelectRange( Range );
			}
		}
	}
}

void __fastcall TCIISZone_Online::CWSelectRFreq( int32_t Freq )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTSelectRFreq( Freq );
			}
		}
  }
}

void __fastcall TCIISZone_Online::CWSelectCh( int32_t Ch )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTSelectCh( Ch );
			}
		}
  }
}

void __fastcall TCIISZone_Online::CWDACalib()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTDACalib();
			}
		}
  }
}

void __fastcall TCIISZone_Online::CWOffsetAdj()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTOffsetAdj();
			}
		}
  }
}

void __fastcall TCIISZone_Online::CWRequestTemp( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTRequestTemp( CTTag );
			}
		}
	}
}

void __fastcall TCIISZone_Online::CWRequestSample( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTRequestSample( CTTag );
			}
		}
	}
}

#pragma argsused
void __fastcall TCIISZone_Online::CWRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode)
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->CTRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode  );
			}
		}
	}
}

void __fastcall TCIISZone_Online::CWUpdateLastValues()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_CW )
			{
				S->UpdateLastValues();
			}
		}
	}
}


// MRE-Nodes

bool __fastcall TCIISZone_Online::MREInZone()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool InZone;

  InZone = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				InZone = true;
				break;
			}
		}
	}
  return InZone;
}

bool __fastcall TCIISZone_Online::MREUpdatingLastValue()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool InZone;

  InZone = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				InZone = S->UpdatingLastValue;
				if( InZone ) break;
			}
		}
	}
  return InZone;
}

void __fastcall TCIISZone_Online::MRESelectMode( int32_t Mode )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTSelectMode( Mode );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MRESelectRange( int32_t Range )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTSelectRange( Range );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MRESelectRFreq( int32_t Freq )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTSelectRFreq( Freq );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MRESelectCh( int32_t Ch )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTSelectCh( Ch );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MREDACalib()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTDACalib();
			}
		}
  }
}

void __fastcall TCIISZone_Online::MREOffsetAdj()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTOffsetAdj();
			}
		}
  }
}

void __fastcall TCIISZone_Online::MRERequestTemp( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTRequestTemp( CTTag );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MRERequestSample( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->CTRequestSample( CTTag );
			}
		}
	}
}

void __fastcall TCIISZone_Online::MREUpdateLastValues()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_MRE )
			{
				S->UpdateLastValues();
			}
		}
  }
}

// Ladder

bool __fastcall TCIISZone_Online::LadderInZone()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
  bool InZone;

  InZone = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				InZone = true;
				break;
			}
		}
  }
  return InZone;
}

bool __fastcall TCIISZone_Online::LadderUpdatingLastValue()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
  bool UpdateRunning;

  UpdateRunning = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				UpdateRunning = S->UpdatingLastValue;
				if( UpdateRunning ) break;
			}
		}
	}
	return UpdateRunning;
}

void __fastcall TCIISZone_Online::LadderSelectMode( int32_t Mode )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTSelectMode( Mode );
			}
		}
  }
}

void __fastcall TCIISZone_Online::LadderSelectRange( int32_t Range )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTSelectRange( Range );
			}
		}
  }
}

void __fastcall TCIISZone_Online::LadderSelectRFreq( int32_t Freq )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTSelectRFreq( Freq );
			}
		}
  }
}

void __fastcall TCIISZone_Online::LadderSelectCh( int32_t Ch )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTSelectCh( Ch );
			}
		}
	}
}

void __fastcall TCIISZone_Online::LadderDACalib()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTDACalib();
			}
		}
  }
}

void __fastcall TCIISZone_Online::LadderOffsetAdj()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTOffsetAdj();
			}
		}
	}
}

void __fastcall TCIISZone_Online::LadderRequestTemp( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTRequestTemp( CTTag );
			}
		}
  }
}

void __fastcall TCIISZone_Online::LadderRequestSample( int32_t CTTag, TDateTime STime )
{
  TCIISObj *CIISObj;
  TCIISSensor *S;

	ExpectedTag = CTTag;
	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTRequestSample( CTTag );
			}
		}
  }
}



// Power supply temp

bool __fastcall TCIISZone_Online::PSTempInZone()
{
  TCIISObj *CIISObj;
	TCIISPowerSupply *PS;
	bool InZone;

	InZone = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ))
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->PSTemp )
			{
				InZone = true;
				break;
			}
		}

	}
	return InZone;
}

bool __fastcall TCIISZone_Online::PSUpdatingLastValue()
{
  TCIISObj *CIISObj;
	TCIISPowerSupply *PS;
  bool UpdateRunning;

  UpdateRunning = false;
  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			UpdateRunning = PS->UpdatingLastValue;
			if( UpdateRunning ) break;

		}
	}
	return UpdateRunning;
}

void __fastcall TCIISZone_Online::PSUpdateLastTempValues()
{
	TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->PSTemp )
			{
				PS->UpdateLastTempValues();
			}
		}
	}
}

void __fastcall TCIISZone_Online::PSRequestTemp( TDateTime STime )
{
  TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->PSTemp )
			{
				PS->PSRequestTemp();
			}
		}
	}
}

// CTx1 temp

bool __fastcall TCIISZone_Online::CTx1TempInZone()
{
  TCIISObj *CIISObj;
	TCIISSensor *S;
	bool InZone;

	InZone = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ))
			{
				S = (TCIISSensor*)CIISObj;
				if( S->SensorTemp &&
						(
							( S->SensorType == Camur_II_LPR ) ||
							( S->SensorType == Camur_II_ZRA ) ||
							( S->SensorType == Camur_II_R )   ||
							( S->SensorType == Camur_II_PT )
						)
					)
				{
  				InZone = true;
					break;
				}
			}
	}
	return InZone;
}

bool __fastcall TCIISZone_Online::CTx1UpdatingLastValue()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	bool InZone;

	InZone = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorTemp &&
					(
						( S->SensorType == Camur_II_LPR ) ||
						( S->SensorType == Camur_II_ZRA ) ||
						( S->SensorType == Camur_II_R )   ||
						( S->SensorType == Camur_II_Wenner )   ||
						( S->SensorType == Camur_II_PT )
					)
				)
			{
				InZone = S->UpdatingLastValue;
				if( InZone ) break;
			}
		}
	}
	return InZone;
}


bool __fastcall TCIISZone_Online::CTx1UpdatingTemp()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	bool Updating;

	Updating = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorTemp &&
					(
						( S->SensorType == Camur_II_LPR ) ||
						( S->SensorType == Camur_II_ZRA ) ||
						( S->SensorType == Camur_II_R )   ||
						( S->SensorType == Camur_II_PT )
					)
				)
			{
				Updating = S->CTUpdatingTemp();
				if( Updating ) break;
			}
		}
	}
	return Updating;
}

void __fastcall TCIISZone_Online::CTx1UpdateLastTempValues()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorTemp &&
					(
						( S->SensorType == Camur_II_LPR ) ||
						( S->SensorType == Camur_II_ZRA ) ||
						( S->SensorType == Camur_II_R )   ||
						( S->SensorType == Camur_II_PT )
					)
				)
			{
				S->UpdateLastTempValues();
			}
		}
	}
}

void __fastcall TCIISZone_Online::CTx1RequestTemp( int32_t CTTag, TDateTime STime )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	SampleTimeStamp = STime;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorTemp &&
					(
						( S->SensorType == Camur_II_LPR ) ||
						( S->SensorType == Camur_II_ZRA ) ||
						( S->SensorType == Camur_II_R )   ||
						( S->SensorType == Camur_II_PT )
					)
				)
			{
				S->CTRequestTemp( CTTag );
			}
		}
	}
}


#pragma argsused
void __fastcall TCIISZone_Online::LadderRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode)
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->CTRequestLPRStart( LPRRange, LPRStep, LPRTOn, LPRTOff, RR->LPRMode  );
			}
		}
	}
}

void __fastcall TCIISZone_Online::LadderUpdateLastValues()
{
	TCIISObj *CIISObj;
	TCIISSensor *S;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SensorType == Camur_II_Ladder )
			{
				S->UpdateLastValues();
			}
		}
	}
}


	// Zone_uCtrl

__fastcall TCIISZone_uCtrl::TCIISZone_uCtrl(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
																TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
																TObject *SetCIISParent, bool NewZone )
						:TCIISZone( SetDB, SetCIISBusInt, SetDebugWin, SetZoneRec, SetCIISParent, NewZone )
{

	ZoneRec->uCtrlConnected = false;
	uCtrlRecStatus = 0;
	uCtrlSampleTimeStamp = CIISStrToDateTime("1980-01-01 00:00:00");
	uCtrlDataLenght = 0;
	uCtrlInPointer = 0;
	DebugCnt = 0;
	DebugStr = "";
}

__fastcall TCIISZone_uCtrl::~TCIISZone_uCtrl()
{
}

bool __fastcall TCIISZone_uCtrl::ResetCIISBus()
{
  TCIISObj *CIISObj;
  TCIISSensor *S;
	TCIISPowerSupply *PS;
	TCIISAlarm *Alarm;

	ZoneRec->ZoneCanAdr = CIISBusInt->GetZonCanAdr();
  if( DB->LocateZoneRec( ZoneRec ) )
  {
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	}

	// Save status for recording and schedule for restart functions.

	ZoneRec->RecType = RT_StandBy;
	SMRunning_RestartZone = false;

  OnSlowClockTick = 200;

  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			S->ResetCIISBus();
			S->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
		else if( CIISObj->CIISObjIs( CIISAlarm ) )
		{
			Alarm = (TCIISAlarm*)CIISObj;
			Alarm->ResetCIISBus();
			Alarm->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			PS->ResetCIISBus();
			PS->ZoneCanAdr = ZoneRec->ZoneCanAdr;
		}
	}

	DB->ApplyUpdatesSensor();
	DB->ApplyUpdatesPS();

  return true;
}

void __fastcall TCIISZone_uCtrl::RestartZone()
{
	OrgIncommingBIChMessage = CIISBICh->GetPMBI();
	CIISBICh->SetPMBI( &IncomingBIChMessage );

	State_RestartZone = 100;
	SMRunning_RestartZone = true;
	TRestart = 0;
	TNextEvent_Restart = 0;
	uCtrlInPointer = 0;
}

void __fastcall TCIISZone_uCtrl::SM_RestartuCtrl()
{
	if( TRestart >= TNextEvent_Restart )
	{
		switch( State_RestartZone )
		{
			case 100:
				CIISBICh->RequestNodeCount();
				uCtrlNodeIndex = 0;
				TNextEvent_Restart += 1000;
				State_RestartZone = 110;
				break;

			case 110:
				if( uCtrlNodeIndex < uCtrlNodeCount )
				{
					InstalluCtrlNodeReady = false;
					CIISBICh->RequestNodeInfo(uCtrlNodeIndex);
					State_RestartZone = 120;
					TNextEvent_Restart += 1000;
				}
				else
				{
					TNextEvent_Restart += 100;
					State_RestartZone = 130;
        }

				break;

			case 120:
				if( InstalluCtrlNodeReady )
				{
					uCtrlNodeIndex++;
					TNextEvent_Restart += 500;
					State_RestartZone = 110;
				}
				else
				{
					TNextEvent_Restart += 1000;
					State_RestartZone = 120;
				}

				break;

			case 130:
				CIISBICh->RequestPowerSetting();
				ReaduCtrlPowerSettingReady = false;
				TNextEvent_Restart += 1000;
				State_RestartZone = 140;
				break;

			case 140:
					if( ReaduCtrlPowerSettingReady )
					{
						TNextEvent_Restart += 100;
						State_RestartZone = 150;
					}
					else
					{
						TNextEvent_Restart += 1000;
						State_RestartZone = 140;
					}

					break;

			case 150:
				P_Prj->uCtrlBytesRead = 0;
				P_Prj->uCtrlSampleTimeStamp = CIISStrToDateTime("1980-01-01 00:00:00");

				CIISBICh->RequestRecStatus();
				ReaduCtrlRecStatus = false;
				TNextEvent_Restart += 1000;
				State_RestartZone = 160;
				break;

			case 160:
				if( ReaduCtrlRecStatus )
				{
					ZoneRec->RecType = uCtrlRecStatus;

					if( DB->LocateZoneRec( ZoneRec ) )
					{
						DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
					}

					if( uCtrlRecStatus == 1 ) P_Prj->uCtrlRecordingOn = true;
					else P_Prj->uCtrlRecordingOn = false;
					P_Ctrl->UpdateNodeCountInZones();
					TNextEvent_Restart += 100;
					State_RestartZone = 200; // Skip datadump 900 200;
				}
				else
				{
					TNextEvent_Restart += 1000;
					State_RestartZone = 140;
				}

				break;

			case 200:  // Read data

				if( DB->LocateLastRecNo( ZoneRec->ZoneNo ) )
				{
					DB->GetRecordingRec( RR, false );
				}
				else
				{
					RR->RecStart = CIISStrToDateTime("1980-01-01 00:00:00");
					RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
        }
				P_Prj->uCtrlReadingData = true;
				CIISBICh->RequestData( uCtrlInPointer );
				ReaduCtrlDataReady = false;

				TNextEvent_Restart += 2000;
				State_RestartZone = 210;
				break;

			case 210:
				if( ReaduCtrlDataReady )
				{
					if( DB->LocateRecordingNo( RR->RecNo ) )
					{
						DB->SetRecordingRec( RR ); DB->ApplyUpdatesRecording();
					}

					DB->ApplyUpdatesMonitorValue();
					P_Prj->DataChanged();
					DB->ApplyUpdatesPrj();

					P_Prj->uCtrlReadingData = false;
					TNextEvent_Restart += 500;
					State_RestartZone = 900;
				}
				else
				{
					TNextEvent_Restart += 1000;
					State_RestartZone = 210;
				}
				P_Prj->uCtrlBytesRead = uCtrlInPointer;
				P_Prj->uCtrlSampleTimeStamp = uCtrlSampleTimeStamp;
				break;

			case 900:
				SMRunning_RestartZone = false;;
				break;
		}
	}
	TRestart += SysClock;
}

#pragma argsused
void __fastcall TCIISZone_uCtrl::OnSysClockTick( TDateTime TickTime )
{
	if( SMRunning_RestartZone ) SM_RestartuCtrl();

	OnSlowClockTick--;
	if( OnSlowClockTick < 0 )
	{
		OnSlowClockTick = 20;
		if( !SMRunning_RestartZone && ZoneRec->uCtrlConnected )
		{
			if( P_Prj->uCtrlSyncTime )
			{
				Word Year, Month, Day, Hour, Minute, Sec, MSec;
				TDateTime NewuCtrlTime;

				NewuCtrlTime = Now();

				DecodeDate(NewuCtrlTime, Year, Month, Day);
				DecodeTime(NewuCtrlTime, Hour, Minute, Sec, MSec);

				CIISBICh->SetBITime( CIISIntToBcd(Year - 2000) , CIISIntToBcd(Month), CIISIntToBcd(Day), CIISIntToBcd(Hour), CIISIntToBcd(Minute), CIISIntToBcd(Sec));

				P_Prj->uCtrlSyncTime = false;
			}
			else CIISBICh->RequestBITime();

			if( P_Prj->uCtrlSyncData )
			{
				State_RestartZone = 200;
				SMRunning_RestartZone = true;
				TRestart = 0;
				TNextEvent_Restart = 0;
				P_Prj->uCtrlSyncData = false;
      }
		}
	}
	else if( OnSlowClockTick == 10 )
	{
		if( !SMRunning_RestartZone && ZoneRec->uCtrlConnected )
		{
			CIISBICh->RequestLastValueUpdate();
		}
	}
}

void __fastcall TCIISZone_uCtrl::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
	switch (BPIn->MessageCommand)
	{
	case MSG_TX_BIMode:

		CIISBICh = (TCIISBIChannel*)BPIn->GetBIChannel();
		if(ZoneRec->BIChSerNo == CIISBICh->BusInterfaceSerialNo)
		{
			if( BPIn->BIMode == 1 )
			{
				ZoneRec->uCtrlConnected = true;
			}
			else
			{
				ZoneRec->uCtrlConnected = false;
			}
			BPIn->MsgMode = CIISBus_MsgReady;
			BPIn->ParseMode = CIISParseReady;
		}
		break;

	default:
	  break;
	}
}

#pragma argsused
void __fastcall TCIISZone_uCtrl::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISZone_uCtrl::StartSchedule()
{

}

int32_t __fastcall TCIISZone_uCtrl::GetNodeCount()
{
	return uCtrlNodeCount;
}

void __fastcall TCIISZone_uCtrl::UpdateLastValues()
{
	for( int32_t i = 0; i < uCtrlNodeCount; i++ )
	{
		RequestuCtrlNodeInfo( i );
	}
}

bool __fastcall TCIISZone_uCtrl::StartMonitor()
{
	if( ZoneRec->RecType != RT_StandBy ) return false;

	CIISBICh->StartRecording();

	State_RestartZone = 150;
	SMRunning_RestartZone = true;
	TRestart = 0;
	TNextEvent_Restart = 1000;
	uCtrlInPointer = 0;

	return true;
}

bool __fastcall TCIISZone_uCtrl::StartMonitorExtended()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartScheduledValues()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartDecay()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartLPR()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartCTNonStat()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartCTStat()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StartRExt()
{
	return false;
}

bool __fastcall TCIISZone_uCtrl::StopRecording()
{
	if( ZoneRec->RecType == 0 ) return false;

	CIISBICh->StopRecording();

	State_RestartZone = 150;
	SMRunning_RestartZone = true;
	TRestart = 0;
	TNextEvent_Restart = 1000;


  return true;
}

void __fastcall TCIISZone_uCtrl::PrepareShutdown()
{

}

/*
  void __fastcall TCIISZone_uCtrl::SetBIChannel( TCIISBIChannel *SetBICh )
  {
		BICh = SetBICh;
	}
*/

#pragma argsused
void __fastcall TCIISZone_uCtrl::IncomingBIChMessage(const Byte *BusIntMsg, PVOID BIChannel)
{
	switch (BusIntMsg[0])
	{
		case MSG_TX_uCtrlTime:
			try
			{
				P_Prj->uCtrlTime = EncodeDate( 2000 + CIISBcdToInt( BusIntMsg[2] ),
																							CIISBcdToInt( BusIntMsg[3] ),
																							CIISBcdToInt( BusIntMsg[4] ) ) +
																	EncodeTime( CIISBcdToInt( BusIntMsg[5] ),
																							CIISBcdToInt( BusIntMsg[6] ),
																							CIISBcdToInt( BusIntMsg[7] ),
																							0 );
			}
			catch (Exception &exception)
			{
				P_Prj->uCtrlTime = CIISStrToDateTime("1980-01-01 00:00:00");
			}

			#if DebuguCtrl == 1
			Debug(  "MSG_TX_uCtrlTime: " + DateTimeToStr( P_Prj->uCtrlTime ) );
			#endif

			break;

		case MSG_TX_uCtrlPowerSettings:
			ReaduCtrlPowerSettings( BusIntMsg );

			#if DebuguCtrl == 1
				Debug(  "MSG_TX_uCtrlPowerSettings (bits) V = " + IntToStr((int16_t)BusIntMsg[2] + (int16_t)BusIntMsg[3]*256 ) +
																								" I = " + IntToStr((int16_t)BusIntMsg[4] + (int16_t)BusIntMsg[5]*256 ) +
																								" Mode = " + IntToStr( BusIntMsg[6] ));
			#endif
			break;

		case MSG_TX_uCtrlNodeCount:
			uCtrlNodeCount = BusIntMsg[2];

			#if DebuguCtrl == 1
				Debug(  "uCtrlNodeCount = " + IntToStr( BusIntMsg[2] ));
			#endif
			break;

		case MSG_TX_uCtrlNodeInfo:
			if( !InstalluCtrlNodeReady )InstalluCtrlNode( BusIntMsg );
			else UpdateuCtrlNode( BusIntMsg );

			#if DebuguCtrl == 1
				Debug(  "MSG_TX_uCtrlNodeInfo = " + IntToStr( BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536 ));
			#endif
			break;

		case MSG_TX_uCtrlRecStatus:
			uCtrlRecStatus = BusIntMsg[2];
			ReaduCtrlRecStatus = true;

			#if DebuguCtrl == 1
				Debug(  "MSG_TX_uCtrlRecStatus = " + IntToStr( uCtrlRecStatus ));
			#endif
			break;

		case MSG_TX_uCtrlDataLenght:
			uCtrlDataLenght = BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536 + BusIntMsg[5]*16777216;;
			if( uCtrlInPointer == uCtrlDataLenght ) ReaduCtrlDataReady = true;

			#if DebuguCtrl == 1
				Debug(  "MSG_TX_uCtrlDataLenght = " + IntToStr( uCtrlDataLenght ));
				Debug(  "uCtrlInPointer = " + IntToStr( uCtrlInPointer ));
				DebugCnt = 0;
			#endif
			break;

		case MSG_TX_uCtrlData:
			int32_t DataLenght;

			DataLenght = BusIntMsg[1];

			#if DebuguCtrl == 1
			if( DebugCnt == 0 )
			{
				Debug(  "First datapack recived Lengt = " + IntToStr( DataLenght ));
				for( int32_t i = 0; i < DataLenght; i++ )
				{
					if( BusIntMsg[i + 2] == 25 )
					{
						if( ( BusIntMsg[i + 3] == 4 ) && ( BusIntMsg[i + 4] == 17 ) )
						{
							Debug( int32_tToStr( BusIntMsg[i + 2] ) + "/" +
										 IntToStr( BusIntMsg[i + 3] ) + "/" +
										 IntToStr( BusIntMsg[i + 4] ) + "/" +
										 IntToStr( BusIntMsg[i + 5] ) + "/" +
										 IntToStr( BusIntMsg[i + 6] ) + "/" +
										 IntToStr( BusIntMsg[i + 7] ) + "/" );

							uCtrlSampleTimeStamp =  EncodeDate( 2000 + CIISBcdToInt( BusIntMsg[i+2] ),
																									CIISBcdToInt( BusIntMsg[i+3] ),
																									CIISBcdToInt( BusIntMsg[i+4] ) ) +
																			EncodeTime( CIISBcdToInt( BusIntMsg[i+5] ),
																									CIISBcdToInt( BusIntMsg[i+6] ),
																									CIISBcdToInt( BusIntMsg[i+7] ),
																									0 );

							Debug(  "uCtrlSampleTimeStamp: " + DateTimeToStr( uCtrlSampleTimeStamp ) );

						}

					}

				}
			}
			DebugCnt++;

			if( DebugCnt % 100 == 0 )
			{
/*
  				for( int32_t i = 0; i < DataLenght; i++ )
  				{
  					Debug( int32_tToStr( BusIntMsg[i + 2] ));
					}
*/

				for( int32_t i = 0; i < DataLenght; i++ )
				{
					if( BusIntMsg[i + 2] == 25 )
					{
						if( ( BusIntMsg[i + 3] == 4 ) && ( BusIntMsg[i + 4] == 17 ) )
						{
							Debug( int32_tToStr( BusIntMsg[i + 2] ) + "/" +
										 IntToStr( BusIntMsg[i + 3] ) + "/" +
										 IntToStr( BusIntMsg[i + 4] ) + "/" +
										 IntToStr( BusIntMsg[i + 5] ) + "/" +
										 IntToStr( BusIntMsg[i + 6] ) + "/" +
										 IntToStr( BusIntMsg[i + 7] ) + "/" );

							uCtrlSampleTimeStamp =  EncodeDate( 2000 + CIISBcdToInt( BusIntMsg[i+2] ),
																									CIISBcdToInt( BusIntMsg[i+3] ),
																									CIISBcdToInt( BusIntMsg[i+4] ) ) +
																			EncodeTime( CIISBcdToInt( BusIntMsg[i+5] ),
																									CIISBcdToInt( BusIntMsg[i+6] ),
																									CIISBcdToInt( BusIntMsg[i+7] ),
																									0 );

							Debug(  "uCtrlSampleTimeStamp: " + DateTimeToStr( uCtrlSampleTimeStamp ) );

							Debug(  "i/uCtrlInPointer : " + IntToStr( i ) + " / " + IntToStr (uCtrlInPointer) );
						}

					}

				}

			}

			#endif


			for( int32_t i = 0; i < DataLenght; i++ )
			{
				RecordingMem[ uCtrlInPointer + i] = BusIntMsg[i + 2];
			}
			uCtrlInPointer += DataLenght;

			if( uCtrlInPointer == uCtrlDataLenght )
			{

			#if DebuguCtrl == 1
				Debug(  "Last datapack recived = " + IntToStr( DataLenght ));
				Debug(  "No of packets = " + IntToStr( DebugCnt ));
				DebugCnt = 0;
			#endif
			
			 ReaduCtrlData();
			 ReaduCtrlDataReady = true;
			}
			break;

		case MSG_TX_Debug:
			DebugStr = DebugStr + Char( BusIntMsg[2] );
			if( BusIntMsg[2] == 10 )
			{
				Debug( DebugStr );
				DebugStr = "";
			}
			break;
	}
}

void __fastcall TCIISZone_uCtrl::InstalluCtrlNode( const Byte *BusIntMsg )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;
	bool NodeInstalled;
	int32_t Snr;

	Snr = BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536;

	NodeInstalled = false;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SerialNo == Snr )
			{
				S->uCtrlZone = true;
				S->uCtrlNodeIndex = uCtrlNodeIndex;
				S->uCtrlNodeInfo( BusIntMsg );
				InstalledSensor = S;
				P4Index = uCtrlNodeIndex;
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				NodeInstalled = true;
			}
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->SerialNo == Snr )
			{
				PS->uCtrlZone = true;
				PS->uCtrlNodeIndex = uCtrlNodeIndex;
				PS->uCtrlNodeInfo( BusIntMsg );
				InstalledPS = PS;
				PSIndex = uCtrlNodeIndex;
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				NodeInstalled = true;
			}
		}
	}

	if( !NodeInstalled )  // Add new nodes
	{
		CIISSensorRec *SR;
		CIISPowerSupplyRec *PSR;

		switch( BusIntMsg[5] )
		{
			case 24:

				SR = new CIISSensorRec;
				SR->SensorSerialNo = Snr;
				SR->SensorCanAdr = 0;
				SR->SensorType = 24;
				SR->ZoneNo = ZoneRec->ZoneNo;
				SR->ZoneCanAdr = ZoneRec->ZoneCanAdr;
				SR->SensorVerMajor = 0;
				SR->SensorVerMinor = 0;
				DB->AppendSensorRec( SR ); DB->ApplyUpdatesSensor();

				S = new TCIISSensor_P4(DB, CIISBusInt, DW, SR, this );

				S->SetDefaultValues();
				SR->VerNotSupported = false;   // Versions not supported in uCtrl
				S->CIISBICh = CIISBICh;
				S->IniErrorCount = 0;
				ChildList->Add( S );

				S->uCtrlZone = true;
				S->uCtrlNodeIndex = uCtrlNodeIndex;
				S->uCtrlNodeInfo( BusIntMsg );
				InstalledSensor = S;
				P4Index = uCtrlNodeIndex;
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				break;


			case 31:
				PSR = new CIISPowerSupplyRec;
				PSR->PSSerialNo = Snr;
				PSR->PSCanAdr = 0;
				PSR->PSType = 31;
				PSR->ZoneNo = ZoneRec->ZoneNo;
				PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;
				PSR->PSVerMajor = 0;
				PSR->PSVerMinor = 0;
				DB->AppendPSRec( PSR );

				PS = new TCIISPowerSupply_FixVolt(DB, CIISBusInt, DW, PSR, this );

				PS->SetDefaultValues();
				PSR->VerNotSupported = false;  // Versions not supported in uCtrl

				PS->CIISBICh = CIISBICh;
				PS->IniErrorCount = 0;
				ChildList->Add( PS );

				PS->uCtrlZone = true;
				PS->uCtrlNodeIndex = uCtrlNodeIndex;
				PS->uCtrlNodeInfo( BusIntMsg );
				InstalledPS = PS;
				PSIndex = uCtrlNodeIndex;
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				break;

			case 32:
				PSR = new CIISPowerSupplyRec;
				PSR->PSSerialNo = Snr;
				PSR->PSCanAdr = 0;
				PSR->PSType = 32;
				PSR->ZoneNo = ZoneRec->ZoneNo;
				PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;
				PSR->PSVerMajor = 0;
				PSR->PSVerMinor = 0;
				DB->AppendPSRec( PSR );

				PS = new TCIISPowerSupply_FixVolt3A(DB, CIISBusInt, DW, PSR, this );

				PS->SetDefaultValues();
				PSR->VerNotSupported = false;  // Versions not supported in uCtrl
				PS->CIISBICh = CIISBICh;
				PS->IniErrorCount = 0;
				ChildList->Add( PS );

        PS->uCtrlZone = true;
				PS->uCtrlNodeIndex = uCtrlNodeIndex;
				PS->uCtrlNodeInfo( BusIntMsg );
				InstalledPS = PS;
				PSIndex = uCtrlNodeIndex;
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				break;

			case 33:
				PSR = new CIISPowerSupplyRec;
				PSR->PSSerialNo = Snr;
				PSR->PSCanAdr = 0;
				PSR->PSType = 33;
				PSR->ZoneNo = ZoneRec->ZoneNo;
				PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;
				PSR->PSVerMajor = 0;
				PSR->PSVerMinor = 0;
				DB->AppendPSRec( PSR );

				PS = new TCIISPowerSupply_FixVolt01A(DB, CIISBusInt, DW, PSR, this );

				PS->SetDefaultValues();
				PSR->VerNotSupported = false;  // Versions not supported in uCtrl
				PS->CIISBICh = CIISBICh;
				PS->IniErrorCount = 0;
				ChildList->Add( PS );

				PS->uCtrlZone = true;
				PS->uCtrlNodeIndex = uCtrlNodeIndex;
				PS->uCtrlNodeInfo( BusIntMsg );
				InstalledPS = PS;
				PSIndex = uCtrlNodeIndex;
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				break;

			case 34:
				PSR = new CIISPowerSupplyRec;
				PSR->PSSerialNo = Snr;
				PSR->PSCanAdr = 0;
				PSR->PSType = 34;
				PSR->ZoneNo = ZoneRec->ZoneNo;
				PSR->ZoneCanAdr = ZoneRec->ZoneCanAdr;
				PSR->PSVerMajor = 0;
				PSR->PSVerMinor = 0;
				DB->AppendPSRec( PSR );

				PS = new TCIISPowerSupply_FixVolt1A(DB, CIISBusInt, DW, PSR, this );

				PS->SetDefaultValues();
        PSR->VerNotSupported = false;  // Versions not supported in uCtrl
				PS->CIISBICh = CIISBICh;
				PS->IniErrorCount = 0;
				ChildList->Add( PS );

				PS->uCtrlZone = true;
				PS->uCtrlNodeIndex = uCtrlNodeIndex;
				PS->uCtrlNodeInfo( BusIntMsg );
				InstalledPS = PS;
				PSIndex = uCtrlNodeIndex;
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
				break;
		}
	}
	InstalluCtrlNodeReady = true;
	P_Ctrl->IncDetectedNodeCount();

}

void __fastcall TCIISZone_uCtrl::UpdateuCtrlNode( const Byte *BusIntMsg )
{
	TCIISObj *CIISObj;
	TCIISSensor *S;
	TCIISPowerSupply *PS;
	int32_t Snr;

	Snr = BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISSensors ) )
		{
			S = (TCIISSensor*)CIISObj;
			if( S->SerialNo == Snr )
			{
				S->uCtrlNodeInfo( BusIntMsg );
				DB->ApplyUpdatesSensor();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			}
		}
		else if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS->SerialNo == Snr )
			{
				PS->uCtrlNodeInfo( BusIntMsg );
				DB->ApplyUpdatesPS();
				DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			}
		}
	}
}

void __fastcall TCIISZone_uCtrl::ReaduCtrlPowerSettings( const Byte *BusIntMsg )
{
	TCIISObj *CIISObj;
	TCIISPowerSupply *PS;

	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISPowerSupply ) )
		{
			PS = (TCIISPowerSupply*)CIISObj;
			if( PS == InstalledPS )
			{
				PS->uCtrlPowerSettings( BusIntMsg );
			}
		}
	}

	uCtrlSampInt =  BusIntMsg[8] + BusIntMsg[9]*256;

	ZoneRec->ZoneSampInterval = P_Ctrl->GetSetSampelIntervall( uCtrlSampInt );

	if( DB->LocateZoneRec( ZoneRec ) )
	{
		DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
	}

	ReaduCtrlPowerSettingReady = true;

}

void __fastcall TCIISZone_uCtrl::ReaduCtrlData()
{
	int32_t i, Skip, ApplyCnt;

	i = 0;
	ApplyCnt = 0;
  P_Prj->uCtrlStoringData = true;

		if(( RecordingMem[i] & 128 ) == 0 )
		{
			try
			{
				uCtrlRecStart = EncodeDate( 2000 + CIISBcdToInt( RecordingMem[i+1] ),
																		CIISBcdToInt( RecordingMem[i+2] ),
																		CIISBcdToInt( RecordingMem[i+3] ) ) +
												EncodeTime( CIISBcdToInt( RecordingMem[i+4] ),
																		CIISBcdToInt( RecordingMem[i+5] ),
																		CIISBcdToInt( RecordingMem[i+6] ),
																		0 );

				#if DebuguCtrl == 1
				Debug(  "ReaduCtrlData RecStart: " + DateTimeToStr( uCtrlRecStart ) );
				#endif
			}
			catch (Exception &exception)
			{
				uCtrlRecStart = Now();
				#if DebuguCtrl == 1
				Debug(  "Error ReaduCtrlData RecStart. Set to: " + DateTimeToStr( uCtrlRecStart ) );
				#endif
			}


			if( uCtrlRecStart == RR->RecStart )  //Continue data read of previus recording
			{
				uCtrlDataTransferredUntil = RR->RecStop;

				#if DebuguCtrl == 1
				Debug(  "Continue uCtrlDataTransferredUntil: " + DateTimeToStr( uCtrlDataTransferredUntil ) );
				#endif
			}
			else // New recording
			{
				uCtrlDataTransferredUntil = uCtrlRecStart;

				/*
				//Create new recording...
				if( DB->FindLastRecording() )
				{
					DB->GetRecordingRec( RR, false );
					RR->RecNo = RR->RecNo + 1;
				}
				else RR->RecNo = 1;
				*/
				RR->RecNo = DB->GetRecordingRecCount() + 1;

				RR->RecStart = uCtrlRecStart;
				RR->RecStop = CIISStrToDateTime("1980-01-01 00:00:00");
				RR->RecType = RT_Monitor;
				RR->SampInterval= uCtrlSampInt;
				RR->ZoneNo = ZoneRec->ZoneNo;

				RR->DecaySampInterval = 0;
				RR->DecayDuration = 0;
				RR->LPRRange = 0;
				RR->LPRStep = 0;
				RR->LPRDelay1 = 0;
				RR->LPRDelay2 = 0;
				RR->LPRMode = 0;

				DB->AppendRecordingRec( RR ); DB->ApplyUpdatesRecording();
				ZoneRec->RecNo = RR->RecNo;
				if( DB->LocateZoneRec( ZoneRec ) )
				{
					DB->SetZoneRec( ZoneRec ); DB->ApplyUpdatesZone();
				}

			#if DebuguCtrl == 1
			Debug(  "uCtrl New recording: " + IntToStr( RR->RecNo ) );
			#endif
			}

		}
		else return; // Start datetime missing


	do
	{
		if(( RecordingMem[i] & 128 ) == 0 )
		{
			try
			{
				uCtrlSampleTimeStamp =  EncodeDate( 2000 + CIISBcdToInt( RecordingMem[i+1] ),
										CIISBcdToInt( RecordingMem[i+2] ),
										CIISBcdToInt( RecordingMem[i+3] ) ) +
										EncodeTime( CIISBcdToInt( RecordingMem[i+4] ),
										CIISBcdToInt( RecordingMem[i+5] ),
										CIISBcdToInt( RecordingMem[i+6] ),
										0 );
				Skip = RecordingMem[i+7];

				#if DebuguCtrl == 1
				if( DebugCnt == 0 )
				{
					Debug(  "uCtrlSampleTimeStamp: " + DateTimeToStr( uCtrlSampleTimeStamp ) );
					Debug(  "i : " + IntToStr( i ) );
        }
				DebugCnt++;
				if( DebugCnt >= 99 ) DebugCnt = 0;

				#endif
			}
			catch (Exception &exception)
			{
				Debug( "EncodeError at i = " + IntToStr(i) +
													 "Data = " + IntToStr( RecordingMem[i+1] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+2] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+3] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+4] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+5] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+6] ));

				#if DebuguCtrl == 1
				Debug( "EncodeError at i = " + IntToStr(i) +
													 "Data = " + IntToStr( RecordingMem[i+1] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+2] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+3] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+4] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+5] ) + " / " +
													 "Data = " + IntToStr( RecordingMem[i+6] ));
				#endif
      }

			switch( Skip )
			{
				case 0:
					i += 32;
					break;

				default :
					i += 14;
					break;
			}
		}
		else
		{
			if((( RecordingMem[i] & 127 ) == PSIndex ) && ( uCtrlSampleTimeStamp > uCtrlDataTransferredUntil ))  // FixVolt
			{
					InstalledPS->uCtrlSample( RecordingMem, i, RR->RecNo, uCtrlSampleTimeStamp );
			}
			else if ((( RecordingMem[i] & 127 ) == P4Index ) && ( uCtrlSampleTimeStamp > uCtrlDataTransferredUntil ) ) // P4
			{
					InstalledSensor->uCtrlSample( RecordingMem, i, RR->RecNo, uCtrlSampleTimeStamp );
			}
			else
			{
				//Debug( "Fel index:" + IntToStr( RecordingMem[i] ) + " i= " + IntToStr(i));
			}

			if( ApplyCnt++ > 99 )
			{
				ApplyCnt = 0;
				DB->ApplyUpdatesMonitorValue();
      }

			switch( Skip )
			{
			case 1:
				i += 18;
				break;

			default :
				i += 9;
				break;
			}
		}
	} while( i < uCtrlDataLenght );

	P_Prj->uCtrlSampleTimeStamp = uCtrlSampleTimeStamp;
	P_Prj->uCtrlStoringData = false;
	RR->RecStop = uCtrlSampleTimeStamp;
 }

void __fastcall TCIISZone_uCtrl::RequestuCtrlNodeInfo( int32_t uCtrlNodeIndex )
{
	CIISBICh->RequestNodeInfo(uCtrlNodeIndex);
}

void __fastcall TCIISZone_uCtrl::uCtrlSetPower( double SetVoltage, double SetCurrent,
																 int32_t Mode, bool VOutEnabled)
{
	CIISBICh->SetPower( SetVoltage, SetCurrent, Mode, VOutEnabled, P_Ctrl->GetSampelIntervall( ZoneRec->ZoneSampInterval ) );
}

