//---------------------------------------------------------------------------

#ifndef DebugWinUH
#define DebugWinUH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>

#include "CIISCommon.h"
#include <Vcl.Dialogs.hpp>

#define DebugMain 0
#define DebugClientInt 0
#define DebugNetIntLocal 0
#define DebugUSBCANInt 0
#define DebugRecordings 0
#define DebugNetIntRemote 0
#define DebugServerSocket 0
#define DebugClientSocket 0
#define DebugClientSocketBI 0
#define DebugClientSocketBIEOT 0
#define DebugClientSocketEvent 0
#define DebugLPR 0
#define DebugMsg 0

#define DebugCIISObj 0
#define DebugCIISZone 0
#define DebugCIIDataTable 0
#define DebugCIISBusInt 0
#define DebugCIISBusIntRx 0
#define DebugCIISBusIntTx 0
#define DebugCIISAlarm 0
#define DebugDB 0
#define DebugCIISStart 0
#define DebugCIISSensor 0
#define DebugCIISCorrRate 0
#define DebugCIIApplyUppdate 0
#define DebuguCtrl 0

//---------------------------------------------------------------------------
class TDebugWin : public TForm
{
__published:	// IDE-managed Components
        TStringGrid *StrGridDebug;
        TPanel *PanelButtons;
        TBitBtn *BitBtnClear;
	TSaveDialog *SaveDialog1;
	TBitBtn *BBSaveToFile;
	TBitBtn *BitBtn1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BitBtnClearClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BBSaveToFileClick(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
private:
	int32_t NextRow;	// User declarations
	DebugStateFP ChangeDS;
	int32_t iFileHandle;
	bool WriteToDebugFile;

public:		// User declarations
        __fastcall TDebugWin(TComponent* Owner, DebugStateFP SetDS);
        void __fastcall Log(String Message);
};
//---------------------------------------------------------------------------
extern PACKAGE TDebugWin *DebugWin;
//---------------------------------------------------------------------------
#endif
