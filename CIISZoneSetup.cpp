//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISZoneSetup.h"
#include "ustring.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFrmZoneSetup *FrmZoneSetup;
//---------------------------------------------------------------------------
__fastcall TFrmZoneSetup::TFrmZoneSetup(TComponent* Owner, TCIISZone* SetZone )
	: TForm(Owner)
{
	Zone = SetZone;
	BICh = Zone->GetBIChannel();
	BIChVersion = "???";
	if( BICh != NULL )
	{
		ZoneIncommingBIChMessage = BICh->GetPMBI();
		BICh->SetPMBI( &IncomingBIChMessage );
	}
	else
	{
		BGetBIChSerial->Enabled = false;
		BGetBIChMode->Enabled = false;
		BGetTime->Enabled = false;
		BStdMode->Enabled = false;
		BuCtrlMode->Enabled = false;
		BSetTime->Enabled = false;
		BReadFixVolt->Enabled = false;
		BSetFixVolt->Enabled = false;
		BStartRec->Enabled = false;
		BStopRec->Enabled = false;
		BGetNodeCount->Enabled = false;
		BGetNodeInfo1->Enabled = false;
		BGetNodeInfo2->Enabled = false;
		//BGetData->Enabled = false;
  }

}

void __fastcall TFrmZoneSetup::FormClose(TObject *Sender, TCloseAction &Action)
{
	if( BICh != NULL ) BICh->SetPMBI( ZoneIncommingBIChMessage );
}

//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BGetBIChSerialClick(TObject *Sender)
{
	LBIChSerialNo->Caption = "???";
	LBIChSerialNo->Caption = BICh->BusInterfaceSerialNo;
	LBIChVer->Caption =  + "Ver: " + IntToStr( BICh->BusInterfaceVerMajor ) + "." + IntToStr( BICh->BusInterfaceVerMinor );

}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BGetBIChModeClick(TObject *Sender)
{
	LBIChMode->Caption = "???";
	BICh->RequestBIMode();
}
//---------------------------------------------------------------------------

void __fastcall TFrmZoneSetup::IncomingBIChMessage(const Byte *BusIntMsg, PVOID BIChannel)
{
 if ( BusIntMsg[0] == 31 )
 {

	 Word Year, Month, Day, Hour, Minute, Sec, MSec;


						Year = CIISBcdToInt( BusIntMsg[2] );
						Month = CIISBcdToInt( BusIntMsg[3] );
						Day = CIISBcdToInt( BusIntMsg[4] );
						Hour = CIISBcdToInt( BusIntMsg[5] );
						Minute = CIISBcdToInt( BusIntMsg[6] );
						Sec = CIISBcdToInt( BusIntMsg[7] );

						LuCtrlTime->Caption =   IntToStr( Year ) + '-'
																	+ IntToStr( Month ) + '-'
																	+ IntToStr( Day ) + ' '
																	+ IntToStr( Hour ) + ':'
																	+ IntToStr( Minute ) + ':'
																	+ IntToStr( Sec );


 }
 else if ( BusIntMsg[0] == 32 )
 {

		EduCtrlVOut->Text = IntToStr( BusIntMsg[2] + BusIntMsg[3]*256 );
		EduCtrlIOut->Text = IntToStr( BusIntMsg[4] + BusIntMsg[5]*256 );

		switch( BusIntMsg[6] )
		{
		case 0:
			RGuCtrlPMode->ItemIndex = 0;
			break;

		case 1:
			RGuCtrlPMode->ItemIndex = 1;
			break;
		}

		switch( BusIntMsg[7] )
		{
		case 0:
			RGuCtrlPOut->ItemIndex = 0;
			break;

		case 1:
			RGuCtrlPOut->ItemIndex = 1;
			break;
		}

		EduCtrlInterval->Text = IntToStr( BusIntMsg[8] + BusIntMsg[9]*256 );

 }
 else if ( BusIntMsg[0] == 33 )
 {
	 TotLenght = BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536 + BusIntMsg[5]*16777216;
	 LStoredValues->Caption = TotLenght;
	 InPointer = 0;
 }
 else if ( BusIntMsg[0] == 34 )
 {

	DataLenght = BusIntMsg[1];

	for( int i = 0; i < DataLenght; i++ )
		RecordingMem[ InPointer + i] = BusIntMsg[i + 2];

	InPointer += DataLenght;

	 if( InPointer == TotLenght )
	 {
			int i, DataLenght, Skip;
			Word Year, Month, Day, Hour, Minute, Sec, MSec;
			double Ch1, Ch2, Ch3, Ch4;

			i = 0;

			do
			{
				if(( RecordingMem[i] && 128 ) == 0 )
				{
								Year = CIISBcdToInt( RecordingMem[i+1] );
								Month = CIISBcdToInt( RecordingMem[i+2] );
								Day = CIISBcdToInt( RecordingMem[i+3] );
								Hour = CIISBcdToInt( RecordingMem[i+4] );
								Minute = CIISBcdToInt( RecordingMem[i+5] );
								Sec = CIISBcdToInt( RecordingMem[i+6] );
								Skip = RecordingMem[i+7];

								LTimeStamp->Caption =   IntToStr( Year ) + '-'
																			+ IntToStr( Month ) + '-'
																			+ IntToStr( Day ) + ' '
																			+ IntToStr( Hour ) + ':'
																			+ IntToStr( Minute )+ ':'
																			+ IntToStr( Sec );


					switch( Skip )
					{
					case 0:
						i += 32;
						break;

					default :
						i += 14;
						break;
					}





					if( ChBStoreData->Checked )
					{
						 //
					}
					else
					{
						 Debug( "Time Stamp: " + LTimeStamp->Caption );
						 Debug( "Samples: " + IntToStr( Skip ));
					}


				}
				else
				{

				if(( RecordingMem[i] & 127 ) == PSIndex )  // FixVolt
				{
					Ch1 =  PS_Ch1BitVal * (unsigned short)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
					Ch2 =  PS_Ch2BitVal * (unsigned short)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
					Ch3 =  PS_Ch1BitVal * (unsigned short)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
					Ch4 =  PS_Ch1BitVal * (unsigned short)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

					if( ChBStoreData->Checked )
					{
						 //
					}
					else
					{
						Debug( "Index: " + IntToStr( RecordingMem[i] & 127 ) +
									 " Ch1 = " + FloatToStrF( Ch1, ffFixed, 4, 2  ) +
									 " Ch2 = " + FloatToStrF( Ch2, ffFixed, 4, 2  ) +
									 " Ch3 = " + FloatToStrF( Ch3, ffFixed, 4, 2  ) +
									 " Ch4 = " + FloatToStrF( Ch4, ffFixed, 4, 2  ));
					}

				}
				else if (( RecordingMem[i] & 127 ) == P4Index ) // P4
				{
					Ch1 =  P4_Ch1BitVal * (Smallint)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
					Ch2 =  P4_Ch2BitVal * (Smallint)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
					Ch3 =  P4_Ch3BitVal * (Smallint)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
					Ch4 =  P4_Ch4BitVal * (Smallint)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

					if( ChBStoreData->Checked )
					{
             //
					}
					else
					{
						Debug( "Index: " + IntToStr( RecordingMem[i] & 127 ) +
									 " Ch1 = " + FloatToStrF( Ch1, ffFixed, 4, 2  ) +
									 " Ch2 = " + FloatToStrF( Ch2, ffFixed, 4, 2  ) +
									 " Ch3 = " + FloatToStrF( Ch3, ffFixed, 4, 2  ) +
									 " Ch4 = " + FloatToStrF( Ch4, ffFixed, 4, 2  ));
					}

				}
				else
				{
					Debug( "Fel index:" + IntToStr( RecordingMem[i] ) + " i= " + IntToStr(i));
        }

					switch( Skip )
					{
					case 1:
						i += 18;
						break;

					default :
						i += 9;
						break;
					}
				}
			} while( i < TotLenght );
		}


 }

 else if ( BusIntMsg[0] == 35 )
 {
	 LBIChMode->Caption = IntToStr( BusIntMsg[2] );
 }
 else if ( BusIntMsg[0] == 36 )
 {
	 int Snr;

	 Snr = BusIntMsg[2] + BusIntMsg[3]*256 + BusIntMsg[4]*65536;

	 switch( BusIntMsg[5] )
	 {
	 case 31:
	 case 32:

		PSIndex = RequestIndex;

		LFixVoltSnr->Caption = IntToStr( Snr );
		if( BusIntMsg[6] == 1 ) ShPowerIn->Brush->Color = clLime;
		else ShPowerIn->Brush->Color = clBtnFace;

		if( BusIntMsg[7] == 1 ) ShPowerOn->Brush->Color = clLime;
		else ShPowerOn->Brush->Color = clBtnFace;

		if( BusIntMsg[8] == 1 ) ShPowerOk->Brush->Color = clLime;
		else ShPowerOk->Brush->Color = clBtnFace;

		PS_Ch1BitVal = BusIntMsg[11] * 0.2 / 32767;
		PS_Ch2BitVal = BusIntMsg[12] * 0.2 / 32767;
		PS_Ch3BitVal = BusIntMsg[13] * 0.2 / 32767;
		PS_Ch4BitVal = BusIntMsg[14] * 0.2 / 32767;

		LVout->Caption = FloatToStrF( PS_Ch1BitVal * (unsigned short)( BusIntMsg[19] + BusIntMsg[20]*256 ), ffFixed, 2, 2 );
		LIOut->Caption = FloatToStrF( PS_Ch2BitVal * (unsigned short)( BusIntMsg[21] + BusIntMsg[22]*256 ), ffFixed, 2, 2 );
		LVIn->Caption = FloatToStrF( PS_Ch3BitVal * (unsigned short)( BusIntMsg[23] + BusIntMsg[24]*256 ), ffFixed, 2, 2 );
		LVTerm->Caption = FloatToStrF( PS_Ch4BitVal * (unsigned short)( BusIntMsg[25] + BusIntMsg[26]*256 ), ffFixed, 2, 2 );
		break;

	 case 24:

	  P4Index = RequestIndex;

		LP4Snr->Caption = Snr;

		P4_Ch1BitVal = BusIntMsg[11] * 0.2 / 32767 * 1000;
		P4_Ch2BitVal = BusIntMsg[12] * 0.2 / 32767 * 1000;
		P4_Ch3BitVal = BusIntMsg[13] * 0.2 / 32767 * 1000;
		P4_Ch4BitVal = BusIntMsg[14] * 0.2 / 32767 * 1000;

		LV1->Caption = FloatToStrF( P4_Ch1BitVal * (Smallint)( BusIntMsg[19] + BusIntMsg[20]*256 ), ffFixed, 4, 2 );
		LV2->Caption = FloatToStrF( P4_Ch2BitVal * (Smallint)( BusIntMsg[21] + BusIntMsg[22]*256 ), ffFixed, 4, 2 );
		LV3->Caption = FloatToStrF( P4_Ch3BitVal * (Smallint)( BusIntMsg[23] + BusIntMsg[24]*256 ), ffFixed, 4, 2 );
		LV4->Caption = FloatToStrF( P4_Ch4BitVal * (Smallint)( BusIntMsg[25] + BusIntMsg[26]*256 ), ffFixed, 4, 2 );
		break;
	 }
	 ;
 }
 else if ( BusIntMsg[0] == 37 )
 {
	 LNodeCount->Caption = IntToStr( BusIntMsg[2] );
 }
 else if ( BusIntMsg[0] == 38 )
 {
	 LRecStatus->Caption = IntToStr( BusIntMsg[2] );
 }


}





//
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BGetTimeClick(TObject *Sender)
{
	LuCtrlTime->Caption = "??-??-?? ??:??";

  BICh->RequestBITime();

}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BNowClick(TObject *Sender)
{
	DTPSetuCtrlDate->DateTime = Now();
	DTPSetuCtrlTime->DateTime = DTPSetuCtrlDate->DateTime;
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BStdModeClick(TObject *Sender)
{
  BICh->SetBIMode( 0 );
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BuCtrlModeClick(TObject *Sender)
{
  BICh->SetBIMode( 1 );
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BSetTimeClick(TObject *Sender)
{

	Word Year, Month, Day, Hour, Minute, Sec, MSec;

	DecodeDate(DTPSetuCtrlDate->DateTime, Year, Month, Day);
	DecodeTime(DTPSetuCtrlTime->Time, Hour, Minute, Sec, MSec);

	Year = Year - 2000;

	BICh->SetBITime( CIISIntToBcd(Year) , CIISIntToBcd(Month), CIISIntToBcd(Day), CIISIntToBcd(Hour), CIISIntToBcd(Minute), CIISIntToBcd(Sec));

}

Word __fastcall TFrmZoneSetup::CIISIntToBcd( Word ival )
{

	return ((ival / 10) << 4) | (ival % 10);

}

Word __fastcall TFrmZoneSetup::CIISBcdToInt( Word ival )
{
	return ((ival >> 4) * 10) + (ival % 16);
}

//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BReadFixVoltClick(TObject *Sender)
{
	EduCtrlVOut->Text = '?';
	EduCtrlIOut->Text = '?';
	RGuCtrlPMode->ItemIndex = -1;
	RGuCtrlPOut->ItemIndex = -1;
	EduCtrlInterval->Text = '?';

	BICh->RequestPowerSetting();
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BSetFixVoltClick(TObject *Sender)
{
	BICh->SetPower( StrToInt( EduCtrlVOut->Text ),
									StrToInt( EduCtrlIOut->Text ),
									RGuCtrlPMode->ItemIndex,
									RGuCtrlPOut->ItemIndex,
									StrToInt( EduCtrlInterval->Text )
								);
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BStartRecClick(TObject *Sender)
{
	BICh->StartRecording();
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BStopRecClick(TObject *Sender)
{
	BICh->StopRecording();
}
//---------------------------------------------------------------------------
void __fastcall TFrmZoneSetup::BGetNodeCountClick(TObject *Sender)
{
	LNodeCount->Caption = "???";
	BICh->RequestNodeCount();
}
//---------------------------------------------------------------------------

void __fastcall TFrmZoneSetup::BGetNodeInfo1Click(TObject *Sender)
{
	/*
	LFixVoltSnr->Caption = "???";
	LVout->Caption = "???";
	LIOut->Caption = "???";
	LVIn->Caption = "???";
	LVTerm->Caption = "???";
	LV1->Caption = "???";
	LV2->Caption = "???";
	LV3->Caption = "???";
	LV4->Caption = "???";

	ShPowerIn->Brush->Color = clBtnFace;
	ShPowerOn->Brush->Color = clBtnFace;
	ShPowerOk->Brush->Color = clBtnFace;
	*/


	RequestIndex = 0;
	BICh->RequestNodeInfo(0);
}
//---------------------------------------------------------------------------

void __fastcall TFrmZoneSetup::BGetNodeInfo2Click(TObject *Sender)
{
	/*
	LFixVoltSnr->Caption = "???";
	LVout->Caption = "???";
	LIOut->Caption = "???";
	LVIn->Caption = "???";
	LVTerm->Caption = "???";
	LV1->Caption = "???";
	LV2->Caption = "???";
	LV3->Caption = "???";
	LV4->Caption = "???";

	ShPowerIn->Brush->Color = clBtnFace;
	ShPowerOn->Brush->Color = clBtnFace;
	ShPowerOk->Brush->Color = clBtnFace;
	*/

	RequestIndex = 1;
	BICh->RequestNodeInfo(1);
}
//---------------------------------------------------------------------------

void __fastcall TFrmZoneSetup::BGetDataClick(TObject *Sender)
{
	DebWin = new TDebugWin( this, &ChangeDebugState);
  DebWin->Top = Top + Height;
  DebWin->Left = Left;
	DebWin->Show();

	BICh->RequestData();
}

void __fastcall TFrmZoneSetup::Debug(String Message)
{
	if( DebWin != NULL ) DebWin->Log(Message);
}

void __fastcall TFrmZoneSetup::ChangeDebugState(int DState)
{
  if( DState == 0 )
  {
	DebWin = NULL;
  }
}

//---------------------------------------------------------------------------

void __fastcall TFrmZoneSetup::BRecStatusClick(TObject *Sender)
{
	LRecStatus->Caption = "???";

	BICh->RequestRecStatus();
}
//---------------------------------------------------------------------------


