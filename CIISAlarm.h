//---------------------------------------------------------------------------

#ifndef CIISAlarmH
#define CIISAlarmH
//---------------------------------------------------------------------------
#include "CIISObj.h"
#include "CIISNodeIni.h"
#include "CIISCommon.h"

enum CIISAlarmType{ Off = 0,
										ProjectAlarm = 1,
										ControllerAlarm = 2,
										ZoneAlarm = 3 };

class TCIISAlarm : public TCIISObj
{
private:
	//CIISClientInt
	bool __fastcall ThisRec( TCamurPacket *P );
	void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
	void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );


	//CIISBusInt
	void __fastcall AlarmIni( TCIISBusPacket *BPIn );
	void __fastcall SM_AlarmIni();
	void __fastcall AlarmIniCapability( TCIISBusPacket *BPIn );
	void __fastcall AlarmIniVersion( TCIISBusPacket *BPIn );

	int32_t __fastcall GetZoneNo() { return AlarmRec->ZoneNo; }
	int32_t __fastcall GetSerialNo() { return AlarmRec->AlarmSerialNo; }
	int32_t __fastcall GetCanAdr() { return AlarmRec->AlarmCanAdr; }
	int32_t __fastcall GetZoneCanAdr() { return AlarmRec->ZoneCanAdr; }
	int32_t __fastcall GetAlarmStatus() { return AlarmRec->AlarmStatus; }
	CIISNodeCapability __fastcall GetAlarmType() { return (CIISNodeCapability)AlarmRec->AlarmType; }
	void _fastcall SetZoneCanAdr( int32_t ZoneCanAdr );


	bool LogCanDupTag, LogCanMissingTag, MissingTagDetected;
	int32_t DupCount;

	String __fastcall GetAlarmName() { return AlarmRec->AlarmName; }

	void __fastcall SetAlarms();
	bool	CheckAlarm( CIISAlarmType AlarmType);

protected:
	//CIISClientInt
	CIISAlarmRec *AlarmRec;

	void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

	//CIISBusInt
	//TCIISBIChannel *CIISBICh;
	int32_t IniRetrys;
	bool AlarmIniRunning, UpdateLastValueRunning;
	NodeIniState NIState;
	int32_t T, TNextEvent;
	int32_t T2, T2NextEvent;
	bool NodeDetected;
	bool CapabilityRecived;
	bool CalibValuesRecived;
	bool VerRecived;

	int32_t FIniErrorCount;
	int32_t LastStoredTag;

	int32_t OnSlowClockTick;
	int32_t RequestSenseGuardTag, ExpectedSenseGuardTag;
	int32_t SenseGuardDelay;
	bool OutputTmpOff;

	bool AlarmLastValueRequest;
	TDateTime SampleTimeStamp;

	void __fastcall OnSysClockTick( TDateTime TickTime );
	void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );
	void __fastcall ClearLastValues(void);

public:
	__fastcall TCIISAlarm( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISAlarmRec *SetAlarmRec, TObject *SetCIISParent );
	__fastcall ~TCIISAlarm();
	bool __fastcall ResetCIISBus();
	bool __fastcall NodeIniReady();
	void __fastcall SetDefaultValues();
	void __fastcall AlarmIniState();


	void __fastcall CheckSampleRecived( int32_t RequestedTag );
	void __fastcall ResetLastStoredTag() { LastStoredTag = 0; }
	void __fastcall ResetCANAlarm();

__published:

	__property int32_t ZoneNo = { read = GetZoneNo };
	__property int32_t SerialNo = { read = GetSerialNo };
	__property int32_t CanAdr = { read = GetCanAdr };
	__property int32_t ZoneCanAdr = { read = GetZoneCanAdr, write = SetZoneCanAdr };
	__property bool IniRunning = { read = AlarmIniRunning, write = AlarmIniRunning };
	__property int32_t AlarmStatus = { read = GetAlarmStatus };
	__property CIISNodeCapability AlarmType = {read = GetAlarmType };
	//	__property TCIISBIChannel* BIChannel = { read = CIISBICh, write = CIISBICh };
	__property int32_t IniErrorCount = { read = FIniErrorCount, write = FIniErrorCount };
	__property String AlarmName = { read = GetAlarmName };
};













#endif
