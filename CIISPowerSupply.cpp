//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H
// 2013-02-25   ClearLastValues
// 2015-02-08   Converted to Xe7, Renamed variable PSMoved
//				because of name-collision with event-code

#pragma hdrstop

#include "CIISPowerSupply.h"
#include "CIISProject.h"
#include "CIISController.h"
#include "math.h"
#include "CIISBusInterface.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

/*
If one BI in system with many BI is removed and system restarted:
Check CIISBICh != NULL in function that can be cald for not connected PS:S
if( PSRec->PSConnected ) or if( CIISBICh != NULL )
*/

__fastcall TCIISPowerSupply::TCIISPowerSupply(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{

  CIISObjType = CIISPowerSupply;

  P_Zone = (TCIISZone*)CIISParent;
  P_Ctrl = (TCIISController*)P_Zone->CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	CIISBICh = NULL;
  PSRec = SetPSRec;

	UpdateLastValueRunning = false;
  NodeDetected = false;

	PSIniRunning = false;
	FIniErrorCount = 0;
	NIState = NI_ZoneAdr;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;

	PSIniStateRunning = false;

  OnSlowClockTick = SenseGuardInterval / SysClock;
	RequestSenseGuardTag = 201;
  SenseGuardDelay = SenseGuardDelayTime;

	ExpectedSenseGuardTag = 211;
	PSLastValueRequest = false;
	PSLastValueExtRequest = false;
	PSLastValueTempRequest = false;

	VRamp = 0;
	RampDelay = 0;
  FIniErrorCount = 0;
  OutputTmpOff = false;
	FIniDecaySample = false;
	FRunDistDecay = false;


  LogCanDupTag = true;
	LogCanMissingTag = true;
	MissingTagDetected = false;
	DupCount = 5;

	FSampReqRunnig = false;
	FTempReqRunning = false;
	FSensGuardReqRunning = false;
	FLastValueSampRunning = false;
	FLastValueTempRunning = false;

  //uCtrl

  FuCtrlZone = false;

  #if DebugCIISStart == 1

  Debug( "PS created: " + IntToStr( PSRec->PSCanAdr ) + "/" + IntToStr( PSRec->PSSerialNo ));

  #endif
}

__fastcall TCIISPowerSupply::~TCIISPowerSupply()
{
  delete PSRec;
}

void __fastcall TCIISPowerSupply::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "PS: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	  {
		WriteRec( P, O );
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase )
	  {
		if( PSRec->ZoneNo == 1 &&  DB->DeletePSRec( PSRec ) )
		{
			DB->ApplyUpdatesPS();
		  P_Prj->Log( ClientCom, CIIEventCode_PSTabChanged, CIIEventLevel_High, "", 0, 0 );
		  P->CommandMode = CIISDeletePS;
		  P->CIISObj = this;
		  O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_DeleteError;
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}

#pragma argsused
void __fastcall TCIISPowerSupply::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "PS: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISPowerSupply::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == PSRec->PSSerialNo;
}

void __fastcall TCIISPowerSupply::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
	if( P->ArgInc( 200 ) )
	{
	  if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
	  else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetLastValFirstToEnd") P->CommandMode = CIISRecAddLastVal;
	  else if( P->GetArg( 200 ) == "GetLastValNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetAlarmFirstToEnd") P->CommandMode = CIISRecAddAlarm;
	  else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
	}
  }

  if( P->CommandMode == CIISRecSearch )
  {
	if( ThisRec( P ) )
	{
	  if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
	  else if( P->GetArg( 200 ) == "GetLastValNextToEnd" ) P->CommandMode = CIISRecAddLastVal;
	  else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" ) P->CommandMode = CIISRecAddAlarm;
	  O->MessageCode = CIISMsg_Ok;
	}
	else O->MessageCode = CIISMsg_RecNotFound;
  }

	else if( P->CommandMode == CIISRecAdd )
	{
	O->MessageData = O->MessageData +
	"1=" + IntToStr( PSRec->PSSerialNo ) + "\r\n" +
	"2=" + IntToStr( PSRec->PSCanAdr ) + "\r\n" +
	"3=" + IntToStr( PSRec->PSStatus ) + "\r\n" +
	"4=" + PSRec->PSName + "\r\n" +
	"5=" + PSRec->PSSerialID + "\r\n" +
	"6=" + IntToStr( PSRec->PSType ) + "\r\n" +
	"7=" + CIISFloatToStr( PSRec->PSLastValueU ) + "\r\n" +
	"8=" + CIISFloatToStr( PSRec->PSLastValueI ) + "\r\n" +
	"9=" + IntToStr( PSRec->PSMode ) + "\r\n" +
	"10=" + CIISFloatToStr( PSRec->PSSetVoltage ) + "\r\n" +
	"11=" + CIISFloatToStr( PSRec->PSSetCurrent ) + "\r\n" +
	"12=" + CIISFloatToStr( PSRec->PSOutGain ) + "\r\n" +
	"13=" + CIISFloatToStr( PSRec->PSOutOffset ) + "\r\n" +
	"14=" + CIISFloatToStr( PSRec->PSLowU ) + "\r\n" +
	"15=" + CIISFloatToStr( PSRec->PSLowI ) + "\r\n" +
	"16=" + CIISFloatToStr( PSRec->PSHighU ) + "\r\n" +
	"17=" + CIISFloatToStr( PSRec->PSHighI ) + "\r\n" +
	"18=" + CIISBoolToStr( PSRec->PSAlarmEnabled ) + "\r\n" +
	"19=" + IntToStr( PSRec->PSAlarmStatusU ) + "\r\n" +
	"20=" + IntToStr( PSRec->PSAlarmStatusI ) + "\r\n" +
	"21=" + CIISBoolToStr( PSRec->PSVOutEnabled ) + "\r\n" +
	"22=" + CIISBoolToStr( PSRec->PSRemote ) + "\r\n" +
	"23=" + IntToStr( PSRec->RequestStatus ) + "\r\n" +
	"24=" + CIISBoolToStr( PSRec->PSConnected ) + "\r\n" +
	"25=" + CIISFloatToStr( PSRec->Ch1Gain ) + "\r\n" +
	"26=" + CIISFloatToStr( PSRec->Ch1Offset ) + "\r\n" +
	"27=" + PSRec->Ch1Unit + "\r\n" +
	"28=" + CIISFloatToStr( PSRec->Ch2Gain ) + "\r\n" +
	"29=" + CIISFloatToStr( PSRec->Ch2Offset ) + "\r\n" +
	"30=" + PSRec->Ch2Unit + "\r\n" +
	"31=" + CIISFloatToStr( PSRec->Ch3Gain ) + "\r\n" +
	"32=" + CIISFloatToStr( PSRec->Ch3Offset ) + "\r\n" +
	"33=" + PSRec->Ch3Unit + "\r\n" +
	"34=" + CIISFloatToStr( PSRec->Ch4Gain ) + "\r\n" +
	"35=" + CIISFloatToStr( PSRec->Ch4Offset ) + "\r\n" +
	"36=" + PSRec->Ch4Unit + "\r\n" +
	"37=" + CIISFloatToStr( PSRec->Ch1BitVal ) + "\r\n" +
	"38=" + CIISFloatToStr( PSRec->Ch2BitVal ) + "\r\n" +
	"39=" + CIISFloatToStr( PSRec->Ch3BitVal ) + "\r\n" +
	"40=" + CIISFloatToStr( PSRec->Ch4BitVal ) + "\r\n" +
	"41=" + CIISFloatToStr( PSRec->PSLastValueCh3 ) + "\r\n" +
	"42=" + CIISFloatToStr( PSRec->PSLastValueCh4 ) + "\r\n" +
	"43=" + IntToStr( PSRec->PSVerMajor ) + "\r\n" +
	"44=" + IntToStr( PSRec->PSVerMinor ) + "\r\n" +
	"45=" + CIISBoolToStr( PSRec->SenseGuardEnabled ) + "\r\n" +
	"46=" + CIISBoolToStr( PSRec->Fallback ) + "\r\n" +
	"47=" + CIISBoolToStr( PSRec->PSTemp ) + "\r\n" +
	"48=" + CIISBoolToStr( PSRec->VerNotSupported ) + "\r\n" +
	"49=" + IntToStr( PSRec->DisabledChannels ) + "\r\n" +
	"50=" + IntToStr( PSRec->ZoneNo ) + "\r\n" +
	"51=" + CIISBoolToStr( PSRec->InvDO1 ) + "\r\n" +
	"52=" + CIISBoolToStr( PSRec->InvDO2 ) + "\r\n" +
	"53=" + CIISBoolToStr( PSRec->CANAlarmEnabled ) + "\r\n" +
	"54=" + IntToStr( PSRec->CANAlarmStatus ) + "\r\n";

	O->MessageCode = CIISMsg_Ok;

	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
	else if( P->CommandMode == CIISRecAddLastVal )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( PSRec->PSSerialNo ) + "\r\n" +
	"7=" + CIISFloatToStr( PSRec->PSLastValueU ) + "\r\n" +
	"8=" + CIISFloatToStr( PSRec->PSLastValueI ) + "\r\n";

	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }

	else if( P->CommandMode == CIISRecAddAlarm )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( PSRec->PSSerialNo ) + "\r\n" +
	"19=" + IntToStr( PSRec->PSAlarmStatusU ) + "\r\n" +
	"20=" + IntToStr( PSRec->PSAlarmStatusI ) + "\r\n";

	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetPSRecCount() ) + "\r\n";;
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISPowerSupply::ClearLastValues(void)
{
    DB->ClearSensorMiscValues(SerialNo);
}

void __fastcall TCIISPowerSupply::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  bool PSMovedToOtherZone = false;
  double CurrentV, CurrentI;

  if( P->ArgInc(200) )
  {
     String Command = P->GetArg(200);
	 if( Command == "Update LastValue" )
	 {
	    UpdateLastValues();
	    O->MessageCode = CIISMsg_Ok;
	 }
    else if (Command == "Clear LastValues")
    {
        ClearLastValues();
        O->MessageCode = CIISMsg_Ok;
    }
  }
  else if( DB->LocatePSRec( PSRec ) )
	{
		CurrentV = PSRec->PSSetVoltage;
		CurrentI = PSRec->PSSetCurrent;

		if( P->ArgInc(2) ) PSRec->PSName = P->GetArg(2);
		if( P->ArgInc(3) ) PSRec->PSSerialID = P->GetArg(3);
		if( P->ArgInc(4) ) PSRec->PSType = StrToInt(P->GetArg(4));
		if( P->ArgInc(5) ) PSRec->PSMode = StrToInt(P->GetArg(5));
		if( P->ArgInc(6) ) PSRec->PSSetVoltage = CIISStrToFloat(P->GetArg(6));
		if( P->ArgInc(7) ) PSRec->PSSetCurrent = CIISStrToFloat(P->GetArg(7));
		if( P->ArgInc(8) ) PSRec->PSOutGain = CIISStrToFloat(P->GetArg(8));
		if( P->ArgInc(9) ) PSRec->PSOutOffset = CIISStrToFloat(P->GetArg(9));
		if( P->ArgInc(10) ) PSRec->PSLowU = CIISStrToFloat(P->GetArg(10));
		if( P->ArgInc(11) ) PSRec->PSLowI = CIISStrToFloat(P->GetArg(11));
		if( P->ArgInc(12) ) PSRec->PSHighU = CIISStrToFloat(P->GetArg(12));
		if( P->ArgInc(13) ) PSRec->PSHighI = CIISStrToFloat(P->GetArg(13));
		if( P->ArgInc(14) )
		{
			if( P->GetArg(14) == "True" )
			{
				PSRec->PSAlarmEnabled = true;
				P_Prj->Log( ClientCom, CIIEventCode_AlarmEnable, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
			else
			{
				PSRec->PSAlarmEnabled = false;
				//PSRec->PSAlarmStatusU = 0;
				//PSRec->PSAlarmStatusI = 0;
				P_Prj->Log( ClientCom, CIIEventCode_AlarmDisable, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
		}
		if( P->ArgInc(15) )
		{
			RampDelay = max( PSRec->PSSetVoltage - CurrentV + 1, ( PSRec->PSSetCurrent - CurrentI ) * 2 + 1 );
			if( RampDelay < 0 ) RampDelay = 0;

			if( P->GetArg(15) == "True" )
			{
				PSRec->PSVOutEnabled = true;
				P_Prj->Log( ClientCom, CIIEventCode_PSVOutEnable, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
			else
			{
				PSRec->PSVOutEnabled = false;
				P_Prj->Log( ClientCom, CIIEventCode_PSVOutDisable, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
		}
		if( P->ArgInc(16) )
		{
			if( P->GetArg(16) == "True" )
			{
				PSRec->PSRemote = true;
				P_Prj->Log( ClientCom, CIIEventCode_PSRemote, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
			else
			{
				PSRec->PSRemote = false;
				P_Prj->Log( ClientCom, CIIEventCode_PSLocal, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			}
		}
		if( P->ArgInc(17) ) PSRec->Ch1Gain = CIISStrToFloat(P->GetArg(17));
		if( P->ArgInc(18) ) PSRec->Ch1Offset = CIISStrToFloat(P->GetArg(18));
		if( P->ArgInc(19) ) PSRec->Ch1Unit = P->GetArg(19);
		if( P->ArgInc(20) ) PSRec->Ch2Gain = CIISStrToFloat(P->GetArg(20));
		if( P->ArgInc(21) ) PSRec->Ch2Offset = CIISStrToFloat(P->GetArg(21));
		if( P->ArgInc(22) ) PSRec->Ch2Unit = P->GetArg(22);
		if( P->ArgInc(23) ) PSRec->Ch3Gain = CIISStrToFloat(P->GetArg(23));
		if( P->ArgInc(24) ) PSRec->Ch3Offset = CIISStrToFloat(P->GetArg(24));
		if( P->ArgInc(25) ) PSRec->Ch3Unit = P->GetArg(25);
		if( P->ArgInc(26) ) PSRec->Ch4Gain = CIISStrToFloat(P->GetArg(26));
		if( P->ArgInc(27) ) PSRec->Ch4Offset = CIISStrToFloat(P->GetArg(27));
		if( P->ArgInc(28) ) PSRec->Ch4Unit = P->GetArg(28);
		if( P->ArgInc(29) ) PSRec->Ch1BitVal = CIISStrToFloat(P->GetArg(29));
		if( P->ArgInc(30) ) PSRec->Ch2BitVal = CIISStrToFloat(P->GetArg(30));
		if( P->ArgInc(31) ) PSRec->Ch3BitVal = CIISStrToFloat(P->GetArg(31));
		if( P->ArgInc(32) ) PSRec->Ch4BitVal = CIISStrToFloat(P->GetArg(32));
		if( P->ArgInc(33) ) PSRec->SenseGuardEnabled = CIISStrToBool( P->GetArg(33));
		if( P->ArgInc(34) ) PSRec->PSStatus = StrToInt(P->GetArg(34));
		if( P->ArgInc(35) ) PSRec->Fallback = CIISStrToBool( P->GetArg(35));
		if( P->ArgInc(36) ) PSRec->PSTemp = CIISStrToBool( P->GetArg(36));
		if( P->ArgInc(37) ) PSRec->DisabledChannels = StrToInt(P->GetArg(37));
		if( P->ArgInc(38) ) PSRec->InvDO1 = CIISStrToBool( P->GetArg(38));
		if( P->ArgInc(39) ) PSRec->InvDO2 = CIISStrToBool( P->GetArg(39));
		if( P->ArgInc(40) ) PSRec->CANAlarmEnabled = CIISStrToBool( P->GetArg(40));
		if( P->ArgInc(41) ) PSRec->CANAlarmStatus = StrToInt(P->GetArg(41));
		if( P->ArgInc(50) )
		{
			if( PSRec->ZoneNo != P->GetArg(50) )
			{
				PSMovedToOtherZone = true;
				PSRec->ZoneNo = StrToInt(P->GetArg(50));
			}
		}

		if( CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

		if( DB->SetPSRec( PSRec ) )
		{
			DB->ApplyUpdatesPS();
			P_Prj->Log( ClientCom, CIIEventCode_PSTabChanged, CIIEventLevel_High, "", 0, 0 );
			//if( CheckAlarm() ) P_Prj->AlarmStatusChanged = true;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;
	}
  else O->MessageCode = CIISMsg_RecNotFound;

  if( PSMovedToOtherZone )
	{
		//NetInt->MovePS( PSRec->PSSerialNo );
		P_Prj->Log( ClientCom, CIIEventCode_PSMoved, CIIEventLevel_High, PSRec->PSSerialNo, 0, 0 );
		P->CommandMode = CIISPSMoved;
		P->CIISObj = this;
	}
  else P->CommandMode = CIISCmdReady;

  if( P->ArgInc(5) )
  {
		// if( DB->LocatePSRec( PSRec ) ) CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );

		if( FuCtrlZone )
		{
			uCtrlIniState();
		}
		else if(( CIISBICh != NULL ) && ( PSRec->VerNotSupported == false ))
		{
			if( DB->LocatePSRec( PSRec ) ) CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
		}
	}

	if(( P->ArgInc(35)) && ( CIISBICh != NULL ) && ( PSRec->VerNotSupported == false ))
	{
		if( DB->LocatePSRec( PSRec ) ) CIISBICh->SetPSFallback( PSRec->PSCanAdr, PSRec->Fallback );
	}

	if( P->ArgInc(6) || P->ArgInc(7) || P->ArgInc(8) || P->ArgInc(9) ||
		P->ArgInc(15) || P->ArgInc(16) || P->ArgInc(38) || P->ArgInc(39) )
	{
		if( DB->LocatePSRec( PSRec ) ) this->PSIniState();
  }

}

void _fastcall TCIISPowerSupply::SetZoneCanAdr( int32_t ZoneCanAdr )
{
  PSRec->ZoneCanAdr = ZoneCanAdr;
  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); // DB->ApplyUpdatesPS();
  }
  if( CIISBICh != NULL ) CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
}

	//CIISBusInt

bool __fastcall TCIISPowerSupply::ResetCIISBus()
{
	CIISBICh = NULL;
  PSRec->PSCanAdr = 0;
  PSRec->PSStatus = 1;
  PSRec->PSConnected = false;
	NodeDetected = false;
	PSIniRunning = false;
	PSIniStateRunning = false;

	FIniDecaySample = false;
	FRunDistDecay = false;
	SenseGuardDelay = SenseGuardDelayTime;

	PSLastValueRequest = false;
	PSLastValueExtRequest = false;
	PSLastValueTempRequest = false;

	FSampReqRunnig = false;
	FTempReqRunning = false;
	FSensGuardReqRunning = false;
	FLastValueSampRunning = false;
	FLastValueTempRunning = false;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }
  return true;
}

bool __fastcall TCIISPowerSupply::NodeIniReady()
{
  return !PSIniRunning;
}

#pragma argsused
void __fastcall TCIISPowerSupply::OnSysClockTick( TDateTime TickTime )
{
  if( PSIniRunning ) SM_PSIni();
	else
	{
		if( PSIniStateRunning ) SM_PSIniState();
		if( UpdateLastValueRunning ) SM_UpdateLastValue();
	}

	OnSlowClockTick--;
  if( OnSlowClockTick <= 0 )
	{
		OnSlowClockTick = SenseGuardInterval / SysClock;

		if( ( CIISBICh != NULL ) && PSRec->SenseGuardEnabled && PSRec->PSVOutEnabled )
		{
			RequestSenseGuardTag++;
			if( RequestSenseGuardTag > 210 ) RequestSenseGuardTag = 201;
			ExpectedSenseGuardTag = RequestSenseGuardTag;
			FSensGuardReqRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, RequestSenseGuardTag );
		}
	}
}

void __fastcall TCIISPowerSupply::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
	if( ( BPIn->MessageCommand == MSG_TX_IDENTIFY ) && ( BPIn->SerNo == PSRec->PSSerialNo )) PSIni( BPIn );
	else if(( CIISBICh == BPIn->GetBIChannel() ) && ( BPIn->CANAdr == PSRec->PSCanAdr ) )
	{
		if( PSRec->VerNotSupported )
		{
			switch (BPIn->MessageCommand)
			{
			case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )
				break;

			case MSG_TX_SAMPLEEXTCH: // Reqest extended Sample ( Monitor, LastValue or Decay )
				break;

			case MSG_TX_PSTEMP:
				break;

			case MSG_TX_CAPABILITY:
				PSIniCapability( BPIn );
				break;

			case MSG_TX_RANGE:
				break;

			case MSG_TX_VERSION:
				PSIniVersion( BPIn );
				break;

			default:
				break;
			}
		}
		else
		{
			switch (BPIn->MessageCommand)
			{
			case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )
				if( FIniDecaySample )
				{
					if( FRunDistDecay )
					{
						if( BPIn->Tag == 0 ) IniDecayTime = BPIn->SampleTimeStamp;
						else if( BPIn->Tag == 1 ) IniDecayTime += OneSecond + P_Ctrl->DecayDelay * One_mS;
						else IniDecayTime += P_Ctrl->DecaySampInterval2 * OneSecond;
					}

					PSSample( BPIn );
				}
				else if( BPIn->Tag == 0 )
				{
					if( PSLastValueRequest ) // LastValue request on PS level
					{
						BPIn->SampleTimeStamp = SampleTimeStamp;
						PSLastValueRequest = false;
					}
					PSSample( BPIn ); // LastValue
				}
				else if( BPIn->Tag > 200 ) PSSample( BPIn ); // SenseGuard
				else if(( BPIn->Tag < 201 ) && ( BPIn->Tag == LastStoredTag ))
				{
					if( LogCanDupTag ) P_Prj->Log(CANCom, CIIEventCode_CANDupTag, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, BPIn->Tag);
					LogCanDupTag = false;
					DupCount = 5;
				}
				else  // Monitor, Decay ....
				{
					LastStoredTag = BPIn->Tag;
					if( !LogCanDupTag )
					{
						DupCount--;
						if( DupCount == 0 ) LogCanDupTag = true;
					}
					PSSample( BPIn );
				}
				break;

			case MSG_TX_SAMPLEEXTCH: // Reqest extended Sample ( Monitor, LastValue or Decay )
				if( PSLastValueExtRequest )  // LastValue request on PS level
				{
					BPIn->SampleTimeStamp = SampleTimeStamp;
					PSLastValueExtRequest = false;
				}
				PSSampleExt( BPIn );
				break;

			case MSG_TX_PSTEMP:
				if( PSLastValueTempRequest )  // LastValue request on PS level
				{
					BPIn->RequestedTag = 0;
					BPIn->SampleTimeStamp = SampleTimeStamp;
					PSLastValueTempRequest = false;
				}
				PSSampleTemp( BPIn );
				break;

			case MSG_TX_CAPABILITY:
				PSIniCapability( BPIn );
				break;

			case MSG_TX_RANGE:
				PSIniCalibValues( BPIn );
				break;

			case MSG_TX_VERSION:
				PSIniVersion( BPIn );
				break;

			default:
				break;
			}
		}
	}
}

#pragma argsused
void __fastcall TCIISPowerSupply::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISPowerSupply::PSIni( TCIISBusPacket *BPIn )
{
	NIState = NI_Accept;
	IniRetrys = NoOffIniRetrys;
	FIniErrorCount = 0;
	T = 0;
	TNextEvent = 0;
  PSIniRunning = true;

  CIISBICh = (TCIISBIChannel*) BPIn->GetBIChannel();

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  SM_PSIni();
}

void __fastcall TCIISPowerSupply::SM_PSIni()
{

}

void __fastcall TCIISPowerSupply::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
  PSRec->PSStatus = 0;
	BPIn->CIISObj = this;
  BPIn->MsgMode = CIISBus_UpdateNode;
  BPIn->ParseMode = CIISParseFinalize;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply::PSIniVersion( TCIISBusPacket *BPIn )
{
	if( VerRecived )
	{
		P_Prj->Log(CANCom, CIIEventCode_DupSNR, CIIEventLevel_High, "BI:" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( PSRec->PSCanAdr ) + "/" + IntToStr( PSRec->PSSerialNo ), PSRec->PSSerialNo, BPIn->Tag);
	}

  VerRecived = true;
  PSRec->PSVerMinor = BPIn->Byte4;
  PSRec->PSVerMajor = BPIn->Byte3;
  PSRec->PSStatus = 0;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  #if DebugCIISBusInt == 1
  Debug( "PSVersion recived PS: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( PSRec->PSVerMajor) + "." + IntToStr( PSRec->PSVerMinor));
  #endif
}

#pragma argsused
void __fastcall TCIISPowerSupply::PSIniCalibValues( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISPowerSupply::SetDefaultValues()
{

  PSRec->PSName = IntToStr( PSRec->PSSerialNo );
  PSRec->PSStatus = 0;
  PSRec->RequestStatus = -1;
	PSRec->PSAlarmEnabled = False;
  PSRec->PSAlarmStatusU = 0;
  PSRec->PSAlarmStatusI = 0;
  PSRec->PSLowU = 0;
  PSRec->PSHighU = 0;
  PSRec->PSLowI = 0;
  PSRec->PSHighI = 0;
	PSRec->PSMode = 0;
	PSRec->PSSetVoltage = 0;
  PSRec->PSSetCurrent = 0;
  PSRec->PSOutGain = 1;
  PSRec->PSOutOffset = 0;
  PSRec->PSVOutEnabled = false;
  PSRec->PSRemote = false;
  PSRec->PSConnected = true;
  PSRec->Ch1Gain = 1;
  PSRec->Ch1Offset = 0;
  PSRec->Ch1Unit = "V";
  PSRec->Ch2Gain = 1;
	PSRec->Ch2Offset = 0;
  PSRec->Ch2Unit = "A";
  PSRec->Ch3Gain = 1;
  PSRec->Ch3Offset = 0;
  PSRec->Ch4Gain = 1;
  PSRec->Ch4Offset = 0;
  PSRec->Ch1BitVal = 5.0/32767;
  PSRec->Ch2BitVal = 5.0/32767;
  PSRec->Ch3BitVal = 5.0/32767;
  PSRec->Ch4BitVal = 5.0/32767;
  PSRec->SenseGuardEnabled = false;
	PSRec->Fallback = true;
	PSRec->PSTemp = false;
	PSRec->VerNotSupported = true;
	PSRec->DisabledChannels = 0;
	PSRec->InvDO1 = false;
	PSRec->InvDO2 = false;
	PSRec->CANAlarmEnabled = true;
	PSRec->CANAlarmStatus = 0;

	SetDefaultValuesSubType();

	DB->SetPSRec( PSRec );
}

void __fastcall TCIISPowerSupply::SetDefaultValuesSubType()
{

}

void __fastcall TCIISPowerSupply::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			T2 = 0;
			T2NextEvent = ApplyUpdDelay;
		}
	}
}

void __fastcall TCIISPowerSupply::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		DB->ApplyUpdatesPS();
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();
		UpdateLastValueRunning = false;
		FLastValueSampRunning = false;  // Timeout if no frame was recived prevent hangup in PSIniState
		FLastValueTempRunning = false;
	}
	T2 += SysClock;
}

void __fastcall TCIISPowerSupply::PSIniState()
{
  if( FuCtrlZone )
  {
		uCtrlIniState();
  }
	else if(( CIISBICh != NULL ) && ( PSRec->VerNotSupported == false ))
	{
		IniState = PSStateVOut;
		PSIniStateRunning = true;
		SenseGuardDelay = SenseGuardDelayTime;
		T3 = 0;
		T3NxtEvent = 200;
  }

}

void __fastcall TCIISPowerSupply::SM_PSIniState()
{

}

void __fastcall TCIISPowerSupply::uCtrlIniState()
{
	;
}

void __fastcall TCIISPowerSupply::PSSetOutputOff()
{

}

void __fastcall TCIISPowerSupply::PSSetOutputState()
{

}

void __fastcall TCIISPowerSupply::PSOnAlarm()
{

	this->PSSetOutputOff();

	PSRec->PSVOutEnabled = false;
	if( DB->SetPSRec( PSRec ) )
	{
		DB->ApplyUpdatesPS();
	}
	P_Prj->Log( LevAlarm, CIIEventCode_PSVOutDisable, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, 0 );
}

void __fastcall TCIISPowerSupply::PSSample( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISPowerSupply::PSSampleExt( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISPowerSupply::UpdateLastTempValues()
{

}

void __fastcall TCIISPowerSupply::PSRequestTemp()
{

}

bool __fastcall TCIISPowerSupply::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	return false;
}

void __fastcall TCIISPowerSupply::PSSampleTemp( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

bool __fastcall TCIISPowerSupply::CheckAlarm()
{
  int32_t AlarmStatus1, AlarmStatus2;
  bool StatusChanged;


  AlarmStatus1 = PSRec->PSAlarmStatusU;
  AlarmStatus2 = PSRec->PSAlarmStatusI;
  StatusChanged = false;

	if( PSRec->PSAlarmEnabled == true )
  {

		if( ( PSRec->PSLastValueU < PSRec->PSLowU ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSLowU, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );
		}
		else if( ( PSRec->PSLastValueU > PSRec->PSHighU ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSHighU, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );
		}
		else if( ( PSRec->PSLastValueU > PSRec->PSLowU ) && ( PSRec->PSLastValueU < PSRec->PSHighU ) && ( AlarmStatus1 == 1 ) )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSNormalU, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );
		}

		if( ( PSRec->PSLastValueI < PSRec->PSLowI ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSLowI, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueI );
		}
		else if( ( PSRec->PSLastValueI > PSRec->PSHighI ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSHighI, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueI );
		}
		else if( ( PSRec->PSLastValueI > PSRec->PSLowI ) && ( PSRec->PSLastValueI < PSRec->PSHighI ) && ( AlarmStatus2 == 1 ) )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_PSNormalI, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueI );
		}

	} // AlarmCheck
	else // PSAlarmEnabled == false
  {
		if( AlarmStatus1 != 0 )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus2 != 0 )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
		}
	}

	if( PSRec->CANAlarmEnabled )
	{
		if( MissingTagDetected && ( PSRec->CANAlarmStatus == 0 ))
		{
			PSRec->CANAlarmStatus = 1;
			StatusChanged = true;
		}
	}
	else
	{
		if( PSRec->CANAlarmStatus == 1 )
		{
			PSRec->CANAlarmStatus = 0;
			MissingTagDetected = false;
			StatusChanged = true;
		}
	}


  if( StatusChanged == true )
  {
	PSRec->PSAlarmStatusU = AlarmStatus1;
	PSRec->PSAlarmStatusI = AlarmStatus2;
  }

  #if DebugCIISAlarm == 1
  Debug( "CheckAlarm Sensor StatusChanged = " + BoolToStr( StatusChanged ) );
  #endif

  return StatusChanged;
}

void __fastcall TCIISPowerSupply::ResetCANAlarm()
{
	if( PSRec->CANAlarmStatus == 1 )
	{
		PSRec->CANAlarmStatus = 0;
		MissingTagDetected = false;
		P_Prj->AlarmStatusChanged = true;
	}
}

void __fastcall TCIISPowerSupply::CheckSampleRecived( int32_t RequestedTag )
{
	if( LastStoredTag != RequestedTag )
	{
		MissingTagDetected = true;
		if( LogCanMissingTag )
		{
			P_Prj->Log(CANCom, CIIEventCode_CANMissingTag, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, RequestedTag);
			LogCanMissingTag = false;
		}
		if( CheckAlarm() )
		{
			P_Prj->AlarmStatusChanged = true;
			if( DB->LocatePSRec(PSRec) ) DB->SetPSRec( PSRec );
		}

	}
	else if( !LogCanMissingTag ) //( LastStoredTag == RequestedTag ) &&
	{
		if( CheckAlarm() )
		{
			P_Prj->AlarmStatusChanged = true;
			if( DB->LocatePSRec(PSRec) ) DB->SetPSRec( PSRec );
		}
		LogCanMissingTag = true;
	}
}

#pragma argsused
void __fastcall TCIISPowerSupply::uCtrlNodeInfo( const Byte *BusIntMsg )
{
		PSRec->PSConnected = true;
		PSRec->PSStatus = 0;
}

#pragma argsused
void __fastcall TCIISPowerSupply::uCtrlPowerSettings( const Byte *BusIntMsg )
{
	;
}
#pragma argsused
void __fastcall TCIISPowerSupply::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	;
}

// Camur II Power Interface

__fastcall TCIISPowerSupply_PI::TCIISPowerSupply_PI( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
  PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_PI::~TCIISPowerSupply_PI()
{


}

void __fastcall TCIISPowerSupply_PI::SM_PSIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  PSRec->PSCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
		TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

	  case NI_ZoneAdr :
		CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

	  case NI_ReqVersion :
		CIISBICh->RequestVer( PSRec->PSCanAdr );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForVer :
		if( VerRecived )
		{
			if( PSRec->PSVerMajor > PS_PI_SupportedVer )
			{
				PSRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				PSRec->VerNotSupported = false;

				if( PSRec->PSVerMajor < 3 ) NIState = NI_ReqCalibValues;
				else if( PSRec->PSVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
				else NIState = NI_SetCamurIIMode;

				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		// For version < 3

	  case NI_ReqCalibValues :
		CIISBICh->RequestIShunt( PSRec->PSCanAdr );
		CalibValuesRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

	  case NI_WaitForCalibValues :
		if( CalibValuesRecived )
		{
		  NIState = NI_Ready;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		//For version >= 3

		case NI_SetCamurIIMode:
			CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIRecPar;
			break;

		case NI_ClrCIIIRecPar:
			CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIAlarm;
			break;

		case NI_ClrCIIIAlarm:
			CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SetInvertOutputOn;
			break;

		case NI_SetInvertOutputOn:
			CIISBICh->InvertOutputOn( PSRec->PSCanAdr, false );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SetFallback;
			break;

		case NI_SetFallback:
			CIISBICh->SetPSFallback( PSRec->PSCanAdr, false);
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqCalibValues_Ver3;
			break;

	  case NI_ReqCalibValues_Ver3 :
		BitValueRequested = 0;
		CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		BitValueRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCh1BitValue;
		IniRetrys = NoOffIniRetrys;
		break;

	  case NI_WaitForCh1BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 1;
		  CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		  BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh2BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh2BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 2;
		  CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		  BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh3BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh3BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 3;
		  CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		  BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh4BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh4BitValue:
		if( BitValueRecived )
		{
		  NIState = NI_Ready;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;


	  case NI_Ready :
		PSIniRunning = false;
		PSRec->PSConnected = true;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
		break;

	  case NI_TimeOut :
		PSIniRunning = false;
		PSRec->PSConnected = false;
		if( DB->LocatePSRec( PSRec ) )
		{
			DB->SetPSRec( PSRec );
		}
		break;

	  default:
		 break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISPowerSupply_PI::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS_PI: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_PI::PSIniCalibValues( TCIISBusPacket *BPIn )
{

	if( PSRec->PSVerMajor < 3 )
	{
		CalibValuesRecived = true;

		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 65535;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535;
		PSRec->Ch3BitVal = BPIn->Byte5 * 0.2 / 65535;
		PSRec->Ch4BitVal = BPIn->Byte6 * 0.2 / 65535;
	}
	else if( PSRec->PSVerMajor < 5 )
	{
		switch (BitValueRequested)
		{
			case 0: PSRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767; break;
			case 1: PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767; break;
			case 2: PSRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767; break;
			case 3: PSRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767; break;
		}

		BitValueRecived = true;
	}
	else
	{
		switch (BitValueRequested)
		{
			case 0: PSRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 1: PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 2: PSRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 3: PSRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		}

		BitValueRecived = true;
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
	}

  #if DebugCIISBusInt == 1
  Debug( "PSCalib recived PS_PI: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISPowerSupply_PI::SetDefaultValuesSubType()
{
	PSRec->PSMode = 1;
	PSRec->Ch3Unit = "?";
	PSRec->Ch4Unit = "?";
}

void __fastcall TCIISPowerSupply_PI::SM_PSIniState()
{
	if( T3 >= T3NxtEvent)
	{
		switch (IniState)
		{
			case PSStateVOut:
			if( PSRec->PSVerMajor < 3 )
			{
				if( PSRec->PSMode == 1 ) CIISBICh->SetAOut( PSRec->PSCanAdr, ( PSRec->PSSetVoltage * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000 );
				else if( PSRec->PSMode == 2 ) CIISBICh->SetAOut( PSRec->PSCanAdr, ( PSRec->PSSetCurrent * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000 );
				else CIISBICh->SetAOut( PSRec->PSCanAdr, 0 );
				IniState = PSStateOutputOn;
			}
			else if( PSRec->PSVerMajor < 5 )
			{
				if( PSRec->PSMode == 1 ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, ( PSRec->PSSetVoltage * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000, 10000 );
				else if( PSRec->PSMode == 2 ) CIISBICh->SetPSIOut( PSRec->PSCanAdr, ( PSRec->PSSetCurrent * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000, 20000 );
				else CIISBICh->SetPSVOut( PSRec->PSCanAdr, 0, 10000 );
				IniState = PSStateOutputOn;
			}
			else  // Ver Start from zero, first power on then I/U out  for ver 5 PI
			{
				CIISBICh->SetPSVOut( PSRec->PSCanAdr, 0 , 10000 );
				IniState = PSStateIOut;
			}
			T3 = 0;
			break;

		case PSStateIOut:
			CIISBICh->SetPI_IOut( PSRec->PSCanAdr, 0 , 20000 );
			IniState = PSStateSetInvertOutputOn;
			break;

		case PSStateSetInvertOutputOn:
			CIISBICh->InvertOutputOn( PSRec->PSCanAdr, PSRec->InvDO1 );
			IniState = PSStateOutputOn;
			T3 = 0;
			break;

		case PSStateOutputOn:
			if( PSRec->PSVerMajor < 5 )
			{
				CIISBICh->SetDigOut( PSRec->PSCanAdr, 0, PSRec->PSVOutEnabled ^ PSRec->InvDO1 );
				OutputTmpOff = false;
				IniState = PSStateRemote;
			}
			else
			{
				CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
				OutputTmpOff = false;
				IniState = PSStateRemote;
			}
			T3 = 0;
			break;

		case PSStateRemote :
			CIISBICh->SetDigOut( PSRec->PSCanAdr, 1, PSRec->PSRemote ^ PSRec->InvDO2 );
			if( PSRec->PSVerMajor < 5 ) IniState = PSStateReady;
			else IniState = PSStateRampUpVout;
			T3 = 0;
			break;

		case PSStateRampUpVout:   // Ver 5 PI ramp after Power On
			if( PSRec->PSMode == 1 ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, ( PSRec->PSSetVoltage * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000, 10000 );
			else if( PSRec->PSMode == 2 ) CIISBICh->SetPI_IOut( PSRec->PSCanAdr, ( PSRec->PSSetCurrent * PSRec->PSOutGain + PSRec->PSOutOffset ) * 1000, 20000 );
			else CIISBICh->SetPSVOut( PSRec->PSCanAdr, 0, 10000 );
			IniState = PSStateReady;
			T3 = 0;
			break;


		case PSStateReady :
			PSIniStateRunning = false;
			break;
		}
	}
	T3 += SysClock;
}

void __fastcall TCIISPowerSupply_PI::PSSetOutputOff()
{
	if( PSRec->PSVerMajor < 5 )
	{
		if( CIISBICh != NULL ) CIISBICh->SetDigOut( PSRec->PSCanAdr, 0, false ^ PSRec->InvDO1 );
	}
	else
	{
		if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
	}
	OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_PI::PSSetOutputState()
{
	if( PSRec->PSVerMajor < 5 )
	{
		if( CIISBICh != NULL ) CIISBICh->SetDigOut( PSRec->PSCanAdr, 0, PSRec->PSVOutEnabled ^ PSRec->InvDO1 );
	}
	else
	{
		if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	}
	OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_PI::PSSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

	if( PSRec->PSVerMajor < 3 )
	{
		UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;

		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}
	else if( PSRec->PSVerMajor < 5 )
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal2 = ADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}
	else
	{

		UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		if( UADVal1 == 65534 )  // PSU underflow;  Ver
		{
			AnVal1 = FixVoltUnderflow;
			AnVal1_Scl = FixVoltUnderflow;
		}
		else if( UADVal1 == 65535 ) // PSU overflow;
		{
			AnVal1 = FixVoltOverflow;
			AnVal1_Scl = FixVoltOverflow;
		}
		else
		{
			AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
			AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
		}

		 UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		if( UADVal2 == 65534 )  // PSI underflow;
		{
			AnVal2 = FixVoltUnderflow;
			AnVal2_Scl = FixVoltUnderflow;
		}
		else if( UADVal2 == 65535 ) // PSI overflow;
		{
			AnVal2 = FixVoltOverflow;
			AnVal2_Scl = FixVoltOverflow;
		}
		else
		{
			AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
			AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
		}
	}




	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }


  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
	}
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	if( PSRec->PSVerMajor < 3 )
	{
		Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
	}
	else
	{
		Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	}
  #endif

}

void __fastcall TCIISPowerSupply_PI::PSSampleExt( TCIISBusPacket *BPIn )
{

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		return;
	}


  TDateTime DTS;
  DTS = Now();

	if( PSRec->PSVerMajor < 3 )
	{
		UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;

		AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
		AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
	}
	else if( PSRec->PSVerMajor < 5 )
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
		AnVal2 = ADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
		AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
	}
	else
	{
		UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
		{
			AnVal1 = FixVoltUnderflow;
			AnVal1_Scl = FixVoltUnderflow;
		}
		else if( UADVal1 == 65535 ) // PSU overflow;
		{
			AnVal1 = FixVoltOverflow;
			AnVal1_Scl = FixVoltOverflow;
		}
		else
		{
			AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
			AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
		}

		UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		if( UADVal2 == 65534 )  // PSI underflow;
		{
			AnVal2 = FixVoltUnderflow;
			AnVal2_Scl = FixVoltUnderflow;
		}
		else if( UADVal2 == 65535 ) // PSI overflow;
		{
			AnVal2 = FixVoltOverflow;
			AnVal2_Scl = FixVoltOverflow;
		}
		else
		{
			AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;
			AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
		}
	}

  PSRec->PSLastValueCh3 = AnVal1_Scl;
  PSRec->PSLastValueCh4 = AnVal2_Scl;

  if( DB->LocatePSRec( PSRec ) )
  {
	DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if( !FIniDecaySample )
	{
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PS3";
		MVR->ValueUnit = PSRec->Ch3Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PS4";
		MVR->ValueUnit =  PSRec->Ch4Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {

	CIISMonitorValueRec *MValRec;
	MValRec = new CIISMonitorValueRec;

	MValRec->DateTimeStamp = DTS;
	MValRec->SampleDateTime = BPIn->SampleTimeStamp;
	MValRec->RecNo = BPIn->RecordingNo;
	MValRec->SensorSerialNo = PSRec->PSSerialNo;
	MValRec->ValueType = "PS3";
	MValRec->ValueUnit = PSRec->Ch3Unit;
	MValRec->RawValue = AnVal1;
	MValRec->Value = AnVal1_Scl;

	DB->AppendMonitorValueRec( MValRec );

	MValRec->ValueType = "PS4";
	MValRec->ValueUnit = PSRec->Ch4Unit;
	MValRec->RawValue = AnVal2;
	MValRec->Value = AnVal2_Scl;

	DB->AppendMonitorValueRec( MValRec );

	delete MValRec;

	P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;


	#if DebugCIISBusInt == 1
	if( PSRec->PSVerMajor < 3 )
	{
		Debug("Incoming SampleExt recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
	}
	else
	{
		Debug("Incoming SampleExt recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	}
	#endif

}

// Camur II FixVolt

__fastcall TCIISPowerSupply_FixVolt::TCIISPowerSupply_FixVolt( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
  PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_FixVolt::~TCIISPowerSupply_FixVolt()
{


}

void __fastcall TCIISPowerSupply_FixVolt::SM_PSIni()
{
	if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				PSRec->PSCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
			TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( PSRec->PSCanAdr  );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( PSRec->PSVerMajor > PS_FV8_SupportedVer )
				{
					PSRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					PSRec->VerNotSupported = false;

					if( PSRec->PSVerMajor < 3 ) NIState = NI_SelMode;
					else if( PSRec->PSVerMajor < 5 )  NIState = NI_SetFallback;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_SetCamurIIMode:
			CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIRecPar;
			break;

			case NI_ClrCIIIRecPar:
			CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIAlarm;
			break;

			case NI_ClrCIIIAlarm:
			CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SetFallback;
			break;

			case NI_SetFallback:
			CIISBICh->SetPSFallback( PSRec->PSCanAdr, PSRec->Fallback);
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelMode;
			break;

			case NI_SelMode:
			CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			if( PSRec->PSVerMajor > 3 ) NIState = NI_ReqCalibValues_Ver4;
			else NIState = NI_ReqCalibValues;
			break;


			case NI_ReqCalibValues_Ver4:
			BitValueRequested = 0;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh1BitValue;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 2;
				CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh3BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh3BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 3;
				CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh4BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh4BitValue:
			if( BitValueRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( PSRec->PSCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			PSIniRunning = false;
			PSRec->PSConnected = true;
			if( DB->LocatePSRec( PSRec ) )
			{
				DB->SetPSRec( PSRec );
			}
			// If the PS is not initiated from Controller:RestartCIIBus (Power recovery on a PS on the external CIIBus) output is initiaded here
			if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
			break;

			case NI_TimeOut :
			PSIniRunning = false;
			PSRec->PSConnected = false;
			if( DB->LocatePSRec( PSRec ) )
			{
				DB->SetPSRec( PSRec );
			}
			break;

			default:
			break;
		}
  }
	T += SysClock;
}

void __fastcall TCIISPowerSupply_FixVolt::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt::PSIniCalibValues( TCIISBusPacket *BPIn )
{
	CalibValuesRecived = true;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 32767;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767;
		PSRec->Ch3BitVal = BPIn->Byte5 * 0.2 / 32767;
		PSRec->Ch4BitVal = BPIn->Byte6 * 0.2 / 32767;
	}
	else if( PSRec->PSVerMajor < 4 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 65535;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535;
		PSRec->Ch3BitVal = BPIn->Byte5 * 0.2 / 65535;
		PSRec->Ch4BitVal = BPIn->Byte6 * 0.2 / 65535;
	}
	else
	{
		switch (BitValueRequested)
		{
			case 0: PSRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 1: PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 2: PSRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 3: PSRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		}
		BitValueRecived = true;
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	#if DebugCIISBusInt == 1
	Debug( "PSCalib recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
	#endif
}

void __fastcall TCIISPowerSupply_FixVolt::SetDefaultValuesSubType()
{
  PSRec->Ch3Unit = "V";
  PSRec->Ch4Unit = "V";
}

void __fastcall TCIISPowerSupply_FixVolt::SM_PSIniState()
{
	if( FSampReqRunnig || FTempReqRunning || FSensGuardReqRunning || FLastValueSampRunning || FLastValueTempRunning ) // FV nodes can only buffer one command. Send kommands after sample req.
	{
  	#if DebugCIISBusInt == 1
		Debug( "Wait for samp seq. SampReq:" + BoolToStr( FSampReqRunnig ) + " TempReq: " + BoolToStr( FTempReqRunning ) +
					 " SensGuard:" + BoolToStr( FSensGuardReqRunning ) + " LastVal: " + BoolToStr( FLastValueSampRunning ) + " LastTemp " + BoolToStr( FLastValueTempRunning ) );
		#endif

	 if(( IniState == PSStateOutputOn ) && ( RampDelay > 0 )) RampDelay--;  // But dec delay for ramp
	 return;
	}

	switch (IniState)
	{
	case PSStateVOut:
		if( PSRec->PSVerMajor < 3 ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
		else CIISBICh->SetPSVOut( PSRec->PSCanAdr, PSRec->PSSetVoltage * 1000, 20000 );
		IniState = PSStateIOut;
		break;

	case PSStateIOut:
		CIISBICh->SetPSIOut( PSRec->PSCanAdr, PSRec->PSSetCurrent * 1000, 10000 );
		IniState = PSStateOutputOn;
		break;

	case PSStateOutputOn:
		if( PSRec->PSVerMajor < 3 )
		{
			CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
			OutputTmpOff = false;
			IniState = PSStateRampUpVout;
		}
		else
		{
			if( RampDelay == 0 ) // FixVolt with internal U/I ramping  dont responde to VOutEnable while stepping up U/I
			{
				CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
				OutputTmpOff = false;
				IniState = PSStateReady;
			}
			else RampDelay--;
		}
		break;

	case PSStateRampUpVout:
		if( PSRec->PSVOutEnabled )
		{
			VRamp += 0.2;
			if( VRamp >= PSRec->PSSetVoltage )
			{
				VRamp = PSRec->PSSetVoltage;
				IniState = PSStateReady;
			}
		}
		else
		{
			VRamp = 0;
			IniState = PSStateReady;
		}
		CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
		break;

	case PSStateReady:
		PSIniStateRunning = false;
		break;

	default:
		break;
	}
}

void __fastcall TCIISPowerSupply_FixVolt::uCtrlIniState()
{
	TCIISZone_uCtrl *ZuCtrl;
	ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

	ZuCtrl->uCtrlSetPower( PSRec->PSSetVoltage * 1000,
												 PSRec->PSSetCurrent * 1000,
												 PSRec->PSMode,
												 PSRec->PSVOutEnabled );

	PSIniStateRunning = false;
}

void __fastcall TCIISPowerSupply_FixVolt::PSSetOutputOff()
{
   VRamp = 0;
   if(PSRec->PSVerMajor <= 1 && PSRec->PSVerMinor <= 2 )
   {
		if( CIISBICh != NULL ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, 0, 20000 );
   }
   if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
   OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_FixVolt::PSSetOutputState()
{
   if(PSRec->PSVerMajor <= 1 && PSRec->PSVerMinor <= 2 )
   {
		if( CIISBICh != NULL ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, PSRec->PSSetVoltage * 1000, 20000 );
   }
   if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
   OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_FixVolt::PSSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	if((( PSRec->PSVerMajor == 1 ) && ( PSRec->PSVerMinor < 3  )))
	{
		if( BPIn->Tag == ExpectedSenseGuardTag )
		{
			FSensGuardReqRunning = false;
			return;
		}
		else if( BPIn->Tag == 0 )
		{
			FLastValueSampRunning = false;
		}
		else
		{
			FSampReqRunnig = false;
		}
  }

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	}

   UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}

	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;


	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
	}



  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
  }
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
  {
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		if( FIniDecaySample && FRunDistDecay ) DValRec->SampleDateTime = IniDecayTime;
		else DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt::PSSampleExt( TCIISBusPacket *BPIn )
{

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		FSensGuardReqRunning = false;
		return;
	}
	else if( BPIn->Tag == 0 )
	{
		FLastValueSampRunning = false;
	}
	else
	{
		FSampReqRunnig = false;
	}

  TDateTime DTS;
  DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	}

	UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
	}

  PSRec->PSLastValueCh3 = AnVal1_Scl;
  PSRec->PSLastValueCh4 = AnVal2_Scl;


	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PS3";
		MVR->ValueUnit = PSRec->Ch3Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PS4";
		MVR->ValueUnit =  PSRec->Ch4Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PS3";
		MValRec->ValueUnit = PSRec->Ch3Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PS4";
		MValRec->ValueUnit = PSRec->Ch4Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt::PSRequestTemp()
{
	FTempReqRunning = true;
	if( CIISBICh != NULL ) CIISBICh->RequestTemp( PSRec->PSCanAdr );
}

void __fastcall TCIISPowerSupply_FixVolt::PSSampleTemp( TCIISBusPacket *BPIn )
{
	if( BPIn->RequestedTag == 0 )
	{
		FLastValueTempRunning = false;
	}
	else
	{
		FTempReqRunning = false;
	}

  if( PSRec->PSTemp )
  {
		TDateTime DTS;
		DTS = Now();

		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;

		if( PSRec->PSVerMajor < 3 )
		{
			if( ADVal1 & 512 ) ADVal1 = - ((( ~ADVal1 ) & 511 ) + 1 );
			AnVal1 = ADVal1;
			AnVal1 = AnVal1 / 4;
		}
	else AnVal1 = ADVal1;


	if( BPIn->RequestedTag == 0 )
	{
	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();
	}
	else if( BPIn->RecType == RT_Monitor ) //Store Monitor Value
	{

	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();

	  CIISMonitorValueRec *MValRec;
	  MValRec = new CIISMonitorValueRec;

	  MValRec->DateTimeStamp = DTS;
	  MValRec->SampleDateTime = BPIn->SampleTimeStamp;
	  MValRec->RecNo = BPIn->RecordingNo;
	  MValRec->SensorSerialNo = PSRec->PSSerialNo;
	  MValRec->ValueType = "T";
	  MValRec->ValueUnit = "C";
	  MValRec->RawValue = AnVal1;
	  MValRec->Value = AnVal1;

	  DB->AppendMonitorValueRec( MValRec );
	  delete MValRec;

	  P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	#if DebugCIISBusInt == 1
	Debug("Incoming temp recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(ADVal1));
	#endif
	 }
}

void __fastcall TCIISPowerSupply_FixVolt::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			PSLastValueExtRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			if( PSRec->PSTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISPowerSupply_FixVolt::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SampleTimeStamp = Now();
		PSLastValueTempRequest = true;
		UpdateLastValueRunning = true;
		FLastValueTempRunning = true;
		CIISBICh->RequestTemp( PSRec->PSCanAdr );

		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISPowerSupply_FixVolt::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
  {
		switch (T2State)
		{
			case 10: // Request temp
			PSLastValueTempRequest = true;
			FLastValueTempRunning = true;
			CIISBICh->RequestTemp( PSRec->PSCanAdr ); // Tag always 0 fore Last value request

			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesPS();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			UpdateLastValueRunning = false;
			FLastValueSampRunning = false; // Timeout if no frame was recived prevent hangup in PSIniState
			FLastValueTempRunning = false;
			break;
		}
  }
  T2 += SysClock;
}

bool __fastcall TCIISPowerSupply_FixVolt::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if( PSRec->PSVerMajor >= 3 )
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni(PSRec->PSCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

void __fastcall TCIISPowerSupply_FixVolt::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;
	TDateTime DTS;

	DTS = Now();

	PSRec->PSConnected = true;
	PSRec->PSStatus = 0;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 32767;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 32767;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 32767;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 65535;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 65535;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 65535;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 65535;
	}

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	PSRec->PSLastValueU = Ch1_Scl;
	PSRec->PSLastValueI = Ch2_Scl;
	PSRec->PSLastValueCh3 = Ch3_Scl;
	PSRec->PSLastValueCh4 = Ch4_Scl;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec );
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
	MVR.SensorSerialNo = PSRec->PSSerialNo;
	MVR.ValueType = "PSU";
	MVR.ValueUnit = PSRec->Ch1Unit;
	MVR.Value = Ch1_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PSI";
	MVR.ValueUnit =  PSRec->Ch2Unit;
	MVR.Value = Ch2_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS3";
	MVR.ValueUnit = PSRec->Ch3Unit;
	MVR.Value = Ch3_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS4";
	MVR.ValueUnit =  PSRec->Ch4Unit;
	MVR.Value = Ch4_Scl;

	DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	if( BusIntMsg[6] == 1 ) P_Prj->uCtrlPowerIn = true;
	else P_Prj->uCtrlPowerIn = false;

	if( BusIntMsg[7] == 1 ) P_Prj->uCtrlPowerOn = true;
	else P_Prj->uCtrlPowerOn = false;

	if( BusIntMsg[8] == 1 ) P_Prj->uCtrlPowerOk = true;
	else P_Prj->uCtrlPowerOk = false;


			/*
			LVout->Caption = FloatToStrF( PS_Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20]*256 ), ffFixed, 2, 2 );
			LIOut->Caption = FloatToStrF( PS_Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22]*256 ), ffFixed, 2, 2 );
			LVIn->Caption = FloatToStrF( PS_Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24]*256 ), ffFixed, 2, 2 );
			LVTerm->Caption = FloatToStrF( PS_Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26]*256 ), ffFixed, 2, 2 );
			*/
}

void __fastcall TCIISPowerSupply_FixVolt::uCtrlPowerSettings( const Byte *BusIntMsg )
{



	PSRec->PSSetVoltage = ((double)( (uint16_t)BusIntMsg[2] + (uint16_t)BusIntMsg[3]*256 )) / 1000;
	PSRec->PSSetCurrent = ((double)( (uint16_t)BusIntMsg[4] + (uint16_t)BusIntMsg[5]*256 )) / 1000;

	PSRec->PSMode = BusIntMsg[6];

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = false;
		break;
	}

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); DB->ApplyUpdatesPS();
	}

	P_Prj->DataChanged();


 /*
	EduCtrlVOut->Text = IntToStr( BusIntMsg[2] + BusIntMsg[3]*256 );
	EduCtrlIOut->Text = IntToStr( BusIntMsg[4] + BusIntMsg[5]*256 );

	switch( BusIntMsg[6] )
	{
	case 0:
		RGuCtrlPMode->ItemIndex = 0;
		break;

	case 1:
		RGuCtrlPMode->ItemIndex = 1;
		break;
	}

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = true;
		break;
	}
  */

}

void __fastcall TCIISPowerSupply_FixVolt::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = PSRec->PSSerialNo;

	MValRec.ValueType = "PSU";
	MValRec.ValueUnit = PSRec->Ch1Unit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PSI";
	MValRec.ValueUnit = PSRec->Ch2Unit;
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS3";
	MValRec.ValueUnit = PSRec->Ch3Unit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS4";
	MValRec.ValueUnit = PSRec->Ch4Unit;
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );




}

// Camur II FixVolt3A

__fastcall TCIISPowerSupply_FixVolt3A::TCIISPowerSupply_FixVolt3A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
  PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_FixVolt3A::~TCIISPowerSupply_FixVolt3A()
{


}

void __fastcall TCIISPowerSupply_FixVolt3A::SM_PSIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  PSRec->PSCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
		TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

	  case NI_ZoneAdr :
		CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion :
		CIISBICh->RequestVer( PSRec->PSCanAdr  );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

	  case NI_WaitForVer :
		if( VerRecived )
		{
			if( PSRec->PSVerMajor > PS_FV3_SupportedVer )
			{
				PSRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				PSRec->VerNotSupported = false;

				if( PSRec->PSVerMajor < 3 ) NIState = NI_SelMode;
				else if( PSRec->PSVerMajor < 5 )  NIState = NI_SetFallback;
				else NIState = NI_SetCamurIIMode;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		case NI_SetCamurIIMode:
		CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIRecPar;
		break;

		case NI_ClrCIIIRecPar:
		CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIAlarm;
		break;

		case NI_ClrCIIIAlarm:
		CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SetFallback;
		break;

		case NI_SetFallback:
		CIISBICh->SetPSFallback( PSRec->PSCanAdr, PSRec->Fallback);
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SelMode;
		break;

	  case NI_SelMode:
		CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		if( PSRec->PSVerMajor > 3 ) NIState = NI_ReqCalibValues_Ver4;
		else NIState = NI_ReqCalibValues;
		break;

		case NI_ReqCalibValues_Ver4:
		BitValueRequested = 0;
		CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		BitValueRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCh1BitValue;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForCh1BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 1;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh2BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh2BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 2;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh3BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh3BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 3;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh4BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh4BitValue:
		if( BitValueRecived )
		{
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

	  case NI_ReqCalibValues :
		CIISBICh->RequestIShunt( PSRec->PSCanAdr );
		CalibValuesRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

	  case NI_WaitForCalibValues :
		if( CalibValuesRecived )
		{
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_Ready :
		PSIniRunning = false;
		PSRec->PSConnected = true;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		// If the PS is not initiated from Controller:RestartCIIBus (Power recovery on a PS on the external CIIBus) output is initiaded here
		if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
		break;

	  case NI_TimeOut :
		PSIniRunning = false;
		PSRec->PSConnected = false;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		break;

	  default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS_FixVolt3A: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSIniCalibValues( TCIISBusPacket *BPIn )
{
  CalibValuesRecived = true;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 32767;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767;
		PSRec->Ch3BitVal = BPIn->Byte5 * 0.2 / 32767;
		PSRec->Ch4BitVal = BPIn->Byte6 * 0.2 / 32767;
	}
	else if( PSRec->PSVerMajor < 4 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 65535;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535;
		PSRec->Ch3BitVal = BPIn->Byte5 * 0.2 / 65535;
		PSRec->Ch4BitVal = BPIn->Byte6 * 0.2 / 65535;
	}
	else
	{
		switch (BitValueRequested)
		{
			case 0: PSRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 1: PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 2: PSRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 65535; break;
			case 3: PSRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		}
		BitValueRecived = true;
	}


  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	#if DebugCIISBusInt == 1
	Debug( "PSCalib recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt3A::SetDefaultValuesSubType()
{
  PSRec->Ch3Unit = "V";
  PSRec->Ch4Unit = "V";
}

void __fastcall TCIISPowerSupply_FixVolt3A::SM_PSIniState()
{
  switch (IniState)
  {
	case PSStateVOut:
	  if( PSRec->PSVerMajor < 3 ) CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
	  else CIISBICh->SetPSVOut( PSRec->PSCanAdr, PSRec->PSSetVoltage * 1000, 20000 );
	  IniState = PSStateIOut;
	  break;

	case PSStateIOut:
		CIISBICh->SetPSIOut( PSRec->PSCanAdr, PSRec->PSSetCurrent * 3000, 10000 );
	  IniState = PSStateOutputOn;
	  break;

	case PSStateOutputOn:
		if( PSRec->PSVerMajor < 3 )
	  {
			CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
			OutputTmpOff = false;
			IniState = PSStateRampUpVout;
	  }
	  else
	  {
			if( RampDelay == 0 ) // FixVolt with internal U/I ramping  dont responde to VOutEnable while stepping up U/I
			{
				CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
				OutputTmpOff = false;
				IniState = PSStateReady;
			}
			else RampDelay--;
	  }
	  break;

	case PSStateRampUpVout:
	  if( PSRec->PSVOutEnabled )
		{
			VRamp += 0.2;
			if( VRamp >= PSRec->PSSetVoltage )
			{
				VRamp = PSRec->PSSetVoltage;
				IniState = PSStateReady;
			}
		}
	  else
	  {
			VRamp = 0;
			IniState = PSStateReady;
	  }
	  CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
	  break;

	case PSStateReady:
	  PSIniStateRunning = false;
	  break;

	default:
		break;
  }
}

void __fastcall TCIISPowerSupply_FixVolt3A::uCtrlIniState()
{
	TCIISZone_uCtrl *ZuCtrl;
	ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

	ZuCtrl->uCtrlSetPower( PSRec->PSSetVoltage * 1000,
												 PSRec->PSSetCurrent * 3000,
												 PSRec->PSMode,
												 PSRec->PSVOutEnabled );

	PSIniStateRunning = false;
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSSetOutputOff()
{
	 VRamp = 0;
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
	 OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSSetOutputState()
{
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	 OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	}

   UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}

	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
	}

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
  }
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
  {
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		//if( FIniDecaySample && PSRec->PSVerMajor >= 3 ) DValRec->SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec->SampleDateTime = IniDecayTime;
		else DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt3A::PSSampleExt( TCIISBusPacket *BPIn )
{
	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		FSensGuardReqRunning = false;
		return;
	}
	else if( BPIn->Tag == 0 )
	{
		FLastValueSampRunning = false;
	}
	else
	{
		FSampReqRunnig = false;
	}

	TDateTime DTS;
	DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	}

	UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
	}

	PSRec->PSLastValueCh3 = AnVal1_Scl;
	PSRec->PSLastValueCh4 = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PS3";
		MVR->ValueUnit = PSRec->Ch3Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PS4";
		MVR->ValueUnit =  PSRec->Ch4Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }



  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{

		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PS3";
		MValRec->ValueUnit = PSRec->Ch3Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PS4";
		MValRec->ValueUnit = PSRec->Ch4Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt3A::PSRequestTemp()
{
	FTempReqRunning = true;
	if( CIISBICh != NULL ) CIISBICh->RequestTemp( PSRec->PSCanAdr );
}

void __fastcall TCIISPowerSupply_FixVolt3A::PSSampleTemp( TCIISBusPacket *BPIn )
{
	if( BPIn->RequestedTag == 0 )
	{
		FLastValueTempRunning = false;
	}
	else
	{
		FTempReqRunning = false;
	}

  if( PSRec->PSTemp )
  {
		TDateTime DTS;
		DTS = Now();

		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;

	if( PSRec->PSVerMajor < 3 )
	{
	  if( ADVal1 & 512 ) ADVal1 = - ((( ~ADVal1 ) & 511 ) + 1 );
	  AnVal1 = ADVal1;
	  AnVal1 = AnVal1 / 4;
	}
	else AnVal1 = ADVal1;

	if( BPIn->RequestedTag == 0 )
	{
	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();
	}
	else if( BPIn->RecType == RT_Monitor ) //Store Monitor Value
	{

	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();

	  CIISMonitorValueRec *MValRec;
	  MValRec = new CIISMonitorValueRec;

	  MValRec->DateTimeStamp = DTS;
	  MValRec->SampleDateTime = BPIn->SampleTimeStamp;
	  MValRec->RecNo = BPIn->RecordingNo;
	  MValRec->SensorSerialNo = PSRec->PSSerialNo;
	  MValRec->ValueType = "T";
	  MValRec->ValueUnit = "C";
	  MValRec->RawValue = AnVal1;
	  MValRec->Value = AnVal1;

	  DB->AppendMonitorValueRec( MValRec );
	  delete MValRec;

	  P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	#if DebugCIISBusInt == 1
	Debug("Incoming temp recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(ADVal1));
	#endif
  }
}

void __fastcall TCIISPowerSupply_FixVolt3A::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			PSLastValueExtRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			if( PSRec->PSTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISPowerSupply_FixVolt3A::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SampleTimeStamp = Now();
		PSLastValueTempRequest = true;
		UpdateLastValueRunning = true;
		FLastValueTempRunning = true;
		CIISBICh->RequestTemp( PSRec->PSCanAdr );
		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISPowerSupply_FixVolt3A::SM_UpdateLastValue()
{
  if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			PSLastValueTempRequest = true;
			FLastValueTempRunning = true;
			CIISBICh->RequestTemp( PSRec->PSCanAdr ); // Tag always 0 fore Last value request

			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesPS();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			UpdateLastValueRunning = false;
			FLastValueSampRunning = false;  // Timeout if no frame was recived prevent hangup in PSIniState
			FLastValueTempRunning = false;
			break;
		}
	}
	T2 += SysClock;
}

bool __fastcall TCIISPowerSupply_FixVolt3A::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if( PSRec->PSVerMajor >= 3 )
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni(PSRec->PSCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

void __fastcall TCIISPowerSupply_FixVolt3A::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;
	TDateTime DTS;

	DTS = Now();

	PSRec->PSConnected = true;
	PSRec->PSStatus = 0;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 32767;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 32767;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 32767;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 65535;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 65535;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 65535;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 65535;
	}

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	PSRec->PSLastValueU = Ch1_Scl;
	PSRec->PSLastValueI = Ch2_Scl;
	PSRec->PSLastValueCh3 = Ch3_Scl;
	PSRec->PSLastValueCh4 = Ch4_Scl;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec );
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
	MVR.SensorSerialNo = PSRec->PSSerialNo;
	MVR.ValueType = "PSU";
	MVR.ValueUnit = PSRec->Ch1Unit;
	MVR.Value = Ch1_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PSI";
	MVR.ValueUnit =  PSRec->Ch2Unit;
	MVR.Value = Ch2_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS3";
	MVR.ValueUnit = PSRec->Ch3Unit;
	MVR.Value = Ch3_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS4";
	MVR.ValueUnit =  PSRec->Ch4Unit;
	MVR.Value = Ch4_Scl;

	DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( BusIntMsg[6] == 1 ) P_Prj->uCtrlPowerIn = true;
	else P_Prj->uCtrlPowerIn = false;

	if( BusIntMsg[7] == 1 ) P_Prj->uCtrlPowerOn = true;
	else P_Prj->uCtrlPowerOn = false;

	if( BusIntMsg[8] == 1 ) P_Prj->uCtrlPowerOk = true;
	else P_Prj->uCtrlPowerOk = false;

}

void __fastcall TCIISPowerSupply_FixVolt3A::uCtrlPowerSettings( const Byte *BusIntMsg )
{
	PSRec->PSSetVoltage = ((double)( (uint16_t)BusIntMsg[2] + (uint16_t)BusIntMsg[3]*256 )) / 1000;
	PSRec->PSSetCurrent = ((double)( (uint16_t)BusIntMsg[4] + (uint16_t)BusIntMsg[5]*256 )) / 3000;

	PSRec->PSMode = BusIntMsg[6];

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = false;
		break;
	}

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); DB->ApplyUpdatesPS();
	}

	P_Prj->DataChanged();

}

void __fastcall TCIISPowerSupply_FixVolt3A::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = PSRec->PSSerialNo;

	MValRec.ValueType = "PSU";
	MValRec.ValueUnit = PSRec->Ch1Unit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PSI";
	MValRec.ValueUnit = PSRec->Ch2Unit;
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS3";
	MValRec.ValueUnit = PSRec->Ch3Unit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS4";
	MValRec.ValueUnit = PSRec->Ch4Unit;
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );
}

// Camur II FixVolt 01A

__fastcall TCIISPowerSupply_FixVolt01A::TCIISPowerSupply_FixVolt01A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
  PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_FixVolt01A::~TCIISPowerSupply_FixVolt01A()
{


}

void __fastcall TCIISPowerSupply_FixVolt01A::SM_PSIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
	  if( !NodeDetected )
		{
		  PSRec->PSCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
		TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

	  case NI_ZoneAdr :
		CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion :
		CIISBICh->RequestVer( PSRec->PSCanAdr  );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

	  case NI_WaitForVer :
		if( VerRecived )
		{
			if( PSRec->PSVerMajor > PS_FV01_SupportedVer )
			{
				PSRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				PSRec->VerNotSupported = false;
				if( PSRec->PSVerMajor < 5 )  NIState = NI_SelMode;
				else NIState = NI_SetCamurIIMode;
				IniRetrys = NoOffIniRetrys;
			}

		}
		else // retry
		{
          FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		case NI_SetCamurIIMode:
		CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIRecPar;
		break;

		case NI_ClrCIIIRecPar:
		CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIAlarm;
		break;

		case NI_ClrCIIIAlarm:
		CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SelMode;
		break;

		case NI_SelMode:
		CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqCalibValues;
		break;

	  case NI_ReqCalibValues :
		CIISBICh->RequestIShunt( PSRec->PSCanAdr );
		CalibValuesRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

	  case NI_WaitForCalibValues :
		if( CalibValuesRecived )
		{
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_Ready :
		PSIniRunning = false;
		PSRec->PSConnected = true;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
		break;

	  case NI_TimeOut :
		PSIniRunning = false;
		PSRec->PSConnected = false;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		break;

	  default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS_FixVolt01A: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSIniCalibValues( TCIISBusPacket *BPIn )
{
	CalibValuesRecived = true;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 32767;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.002 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 65535;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.002 / 65535;
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	#if DebugCIISBusInt == 1
	Debug( "PSCalib recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt01A::SetDefaultValuesSubType()
{
  PSRec->Ch3Unit = "V";
  PSRec->Ch4Unit = "V";
}

void __fastcall TCIISPowerSupply_FixVolt01A::SM_PSIniState()
{
	if( FSampReqRunnig || FTempReqRunning || FSensGuardReqRunning || FLastValueSampRunning || FLastValueTempRunning ) return; 	// FV nodes can only buffer one command. Send kommands after sample req.

  switch (IniState)
  {
	case PSStateVOut:
	  CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 12000 );
	  IniState = PSStateIOut;
	  break;

	case PSStateIOut:
		CIISBICh->SetPSIOut( PSRec->PSCanAdr, PSRec->PSSetCurrent * 1000, 200 );
		IniState = PSStateOutputOn;
	  break;

	case PSStateOutputOn:
	  CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	  OutputTmpOff = false;
	  IniState = PSStateRampUpVout;
	  break;

	case PSStateRampUpVout:
	  if( PSRec->PSVOutEnabled )
	  {
			VRamp += 0.4;
			if( VRamp >= PSRec->PSSetVoltage )
			{
				VRamp = PSRec->PSSetVoltage;
				IniState = PSStateReady;
			}
	  }
	  else
	  {
			VRamp = 0;
			IniState = PSStateReady;
	  }
		CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
		break;

	case PSStateReady:
		PSIniStateRunning = false;
		break;

	default:
		break;
   }
}

void __fastcall TCIISPowerSupply_FixVolt01A::uCtrlIniState()
{
	TCIISZone_uCtrl *ZuCtrl;
	ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

	ZuCtrl->uCtrlSetPower( PSRec->PSSetVoltage * 1000,
												 PSRec->PSSetCurrent * 1000,
												 PSRec->PSMode,
												 PSRec->PSVOutEnabled );

	PSIniStateRunning = false;
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSSetOutputOff()
{
	 VRamp = 0;
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
	 OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSSetOutputState()
{
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	 OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSSample( TCIISBusPacket *BPIn )
{
	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		FSensGuardReqRunning = false;
	}
	else if( BPIn->Tag == 0 )
	{
		FLastValueSampRunning = false;
	}
	else
	{
		FSampReqRunnig = false;
	}

	TDateTime DTS;
	DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	}

   UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}


	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		//if( FIniDecaySample && PSRec->PSVerMajor >= 3 ) DValRec->SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec->SampleDateTime = IniDecayTime;
		else DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt01A::PSSampleExt( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSRequestTemp()
{
	FTempReqRunning = true;
	if( CIISBICh != NULL ) CIISBICh->RequestTemp( PSRec->PSCanAdr );
}

void __fastcall TCIISPowerSupply_FixVolt01A::PSSampleTemp( TCIISBusPacket *BPIn )
{
	if( BPIn->RequestedTag == 0 )
	{
		FLastValueTempRunning = false;
	}
	else
	{
		FTempReqRunning = false;
	}

  if( PSRec->PSTemp )
  {
		TDateTime DTS;
		DTS = Now();

		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;
		AnVal1 = ADVal1 * 2.048 / 12.25 - 273;

		if( BPIn->RequestedTag == 0 )
		{
			CIISMiscValueRec *MVR;
			MVR = new CIISMiscValueRec;

			MVR->DateTimeStamp = DTS;
			MVR->SensorSerialNo = PSRec->PSSerialNo;
			MVR->ValueType = "T";
			MVR->ValueUnit = "C";
			MVR->Value = AnVal1;

			DB->AppendMiscValueRec( MVR );
			delete MVR;

			P_Prj->LastValChanged();
		}
		else if( BPIn->RecType == RT_Monitor ) //Store Monitor Value
		{

			CIISMiscValueRec *MVR;
			MVR = new CIISMiscValueRec;

			MVR->DateTimeStamp = DTS;
			MVR->SensorSerialNo = PSRec->PSSerialNo;
			MVR->ValueType = "T";
			MVR->ValueUnit = "C";
			MVR->Value = AnVal1;

			DB->AppendMiscValueRec( MVR );
			delete MVR;

			P_Prj->LastValChanged();

			CIISMonitorValueRec *MValRec;
			MValRec = new CIISMonitorValueRec;

			MValRec->DateTimeStamp = DTS;
			MValRec->SampleDateTime = BPIn->SampleTimeStamp;
			MValRec->RecNo = BPIn->RecordingNo;
			MValRec->SensorSerialNo = PSRec->PSSerialNo;
			MValRec->ValueType = "T";
			MValRec->ValueUnit = "C";
			MValRec->RawValue = AnVal1;
			MValRec->Value = AnVal1;

			DB->AppendMonitorValueRec( MValRec );
			delete MValRec;

			P_Prj->DataChanged();
		}

		BPIn->MsgMode = CIISBus_MsgReady;
		BPIn->ParseMode = CIISParseReady;

		#if DebugCIISBusInt == 1
		Debug("Incoming temp recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(ADVal1));
		#endif
  }
}

void __fastcall TCIISPowerSupply_FixVolt01A::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			PSLastValueExtRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			if( PSRec->PSTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISPowerSupply_FixVolt01A::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SampleTimeStamp = Now();
		PSLastValueTempRequest = true;
		UpdateLastValueRunning = true;
		FLastValueTempRunning = true;
		CIISBICh->RequestTemp( PSRec->PSCanAdr );

		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISPowerSupply_FixVolt01A::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
  {
		switch (T2State)
		{
			case 10: // Request temp
			PSLastValueTempRequest = true;
			FLastValueTempRunning = true;
			CIISBICh->RequestTemp( PSRec->PSCanAdr ); // Tag always 0 fore Last value request

			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesPS();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			UpdateLastValueRunning = false;
			FLastValueSampRunning = false; // Timeout if no frame was recived prevent hangup in PSIniState
			FLastValueTempRunning = false;
			break;
		}
  }
  T2 += SysClock;
}

bool __fastcall TCIISPowerSupply_FixVolt01A::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if( PSRec->PSVerMajor >= 3 )
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni(PSRec->PSCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

void __fastcall TCIISPowerSupply_FixVolt01A::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;
	TDateTime DTS;

	DTS = Now();

	PSRec->PSConnected = true;
	PSRec->PSStatus = 0;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 32767;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.002 / 32767;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 32767;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 65535;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.002 / 65535;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 65535;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 65535;
	}

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	PSRec->PSLastValueU = Ch1_Scl;
	PSRec->PSLastValueI = Ch2_Scl;
	PSRec->PSLastValueCh3 = Ch3_Scl;
	PSRec->PSLastValueCh4 = Ch4_Scl;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec );
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
	MVR.SensorSerialNo = PSRec->PSSerialNo;
	MVR.ValueType = "PSU";
	MVR.ValueUnit = PSRec->Ch1Unit;
	MVR.Value = Ch1_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PSI";
	MVR.ValueUnit =  PSRec->Ch2Unit;
	MVR.Value = Ch2_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS3";
	MVR.ValueUnit = PSRec->Ch3Unit;
	MVR.Value = Ch3_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS4";
	MVR.ValueUnit =  PSRec->Ch4Unit;
	MVR.Value = Ch4_Scl;

	DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( BusIntMsg[6] == 1 ) P_Prj->uCtrlPowerIn = true;
	else P_Prj->uCtrlPowerIn = false;

	if( BusIntMsg[7] == 1 ) P_Prj->uCtrlPowerOn = true;
	else P_Prj->uCtrlPowerOn = false;

	if( BusIntMsg[8] == 1 ) P_Prj->uCtrlPowerOk = true;
	else P_Prj->uCtrlPowerOk = false;

}

void __fastcall TCIISPowerSupply_FixVolt01A::uCtrlPowerSettings( const Byte *BusIntMsg )
{
	PSRec->PSSetVoltage = ((double)( (uint16_t)BusIntMsg[2] + (uint16_t)BusIntMsg[3]*256 )) / 1000;
	PSRec->PSSetCurrent = ((double)( (uint16_t)BusIntMsg[4] + (uint16_t)BusIntMsg[5]*256 )) / 1000;

	PSRec->PSMode = BusIntMsg[6];

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = false;
		break;
	}

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); DB->ApplyUpdatesPS();
	}

	P_Prj->DataChanged();

}

void __fastcall TCIISPowerSupply_FixVolt01A::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = PSRec->PSSerialNo;

	MValRec.ValueType = "PSU";
	MValRec.ValueUnit = PSRec->Ch1Unit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PSI";
	MValRec.ValueUnit = PSRec->Ch2Unit;
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS3";
	MValRec.ValueUnit = PSRec->Ch3Unit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS4";
	MValRec.ValueUnit = PSRec->Ch4Unit;
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );
}


// Camur II FixVolt 1A

__fastcall TCIISPowerSupply_FixVolt1A::TCIISPowerSupply_FixVolt1A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
	PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
	T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_FixVolt1A::~TCIISPowerSupply_FixVolt1A()
{


}

void __fastcall TCIISPowerSupply_FixVolt1A::SM_PSIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  PSRec->PSCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
		TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

		case NI_ZoneAdr:
		CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion:
		CIISBICh->RequestVer( PSRec->PSCanAdr  );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

	  case NI_WaitForVer :
		if( VerRecived )
		{
			if( PSRec->PSVerMajor > PS_FV1_SupportedVer )
			{
				PSRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				PSRec->VerNotSupported = false;
				if( PSRec->PSVerMajor < 5 )  NIState = NI_SelMode;
				else NIState = NI_SetCamurIIMode;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_SetCamurIIMode:
		CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIRecPar;
		break;

		case NI_ClrCIIIRecPar:
		CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIAlarm;
		break;

		case NI_ClrCIIIAlarm:
		CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SelMode;
		break;


	  case NI_SelMode:
		CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqCalibValues;
		break;

	  case NI_ReqCalibValues :
		CIISBICh->RequestIShunt( PSRec->PSCanAdr );
		CalibValuesRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

	  case NI_WaitForCalibValues :
		if( CalibValuesRecived )
		{
		  NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_Ready :
		PSIniRunning = false;
		PSRec->PSConnected = true;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
		break;

	  case NI_TimeOut :
		PSIniRunning = false;
		PSRec->PSConnected = false;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		break;

	  default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug( "PSCapability recived PS_FixVolt1A: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSIniCalibValues( TCIISBusPacket *BPIn )
{
	CalibValuesRecived = true;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 32767;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BPIn->Byte3 * 0.2 / 65535;
		PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535;
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	#if DebugCIISBusInt == 1
	Debug( "PSCalib recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVolt1A::SetDefaultValuesSubType()
{
  PSRec->Ch3Unit = "V";
  PSRec->Ch4Unit = "V";
}

void __fastcall TCIISPowerSupply_FixVolt1A::SM_PSIniState()
{
  switch (IniState)
  {
  case PSStateVOut:
	CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 12000 );
	IniState = PSStateIOut;
	break;

  case PSStateIOut:
	CIISBICh->SetPSIOut( PSRec->PSCanAdr, PSRec->PSSetCurrent * 1000, 2000 );
	IniState = PSStateOutputOn;
	break;

  case PSStateOutputOn:
	CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	OutputTmpOff = false;
	IniState = PSStateRampUpVout;
	break;

	case PSStateRampUpVout:
	if( PSRec->PSVOutEnabled )
	{
	  VRamp += 0.2;
	  if( VRamp >= PSRec->PSSetVoltage )
	  {
		VRamp = PSRec->PSSetVoltage;
		IniState = PSStateReady;
	  }
	}
	else
	{
	  VRamp = 0;
	  IniState = PSStateReady;
	}
	CIISBICh->SetPSVOut( PSRec->PSCanAdr, VRamp * 1000, 20000 );
	break;

  case PSStateReady:
	PSIniStateRunning = false;
	break;

  default:
	break;
  }
}

void __fastcall TCIISPowerSupply_FixVolt1A::uCtrlIniState()
{
	TCIISZone_uCtrl *ZuCtrl;
	ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

	ZuCtrl->uCtrlSetPower( PSRec->PSSetVoltage * 1000,
												 PSRec->PSSetCurrent * 1000,
												 PSRec->PSMode,
												 PSRec->PSVOutEnabled );

	PSIniStateRunning = false;
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSSetOutputOff()
{
	 VRamp = 0;
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
	 OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSSetOutputState()
{
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	 OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSSample( TCIISBusPacket *BPIn )
{
	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		FSensGuardReqRunning = false;
	}
	else if( BPIn->Tag == 0 )
	{
		FLastValueSampRunning = false;
	}
	else
	{
		FSampReqRunnig = false;
	}

  TDateTime DTS;
	DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	}

   UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}

	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		//if( FIniDecaySample && PSRec->PSVerMajor >= 3 ) DValRec->SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec->SampleDateTime = IniDecayTime;
		else DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVolt1A::PSSampleExt( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSRequestTemp()
{
	FTempReqRunning = true;
	if( CIISBICh != NULL ) CIISBICh->RequestTemp( PSRec->PSCanAdr );
}

void __fastcall TCIISPowerSupply_FixVolt1A::PSSampleTemp( TCIISBusPacket *BPIn )
{
	if( BPIn->RequestedTag == 0 )
	{
		FLastValueTempRunning = false;
	}
	else
	{
		FTempReqRunning = false;
	}

  if( PSRec->PSTemp )
  {
		TDateTime DTS;
		DTS = Now();

		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;
		AnVal1 = ADVal1 * 2.048 / 12.25 - 273;

		if( BPIn->RequestedTag == 0 )
		{
			CIISMiscValueRec *MVR;
			MVR = new CIISMiscValueRec;

			MVR->DateTimeStamp = DTS;
			MVR->SensorSerialNo = PSRec->PSSerialNo;
			MVR->ValueType = "T";
			MVR->ValueUnit = "C";
			MVR->Value = AnVal1;

			DB->AppendMiscValueRec( MVR );
			delete MVR;

			P_Prj->LastValChanged();
		}
		else if( BPIn->RecType == RT_Monitor ) //Store Monitor Value
		{

			CIISMiscValueRec *MVR;
			MVR = new CIISMiscValueRec;

			MVR->DateTimeStamp = DTS;
			MVR->SensorSerialNo = PSRec->PSSerialNo;
			MVR->ValueType = "T";
			MVR->ValueUnit = "C";
			MVR->Value = AnVal1;

			DB->AppendMiscValueRec( MVR );
			delete MVR;

			P_Prj->LastValChanged();

			CIISMonitorValueRec *MValRec;
			MValRec = new CIISMonitorValueRec;

			MValRec->DateTimeStamp = DTS;
			MValRec->SampleDateTime = BPIn->SampleTimeStamp;
			MValRec->RecNo = BPIn->RecordingNo;
			MValRec->SensorSerialNo = PSRec->PSSerialNo;
			MValRec->ValueType = "T";
			MValRec->ValueUnit = "C";
			MValRec->RawValue = AnVal1;
			MValRec->Value = AnVal1;

			DB->AppendMonitorValueRec( MValRec );
			delete MValRec;

			P_Prj->DataChanged();
		}

		BPIn->MsgMode = CIISBus_MsgReady;
		BPIn->ParseMode = CIISParseReady;

		#if DebugCIISBusInt == 1
		Debug("Incoming temp recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(ADVal1));
		#endif
	}
}

void __fastcall TCIISPowerSupply_FixVolt1A::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			PSLastValueExtRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			if( PSRec->PSTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISPowerSupply_FixVolt1A::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SampleTimeStamp = Now();
		PSLastValueTempRequest = true;
		UpdateLastValueRunning = true;
		FLastValueTempRunning = true;
		CIISBICh->RequestTemp( PSRec->PSCanAdr );

		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISPowerSupply_FixVolt1A::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
  {
		switch (T2State)
		{
			case 10: // Request temp
			PSLastValueTempRequest = true;
			FLastValueTempRunning = true;
			CIISBICh->RequestTemp( PSRec->PSCanAdr ); // Tag always 0 fore Last value request

			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesPS();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			UpdateLastValueRunning = false;
			FLastValueSampRunning = false; // Timeout if no frame was recived prevent hangup in PSIniState
			FLastValueTempRunning = false;
			break;
		}
  }
  T2 += SysClock;
}

bool __fastcall TCIISPowerSupply_FixVolt1A::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if( PSRec->PSVerMajor >= 3 )
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni(PSRec->PSCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

void __fastcall TCIISPowerSupply_FixVolt1A::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;
	TDateTime DTS;

	DTS = Now();

	PSRec->PSConnected = true;
	PSRec->PSStatus = 0;

	if( PSRec->PSVerMajor < 3 )
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 32767;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 32767;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 32767;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 32767;
	}
	else
	{
		PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 65535;
		PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 65535;
		PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 65535;
		PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 65535;
	}

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	PSRec->PSLastValueU = Ch1_Scl;
	PSRec->PSLastValueI = Ch2_Scl;
	PSRec->PSLastValueCh3 = Ch3_Scl;
	PSRec->PSLastValueCh4 = Ch4_Scl;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec );
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
	MVR.SensorSerialNo = PSRec->PSSerialNo;
	MVR.ValueType = "PSU";
	MVR.ValueUnit = PSRec->Ch1Unit;
	MVR.Value = Ch1_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PSI";
	MVR.ValueUnit =  PSRec->Ch2Unit;
	MVR.Value = Ch2_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS3";
	MVR.ValueUnit = PSRec->Ch3Unit;
	MVR.Value = Ch3_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS4";
	MVR.ValueUnit =  PSRec->Ch4Unit;
	MVR.Value = Ch4_Scl;

	DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( BusIntMsg[6] == 1 ) P_Prj->uCtrlPowerIn = true;
	else P_Prj->uCtrlPowerIn = false;

	if( BusIntMsg[7] == 1 ) P_Prj->uCtrlPowerOn = true;
	else P_Prj->uCtrlPowerOn = false;

	if( BusIntMsg[8] == 1 ) P_Prj->uCtrlPowerOk = true;
	else P_Prj->uCtrlPowerOk = false;

}

void __fastcall TCIISPowerSupply_FixVolt1A::uCtrlPowerSettings( const Byte *BusIntMsg )
{
	PSRec->PSSetVoltage = ((double)( (uint16_t)BusIntMsg[2] + (uint16_t)BusIntMsg[3]*256 )) / 1000;
	PSRec->PSSetCurrent = ((double)( (uint16_t)BusIntMsg[4] + (uint16_t)BusIntMsg[5]*256 )) / 1000;

	PSRec->PSMode = BusIntMsg[6];

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = false;
		break;
	}

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); DB->ApplyUpdatesPS();
	}

	P_Prj->DataChanged();

}

void __fastcall TCIISPowerSupply_FixVolt1A::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = PSRec->PSSerialNo;

	MValRec.ValueType = "PSU";
	MValRec.ValueUnit = PSRec->Ch1Unit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PSI";
	MValRec.ValueUnit = PSRec->Ch2Unit;
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS3";
	MValRec.ValueUnit = PSRec->Ch3Unit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS4";
	MValRec.ValueUnit = PSRec->Ch4Unit;
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );
}


// Camur II FixVoltHV1A

__fastcall TCIISPowerSupply_FixVoltHV1A::TCIISPowerSupply_FixVoltHV1A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent )
							   :TCIISPowerSupply( SetDB, SetCIISBusInt, SetDebugWin, SetPSRec, SetCIISParent )
{
  CIISObjType = CIISPowerSupply;
  PSRec = SetPSRec;
  PSIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  PSRec->PSConnected = false;
}

__fastcall TCIISPowerSupply_FixVoltHV1A::~TCIISPowerSupply_FixVoltHV1A()
{


}

void __fastcall TCIISPowerSupply_FixVoltHV1A::SM_PSIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  PSRec->PSCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( PSRec->PSSerialNo, PSRec->PSCanAdr );
		TNextEvent += AcceptDelay + ( PSRec->PSCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

	  case NI_ZoneAdr :
		CIISBICh->SetGroup( PSRec->PSCanAdr, PSRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion :
		CIISBICh->RequestVer( PSRec->PSCanAdr  );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForVer :
		if( VerRecived )
		{
			if( PSRec->PSVerMajor > PS_FVHV1_SupportedVer )
			{
				PSRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				PSRec->VerNotSupported = false;
				NIState = NI_SetCamurIIMode;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		case NI_SetCamurIIMode:
		CIISBICh->SetCamurIIMode( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIRecPar;
		break;

		case NI_ClrCIIIRecPar:
		CIISBICh->ClearCIIIRecPar( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ClrCIIIAlarm;
		break;

		case NI_ClrCIIIAlarm:
		CIISBICh->ClearCIIIAlarm( PSRec->PSCanAdr );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SetFallback;
		break;

		case NI_SetFallback:
		CIISBICh->SetPSFallback( PSRec->PSCanAdr, PSRec->Fallback);
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_SelMode;
		break;

	  case NI_SelMode:
		CIISBICh->SetPSMode( PSRec->PSCanAdr, PSRec->PSMode );
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqCalibValues_Ver4;
		break;

		case NI_ReqCalibValues_Ver4:
		BitValueRequested = 0;
		CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
		BitValueRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCh1BitValue;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForCh1BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 1;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh2BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh2BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 2;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh3BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh3BitValue:
		if( BitValueRecived )
		{
			BitValueRequested = 3;
			CIISBICh->RequestBitVal( PSRec->PSCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh4BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_WaitForCh4BitValue:
		if( BitValueRecived )
		{
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_Ready :
		PSIniRunning = false;
		PSRec->PSConnected = true;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		// If the PS is not initiated from Controller:RestartCIIBus (Power recovery on a PS on the external CIIBus) output is initiaded here
		if( !P_Ctrl->RestartRunning && (( P_Zone->RecType == RT_StandBy ) || ( P_Zone->RecType == RT_Monitor ) ) ) PSIniState();
		break;

	  case NI_TimeOut :
		PSIniRunning = false;
		PSRec->PSConnected = false;
		if( DB->LocatePSRec( PSRec ) )
		{
		  DB->SetPSRec( PSRec );
		}
		break;

	  default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSIniCapability( TCIISBusPacket *BPIn )
{
  PSRec->PSType = BPIn->NodeCap;
	NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "PSCapability recived PS_FixVoltHV1A: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSIniCalibValues( TCIISBusPacket *BPIn )
{
  CalibValuesRecived = true;


	switch (BitValueRequested)
	{
		case 0: PSRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		case 1: PSRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		case 2: PSRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 65535; break;
		case 3: PSRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 65535; break;
	}
	BitValueRecived = true;



  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  if( DB->LocatePSRec( PSRec ) )
  {
		DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	#if DebugCIISBusInt == 1
	Debug( "PSCalib recived PS_FixVolt: " + IntToStr( PSRec->PSSerialNo ) + " / " + IntToStr( PSRec->PSCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::SetDefaultValuesSubType()
{
  PSRec->Ch3Unit = "V";
	PSRec->Ch4Unit = "V";

}

void __fastcall TCIISPowerSupply_FixVoltHV1A::SM_PSIniState()
{
  switch (IniState)
  {
	case PSStateVOut:
		CIISBICh->SetPSVOut( PSRec->PSCanAdr, PSRec->PSSetVoltage * 1000, 40000 );
	  IniState = PSStateIOut;
	  break;

	case PSStateIOut:
		CIISBICh->SetPSIOut( PSRec->PSCanAdr, PSRec->PSSetCurrent * 3000, 10000 );
		IniState = PSStateOutputOn;
	  break;

	case PSStateOutputOn:
		if( RampDelay == 0 ) // FixVolt with internal U/I ramping  dont responde to VOutEnable while stepping up U/I
		{
			CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
			OutputTmpOff = false;
			IniState = PSStateReady;
		}
		else RampDelay--;
		break;

	case PSStateReady:
	  PSIniStateRunning = false;
	  break;

	default:
	break;
  }
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::uCtrlIniState()
{
	TCIISZone_uCtrl *ZuCtrl;
	ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

	ZuCtrl->uCtrlSetPower( PSRec->PSSetVoltage * 1000,
												 PSRec->PSSetCurrent * 3000,
												 PSRec->PSMode,
												 PSRec->PSVOutEnabled );

	PSIniStateRunning = false;
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSSetOutputOff()
{
	 VRamp = 0;
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, false );
	 OutputTmpOff = true;
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSSetOutputState()
{
	 if( CIISBICh != NULL ) CIISBICh->SetPSOutputOn( PSRec->PSCanAdr, PSRec->PSVOutEnabled );
	 OutputTmpOff = false;
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch1BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	}

   UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch2BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	}

	// Handle SensGuard

	if( BPIn->Tag == ExpectedSenseGuardTag )
	{

		if((PSRec->PSVOutEnabled ) && ( !OutputTmpOff ) && ( PSRec->PSMode == 1	) && ( PSRec->SenseGuardEnabled ))
		{
			if( fabs( PSRec->PSSetVoltage - AnVal1_Scl ) > SenseGuardVDiff )
			{
				SenseGuardDelay -= SenseGuardInterval;

				if( --SenseGuardDelay <= 0 )
				{
					this->PSSetOutputOff();
					PSRec->PSVOutEnabled = false;
					if( DB->SetPSRec( PSRec ) )
					{
						DB->ApplyUpdatesPS();
					}

					P_Prj->Log( LevAlarm, CIIEventCode_SenseGuardPSOff, CIIEventLevel_High, PSRec->PSName, PSRec->PSSerialNo, PSRec->PSLastValueU );

					Debug(" PS " + IntToStr( PSRec->PSSerialNo ) + " tuned off ( SensGuard )" );
				}
			}
			else SenseGuardDelay = SenseGuardDelayTime;
		}
		return;
	}

  PSRec->PSLastValueU = AnVal1_Scl;
  PSRec->PSLastValueI = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PSU";
		MVR->ValueUnit = PSRec->Ch1Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PSI";
		MVR->ValueUnit =  PSRec->Ch2Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
	}

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PSU";
		MValRec->ValueUnit = PSRec->Ch1Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PSI";
		MValRec->ValueUnit = PSRec->Ch2Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
  }
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
  {
		CIISDecayValueRec *DValRec;
		DValRec = new CIISDecayValueRec;

		DValRec->DateTimeStamp = DTS;
		//if( FIniDecaySample && PSRec->PSVerMajor >= 3 ) DValRec->SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec->SampleDateTime = IniDecayTime;
		else DValRec->SampleDateTime = BPIn->SampleTimeStamp;
		DValRec->RecNo = BPIn->RecordingNo;
		DValRec->SensorSerialNo = PSRec->PSSerialNo;
		DValRec->Value = AnVal1_Scl;
		DValRec->ValueType = "PSU";
		DB->AppendDecayValueRec( DValRec );

		DValRec->Value = AnVal2_Scl;
		DValRec->ValueType = "PSI";
		DB->AppendDecayValueRec( DValRec );

		delete DValRec;

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSSampleExt( TCIISBusPacket *BPIn )
{
	if( BPIn->Tag == ExpectedSenseGuardTag )
	{
		FSensGuardReqRunning = false;
		return;
	}
	else if( BPIn->Tag == 0 )
	{
		FLastValueSampRunning = false;
	}
	else
	{
		FSampReqRunnig = false;
	}

	TDateTime DTS;
	DTS = Now();

	UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	if( UADVal1 == 65534 )  // PSU underflow;  Ver 3.22.0.0
	{
		AnVal1 = FixVoltUnderflow;
		AnVal1_Scl = FixVoltUnderflow;
	}
	else if( UADVal1 == 65535 ) // PSU overflow;
	{
		AnVal1 = FixVoltOverflow;
		AnVal1_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal1 = UADVal1; AnVal1 = AnVal1 * PSRec->Ch3BitVal;
		AnVal1_Scl = AnVal1 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	}

	UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	if( UADVal2 == 65534 )  // PSI underflow;
	{
		AnVal2 = FixVoltUnderflow;
		AnVal2_Scl = FixVoltUnderflow;
	}
	else if( UADVal2 == 65535 ) // PSI overflow;
	{
		AnVal2 = FixVoltOverflow;
		AnVal2_Scl = FixVoltOverflow;
	}
	else
	{
		AnVal2 = UADVal2; AnVal2 = AnVal2 * PSRec->Ch4BitVal;
		AnVal2_Scl = AnVal2 * PSRec->Ch4Gain + PSRec->Ch4Offset;
	}

	PSRec->PSLastValueCh3 = AnVal1_Scl;
	PSRec->PSLastValueCh4 = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec *MVR;
		MVR = new CIISMiscValueRec;

		MVR->DateTimeStamp = DTS;
		MVR->SensorSerialNo = PSRec->PSSerialNo;
		MVR->ValueType = "PS3";
		MVR->ValueUnit = PSRec->Ch3Unit;
		MVR->Value = AnVal1_Scl;

		DB->AppendMiscValueRec( MVR );

		MVR->ValueType = "PS4";
		MVR->ValueUnit =  PSRec->Ch4Unit;
		MVR->Value = AnVal2_Scl;

		DB->AppendMiscValueRec( MVR );

		delete MVR;

		P_Prj->LastValChanged();
  }



  if( DB->LocatePSRec( PSRec ) )
  {
	  DB->SetPSRec( PSRec ); //DB->ApplyUpdatesPS();
  }

  if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{

		CIISMonitorValueRec *MValRec;
		MValRec = new CIISMonitorValueRec;

		MValRec->DateTimeStamp = DTS;
		MValRec->SampleDateTime = BPIn->SampleTimeStamp;
		MValRec->RecNo = BPIn->RecordingNo;
		MValRec->SensorSerialNo = PSRec->PSSerialNo;
		MValRec->ValueType = "PS3";
		MValRec->ValueUnit = PSRec->Ch3Unit;
		MValRec->RawValue = AnVal1;
		MValRec->Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( MValRec );

		MValRec->ValueType = "PS4";
		MValRec->ValueUnit = PSRec->Ch4Unit;
		MValRec->RawValue = AnVal2;
		MValRec->Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( MValRec );

		delete MValRec;

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(UADVal2));
  #endif

}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSRequestTemp()
{
	FTempReqRunning = true;
	if( CIISBICh != NULL ) CIISBICh->RequestTemp( PSRec->PSCanAdr );
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::PSSampleTemp( TCIISBusPacket *BPIn )
{
	if( BPIn->RequestedTag == 0 )
	{
		FLastValueTempRunning = false;
	}
	else
	{
		FTempReqRunning = false;
	}

  if( PSRec->PSTemp )
  {
		TDateTime DTS;
		DTS = Now();

		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;

	if( PSRec->PSVerMajor < 3 )
	{
	  if( ADVal1 & 512 ) ADVal1 = - ((( ~ADVal1 ) & 511 ) + 1 );
	  AnVal1 = ADVal1;
	  AnVal1 = AnVal1 / 4;
	}
	else AnVal1 = ADVal1;

	if( BPIn->RequestedTag == 0 )
	{
	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();
	}
	else if( BPIn->RecType == RT_Monitor ) //Store Monitor Value
	{

	  CIISMiscValueRec *MVR;
	  MVR = new CIISMiscValueRec;

	  MVR->DateTimeStamp = DTS;
	  MVR->SensorSerialNo = PSRec->PSSerialNo;
	  MVR->ValueType = "T";
	  MVR->ValueUnit = "C";
	  MVR->Value = AnVal1;

	  DB->AppendMiscValueRec( MVR );
	  delete MVR;

	  P_Prj->LastValChanged();

	  CIISMonitorValueRec *MValRec;
	  MValRec = new CIISMonitorValueRec;

	  MValRec->DateTimeStamp = DTS;
	  MValRec->SampleDateTime = BPIn->SampleTimeStamp;
	  MValRec->RecNo = BPIn->RecordingNo;
	  MValRec->SensorSerialNo = PSRec->PSSerialNo;
	  MValRec->ValueType = "T";
	  MValRec->ValueUnit = "C";
	  MValRec->RawValue = AnVal1;
	  MValRec->Value = AnVal1;

	  DB->AppendMonitorValueRec( MValRec );
	  delete MValRec;

	  P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	#if DebugCIISBusInt == 1
	Debug("Incoming temp recived PS: " + IntToStr( PSRec->PSSerialNo ) + " = " + IntToStr(UADVal1) + " / " + IntToStr(ADVal1));
	#endif
  }
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;

		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
		{
			SampleTimeStamp = Now();
			PSLastValueRequest = true;
			PSLastValueExtRequest = true;
			UpdateLastValueRunning = true;
			CIISBICh->RequestSample( PSRec->PSCanAdr, 0 ); // Tag always 0 fore Last value request

			if( PSRec->PSTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SampleTimeStamp = Now();
		PSLastValueTempRequest = true;
		UpdateLastValueRunning = true;
		FLastValueTempRunning = true;
		CIISBICh->RequestTemp( PSRec->PSCanAdr );
		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::SM_UpdateLastValue()
{
  if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			PSLastValueTempRequest = true;
			FLastValueTempRunning = true;
			CIISBICh->RequestTemp( PSRec->PSCanAdr ); // Tag always 0 fore Last value request

			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesPS();
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();

			UpdateLastValueRunning = false;
			FLastValueSampRunning = false;  // Timeout if no frame was recived prevent hangup in PSIniState
			FLastValueTempRunning = false;
			break;
		}
	}
	T2 += SysClock;
}

bool __fastcall TCIISPowerSupply_FixVoltHV1A::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if( PSRec->PSVerMajor >= 3 )
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni(PSRec->PSCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

void __fastcall TCIISPowerSupply_FixVoltHV1A::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;
	TDateTime DTS;

	DTS = Now();

	PSRec->PSConnected = true;
	PSRec->PSStatus = 0;


	PSRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 65535;
	PSRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 65535;
	PSRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 65535;
	PSRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 65535;


	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	PSRec->PSLastValueU = Ch1_Scl;
	PSRec->PSLastValueI = Ch2_Scl;
	PSRec->PSLastValueCh3 = Ch3_Scl;
	PSRec->PSLastValueCh4 = Ch4_Scl;

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec );
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
	MVR.SensorSerialNo = PSRec->PSSerialNo;
	MVR.ValueType = "PSU";
	MVR.ValueUnit = PSRec->Ch1Unit;
	MVR.Value = Ch1_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PSI";
	MVR.ValueUnit =  PSRec->Ch2Unit;
	MVR.Value = Ch2_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS3";
	MVR.ValueUnit = PSRec->Ch3Unit;
	MVR.Value = Ch3_Scl;

	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "PS4";
	MVR.ValueUnit =  PSRec->Ch4Unit;
	MVR.Value = Ch4_Scl;

	DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( BusIntMsg[6] == 1 ) P_Prj->uCtrlPowerIn = true;
	else P_Prj->uCtrlPowerIn = false;

	if( BusIntMsg[7] == 1 ) P_Prj->uCtrlPowerOn = true;
	else P_Prj->uCtrlPowerOn = false;

	if( BusIntMsg[8] == 1 ) P_Prj->uCtrlPowerOk = true;
	else P_Prj->uCtrlPowerOk = false;

}

void __fastcall TCIISPowerSupply_FixVoltHV1A::uCtrlPowerSettings( const Byte *BusIntMsg )
{
	PSRec->PSSetVoltage = ((double)( (uint16_t)BusIntMsg[2] + (uint16_t)BusIntMsg[3]*256 )) / 500;
	PSRec->PSSetCurrent = ((double)( (uint16_t)BusIntMsg[4] + (uint16_t)BusIntMsg[5]*256 )) / 3000;

	PSRec->PSMode = BusIntMsg[6];

	switch( BusIntMsg[7] )
	{
	case 0:
		PSRec->PSVOutEnabled = true;
		break;

	case 1:
		PSRec->PSVOutEnabled = false;
		break;
	}

	if( DB->LocatePSRec( PSRec ) )
	{
		DB->SetPSRec( PSRec ); DB->ApplyUpdatesPS();
	}

	P_Prj->DataChanged();

}

void __fastcall TCIISPowerSupply_FixVoltHV1A::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	Ch1 =  PSRec->Ch1BitVal * (uint16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  PSRec->Ch2BitVal * (uint16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  PSRec->Ch3BitVal * (uint16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  PSRec->Ch4BitVal * (uint16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * PSRec->Ch1Gain + PSRec->Ch1Offset;
	Ch2_Scl = Ch2 * PSRec->Ch2Gain + PSRec->Ch2Offset;
	Ch3_Scl = Ch3 * PSRec->Ch3Gain + PSRec->Ch3Offset;
	Ch4_Scl = Ch4 * PSRec->Ch4Gain + PSRec->Ch4Offset;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = PSRec->PSSerialNo;

	MValRec.ValueType = "PSU";
	MValRec.ValueUnit = PSRec->Ch1Unit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PSI";
	MValRec.ValueUnit = PSRec->Ch2Unit;
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS3";
	MValRec.ValueUnit = PSRec->Ch3Unit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "PS4";
	MValRec.ValueUnit = PSRec->Ch4Unit;
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );
}

