//---------------------------------------------------------------------------
// History
//
//  Date			Comment							Sign
//  2015-02-08	Converted to Xe7,
// 				Removed obsolete TTable,TBatchMove
//				declarations				    	Bo H
//---------------------------------------------------------------------------

#ifndef CIISDBH
#define CIISDBH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Db.hpp>
#include "CIISCommon.h"
#include <DB.hpp>

#include "DebugWinU.h"
#include "CIISDBInit.h"
#include <DBClient.hpp>
#include <FMTBcd.hpp>
#include <Provider.hpp>
#include <SqlExpr.hpp>
#include <WideStrings.hpp>
#include <SimpleDS.hpp>

#include <Data.DBXMySQL.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Phys.MySQL.hpp>
#include <FireDAC.Phys.MySQLDef.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.VCLUI.Wait.hpp>

const int32_t DBVer = 3900;


//---------------------------------------------------------------------------


class TCIISDBModule : public TDataModule
{
__published:	// IDE-managed Components
	TClientDataSet *TabValuesAppend;
	TClientDataSet *TabValuesLPRAppend;
	TClientDataSet *TabValuesDecayAppend;
	TFDConnection *FDConnection;
	TFDGUIxWaitCursor *FDGUIxWaitCursor;
	TFDTable *FDTableProject;
	TFDTable *FDTableControllers;
	TFDTable *FDTableZones;
	TFDTable *FDTableSensors;
	TFDTable *FDTablePowerSupply;
	TFDTable *FDTableRecordings;
	TFDTable *FDTableEventLog;
	TFDTable *FDTableWLink;
	TFDTable *FDTableAlarm;
	TFDQuery *FDQueryValues;
	TFDQuery *FDQueryValuesLPR;
	TFDQuery *FDQueryValuesDecay;
	TFDTable *FDTableValuesMisc;
	TFDTable *FDTableValuesCalc;
	TFDQuery *FDQuery;
	TClientDataSet *ClientDataSetValuesMisc;
	TFDTable *FDTableBIChannels;
private:
	CIISServerMode ServerMode;
	String DBHostName;
	String DBName;
			// MySQL
	String DBUserName;
	String DBPassword;

	int32_t InstalledDBVer;
	Boolean ForceUpdate;

	//unsigned long int32_t InsertedMiscValues;
	uint64_t InsertedMiscValues;

  Boolean MiscValueInserted;
  Boolean EventInserted;

	String EMess;
	TCIISDBInitFrm *IFrm;
	TLocateOptions NoOpts;

	TDebugWin *DW;

	TDateTime LastValueMonitorTimeStamp;
	TDateTime LastValueLPRTimeStamp;
	TDateTime LastValueDecayTimeStamp;
	TDateTime LastValueMiscTimeStamp;
	TDateTime LastValueCalcTimeStamp;

	TDateTime LastStoredEvent;
  int32_t LastStoredFraction;

	void __fastcall CreateTabProject();
	void __fastcall CreateTabControllers();
	void __fastcall CreateTabWLink();
	void __fastcall CreateTabZones();
	void __fastcall CreateTabSensors();
	void __fastcall CreateTabPowerSupply();
	void __fastcall CreateTabRecordings();
	void __fastcall CreateTabValues();
	void __fastcall CreateTabValuesLPR();
	void __fastcall CreateTabValuesDecay();
	void __fastcall CreateTabValuesMisc();
	void __fastcall CreateTabValuesCalc();
	void __fastcall CreateTabEventLog();
	void __fastcall CreateTabAlarm();
	void __fastcall CreateTabBIChannels();

	bool __fastcall CheckUpdateTabProject();
	bool __fastcall CheckUpdateTabControllers();
	bool __fastcall CheckUpdateTabWLink();
	bool __fastcall CheckUpdateTabZones();
	bool __fastcall CheckUpdateTabSensors();
	bool __fastcall CheckUpdateTabPowerSupply();
	bool __fastcall CheckUpdateTabRecordings();
	bool __fastcall CheckUpdateTabValues();
	bool __fastcall CheckUpdateTabValuesLPR();
	bool __fastcall CheckUpdateTabValuesDecay();
	bool __fastcall CheckUpdateTabEventLog();
	bool __fastcall CheckUpdateTabAlarm();
	bool __fastcall CheckUpdateTabBIChannels();

	void __fastcall UpdateRecEnd();
	void __fastcall UpdateVer();


	bool __fastcall CreateSQLDatabase(TFDConnection *SQLConnection,String DBName);

	String __fastcall SQLFieldTypeString(enum TFieldType : unsigned char,int32_t Size);

	String __fastcall GenerateSQLCreateTable(String DBName,String TableName,
										TClientDataSet *TableSrc);
	String __fastcall GenerateSQLCreateTable(String DBName,String TableName,
										TFDDataSet *TableSrc);
	bool __fastcall DeleteSQLTable(TFDConnection *SQLConnection,String DBName,
										String TableName);
	bool __fastcall SQLTableExists(String DBName,String TableName);

	bool __fastcall SQLFieldExists(String DBName,String TableName,String FieldName);
	bool __fastcall SQLAddField(String DBName,String TableName,String FieldName,
										enum TFieldType : unsigned char ,String DefaultExpr="",int32_t Size=0,String AfterField="");

	bool __fastcall SQLUpdateField(String DBName,String TableName, String DestFieldName, String OrgFieldName );
	bool __fastcall SQLDropPrimaryKey(String DBName,String TableName);
	bool __fastcall SQLAddPrimaryKey(String DBName,String TableName,String KeyFields);
    bool	__fastcall SQLApplyInserts(String TableName,TClientDataSet *DataSet);

	void __fastcall Debug(String Message);
	void __fastcall Debug_AlwOn(String Message);

	bool 	__fastcall PostEventRec( CIISEventRec *EventRec);
	bool	__fastcall PostPrjRec( CIISPrjRec *PrjRec );
	bool	__fastcall PostCtrlRec( CIISCtrlRec *CtrlRec );
	bool	__fastcall PostWLinkRec( CIISWLinkRec *WLinkRec );
	bool	__fastcall PostZoneRec( CIISZoneRec *ZoneRec);
	bool	__fastcall PostSensorRec( CIISSensorRec *SensorRec );
	bool	__fastcall PostPSRec( CIISPowerSupplyRec *PSRec );
	bool	__fastcall PostRecordingRec( CIISRecordingRec *RecordingRec );
	bool	__fastcall PostAlarmRec( CIISAlarmRec *AlarmRec );
	bool	__fastcall PostBIChannelRec( CIISBIChannelRec *BIChannelRec );

	bool	__fastcall  VerifyDB(String DBName);
	void __fastcall WriteDateTime(TDataSet *ClientDataSet,String FieldName,TDate Date);
	void __fastcall WriteDate(TDataSet *ClientDataSet,String FieldName,TDate Date);
	TDateTime __fastcall ReadDateTime(TDataSet *ClientDataSet,String FieldName);
	TDate __fastcall ReadDate(TDataSet *ClientDataSet,String FieldName);
	void __fastcall WriteBool(TDataSet *ClientDataSet,String FieldName,bool Value);
	bool __fastcall ReadBool(TDataSet *ClientDataSet,String FieldName);

	String __fastcall DateTimeToSQLStr( TDateTime t );
	String __fastcall FieldToSQLStr(TClientDataSet *ds, int32_t fi);
	String __fastcall FloatToSQLStr( String FloatAsString );

	String __fastcall SQLSelectFromTimeStamp(String TableName, TDateTime DateTimeStamp, int32_t Fraction);

	String __fastcall TodayThisHourSQLStr(void);
	String __fastcall TodayThisSecSQLStr(void);

	bool    __fastcall FindLastMonitorValueInRecording(int32_t RecNo);
	bool    __fastcall FindLastLPRValueInRecording(int32_t RecNo);
	bool    __fastcall FindLastDecayValueInRecording(int32_t RecNo);

		TDateTime __fastcall GetLastMonitorTimestamp(void);
    TDateTime __fastcall GetLastLPRTimestamp(void);
    TDateTime __fastcall GetLastDecayTimestamp(void);
		TDateTime __fastcall GetLastMiscTimestamp(void);
		TDateTime __fastcall GetLastCalcTimestamp(void);

	void __fastcall DisplayLastErr(String MessageCaption);


public:

			__fastcall TCIISDBModule(TComponent* Owner);
			__fastcall ~TCIISDBModule();

    void __fastcall SetDebugWin(TDebugWin *SetDebugWin);


	void 	__fastcall LogEvent(CIIEventType EvType, CIIEventCode EvCode,
						CIIEventLevel EvLevel, String EvString,
						int32_t EvInt, double EvFloat, int32_t EvLogSize);	// User declarations


	bool    __fastcall IsMySQLServiceStarted(void);
	bool	__fastcall IsLoginPossible(String DBHostName,
										String DBUserName,
										String DBPassword);

	bool	__fastcall DatabaseExists(String DBHostName,
										String DBUserName,
										String DBPassword,
										String DBName);

	bool	__fastcall InitCIISDB( String SetDBHostName,String SetDBName, CIISServerMode SetServerMode ,
			String SetDBUserName, String SetDBPassword, TDebugWin * SetDebugWin=NULL);

	void	__fastcall CloseCIISDB(void);
	String	__fastcall DatabaseName(void);
	bool	__fastcall SetDBDir( String NewDir );
	bool	__fastcall RecRunning();

	//		Project table
	bool	__fastcall GetPrjRec( CIISPrjRec *PrjRec );
	bool	__fastcall SetPrjRec( CIISPrjRec *PrjRec );
	bool	__fastcall ApplyUpdatesPrj(void);

	//		Ctrl table
	bool	__fastcall FindFirstCtrl();
	bool	__fastcall LocateCtrlRec( CIISCtrlRec *CtrlRec );
	bool	__fastcall GetCtrlRec( CIISCtrlRec *CtrlRec, bool AutoInc );
	bool	__fastcall SetCtrlRec( CIISCtrlRec *CtrlRec );
	bool	__fastcall DeleteCtrlRec( CIISCtrlRec *CtrlRec );
	bool	__fastcall ApplyUpdatesCtrl(void);

	//		WLink table
	uint32_t __fastcall GetWLinkRecCount();
	void	__fastcall SetWLinkFilter( CIISCtrlRec *CtrlRec );
	void	__fastcall ClearWLinkFilter();
	bool	__fastcall FindFirstWLink();
	bool	__fastcall LocateWLinkRec( CIISWLinkRec *WLinkRec );
	bool	__fastcall GetWLinkRec( CIISWLinkRec *WLinkRec, bool AutoInc );
	bool	__fastcall SetWLinkRec( CIISWLinkRec *WLinkRec);
	bool	__fastcall AppendWLinkRec( CIISWLinkRec *WLinkRec);
	bool	__fastcall DeleteWLinkRec( CIISWLinkRec *WLinkRec );
	bool	__fastcall ApplyUpdatesWLink(void);

	//		Zone table
	uint32_t __fastcall GetZoneRecCount();
	void	__fastcall SetZoneFilter( CIISCtrlRec *CtrlRec );
	void	__fastcall ClearZoneFilter();
	bool	__fastcall FindFirstZone();
	bool	__fastcall LocateZoneRec( CIISZoneRec *ZoneRec );
	bool	__fastcall GetZoneRec( CIISZoneRec *ZoneRec, bool AutoInc );
	bool	__fastcall SetZoneRec( CIISZoneRec *ZoneRec);
	bool	__fastcall AppendZoneRec( CIISZoneRec *ZoneRec, bool AutoInc );
	bool	__fastcall DeleteZoneRec( CIISZoneRec *ZoneRec );
	bool	__fastcall ApplyUpdatesZone(void);

	//		Sensor table
	uint32_t __fastcall GetSensorRecCount();
	void	__fastcall SetSensorFilter( CIISZoneRec *ZoneRec );
	void	__fastcall ClearSensorFilter();
	bool	__fastcall FindFirstSensor();
	bool	__fastcall LocateSensorRec( CIISSensorRec *SensorRec );
	bool	__fastcall GetSensorRec( CIISSensorRec *SensorRec, bool AutoInc );
	bool	__fastcall SetSensorRec( CIISSensorRec *SensorRec );
	bool    __fastcall AppendSensorRec( CIISSensorRec *SensorRec );
	bool	__fastcall DeleteSensorRec( CIISSensorRec *SensorRec );
	bool	__fastcall ApplyUpdatesSensor(void);

	//		PowerSupply table
	uint32_t __fastcall GetPSRecCount();
	void	__fastcall SetPSFilter( CIISZoneRec *ZoneRec );
	void	__fastcall ClearPSFilter();
	bool	__fastcall FindFirstPS();
	bool	__fastcall LocatePSRec( CIISPowerSupplyRec *PSRec );
	bool	__fastcall GetPSRec( CIISPowerSupplyRec *PSRec, bool AutoInc );
	bool	__fastcall SetPSRec( CIISPowerSupplyRec *PSRec );
	bool 	__fastcall AppendPSRec( CIISPowerSupplyRec *PSRec );
	bool	__fastcall DeletePSRec( CIISPowerSupplyRec *PSRec );
	bool	__fastcall ApplyUpdatesPS(void);

		//		Alarm table
	uint32_t __fastcall GetAlarmRecCount();
	void	__fastcall SetAlarmFilter( CIISZoneRec *ZoneRec );
	void	__fastcall ClearAlarmFilter();
	bool	__fastcall FindFirstAlarm();
	bool	__fastcall LocateAlarmRec( CIISAlarmRec *AlarmRec );
	bool	__fastcall GetAlarmRec( CIISAlarmRec *AlarmRec, bool AutoInc );
	bool	__fastcall SetAlarmRec( CIISAlarmRec *AlarmRec);
	bool	__fastcall AppendAlarmRec( CIISAlarmRec *AlarmRec);
	bool	__fastcall DeleteAlarmRec( CIISAlarmRec *AlarmRec );
	bool	__fastcall ApplyUpdatesAlarm(void);

		//		BIChannels table
	uint32_t __fastcall GetBIChannelsRecCount();
	bool	__fastcall FindFirstBIChannel();
	bool	__fastcall LocateBIChannelRec( CIISBIChannelRec *BIChannelRec );
	bool	__fastcall GetBIChannelRec( CIISBIChannelRec *BIChannelRec, bool AutoInc );
	bool	__fastcall SetBIChannelRec( CIISBIChannelRec *BIChannelRec);
	bool	__fastcall AppendBIChannelRec( CIISBIChannelRec *BIChannelRec);
	bool	__fastcall DeleteBIChannelRec( CIISBIChannelRec *BIChannelRec );
	bool	__fastcall ApplyUpdatesBIChannels(void);

	//		Eventlog table
	uint32_t __fastcall GetEventLogRecCount();
	bool	__fastcall FindFirstEvent();
	bool	__fastcall FindLastEvent();
	bool	__fastcall LocateEvent( TDateTime EvDateTime, int32_t EventNo );
//	bool	__fastcall LocateEvent( uint32_t RecNo );
	bool	__fastcall FindNextEvent();
	bool	__fastcall GetEventRec( CIISEventRec *EventRec, bool AutoInc );
	bool	__fastcall AppendEventRec( CIISEventRec *EventRec);
	bool	__fastcall ApplyUpdatesEvent(void);
  void __fastcall DeleteOldEvents();

	//		Recording table
	uint32_t __fastcall GetRecordingRecCount();
//	uint32_t __fastcall GetCurrentRecordingRecNo();
	bool	__fastcall FindFirstRecording();
	bool	__fastcall FindLastRecording();
	bool	__fastcall LocateRecordingNo( uint32_t RecordingNo );
//	bool	__fastcall LocateRecordingRecNo( uint32_t RecNo );
	bool	__fastcall FindNextRecording();
	bool  __fastcall LocateLastRecNo( int32_t ZoneNo );

	bool	__fastcall SetRecordingRec( CIISRecordingRec *RecordingRec );
	bool 	__fastcall AppendRecordingRec( CIISRecordingRec *RecordingRec );
	bool	__fastcall GetRecordingRec( CIISRecordingRec *RecordingRec, bool AutoInc );
	bool	__fastcall ApplyUpdatesRecording(void);

	//		Value table
	uint32_t __fastcall GetMonitorValuesRecCount();
	uint32_t __fastcall GetMonitorValuesRecCountAfter(CIISMonitorValueRec *MValRec);
	bool	__fastcall FindFirstMonitorValue();
	bool	__fastcall FindLastMonitorValue();

	bool	__fastcall LocateMonitorValue( TDateTime DateTimeStamp, uint32_t RecNo, uint32_t SensorSerialNo , String ValueType );
	bool	__fastcall FindNextMonitorValue();
	bool	__fastcall GetMonitorValueRec( CIISMonitorValueRec *MValRec, bool AutoInc );
	bool	__fastcall AppendMonitorValueRec( CIISMonitorValueRec *MValRec);
	 bool __fastcall AppendMonitorValueRec_NoTimeAdj( CIISMonitorValueRec *MValRec);

	bool	__fastcall ApplyUpdatesMonitorValue(void);

	//		LPR table
	uint32_t __fastcall GetLPRValuesRecCount(void);
	uint32_t __fastcall GetLPRLocalValuesRecCount(void);
	uint32_t __fastcall GetLPRValuesRecCountAfter(CIISLPRValueRec *LPRValRec);
	bool	__fastcall FindFirstLPRValue(void);
	bool	__fastcall FindFirstLPRLocalValue(void);
	bool	__fastcall FindLastLPRValue(void);
	bool	__fastcall LocateLPRValue( TDateTime DateTimeStamp );
	bool	__fastcall FindNextLPRValue(void);
	bool	__fastcall GetLPRValueRec( CIISLPRValueRec *LPRValRec, bool AutoInc );
	bool	__fastcall AppendLPRValueRec( CIISLPRValueRec *LPRValRec);
	bool	__fastcall ApplyUpdatesLPRValue(void);

	void __fastcall SetLPRQuery(TDateTime RecStart, uint32_t RecNo,int32_t SensorSerialNo,String ValueType);

	//		Values calc table
	uint32_t __fastcall GetCalcValuesRecCount();
	uint32_t __fastcall GetCalcValuesRecCountAfter(CIISCalcValueRec *MValRec);
	bool	__fastcall FindFirstCalcValue();
	bool	__fastcall FindLastCalcValue();


		bool	__fastcall LocateCalcValue( TDateTime DateTimeStamp, uint32_t RecNo,
										uint32_t SensorSerialNo , String ValueType );

/*
	bool	__fastcall LocateCalcValue( TDateTime DateTimeStamp );
*/

	bool	__fastcall FindNextCalcValue();
	bool	__fastcall GetCalcValueRec( CIISCalcValueRec *MValRec, bool AutoInc );
	bool	__fastcall AppendCalcValueRec( CIISCalcValueRec *CalcValRec);
	bool	__fastcall AppendCalcValueRec_NoTimeAdj( CIISCalcValueRec *CalcValRec);
	bool	__fastcall ApplyUpdatesCalcValue(void);



	//		Decay table
	uint32_t __fastcall GetDecayValuesRecCount();
	uint32_t __fastcall GetDecayValuesRecCountAfter(CIISDecayValueRec *DecValRec);
	bool	__fastcall FindFirstDecayValue();
	bool	__fastcall FindLastDecayValue();
	bool	__fastcall LocateDecayValue( TDateTime DateTimeStamp, uint32_t RecNo, uint32_t SensorSerialNo, String ValueType );
	bool	__fastcall FindNextDecayValue();
	bool	__fastcall GetDecayValueRec( CIISDecayValueRec *DecValRec, bool AutoInc );
	bool 	__fastcall AppendDecayValueRec( CIISDecayValueRec *DecValRec );
	bool 	__fastcall AppendDecayValueRec_NoTimeAdj( CIISDecayValueRec *DecValRec );
	bool	__fastcall ApplyUpdatesDecayValue(void);

	//		Values misc table
	uint32_t __fastcall GetMiscValuesRecCount();
	uint32_t __fastcall GetMiscValuesRecCountAfter(CIISMiscValueRec *MValRec);
	bool	__fastcall FindFirstMiscValue();
	bool	__fastcall FindLastMiscValue();

	bool	__fastcall LocateMiscValue( TDateTime DateTimeStamp, uint32_t SensorSerialNo , String ValueType );
	bool	__fastcall FindNextMiscValue();
	bool	__fastcall GetMiscValueRec( CIISMiscValueRec *MValRec, bool AutoInc );
	bool	__fastcall AppendMiscValueRec( CIISMiscValueRec *MValRec);
	bool	__fastcall ApplyUpdatesMiscValue(int32_t ValuesMiscSize);
	uint32_t __fastcall PeekMiscValuesRecCount();
	void __fastcall DeleteOldMiscValues();
	void	__fastcall ClearSensorMiscValues(uint32_t SensorSerialNo);




	TDateTime __fastcall DateTimeNoMilliSecs(TDateTime DateTime,int32_t *Fraction);
	TDateTime __fastcall AssembleDateTime(TDateTime DateTime,int32_t Fraction);

    void __fastcall KeepDB(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TCIISDBModule *CIISDBModule;
//---------------------------------------------------------------------------
#endif
