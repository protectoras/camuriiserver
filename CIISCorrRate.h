//---------------------------------------------------------------------------

#ifndef CIISCorrRateH
#define CIISCorrRateH

#include "CIISCommon.h"
#include "CIISDB.h"
#include "DebugWinU.h"
//---------------------------------------------------------------------------

const int32_t SternGearyConst = 26;


class TCIISCorrRate : public TObject
{
private:
  TCIISDBModule *DB;
  TDebugWin *DW;

  TList *LPRValList, *T1, *T2;

  int32_t FResultQuality;
  double FCorrRate;
  double FICorr, FISigma;

  void __fastcall Test_AddRec( TDateTime DTS, int32_t RecNo, int32_t SerNo, double ValV, double ValI );
  void __fastcall Debug(String Message);

protected:

public:
  __fastcall TCIISCorrRate( TCIISDBModule *SetDB, TDebugWin *SetDebugWin );
  __fastcall ~TCIISCorrRate();

  int32_t __fastcall CalcCorrRate( int32_t Recording, int32_t Sensor, String ValuType );
//  int32_t __fastcall Test_GenData(  int32_t Sensor, int32_t ZoneNo );


__published:
  __property double CorrRate = { read=FCorrRate };
  __property double ICorr = { read=FICorr };
  __property double ISigma = { read=FISigma };

};

#endif
