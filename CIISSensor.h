//---------------------------------------------------------------------------

#ifndef CIISSensorH
#define CIISSensorH
//---------------------------------------------------------------------------
#include "CIISObj.h"
#include "CIISNodeIni.h"
#include "CIISCommon.h"
#include "CIISCorrRate.h"

class TCIISSensor : public TCIISObj
{
private:
  //CIISClientInt
  bool __fastcall ThisRec( TCamurPacket *P );
  void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
  void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt

  void __fastcall SensorIni( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_SensorIni();
  void __fastcall SensorIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  void __fastcall SensorIniVersion( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorLPRSample( TCIISBusPacket *BPIn );
	//virtual void __fastcall SensorLPRSample_U( TCIISBusPacket *BPIn );
	//virtual void __fastcall SensorLPRSample_I( TCIISBusPacket *BPIn );
  void __fastcall SensorLPREnd( TCIISBusPacket *BPIn );

  int32_t __fastcall GetZoneNo() { return SensorRec->ZoneNo; }
  int32_t __fastcall GetSerialNo() { return SensorRec->SensorSerialNo; }
  int32_t __fastcall GetCanAdr() { return SensorRec->SensorCanAdr; }
  int32_t __fastcall GetZoneCanAdr() { return SensorRec->ZoneCanAdr; }
  int32_t __fastcall GetSensorStatus() { return SensorRec->SensorStatus; }
  int32_t __fastcall GetSensorWarmUp() { return SensorRec->SensorWarmUp; }
  CIISNodeCapability __fastcall GetSensorType() { return (CIISNodeCapability)SensorRec->SensorType; }
  int32_t __fastcall GetSensorVerMajor() { return SensorRec->SensorVerMajor; }
  int32_t __fastcall GetSensorVerMinor() { return SensorRec->SensorVerMinor; }
  int32_t __fastcall GetSensorChannelCount() { return SensorRec->SensorChannelCount; }
  bool __fastcall GetLPREnd() { return SensorRec->RequestStatus == 0; }

	bool __fastcall GetAlarmStatus() { return ( SensorRec->SensorAlarmStatus == 1 ) ||
																						( SensorRec->SensorAlarmStatus2 == 1  ) ||
																						( SensorRec->SensorAlarmStatus3 == 1  ) ||
																						( SensorRec->SensorAlarmStatus4 == 1  ) ||
																						( SensorRec->CANAlarmStatus == 1 ); }

  bool __fastcall GetSensorTemp() { return SensorRec->SensorTemp; }
	bool __fastcall GetUpdateLastValueRunning() { return UpdateLastValueRunning; }
  void _fastcall SetZoneCanAdr( int32_t ZoneCanAdr );
  virtual void __fastcall SetDefaultValuesSubType();



  String __fastcall GetSensorName() { return SensorRec->SensorName; }

protected:
  //CIISClientInt
  CIISSensorRec *SensorRec;

  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt
  //TCIISBIChannel *CIISBICh;
  int32_t IniRetrys;
  bool SensorIniRunning, UpdateLastValueRunning, SensorIniStateRunning;
  NodeIniState NIState;
  int32_t NISState;
  int32_t T, TNextEvent;
	int32_t T2, T2NextEvent, T2State;
  int32_t TNIS, TNISNextEvent;


  int32_t OnSlowClockTick;
  bool NodeDetected;
  bool CapabilityRecived;
  bool CalibValuesRecived;
  bool CalibR1Recived, CalibR2Recived, CalibR3Recived, CalibR4Recived;
  bool CalibR5Recived, CalibR6Recived, CalibR7Recived, CalibR8Recived;
  bool CalibR9Recived, CalibR10Recived, CalibR11Recived, CalibR12Recived;
  bool CalibR13Recived, CalibR14Recived, CalibR15Recived;
  bool BitValueRecived;
  int32_t  BitValueRequested;
  bool VerRecived;

  int32_t FIniErrorCount;
  int32_t LastStoredTag;
  int32_t RequestShutdownTag, ExpectedShutdownTag;
	bool FIniDecaySample;
	bool FRunDistDecay;
  TDateTime IniDecayTime;
  double RMeasureFreq;

  int16_t ADVal1, ADVal2;
  uint16_t UADVal1, UADVal2;
	double AnVal1, AnVal2, AnVal1_Scl, AnVal2_Scl, LowLev1, HighLev1, LowLev2, HighLev2;
	//double LPRVal_U;
  double VRes, IRes, ZRALast, ZRACurrent;
  int32_t CTMode, CTCh, CTRange;

  bool SensorLastValueRequest, SensorLastValueExtRequest, SensorLastValueTempRequest;
  TDateTime SampleTimeStamp;

  virtual void __fastcall SM_UpdateLastValue();
  virtual void __fastcall SM_SensorIniState();
	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
	virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );
  virtual bool __fastcall CheckAlarm();

	void __fastcall ClearLastValues(void);

  bool LogCanDupTag, LogCanMissingTag, MissingTagDetected;
	int32_t DupCount;


  //uCtrl

	bool FuCtrlZone;
	int32_t FuCtrlNodeIndex;


public:
  __fastcall TCIISSensor(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor();
  void __fastcall ResetCIISBus();
  bool __fastcall NodeIniReady();
  void __fastcall SetDefaultValues();
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();
  void __fastcall CheckSampleRecived( int32_t RequestedTag );
	void __fastcall ResetLastStoredTag() { LastStoredTag = 0; }
  void __fastcall ResetCANAlarm();

  virtual void __fastcall SensorWarmUp( bool On );
	virtual bool __fastcall SensorLPRIni();
	virtual void __fastcall SensorIniState();
	virtual void __fastcall LPRSelectRange( int32_t Range );

  virtual void __fastcall CTSelectMode( int32_t Mode );
  virtual void __fastcall CTSelectRange( int32_t Range );
  virtual void __fastcall CTSelectRFreq( int32_t Freq );
  virtual void __fastcall CTSelectCh( int32_t Ch );
  virtual void __fastcall CTDACalib();
  virtual void __fastcall CTOffsetAdj();
  virtual void __fastcall HUMRequestSample( int32_t RequestTag );
  virtual void __fastcall CTRequestSample(int32_t RequestTag );
	virtual void __fastcall CTRequestTemp( int32_t RequestTag );
  virtual void __fastcall CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
	virtual double __fastcall CTZRAChange();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );

	virtual bool __fastcall CTUpdatingTemp();

__published:

  __property int32_t ZoneNo = { read = GetZoneNo };
  __property int32_t SerialNo = { read = GetSerialNo };
  __property int32_t CanAdr = { read = GetCanAdr };
	__property int32_t ZoneCanAdr = { read = GetZoneCanAdr, write = SetZoneCanAdr };
  __property bool IniRunning = { read = SensorIniRunning, write = SensorIniRunning };
  __property int32_t SensorStatus = { read = GetSensorStatus };
  __property int32_t SensorWarmUpTime = { read = GetSensorWarmUp };
  __property CIISNodeCapability SensorType = {read = GetSensorType };
  __property int32_t VerMajor = { read = GetSensorVerMajor };
  __property int32_t VerMinor = { read = GetSensorVerMinor };
  __property bool LPREnd = { read = GetLPREnd };
  __property bool AlarmStatus = { read = GetAlarmStatus };
  __property bool UpdatingLastValue = { read = GetUpdateLastValueRunning };
//  __property TCIISBIChannel* BIChannel = { read = CIISBICh, write = CIISBICh };
  __property int32_t IniErrorCount = { read = FIniErrorCount, write = FIniErrorCount };
	__property bool IniDecaySample = { read = FIniDecaySample, write = FIniDecaySample };
 	__property bool RunDistDecay = { read = FRunDistDecay, write = FRunDistDecay };
	__property bool SensorTemp = { read = GetSensorTemp };
  __property String SensorName = { read = GetSensorName };
  __property int32_t SensorChannelCount = { read = GetSensorChannelCount };

	__property bool uCtrlZone = { read = FuCtrlZone, write = FuCtrlZone};
	__property int32_t uCtrlNodeIndex = { read = FuCtrlNodeIndex, write = FuCtrlNodeIndex };


};

//---------------------------------------------------------------------------

/* Sensors
	CIISNodeCapability

	Camur_II_LPR	= 10,
	Camur_II_IO		= 12,
	Camur_II_Alarm = 13,
	Camur_II_P		= 14,
	Camur_II_HUM	= 15,
	Camur_II_HiRes	= 16,
	Camur_II_R		= 17,
	Camur_II_ZRA	= 18,
	Camur_II_LPRExt	= 20,
	Camur_II_CT		= 21,
	Camur_II_MRE	= 22,
	Camur_II_CW		= 23,
	Camur_II_P4		= 24
	Camur_II_Ladder	= 25
	Camur_II_CrackWatch = 26
	Camur_II_PT = 27
	Camur_II_Wenner = 28
*/
//---------------------------------------------------------------------------

class TCIISSensor_LPR : public TCIISSensor
{
private:
	virtual void __fastcall SM_SensorIni();
	virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
	virtual void __fastcall SetDefaultValuesSubType();
	virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorLPRSample( TCIISBusPacket *BPIn );
	//virtual void __fastcall SensorLPRSample_U( TCIISBusPacket *BPIn );
	//virtual void __fastcall SensorLPRSample_I( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall SM_UpdateLastValue();


protected:


public:
	__fastcall TCIISSensor_LPR( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_LPR();

	virtual bool __fastcall SensorLPRIni();
	virtual void __fastcall CTRequestTemp( int32_t RequestTag );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall LPRSelectRange( int32_t Range );

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_IO : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual bool __fastcall CheckAlarm();
	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
	virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }

protected:

public:
  __fastcall TCIISSensor_IO(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_IO();

__published:
};
//---------------------------------------------------------------------------

class TCIISSensor_P : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );

protected:


public:
  __fastcall TCIISSensor_P(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_P();

	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:
};
//---------------------------------------------------------------------------

class TCIISSensor_P4 : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual bool __fastcall CheckAlarm();


protected:


public:
  __fastcall TCIISSensor_P4(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_P4();

	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
  virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_LPRExt : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorLPRSample( TCIISBusPacket *BPIn );


protected:


public:
  __fastcall TCIISSensor_LPRExt(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_LPRExt();

  virtual bool __fastcall SensorLPRIni();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_HUM : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorWarmUp( bool On );
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual bool __fastcall CheckAlarm();

protected:


public:
  __fastcall TCIISSensor_HUM(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_HUM();

  virtual void __fastcall HUMRequestSample( int32_t RequestTag );

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_HiRes : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );

protected:


public:
  __fastcall TCIISSensor_HiRes(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_HiRes();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_R : public TCIISSensor
{
private:
	int32_t TR, TRNextEvent, TRState;
	int32_t FRequestTag;
	bool CTRequestTempRunning;

	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
  virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall CTSelectRFreq( int32_t Freq );
	virtual void __fastcall SM_UpdateLastValue();

	void __fastcall SM_CTRequestTemp();


	virtual void __fastcall CTSelectMode( int32_t Mode );
	virtual void __fastcall CTDACalib();
	virtual void __fastcall CTOffsetAdj();

protected:


public:
	__fastcall TCIISSensor_R(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_R();

	virtual void __fastcall CTRequestTemp( int32_t RequestTag );
	virtual bool __fastcall CTUpdatingTemp();
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_Wenner : public TCIISSensor
{
private:
	int32_t TR, TRNextEvent, TRState;
	int32_t FRequestTag;


	virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall CTSelectRFreq( int32_t Freq );
	virtual void __fastcall SM_UpdateLastValue();

	virtual void __fastcall CTSelectMode( int32_t Mode );
	virtual void __fastcall CTDACalib();
	virtual void __fastcall CTOffsetAdj();

protected:


public:
	__fastcall TCIISSensor_Wenner(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_Wenner();

	virtual void __fastcall UpdateLastValues();

__published:
};
//---------------------------------------------------------------------------

class TCIISSensor_ZRA : public TCIISSensor
{
private:
	virtual void __fastcall SM_SensorIni();
	void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
	virtual void __fastcall SetDefaultValuesSubType();
	virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorWarmUp( bool On );
	virtual void __fastcall SensorIniState();
	virtual void __fastcall SM_SensorIniState();
	virtual void __fastcall SM_UpdateLastValue();

protected:


public:
	__fastcall TCIISSensor_ZRA(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_ZRA();

	virtual void __fastcall CTRequestTemp( int32_t RequestTag );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_CrackWatch : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );

protected:

public:
	__fastcall TCIISSensor_CrackWatch(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_CrackWatch();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_PT : public TCIISSensor
{
private:
	virtual void __fastcall SM_SensorIni();
	virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
	virtual void __fastcall SetDefaultValuesSubType();
	virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );

	virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
	__fastcall TCIISSensor_PT(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
	__fastcall ~TCIISSensor_PT();


	virtual void __fastcall CTRequestTemp( int32_t RequestTag );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();
  virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_CT : public TCIISSensor
{
private:
  virtual void __fastcall SM_SensorIni();
  virtual void __fastcall SensorIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorLPRSample( TCIISBusPacket *BPIn );
  

protected:


public:
  __fastcall TCIISSensor_CT(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_CT();

  virtual bool __fastcall SensorLPRIni();

	virtual void __fastcall CTSelectMode( int32_t Mode );
	virtual void __fastcall CTSelectRange( int32_t Range );
	virtual void __fastcall CTSelectRFreq( int32_t Freq );
	virtual void __fastcall CTSelectCh( int32_t Ch );
	virtual void __fastcall CTDACalib();
	virtual void __fastcall CTOffsetAdj();
  virtual void __fastcall CTRequestSample( int32_t RequestTag );
  virtual void __fastcall CTRequestTemp( int32_t RequestTag );
  virtual void __fastcall CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
  virtual double __fastcall CTZRAChange();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_MRE : public TCIISSensor_CT
{
private:

  int32_t T_MRE, TNextEvent_MRE, State_MRE, MRESelectedCh;

	virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_UpdateLastValue();
  

protected:


public:
  __fastcall TCIISSensor_MRE(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_MRE();

	virtual void __fastcall UpdateLastValues();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_CW : public TCIISSensor_CT
{
private:

	int32_t T_CW, TNextEvent_CW, State_CW, CWSelectedCh;

  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_UpdateLastValue();

protected:


public:
  __fastcall TCIISSensor_CW(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_CW();

  virtual bool __fastcall SensorLPRIni();
  virtual void __fastcall CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
  virtual void __fastcall UpdateLastValues();

__published:
};

//---------------------------------------------------------------------------

class TCIISSensor_Ladder : public TCIISSensor_CT
{
private:

  int32_t T_Ldr, TNextEvent_Ldr, State_Ldr, LdrSelectedCh;

  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SensorSample( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall SensorSampleTemp( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_UpdateLastValue();

protected:


public:
  __fastcall TCIISSensor_Ladder(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent );
  __fastcall ~TCIISSensor_Ladder();

  virtual bool __fastcall SensorLPRIni();
  virtual void __fastcall CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
  virtual void __fastcall UpdateLastValues();

__published:
};

#endif
