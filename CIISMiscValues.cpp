//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISMiscValues.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISMiscValues::TCIISMiscValues(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISMiscValueRec *SetMiscValRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISValuesMisc;
  MiscValRec = SetMiscValRec;


}

__fastcall TCIISMiscValues::~TCIISMiscValues()
{
  delete MiscValRec;
}

void __fastcall TCIISMiscValues::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "MiscValues: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISMiscValues::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "MiscValues: ParseCommand_End" );
	#endif
}


bool __fastcall TCIISMiscValues::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( MiscValRec->DateTimeStamp ) &&
		 P->GetArg(2) == IntToStr( MiscValRec->SensorSerialNo ) &&
		 P->GetArg(3) == MiscValRec->ValueType;
}

void __fastcall TCIISMiscValues::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
	if( P->GetArg( 200 ) == "GetFirstToEnd" )
	{
	  if( DB->FindFirstMiscValue() )P->CommandMode = CIISRecAdd;
	  else
	  {
		O->MessageCode = CIISMsg_RecNotFound;
		P->CommandMode = CIISCmdReady;
	  }
	}
	else if( P->GetArg( 200 ) == "GetNextToEnd" )
	{
	  if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) )
	  {
		if( DB->LocateMiscValue( CIISStrToDateTime( P->GetArg(1) ), StrToInt( P->GetArg(2) ),  P->GetArg(3) ))
		{
		  if( DB->FindNextMiscValue() ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_Ok;
			P->CommandMode = CIISCmdReady;
		  }
		}
		else if( DB->FindFirstMiscValue() )
		{
		  DB->GetMiscValueRec( MiscValRec, false );
		  if( CIISStrToDateTime( P->GetArg(1) ) < MiscValRec->DateTimeStamp ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_RecNotFound;
			P->CommandMode = CIISCmdReady;
		  }
        }
		else
		{
		  O->MessageCode = CIISMsg_RecNotFound;
		  P->CommandMode = CIISCmdReady;
		}
	  }
	  else if( P->ArgInc(99) )
	  {
		O->MessageCode = CIISMsg_NoLongerSupported;
	  }
	}
	else if( P->GetArg( 200 ) == "GetLast" )
	{
	  if( DB->FindLastMiscValue() )P->CommandMode = CIISRecAdd;
	  else
	  {
		O->MessageCode = CIISMsg_RecNotFound;
		P->CommandMode = CIISCmdReady;
	  }
	}
	else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
  }

  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {

	#if DebugCIIDataTable == 1
	Debug( "Dataread started"  );
	#endif

	do
	{
	  MoreRecords = DB->GetMiscValueRec( MiscValRec, true );
	  O->MessageData = O->MessageData +
	  "1=" + CIISDateTimeToStr( MiscValRec->DateTimeStamp ) + "\r\n" +
	  "2=" + IntToStr( MiscValRec->SensorSerialNo ) + "\r\n" +
	  "3=" + MiscValRec->ValueType + "\r\n" +
	  "4=" + MiscValRec->ValueUnit + "\r\n" +
	  "5=" + CIISFloatToStr( MiscValRec->Value ) + "\r\n";
	} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );

	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;

	#if DebugCIIDataTable == 1
	Debug( "Dataread ready"  );
	#endif	

  }
  else if( P->CommandMode == CIISRecCount )
  {
	if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) )
	{
	  MiscValRec->DateTimeStamp = CIISStrToDateTime( P->GetArg(1) );
	  MiscValRec->SensorSerialNo = StrToInt( P->GetArg(2) );
	  MiscValRec->ValueType = P->GetArg(3);

	  O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetMiscValuesRecCountAfter( MiscValRec ) ) + "\r\n";
	}
	else
	{
	  O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetMiscValuesRecCount() ) + "\r\n";
	}

	
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISMiscValues::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISMiscValues::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISMiscValues::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}