//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISClientInterfaceLogger.h"
#include "CIISCommon.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

__fastcall TCIISClientInterfaceLogger::TCIISClientInterfaceLogger(  TCIISProject *SetPrj,
																	TDebugWin * SetDebugWin,
																	int32_t SetPortNoLocal,
																	int32_t SetPortNoRemote )
{
  Prj = SetPrj;
  DW = SetDebugWin;
  PortNoLocal = SetPortNoLocal;
  PortNoRemote = SetPortNoRemote;

	LSocket = new TCIIServerSocket( &GetLocalMessage, PortNoLocal, DW  );
  RSocket = new TCIIServerSocket( &GetRemoteMessage, PortNoRemote, DW  );

  #if DebugClientInt == 1
  Debug("CIISClientInterfaceLogger Created");
  #endif
}

__fastcall TCIISClientInterfaceLogger::~TCIISClientInterfaceLogger()
{
  delete LSocket;
  delete RSocket;
}

TCIIServerSocket* __fastcall TCIISClientInterfaceLogger::GetLSocket()
{
	return LSocket;
}

TCIIServerSocket* __fastcall TCIISClientInterfaceLogger::GetRSocket()
{
	return RSocket;
}

void __fastcall TCIISClientInterfaceLogger::GetRemoteMessage()
{

	#if DebugMsg == 1
	Debug( "ClientInt: GetRemoteMessage" );
	#endif

	while( ParseCommands() );
}

void __fastcall TCIISClientInterfaceLogger::GetLocalMessage()
{
	#if DebugMsg == 1
	Debug( "ClientInt: GetLocalMessage" );
	#endif

	while( ParseCommands() );
}

bool __fastcall TCIISClientInterfaceLogger::ParseCommands()
{
	TCamurPacket  *p, *o;

	#if DebugMsg == 1
	Debug( "ClientInt: ParseCommands" );
	#endif

  p = LSocket->GetMessage();
  if( p == NULL ) p = RSocket->GetMessage();

	if( p != NULL )
	{
		#if DebugClientInt == 1
		Debug( "Parsing MSG Number  : "+IntToStr( p->MessageNumber ) + ": " + p->MessageData );
		#endif

		// Create respond package

		o = new TCamurPacket;
		o->MessageType = CIISResponse;
		o->PortNo = p->PortNo;
		o->MessageNumber = p->MessageNumber;
		o->MessageCode = CIISUnDefErr;
		o->MessageCommand = p->MessageCommand;
		o->MessageTable = p->MessageTable;
		o->MessageData = "";

			// From ver 3 Commands with undef table is handled by CIISProject object.

		if( p->MessageTable == CIISUndefinedTab ) p->MessageTable = CIISProject;

		// Check if remot or local message
		if( p->PortNo == PortNoLocal ) CurrSocket = LSocket;
		else CurrSocket = RSocket;

		// UserLevel depending on input socket
		p->UserLevel = CurrSocket->UserLevel;

		// CommadnMode used in CIISObj message parsing
		p->CommandMode = CIISUndef;
		Prj->ParseCommand( p, o );


		// If login command update incoming socket user level
		if( p->MessageCommand == CIISLogin )
		{
			if( o->UserLevel == ULNone ) CurrSocket->ClientLoggedIn = false;
			else CurrSocket->ClientLoggedIn = true;

			CurrSocket->UserLevel = o->UserLevel;
		}
		else if( p->MessageCommand == CIISLogout )
		{
			CurrSocket->UserLevel = ULNone;
			CurrSocket->ClientLoggedIn = false;
		}

		if( o->PortNo == PortNoLocal )
		{
			LSocket->PutMessage( o );
			LSocket->SendMessage();
		}
		else
		{
			RSocket->PutMessage( o );
			RSocket->SendMessage();
		}

		delete p;

		return true;
  }
	else return false;  // No more messages in queue
}

void __fastcall TCIISClientInterfaceLogger::SetDebugWin(TDebugWin * SetDebugWin)
{
  DW = SetDebugWin;
  LSocket->SetDebugWin( SetDebugWin );
  RSocket->SetDebugWin( SetDebugWin );
}

void __fastcall TCIISClientInterfaceLogger::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}


bool __fastcall TCIISClientInterfaceLogger::GetLocalClientConnected()
{
  return LSocket->ClientConnected;
}

bool __fastcall TCIISClientInterfaceLogger::GetRemoteClientConnected()
{
  return RSocket->ClientConnected;
}

bool __fastcall TCIISClientInterfaceLogger::GetRemoteClientAktiv()
{
	return RSocket->ClientAktiv;
}

void __fastcall TCIISClientInterfaceLogger::DisconnectRemoteClient()
{
  delete RSocket;
	RSocket = new TCIIServerSocket( &GetRemoteMessage, PortNoRemote, DW  );

	//RSocket->DisconnectClient();
}

