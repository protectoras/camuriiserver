//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISRecordings.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISRecordings::TCIISRecordings(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISRecordingRec *SetRecordingRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISRecordings;
  RecordingRec = SetRecordingRec;
  
  CIISObjType = CIISRecordings;
}

__fastcall TCIISRecordings::~TCIISRecordings()
{
  delete RecordingRec;
}

void __fastcall TCIISRecordings::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "Recordings: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;	
	break;

	case CIISAppend:
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISRecordings::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Recordings: ParseCommand_Begin" );
	#endif
}

bool __fastcall TCIISRecordings::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == IntToStr( RecordingRec->RecNo );
}

void __fastcall TCIISRecordings::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
	if( P->GetArg( 200 ) == "GetFirstToEnd" )
    {
	  if( DB->FindFirstRecording() )P->CommandMode = CIISRecAdd;
	  else
	  {
		O->MessageCode = CIISMsg_RecNotFound;
		P->CommandMode = CIISCmdReady;
	  }
	}
	else if( P->GetArg( 200 ) == "GetNextToEnd" )
	{
	  if( P->ArgInc(1))
	  {
		if( DB->LocateRecordingNo( StrToInt( P->GetArg(1) ) ) )
		{
		  if( DB->FindNextRecording() ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_Ok;
			P->CommandMode = CIISCmdReady;
		  }
		}
		else
		{
		  O->MessageCode = CIISMsg_RecNotFound;
		  P->CommandMode = CIISCmdReady;
		}
	  }
	  else if( P->ArgInc(99) )
	  {
		O->MessageCode = CIISMsg_NoLongerSupported;
	  }
	}
	else if( P->GetArg( 200 ) == "GetRecStop" && P->ArgInc(1) )
	{
	  if( DB->LocateRecordingNo( StrToInt( P->GetArg(1) ) ) ) P->CommandMode = CIISRecordingStop;
	  else O->MessageCode = CIISMsg_RecNotFound;
    }
	else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
  }

  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {
		do
		{
			MoreRecords = DB->GetRecordingRec( RecordingRec, true );
			O->MessageData = O->MessageData +
			"1=" + IntToStr( RecordingRec->RecNo ) + "\r\n" +
			"2=" + CIISDateTimeToStr( RecordingRec->RecStart ) + "\r\n" +
			"3=" + CIISDateTimeToStr( RecordingRec->RecStop ) + "\r\n" +
			"4=" + IntToStr( RecordingRec->RecType ) + "\r\n" +
			"5=" + IntToStr( RecordingRec->DecaySampInterval ) + "\r\n" +
			"6=" + IntToStr( RecordingRec->DecayDuration ) + "\r\n" +
			"7=" + IntToStr( RecordingRec->LPRRange ) + "\r\n" +
			"8=" + IntToStr( RecordingRec->LPRStep ) + "\r\n" +
			"9=" + IntToStr( RecordingRec->LPRDelay1 ) + "\r\n" +
			"10=" + IntToStr( RecordingRec->LPRDelay2 ) + "\r\n" +
			"11=" + IntToStr( RecordingRec->SampInterval ) + "\r\n" +
			"12=" + IntToStr( RecordingRec->LPRMode ) + "\r\n" +
			"13=" + IntToStr( RecordingRec->DecayDelay ) + "\r\n" +
			"14=" + IntToStr( RecordingRec->DecayDuration2 ) + "\r\n" +
			"15=" + IntToStr( RecordingRec->DecaySampInterval2 ) + "\r\n" +
			"50=" + IntToStr( RecordingRec->ZoneNo ) + "\r\n";
		} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecordingStop )
	{
		DB->GetRecordingRec( RecordingRec, false );
		O->MessageData = "1=" + CIISDateTimeToStr( RecordingRec->RecStop );
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
		O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetRecordingRecCount() ) + "\r\n";;
		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISRecordings::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISRecordings::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISRecordings::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}
