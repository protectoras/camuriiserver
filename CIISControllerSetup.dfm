object FrmControllerSetup: TFrmControllerSetup
  Left = 0
  Top = 0
  Caption = 'CIIS Controller'
  ClientHeight = 337
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object LSerialNo: TLabel
    Left = 120
    Top = 24
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LBIMode: TLabel
    Left = 120
    Top = 53
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LBIVer: TLabel
    Left = 264
    Top = 24
    Width = 15
    Height = 13
    Caption = '???'
  end
  object BGetSerial: TButton
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Get BI Serial'
    TabOrder = 0
    OnClick = BGetSerialClick
  end
  object BGetBIMode: TButton
    Left = 16
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Get BI Mode'
    TabOrder = 1
    OnClick = BGetBIModeClick
  end
  object BGetBIVer: TButton
    Left = 168
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Get BI Version'
    TabOrder = 2
    OnClick = BGetBIVerClick
  end
end
