//---------------------------------------------------------------------------

#pragma hdrstop

#include "CIISCalcValues.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
 __fastcall TCIISCalcValues::TCIISCalcValues(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISCalcValueRec *SetCalcValRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
	CIISObjType = CIISValuesCalc;
  CalcValRec = SetCalcValRec;
}

__fastcall TCIISCalcValues::~TCIISCalcValues()
{
  delete CalcValRec;
}

void __fastcall TCIISCalcValues::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "CVValues: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISCalcValues::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "CVValues: ParseCommand_End" );
	#endif
}


bool __fastcall TCIISCalcValues::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( CalcValRec->DateTimeStamp );
}

void __fastcall TCIISCalcValues::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
	{
		if( P->GetArg( 200 ) == "GetFirstToEnd" )
			{
			if( DB->FindFirstCalcValue() )P->CommandMode = CIISRecAdd;
			else
			{
				O->MessageCode = CIISMsg_RecNotFound;
				P->CommandMode = CIISCmdReady;
			}
		}
		else if( P->GetArg( 200 ) == "GetNextToEnd" )
		{
			if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) && P->ArgInc(4))
			{
				if( DB->LocateCalcValue( CIISStrToDateTime( P->GetArg(1) ), StrToInt( P->GetArg(2) ), StrToInt( P->GetArg(3) ), P->GetArg(4) ))
				{
					if( DB->FindNextCalcValue() ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_Ok;
						P->CommandMode = CIISCmdReady;
					}
				}
				else if( DB->FindFirstCalcValue() )
				{
					DB->GetCalcValueRec( CalcValRec, false );
					if( CIISStrToDateTime( P->GetArg(1) ) < CalcValRec->DateTimeStamp ) P->CommandMode = CIISRecAdd;
					else
					{
						O->MessageCode = CIISMsg_RecNotFound;
						P->CommandMode = CIISCmdReady;
					}
				}
				else
				{
					O->MessageCode = CIISMsg_RecNotFound;
					P->CommandMode = CIISCmdReady;
				}
			}
			else if( P->ArgInc(99) )
			{
				O->MessageCode = CIISMsg_NoLongerSupported;
			}
		}
		else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
	}

  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {
		do
		{
			MoreRecords = DB->GetCalcValueRec( CalcValRec, true );
			O->MessageData = O->MessageData +
			"1=" + CIISDateTimeToStr(CalcValRec->DateTimeStamp) + "\r\n" +
			"2=" + IntToStr( CalcValRec->RecNo ) + "\r\n" +
			"3=" + IntToStr( CalcValRec->SensorSerialNo ) + "\r\n" +
			"4=" + CalcValRec->ValueType + "\r\n" +
			"5=" + CalcValRec->ValueUnit + "\r\n" +
			"6=" + CIISFloatToStr( CalcValRec->Value ) + "\r\n" +
			"7=" + IntToStr( CalcValRec->Code ) + "\r\n";
		} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );

		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
  }
	else if( P->CommandMode == CIISRecCount )
  {
		if( P->ArgInc(1) )
		{
			 CalcValRec->DateTimeStamp = CIISStrToDateTime( P->GetArg(1) );
			 O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetCalcValuesRecCountAfter( CalcValRec ) ) + "\r\n";
		}
		else
		{
			O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetCalcValuesRecCount() ) + "\r\n";
		}

		O->MessageCode = CIISMsg_Ok;
		P->CommandMode = CIISCmdReady;
	}
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISCalcValues::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISCalcValues::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISCalcValues::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}