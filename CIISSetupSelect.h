//---------------------------------------------------------------------------

#ifndef CIISSetupSelectH
#define CIISSetupSelectH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "CIISProject.h"
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------
class TFrmCalibSelect : public TForm
{
__published:	// IDE-managed Components
	TGroupBox *GBController;
	TGroupBox *GBZone;
	TGroupBox *GBSensor;
	TGroupBox *GBPowerSupply;
	TComboBox *CBSelCtrl;
	TComboBox *CBSelZone;
	TComboBox *CBSelSensor;
	TComboBox *CBSelPS;
	TBitBtn *BBCtrlSetup;
	TBitBtn *BBZoneSetup;
	TBitBtn *BBSensorSetup;
	TBitBtn *BBPSSetup;
	void __fastcall CBSelZoneChange(TObject *Sender);
	void __fastcall CBSelCtrlChange(TObject *Sender);
	void __fastcall BBCtrlSetupClick(TObject *Sender);
	void __fastcall BBZoneSetupClick(TObject *Sender);
	void __fastcall BBSensorSetupClick(TObject *Sender);
	void __fastcall CBSelSensorChange(TObject *Sender);
private:	// User declarations

	TList *CtrlList;
	TList *ZoneList;

	TCIISProject *P_Prj;
	TCIISController *P_Ctrl;
	TCIISZone *P_Zone;
	TCIISSensor *P_Sensor;
	TCIISPowerSupply *P_PS;
public:		// User declarations
	__fastcall TFrmCalibSelect(TComponent* Owner, TCIISProject* Prj);
};
//---------------------------------------------------------------------------
extern PACKAGE TFrmCalibSelect *FrmCalibSelect;
//---------------------------------------------------------------------------
#endif
