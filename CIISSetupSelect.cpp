//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISSetupSelect.h"
#include "CIISControllerSetup.h"
#include "CIISZoneSetup.h"
#include "CIISCTx8Setup.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFrmCalibSelect *FrmCalibSelect;
//---------------------------------------------------------------------------
__fastcall TFrmCalibSelect::TFrmCalibSelect( TComponent* Owner, TCIISProject* Prj ): TForm(Owner)
{
	int CtrlNo, ZoneNo, SensorNo, PSNo;

	P_Prj = Prj;

	CtrlNo = 1;
	P_Ctrl = NULL;
	do
	{
		P_Ctrl = P_Prj->GetCtrl( CtrlNo );
		if( P_Ctrl != NULL )
		{
			CBSelCtrl->AddItem( P_Ctrl->CtrlName, P_Ctrl);
			CtrlNo++;
		}

	} while ( P_Ctrl != NULL );

	if( CBSelCtrl->Items->Count > 0 )
	{
		CBSelCtrl->ItemIndex = 0;

		P_Ctrl = (TCIISController*)CBSelCtrl->Items->Objects[0];

		ZoneNo = 1;
		P_Zone = NULL;
		do
		{
			P_Zone = P_Ctrl->GetZone( ZoneNo );
			if( P_Zone != NULL )
			{
				CBSelZone->AddItem( P_Zone->ZoneName, P_Zone);
				ZoneNo++;
			}

		} while ( P_Zone != NULL );


		if( CBSelZone->Items->Count > 0 )
		{
			CBSelZone->ItemIndex = 0;

			P_Zone = (TCIISZone*)CBSelZone->Items->Objects[0];

			SensorNo = 1;
			P_Sensor = NULL;
			do
			{
				P_Sensor = P_Zone->GetSensor( SensorNo );
				if( P_Sensor != NULL )
				{
					CBSelSensor->AddItem( P_Sensor->SensorName, P_Sensor);
					SensorNo++;
				}

			} while ( P_Sensor != NULL );
			if( CBSelSensor->Items->Count > 0 ) CBSelSensor->ItemIndex = 0;

			PSNo = 1;
			P_PS = NULL;
			do
			{
				P_PS = P_Zone->GetPS( PSNo );
				if( P_PS != NULL )
				{
					CBSelPS->AddItem( P_PS->PSName, P_PS);
					PSNo++;
				}

			} while ( P_PS != NULL );
			if( CBSelPS->Items->Count > 0 ) CBSelPS->ItemIndex = 0;
		}
	}



}
//---------------------------------------------------------------------------

void __fastcall TFrmCalibSelect::CBSelSensorChange(TObject *Sender)
{
	P_Sensor = (TCIISSensor*)CBSelSensor->Items->Objects[CBSelSensor->ItemIndex];
}

void __fastcall TFrmCalibSelect::CBSelZoneChange(TObject *Sender)
{
	int SensorNo, PSNo;

	CBSelSensor->Clear();
	CBSelPS->Clear();

	P_Zone = (TCIISZone*)CBSelZone->Items->Objects[CBSelZone->ItemIndex];

	SensorNo = 1;
	P_Sensor = NULL;
	do
	{
		P_Sensor = P_Zone->GetSensor( SensorNo );
		if( P_Sensor != NULL )
		{
			CBSelSensor->AddItem( P_Sensor->SensorName, P_Sensor);
			SensorNo++;
		}

	} while ( P_Sensor != NULL );
	if( CBSelSensor->Items->Count > 0 ) CBSelSensor->ItemIndex = 0;

	PSNo = 1;
	P_PS = NULL;
	do
	{
		P_PS = P_Zone->GetPS( PSNo );
		if( P_PS != NULL )
		{
			CBSelPS->AddItem( P_PS->PSName, P_PS);
			PSNo++;
		}

	} while ( P_PS != NULL );
	if( CBSelPS->Items->Count > 0 ) CBSelPS->ItemIndex = 0;

}
//---------------------------------------------------------------------------

void __fastcall TFrmCalibSelect::CBSelCtrlChange(TObject *Sender)
{
	int ZoneNo;

	CBSelSensor->Clear();
	CBSelPS->Clear();
	CBSelZone->Clear();

	P_Ctrl = (TCIISController*)CBSelCtrl->Items->Objects[CBSelCtrl->ItemIndex];

	ZoneNo = 1;
	P_Zone = NULL;
	do
	{
		P_Zone = P_Ctrl->GetZone( ZoneNo );
		if( P_Zone != NULL )
		{
			CBSelZone->AddItem( P_Zone->ZoneName, P_Zone);
			ZoneNo++;
		}

	} while ( P_Zone != NULL );


	if( CBSelZone->Items->Count > 0 )
	{
		CBSelZone->ItemIndex = 0;
		P_Zone = (TCIISZone*)CBSelZone->Items->Objects[0];

		CBSelZoneChange( this );

	}
}

//---------------------------------------------------------------------------


void __fastcall TFrmCalibSelect::BBCtrlSetupClick(TObject *Sender)
{
	TFrmControllerSetup* FrmCtrlSetup;

	FrmCtrlSetup = new TFrmControllerSetup( NULL, P_Ctrl );



	FrmCtrlSetup->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFrmCalibSelect::BBZoneSetupClick(TObject *Sender)
{
	TFrmZoneSetup *FrmZoneSetup;

	FrmZoneSetup = new TFrmZoneSetup( NULL, P_Zone );

	FrmZoneSetup->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFrmCalibSelect::BBSensorSetupClick(TObject *Sender)
{
	TFrmCTx8SetUp *FrmCTx8Setup;

	FrmCTx8Setup = new TFrmCTx8SetUp( NULL, P_Sensor );

	FrmCTx8Setup->Show();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

