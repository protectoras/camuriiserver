//---------------------------------------------------------------------------
// History
//
// Date			Comment											Sign
// 2015-02-08   Converted to Xe7, Renamed variable AlarmMoved
//				because of name-collision with event-code      	Bo H
//---------------------------------------------------------------------------

#pragma hdrstop

#include "CIISAlarm.h"
#include "CIISProject.h"
#include "CIISController.h"
#include "CIISBusInterface.h"
#include "math.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)



__fastcall TCIISAlarm::TCIISAlarm(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISAlarmRec *SetAlarmRec, TObject *SetCIISParent )
							:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{

	CIISObjType = CIISAlarm;

  P_Zone = (TCIISZone*)CIISParent;
	P_Ctrl = (TCIISController*)P_Zone->CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

  CIISBICh = NULL;
	AlarmRec = SetAlarmRec;

	UpdateLastValueRunning = false;
	NodeDetected = false;

	AlarmIniRunning = false;
	FIniErrorCount = 0;
	NIState = NI_ZoneAdr;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;

	 OnSlowClockTick = 5 * SetAlarmInterval;

  FIniErrorCount = 0;
  OutputTmpOff = false;


	LogCanDupTag = true;
	LogCanMissingTag = true;
  MissingTagDetected = false;
	DupCount = 5;

	#if DebugCIISStart == 1

	Debug( "Alarm created: " + IntToStr( AlarmRec->AlarmCanAdr ) + "/" + IntToStr( AlarmRec->AlarmSerialNo ));

	#endif
}

__fastcall TCIISAlarm::~TCIISAlarm()
{
  delete AlarmRec;
}

void __fastcall TCIISAlarm::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "Alarm: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	  {
		WriteRec( P, O );
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase )
	  {
		if( AlarmRec->ZoneNo == 1 &&  DB->DeleteAlarmRec( AlarmRec ) )
		{
			DB->ApplyUpdatesAlarm();
			P_Prj->Log( ClientCom, CIIEventCode_AlarmTabChanged, CIIEventLevel_High, "", 0, 0 );
			P->CommandMode = CIISDeleteAlarm;
		  P->CIISObj = this;
		  O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_DeleteError;
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISAlarm::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Alarm: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISAlarm::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == AlarmRec->AlarmSerialNo;
}

void __fastcall TCIISAlarm::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
		if( P->ArgInc( 200 ) )
		{
			if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
			else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetLastValFirstToEnd") P->CommandMode = CIISRecAddLastVal;
			else if( P->GetArg( 200 ) == "GetLastValNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetAlarmFirstToEnd") P->CommandMode = CIISRecAddAlarm;
			else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
			else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
		}
	}

	if( P->CommandMode == CIISRecSearch )
  {
		if( ThisRec( P ) )
		{
			if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
			else if( P->GetArg( 200 ) == "GetLastValNextToEnd" ) P->CommandMode = CIISRecAddLastVal;
			else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" ) P->CommandMode = CIISRecAddAlarm;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_RecNotFound;
	}

	else if( P->CommandMode == CIISRecAdd )
	{
		O->MessageData = O->MessageData +
		"1=" + IntToStr( AlarmRec->AlarmSerialNo ) + "\r\n" +
		"2=" + IntToStr( AlarmRec->AlarmCanAdr ) + "\r\n" +
		"3=" + IntToStr( AlarmRec->AlarmStatus ) + "\r\n" +
		"4=" + AlarmRec->AlarmName + "\r\n" +
		"5=" + IntToStr( AlarmRec->AlarmType ) + "\r\n" +
		"6=" + IntToStr( AlarmRec->RequestStatus ) + "\r\n" +
		"7=" + CIISBoolToStr( AlarmRec->AlarmConnected ) + "\r\n" +
		"8=" + IntToStr( AlarmRec->AlarmVerMajor ) + "\r\n" +
		"9=" + IntToStr( AlarmRec->AlarmVerMinor ) + "\r\n" +

		"10=" + CIISBoolToStr( AlarmRec->DO1 ) + "\r\n" +
		"11=" + CIISBoolToStr( AlarmRec->DO2 ) + "\r\n" +
		"12=" + CIISBoolToStr( AlarmRec->DO3 ) + "\r\n" +
		"13=" + CIISBoolToStr( AlarmRec->DO4 ) + "\r\n" +

		"14=" + IntToStr( AlarmRec->DO1AlarmType ) + "\r\n" +
		"15=" + IntToStr( AlarmRec->DO2AlarmType ) + "\r\n" +
		"16=" + IntToStr( AlarmRec->DO3AlarmType ) + "\r\n" +
		"17=" + IntToStr( AlarmRec->DO4AlarmType ) + "\r\n" +

		"18=" + CIISBoolToStr( AlarmRec->InvDO1 ) + "\r\n" +
		"19=" + CIISBoolToStr( AlarmRec->InvDO2 ) + "\r\n" +
		"20=" + CIISBoolToStr( AlarmRec->InvDO3 ) + "\r\n" +
		"21=" + CIISBoolToStr( AlarmRec->InvDO4 ) + "\r\n" +

		"22=" + CIISBoolToStr( AlarmRec->VerNotSupported ) + "\r\n" +
		"23=" + CIISBoolToStr( AlarmRec->CANAlarmEnabled ) + "\r\n" +
		"24=" + IntToStr( AlarmRec->CANAlarmStatus ) + "\r\n" +

		"50=" + IntToStr( AlarmRec->ZoneNo ) + "\r\n";



		O->MessageCode = CIISMsg_Ok;

		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecAddLastVal )
	{
		O->MessageData = O->MessageData +
		"1=" + IntToStr( AlarmRec->AlarmSerialNo ) + "\r\n" +
		"7=" + CIISBoolToStr( AlarmRec->DO1 ) + "\r\n" +
		"8=" + CIISBoolToStr( AlarmRec->DO2 ) + "\r\n" +
		"9=" + CIISBoolToStr( AlarmRec->DO3 ) + "\r\n" +
		"10=" + CIISBoolToStr( AlarmRec->DO4 ) + "\r\n";

		O->MessageCode = CIISMsg_Ok;
		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }

  else if( P->CommandMode == CIISRecAddAlarm )
  {
		O->MessageData = O->MessageData +
		"1=" + IntToStr( AlarmRec->AlarmSerialNo ) + "\r\n";

		O->MessageCode = CIISMsg_Ok;
		if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
	}
  else if( P->CommandMode == CIISRecCount )
  {
	O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetAlarmRecCount() ) + "\r\n";;
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else O->MessageCode = CIISMsg_UnknownCommand;
}
void __fastcall TCIISAlarm::ClearLastValues(void)
{
    DB->ClearSensorMiscValues(SerialNo);
}
void __fastcall TCIISAlarm::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  bool AlarmMovedToOtherZone = false;

  if( P->ArgInc(200) )
  {
     String Command = P->GetArg(200);
	 if( Command == "Update LastValue" )
	 {
			O->MessageCode = CIISMsg_Ok;
	 }
    else if (Command == "Clear LastValues")
    {
				ClearLastValues();
        O->MessageCode = CIISMsg_Ok;
    }
  }
  else if( DB->LocateAlarmRec( AlarmRec ) )
	{
		if( P->ArgInc(2) ) AlarmRec->AlarmName = P->GetArg(2);

		if( P->ArgInc(3) ) AlarmRec->DO1 = CIISStrToBool(P->GetArg(3));
		if( P->ArgInc(4) ) AlarmRec->DO2 = CIISStrToBool(P->GetArg(4));
		if( P->ArgInc(5) ) AlarmRec->DO3 = CIISStrToBool(P->GetArg(5));
		if( P->ArgInc(6) ) AlarmRec->DO4 = CIISStrToBool(P->GetArg(6));

		if( P->ArgInc(7) ) AlarmRec->DO1AlarmType = StrToInt(P->GetArg(7));
		if( P->ArgInc(8) ) AlarmRec->DO2AlarmType = StrToInt(P->GetArg(8));
		if( P->ArgInc(9) ) AlarmRec->DO3AlarmType = StrToInt(P->GetArg(9));
		if( P->ArgInc(10) ) AlarmRec->DO4AlarmType = StrToInt(P->GetArg(10));

		if( P->ArgInc(11) ) AlarmRec->InvDO1 = CIISStrToBool(P->GetArg(11));
		if( P->ArgInc(12) ) AlarmRec->InvDO2 = CIISStrToBool(P->GetArg(12));
		if( P->ArgInc(13) ) AlarmRec->InvDO3 = CIISStrToBool(P->GetArg(13));
		if( P->ArgInc(14) ) AlarmRec->InvDO4 = CIISStrToBool(P->GetArg(14));
		if( P->ArgInc(15) ) AlarmRec->CANAlarmEnabled = CIISStrToBool(P->GetArg(15));
		if( P->ArgInc(16) ) AlarmRec->CANAlarmStatus = StrToInt(P->GetArg(16));

		if( P->ArgInc(50) )
		{
			if( AlarmRec->ZoneNo != P->GetArg(50) )
			{
			 AlarmMovedToOtherZone = true;
			 AlarmRec->ZoneNo = StrToInt(P->GetArg(50));
			}
		}

		if( DB->SetAlarmRec( AlarmRec ) )
		{
			DB->ApplyUpdatesAlarm();
			P_Prj->Log( ClientCom, CIIEventCode_AlarmTabChanged, CIIEventLevel_High, "", 0, 0 );
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;
	}
  else O->MessageCode = CIISMsg_RecNotFound;

  if( AlarmMovedToOtherZone )
	{
		P_Prj->Log( ClientCom, CIIEventCode_AlarmMoved, CIIEventLevel_High, AlarmRec->AlarmSerialNo, 0, 0 );
		P->CommandMode = CIISAlarmMoved;
		P->CIISObj = this;
  }
  else P->CommandMode = CIISCmdReady;

	if( P->ArgInc(3) || P->ArgInc(4) || P->ArgInc(5) || P->ArgInc(6) || P->ArgInc(7) ||
			P->ArgInc(8) || P->ArgInc(9) || P->ArgInc(10)|| P->ArgInc(11)|| P->ArgInc(12) ||
			P->ArgInc(13)|| P->ArgInc(14) )
	{
		if( DB->LocateAlarmRec( AlarmRec ) ) this->AlarmIniState();
  }
}

void _fastcall TCIISAlarm::SetZoneCanAdr( int32_t ZoneCanAdr )
{
  AlarmRec->ZoneCanAdr = ZoneCanAdr;
  if( DB->LocateAlarmRec( AlarmRec ) )
	{
		DB->SetAlarmRec( AlarmRec ); // DB->ApplyUpdatesAlarm();
  }
  if( CIISBICh != NULL ) CIISBICh->SetGroup( AlarmRec->AlarmCanAdr, AlarmRec->ZoneCanAdr );
}

  //CIISBusInt

bool __fastcall TCIISAlarm::ResetCIISBus()
{
  CIISBICh = NULL;
  AlarmRec->AlarmCanAdr = 0;
  AlarmRec->AlarmStatus = 1;
  AlarmRec->AlarmConnected = false;
	NodeDetected = false;
	AlarmIniRunning = false;

  if( DB->LocateAlarmRec( AlarmRec ) )
  {
		DB->SetAlarmRec( AlarmRec ); //DB->ApplyUpdatesAlarm();
  }
  return true;
}

bool __fastcall TCIISAlarm::NodeIniReady()
{
  return !AlarmIniRunning;
}

#pragma argsused
void __fastcall TCIISAlarm::OnSysClockTick( TDateTime TickTime )
{
	if( AlarmIniRunning ) SM_AlarmIni();

  OnSlowClockTick--;
  if( OnSlowClockTick <= 0 )
	{
		OnSlowClockTick = SetAlarmInterval;
		if( !P_Ctrl->RestartRunning )SetAlarms();
	}

}

void __fastcall TCIISAlarm::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
	if( ( BPIn->MessageCommand == MSG_TX_IDENTIFY ) && ( BPIn->SerNo == AlarmRec->AlarmSerialNo )) AlarmIni( BPIn );
	else if(( CIISBICh == BPIn->GetBIChannel() ) && ( BPIn->CANAdr == AlarmRec->AlarmCanAdr ) )
	{
		switch (BPIn->MessageCommand)
		{
		case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )
			//
			break;

		case MSG_TX_SAMPLEEXTCH: // Reqest extended Sample ( Monitor, LastValue or Decay )
			//
			break;

		case MSG_TX_CAPABILITY:
			AlarmIniCapability( BPIn );
			break;

		case MSG_TX_RANGE:
			//
			break;

		case MSG_TX_VERSION:
			AlarmIniVersion( BPIn );
			break;

		default:
			break;
		}
  }
}

#pragma argsused
void __fastcall TCIISAlarm::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISAlarm::AlarmIni( TCIISBusPacket *BPIn )
{
	NIState = NI_Accept;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;
	FIniErrorCount = 0;
	AlarmIniRunning = true;

  CIISBICh = (TCIISBIChannel*) BPIn->GetBIChannel();

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  SM_AlarmIni();
}

void __fastcall TCIISAlarm::SM_AlarmIni()
{
	if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				AlarmRec->AlarmCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( AlarmRec->AlarmSerialNo, AlarmRec->AlarmCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( AlarmRec->AlarmCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( AlarmRec->AlarmCanAdr, AlarmRec->ZoneCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer(  AlarmRec->AlarmCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( AlarmRec->AlarmVerMajor > Alarm_SupportedVer )
				{
					AlarmRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					AlarmRec->VerNotSupported = false;

					NIState = NI_Ready;
					IniRetrys = NoOffIniRetrys;
				}

			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			AlarmIniRunning = false;
			AlarmRec->AlarmConnected = true;
			if( DB->LocateAlarmRec( AlarmRec ) )
			{
				DB->SetAlarmRec( AlarmRec );
			}
			AlarmIniState();
			break;

			case NI_TimeOut :
			AlarmIniRunning = false;
			AlarmRec->AlarmConnected = false;
			if( DB->LocateAlarmRec( AlarmRec ) )
			{
				DB->SetAlarmRec( AlarmRec );
			}
			break;

			default:
			break;
		}
	}
	T += SysClock;
}

void __fastcall TCIISAlarm::AlarmIniCapability( TCIISBusPacket *BPIn )
{
	AlarmRec->AlarmType = BPIn->NodeCap;
	AlarmRec->AlarmStatus = 0;
	BPIn->CIISObj = this;
	BPIn->MsgMode = CIISBus_UpdateNode;
	BPIn->ParseMode = CIISParseFinalize;

  #if DebugCIISBusInt == 1
  Debug( "AlarmCapability recived Alarm: " + IntToStr( AlarmRec->AlarmSerialNo ) + " / " + IntToStr( AlarmRec->AlarmCanAdr ) + " = " + IntToStr( BPIn->NodeCap ) );
  #endif
}

void __fastcall TCIISAlarm::AlarmIniVersion( TCIISBusPacket *BPIn )
{
	if( VerRecived )
	{
		P_Prj->Log(CANCom, CIIEventCode_DupSNR, CIIEventLevel_High, "BI:" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( AlarmRec->AlarmCanAdr ) + "/" + IntToStr( AlarmRec->AlarmSerialNo ), AlarmRec->AlarmSerialNo, BPIn->Tag);
	}

	VerRecived = true;
  AlarmRec->AlarmVerMinor = BPIn->Byte4;
  AlarmRec->AlarmVerMajor = BPIn->Byte3;
  AlarmRec->AlarmStatus = 0;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  #if DebugCIISBusInt == 1
  Debug( "AlarmVersion recived Alarm: " + IntToStr( AlarmRec->AlarmSerialNo ) + " / " + IntToStr( AlarmRec->AlarmCanAdr ) + " = " + IntToStr( AlarmRec->AlarmVerMajor) + "." + IntToStr( AlarmRec->AlarmVerMinor));
  #endif
}


void __fastcall TCIISAlarm::SetDefaultValues()
{

	AlarmRec->AlarmName = IntToStr( AlarmRec->AlarmSerialNo );
  AlarmRec->AlarmStatus = 0;
	AlarmRec->RequestStatus = -1;

	AlarmRec->DO1 = false;
	AlarmRec->DO2 = false;
	AlarmRec->DO3 = false;
	AlarmRec->DO4 = false;

	AlarmRec->DO1AlarmType = 0;
	AlarmRec->DO2AlarmType = 0;
	AlarmRec->DO3AlarmType = 0;
	AlarmRec->DO4AlarmType = 0;

	AlarmRec->InvDO1 = false;
	AlarmRec->InvDO2 = false;
	AlarmRec->InvDO3 = false;
	AlarmRec->InvDO4 = false;

	AlarmRec->VerNotSupported = true;
	AlarmRec->CANAlarmEnabled = true;
	AlarmRec->CANAlarmStatus = 0;

	DB->SetAlarmRec( AlarmRec );

}

void __fastcall TCIISAlarm::ResetCANAlarm()
{
	if( AlarmRec->CANAlarmStatus == 1 )
	{
		AlarmRec->CANAlarmStatus = 0;
		MissingTagDetected = false;
		P_Prj->AlarmStatusChanged = true;
	}
}


void __fastcall TCIISAlarm::AlarmIniState()
{
	if( CIISBICh != NULL )
	{
		CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,0, AlarmRec->DO1 ^ AlarmRec->InvDO1 );
		CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,1, AlarmRec->DO2 ^ AlarmRec->InvDO2 );
		CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,2, AlarmRec->DO3 ^ AlarmRec->InvDO3 );
		CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,3, AlarmRec->DO4 ^ AlarmRec->InvDO4 );
		SetAlarms();
	}
}

bool TCIISAlarm::CheckAlarm( CIISAlarmType AlarmType)
{
	bool Alarm;

	switch ( AlarmType )
	{
		case ProjectAlarm:
		Alarm = P_Prj->AlarmStatus;
		break;

		case ControllerAlarm:
		Alarm = P_Ctrl->AlarmStatus;
		break;

		case ZoneAlarm:
		Alarm = P_Zone->AlarmStatus;
		break;

		default:
		break;
	}

	return Alarm;
}

void __fastcall TCIISAlarm::SetAlarms()
{
	bool Alarm, DOChanged;

	DOChanged = false;

	if( AlarmRec->DO1AlarmType != 0 )
	{
		Alarm = CheckAlarm( (CIISAlarmType) AlarmRec->DO1AlarmType );
		if( Alarm  && !AlarmRec->DO1 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,0, true ^ AlarmRec->InvDO1 );
			AlarmRec->DO1 = true;
			DOChanged = true;
		}
		else if(!Alarm && AlarmRec->DO1 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,0, false ^ AlarmRec->InvDO1 );
			AlarmRec->DO1 = false;
			DOChanged = true;
		}
	}

	if( AlarmRec->DO2AlarmType != 0 )
	{
		Alarm = CheckAlarm( (CIISAlarmType)AlarmRec->DO2AlarmType );
		if( Alarm  && !AlarmRec->DO2 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,1, true ^ AlarmRec->InvDO2 );
			AlarmRec->DO2 = true;
			DOChanged = true;
		}
		else if(!Alarm && AlarmRec->DO2 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,1, false ^ AlarmRec->InvDO2 );
			AlarmRec->DO2 = false;
			DOChanged = true;
		}
	}

	if( AlarmRec->DO3AlarmType != 0 )
	{
		Alarm = CheckAlarm( (CIISAlarmType)AlarmRec->DO3AlarmType );
		if( Alarm  && !AlarmRec->DO3 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,2, true ^ AlarmRec->InvDO3 );
			AlarmRec->DO3 = true;
			DOChanged = true;
		}
		else if(!Alarm && AlarmRec->DO3 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,2, false ^ AlarmRec->InvDO3 );
			AlarmRec->DO3 = false;
			DOChanged = true;
		}
	}

	if( AlarmRec->DO4AlarmType != 0 )
	{
		Alarm = CheckAlarm( (CIISAlarmType)AlarmRec->DO4AlarmType );
		if( Alarm  && !AlarmRec->DO4 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,3, true ^ AlarmRec->InvDO4 );
			AlarmRec->DO4 = true;
			DOChanged = true;
		}
		else if(!Alarm && AlarmRec->DO4 )
		{
			CIISBICh->SetDigOut( AlarmRec->AlarmCanAdr,3, false ^ AlarmRec->InvDO4 );
			AlarmRec->DO4 = false;
			DOChanged = true;
		}
	}

	if( DOChanged )
	{
		DB->SetAlarmRec( AlarmRec ); DB->ApplyUpdatesAlarm();
		P_Prj->Log( ClientCom, CIIEventCode_AlarmTabChanged, CIIEventLevel_High, "", 0, 0 );
  }

}



