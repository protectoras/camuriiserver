//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISNodeIni.h"
#include "CIISProject.h"
#include "CIISController.h"
#include "CIISBusInterface.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISNodeIni::TCIISNodeIni( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISNodeRec *SetNodeRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISNode;

  P_Ctrl = (TCIISController*)CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	CIISBICh = NULL;

  NodeRec = SetNodeRec;

  SensorIniRunning = false;
  NodeDetected = false;

	FIniErrorCount = 0;

	#if DebugCIISStart == 1

	Debug( "Nod created: " + IntToStr( NodeRec->NodeCanAdr ) + "/" + IntToStr( NodeRec->NodeSerialNo ));

	#endif
}


__fastcall TCIISNodeIni::~TCIISNodeIni()
{
  //P_Ctrl->DecDetectedNodeCount();
}

//---------------------------------------------------------------------------
#pragma argsused
void __fastcall TCIISNodeIni::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "NodeIni: ParseCommand_Begin" );
	#endif
}
#pragma argsused
void __fastcall TCIISNodeIni::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "NodeIni: ParseCommand_End" );
	#endif
}
#pragma argsused
void __fastcall TCIISNodeIni::OnSysClockTick( TDateTime TickTime )
{
  if( SensorIniRunning ) SM_SensorIni();
}

  //CIISBusInt

bool __fastcall TCIISNodeIni::ResetCIISBus()
{
	CIISBICh = NULL;
  NodeRec->NodeCanAdr = 0;
  NodeRec->NodeStatus = 1;
  NodeRec->NodeConnected = false;
	NodeDetected = false;
	SensorIniRunning = false;
	return true;
}

bool __fastcall TCIISNodeIni::NodeIniReady()
{
  return !SensorIniRunning;
}

void __fastcall TCIISNodeIni::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
  if( ( BPIn->MessageCommand == MSG_TX_IDENTIFY ) && ( BPIn->SerNo == NodeRec->NodeSerialNo )) NodeIni( BPIn );
	else if(( CIISBICh == BPIn->GetBIChannel() ) && ( BPIn->CANAdr == NodeRec->NodeCanAdr ) )
  {
		switch (BPIn->MessageCommand)
		{
			case MSG_TX_CAPABILITY:
			NodeIniCapability( BPIn );
			break;

			default:
			break;
		}
	}
}
#pragma argsused
void __fastcall TCIISNodeIni::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISNodeIni::NodeIni( TCIISBusPacket *BPIn )
{
  NIState = NI_Accept;
  IniRetrys = NoOffIniRetrys;
  T = 0;
	TNextEvent = 0;
	FIniErrorCount = 0;
  SensorIniRunning = true;

	CIISBICh = (TCIISBIChannel*) BPIn->GetBIChannel();

  BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	SM_SensorIni();
}

void __fastcall TCIISNodeIni::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;
  
		switch( NIState )
		{
			case NI_Accept :
				if( !NodeDetected )
				{
					NodeRec->NodeCanAdr = CIISBICh->GetCanAdr();
					P_Ctrl->IncDetectedNodeCount();
					NodeDetected = true;
				}
				//else FIniErrorCount++;

				CIISBICh->SendAccept( NodeRec->NodeSerialNo, NodeRec->NodeCanAdr );
				TNextEvent = TNextEvent + AcceptDelay + ( NodeRec->NodeCanAdr - CANStartAdrNode ) * NodToNodDelay;
				NIState = NI_ReqCapability;
				break;

			case NI_ReqCapability :
				CIISBICh->RequestCapability( NodeRec->NodeCanAdr );
				CapabilityRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCapability;
				break;

			case NI_WaitForCapability :
				if( CapabilityRecived )
				{
					NIState = NI_Ready;
				}
				else // retry
				{
					FIniErrorCount++;
					if( IniRetrys-- > 0 ) NIState = NI_ReqCapability;
					else NIState = NI_TimeOut;
				}
				break;

			case NI_Ready :
				NodeRec->NodeConnected = true;
				SensorIniRunning = false;
				break;

			case NI_TimeOut :
				SensorIniRunning = false;
				break;

			default:
				break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISNodeIni::NodeIniCapability( TCIISBusPacket *BPIn )
{
  CapabilityRecived = true;
  NodeRec->NodeType = BPIn->NodeCap;
  //SensorIniRunning = false;
  //NodeRec->NodeConnected = true;
  BPIn->CIISObj = this;
  BPIn->MsgMode = CIISBus_UpdateNode;
  BPIn->ParseMode = CIISParseFinalize;
}









