//---------------------------------------------------------------------------

#ifndef CIISMiscValuesH
#define CIISMiscValuesH
//---------------------------------------------------------------------------


#include "CIISObj.h"


class TCIISMiscValues : public TCIISObj
{
private:
  CIISMiscValueRec *MiscValRec;

 bool __fastcall ThisRec( TCamurPacket *P );
 void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;

protected:
  //CIISClientInt
  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );
  //CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
  __fastcall TCIISMiscValues( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISMiscValueRec *SetMValRec, TObject *SetCIISParent );
  __fastcall ~TCIISMiscValues();



__published:

};

#endif
