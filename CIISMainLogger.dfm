object CIISMainLoggerFrm: TCIISMainLoggerFrm
  Left = 100
  Top = 53
  ParentCustomHint = False
  Caption = 'Camur II Server'
  ClientHeight = 336
  ClientWidth = 481
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poDesigned
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 16
  object BBReset: TBitBtn
    Left = 378
    Top = 300
    Width = 93
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Reset'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 0
    OnClick = BBResetClick
  end
  object BBDebugWin: TBitBtn
    Left = 280
    Top = 300
    Width = 94
    Height = 31
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Debug'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555500000000
      0555555F7777777775F55500FFFFFFFFF0555577F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF777FFFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF7F777FF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FF77F7FFF05557F7F5FFFFFFF7F550F0FEEEEEEE
      F05557F7F777777757F550F0FFFFFFFFF05557F7FF5F5F5F57F550F00F0F0F0F
      005557F77F7F7F7F77555055070707070555575F7F7F7F7F7F55550507070707
      0555557575757575755555505050505055555557575757575555}
    NumGlyphs = 2
    TabOrder = 1
    OnClick = BBDebugWinClick
  end
  object GBServerPortsConnected: TGroupBox
    Left = 9
    Top = 9
    Width = 252
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Active Client'
    TabOrder = 2
    object LCIIMonitor: TLabel
      Left = 56
      Top = 25
      Width = 33
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Local'
    end
    object LCIIUser: TLabel
      Left = 56
      Top = 49
      Width = 48
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Remote'
    end
    object ShMonitorConnected: TShape
      Left = 20
      Top = 28
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object ShUserConnected: TShape
      Left = 20
      Top = 48
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
  end
  object GBCamurIILogger: TGroupBox
    Left = 280
    Top = 113
    Width = 191
    Height = 145
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Camur II Bus'
    TabOrder = 3
    object LCIIBusInt: TLabel
      Left = 61
      Top = 25
      Width = 84
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Bus Interfaces'
    end
    object LNodes: TLabel
      Left = 61
      Top = 65
      Width = 109
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Connected Nodes'
    end
    object LNoOfNodes: TLabel
      Left = 25
      Top = 65
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LNoOfBusInt: TLabel
      Left = 25
      Top = 25
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LNoOfDetecedNodes: TLabel
      Left = 25
      Top = 45
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LDetectedNodes: TLabel
      Left = 61
      Top = 45
      Width = 99
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Detected Nodes'
    end
    object LNodeIniErrors: TLabel
      Left = 61
      Top = 85
      Width = 89
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Node Ini Errors'
    end
    object LNoOfNodIniErrors: TLabel
      Left = 25
      Top = 85
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LTCANRx: TLabel
      Left = 61
      Top = 106
      Width = 88
      Height = 16
      Caption = 'T CAN Rx ( % )'
    end
    object LTSys: TLabel
      Left = 61
      Top = 126
      Width = 64
      Height = 16
      Caption = 'T Sys ( % )'
    end
    object LTCANRxValue: TLabel
      Left = 25
      Top = 106
      Width = 7
      Height = 16
      Caption = '0'
    end
    object LTSysValue: TLabel
      Left = 25
      Top = 126
      Width = 7
      Height = 16
      Caption = '0'
    end
    object ShRunning: TShape
      Left = 173
      Top = 14
      Width = 10
      Height = 10
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clLime
      Shape = stCircle
    end
  end
  object GBRecordings: TGroupBox
    Left = 280
    Top = 10
    Width = 191
    Height = 105
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Recordings'
    TabOrder = 4
    object LNoOfMonitors: TLabel
      Left = 25
      Top = 25
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LMonitors: TLabel
      Left = 61
      Top = 25
      Width = 72
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Monitor Rec'
    end
    object LNoOfDecays: TLabel
      Left = 25
      Top = 61
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LDecays: TLabel
      Left = 61
      Top = 61
      Width = 68
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Decay Rec'
    end
    object NoOfLPRs: TLabel
      Left = 25
      Top = 79
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
      Color = clBtnFace
      ParentColor = False
    end
    object LLPRs: TLabel
      Left = 61
      Top = 79
      Width = 54
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'LPR Rec'
    end
    object LNoOfMonitorsExt: TLabel
      Left = 25
      Top = 44
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '0'
    end
    object LMonitorsExt: TLabel
      Left = 61
      Top = 44
      Width = 93
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Monitor Ext Rec'
    end
  end
  object BBStartCIISLoggger: TBitBtn
    Left = 280
    Top = 261
    Width = 191
    Height = 32
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Start CIISLogger'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      3333333777333777FF33339993707399933333773337F3777FF3399933000339
      9933377333777F3377F3399333707333993337733337333337FF993333333333
      399377F33333F333377F993333303333399377F33337FF333373993333707333
      333377F333777F333333993333101333333377F333777F3FFFFF993333000399
      999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
      99933773FF777F3F777F339993707399999333773F373F77777F333999999999
      3393333777333777337333333999993333333333377777333333}
    NumGlyphs = 2
    TabOrder = 5
    OnClick = BBStartCIISLogggerClick
  end
  object GBuCtrl: TGroupBox
    Left = 9
    Top = 116
    Width = 252
    Height = 218
    Caption = 'uCtrl'
    TabOrder = 6
    Visible = False
    object ShuCtrlRecordingOn: TShape
      Left = 8
      Top = 25
      Width = 21
      Height = 11
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object LuCtrlRecordingOn: TLabel
      Left = 39
      Top = 23
      Width = 83
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Recording On'
    end
    object ShuCtrlReadingData: TShape
      Left = 8
      Top = 41
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object LuCtrlReadingData: TLabel
      Left = 39
      Top = 39
      Width = 84
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Reading Data'
    end
    object LuCtrlSampleTimeStamp: TLabel
      Left = 130
      Top = 58
      Width = 115
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '1980-01-01 00:00:00'
    end
    object LuCtrlBytesRead: TLabel
      Left = 239
      Top = 39
      Width = 7
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label2: TLabel
      Left = 39
      Top = 105
      Width = 54
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Power IN'
    end
    object ShuCtrlPowerIn: TShape
      Left = 8
      Top = 109
      Width = 21
      Height = 12
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 5
      Margins.Bottom = 5
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object ShuCtrlPowerOn: TShape
      Left = 8
      Top = 125
      Width = 21
      Height = 13
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 5
      Margins.Bottom = 5
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object Label3: TLabel
      Left = 39
      Top = 121
      Width = 58
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Power On'
    end
    object ShuCtrlPowerOk: TShape
      Left = 8
      Top = 141
      Width = 21
      Height = 13
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 5
      Margins.Bottom = 5
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object Label4: TLabel
      Left = 39
      Top = 138
      Width = 59
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Output OK'
    end
    object LuCtrlTime: TLabel
      Left = 8
      Top = 188
      Width = 115
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '1980-01-01 00:00:00'
    end
    object ShuCtrlStoringData: TShape
      Left = 8
      Top = 59
      Width = 21
      Height = 12
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Brush.Color = clBtnFace
      Shape = stRoundRect
    end
    object Label1: TLabel
      Left = 39
      Top = 58
      Width = 83
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Storing    Data'
    end
    object BBuCtrlSync: TBitBtn
      Left = 140
      Top = 181
      Width = 106
      Height = 33
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Sync Time'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333000003333333333F77777FFF333333009999900
        3333333777777777FF33330998FFF899033333777333F3777FF33099FFFCFFF9
        903337773337333777F3309FFFFFFFCF9033377333F3337377FF098FF0FFFFFF
        890377F3373F3333377F09FFFF0FFFFFF90377F3F373FFFFF77F09FCFFF90000
        F90377F733377777377F09FFFFFFFFFFF90377F333333333377F098FFFFFFFFF
        890377FF3F33333F3773309FCFFFFFCF9033377F7333F37377F33099FFFCFFF9
        90333777FF37F3377733330998FCF899033333777FF7FF777333333009999900
        3333333777777777333333333000003333333333377777333333}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BBuCtrlSyncClick
    end
    object BBuCtrlREadData: TBitBtn
      Left = 143
      Top = 143
      Width = 106
      Height = 31
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Read Data'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333333333333333333333FFFFF3333333333CCCCC33
        33333FFFF77777FFFFFFCCCCCC808CCCCCC377777737F777777F008888070888
        8003773FFF7773FFF77F0F0770F7F0770F037F777737F777737F70FFFFF7FFFF
        F07373F3FFF7F3FFF37F70F000F7F000F07337F77737F777373330FFFFF7FFFF
        F03337FF3FF7F3FF37F3370F00F7F00F0733373F7737F77337F3370FFFF7FFFF
        0733337F33373F337333330FFF030FFF03333373FF7373FF7333333000333000
        3333333777333777333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BBuCtrlREadDataClick
    end
  end
  object TimerSysCheck: TTimer
    Enabled = False
    Interval = 50
    OnTimer = TimerSystemClockEvent
    Left = 456
    Top = 8
  end
end
