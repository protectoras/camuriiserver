//---------------------------------------------------------------------------

#ifndef CIISBIChInfoH
#define CIISBIChInfoH
//---------------------------------------------------------------------------

#include "CIISObj.h"


class TCIISBIChInfo : public TCIISObj
{
private:
	CIISBIChannelRec *BIChRec;

 bool __fastcall ThisRec( TCamurPacket *P );
 void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
 void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );

protected:
	//CIISClientInt
	void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );
	//CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
	__fastcall TCIISBIChInfo( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISBIChannelRec *SetBIChRec, TObject *SetCIISParent );
	__fastcall ~TCIISBIChInfo();



__published:

};

#endif
