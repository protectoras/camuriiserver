//---------------------------------------------------------------------------
// History
//
//  Date			Comment							Sign
//  2012-03-04	Converted to Xe2    				Bo H
//  2015-02-08	Converted to Xe7,
// 				Removed obsolete CIITabFileSize    	Bo H

#include <vcl.h>
#include <dir.h>
#pragma hdrstop
#include <math.h>
#include "CIISCommon.h"



//void __fastcall TTCPHandler::IPBytesFromString(UnicodeString _str, char ip[4])
void __fastcall StringToIPBytes( String IPAdr, Word IPAdrBytes[4] )
{

	for ( int32_t i = 0; i < 4; i++ )
	{
		String byteString;
		if (IPAdr.Pos(".") > 0) {
			byteString = IPAdr.SubString(0, IPAdr.Pos(".") - 1);
			IPAdr.Delete(1, IPAdr.Pos("."));
		}
		else
			byteString = IPAdr;

		Word byte = (Word)byteString.ToInt();
		IPAdrBytes[i] = byte;
	}
}


uint32_t __fastcall HexToInt( String Hex )
{
	Hex = Hex.LowerCase();
	uint32_t result = 0;

	for( int32_t i = 0; i < Hex.Length(); i++ )
	{
		String CharAtPos = Hex.SubString0(i,1);
		int32_t Dec = 0;
		if(CharAtPos == "a") Dec = 10;
		else if(CharAtPos == "b") Dec = 11;
		else if(CharAtPos == "c") Dec = 12;
		else if(CharAtPos == "d") Dec = 13;
		else if(CharAtPos == "e") Dec = 14;
		else if(CharAtPos == "f") Dec = 15;
		else Dec = StrToInt( CharAtPos );

		result += Dec * pow((double)16, Hex.Length() -i - 1);
	}

	return result;
}


//---------------------------------------------------------------------------

TDateTime __fastcall Now_ms_cleard()
{
  Word Year, Month, Day, Hour, Min, Sec, MSec;
  TDateTime CurrentEvTime;

  CurrentEvTime = Now();
  DecodeDate(CurrentEvTime, Year, Month, Day);
  DecodeTime(CurrentEvTime, Hour, Min, Sec, MSec);
  CurrentEvTime = EncodeDate( Year, Month, Day );
  CurrentEvTime = CurrentEvTime + EncodeTime( Hour, Min, Sec, 0 );

  return CurrentEvTime;
}

//---------------------------------------------------------------------------

TDateTime __fastcall Now_ms()
{
  Word Year, Month, Day, Hour, Min, Sec, MSec;
  TDateTime CurrentEvTime;

  CurrentEvTime = Now();
  DecodeDate(CurrentEvTime, Year, Month, Day);
  DecodeTime(CurrentEvTime, Hour, Min, Sec, MSec);
  CurrentEvTime = EncodeDate( Year, Month, Day );
  CurrentEvTime = CurrentEvTime + EncodeTime( Hour, Min, Sec, MSec );

  return CurrentEvTime;
}

//---------------------------------------------------------------------------

String __fastcall CIISBoolToStr( bool b )
{
 String s;

 if( b ) s = "True";
 else s = "False";

 return s;

}

bool __fastcall CIISStrToBool( String s )
{
  return s == "True";
}

String __fastcall CIISFloatToStr( double d )
{
  int32_t p;
  String s;

  s = FloatToStrF( d, ffExponent, 5, 0 );
  p = s.Pos( FormatSettings.DecimalSeparator );
  if( p > 0 ) s[p] = '.';

  return s;
}

//---------------------------------------------------------------------------

double __fastcall CIISStrToFloat( String s )
{
  int32_t p;
  double d;

  p = s.Pos( "." );
  if( p > 0 ) s[p] = FormatSettings.DecimalSeparator;
  d = StrToFloat( s );

  return d;
}

//---------------------------------------------------------------------------

TDateTime __fastcall CIISStrToDateTime( String s )
{
  TDateTime t;
  Word Year, Month, Day, Hour, Min, Sec, MSec;
  int32_t p;

  Year = 1899;
  Month = 12;
  Day = 30;
  Hour = 0;
  Min = 0;
  Sec = 0;
  MSec = 0;

	p = s.Pos( "-" );
	if( p > 0 )
	{
		Year = StrToInt( s.SubString( 1, p - 1 ) );
		s.Delete( 1, p );
	}

	p = s.Pos( "-" );
	if( p > 0 )
	{
		Month = StrToInt( s.SubString( 1, p - 1 ));
		s.Delete( 1, p );
	}

	p = s.Pos( " " );
	if( p > 0 )
	{
		Day = StrToInt( s.SubString( 1, p - 1 ));
		s.Delete( 1, p );
	}
	else
	{
		Day = StrToInt( s );
		t = EncodeDate( Year, Month, Day );
		return t;
	}

	p = s.Pos( ":" );
	if( p > 0 )
	{
		Hour = StrToInt( s.SubString( 1, p - 1 ));
		s.Delete( 1, p );
	}

	p = s.Pos( ":" );
	if( p > 0 )
	{
		Min = StrToInt( s.SubString( 1, p - 1 ));
		s.Delete( 1, p );
	}

  p = s.Pos( "-" );
	if( p > 0 )
	{
		Sec = StrToInt( s.SubString( 1, p - 1 ));
		s.Delete( 1, p );
	}
	else
	{
		Sec = StrToInt( s );
		s.Delete( 1, s.Length() );
  }

  p = s.Length();
	if( p > 0 )
	{
		MSec = StrToInt( s.SubString( 1, p));
		s.Delete( 1, p );
	}

	t = EncodeDate( Year, Month, Day );
  t = t + EncodeTime( Hour, Min, Sec, MSec );

	return t;
}

//---------------------------------------------------------------------------

String __fastcall CIISDateTimeToStr( TDateTime t )
{
	Word Year, Month, Day, Hour, Min, Sec, MSec;
  String s;

	DecodeDate(t, Year, Month, Day);
	DecodeTime(t, Hour, Min, Sec, MSec);
  s.sprintf(L"%4d-%02d-%02d %02d:%02d:%02d-%03d",
            Year, Month, Day, Hour, Min, Sec, MSec);

  return s;
}

String __fastcall CIISDateTimeToStrNomS( TDateTime t )
{
	Word Year, Month, Day, Hour, Min, Sec, MSec;
  String s;

	DecodeDate(t, Year, Month, Day);
	DecodeTime(t, Hour, Min, Sec, MSec);
	s.sprintf(L"%4d-%02d-%02d %02d:%02d:%02d",
            Year, Month, Day, Hour, Min, Sec );

  return s;
}

Word __fastcall CIISBcdToInt( Word ival )
{
	return ((ival >> 4) * 10) + (ival % 16);
}

Word __fastcall CIISIntToBcd( Word ival )
{

	return ((ival / 10) << 4) | (ival % 10);

}

//---------------------------------------------------------------------------

bool __fastcall CIISSetSysTime( TDateTime t )
{
	Word Year, Month, Day, Hour, Min, Sec, MSec;
	SYSTEMTIME st;

	DecodeDate(t, Year, Month, Day);
	DecodeTime(t, Hour, Min, Sec, MSec);

	GetSystemTime(&st);

	st.wYear = Year;
	st.wMonth = Month;
	st.wDay = Day;
	st.wHour = Hour;
	st.wMinute = Min;
	st.wSecond = Sec;

	if ( SetSystemTime(&st) ) return true;
	else return false;
}

/*
  BOOL SetNewTime(WORD hour, WORD minutes)
  {
			SYSTEMTIME st;
  		char *pc;
  
			GetSystemTime(&st);       // gets current time
			st.wHour = hour;          // adjusts hours
			st.wMinute = minutes;     // and minutes
			if (!SetSystemTime(&st))  // sets system time
					return FALSE;
			return TRUE;
	}
*/



//---------------------------------------------------------------------------

AnsiString __fastcall GetProgVersion()
{
 unsigned long dummy;
 int32_t InfoSize;
 char *VerBuffer;
 AnsiString VersionString = "Unknown";

								// Get size
 InfoSize = GetFileVersionInfoSizeW(Application->ExeName.c_str(), &dummy);

 if (InfoSize > 0)
  {
   VerBuffer = new char[InfoSize];

   if (GetFileVersionInfoW(Application->ExeName.c_str(), NULL, InfoSize, VerBuffer))
	{
	  // Structure used to store enumerated languages and code pages.

	  struct LANGANDCODEPAGE {
		WORD wLanguage;
		WORD wCodePage;
	  } *lpTranslate;
	  uint32_t cbTranslate;

	  // Read the list of languages and code pages.

	  if (VerQueryValue(VerBuffer, "\\VarFileInfo\\Translation",
						(LPVOID*)&lpTranslate,
						&cbTranslate))
	  {

	  // Read the file version for each language and code page.
		char SubBlock[200];
		wsprintf( SubBlock,
				  TEXT("\\StringFileInfo\\%04x%04x\\FileVersion"),
				  lpTranslate[0].wLanguage,
				  lpTranslate[0].wCodePage);

		void *lpBuffer;
		uint32_t dwBytes;

		// Retrieve file version
		if (VerQueryValue(VerBuffer, SubBlock, &lpBuffer, &dwBytes))
		  VersionString = AnsiString((char*)lpBuffer);
	  }
	}
   delete VerBuffer;
  }

 return VersionString;

}

//---------------------------------------------------------------------------
// Returns the path to the Common Application Data folder if available.
// Otherwise the path to the Application exe-file
//
//

String __fastcall GetCommonAppFolderPath(void)
{
		String CommonAppFolderPath;
		char szPath[MAXPATH];
		if (SHGetFolderPath(NULL,CSIDL_COMMON_APPDATA|CSIDL_FLAG_CREATE,NULL,0,szPath) == 0)
		{
				CommonAppFolderPath=String(szPath);
				CommonAppFolderPath+= "\\CamurII\\";
				ForceDirectories(CommonAppFolderPath);
		}
		else
		{
				CommonAppFolderPath=ExtractFilePath(Application->ExeName);
		}

		return CommonAppFolderPath;
}
//---------------------------------------------------------------------------

__int64 __fastcall CIIDiskFree( String Path )
{
	Char DrvChar;
	int32_t DrvNo;

	DrvChar = ExtractFileDrive( Path ).UpperCase()[1];
	DrvNo = DrvChar - 'A' + 1;

	return DiskFree( DrvNo );

}


//---------------------------------------------------------------------------
// TCamurPacket
//---------------------------------------------------------------------------

__fastcall TCamurPacket::TCamurPacket()
{
Args = new TCollection(__classid(TDataArg));
FUserLevel = ULNone;
}

//---------------------------------------------------------------------------

__fastcall TCamurPacket::~TCamurPacket()
{
delete Args;
}

//---------------------------------------------------------------------------

String __fastcall TCamurPacket::GetArg(int32_t n)
{
for( int32_t i = 0; i < Args->Count; i++ )
  if( ((TDataArg *)Args->Items[i])->Num == n)
    return ((TDataArg *)Args->Items[i])->Val;
return "";
}

//---------------------------------------------------------------------------

String __fastcall TCamurPacket::GetDelArg(int32_t n)
{
String Rslt;
Rslt = "";
for( int32_t i = 0; i < Args->Count; i++ )
  if( ((TDataArg *)Args->Items[i])->Num == n)
  {
    Rslt = ((TDataArg *)Args->Items[i])->Val;
    Args->Delete( i );
    break;
  }  
return Rslt;
}

//---------------------------------------------------------------------------

bool __fastcall TCamurPacket::ArgInc(int32_t n)
{
for( int32_t i = 0; i < Args->Count; i++ )
  if( ((TDataArg *)Args->Items[i])->Num == n)
    return true;
return false;
}
//---------------------------------------------------------------------------

__fastcall TCIISBusPacket::TCIISBusPacket()
{
  FByte0 = 0;
  FByte1 = 0;
  FByte2 = 0;
  FByte3 = 0;
  FByte4 = 0;
  FByte5 = 0;
  FByte6 = 0;
  FByte7 = 0;
  FParseMode = CIISParseCont;
}

__fastcall TCIISBusPacket::~TCIISBusPacket()
{
}


int32_t __fastcall TCIISBusPacket::ByteToHex( Byte B)
{
  int32_t HexVal;

  HexVal = B - 48;
  if( HexVal < 0 ) HexVal = 0;
  if( HexVal > 9 ) HexVal = HexVal - 7;
  if( HexVal > 15 ) HexVal = 0 ;

  return HexVal;
}

int32_t __fastcall TCIISBusPacket::GetWLinkPar()
{

  int32_t HexVal;

  HexVal = 0;
  if( FByte3 == 0 ) return HexVal;
  HexVal = ByteToHex( FByte3 );
  if( FByte4 == 0 ) return HexVal;
  HexVal = HexVal * 16 + ByteToHex( FByte4 );
  if( FByte5 == 0 ) return HexVal;
  HexVal = HexVal * 16 + ByteToHex( FByte5 );
  if( FByte6 == 0 ) return HexVal;
  HexVal = HexVal * 16 + ByteToHex( FByte6 );
  if( FByte7 == 0 ) return HexVal;
  HexVal = HexVal * 16 + ByteToHex( FByte7 );
  return HexVal;
}

PVOID __fastcall TCIISBusPacket::GetBIChannel()
{
  return FBIChannel;
}

void __fastcall TCIISBusPacket::SetBIChannel( PVOID BIChannel )
{
  FBIChannel = BIChannel;
}


#pragma package(smart_init)
