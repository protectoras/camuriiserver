//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

USEFORM("CIISOnTopMessage.cpp", FormOnTopMessage);
USEFORM("UnitMySQLLogin.cpp", FormMySQLLogin);
USEFORM("DebugWinU.cpp", DebugWin);
USEFORM("CIISMainLogger.cpp", CIISMainLoggerFrm);
USEFORM("CIISDB.cpp", CIISDBModule); /* TDataModule: File Type */
USEFORM("CIISDBInit.cpp", CIISDBInitFrm);
//---------------------------------------------------------------------------
WINAPI int32_t WinMain(HINSTANCE, HINSTANCE, LPSTR, int32_t)
{
        HWND hWndApp=FindWindow("TCIISMainLoggerFrm", NULL);
        if ( hWndApp != NULL)
        {
            ShowMessage("Application already started!");
            return 0;
        }

        try
        {
                 Application->Initialize();
                 Application->Title = "Camur II Server";
		Application->CreateForm(__classid(TCIISMainLoggerFrm), &CIISMainLoggerFrm);
		Application->CreateForm(__classid(TCIISDBInitFrm), &CIISDBInitFrm);
		Application->CreateForm(__classid(TFormOnTopMessage), &FormOnTopMessage);
		Application->CreateForm(__classid(TFormMySQLLogin), &FormMySQLLogin);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
