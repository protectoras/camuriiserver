object FrmZoneSetup: TFrmZoneSetup
  Left = 0
  Top = 0
  Caption = 'FrmZoneSetup'
  ClientHeight = 511
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object LBIChSerialNo: TLabel
    Left = 120
    Top = 24
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LBIChMode: TLabel
    Left = 120
    Top = 53
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LuCtrlTime: TLabel
    Left = 296
    Top = 21
    Width = 65
    Height = 13
    Caption = '??-??-?? ??:??'
  end
  object Label34: TLabel
    Left = 438
    Top = 117
    Width = 17
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'V ='
  end
  object Label35: TLabel
    Left = 438
    Top = 150
    Width = 18
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'I = '
  end
  object Label39: TLabel
    Left = 438
    Top = 184
    Width = 17
    Height = 13
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'T ='
  end
  object LNodeCount: TLabel
    Left = 296
    Top = 213
    Width = 15
    Height = 13
    Caption = '???'
  end
  object Label1: TLabel
    Left = 296
    Top = 244
    Width = 80
    Height = 13
    Caption = 'Camur II FixVolt '
  end
  object ShPowerIn: TShape
    Left = 296
    Top = 374
    Width = 21
    Height = 12
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Brush.Color = clBtnFace
    Shape = stRoundRect
  end
  object ShPowerOn: TShape
    Left = 296
    Top = 394
    Width = 21
    Height = 12
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Brush.Color = clBtnFace
    Shape = stRoundRect
  end
  object ShPowerOk: TShape
    Left = 296
    Top = 414
    Width = 21
    Height = 12
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Brush.Color = clBtnFace
    Shape = stRoundRect
  end
  object Label2: TLabel
    Left = 324
    Top = 373
    Width = 44
    Height = 13
    Caption = 'Power IN'
  end
  object Label3: TLabel
    Left = 324
    Top = 392
    Width = 47
    Height = 13
    Caption = 'Power On'
  end
  object Label4: TLabel
    Left = 324
    Top = 413
    Width = 51
    Height = 13
    Caption = 'Output OK'
  end
  object LFixVoltSnr: TLabel
    Left = 317
    Top = 263
    Width = 25
    Height = 13
    Caption = '?????'
  end
  object Label5: TLabel
    Left = 294
    Top = 288
    Width = 38
    Height = 13
    Caption = 'V Out ='
  end
  object Label6: TLabel
    Left = 296
    Top = 307
    Width = 36
    Height = 13
    Caption = 'I Out ='
  end
  object Label7: TLabel
    Left = 296
    Top = 326
    Width = 30
    Height = 13
    Caption = 'V In ='
  end
  object Label8: TLabel
    Left = 296
    Top = 345
    Width = 44
    Height = 13
    Caption = 'V Term ='
  end
  object LVout: TLabel
    Left = 346
    Top = 288
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LIOut: TLabel
    Left = 346
    Top = 307
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LVIn: TLabel
    Left = 346
    Top = 326
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LVTerm: TLabel
    Left = 346
    Top = 345
    Width = 15
    Height = 13
    Caption = '???'
  end
  object Label9: TLabel
    Left = 438
    Top = 244
    Width = 57
    Height = 13
    Caption = 'Camur II P4'
  end
  object LP4Snr: TLabel
    Left = 451
    Top = 263
    Width = 30
    Height = 13
    Caption = '??????'
  end
  object Label11: TLabel
    Left = 438
    Top = 288
    Width = 23
    Height = 13
    Caption = 'V1 ='
  end
  object Label12: TLabel
    Left = 438
    Top = 307
    Width = 23
    Height = 13
    Caption = 'V2 ='
  end
  object Label13: TLabel
    Left = 438
    Top = 326
    Width = 23
    Height = 13
    Caption = 'V3 ='
  end
  object Label14: TLabel
    Left = 438
    Top = 345
    Width = 23
    Height = 13
    Caption = 'V4 ='
  end
  object LV1: TLabel
    Left = 487
    Top = 288
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LV2: TLabel
    Left = 487
    Top = 307
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LV3: TLabel
    Left = 487
    Top = 326
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LV4: TLabel
    Left = 487
    Top = 345
    Width = 15
    Height = 13
    Caption = '???'
  end
  object Label10: TLabel
    Left = 16
    Top = 373
    Width = 70
    Height = 13
    Caption = 'Stored values:'
  end
  object LStoredValues: TLabel
    Left = 92
    Top = 373
    Width = 15
    Height = 13
    Caption = '???'
  end
  object Label15: TLabel
    Left = 16
    Top = 397
    Width = 59
    Height = 13
    Caption = 'Time Stamp:'
  end
  object LTimeStamp: TLabel
    Left = 92
    Top = 397
    Width = 15
    Height = 13
    Caption = '???'
  end
  object LRecStatus: TLabel
    Left = 120
    Top = 213
    Width = 15
    Height = 13
    Caption = '???'
  end
  object Label16: TLabel
    Left = 576
    Top = 117
    Width = 14
    Height = 13
    Caption = 'mV'
  end
  object Label17: TLabel
    Left = 576
    Top = 148
    Width = 15
    Height = 13
    Caption = 'mA'
  end
  object Label18: TLabel
    Left = 576
    Top = 184
    Width = 6
    Height = 13
    Caption = 'S'
  end
  object LBIChVer: TLabel
    Left = 144
    Top = 24
    Width = 38
    Height = 13
    Caption = 'Ver: ???'
  end
  object BGetBIChSerial: TButton
    Left = 16
    Top = 17
    Width = 89
    Height = 25
    Caption = 'Get BICh Serial'
    TabOrder = 0
    OnClick = BGetBIChSerialClick
  end
  object BGetBIChMode: TButton
    Left = 16
    Top = 48
    Width = 89
    Height = 25
    Caption = 'Get BICh Mode'
    TabOrder = 1
    OnClick = BGetBIChModeClick
  end
  object BStdMode: TButton
    Left = 16
    Top = 79
    Width = 89
    Height = 25
    Caption = 'Set Std Mode'
    TabOrder = 2
    OnClick = BStdModeClick
  end
  object BuCtrlMode: TButton
    Left = 16
    Top = 112
    Width = 89
    Height = 25
    Caption = 'Set UCtrl Mode'
    TabOrder = 3
    OnClick = BuCtrlModeClick
  end
  object BGetTime: TButton
    Left = 201
    Top = 16
    Width = 89
    Height = 25
    Caption = 'Get Time'
    TabOrder = 4
    OnClick = BGetTimeClick
  end
  object BSetTime: TButton
    Left = 201
    Top = 47
    Width = 89
    Height = 25
    Caption = 'Set Time'
    TabOrder = 5
    OnClick = BSetTimeClick
  end
  object DTPSetuCtrlDate: TDateTimePicker
    Left = 296
    Top = 48
    Width = 97
    Height = 21
    Date = 41093.956293877320000000
    Time = 41093.956293877320000000
    TabOrder = 6
  end
  object DTPSetuCtrlTime: TDateTimePicker
    Left = 408
    Top = 48
    Width = 73
    Height = 21
    Date = 41093.957898402780000000
    Time = 41093.957898402780000000
    Kind = dtkTime
    TabOrder = 7
  end
  object BNow: TButton
    Left = 496
    Top = 47
    Width = 41
    Height = 25
    Caption = 'Now'
    TabOrder = 8
    OnClick = BNowClick
  end
  object RGuCtrlPOut: TRadioGroup
    Left = 293
    Top = 112
    Width = 60
    Height = 60
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Output'
    Items.Strings = (
      'On'
      'Off')
    TabOrder = 9
  end
  object RGuCtrlPMode: TRadioGroup
    Left = 361
    Top = 112
    Width = 60
    Height = 60
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Caption = 'Mode'
    Items.Strings = (
      'I'
      'U')
    TabOrder = 10
  end
  object EduCtrlVOut: TEdit
    Left = 473
    Top = 114
    Width = 82
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 11
    Text = '0'
  end
  object EduCtrlIOut: TEdit
    Left = 473
    Top = 147
    Width = 82
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 12
    Text = '0'
  end
  object EduCtrlInterval: TEdit
    Left = 473
    Top = 181
    Width = 82
    Height = 21
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    TabOrder = 13
    Text = '10'
  end
  object BReadFixVolt: TButton
    Left = 201
    Top = 112
    Width = 89
    Height = 25
    Caption = 'Read uCtrl'
    TabOrder = 14
    OnClick = BReadFixVoltClick
  end
  object BSetFixVolt: TButton
    Left = 201
    Top = 143
    Width = 89
    Height = 25
    Caption = 'Set uCtrl'
    TabOrder = 15
    OnClick = BSetFixVoltClick
  end
  object BStartRec: TButton
    Left = 16
    Top = 239
    Width = 89
    Height = 25
    Caption = 'Start Recording'
    TabOrder = 16
    OnClick = BStartRecClick
  end
  object BStopRec: TButton
    Left = 16
    Top = 270
    Width = 89
    Height = 25
    Caption = 'Stop Recording'
    TabOrder = 17
    OnClick = BStopRecClick
  end
  object BGetNodeCount: TButton
    Left = 201
    Top = 208
    Width = 89
    Height = 25
    Caption = 'Get Node Count'
    TabOrder = 18
    OnClick = BGetNodeCountClick
  end
  object BGetNodeInfo1: TButton
    Left = 201
    Top = 239
    Width = 89
    Height = 25
    Caption = 'Node 1 Info'
    TabOrder = 19
    OnClick = BGetNodeInfo1Click
  end
  object BGetNodeInfo2: TButton
    Left = 201
    Top = 270
    Width = 89
    Height = 25
    Caption = 'Node 2 Info'
    TabOrder = 20
    OnClick = BGetNodeInfo2Click
  end
  object BGetData: TButton
    Left = 16
    Top = 301
    Width = 89
    Height = 25
    Caption = 'Get Data'
    TabOrder = 21
    OnClick = BGetDataClick
  end
  object BRecStatus: TButton
    Left = 16
    Top = 208
    Width = 89
    Height = 25
    Caption = 'Recording ?'
    TabOrder = 22
    OnClick = BRecStatusClick
  end
  object ChBStoreData: TCheckBox
    Left = 16
    Top = 344
    Width = 97
    Height = 17
    Caption = 'Store Data'
    TabOrder = 23
  end
end
