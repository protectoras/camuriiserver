//---------------------------------------------------------------------------


// History
//
// Date			Comment							Sign
//  2012-03-04	Converted to Xe2    			Bo H
//  2012-10-16  Ini-file now located in CommonAppData\Camur II
//                                              Bo H

#include <vcl.h>
#include <shlobj.h>
#pragma hdrstop

#include "CIISMainLogger.h"
#include "CIISOnTopMessage.h"
#include "UnitMySQLLogin.h"
#include "CIISBusInterface.h"

#define ReduceWorkingSetPeriod 7200L


// 1 - Link Midas.Lib in your project
// 2 - Declare the external method DllGetDataSnapClassObject:
//
extern "C" __stdcall int32_t DllGetDataSnapClassObject (REFCLSID rclsid, REFIID riid, void** ppv);
// 3 - In startup application, form create or other point execute register Midas dll througt RegisterMidasLib:
//


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

String DBHostName ="localhost";
String DBName = "CIISLogger";
String DBUserName = "root";
String DBPassword = "camur";

String CIIMonitorPort = "5019";
String CIIUserPort = "5020";


TCIISMainLoggerFrm *CIISMainLoggerFrm;

__fastcall TCIISMainLoggerFrm::TCIISMainLoggerFrm(TComponent* Owner)
		: TForm(Owner)
{
		 DatabaseCreated = false;
}


#pragma argsused
void __fastcall TCIISMainLoggerFrm::FormCreate(TObject *Sender)
{

// Register MidasLib in order to NOT use MIDAS.DLL
// Ver 3.2.7
	RegisterMidasLib(DllGetDataSnapClassObject);

	DB = NULL;
	TimerSysCheck->Enabled = false;
	SysClockEnabled = false;
	BBReset->Enabled = false;
	InitFaild = false;
	ReduceWorkingSetCounter = 0;
	KeepDBCounter = 0;


                  // Path to IniFile
	IniFilePath=GetCommonAppFolderPath() + ChangeFileExt(ExtractFileName(Application->ExeName),".Ini");

	TReadCAN = 0;
	TSysClock = 0;
	OnSlowClockTick = 10;
	RemoteClientUnaktive = 30;



  #if Startmode == 1

	StartDebugWin = false;
	BBDebugWin->Visible = false;
	CIISMainLoggerFrm->WindowState = wsMinimized;
	BBStartCIISLoggger->Visible = false;

	DebWin = NULL;

	BBStartCIISLogggerClick( this );

	#elif Startmode == 2

	StartDebugWin = true;
	BBDebugWin->Visible = true;
	BBStartCIISLoggger->Visible = false;

	DebWin = new TDebugWin(this, &ChangeDebugState);
	DebWin->Top = Top + Height;
	DebWin->Left = Left;
	DebWin->Show();

	BBStartCIISLogggerClick( this );

	#elif Startmode == 3

	StartDebugWin = true;
	BBDebugWin->Visible = true;

	DebWin = new TDebugWin(this, &ChangeDebugState);
	DebWin->Top = Top + Height;
	DebWin->Left = Left;
	DebWin->Show();

	BBStartCIISLoggger->Visible = true;

  #endif

	//if( !InitFaild ) TimerSysCheck->Enabled = true;

}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::FormDestroy(TObject *Sender)
{
	TimerSysCheck->Enabled = false;

	if( Prj != NULL )
  {
		Prj->PrepareShutdown();
  }

  delete ClientInt;

  if( DB != NULL)
  {
		DB->CloseCIISDB();
		delete DB;
  }

  delete Prj;
  delete DebWin;
}

void __fastcall TCIISMainLoggerFrm::Debug(String Message)
{
  if( DebWin != NULL ) DebWin->Log(Message);
}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::BBResetClick(TObject *Sender)
{
	LNoOfDetecedNodes->Caption = "0";
  LNoOfNodes->Caption =  "0";
  LNoOfMonitors->Caption =  "0";
  LNoOfMonitorsExt->Caption =  "0";
  LNoOfDecays->Caption =  "0";
  NoOfLPRs->Caption =  "0";
  LNoOfBusInt->Caption =  "0";

  SysClockEnabled = false;

	if( !Prj->ResetCIISBus() )
  {
	#if DebugMain == 1
	Debug("Unable to start CIIS Net")
	#endif
	;
  }
  else
  {
	#if DebugMain == 1
	Debug("Reset CII Bus")
	#endif
	;
	Prj->Log( SystemEvent, CIIEventCode_ServerStart, CIIEventLevel_High, "ServerStart", 0, 0 );

	SClockStart = Now();
	SClockNextEvent = SClockStart;
	SClockEvent = 0;
	TimerSysCheck->Enabled = true;
	SysClockEnabled = true;
  }
}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::TimerSystemClockEvent(TObject *Sender)
{
	TimerSysCheck->Enabled = false;

	TimerStart = Now();

	for( int32_t i = 0; i < BusIntList->Count ; i++ )
	{
		CIISBusInterface = (TCIISBusInterface*)BusIntList->Items[i];
		CIISBusInterface->TimerCanRxEvent(this);
	}

	TAfterReadCAN = Now();
	TReadCAN += ( TAfterReadCAN - TimerStart );

	// 100 ms interval --------------------------------------------------

	if( Now() >= SClockNextEvent )
	{
		SClockEvent++;
		NextEvent_ms = SysClock * (uint64_t)SClockEvent;
		if( SClockEvent == std::numeric_limits<uint32_t>::max() ) //INT_MAX )
		{
			SClockStart += NextEvent_ms * One_mS;
			SClockEvent = 0;
			NextEvent_ms = 0;
		}
		SClockNextEvent = SClockStart + TDateTime(NextEvent_ms * One_mS);

		if( SysClockEnabled )
						Prj->SysClockEvent();

		TAfterSysClock = Now();
		TSysClock += ( TAfterSysClock - TAfterReadCAN );

		//LSocket->TimerSReadEvent();
		//RSocket->TimerSReadEvent();

		// 100 ms interval --------------------------------------------------

		// 1000 ms interval---------------------------------------------------
		OnSlowClockTick--;
		if( OnSlowClockTick == 0 )
		{

			ShRunning->Visible = !ShRunning->Visible;


			OnSlowClockTick = 10;

			LNoOfNodes->Caption = Prj->NodeCount;
			LNoOfDetecedNodes->Caption = Prj->DetectedNodeCount;
			LNoOfNodIniErrors->Caption = Prj->IniErrorCount;
			LNoOfMonitors->Caption = Prj->MonitorCount;
			LNoOfMonitorsExt->Caption = Prj->MonitorExtCount;
			LNoOfDecays->Caption = Prj->DecayCount;
			NoOfLPRs->Caption = Prj->LPRCount;
			LNoOfBusInt->Caption = Prj->USBCANCount;


			LTCANRxValue->Caption = IntToStr( (int32_t)((double)TReadCAN * 24 * 60 * 60 * 100 ));
			LTSysValue->Caption = IntToStr( (int32_t)((double)TSysClock * 24 * 60 * 60 * 100 ));

			TReadCAN = 0;
			TSysClock = 0;

			if( ClientInt->LocalClientConnected ) ShMonitorConnected->Brush->Color = clLime;
			else ShMonitorConnected->Brush->Color = clBtnFace;

			if( ClientInt->RemoteClientConnected ) ShUserConnected->Brush->Color = clLime;
			else ShUserConnected->Brush->Color = clBtnFace;

			if( ClientInt->RemoteClientConnected )
			{
				if( !ClientInt->RemoteClientAkltiv )
				{
					if( --RemoteClientUnaktive == 0 )
					{
						RemoteClientUnaktive = 30;
						ClientInt->DisconnectRemoteClient();
					}
				}
				else
				{
					 RemoteClientUnaktive = 30;
				}
      }

			if( Prj->ShutdownReady ) CIISMainLoggerFrm->Close();

			if( Prj->ReqReset )
			{
				Prj->ReqReset = false;
				BBResetClick( this );
			}

			//uCtrl
			if( Prj->ShowuCtrlInfo ) GBuCtrl->Visible = true;
			else GBuCtrl->Visible = false;

			if( Prj->uCtrlRecordingOn ) ShuCtrlRecordingOn->Brush->Color = clLime;
			else ShuCtrlRecordingOn->Brush->Color = clBtnFace;

			if( Prj->uCtrlReadingData ) ShuCtrlReadingData->Brush->Color = clLime;
			else ShuCtrlReadingData->Brush->Color = clBtnFace;

			if( Prj->uCtrlStoringData ) ShuCtrlStoringData->Brush->Color = clLime;
			else ShuCtrlStoringData->Brush->Color = clBtnFace;

			LuCtrlBytesRead->Caption = IntToStr( Prj->uCtrlBytesRead );

			LuCtrlTime->Caption = CIISDateTimeToStrNomS( Prj->uCtrlTime );

			LuCtrlSampleTimeStamp->Caption = CIISDateTimeToStrNomS( Prj->uCtrlSampleTimeStamp );

			if( Prj->uCtrlPowerIn ) ShuCtrlPowerIn->Brush->Color = clLime;
			else ShuCtrlPowerIn->Brush->Color = clBtnFace;

			if( Prj->uCtrlPowerOn ) ShuCtrlPowerOn->Brush->Color = clLime;
			else ShuCtrlPowerOn->Brush->Color = clBtnFace;

			if( Prj->uCtrlPowerOk ) ShuCtrlPowerOk->Brush->Color = clLime;
			else ShuCtrlPowerOk->Brush->Color = clBtnFace;





			ReduceWorkingSetCounter++;
			if ((ReduceWorkingSetCounter % ReduceWorkingSetPeriod) == 0)
			{
				TrimAppMemorySize();

			}

			if (!SysClockEnabled)
			{
				KeepDBCounter++;
				if ((KeepDBCounter % KeepDBPeriod) == 0)
				{
					// Ver 3.16.0.5
					// Dummy query to keep MySQL Server from going away
					// in case no hardware is connected
					DB->KeepDB();
				}
			}
		}
		// 1000 ms interval---------------------------------------------------
	}

	TimerSysCheck->Enabled = true;
}
//
//
void __fastcall TCIISMainLoggerFrm::TrimAppMemorySize(void)
{
 SetProcessWorkingSetSize(GetCurrentProcess(), -1, -1);
}

void __fastcall TCIISMainLoggerFrm::ChangeDebugState( int32_t DState)
{
  if( DState == 0 )
  {
	DebWin = NULL;
	if( ClientInt != NULL ) ClientInt->SetDebugWin( NULL );
  }
}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::BBStartCIISLogggerClick(TObject *Sender)
{
  int8_t Answer;

	TimerSysCheck->Enabled = false;
  BBReset->Enabled = false;

	#if DebugCIISStart == 1
  Debug( "After BBStartCIISLogggerClick" );
  #endif


  DB = new TCIISDBModule(this);

  #if DebugCIISStart == 1
  Debug( "After DB = new TCIISDBModule(this)" );
  #endif

	TIniFile *pIniFile = new TIniFile(IniFilePath);
	if (pIniFile)
  {
		DBHostName = pIniFile->ReadString("SQL", "HostName", DBHostName);
		DBName = pIniFile->ReadString("SQL", "Database", DBName);
		DBUserName = pIniFile->ReadString("SQL", "User_Name", DBUserName);
		DBPassword = pIniFile->ReadString("SQL", "Password", DBPassword);

		CIIMonitorPort =  pIniFile->ReadString("Communication", "CIIMonitorPort", CIIMonitorPort);
		CIIUserPort =  pIniFile->ReadString("Communication", "CIIUserPort", CIIUserPort);

		delete pIniFile;
	}

	TFormOnTopMessage *OnTopMessage=new TFormOnTopMessage(NULL);
	if (DBHostName.Pos("localhost") || DBHostName.Pos("127.0.0.1")) // Only check MySQL-service if local MySQL - database
	{
		int32_t WaitForMySQL = MYSQL_SERVICE_START_TIMEOUT;
		while (!DB->IsMySQLServiceStarted() && WaitForMySQL)
		{
			OnTopMessage->Show();
			Sleep(1000);
			WaitForMySQL--;
		}
		OnTopMessage->Hide();
		delete OnTopMessage;
	}

  if (!DB->IsMySQLServiceStarted())
	{
		if (Application->MessageBox(L"Local MySQL service is not running! Close application and install MySQL?",Caption.c_str(),MB_YESNO|MB_ICONQUESTION) == IDYES)
		{
			InitFaild = true;
		}
		else
		Application->MessageBox(L"Specify remote MySQL host and credentials",Caption.c_str(),MB_OK|MB_ICONINFORMATION);
	}

	TFormMySQLLogin *FormMySQLLogin=new TFormMySQLLogin(this);
	bool OkToContinue=false;
	while (!OkToContinue)
	{
		if (DB->IsLoginPossible(DBHostName,DBUserName,DBPassword))
		{
			if (DB->DatabaseExists(DBHostName,DBUserName,DBPassword,DBName))
			OkToContinue = true;
			else
			{
				String DBNotFoundMessage="Database:" + DBName + " not found!\nCreate this database? [Yes] \nTry another database name[No]";
				if (Application->MessageBox(DBNotFoundMessage.c_str(),Caption.c_str(),MB_YESNO|MB_ICONQUESTION) == IDYES)
				{
					// InitCIISDB will create the database
					DatabaseCreated = true;
					OkToContinue=true;
				}
			}
		}

		if (!OkToContinue)
		{
			FormMySQLLogin->LabeledEditHostName->Text=DBHostName;
			FormMySQLLogin->LabeledEditUserName->Text=DBUserName;
			FormMySQLLogin->LabeledEditPassword->Text=DBPassword;
			FormMySQLLogin->LabeledEditDbName->Text=DBName;

			Answer = FormMySQLLogin->ShowModal();
			if (Answer == mrYes)
			{
				DBHostName = FormMySQLLogin->LabeledEditHostName->Text;
				DBUserName = FormMySQLLogin->LabeledEditUserName->Text;
				DBPassword = FormMySQLLogin->LabeledEditPassword->Text;
				DBName = FormMySQLLogin->LabeledEditDbName->Text;
			}
			else // mrNo
			{
				InitFaild = true;
				OkToContinue = true;
			}
		}
	}
	delete FormMySQLLogin;

	if( InitFaild ) return;

	pIniFile = new TIniFile(IniFilePath);
	if (pIniFile)
	{
		pIniFile->WriteString("SQL", "HostName",DBHostName);
		pIniFile->WriteString("SQL", "User_Name",DBUserName);
		pIniFile->WriteString("SQL", "Password",DBPassword);
		pIniFile->WriteString("SQL", "Database",DBName);
		delete pIniFile;
  }

  if( DB->InitCIISDB(DBHostName, DBName, CIISLocalServer, DBUserName, DBPassword, DebWin ))
	{

		#if DebugCIISStart == 1
		Debug( "After DB->InitCIISDB" );
		#endif

		Prj = new TCIISProject( DB, DebWin );
		BusIntList = Prj->GetBusIntList();

		SClockStart = Now();
		SClockNextEvent = SClockStart;
		SClockEvent = 0;

		#if DebugCIISStart == 1
		Debug( "After Prj = new TCIISProject" );
		#endif

		ClientInt = new TCIISClientInterfaceLogger( Prj, DebWin, StrToInt( CIIMonitorPort ), StrToInt( CIIUserPort ));
		//LSocket = ClientInt->GetLSocket();
		//RSocket = ClientInt->GetRSocket();

		BBReset->Enabled = true;
		Prj->ServerStatus = CIIDataSync;

		LNoOfDetecedNodes->Caption = "0";
		LNoOfNodes->Caption =  "0";
		LNoOfMonitors->Caption =  "0";
		LNoOfMonitorsExt->Caption =  "0";
		LNoOfDecays->Caption =  "0";
		NoOfLPRs->Caption =  "0";
		LNoOfBusInt->Caption =  "0";

		SysClockEnabled = false;

		if( !Prj->ResetCIISBus() )
		{
			#if DebugMain == 1
			Debug("Unable to start CIIS Net");
			#endif
		}
		else
		{
			Prj->Log( SystemEvent, CIIEventCode_ServerStart, CIIEventLevel_High, "ServerStart", 0, 0 );
			SClockStart = Now();
			SClockNextEvent = SClockStart;
			SClockEvent = 0;
			SysClockEnabled = true;
		}

	}
  else
	{
		InitFaild = true;
	}

	if( !InitFaild ) TimerSysCheck->Enabled = true;
}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::BBDebugWinClick(TObject *Sender)
{
  DebWin = new TDebugWin(this, &ChangeDebugState);
  DebWin->Top = Top + Height;
  DebWin->Left = Left;
  DebWin->Show();

  if( ClientInt != NULL ) ClientInt->SetDebugWin( DebWin );
  if( DB != NULL ) DB->SetDebugWin( DebWin );
  if( Prj!= NULL ) Prj->SetDebugWin( DebWin );
}
#pragma argsused
void __fastcall TCIISMainLoggerFrm::FormShow(TObject *Sender)
{
	CIISMainLoggerFrm->Caption = "Camur II Server Ver " + GetProgVersion();
	if( InitFaild ) Close();

}

#pragma argsused
void __fastcall TCIISMainLoggerFrm::BBuCtrlSyncClick(TObject *Sender)
{
	Prj->uCtrlSyncTime = true;
}
//---------------------------------------------------------------------------

#pragma argsused
void __fastcall TCIISMainLoggerFrm::BBuCtrlREadDataClick(TObject *Sender)
{
	LuCtrlBytesRead->Caption = "0";
	Prj->uCtrlSyncData = true;
}
//---------------------------------------------------------------------------


