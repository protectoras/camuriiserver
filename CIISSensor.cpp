//---------------------------------------------------------------------------
// History
//
// Date			Comment							        Sign
// 2012-03-04	Converted to Xe2    			        Bo H
// 2012-11-26   Removed new/delete of local variables
// 2013-02-25   ClearLastValues                         Bo H
// 2015-02-08   Converted to Xe7, Renamed variable SensorMoved
//				because of name-collision with event-code


#pragma hdrstop

#include "CIISSensor.h"

#include "CIISProject.h"
#include "CIISController.h"
#include "math.h"
#include "CIISCommon.h"
#include "CIISBusInterface.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
/*
If one BI in system with many BI is removed and system restarted:
Check CIISBICh != NULL in function that can be cald for not connected sensors
if( SensorRec->SensorConnected ) or if( CIISBICh != NULL )
*/
__fastcall TCIISSensor::TCIISSensor(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{

  CIISObjType = CIISSensors;

  P_Zone = (TCIISZone*)CIISParent;
  P_Ctrl = (TCIISController*)P_Zone->CIISParent;
  P_Prj = (TCIISProject*)P_Ctrl->CIISParent;

	CIISBICh = NULL;

  SensorRec = SetSensorRec;

	UpdateLastValueRunning = false;
	NodeDetected = false;

	SensorIniRunning = false;
	FIniErrorCount = 0;
	NIState = NI_ZoneAdr;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;

  OnSlowClockTick = 0;
  RequestShutdownTag = 201;

  CTCh = 1;

	FIniDecaySample = false;
	FRunDistDecay = false;

	LogCanDupTag = true;
	LogCanMissingTag = true;
	MissingTagDetected = false;
	DupCount = 5;

	SensorLastValueRequest = false;

	// uCtrl

	FuCtrlZone = false;

	#if DebugCIISStart == 1

	Debug( "Sensor created: " + IntToStr( SensorRec->SensorCanAdr ) + "/" + IntToStr( SensorRec->SensorSerialNo ));

	#endif

}

__fastcall TCIISSensor::~TCIISSensor()
{
 delete SensorRec;
}

void __fastcall TCIISSensor::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Sensor: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase  || P->UserLevel == ULModify )
	  {
		WriteRec( P, O );
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	if( ThisRec( P ) )
	{
	  if( P->UserLevel == ULErase )
	  {
		if( SensorRec->ZoneNo == 1 &&  DB->DeleteSensorRec( SensorRec ) )
		{
			DB->ApplyUpdatesSensor();
		  P_Prj->Log( ClientCom, CIIEventCode_SensorTabChanged, CIIEventLevel_High, "", 0, 0 );
		  P->CommandMode = CIISDeleteSensor;
		  P->CIISObj = this;
		  O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISMsg_DeleteError;
	  }
	  else
	  {
		O->MessageCode = CIISMsg_UserLevelError;
	  }
	}
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}

#pragma argsused
void __fastcall TCIISSensor::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "Sensor: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISSensor::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == SensorRec->SensorSerialNo;
}

void __fastcall TCIISSensor::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->CommandMode == CIISUndef )
  {
	if( P->ArgInc( 200 ) )
	{
	  if( P->GetArg( 200 ) == "GetFirstToEnd" ) P->CommandMode = CIISRecAdd;
	  else if( P->GetArg( 200 ) == "GetNextToEnd" && P->ArgInc(1)  ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetLastValFirstToEnd") P->CommandMode = CIISRecAddLastVal;
	  else if( P->GetArg( 200 ) == "GetLastValNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetAlarmFirstToEnd") P->CommandMode = CIISRecAddAlarm;
	  else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" && P->ArgInc(1) ) P->CommandMode = CIISRecSearch;
	  else if( P->GetArg( 200 ) == "GetRecordCount")  P->CommandMode = CIISRecCount;
	}
  }

  if( P->CommandMode == CIISRecSearch )
  {
	if( ThisRec( P ) )
	{
	  if( P->GetArg( 200 ) == "GetNextToEnd" ) P->CommandMode = CIISRecAdd;
	  else if( P->GetArg( 200 ) == "GetLastValNextToEnd" ) P->CommandMode = CIISRecAddLastVal;
	  else if( P->GetArg( 200 ) == "GetAlarmNextToEnd" ) P->CommandMode = CIISRecAddAlarm;
	  O->MessageCode = CIISMsg_Ok;
	}
	else O->MessageCode = CIISMsg_RecNotFound;
  }

	else if( P->CommandMode == CIISRecAdd )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( SensorRec->SensorSerialNo ) + "\r\n" +
	"2=" + IntToStr( SensorRec->SensorCanAdr ) + "\r\n" +
	"3=" + IntToStr( SensorRec->SensorStatus ) + "\r\n" +
	"4=" + SensorRec->SensorName + "\r\n" +
	"5=" + IntToStr( SensorRec->SensorType ) + "\r\n" +
	"6=" + CIISFloatToStr( SensorRec->SensorLastValue ) + "\r\n" +
	"7=" + CIISFloatToStr( SensorRec->PreCommOff ) + "\r\n" +
	"8=" + CIISFloatToStr( SensorRec->PreCommLPR ) + "\r\n" +
	"9=" + CIISFloatToStr( SensorRec->SensorArea ) + "\r\n" +
	"10=" + CIISFloatToStr( SensorRec->SensorGain ) + "\r\n" +
	"11=" + CIISFloatToStr( SensorRec->SensorOffset ) + "\r\n" +
	"12=" + SensorRec->SensorUnit + "\r\n" +
	"13=" + CIISFloatToStr( SensorRec->SensorLow ) + "\r\n" +
	"14=" + CIISFloatToStr( SensorRec->SensorHigh ) + "\r\n" +
	"15=" + CIISBoolToStr( SensorRec->SensorAlarmEnabled ) + "\r\n" +
	"16=" + IntToStr( SensorRec->SensorAlarmStatus ) + "\r\n" +
	"17=" + CIISFloatToStr( SensorRec->SensorXPos ) + "\r\n" +
	"18=" + CIISFloatToStr( SensorRec->SensorYPos ) + "\r\n" +
	"19=" + CIISFloatToStr( SensorRec->SensorZPos ) + "\r\n" +
	"20=" + IntToStr( SensorRec->SensorSectionNo ) + "\r\n" +
	"21=" + IntToStr( SensorRec->RequestStatus ) + "\r\n" +
	"22=" + CIISFloatToStr( SensorRec->SensorIShunt ) + "\r\n" +
	"23=" + IntToStr( SensorRec->SensorLPRStep ) + "\r\n" +
	"24=" + CIISBoolToStr( SensorRec->SensorConnected ) + "\r\n" +
	"25=" + IntToStr( SensorRec->SensorWarmUp ) + "\r\n" +
	"26=" + CIISBoolToStr( SensorRec->SensorTemp ) + "\r\n" +
	"27=" + CIISFloatToStr( SensorRec->SensorLastTemp ) + "\r\n" +
	"28=" + IntToStr( SensorRec->SensorVerMajor ) + "\r\n" +
	"29=" + IntToStr( SensorRec->SensorVerMinor ) + "\r\n" +
	"30=" + CIISFloatToStr( SensorRec->SensorIShunt1 ) + "\r\n" +
	"31=" + CIISFloatToStr( SensorRec->SensorIShunt2 ) + "\r\n" +
	"32=" + CIISFloatToStr( SensorRec->SensorIShunt3 ) + "\r\n" +
	"33=" + CIISFloatToStr( SensorRec->SensorLastValue2 ) + "\r\n" +
	"34=" + CIISFloatToStr( SensorRec->SensorGain2 ) + "\r\n" +
	"35=" + CIISFloatToStr( SensorRec->SensorOffset2 ) + "\r\n" +
	"36=" + SensorRec->SensorUnit2 + "\r\n" +
	"37=" + CIISFloatToStr( SensorRec->SensorLow2 ) + "\r\n" +
	"38=" + CIISFloatToStr( SensorRec->SensorHigh2 ) + "\r\n" +
	"39=" + IntToStr( SensorRec->SensorAlarmStatus2 ) + "\r\n" +
	"40=" + CIISBoolToStr( SensorRec->PowerShutdownEnabled ) + "\r\n" +
	"41=" + IntToStr( SensorRec->SensorChannelCount ) + "\r\n" +
	"50=" + IntToStr( SensorRec->ZoneNo ) + "\r\n"
	"51=" + SensorRec->SensorName2 + "\r\n" +
	"52=" + SensorRec->SensorName3 + "\r\n" +
	"53=" + SensorRec->SensorName4 + "\r\n" +
	"54=" + CIISFloatToStr( SensorRec->SensorGain3 ) + "\r\n" +
	"55=" + CIISFloatToStr( SensorRec->SensorOffset3 ) + "\r\n" +
	"56=" + SensorRec->SensorUnit3 + "\r\n" +
	"57=" + CIISFloatToStr( SensorRec->SensorLow3 ) + "\r\n" +
	"58=" + CIISFloatToStr( SensorRec->SensorHigh3 ) + "\r\n" +
	"59=" + IntToStr( SensorRec->SensorAlarmStatus3 ) + "\r\n" +
	"60=" + CIISFloatToStr( SensorRec->SensorGain4 ) + "\r\n" +
	"61=" + CIISFloatToStr( SensorRec->SensorOffset4 ) + "\r\n" +
	"62=" + SensorRec->SensorUnit4 + "\r\n" +
	"63=" + CIISFloatToStr( SensorRec->SensorLow4 ) + "\r\n" +
	"64=" + CIISFloatToStr( SensorRec->SensorHigh4 ) + "\r\n" +
	"65=" + IntToStr( SensorRec->SensorAlarmStatus4 ) + "\r\n" +
	"66=" + IntToStr( SensorRec->SensorLPRIRange ) + "\r\n" +
	"67=" + CIISBoolToStr( SensorRec->VerNotSupported ) + "\r\n" +
	"68=" + IntToStr( SensorRec->DisabledChannels ) + "\r\n" +
	"69=" + CIISFloatToStr( SensorRec->PreCommOff2 ) + "\r\n" +
	"70=" + CIISFloatToStr( SensorRec->PreCommOff3 ) + "\r\n" +
	"71=" + CIISFloatToStr( SensorRec->PreCommOff4 ) + "\r\n" +
	"72=" + CIISBoolToStr( SensorRec->CANAlarmEnabled ) + "\r\n" +
	"73=" + IntToStr( SensorRec->CANAlarmStatus ) + "\r\n";


	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }

  else if( P->CommandMode == CIISRecAddLastVal )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( SensorRec->SensorSerialNo ) + "\r\n" +
	"6=" + CIISFloatToStr( SensorRec->SensorLastValue ) + "\r\n" +
	"27=" + CIISFloatToStr( SensorRec->SensorLastTemp ) + "\r\n";

	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }

  else if( P->CommandMode == CIISRecAddAlarm )
  {
	O->MessageData = O->MessageData +
	"1=" + IntToStr( SensorRec->SensorSerialNo ) + "\r\n" +
	"16=" + IntToStr( SensorRec->SensorAlarmStatus ) + "\r\n"
	"39=" + IntToStr( SensorRec->SensorAlarmStatus2 ) + "\r\n";

	O->MessageCode = CIISMsg_Ok;
	if( O->MessageData.Length() > OutMessageLimit ) P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetSensorRecCount() ) + "\r\n";;
	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else O->MessageCode = CIISMsg_UnknownCommand;
}

void __fastcall TCIISSensor::ClearLastValues(void)
{
    DB->ClearSensorMiscValues(SerialNo);
}
#pragma argsused
void __fastcall TCIISSensor::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  bool SensorMovedToOtherZone = false;

  if( P->ArgInc(200) )
	{
    String Command = P->GetArg(200);
		if( Command == "Update LastValue" )
		{
				UpdateLastValues();
				O->MessageCode = CIISMsg_Ok;
			}
			else if (Command == "Clear LastValues")
			{
			ClearLastValues();
			O->MessageCode = CIISMsg_Ok;
		}
		}
		else if( DB->LocateSensorRec( SensorRec ) )
		{
		if( P->ArgInc(2) ) SensorRec->SensorName = P->GetArg(2);
		if( P->ArgInc(3) ) SensorRec->PreCommOff = CIISStrToFloat(P->GetArg(3));
		if( P->ArgInc(4) ) SensorRec->PreCommLPR = CIISStrToFloat(P->GetArg(4));
		if( P->ArgInc(5) ) SensorRec->SensorArea = CIISStrToFloat(P->GetArg(5));
		if( P->ArgInc(6) ) SensorRec->SensorGain = CIISStrToFloat(P->GetArg(6));
		if( P->ArgInc(7) ) SensorRec->SensorOffset = CIISStrToFloat(P->GetArg(7));
		if( P->ArgInc(8) ) SensorRec->SensorLow = CIISStrToFloat(P->GetArg(8));
		if( P->ArgInc(9) ) SensorRec->SensorHigh = CIISStrToFloat(P->GetArg(9));
		if( P->ArgInc(10) ) SensorRec->SensorXPos = CIISStrToFloat(P->GetArg(10));
		if( P->ArgInc(11) ) SensorRec->SensorYPos = CIISStrToFloat(P->GetArg(11));
		if( P->ArgInc(12) ) SensorRec->SensorZPos = CIISStrToFloat(P->GetArg(12));
		if( P->ArgInc(13) )
		{
			if( P->GetArg(13) == "True" )
			{
			SensorRec->SensorAlarmEnabled = true;
			P_Prj->Log( ClientCom, CIIEventCode_AlarmEnable, CIIEventLevel_High, SensorRec->SensorSerialNo, 0, 0 );
			}
			else
			{
			SensorRec->SensorAlarmEnabled = false;
			//SensorRec->SensorAlarmStatus = 0;
			P_Prj->Log( ClientCom, CIIEventCode_AlarmDisable, CIIEventLevel_High, SensorRec->SensorSerialNo, 0, 0 );
			}
		}
		if( P->ArgInc(14) ) SensorRec->SensorSectionNo = StrToInt(P->GetArg(14));
		if( P->ArgInc(15) ) SensorRec->SensorUnit = P->GetArg(15);
		if( P->ArgInc(16) ) SensorRec->SensorWarmUp = StrToInt(P->GetArg(16));
		if( P->ArgInc(17) ) SensorRec->SensorTemp = CIISStrToBool(P->GetArg(17));
		if( P->ArgInc(18) ) SensorRec->SensorGain2 = CIISStrToFloat(P->GetArg(18));
		if( P->ArgInc(19) ) SensorRec->SensorOffset2 = CIISStrToFloat(P->GetArg(19));
		if( P->ArgInc(20) ) SensorRec->SensorLow2 = CIISStrToFloat(P->GetArg(20));
		if( P->ArgInc(21) ) SensorRec->SensorHigh2 = CIISStrToFloat(P->GetArg(21));
		if( P->ArgInc(22) ) SensorRec->SensorUnit2 = P->GetArg(22);
		if( P->ArgInc(23) )
		{
			if( P->GetArg(23) == "True" )
			{
			SensorRec->PowerShutdownEnabled = true;
			}
			else
			{
			SensorRec->PowerShutdownEnabled = false;
			P_Prj->PowerShutdown = false;
			}

		}
							// Ver 3.15.0.4
		if( P->ArgInc(24) ) SensorRec->SensorChannelCount = StrToInt(P->GetArg(24));
							// Ver 3.16.0.0
		if( P->ArgInc(25) ) SensorRec->SensorStatus = StrToInt(P->GetArg(25));

		if( P->ArgInc(26) ) SensorRec->SensorName2 = P->GetArg(26);
		if( P->ArgInc(27) ) SensorRec->SensorName3 = P->GetArg(27);
		if( P->ArgInc(28) ) SensorRec->SensorName4 = P->GetArg(28);

		if( P->ArgInc(29) ) SensorRec->SensorGain3 = CIISStrToFloat(P->GetArg(29));
		if( P->ArgInc(30) ) SensorRec->SensorOffset3 = CIISStrToFloat(P->GetArg(30));
		if( P->ArgInc(31) ) SensorRec->SensorLow3 = CIISStrToFloat(P->GetArg(31));
		if( P->ArgInc(32) ) SensorRec->SensorHigh3 = CIISStrToFloat(P->GetArg(32));
		if( P->ArgInc(33) ) SensorRec->SensorUnit3 = P->GetArg(33);

		if( P->ArgInc(34) ) SensorRec->SensorGain4 = CIISStrToFloat(P->GetArg(34));
		if( P->ArgInc(35) ) SensorRec->SensorOffset4 = CIISStrToFloat(P->GetArg(35));
		if( P->ArgInc(36) ) SensorRec->SensorLow4 = CIISStrToFloat(P->GetArg(36));
		if( P->ArgInc(37) ) SensorRec->SensorHigh4 = CIISStrToFloat(P->GetArg(37));
		if( P->ArgInc(38) ) SensorRec->SensorUnit4 = P->GetArg(38);
		if( P->ArgInc(39) ) SensorRec->SensorLPRIRange = StrToInt(P->GetArg(39));
		if( P->ArgInc(40) ) SensorRec->DisabledChannels = StrToInt(P->GetArg(40));
		if( P->ArgInc(41) ) SensorRec->PreCommOff2 = CIISStrToFloat(P->GetArg(41));
		if( P->ArgInc(42) ) SensorRec->PreCommOff3 = CIISStrToFloat(P->GetArg(42));
		if( P->ArgInc(43) ) SensorRec->PreCommOff4 = CIISStrToFloat(P->GetArg(43));
		if( P->ArgInc(44) ) SensorRec->CANAlarmEnabled = CIISStrToBool(P->GetArg(44));
		if( P->ArgInc(45) ) SensorRec->CANAlarmStatus = StrToInt(P->GetArg(45));

		if( P->ArgInc(50) )
		{
			if( SensorRec->ZoneNo != P->GetArg(50) )
			{
				SensorMovedToOtherZone = true;
				SensorRec->ZoneNo = StrToInt(P->GetArg(50));
			}
		}

		if( CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

		if( DB->SetSensorRec( SensorRec ) )
		{
			DB->ApplyUpdatesSensor();
			P_Prj->Log( ClientCom, CIIEventCode_SensorTabChanged, CIIEventLevel_High, "", 0, 0 );
			//if( CheckAlarm() ) P_Prj->AlarmStatusChanged = true;
			O->MessageCode = CIISMsg_Ok;
		}
		else O->MessageCode = CIISDBError;
	}
  else O->MessageCode = CIISMsg_RecNotFound;

  if( SensorMovedToOtherZone )
  {
		P_Prj->Log( ClientCom, CIIEventCode_SensorMoved, CIIEventLevel_High, SensorRec->SensorSerialNo, 0, 0 );
		P->CommandMode = CIISSensorMoved;
		P->CIISObj = this;
  }
  else P->CommandMode = CIISCmdReady;

  if( P->ArgInc(16) )
  {
		SensorIniState();
  }

}

#pragma argsused
void __fastcall TCIISSensor::OnSysClockTick( TDateTime TickTime )
{
	if( SensorIniRunning ) SM_SensorIni();
	else if( SensorIniStateRunning ) SM_SensorIniState();
	else if( UpdateLastValueRunning ) SM_UpdateLastValue();
}

void _fastcall TCIISSensor::SetZoneCanAdr( int32_t ZoneCanAdr )
{
  SensorRec->ZoneCanAdr = ZoneCanAdr;
  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
	}
  if( CIISBICh != NULL )  CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
}

  //CIISBusInt

void __fastcall TCIISSensor::ResetCIISBus()
{
	CIISBICh = NULL;
	SensorRec->SensorCanAdr = 0;
	SensorRec->SensorStatus = 1;
	SensorRec->SensorConnected = false;
	NodeDetected = false;
	SensorIniRunning = false;
	FIniDecaySample = false;
	FRunDistDecay = false;

	SensorLastValueRequest = false;
	SensorLastValueExtRequest = false;
	SensorLastValueTempRequest = false;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
  }
}

bool __fastcall TCIISSensor::NodeIniReady()
{
  return !SensorIniRunning;
}

void __fastcall TCIISSensor::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{
	if( ( BPIn->MessageCommand == MSG_TX_IDENTIFY ) && ( BPIn->SerNo == SensorRec->SensorSerialNo )) SensorIni( BPIn );
	else if(( CIISBICh == BPIn->GetBIChannel() ) && ( BPIn->CANAdr == SensorRec->SensorCanAdr ))
	{
		if( SensorRec->VerNotSupported )
		{
			switch (BPIn->MessageCommand)
			{
			case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )
				break;

			case MSG_TX_SAMPLEEXTCH: // Reqest Sample ( Monitor, LastValue or Decay )
				break;

			case MSG_TX_TEMP: // Reqest Sample ( Monitor, LastValue or Decay )
				break;

			case MSG_TX_LPR:
				break;

			case MSG_TX_LPREND:
				SensorLPREnd( BPIn );
				break;

			case MSG_TX_CAPABILITY:
				SensorIniCapability( BPIn );
				break;

			case MSG_TX_RANGE:
				break;

			case MSG_TX_SHUNTS:
				break;

			case MSG_TX_VERSION:
				SensorIniVersion( BPIn );
				break;

			default:
				break;
			}
		}
		else
		{
			switch (BPIn->MessageCommand)
			{
			case MSG_TX_SAMPLE: // Reqest Sample ( Monitor, LastValue or Decay )

				if( FIniDecaySample )
				{
					if( FRunDistDecay )
					{
						if( BPIn->Tag == 0 ) IniDecayTime = BPIn->SampleTimeStamp;
						else if( BPIn->Tag == 1 ) IniDecayTime += OneSecond + P_Ctrl->DecayDelay * One_mS;
						else IniDecayTime += P_Ctrl->DecaySampInterval2 * OneSecond;
					}

					SensorSample( BPIn );
				}
				else if( BPIn->Tag == 0 ) // LastValue
				{
					if( SensorLastValueRequest )
					{
						BPIn->SampleTimeStamp = SampleTimeStamp;
						SensorLastValueRequest = false;
					}

					SensorSample( BPIn );
				}
				else if(BPIn->RecType == RT_LPR)  // First sample of LPR is sent as MSG_TX_SAMPLE
				{
					//SensorLPRSample( BPIn );
					//SensorSample( BPIn );
				}
				else if(( BPIn->Tag < 201 ) && ( BPIn->Tag == LastStoredTag ))
				{
					if( LogCanDupTag ) P_Prj->Log(CANCom, CIIEventCode_CANDupTag, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, BPIn->Tag);
					LogCanDupTag = false;
					DupCount = 5;
				}
				else  // LastValue, Monitor, Decay .... CT, MRE, CW, Ladder nodes
				{
					if( SensorLastValueRequest )
					{
						BPIn->SampleTimeStamp = SampleTimeStamp;
						SensorLastValueRequest = false;
					}
					LastStoredTag = BPIn->Tag;
					if( !LogCanDupTag )
					{
						DupCount--;
						if( DupCount == 0 ) LogCanDupTag = true;
					}
					SensorSample( BPIn );
				}
				break;

			case MSG_TX_SAMPLEEXTCH: // Reqest Sample ( Monitor, LastValue or Decay )
				if( SensorLastValueExtRequest )
				{
					BPIn->SampleTimeStamp = SampleTimeStamp;
					SensorLastValueExtRequest = false;
				}
				SensorSampleExt( BPIn );
				break;

			case MSG_TX_TEMP: // Reqest Sample ( Monitor, LastValue or Decay )
				if( SensorLastValueTempRequest )
				{
					BPIn->SampleTimeStamp = SampleTimeStamp;
					SensorLastValueTempRequest = false;
				}
				SensorSampleTemp( BPIn );
				break;

			case MSG_TX_LPR:
				SensorLPRSample( BPIn );
				break;

/*
  			case MSG_TX_LPR_U:
  				SensorLPRSample_U( BPIn );
  				break;

  			case MSG_TX_LPR_I:
  				SensorLPRSample_I( BPIn );
					break;
*/

			//case MSG_TX_LPREND_CIII:
			case MSG_TX_LPREND:
				SensorLPREnd( BPIn );
				break;

			case MSG_TX_CAPABILITY:
				SensorIniCapability( BPIn );
				break;

			case MSG_TX_RANGE:
				SensorIniCalibValues( BPIn );
				break;

			case MSG_TX_SHUNTS:
				SensorIniCalibValues( BPIn );
				break;

			case MSG_TX_VERSION:
				SensorIniVersion( BPIn );
				break;

			default:
				break;
			}
		}
	}
}

#pragma argsused
void __fastcall TCIISSensor::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISSensor::SensorIni( TCIISBusPacket *BPIn )
{
	NIState = NI_Accept;
	IniRetrys = NoOffIniRetrys;
	T = 0;
	TNextEvent = 0;
	FIniErrorCount = 0;
	SensorIniRunning = true;

	CIISBICh = (TCIISBIChannel*) BPIn->GetBIChannel();

  BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	SM_SensorIni();
}

void __fastcall TCIISSensor::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
			P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
		TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ReqCapability;
		break;

	  case NI_ReqCapability :
		CIISBICh->RequestCapability( SensorRec->SensorCanAdr );
		CapabilityRecived = false;
		TNextEvent = TNextEvent + FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCapability;
		break;

	  case NI_WaitForCapability :
		if( CapabilityRecived )
		{
		   NIState = NI_Ready;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_Accept;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_Ready :
		SensorIniRunning = false;
		SensorRec->SensorConnected = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec );
		}
		break;

	  case NI_TimeOut :
		SensorIniRunning = false;
		SensorRec->SensorConnected = false;
		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec );
		}
		break;

	  default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISSensor::SensorIniCapability( TCIISBusPacket *BPIn )
{
  CapabilityRecived = true;
	SensorRec->SensorStatus = 0;
  SensorRec->SensorType = BPIn->NodeCap;
  BPIn->CIISObj = this;
	BPIn->MsgMode = CIISBus_UpdateNode;
  BPIn->ParseMode = CIISParseFinalize;
}

void __fastcall TCIISSensor::SensorIniVersion( TCIISBusPacket *BPIn )
{
  if( VerRecived )
  {
		P_Prj->Log(CANCom, CIIEventCode_DupSNR, CIIEventLevel_High, "BI:" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( SensorRec->SensorCanAdr ) + "/" + IntToStr( SensorRec->SensorSerialNo ), SensorRec->SensorSerialNo, BPIn->Tag);
  }


  VerRecived = true;
  SensorRec->SensorVerMinor = BPIn->Byte4;
  SensorRec->SensorVerMajor = BPIn->Byte3;
  SensorRec->SensorStatus = 0;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
  #if DebugCIISBusInt == 1
  //Debug( "SensorVersion recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) + " = " + IntToStr( SensorRec->SensorVerMajor) + "." + IntToStr( SensorRec->SensorVerMinor));

  Debug( "RX BI" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( SensorRec->SensorCanAdr ) + "/" + IntToStr( SensorRec->SensorSerialNo ) +
			   " Ver = " + IntToStr( SensorRec->SensorVerMajor) + "." + IntToStr( SensorRec->SensorVerMinor));

  #endif
}

#pragma argsused
void __fastcall TCIISSensor::SensorIniCalibValues( TCIISBusPacket *BPIn )
{

}

void __fastcall TCIISSensor::SetDefaultValues()
{
  SensorRec->SensorName = IntToStr( SensorRec->SensorSerialNo );

  SensorRec->SensorStatus = 0;
  SensorRec->RequestStatus = -1;
	SensorRec->SensorLPRStep = 0;
	SensorRec->SensorAlarmEnabled = false;
  SensorRec->SensorAlarmStatus = 0;
  SensorRec->SensorLow = 0;
  SensorRec->SensorHigh = 0;
  SensorRec->SensorGain = 1;
  SensorRec->SensorOffset = 0;
  SensorRec->SensorUnit = "mV";
  SensorRec->SensorXPos = 0;
  SensorRec->SensorYPos = 0;
  SensorRec->SensorZPos = 0;
  SensorRec->SensorArea = 0;
  SensorRec->SensorConnected = true;
  SensorRec->SensorWarmUp = 0;
  SensorRec->SensorTemp = false;
  SensorRec->SensorVerMajor = 0;
  SensorRec->SensorVerMinor = 0;
  SensorRec->SensorAlarmStatus2 = 0;
  SensorRec->SensorLow2 = 0;
  SensorRec->SensorHigh2 = 0;
  SensorRec->SensorGain2 = 1;
  SensorRec->SensorOffset2 = 0;
  SensorRec->SensorUnit2= "mV";
  SensorRec->PreCommOff = 0;
  SensorRec->PreCommLPR = 0;
  SensorRec->PowerShutdownEnabled = false;

  SensorRec->SensorLow3 = 0;
  SensorRec->SensorHigh3 = 0;
  SensorRec->SensorGain3 = 1;
  SensorRec->SensorOffset3 = 0;
  SensorRec->SensorAlarmStatus3 = 0;

  SensorRec->SensorLow4 = 0;
  SensorRec->SensorHigh4 = 0;
  SensorRec->SensorGain4 = 1;
  SensorRec->SensorOffset4 = 0;
	SensorRec->SensorAlarmStatus4 = 0;
	SensorRec->SensorLPRIRange = 2;
	SensorRec->VerNotSupported = true;
	SensorRec->DisabledChannels = 0;

	SensorRec->PreCommOff2 = 0;
	SensorRec->PreCommOff3 = 0;
	SensorRec->PreCommOff4 = 0;

	SensorRec->CANAlarmEnabled = true;
	SensorRec->CANAlarmStatus = 0;

	SetDefaultValuesSubType();

	DB->SetSensorRec( SensorRec );
}

void __fastcall TCIISSensor::SetDefaultValuesSubType()

{

}

void __fastcall TCIISSensor::UpdateLastValues()
{
  if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
  else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;
			T2 = 0;
			T2NextEvent = ApplyUpdDelay;
		}
	}
}

void __fastcall TCIISSensor::UpdateLastTempValues()
{

}

void __fastcall TCIISSensor::SM_UpdateLastValue()
{
  if( T2 >= T2NextEvent)
  {
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesSensor();
		DB->ApplyUpdatesPrj();
		UpdateLastValueRunning = false;
  }
  T2 += SysClock;
}

#pragma argsused
void __fastcall TCIISSensor::SensorWarmUp( bool On )
{
	;
}

void __fastcall TCIISSensor::SensorIniState()
{
	;
}

void __fastcall TCIISSensor::SM_SensorIniState()
{
	;
}

bool __fastcall TCIISSensor::SensorLPRIni()
{
	SensorRec->RequestStatus = 0;
	SensorRec->SensorLPRStep = 0;
	return false;
}

void __fastcall TCIISSensor::LPRSelectRange( int32_t Range )
{
	;
}


void __fastcall TCIISSensor::SensorLPREnd( TCIISBusPacket *BPIn )
{
	if( CIISBICh != NULL ) CIISBICh->SendClrLPR( SensorRec->SensorCanAdr );

	if( SensorRec->RequestStatus != 0 )
	{
		SensorRec->RequestStatus = 0;

		DB->ApplyUpdatesLPRValue();

		int32_t Code;
		TCIISCorrRate *CR;
		CR = new TCIISCorrRate( DB, DW );

		Code = CR->CalcCorrRate(BPIn->RecordingNo, SensorRec->SensorSerialNo, "C" + IntToStr(CTCh) );

		CIISCalcValueRec CVRec;

		CVRec.DateTimeStamp = Now();
		CVRec.RecNo = BPIn->RecordingNo;
		CVRec.SensorSerialNo = SensorRec->SensorSerialNo;
		CVRec.ValueType = "C" + IntToStr(CTCh) + "I";
		CVRec.ValueUnit = "mA";
		CVRec.Value = CR->ICorr;
		CVRec.Code = Code;

		DB->AppendCalcValueRec(&CVRec );

		CVRec.RecNo = BPIn->RecordingNo;
		CVRec.SensorSerialNo = SensorRec->SensorSerialNo;
		CVRec.ValueType = "C" + IntToStr(CTCh) + "S";
		CVRec.ValueUnit = "mA";
		CVRec.Value = CR->ISigma;
		CVRec.Code = Code;

		DB->AppendCalcValueRec( &CVRec );
		DB->ApplyUpdatesCalcValue();

		if( BPIn->MonitorExtended )
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = Now();
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->MonitorExtendedRecNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "C" + IntToStr(CTCh) + "I";
			MValRec.ValueUnit = "mA";
			MValRec.RawValue = CR->ICorr;
			MValRec.Value = CR->ICorr;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}

		delete CR;
	}



	BPIn->ParseMode = CIISParseFinalize;
}


void __fastcall TCIISSensor::SensorSample( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor::SensorSampleExt( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor::SensorLPRSample( TCIISBusPacket *BPIn )
{
	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;
}

/*
  void __fastcall TCIISSensor::SensorLPRSample_U( TCIISBusPacket *BPIn )
  {
  	BPIn->MsgMode = CIISBus_MsgReady;
  	BPIn->ParseMode = CIISParseReady;
  }
  void __fastcall TCIISSensor::SensorLPRSample_I( TCIISBusPacket *BPIn )
  {
  	BPIn->MsgMode = CIISBus_MsgReady;
  	BPIn->ParseMode = CIISParseReady;
  }
*/

bool __fastcall TCIISSensor::CheckAlarm()
{
int32_t AlarmStatus1;
bool StatusChanged;

  //AlarmCheck  ( on scaled values )

	StatusChanged = false;
  AlarmStatus1 = SensorRec->SensorAlarmStatus;

	if( SensorRec->SensorAlarmEnabled == true )
  {
		if( ( SensorRec->SensorLastValue < SensorRec->SensorLow ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorHigh ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorLow ) && ( SensorRec->SensorLastValue < SensorRec->SensorHigh ) && ( AlarmStatus1 == 1 ) )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
	} // AlarmCheck
	else // SensorAlarmEnabled == false
	{
		if( AlarmStatus1 != 0 )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
		}
	}

	if( SensorRec->CANAlarmEnabled )
	{
		if( MissingTagDetected && ( SensorRec->CANAlarmStatus == 0 ))
		{
			SensorRec->CANAlarmStatus = 1;
			StatusChanged = true;
		}
	}
	else
	{
		if( SensorRec->CANAlarmStatus == 1 )
		{
			SensorRec->CANAlarmStatus = 0;
			MissingTagDetected = false;
			StatusChanged = true;
		}
	}

	if( StatusChanged == true )
  {
		SensorRec->SensorAlarmStatus = AlarmStatus1;
  }

  #if DebugCIISAlarm == 1
  Debug( "CheckAlarm Sensor StatusChanged = " + BoolToStr( StatusChanged ) );
  #endif

  return StatusChanged;
}

void __fastcall TCIISSensor::ResetCANAlarm()
{
	if( SensorRec->CANAlarmStatus == 1 )
	{
		SensorRec->CANAlarmStatus = 0;
		MissingTagDetected = false;
		P_Prj->AlarmStatusChanged = true;
	}
}

void __fastcall TCIISSensor::CheckSampleRecived( int32_t RequestedTag )
{
	if( LastStoredTag != RequestedTag )
	{
		MissingTagDetected = true;
		if( LogCanMissingTag )
		{
			P_Prj->Log(CANCom, CIIEventCode_CANMissingTag, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, RequestedTag);
			LogCanMissingTag = false;
		}
		if( CheckAlarm() )
		{
			P_Prj->AlarmStatusChanged = true;
			if( DB->LocateSensorRec( SensorRec ) ) DB->SetSensorRec( SensorRec );
		}

	}
	else if( !LogCanMissingTag ) //( LastStoredTag == RequestedTag ) &&
	{
		if( CheckAlarm() )
		{
			P_Prj->AlarmStatusChanged = true;
			if( DB->LocateSensorRec( SensorRec ) ) DB->SetSensorRec( SensorRec );
		}
		LogCanMissingTag = true;
	}

}

#pragma argsused
void __fastcall TCIISSensor::CTSelectMode( int32_t Mode )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTSelectRange( int32_t Range )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTSelectRFreq( int32_t Freq )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTSelectCh( int32_t Ch )
{

}

void __fastcall TCIISSensor::CTDACalib()
{

}

void __fastcall TCIISSensor::CTOffsetAdj()
{

}

#pragma argsused
void __fastcall TCIISSensor::HUMRequestSample( int32_t RequestTag )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTRequestSample( int32_t RequestTag )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode )
{

}

#pragma argsused
void __fastcall TCIISSensor::CTRequestTemp( int32_t RequestTag )
{

}

bool __fastcall TCIISSensor::CTUpdatingTemp()
{
	return false;
}

double __fastcall TCIISSensor::CTZRAChange()
{
 	return 0.0;
}

bool __fastcall TCIISSensor::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
  return false;
}

//uCtrl

#pragma argsused
void __fastcall TCIISSensor::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	;
}
#pragma argsused
void __fastcall TCIISSensor::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	;
}
/* Sensors

	CIISNodeCapability

	Camur_II_LPR	= 10,
	Camur_II_IO		= 12,
	Camur_II_P		= 14,
	Camur_II_HUM	= 15,
	Camur_II_HiRes	= 16,
	Camur_II_R		= 17,
	Camur_II_ZRA	= 18,
	Camur_II_LPRExt	= 20,
	Camur_II_CT		= 21,
	Camur_II_MRE	= 22,
	Camur_II_CW		= 23,
	Camur_II_P4		= 24,
	Camur_II_Ladder	= 25,
	Camur_II_CrackWatch = 26,
	Camur_II_PT   = 27,
	Camur_II_Wenner = 28


*/

// Camur II LPR


__fastcall TCIISSensor_LPR::TCIISSensor_LPR(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  SensorRec->SensorConnected = false;
}

__fastcall TCIISSensor_LPR::~TCIISSensor_LPR()
{

}

void __fastcall TCIISSensor_LPR::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_OffsetAdj;
			break;

			case NI_OffsetAdj :
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_DACalib;
			break;

			case NI_DACalib:
			CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
			TNextEvent += DACalibDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_ReqVersion :
				CIISBICh->RequestVer( SensorRec->SensorCanAdr );
				VerRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForVer;
				IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_LPR_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;
					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// For version < 3

			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			//For version >= 3

			case NI_SetCamurIIMode:
				CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIRecPar;
				break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ReqCalibValues_Ver3;
				break;

			case NI_ReqCalibValues_Ver3 :
				BitValueRequested = 0;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh1BitValue;
				IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 2;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh3BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh3BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 3;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh4BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh4BitValue:
			if( BitValueRecived )
			{
				CTRange = 2;
				CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, CTRange );
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_LPR::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  double IShunt;
  if( SensorRec->SensorVerMajor < 3 )
  {
	CalibValuesRecived = true;
	IShunt = BPIn->Byte3 + BPIn->Byte4 * 256;
	if( IShunt > 64000 ) IShunt = 100000 + ( IShunt - 65000 ) * 10;
	IShunt = IShunt / 10;
	SensorRec->SensorIShunt = IShunt;


	#if DebugCIISBusInt == 1
	Debug( "Sensor LPR Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
			 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
	#endif
  }
  else
  {
	//BitValueRequested SensorVerMajor >= 3
	switch (BitValueRequested)
	{
		case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
		case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767 ; break;  // * -1
		case 2: SensorRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767 / 100 ; break; // * -1
		case 3: SensorRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767 / 100 ; break;
	}
	BitValueRecived = true;

	#if DebugCIISBusInt == 1
	Debug( "Sensor LPR Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
			 " Range  " + IntToStr( BPIn->Byte3 ) + " = " + IntToStr( BPIn->Byte4 ));
	#endif

  }
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_LPR::SetDefaultValuesSubType()
{
}

void __fastcall TCIISSensor_LPR::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	if( SensorRec->SensorVerMajor < 3 )
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * 1000 * 5 / 65535;
		ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		if(( ADVal2 & 512 ) == 512 ) ADVal2 = - ((( ~ADVal2 ) & 511 ) + 1 );
		AnVal2 = ADVal2; AnVal2 = AnVal2 / 4;
	}
	else if( SensorRec->SensorVerMajor < 4 ) // For V>4 see SensorSampleTemp
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

		ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal2 = ADVal2; AnVal2 = ( AnVal2 * 3 / 12.25 ) - 274.5;
	}
	else
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
  }


  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

  SensorRec->SensorLastValue = AnVal1_Scl;
  SensorRec->SensorLastTemp = AnVal2;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		if( SensorRec->SensorVerMajor < 4 )
		{
			MVR.ValueType = "T";
			MVR.ValueUnit = "C";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR);
		}

		P_Prj->LastValChanged();
  }



	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

	if( DB->LocateSensorRec( SensorRec ) )
	{
		DB->SetSensorRec( SensorRec );
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		if(( SensorRec->SensorTemp ) && ( SensorRec->SensorVerMajor < 4 ))
		{
			MValRec.ValueType = "T";
			MValRec.ValueUnit = "C";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2;

			DB->AppendMonitorValueRec( &MValRec );
		}
		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;

		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.ValueType = "U";
		DValRec.Value = AnVal1_Scl;

		DB->AppendDecayValueRec( &DValRec );

		if( SensorRec->SensorTemp && ( SensorRec->SensorVerMajor < 4 ) )
		{
			DValRec.ValueType = "T";
			DValRec.Value = AnVal2;

			DB->AppendDecayValueRec( &DValRec );
		}
		P_Prj->DataChanged();
	}
	else if( BPIn->RecType == RT_Monitor && BPIn->RequestedTag == 1 )  // First LPR value Camur III
	{
		SensorRec->SensorLPRStep = BPIn->Tag;
		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec );
		}

		CIISLPRValueRec LPRValRec;
		LPRValRec.DateTimeStamp = DTS;
		LPRValRec.RecNo = BPIn->RecordingNo;
		LPRValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		LPRValRec.ValueType = "C1";
		LPRValRec.ValueV = AnVal1;
		LPRValRec.ValueI = 0;

		DB->AppendLPRValueRec( &LPRValRec );
		P_Prj->DataChanged();
  }


  BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;


  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_LPR::SensorLPRSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
  DTS = Now();

  if( SensorRec->SensorVerMajor < 3 )
  {
		ADVal1 = BPIn->Byte5 + BPIn->Byte6 * 256;
		ADVal2 = BPIn->Byte3 + BPIn->Byte4 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * 1000 * 5 / 65535;
		AnVal2 = ADVal2; AnVal2 = AnVal2 * 1000 * 5 / 65535 / SensorRec->SensorIShunt;
	}
	else
	{
		ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;
		ADVal2 = BPIn->Byte5 + BPIn->Byte6 * 256;

		AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

		AnVal2 = ADVal2;
		switch( CTRange )
		{
			case 1: AnVal2 = SensorRec->Ch2BitVal * AnVal2; break;
			case 2: AnVal2 = SensorRec->Ch3BitVal * AnVal2; break;
		}
  }

  CIISMiscValueRec MVR;
  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "U";
  MVR.ValueUnit = "mV";
  MVR.Value = AnVal1;

  DB->AppendMiscValueRec( &MVR);

  MVR.ValueType = "I";
  MVR.ValueUnit = "mA";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR);

	P_Prj->LastValChanged();

	SensorRec->SensorLPRStep = SensorRec->SensorLPRStep + 1;
	if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec );
  }

	CIISLPRValueRec LPRValRec;
	LPRValRec.DateTimeStamp = DTS;
	LPRValRec.RecNo = BPIn->RecordingNo;
	LPRValRec.SensorSerialNo = SensorRec->SensorSerialNo;
	LPRValRec.ValueType = "C1";
	LPRValRec.ValueV = AnVal1;
	LPRValRec.ValueI = AnVal2;

	DB->AppendLPRValueRec( &LPRValRec );
	//DB->ApplyUpdatesLPRValue();

	P_Prj->DataChanged();
	//DB->ApplyUpdatesSensor();
	//DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
	//DB->ApplyUpdatesPrj();


  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
  Debug("Incoming LPRSample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " V = " + FloatToStr(AnVal1) + " / I = " + FloatToStr(AnVal2) );
  #endif
}


/*
void __fastcall TCIISSensor_LPR::SensorLPRSample_U( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
  DTS = Now();

	ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;
	AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
	LPRVal_U = AnVal1;

  CIISMiscValueRec MVR;
  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "U";
  MVR.ValueUnit = "mV";
  MVR.Value = AnVal1;

	DB->AppendMiscValueRec( &MVR);
	P_Prj->LastValChanged();

	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
	Debug("Incoming LPRSample U recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " V = " + FloatToStr(AnVal1));
	#endif
}

void __fastcall TCIISSensor_LPR::SensorLPRSample_I( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
  DTS = Now();

	ADVal2 = BPIn->Byte3 + BPIn->Byte4 * 256;
	AnVal2 = ADVal2;
	switch( CTRange )
	{
		case 1: AnVal2 = SensorRec->Ch2BitVal * AnVal2; break;
		case 2: AnVal2 = SensorRec->Ch3BitVal * AnVal2; break;
	}


  CIISMiscValueRec MVR;
  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
	MVR.ValueType = "I";
  MVR.ValueUnit = "mA";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR);

	P_Prj->LastValChanged();

	SensorRec->SensorLPRStep = BPIn->Tag;
	if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec );
  }

	CIISLPRValueRec LPRValRec;
	LPRValRec.DateTimeStamp = DTS;
	LPRValRec.RecNo = BPIn->RecordingNo;
	LPRValRec.SensorSerialNo = SensorRec->SensorSerialNo;
	LPRValRec.ValueType = "C1";
	LPRValRec.ValueV = LPRVal_U;
	LPRValRec.ValueI = AnVal2;

	DB->AppendLPRValueRec( &LPRValRec );

	P_Prj->DataChanged();

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
  Debug("Incoming LPRSample I recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " I = " + FloatToStr(AnVal2) );
  #endif
}
*/


void __fastcall TCIISSensor_LPR::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
	DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1; //AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2; //AnVal2 = AnVal2 * SensorRec->Ch6BitVal * 1000;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
	Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
  #endif

  if( AnVal2 == 0 )
	{
		AnVal2 = -300;
	}
	else if( fabs(AnVal1/AnVal2) > 7 )
	{
		AnVal2 = -300;
	}
	else
	{
		AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;
	}

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "T";
		MValRec.ValueUnit = "C";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
	#endif
}

bool __fastcall TCIISSensor_LPR::SensorLPRIni()
{
	SensorRec->RequestStatus = -1;
	SensorRec->SensorLPRStep = 0;
	return true;
}

void __fastcall TCIISSensor_LPR::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;
			if( SensorRec->SensorTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISSensor_LPR::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		UpdateLastValueRunning = true;
		CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 );
		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISSensor_LPR::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			SensorLastValueTempRequest = true;
			CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 ); // Tag always 0 fore Last value request
			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			UpdateLastValueRunning = false;
			break;
		}
	}
	T2 += SysClock;
}

void __fastcall TCIISSensor_LPR::LPRSelectRange( int32_t Range )
{
	int32_t FMode;


	if( SensorRec->SensorVerMajor >= 5 )
	{
		if( Range == -1 )
		{
			Range = SensorRec->SensorLPRIRange;
		}
		CTRange = Range;

		if( RMeasureFreq == 100 ) FMode = 0;
		else FMode = 1;

		if( CIISBICh != NULL ) CIISBICh->SetCTModeRangeFreq( 2, true, CTRange, 0, FMode );
	}
	else if( SensorRec->SensorVerMajor >= 3 )
	{
		if( Range == -1 )
		{
			Range = SensorRec->SensorLPRIRange;
		}

		CTRange = Range;
		if( CIISBICh != NULL ) CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, Range );
	}

}

void __fastcall TCIISSensor_LPR::CTRequestTemp( int32_t RequestTag )
{
	if(( CIISBICh != NULL ) && ( SensorRec->SensorVerMajor >= 4 )) CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, RequestTag );
}

//Camur_II_IO

__fastcall TCIISSensor_IO::TCIISSensor_IO(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
							 :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
	CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_IO::~TCIISSensor_IO()
{

}

void __fastcall TCIISSensor_IO::SM_SensorIni()
{
  if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_IO_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else NIState = NI_ReqCalibValues_Ver3;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// For version < 3

			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// End for version < 3

			// For version >= 3

			case NI_ReqCalibValues_Ver3 :
			BitValueRequested = 0;
			CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh1BitValue;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 2;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh3BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh3BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 3;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh4BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh4BitValue:
			if( BitValueRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// End for version >= 3

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_IO::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  if( SensorRec->SensorVerMajor < 3 )
  {
	CalibValuesRecived = true;

	SensorRec->Ch1BitVal = BPIn->Byte3 * 0.2 /65535;
	SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 /65535;
	SensorRec->Ch3BitVal = BPIn->Byte5 * 0.2 /65535;
	SensorRec->Ch4BitVal = BPIn->Byte6 * 0.2 /65535;



  }
  else
  {
	switch (BitValueRequested)
	{
	  case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767; break;
	  case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767; break;
	  case 2: SensorRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767; break;
	  case 3: SensorRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767; break;
	}

	BitValueRecived = true;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "Sensor IO Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) +
		 " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ));
  #endif
}

void __fastcall TCIISSensor_IO::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "V";
  SensorRec->SensorUnit2 = "V";
}

void __fastcall TCIISSensor_IO::SensorSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
  AnVal1 = UADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
  AnVal2 = UADVal2; AnVal2 = AnVal2 * SensorRec->Ch2BitVal;

  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
  AnVal2_Scl = AnVal2 * SensorRec->SensorGain2 + SensorRec->SensorOffset2;

  SensorRec->SensorLastValue = AnVal1_Scl;
  SensorRec->SensorLastValue2 = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "C1";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR);

		MVR.ValueType = "C2";
		MVR.ValueUnit = SensorRec->SensorUnit2;
		MVR.Value = AnVal2_Scl;

		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
  }


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "C1";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		MValRec.ValueType = "C2";
		MValRec.ValueUnit = SensorRec->SensorUnit2;
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;
		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
	}
	else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
  }
  else if( SensorRec->PowerShutdownEnabled && BPIn->Tag == ExpectedShutdownTag )
  {
	if( AnVal1 < 2.5 ) P_Prj->PowerShutdown = true;
    else P_Prj->PowerShutdown = false;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif

}

void __fastcall TCIISSensor_IO::SensorSampleExt( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
  AnVal1 = UADVal1; AnVal1 = AnVal1 * SensorRec->Ch3BitVal;
  AnVal2 = UADVal2; AnVal2 = AnVal2 * SensorRec->Ch4BitVal;
  AnVal1_Scl = AnVal1;//AnVal1_Scl = AnVal1 * SensorRec->Ch3Gain + SensorRec->Ch3Offset;
  AnVal2_Scl = AnVal2;//AnVal2_Scl = AnVal2 * SensorRec->Ch4Gain + SensorRec->Ch4Offset;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "C3";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR);

		MVR.ValueType = "C4";
		MVR.ValueUnit =  SensorRec->SensorUnit2;
		MVR.Value = AnVal2_Scl;

		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
  }



	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "C3";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		MValRec.ValueType = "C4";
		MValRec.ValueUnit = SensorRec->SensorUnit2;
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

bool __fastcall TCIISSensor_IO::CheckAlarm()
{
int32_t AlarmStatus1, AlarmStatus2;
bool StatusChanged;

  //AlarmCheck  ( on scaled values )

  StatusChanged = false;
  AlarmStatus1 = SensorRec->SensorAlarmStatus;
  AlarmStatus2 = SensorRec->SensorAlarmStatus2;

	if( SensorRec->SensorAlarmEnabled == true )
  {
		if( ( SensorRec->SensorLastValue < SensorRec->SensorLow ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorHigh ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorLow ) && ( SensorRec->SensorLastValue < SensorRec->SensorHigh ) && ( AlarmStatus1 == 1 ) )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}

		if( ( SensorRec->SensorLastValue2 < SensorRec->SensorLow2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorLow2 ) && ( SensorRec->SensorLastValue2 < SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 1 ) )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
  } // AlarmCheck
	else // SensorAlarmEnabled == false
	{
		if( AlarmStatus1 != 0 )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus2 != 0 )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
		}
	}

	if( SensorRec->CANAlarmEnabled )
	{
		if( MissingTagDetected && ( SensorRec->CANAlarmStatus == 0 ))
		{
			SensorRec->CANAlarmStatus = 1;
			StatusChanged = true;
		}
	}
	else
	{
		if( SensorRec->CANAlarmStatus == 1 )
		{
			SensorRec->CANAlarmStatus = 0;
			MissingTagDetected = false;
			StatusChanged = true;
		}
	}

  if( StatusChanged == true )
  {
		SensorRec->SensorAlarmStatus = AlarmStatus1;
		SensorRec->SensorAlarmStatus2 = AlarmStatus2;
  }

  #if DebugCIISAlarm == 1
  Debug( "CheckAlarm Sensor StatusChanged = " + BoolToStr( StatusChanged ) );
  #endif

	return StatusChanged;
}

#pragma argsused
void __fastcall TCIISSensor_IO::OnSysClockTick( TDateTime TickTime )
{
  if( SensorIniRunning ) SM_SensorIni();
	else if( UpdateLastValueRunning ) SM_UpdateLastValue();

  OnSlowClockTick--;
  if( OnSlowClockTick <= 0 )
  {
		OnSlowClockTick = 50;
		if( SensorRec->PowerShutdownEnabled && ( CIISBICh != NULL )  )
		{
			RequestShutdownTag++;
			if( RequestShutdownTag > 210 ) RequestShutdownTag = 201;
			ExpectedShutdownTag = RequestShutdownTag;
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, RequestShutdownTag );
		}
  }
}


// Camur II P


__fastcall TCIISSensor_P::TCIISSensor_P(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  SensorRec->SensorConnected = false;
}

__fastcall TCIISSensor_P::~TCIISSensor_P()
{

}

void __fastcall TCIISSensor_P::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_OffsetAdj;
			break;

			case NI_OffsetAdj :
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_P_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// For version < 3

			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;


			//For version >= 3

			case NI_SetCamurIIMode:
				CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIRecPar;
				break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ReqCalibValues_Ver3;
				break;


			case NI_ReqCalibValues_Ver3 :
				BitValueRequested = 0;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh1BitValue;
				IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorRec->SensorConnected = false;
			SensorIniRunning = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_P::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  if( SensorRec->SensorVerMajor < 3 )
  {

	double IShunt;

	CalibValuesRecived = true;
	IShunt = BPIn->Byte3 + BPIn->Byte4 * 256;
	if( IShunt > 64000 ) IShunt = 100000 + ( IShunt - 65000 ) * 10;
	IShunt = IShunt / 10;
	SensorRec->SensorIShunt = IShunt;

	#if DebugCIISBusInt == 1
	Debug( "RX BI" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( SensorRec->SensorCanAdr ) + "/" + IntToStr( SensorRec->SensorSerialNo ) +
		   " Scale = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
	#endif
  }
  else
  {

	switch (BitValueRequested)
	{
		case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	}
	BitValueRecived = true;

	#if DebugCIISBusInt == 1
	Debug( "RX BI" + IntToStr( CIISBICh->BusInterfaceSerialNo ) + "/" + IntToStr( SensorRec->SensorCanAdr ) + "/" + IntToStr( SensorRec->SensorSerialNo ) +
		   " Scale = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
	#endif
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_P::SetDefaultValuesSubType()
{
}

void __fastcall TCIISSensor_P::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	if( SensorRec->SensorVerMajor < 3 )
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * 1000 * 5 / 65535;
		ADVal2 = 0;
	}
	else
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
	}

	AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

	SensorRec->SensorLastValue = AnVal1_Scl;
	SensorRec->SensorLastTemp = AnVal2;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

	if( DB->LocateSensorRec( SensorRec ) )
	{
		DB->SetSensorRec( SensorRec );
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		if( FIniDecaySample && FRunDistDecay ) DValRec.SampleDateTime = IniDecayTime;
		else DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U";
		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;


	#if DebugCIISBusInt == 1
	Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	#endif
}

bool __fastcall TCIISSensor_P::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if(( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 3  )))
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni( SensorRec->SensorCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}

// Camur II P4


__fastcall TCIISSensor_P4::TCIISSensor_P4(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
							 :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
	CIISObjType = CIISSensors;
	SensorRec = SetSensorRec;
	SensorIniRunning = false;


	NIState = NI_ZoneAdr;
	IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  SensorRec->SensorConnected = false;
}

__fastcall TCIISSensor_P4::~TCIISSensor_P4()
{

}

void __fastcall TCIISSensor_P4::SM_SensorIni()
{

	if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept:
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr:
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion:
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer:
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_P4_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 5 ) NIState = NI_OffsetAdj;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_SetCamurIIMode:
			CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIRecPar;
			break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_OffsetAdj;
				break;


			case NI_OffsetAdj:
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqCalibValues;
			break;

			case NI_ReqCalibValues:
			BitValueRequested = 0;
			CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh1BitValue;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 2;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh3BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh3BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 3;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh4BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh4BitValue:
			if( BitValueRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorRec->SensorConnected = false;
			SensorIniRunning = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_P4::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  switch (BitValueRequested)
  {
	case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	case 2: SensorRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	case 3: SensorRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
  }

  BitValueRecived = true;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_P4::SetDefaultValuesSubType()
{
	SensorRec->SensorUnit3= "mV";
	SensorRec->SensorUnit4= "mV";

	SensorRec->SensorName = IntToStr( SensorRec->SensorSerialNo ) + "-1";
	SensorRec->SensorName2 = IntToStr( SensorRec->SensorSerialNo ) + "-2";
	SensorRec->SensorName3 = IntToStr( SensorRec->SensorSerialNo ) + "-3";
	SensorRec->SensorName4 = IntToStr( SensorRec->SensorSerialNo ) + "-4";
}

void __fastcall TCIISSensor_P4::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2; AnVal2 = AnVal2 * SensorRec->Ch2BitVal;



	AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
	AnVal2_Scl = AnVal2 * SensorRec->SensorGain2 + SensorRec->SensorOffset2;

	SensorRec->SensorLastValue = AnVal1_Scl;
	SensorRec->SensorLastValue2 = AnVal2_Scl;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U1";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;
		DB->AppendMiscValueRec( &MVR );

		MVR.ValueType = "U2";
		MVR.Value = AnVal2_Scl;
		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
  }

	// Alarm check moved to SensorSampleExt
	//if( BPIn->RecType == RT_Monitor && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec );
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U1";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		MValRec.ValueType = "U2";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();

	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		//if( FIniDecaySample && (( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 5  )))) DValRec.SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec.SampleDateTime = IniDecayTime;
		else DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U1";
		DB->AppendDecayValueRec( &DValRec );

		DValRec.Value = AnVal2_Scl;
		DValRec.ValueType = "U2";
		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
  }

	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;


  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_P4::SensorSampleExt( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch3BitVal;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2; AnVal2 = AnVal2 * SensorRec->Ch4BitVal;



	AnVal1_Scl = AnVal1 * SensorRec->SensorGain3 + SensorRec->SensorOffset3;
	AnVal2_Scl = AnVal2 * SensorRec->SensorGain4 + SensorRec->SensorOffset4;

	SensorRec->SensorLastValue3 = AnVal1_Scl;
	SensorRec->SensorLastValue4 = AnVal2_Scl;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U3";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;
		DB->AppendMiscValueRec( &MVR );

		MVR.ValueType = "U4";
		MVR.Value = AnVal2_Scl;
		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
  }


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec );
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U3";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		MValRec.ValueType = "U4";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		//if( FIniDecaySample && (( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 5  )))) DValRec.SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec.SampleDateTime = IniDecayTime;
		else DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U3";
		DB->AppendDecayValueRec( &DValRec );

		DValRec.Value = AnVal2_Scl;
		DValRec.ValueType = "U4";
		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
  }

	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;


  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

bool __fastcall TCIISSensor_P4::CheckAlarm()
{
int32_t AlarmStatus1, AlarmStatus2, AlarmStatus3, AlarmStatus4;
bool StatusChanged;

  //AlarmCheck  ( on scaled values )

  StatusChanged = false;
	AlarmStatus1 = SensorRec->SensorAlarmStatus;
	AlarmStatus2 = SensorRec->SensorAlarmStatus2;
	AlarmStatus3 = SensorRec->SensorAlarmStatus3;
	AlarmStatus4 = SensorRec->SensorAlarmStatus4;

	if( SensorRec->SensorAlarmEnabled == true )
  {
		// Channel 1
		if( ( SensorRec->SensorLastValue < SensorRec->SensorLow ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorHigh ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorLow ) && ( SensorRec->SensorLastValue < SensorRec->SensorHigh ) && ( AlarmStatus1 == 1 ) )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		// Channel 2
		if( ( SensorRec->SensorLastValue2 < SensorRec->SensorLow2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow2, CIIEventLevel_High, SensorRec->SensorName2, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh2, CIIEventLevel_High, SensorRec->SensorName2, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorLow2 ) && ( SensorRec->SensorLastValue2 < SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 1 ) )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal2, CIIEventLevel_High, SensorRec->SensorName2, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}

		// Channel 3
		if( ( SensorRec->SensorLastValue3 < SensorRec->SensorLow3 ) && ( AlarmStatus3 == 0 ) )
		{
			AlarmStatus3 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow3, CIIEventLevel_High, SensorRec->SensorName3, SensorRec->SensorSerialNo, SensorRec->SensorLastValue3 );
		}
		else if( ( SensorRec->SensorLastValue3 > SensorRec->SensorHigh3 ) && ( AlarmStatus3 == 0 ) )
		{
			AlarmStatus3 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh3, CIIEventLevel_High, SensorRec->SensorName3, SensorRec->SensorSerialNo, SensorRec->SensorLastValue3 );
		}
		else if( ( SensorRec->SensorLastValue3 > SensorRec->SensorLow3 ) && ( SensorRec->SensorLastValue3 < SensorRec->SensorHigh3 ) && ( AlarmStatus3 == 1 ) )
		{
			AlarmStatus3 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal3, CIIEventLevel_High, SensorRec->SensorName3, SensorRec->SensorSerialNo, SensorRec->SensorLastValue3 );
		}

		// Channel 4
		if( ( SensorRec->SensorLastValue4 < SensorRec->SensorLow4 ) && ( AlarmStatus4 == 0 ) )
		{
			AlarmStatus4 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow4, CIIEventLevel_High, SensorRec->SensorName4, SensorRec->SensorSerialNo, SensorRec->SensorLastValue4 );
		}
		else if( ( SensorRec->SensorLastValue4 > SensorRec->SensorHigh4 ) && ( AlarmStatus4 == 0 ) )
		{
			AlarmStatus4 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh4, CIIEventLevel_High, SensorRec->SensorName4, SensorRec->SensorSerialNo, SensorRec->SensorLastValue4 );
		}
		else if( ( SensorRec->SensorLastValue4 > SensorRec->SensorLow4 ) && ( SensorRec->SensorLastValue4 < SensorRec->SensorHigh4 ) && ( AlarmStatus4 == 1 ) )
		{
			AlarmStatus4 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal4, CIIEventLevel_High, SensorRec->SensorName4, SensorRec->SensorSerialNo, SensorRec->SensorLastValue4 );
		}
	} // AlarmCheck
	else // SensorAlarmEnabled == false
	{
		if( AlarmStatus1 != 0 )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus2 != 0 )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus3 != 0 )
		{
			AlarmStatus3 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus4 != 0 )
		{
			AlarmStatus4 = 0;
			StatusChanged = true;
		}
	}

	if( SensorRec->CANAlarmEnabled )
	{
		if( MissingTagDetected && ( SensorRec->CANAlarmStatus == 0 ))
		{
			SensorRec->CANAlarmStatus = 1;
			StatusChanged = true;
		}
	}
	else
	{
		if( SensorRec->CANAlarmStatus == 1 )
		{
			SensorRec->CANAlarmStatus = 0;
			MissingTagDetected = false;
			StatusChanged = true;
		}
	}

  if( StatusChanged == true )
  {
		SensorRec->SensorAlarmStatus = AlarmStatus1;
		SensorRec->SensorAlarmStatus2 = AlarmStatus2;
		SensorRec->SensorAlarmStatus3 = AlarmStatus3;
		SensorRec->SensorAlarmStatus4 = AlarmStatus4;
	}

  #if DebugCIISAlarm == 1
	Debug( "CheckAlarm Sensor StatusChanged = " + BoolToStr( StatusChanged ) );
  #endif

	return StatusChanged;
}

bool __fastcall TCIISSensor_P4::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	bool DecaySupported;

	if(( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 5  )))
	{
		DecaySupported = true;
		//CIISBusInt->RequestDecayIni( SensorRec->SensorCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	}
	else DecaySupported = false;

	return DecaySupported;
}



void __fastcall TCIISSensor_P4::uCtrlNodeInfo( const Byte *BusIntMsg )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;

	SensorRec->SensorConnected = true;
	SensorRec->SensorStatus = 0;

	SensorRec->Ch1BitVal = BusIntMsg[11] * 0.2 / 32767 * 1000;
	SensorRec->Ch2BitVal = BusIntMsg[12] * 0.2 / 32767 * 1000;
	SensorRec->Ch3BitVal = BusIntMsg[13] * 0.2 / 32767 * 1000;
	SensorRec->Ch4BitVal = BusIntMsg[14] * 0.2 / 32767 * 1000;

	Ch1 =  SensorRec->Ch1BitVal * (int16_t)( BusIntMsg[19] + BusIntMsg[20] * 256  );
	Ch2 =  SensorRec->Ch2BitVal * (int16_t)( BusIntMsg[21] + BusIntMsg[22] * 256  );
	Ch3 =  SensorRec->Ch3BitVal * (int16_t)( BusIntMsg[23] + BusIntMsg[24] * 256  );
	Ch4 =  SensorRec->Ch4BitVal * (int16_t)( BusIntMsg[25] + BusIntMsg[26] * 256  );

	Ch1_Scl = Ch1 * SensorRec->SensorGain + SensorRec->SensorOffset;
	Ch2_Scl = Ch2 * SensorRec->SensorGain2 + SensorRec->SensorOffset2;
	Ch3_Scl = Ch3 * SensorRec->SensorGain3 + SensorRec->SensorOffset3;
	Ch4_Scl = Ch4 * SensorRec->SensorGain4 + SensorRec->SensorOffset4;

	if( DB->LocateSensorRec( SensorRec ) )
	{
		DB->SetSensorRec( SensorRec );
	}

	CIISMiscValueRec MVR;
	MVR.DateTimeStamp = Now();
	MVR.SensorSerialNo = SensorRec->SensorSerialNo;
	MVR.ValueType = "U1";
	MVR.ValueUnit = SensorRec->SensorUnit;
	MVR.Value = Ch1_Scl;
	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "U2";
	MVR.Value = Ch2_Scl;
	DB->AppendMiscValueRec( &MVR);

	MVR.ValueType = "U3";
	MVR.Value = Ch3_Scl;
	DB->AppendMiscValueRec( &MVR );

	MVR.ValueType = "U4";
	MVR.Value = Ch4_Scl;
	DB->AppendMiscValueRec( &MVR);

	DB->ApplyUpdatesMiscValue( P_Prj->MiscSize );

	P_Prj->LastValChanged();



				/*
				LV1->Caption = FloatToStrF( P4_Ch1BitVal * (Smallint)( BusIntMsg[19] + BusIntMsg[20]*256 ), ffFixed, 4, 2 );
				LV2->Caption = FloatToStrF( P4_Ch2BitVal * (Smallint)( BusIntMsg[21] + BusIntMsg[22]*256 ), ffFixed, 4, 2 );
				LV3->Caption = FloatToStrF( P4_Ch3BitVal * (Smallint)( BusIntMsg[23] + BusIntMsg[24]*256 ), ffFixed, 4, 2 );
				LV4->Caption = FloatToStrF( P4_Ch4BitVal * (Smallint)( BusIntMsg[25] + BusIntMsg[26]*256 ), ffFixed, 4, 2 );
				*/
}

void __fastcall TCIISSensor_P4::uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime )
{
	double Ch1, Ch2, Ch3, Ch4, Ch1_Scl, Ch2_Scl, Ch3_Scl, Ch4_Scl;


	Ch1 =  SensorRec->Ch1BitVal * (int16_t)( RecordingMem[i + 1] + RecordingMem[i + 2] * 256  );
	Ch2 =  SensorRec->Ch2BitVal * (int16_t)( RecordingMem[i + 3] + RecordingMem[i + 4] * 256  );
	Ch3 =  SensorRec->Ch3BitVal * (int16_t)( RecordingMem[i + 5] + RecordingMem[i + 6] * 256  );
	Ch4 =  SensorRec->Ch4BitVal * (int16_t)( RecordingMem[i + 7] + RecordingMem[i + 8] * 256  );

	Ch1_Scl = Ch1 * SensorRec->SensorGain + SensorRec->SensorOffset;
	Ch2_Scl = Ch2 * SensorRec->SensorGain2 + SensorRec->SensorOffset2;
	Ch3_Scl = Ch3 * SensorRec->SensorGain3 + SensorRec->SensorOffset3;
	Ch4_Scl = Ch4 * SensorRec->SensorGain4 + SensorRec->SensorOffset4;

	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = SampleDateTime;
	MValRec.SampleDateTime = SampleDateTime;
	MValRec.RecNo = RecNo;
	MValRec.SensorSerialNo = SensorRec->SensorSerialNo;

	MValRec.ValueType = "U1";
	MValRec.ValueUnit = SensorRec->SensorUnit;
	MValRec.RawValue = Ch1;
	MValRec.Value = Ch1_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "U2";
	MValRec.RawValue = Ch2;
	MValRec.Value = Ch2_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "U3";
	MValRec.ValueUnit = SensorRec->SensorUnit;
	MValRec.RawValue = Ch3;
	MValRec.Value = Ch3_Scl;
	DB->AppendMonitorValueRec( &MValRec );

	MValRec.ValueType = "U4";
	MValRec.RawValue = Ch4;
	MValRec.Value = Ch4_Scl;
	DB->AppendMonitorValueRec( &MValRec );
}

// Camur II LPRExt

__fastcall TCIISSensor_LPRExt::TCIISSensor_LPRExt(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_LPRExt::~TCIISSensor_LPRExt()
{

}

void __fastcall TCIISSensor_LPRExt::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
		case NI_Accept:
		if( !NodeDetected )
		{
		  SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
		TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

		case NI_ZoneAdr:
		CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion:
		CIISBICh->RequestVer( SensorRec->SensorCanAdr );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForVer:
		if( VerRecived )
		{
			if( SensorRec->SensorVerMajor > S_LPRExt_SupportedVer )
			{
				SensorRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				SensorRec->VerNotSupported = false;

				NIState = NI_ReqCalibValues;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_ReqCalibValues:
		CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
		CalibValuesRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

		case NI_WaitForCalibValues:
		if( CalibValuesRecived )
		{
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		case NI_Ready:
		SensorIniRunning = false;
		SensorRec->SensorConnected = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

		case NI_TimeOut:
		SensorIniRunning = false;
		SensorRec->SensorConnected = false;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

        default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_LPRExt::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  double IShunt;

  CalibValuesRecived = true;
  IShunt = BPIn->Byte3 + BPIn->Byte4 * 256;
  if( IShunt > 64000 ) IShunt = 100000 + ( IShunt - 65000 ) * 10;
  IShunt = IShunt / 100;
  SensorRec->SensorIShunt = IShunt;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
  Debug( "Sensor LPRExt Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISSensor_LPRExt::SetDefaultValuesSubType()
{
}

void __fastcall TCIISSensor_LPRExt::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  AnVal1 = ADVal1; AnVal1 = AnVal1 * 1000 * 5 / 65535;
  ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
  if(( ADVal2 & 512 ) == 512 ) ADVal2 = - ((( ~ADVal2 ) & 511 ) + 1 );
  AnVal2 = ADVal2; AnVal2 = AnVal2 / 4;

  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

  SensorRec->SensorLastValue = AnVal1_Scl;
  SensorRec->SensorLastTemp = AnVal2;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		MVR.ValueType = "T";
		MVR.ValueUnit = "C";
		MVR.Value = AnVal2;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
  }


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		if( SensorRec->SensorTemp )
		{
			MValRec.ValueType = "T";
			MValRec.ValueUnit = "C";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2;

			DB->AppendMonitorValueRec( &MValRec );
		}

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U";

		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_LPRExt::SensorLPRSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte5 + BPIn->Byte6 * 256;
  ADVal2 = BPIn->Byte3 + BPIn->Byte4 * 256;
  AnVal1 = ADVal1; AnVal1 = AnVal1 * 1000 * 5 / 65535;
  AnVal2 = ADVal2; AnVal2 = AnVal2 * 1000 * 5 / 65535 / SensorRec->SensorIShunt;

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "U";
  MVR.ValueUnit = "mV";
  MVR.Value = AnVal1;

  DB->AppendMiscValueRec( &MVR );

  MVR.ValueType = "I";
  MVR.ValueUnit = "mA";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	SensorRec->SensorLPRStep = SensorRec->SensorLPRStep + 1;
  if( DB->LocateSensorRec( SensorRec ) )
  {
	DB->SetSensorRec( SensorRec );
  }

  CIISLPRValueRec LPRValRec;
  LPRValRec.DateTimeStamp = DTS;
  LPRValRec.RecNo = BPIn->RecordingNo;
  LPRValRec.SensorSerialNo = SensorRec->SensorSerialNo;
  LPRValRec.ValueType = "C1";
  LPRValRec.ValueV = AnVal1;
  LPRValRec.ValueI = AnVal2;

  DB->AppendLPRValueRec( &LPRValRec );

	//DB->ApplyUpdatesLPRValue();
	//P_Prj->DataChanged();
	//DB->ApplyUpdatesSensor();
	//DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
	//DB->ApplyUpdatesPrj();

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
  Debug("Incoming LPRSample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " V = " + FloatToStr(AnVal1) + " / I = " + FloatToStr(AnVal2) );
  #endif
}

bool __fastcall TCIISSensor_LPRExt::SensorLPRIni()
{
	SensorRec->RequestStatus = -1;
	SensorRec->SensorLPRStep = 0;
	return true;
}

// Camur II HUM

__fastcall TCIISSensor_HUM::TCIISSensor_HUM(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_HUM::~TCIISSensor_HUM()
{

}

void __fastcall TCIISSensor_HUM::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;
  
		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_HUM_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// For version < 3


			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// End for version < 3

			// For version >= 3

			case NI_SetCamurIIMode:
				CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIRecPar;
				break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ReqCalibValues_Ver3;
				break;

			case NI_ReqCalibValues_Ver3 :
			BitValueRequested = 0;
			CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
			BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCh1BitValue;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// End for version >= 3

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_HUM::SensorIniCalibValues( TCIISBusPacket *BPIn )
{

  if( SensorRec->SensorVerMajor < 3 )
  {
	CalibValuesRecived = true;
	SensorRec->Ch1BitVal = BPIn->Byte3 * 1000 * 0.2 /65535;
	SensorRec->Ch2BitVal = BPIn->Byte4 * 1000 * 0.2 /65535;
  }
  else if( SensorRec->SensorVerMajor < 5 )
  {
	switch (BitValueRequested)
	{
	  case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 1000 * 0.2 / 65535; break;
	  case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 1000 * 0.2 / 65535; break;
	}
	BitValueRecived = true;
  }
  else
  {
	switch (BitValueRequested)
	{
	  case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 1000 * 0.2 / 32767; break;
	  case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 1000 * 0.2 / 32767; break;
	}
	BitValueRecived = true;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "Sensor HUM Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISSensor_HUM::SetDefaultValuesSubType()
{
  SensorRec->SensorWarmUp = 5;
}

void __fastcall TCIISSensor_HUM::SensorWarmUp( bool On )
{
  if( SensorRec->SensorWarmUp != 0 && ( CIISBICh != NULL )) CIISBICh->SetDigOut( SensorRec->SensorCanAdr, 0, On );;
}

void __fastcall TCIISSensor_HUM::SensorSample( TCIISBusPacket *BPIn )
{

  TDateTime DTS;
  DTS = Now();

  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
  AnVal1 = UADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
  AnVal2 = UADVal2; AnVal2 = AnVal2 * SensorRec->Ch2BitVal;

  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
  AnVal2_Scl = AnVal2 * SensorRec->SensorGain2 + SensorRec->SensorOffset2;

  SensorRec->SensorLastValue = AnVal1_Scl;
  SensorRec->SensorLastValue2 = AnVal2_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "C1";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		MVR.ValueType = "C2";
		MVR.ValueUnit = SensorRec->SensorUnit2;
		MVR.Value = AnVal2_Scl;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
  }


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		MValRec.ValueType = "U2";
		MValRec.ValueUnit = SensorRec->SensorUnit2;
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2_Scl;
		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;

		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U";

		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
  }
  else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		if( BPIn->Tag == 1 ) // Store only first sample
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "RH1";
			MValRec.ValueUnit = SensorRec->SensorUnit;
			MValRec.RawValue = AnVal1;
			MValRec.Value = AnVal1_Scl;
			DB->AppendMonitorValueRec( &MValRec );

			MValRec.ValueType = "RH2";
			MValRec.ValueUnit = SensorRec->SensorUnit2;
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;
			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

bool __fastcall TCIISSensor_HUM::CheckAlarm()
{
int32_t AlarmStatus1, AlarmStatus2;
bool StatusChanged;

  //AlarmCheck  ( on scaled values )

  StatusChanged = false;
  AlarmStatus1 = SensorRec->SensorAlarmStatus;
  AlarmStatus2 = SensorRec->SensorAlarmStatus2;

	if( SensorRec->SensorAlarmEnabled == true )
  {
		if( ( SensorRec->SensorLastValue < SensorRec->SensorLow ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorHigh ) && ( AlarmStatus1 == 0 ) )
		{
			AlarmStatus1 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}
		else if( ( SensorRec->SensorLastValue > SensorRec->SensorLow ) && ( SensorRec->SensorLastValue < SensorRec->SensorHigh ) && ( AlarmStatus1 == 1 ) )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue );
		}

		if( ( SensorRec->SensorLastValue2 < SensorRec->SensorLow2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorLow2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 0 ) )
		{
			AlarmStatus2 = 1;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorHigh2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
		else if( ( SensorRec->SensorLastValue2 > SensorRec->SensorLow2 ) && ( SensorRec->SensorLastValue2 < SensorRec->SensorHigh2 ) && ( AlarmStatus2 == 1 ) )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
			P_Prj->Log( LevAlarm, CIIEventCode_SensorNormal2, CIIEventLevel_High, SensorRec->SensorName, SensorRec->SensorSerialNo, SensorRec->SensorLastValue2 );
		}
	} // AlarmCheck
  else // SensorAlarmEnabled == false
  {
		if( AlarmStatus1 != 0 )
		{
			AlarmStatus1 = 0;
			StatusChanged = true;
		}
		if( AlarmStatus2 != 0 )
		{
			AlarmStatus2 = 0;
			StatusChanged = true;
		}
	}

	if( SensorRec->CANAlarmEnabled )
	{
		if( MissingTagDetected && ( SensorRec->CANAlarmStatus == 0 ))
		{
			SensorRec->CANAlarmStatus = 1;
			StatusChanged = true;
		}
	}
	else
	{
		if( SensorRec->CANAlarmStatus == 1 )
		{
			SensorRec->CANAlarmStatus = 0;
			MissingTagDetected = false;
			StatusChanged = true;
		}
	}

  if( StatusChanged == true )
  {
		SensorRec->SensorAlarmStatus = AlarmStatus1;
		SensorRec->SensorAlarmStatus2 = AlarmStatus2;
  }

  #if DebugCIISAlarm == 1
  Debug( "CheckAlarm Sensor StatusChanged = " + BoolToStr( StatusChanged ) );
  #endif

	return StatusChanged;
}

void __fastcall TCIISSensor_HUM::HUMRequestSample( int32_t RequestTag )
{
  if( CIISBICh != NULL ) CIISBICh->RequestSample( SensorRec->SensorCanAdr, RequestTag );
}

// Camur II HiRes

__fastcall TCIISSensor_HiRes::TCIISSensor_HiRes(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
							 :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_HiRes::~TCIISSensor_HiRes()
{

}

void __fastcall TCIISSensor_HiRes::SM_SensorIni()
{
  if( T >= TNextEvent )
  {
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;
  
		switch( NIState )
		{
			case NI_Accept:
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr:
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion:
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer:
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_HiRes_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					NIState = NI_ReqCalibValues;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_ReqCalibValues:
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues:
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready:
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut:
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
	}
  T += SysClock;
}

void __fastcall TCIISSensor_HiRes::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
	CalibValuesRecived = true;

  SensorRec->SensorIShunt = BPIn->Byte5 * 0.2 /32767 *1000;

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "Sensor HiRes Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISSensor_HiRes::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "uV";
}

void __fastcall TCIISSensor_HiRes::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->SensorIShunt;

  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
  SensorRec->SensorLastValue = AnVal1_Scl;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
  }


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;

		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U";

		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

// Camur II R

__fastcall TCIISSensor_R::TCIISSensor_R(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
	SensorIniRunning = false;
	CTRequestTempRunning = false;
  UpdateLastValueRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_R::~TCIISSensor_R()
{

}

#pragma argsused
void __fastcall TCIISSensor_R::OnSysClockTick( TDateTime TickTime )
{
	if( SensorIniRunning ) SM_SensorIni();
	else if( SensorIniStateRunning ) SM_SensorIniState();
	else if( CTRequestTempRunning ) SM_CTRequestTemp();
	else if( UpdateLastValueRunning ) SM_UpdateLastValue();
}

void __fastcall TCIISSensor_R::SM_SensorIni()
{
  if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;


			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_R_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// Calib for V < 3

			case NI_ReqCalibValues :
			CIISBICh->RequestRShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false; CalibR1Recived = false; CalibR2Recived = false; CalibR3Recived = false; CalibR4Recived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqCalibValues;
				else NIState = NI_TimeOut;
			}
			break;

			// Calib for V > 3

			case NI_SetCamurIIMode:
			CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIRecPar;
			break;

			case NI_ClrCIIIRecPar:
			CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIAlarm;
			break;

			case NI_ClrCIIIAlarm:
			CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqCalibValues_Ver3;
			break;

			case NI_ReqCalibValues_Ver3 :
			CIISBICh->RequestRShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			CalibR1Recived = false; CalibR2Recived = false; CalibR3Recived = false; CalibR4Recived = false;
			CalibR5Recived = false; CalibR6Recived = false; CalibR7Recived = false; CalibR8Recived = false;
			CalibR9Recived = false; CalibR10Recived = false; CalibR11Recived = false; CalibR12Recived = false;
			CalibR13Recived = false; CalibR14Recived = false; CalibR15Recived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues_Ver3;
			break;

			case NI_WaitForCalibValues_Ver3:
			if( CalibValuesRecived )
			{
				CTSelectRFreq(100);
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_OffsetAdj;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqCalibValues_Ver3;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_OffsetAdj :
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_DACalib;
			break;

			case NI_DACalib:
			CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
			TNextEvent += DACalibDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelMode;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_SelMode: // SensorWarmUp used for Always On
			CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 0 );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelRange;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_SelRange:
			CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, 0 );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;


}

void __fastcall TCIISSensor_R::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
	if( ( SensorRec->SensorVerMajor >= 5 )  )
	{
		int32_t intValue;
		float flValue;

		intValue = BPIn->Byte4 +
							 BPIn->Byte5 * 256 +
							 BPIn->Byte6 * 65536 +
							 BPIn->Byte7 * 16777216;

		 flValue = ntohf( intValue );

		switch (BPIn->Byte3)
		{
			case 0: SensorRec->SensorIShunt = flValue; break;
			case 1: SensorRec->SensorIShunt1 = flValue; break;
			case 2: SensorRec->SensorIShunt2 = flValue; break;
			case 3: SensorRec->SensorIShunt3 = flValue; break;
			case 4: SensorRec->SensorIShunt4 = flValue; break;
			case 5: SensorRec->SensorIShunt5 = flValue; break;
			case 6: SensorRec->SensorIShunt6 = flValue; break;
			case 7: SensorRec->SensorIShunt7 = flValue; break;
			case 8: SensorRec->SensorIShunt8 = flValue; break;
			case 9: SensorRec->SensorIShunt9 = flValue; break;
			case 10: SensorRec->SensorIShunt10 = flValue; break;
			case 11: SensorRec->SensorIShunt11 = flValue; break;
			case 12: SensorRec->SensorIShunt12 = flValue; break;
			case 13: SensorRec->SensorIShunt13 = flValue; break;
			case 14: SensorRec->SensorIShunt14 = flValue; break;
			case 15: SensorRec->SensorIShunt15 = flValue; break;
			case 16: SensorRec->SensorIShunt16 = flValue; break;
			case 17: SensorRec->SensorIShunt17 = flValue; break;
			case 18: SensorRec->SensorIShunt18 = flValue; break;
			case 19: SensorRec->SensorIShunt19 = flValue; break;
			case 20: SensorRec->SensorIShunt20 = flValue; break;
			case 21: SensorRec->SensorIShunt21 = flValue; break;
			case 22: SensorRec->SensorIShunt22 = flValue; break;
			case 23: SensorRec->SensorIShunt23 = flValue; break;
		}

		if( BPIn->Byte3 == 31 ) CalibValuesRecived = true;

		#if DebugCIISBusInt == 1
		Debug( "Sensor R Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) +
					 " / " + IntToStr( SensorRec->SensorCanAdr ) +
					 " = " + FloatToStr( flValue ));
		#endif
	}
	else
	{
		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		ADVal2 = BPIn->Byte6; if( ADVal2 > 127 ) ADVal2 = ADVal2 - 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * pow(10.0,ADVal2);

		switch (BPIn->Byte3)
		{
			case 0: SensorRec->SensorIShunt = AnVal1; CalibR1Recived = true; break;
			case 1: SensorRec->SensorIShunt1 = AnVal1; CalibR2Recived = true; break;
			case 2: SensorRec->SensorIShunt2 = AnVal1; CalibR3Recived = true; break;
			case 3: SensorRec->SensorIShunt3 = AnVal1; CalibR4Recived = true; break;
			case 4: SensorRec->SensorIShunt4 = AnVal1; CalibR5Recived = true; break;
			case 5: SensorRec->SensorIShunt5 = AnVal1; CalibR6Recived = true; break;
			case 6: SensorRec->SensorIShunt6 = AnVal1; CalibR7Recived = true; break;
			case 7: SensorRec->SensorIShunt7 = AnVal1; CalibR8Recived = true; break;
			case 8: SensorRec->SensorIShunt8 = AnVal1; CalibR9Recived = true; break;
			case 9: SensorRec->SensorIShunt9 = AnVal1; CalibR10Recived = true; break;
			case 10: SensorRec->SensorIShunt10 = AnVal1; CalibR11Recived = true; break;
			case 11: SensorRec->SensorIShunt11 = AnVal1; CalibR12Recived = true; break;
			case 12: SensorRec->SensorIShunt12 = AnVal1; CalibR13Recived = true; break;
			case 13: SensorRec->SensorIShunt13 = AnVal1; CalibR14Recived = true; break;
			case 14: SensorRec->SensorIShunt14 = AnVal1; CalibR15Recived = true; break;
			default: return;
		}
		if(( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 6 )))
		{
			CalibValuesRecived =
			CalibR15Recived && CalibR14Recived && CalibR13Recived &&
			CalibR12Recived && CalibR11Recived && CalibR10Recived && CalibR9Recived &&
			CalibR8Recived && CalibR7Recived && CalibR6Recived && CalibR5Recived &&
			CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
		}
		else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 4 ) )
		{
			CalibValuesRecived =
			CalibR12Recived && CalibR11Recived && CalibR10Recived && CalibR9Recived &&
			CalibR8Recived && CalibR7Recived && CalibR6Recived && CalibR5Recived &&
			CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
		}
		else
		{
			CalibValuesRecived = CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
		}

		BPIn->MsgMode = CIISBus_MsgReady;
		BPIn->ParseMode = CIISParseReady;

		#if DebugCIISBusInt == 1
		Debug( "Sensor R Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
			 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
		#endif
	}

}

void __fastcall TCIISSensor_R::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "ohm";
}

void __fastcall TCIISSensor_R::SensorSample( TCIISBusPacket *BPIn )
{
	double IShunt;

	TDateTime DTS;
	DTS = Now();

	if( SensorRec->SensorVerMajor < 3 )
	{
		switch( BPIn->Byte6 )
		{
			case 0: IShunt = SensorRec->SensorIShunt; break;
			case 1: IShunt = SensorRec->SensorIShunt1; break;
			case 2: IShunt = SensorRec->SensorIShunt2; break;
			case 3: IShunt = SensorRec->SensorIShunt3; break;
		}
		UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = UADVal1; AnVal1 = AnVal1 * IShunt / 10000;

		AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

		if( !FIniDecaySample )
		{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "R";
			MVR.ValueUnit = SensorRec->SensorUnit;
			MVR.Value = AnVal1_Scl;

			DB->AppendMiscValueRec( &MVR );
			P_Prj->LastValChanged();
		}

		SensorRec->SensorLastValue = AnVal1_Scl;
		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "R";
			MValRec.ValueUnit = SensorRec->SensorUnit;
			MValRec.RawValue = AnVal1;
			MValRec.Value = AnVal1_Scl;

			DB->AppendMonitorValueRec( &MValRec );
			P_Prj->DataChanged();
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			;
		}
	}
	else if( SensorRec->SensorVerMajor < 4 )
	{
		VRes = BPIn->Byte4 + BPIn->Byte5 * 256;

		AnVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
		AnVal2 = ( AnVal2 * 3 / 12.25 ) - 275;

		if( !FIniDecaySample )
		{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "T";
			MVR.ValueUnit = "C";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );
			P_Prj->LastValChanged();
		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			CIISMonitorValueRec MValRec;

			if( SensorRec->SensorTemp )
			{
				MValRec.DateTimeStamp = DTS;
				MValRec.SampleDateTime = BPIn->SampleTimeStamp;
				MValRec.RecNo = BPIn->RecordingNo;
				MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
				MValRec.ValueType = "T";
				MValRec.ValueUnit = "C";
				MValRec.RawValue = AnVal2;
				MValRec.Value = AnVal2;

				DB->AppendMonitorValueRec( &MValRec );
				P_Prj->DataChanged();
			}
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			;
		}
	}
	else  // V > 4   Temp stored in SensorSampleTemp
	{
		VRes = BPIn->Byte4 + BPIn->Byte5 * 256;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_R::SensorSampleExt( TCIISBusPacket *BPIn )
{
	if( !FIniDecaySample )
	{
	  double RErr;
	  TDateTime DTS;
	  DTS = Now();

		if( SensorRec->SensorVerMajor < 3 )
		{
			ADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
			if(( ADVal2 & 512 ) == 512 ) ADVal2 = - ((( ~ADVal2 ) & 511 ) + 1 );
			AnVal2 = ADVal2; AnVal2 = AnVal2 / 4;


			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "T";
			MVR.ValueUnit = "C";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );
			P_Prj->LastValChanged();


			SensorRec->SensorLastTemp = AnVal2;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
			}

			if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
			{
				if( SensorRec->SensorTemp )
				{
					CIISMonitorValueRec MValRec;

					MValRec.DateTimeStamp = DTS;
					MValRec.SampleDateTime = BPIn->SampleTimeStamp;
					MValRec.RecNo = BPIn->RecordingNo;
					MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
					MValRec.ValueType = "T";
					MValRec.ValueUnit = "C";
					MValRec.RawValue = AnVal2;
					MValRec.Value = AnVal2;

					DB->AppendMonitorValueRec( &MValRec );
					P_Prj->DataChanged();
				}
			}
		}
		else // Ver > 3
		{
			IRes = BPIn->Byte4 + BPIn->Byte5 * 256;
			if( IRes == 0 ) IRes = 1;

			if( ( SensorRec->SensorVerMajor >= 5 )  )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
						RErr = SensorRec->SensorIShunt2;
						IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt1;
						break;
					case 2:
						RErr = SensorRec->SensorIShunt5;
						IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt4;
						break;
					case 3:
						RErr = SensorRec->SensorIShunt8;
						IRes = SensorRec->SensorIShunt6 * IRes + SensorRec->SensorIShunt7;
						break;
					case 4:
						RErr = SensorRec->SensorIShunt11;
						IRes = SensorRec->SensorIShunt9 * IRes + SensorRec->SensorIShunt10;
						break;
					case 5:
						RErr = SensorRec->SensorIShunt14;
						IRes = SensorRec->SensorIShunt12 * IRes + SensorRec->SensorIShunt13;
						break;
					case 6:
						RErr = SensorRec->SensorIShunt17;
						IRes = SensorRec->SensorIShunt15 * IRes + SensorRec->SensorIShunt16;
						break;
					case 7:
						RErr = SensorRec->SensorIShunt20;
						IRes = SensorRec->SensorIShunt18 * IRes + SensorRec->SensorIShunt19;
						break;
					case 8:
						RErr = SensorRec->SensorIShunt23;
						IRes = SensorRec->SensorIShunt21 * IRes + SensorRec->SensorIShunt22;
						break;

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ))
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
					break;
					case 2:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
					break;
					case 3:
					RErr = SensorRec->SensorIShunt12;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
					break;
					case 4:
					if( RMeasureFreq == 1000 )
					{
					RErr = SensorRec->SensorIShunt13;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
					}
					else
					{
					RErr = SensorRec->SensorIShunt14;
					IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
					}

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 6 ) )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
					break;

					case 2:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
					break;

					case 3:
					if( RMeasureFreq == 1000 )
					{
					RErr = SensorRec->SensorIShunt12;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
					}
					else
					{
					RErr = SensorRec->SensorIShunt14;
					IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
					}

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}

			else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 4 ) )
			{
				AnVal2 = VRes / IRes;
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt8);
					AnVal2 = SensorRec->SensorIShunt * AnVal2 + SensorRec->SensorIShunt4;
					break;
					case 2:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt9);
					AnVal2 = SensorRec->SensorIShunt1 * AnVal2 + SensorRec->SensorIShunt5;
					break;
					case 3:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt10);
					AnVal2 = SensorRec->SensorIShunt2 * AnVal2 + SensorRec->SensorIShunt6;
					break;
					case 4:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt11);
					AnVal2 = SensorRec->SensorIShunt3 * AnVal2 + SensorRec->SensorIShunt7;
					break;
				}
				AnVal2 = AnVal2 * RErr /( 1 * ( RErr - AnVal2 ));
			}
			else
			{
				AnVal2 = VRes / IRes / 1000;
				switch( BPIn->Byte6 )
				{
					case 1: AnVal2 = SensorRec->SensorIShunt * AnVal2; break;
					case 2: AnVal2 = SensorRec->SensorIShunt1 * AnVal2; break;
					case 3: AnVal2 = SensorRec->SensorIShunt2 * AnVal2; break;
					case 4: AnVal2 = SensorRec->SensorIShunt3 * AnVal2; break;
				}
			}

			AnVal2_Scl = AnVal2 * SensorRec->SensorGain + SensorRec->SensorOffset;

			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "R";
			MVR.ValueUnit = SensorRec->SensorUnit;
			MVR.Value = AnVal2_Scl;

			DB->AppendMiscValueRec( &MVR );
			P_Prj->LastValChanged();


			SensorRec->SensorLastValue = AnVal2_Scl;
			if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}

			if( ((BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
			{
				CIISMonitorValueRec MValRec;

				MValRec.DateTimeStamp = DTS;
				MValRec.SampleDateTime = BPIn->SampleTimeStamp;
				MValRec.RecNo = BPIn->RecordingNo;
				MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
				MValRec.ValueType = "R";
				MValRec.ValueUnit = SensorRec->SensorUnit;
				MValRec.RawValue = AnVal2;
				MValRec.Value = AnVal2_Scl;

				DB->AppendMonitorValueRec( &MValRec );
				P_Prj->DataChanged();
			}
			else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
			{
				;
			}
		}
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_R::SensorSampleTemp( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

  #if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif

  if( AnVal2 == 0 )
	{
		AnVal2 = -300;
	}
	else if( fabs(AnVal1/AnVal2) > 7 )
  {
		AnVal2 = -300;
	}
  else
	{
		AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;
	}

  CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "T";
		MValRec.ValueUnit = "C";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_R::CTSelectRFreq( int32_t Freq )
{
	int32_t FMode;

	RMeasureFreq = Freq;

	if( Freq == 100 ) FMode = 0;
	else FMode = 1;

	if( CIISBICh != NULL ) CIISBICh->CTSelectRFreq( SensorRec->SensorCanAdr, FMode );

}

void __fastcall TCIISSensor_R::CTSelectMode( int32_t Mode )
{
	CTMode = Mode;
	if( CIISBICh != NULL ) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, Mode );
}

void __fastcall TCIISSensor_R::CTDACalib()
{
	if( CIISBICh != NULL ) CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_R::CTOffsetAdj()
{
	if( CIISBICh != NULL ) CIISBICh->CTOffsetAdj( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_R::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SensorLastValueExtRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;
			if( SensorRec->SensorTemp )
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelayResMes;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelayResMes;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISSensor_R::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && !CTRequestTempRunning &&( CIISBICh != NULL ))
	{
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		UpdateLastValueRunning = true;
		CTRequestTemp( 0 ); //CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 );
		T2 = 0;
		T2NextEvent = 100;
		T2State = 15;
	}
}

void __fastcall TCIISSensor_R::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			SensorLastValueTempRequest = true;
			CTRequestTemp( 0 ); // Tag always 0 fore Last value request
			T2NextEvent += 100;
			T2State = 15 ;
			break;

			case 15:
			if( CTRequestTempRunning )
			{
				T2NextEvent += 100;
			}
			else
			{
				T2NextEvent += ApplyUpdDelay;
				T2State = 20 ;
      }

			case 20: // Apply uppdate
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			UpdateLastValueRunning = false;
			break;
		}
	}
	T2 += SysClock;
}


void __fastcall TCIISSensor_R::SM_CTRequestTemp()
{
	if( TR >= TRNextEvent)
	{
		switch (TRState)
		{
			case 10: // Wait for ResMes sample
			TRNextEvent += 7000;
			TRState = 11 ;
			break;

			case 11: // Mode U
			CTSelectMode( 2 );
			TRNextEvent += 500;
			TRState = 12 ;
			break;

			case 12: // Offset adj
			CTOffsetAdj();
			TRNextEvent += 1000;
			TRState = 13 ;
			break;

			case 13: // DA Calib
			CTDACalib();
			TRNextEvent += 2000;
			TRState = 14 ;
			break;

			case 14: // Request temp
			CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, FRequestTag );
			TRNextEvent += 1000;
			TRState = 15 ;
			break;

			case 15: // Set Mode R
			CTSelectMode( 0 );
			CTRequestTempRunning = false;
			break;
		}
	}
	TR += SysClock;
}


void __fastcall TCIISSensor_R::CTRequestTemp( int32_t RequestTag )
{
	//if(( CIISBICh != NULL ) && ( SensorRec->SensorVerMajor >= 4 )) CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, RequestTag );

	if(( CIISBICh != NULL ) && ( SensorRec->SensorVerMajor >= 4 ))
	{
		FRequestTag = RequestTag;

		if( !CTRequestTempRunning && ( CIISBICh != NULL ))
		{
			CTRequestTempRunning = true;
			TR = 0;
			TRNextEvent = 0;
			TRState = 10;
		}
	}
}

bool __fastcall TCIISSensor_R::CTUpdatingTemp()
{
	return CTRequestTempRunning;
}

// Camur II Wenner

__fastcall TCIISSensor_Wenner::TCIISSensor_Wenner(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
	SensorIniRunning = false;
	UpdateLastValueRunning = false;


  NIState = NI_ZoneAdr;
	IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_Wenner::~TCIISSensor_Wenner()
{

}

void __fastcall TCIISSensor_Wenner::SM_SensorIni()
{
  if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;


			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_R_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					NIState = NI_ReqCalibValues_Ver3;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_ReqCalibValues_Ver3 :
			CIISBICh->RequestRShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			CalibR1Recived = false; CalibR2Recived = false; CalibR3Recived = false; CalibR4Recived = false;
			CalibR5Recived = false; CalibR6Recived = false; CalibR7Recived = false; CalibR8Recived = false;
			CalibR9Recived = false; CalibR10Recived = false; CalibR11Recived = false; CalibR12Recived = false;
			CalibR13Recived = false; CalibR14Recived = false; CalibR15Recived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues_Ver3;
			break;

			case NI_WaitForCalibValues_Ver3:
			if( CalibValuesRecived )
			{
				CTSelectRFreq(1000);
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_SelMode;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ReqCalibValues_Ver3;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_SelMode:
			CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 6 ); // Mode = 6 => Wenner
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelRange;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_SelRange:
			CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, 0 );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;


}

void __fastcall TCIISSensor_Wenner::SensorIniCalibValues( TCIISBusPacket *BPIn )
{

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  ADVal2 = BPIn->Byte6; if( ADVal2 > 127 ) ADVal2 = ADVal2 - 256;
  AnVal1 = ADVal1; AnVal1 = AnVal1 * pow(10.0,ADVal2);

  switch (BPIn->Byte3)
  {
	case 0: SensorRec->SensorIShunt = AnVal1; CalibR1Recived = true; break;
	case 1: SensorRec->SensorIShunt1 = AnVal1; CalibR2Recived = true; break;
	case 2: SensorRec->SensorIShunt2 = AnVal1; CalibR3Recived = true; break;
	case 3: SensorRec->SensorIShunt3 = AnVal1; CalibR4Recived = true; break;
	case 4: SensorRec->SensorIShunt4 = AnVal1; CalibR5Recived = true; break;
	case 5: SensorRec->SensorIShunt5 = AnVal1; CalibR6Recived = true; break;
	case 6: SensorRec->SensorIShunt6 = AnVal1; CalibR7Recived = true; break;
	case 7: SensorRec->SensorIShunt7 = AnVal1; CalibR8Recived = true; break;
	case 8: SensorRec->SensorIShunt8 = AnVal1; CalibR9Recived = true; break;
	case 9: SensorRec->SensorIShunt9 = AnVal1; CalibR10Recived = true; break;
	case 10: SensorRec->SensorIShunt10 = AnVal1; CalibR11Recived = true; break;
	case 11: SensorRec->SensorIShunt11 = AnVal1; CalibR12Recived = true; break;
	case 12: SensorRec->SensorIShunt12 = AnVal1; CalibR13Recived = true; break;
	case 13: SensorRec->SensorIShunt13 = AnVal1; CalibR14Recived = true; break;
	case 14: SensorRec->SensorIShunt14 = AnVal1; CalibR15Recived = true; break;
	default: return;
	}

	CalibValuesRecived =
	CalibR15Recived && CalibR14Recived && CalibR13Recived &&
	CalibR12Recived && CalibR11Recived && CalibR10Recived && CalibR9Recived &&
	CalibR8Recived && CalibR7Recived && CalibR6Recived && CalibR5Recived &&
	CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;


  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug( "Sensor R Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
  #endif
}

void __fastcall TCIISSensor_Wenner::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "kohm";
}

void __fastcall TCIISSensor_Wenner::SensorSample( TCIISBusPacket *BPIn )
{
	double IShunt;

	TDateTime DTS;
	DTS = Now();

	VRes = BPIn->Byte4 + BPIn->Byte5 * 256;


  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr( VRes ));
  #endif
}

void __fastcall TCIISSensor_Wenner::SensorSampleExt( TCIISBusPacket *BPIn )
{
	if( !FIniDecaySample )
	{
	  double RErr;
	  TDateTime DTS;
	  DTS = Now();

		IRes = BPIn->Byte4 + BPIn->Byte5 * 256;
		if( IRes == 0 ) IRes = 1;

		switch( BPIn->Byte6 )
		{
			case 1:
			RErr = SensorRec->SensorIShunt10;
			IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
			break;
			case 2:
			RErr = SensorRec->SensorIShunt11;
			IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
			break;
			case 3:
			RErr = SensorRec->SensorIShunt12;
			IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
			break;
			case 4:
			if( RMeasureFreq == 1000 )
			{
			RErr = SensorRec->SensorIShunt13;
			IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
			}
			else
			{
			RErr = SensorRec->SensorIShunt14;
			IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
			}

			break;
		}
		AnVal2 = ( VRes / IRes - RErr ) / 1000;
		AnVal2_Scl = AnVal2 * SensorRec->SensorGain + SensorRec->SensorOffset;

		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "R";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal2_Scl;

		DB->AppendMiscValueRec( &MVR );
		P_Prj->LastValChanged();


		SensorRec->SensorLastValue = AnVal2_Scl;
		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec );
		}

		if( ((BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "R";
			MValRec.ValueUnit = SensorRec->SensorUnit;
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;

			DB->AppendMonitorValueRec( &MValRec );
			P_Prj->DataChanged();
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			;
		}
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_Wenner::CTSelectRFreq( int32_t Freq )
{
  int32_t FMode;

	RMeasureFreq = Freq;

	if( Freq == 100 ) FMode = 0;
  else FMode = 1;

	if( CIISBICh != NULL ) CIISBICh->CTSelectRFreq( SensorRec->SensorCanAdr, FMode );

}

void __fastcall TCIISSensor_Wenner::CTSelectMode( int32_t Mode )
{
	CTMode = Mode;
	if( CIISBICh != NULL ) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, Mode );
}

void __fastcall TCIISSensor_Wenner::CTDACalib()
{
	if( CIISBICh != NULL ) CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_Wenner::CTOffsetAdj()
{
	if( CIISBICh != NULL ) CIISBICh->CTOffsetAdj( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_Wenner::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SensorLastValueExtRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;

				T2 = 0;
				T2NextEvent = ApplyUpdDelayResMes;
				T2State = 20;
		}
	}
}

void __fastcall TCIISSensor_Wenner::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 20: // Apply uppdate
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			UpdateLastValueRunning = false;
			break;
		}
	}
	T2 += SysClock;
}



//Camur II ZRA

__fastcall TCIISSensor_ZRA::TCIISSensor_ZRA(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;

  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_ZRA::~TCIISSensor_ZRA()
{

}

void __fastcall TCIISSensor_ZRA::SM_SensorIni()
{
  if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;
  
		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_OffsetAdj;
			break;

			case NI_OffsetAdj :
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_ZRA_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;

					if( SensorRec->SensorVerMajor < 3 ) NIState = NI_ReqCalibValues;
					else if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues_Ver3;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			// For version < 3

			case NI_ReqCalibValues :
			CIISBICh->RequestIShunt( SensorRec->SensorCanAdr );
			CalibValuesRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForCalibValues;
			break;

			case NI_WaitForCalibValues :
			if( CalibValuesRecived )
			{
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			//For version >= 3

			case NI_SetCamurIIMode:
				CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIRecPar;
				break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ReqCalibValues_Ver3;
				break;

			case NI_ReqCalibValues_Ver3 :
				BitValueRequested = 0;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh1BitValue;
				IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 1;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh2BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh2BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 2;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh3BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh3BitValue:
			if( BitValueRecived )
			{
				BitValueRequested = 3;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh4BitValue;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_WaitForCh4BitValue:
			if( BitValueRecived )
			{
				CIISBICh->CTSelectRFreq( SensorRec->SensorCanAdr, 1 );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_DACalib;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_DACalib:
			CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
			TNextEvent += DACalibDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelMode;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_SelMode: // SensorWarmUp used for Always On
			if( SensorRec->SensorWarmUp != 0 ) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 2 );
			else CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 1 );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelRange;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_SelRange:
			if( SensorRec->SensorVerMajor >= 5 )
			{
				CTRange = SensorRec->SensorLPRIRange;
			}
			else if(( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 2 )))
			{
				CTRange = 0; //Autorange
			}
			else
			{
				CTRange = 2;
			}
			CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, CTRange );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_ZRA::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  double IShunt;
  if( SensorRec->SensorVerMajor < 3 )
  {
	CalibValuesRecived = true;

	SensorRec->SensorIShunt = BPIn->Byte3 * 0.2 /32767;
	SensorRec->SensorIShunt1 = BPIn->Byte4 * 0.2 /32767;
	SensorRec->SensorIShunt2 = BPIn->Byte5 * 0.2 /32767;
	SensorRec->SensorIShunt3 = BPIn->Byte6 * 0.2 /32767;

	#if DebugCIISBusInt == 1
	Debug( "Sensor ZRA Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
		 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
	#endif
  }
  else
  {
	//BitValueRequested
	switch (BitValueRequested)
	{
	  case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	  case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767 ; break;  // * -1
	  case 2: SensorRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767 / 100 ; break; // * -1
	  case 3: SensorRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767 / 1000 ; break;
	}
	BitValueRecived = true;

	#if DebugCIISBusInt == 1
	Debug( "Sensor ZRA Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
			 " Range  " + IntToStr( BPIn->Byte3 ) + " = " + IntToStr( BPIn->Byte4 ));
	#endif

  }
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_ZRA::SensorIniState()
{
  if( CIISBICh != NULL )
  {
	NISState = 10;
	TNIS = 0;
	TNISNextEvent = 0;
	SensorIniStateRunning = true;
	SM_SensorIniState();
  }
}

void __fastcall TCIISSensor_ZRA::SM_SensorIniState()
{
	if( TNIS >= TNISNextEvent )
	{
		switch (NISState)
		{
		case 10:
			if( SensorRec->SensorWarmUp != 0 ) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 2 );
			else CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 1 );
			TNISNextEvent += 1000;
			NISState = 20;
			break;

		case 20:
			case NI_SelRange:
			if( SensorRec->SensorVerMajor >= 5 )
			{
				CTRange = SensorRec->SensorLPRIRange;
			}
			else if(( SensorRec->SensorVerMajor >= 4 ) || (( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 2 )))
			{
				CTRange = 0; //Autorange
			}
			else
			{
				CTRange = 2;
			}
			CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, CTRange );
			TNISNextEvent += 1000;
			NISState = 30;
			break;

		case 30:

			TNISNextEvent += 1000;
			NISState = 100;
			break;

		case 100:
			SensorIniStateRunning = false;
			break;
		}
	}
	TNIS += SysClock;
}

void __fastcall TCIISSensor_ZRA::SetDefaultValuesSubType()
{
	SensorRec->SensorUnit = "mA";
}

void __fastcall TCIISSensor_ZRA::SensorSample( TCIISBusPacket *BPIn )
{
	double IShunt;

	TDateTime DTS;
	DTS = Now();

	if( SensorRec->SensorVerMajor < 3 )  // For V>3 see SensorSampleExt
	{
		switch( BPIn->Byte6 )
		{
		case 0: IShunt = SensorRec->SensorIShunt; break;
		case 1: IShunt = SensorRec->SensorIShunt1; break;
		case 2: IShunt = SensorRec->SensorIShunt2; break;
		case 3: IShunt = SensorRec->SensorIShunt3; break;
		}

		ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
		AnVal1 = ADVal1; AnVal1 = AnVal1 * IShunt;
		AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
		SensorRec->SensorLastValue = AnVal1_Scl;

		if( !FIniDecaySample )
		{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "I";
			MVR.ValueUnit = SensorRec->SensorUnit;
			MVR.Value = AnVal1_Scl;

			DB->AppendMiscValueRec( &MVR);

			P_Prj->LastValChanged();

		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

		if( DB->LocateSensorRec( SensorRec ) )
		{
			DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "I";
			MValRec.ValueUnit = SensorRec->SensorUnit;
			MValRec.RawValue = AnVal1;
			MValRec.Value = AnVal1_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			CIISDecayValueRec DValRec;

			DValRec.DateTimeStamp = DTS;
			DValRec.SampleDateTime = BPIn->SampleTimeStamp;
			DValRec.RecNo = BPIn->RecordingNo;
			DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			DValRec.Value = AnVal1_Scl;
			DValRec.ValueType = "I";

			DB->AppendDecayValueRec( &DValRec );

			P_Prj->DataChanged();
		}
	}
	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	#if DebugCIISBusInt == 1
	Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	#endif
}

void __fastcall TCIISSensor_ZRA::SensorSampleExt( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	if( SensorRec->SensorVerMajor >= 5 ) ADVal1 = - ( BPIn->Byte4 + BPIn->Byte5 * 256 );
	else ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;


	switch( BPIn->Byte6 )
	{
		case 1: AnVal1 = SensorRec->Ch2BitVal * AnVal1; break;
		case 2: AnVal1 = SensorRec->Ch3BitVal * AnVal1; break;
		case 3: AnVal1 = SensorRec->Ch4BitVal * AnVal1; break;
	}

	AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;
	SensorRec->SensorLastValue = AnVal1_Scl;


	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "I";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec ); //DB->ApplyUpdatesSensor();
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues ))
				&& ( BPIn->RequestedTag == BPIn->Tag )) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "I";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;

		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "I";

		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
	}

	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1));
	#endif
}

void __fastcall TCIISSensor_ZRA::SensorSampleTemp( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

  ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

  #if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif

  if( AnVal2 == 0 )
	{
		AnVal2 = -300;
	}
	else if( fabs(AnVal1/AnVal2) > 7 )
  {
		AnVal2 = -300;
	}
  else
	{
		AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;
	}

	CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "T";
		MValRec.ValueUnit = "C";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_ZRA::SensorWarmUp( bool On )
{
	if(On) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 1 );
	else CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 2 );
}

void __fastcall TCIISSensor_ZRA::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SensorLastValueExtRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;
			if( SensorRec->SensorTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISSensor_ZRA::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		UpdateLastValueRunning = true;
		CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 );
		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISSensor_ZRA::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			SensorLastValueTempRequest = true;
			CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 ); // Tag always 0 fore Last value request
			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			UpdateLastValueRunning = false;
			break;
		}
	}
	T2 += SysClock;
}

void __fastcall TCIISSensor_ZRA::CTRequestTemp( int32_t RequestTag )
{
	if(( CIISBICh != NULL ) && ( SensorRec->SensorVerMajor >= 4 )) CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, RequestTag );
}

//Camur II PT

__fastcall TCIISSensor_PT::TCIISSensor_PT(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;

  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
}

__fastcall TCIISSensor_PT::~TCIISSensor_PT()
{

}

void __fastcall TCIISSensor_PT::SM_SensorIni()
{
  if( T >= TNextEvent )
	{
		if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

		switch( NIState )
		{
			case NI_Accept :
			if( !NodeDetected )
			{
				SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
				P_Ctrl->IncDetectedNodeCount();
				NodeDetected = true;
			}
			//else FIniErrorCount++;

			CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
			TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
			NIState = NI_ZoneAdr;
			break;

			case NI_ZoneAdr :
			CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
			TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_SelMode;
			break;

			case NI_SelMode:
			CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, 2 );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_OffsetAdj;
			break;

			case NI_OffsetAdj :
			CIISBICh->OffsetAdj( SensorRec->SensorCanAdr );
			TNextEvent += OffsetDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_DACalib;
			break;

			case NI_DACalib:
			CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
			TNextEvent += DACalibDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqVersion;
			break;

			case NI_ReqVersion :
			CIISBICh->RequestVer( SensorRec->SensorCanAdr );
			VerRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_WaitForVer;
			IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForVer :
			if( VerRecived )
			{
				if( SensorRec->SensorVerMajor > S_PT_SupportedVer )
				{
					SensorRec->VerNotSupported = true;
					NIState = NI_TimeOut;
				}
				else
				{
					SensorRec->VerNotSupported = false;
					if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues;
					else NIState = NI_SetCamurIIMode;
					IniRetrys = NoOffIniRetrys;
				}
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_SetCamurIIMode:
				CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIRecPar;
				break;

			case NI_ClrCIIIRecPar:
				CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ClrCIIIAlarm;
				break;

			case NI_ClrCIIIAlarm:
				CIISBICh->ClearCIIIAlarm( SensorRec->SensorCanAdr );
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_ReqCalibValues;
				break;

			case NI_ReqCalibValues :
				BitValueRequested = 0;
				CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
				BitValueRecived = false;
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_WaitForCh1BitValue;
				IniRetrys = NoOffIniRetrys;
			break;

			case NI_WaitForCh1BitValue:
			if( BitValueRecived )
			{
				TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
			else // retry
			{
				FIniErrorCount++;
				if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
				else NIState = NI_TimeOut;
			}
			break;

			case NI_Ready :
			SensorIniRunning = false;
			SensorRec->SensorConnected = true;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			case NI_TimeOut :
			SensorIniRunning = false;
			SensorRec->SensorConnected = false;
			if( DB->LocateSensorRec( SensorRec ) )
			{
				DB->SetSensorRec( SensorRec );
			}
			break;

			default:
			break;
		}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_PT::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
  double IShunt;
	//BitValueRequested
	switch (BitValueRequested)
	{
			case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
	}
	BitValueRecived = true;

	#if DebugCIISBusInt == 1
	Debug( "Sensor PT Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
			 " Range  " + IntToStr( BPIn->Byte3 ) + " = " + IntToStr( BPIn->Byte4 ));
	#endif

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_PT::SetDefaultValuesSubType()
{
}

void __fastcall TCIISSensor_PT::SensorSample( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
	AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

	SensorRec->SensorLastValue = AnVal1_Scl;
	SensorRec->SensorLastTemp = AnVal2;

	if( !FIniDecaySample )
	{
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

	if( DB->LocateSensorRec( SensorRec ) )
	{
		DB->SetSensorRec( SensorRec );
	}

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "U";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		//if( FIniDecaySample ) DValRec.SampleDateTime = IniDecayTime;
		if( FIniDecaySample && FRunDistDecay ) DValRec.SampleDateTime = IniDecayTime;
		else DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "U";
		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
	}


	BPIn->MsgMode = CIISBus_MsgReady;
	BPIn->ParseMode = CIISParseReady;

	#if DebugCIISBusInt == 1
	Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	#endif
}

void __fastcall TCIISSensor_PT::SensorSampleTemp( TCIISBusPacket *BPIn )
{
	TDateTime DTS;
	DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

  ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

  #if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif

  if( AnVal2 == 0 )
	{
		AnVal2 = -300;
	}
	else if( fabs(AnVal1/AnVal2) > 7 )
  {
		AnVal2 = -300;
	}
  else
	{
		AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;
	}

  CIISMiscValueRec MVR;

	MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

	P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "T";
		MValRec.ValueUnit = "C";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
	Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_PT::UpdateLastValues()
{
	if( FuCtrlZone )
	{
		TCIISZone_uCtrl *ZuCtrl;
		ZuCtrl = (TCIISZone_uCtrl*)P_Zone;
		SampleTimeStamp = Now();
		ZuCtrl->RequestuCtrlNodeInfo( FuCtrlNodeIndex );
	}
	else
	{
		if( !UpdateLastValueRunning && ( CIISBICh != NULL )  )
		{
			SensorLastValueRequest = true;
			SampleTimeStamp = Now();
			CIISBICh->RequestSample( SensorRec->SensorCanAdr, 0 ); // Tag 0 fore Last value request except multi channel nodes ( CT,MRE,CW...)
			UpdateLastValueRunning = true;
			if( SensorRec->SensorTemp )
			{
				T2 = 0;
				T2NextEvent = 1000;
				T2State = 10;
			}
			else
			{
				T2 = 0;
				T2NextEvent = ApplyUpdDelay;
				T2State = 20;
			}
		}
	}
}

void __fastcall TCIISSensor_PT::UpdateLastTempValues()
{
	if( !UpdateLastValueRunning && ( CIISBICh != NULL ))
	{
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		UpdateLastValueRunning = true;
		CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 );
		T2 = 0;
		T2NextEvent = ApplyUpdDelay;
		T2State = 20;
	}
}

void __fastcall TCIISSensor_PT::SM_UpdateLastValue()
{
	if( T2 >= T2NextEvent)
	{
		switch (T2State)
		{
			case 10: // Request temp
			SensorLastValueTempRequest = true;
			CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, 0 ); // Tag always 0 fore Last value request
			T2NextEvent += ApplyUpdDelay;
			T2State = 20 ;
			break;

			case 20: // Apply uppdate
			DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
			DB->ApplyUpdatesPrj();
			UpdateLastValueRunning = false;
			break;
		}
	}
	T2 += SysClock;
}

void __fastcall TCIISSensor_PT::CTRequestTemp( int32_t RequestTag )
{
	if(( CIISBICh != NULL )) CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, RequestTag );
}

bool __fastcall TCIISSensor_PT::RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval )
{
	//CIISBusInt->RequestDecayIni( SensorRec->SensorCanAdr, StartDelay, POffDelay, NoOfIniSamples, IniInterval );
	return true;
}

// Camur II Crack Watch

__fastcall TCIISSensor_CrackWatch::TCIISSensor_CrackWatch(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  SensorRec->SensorConnected = false;
}

__fastcall TCIISSensor_CrackWatch::~TCIISSensor_CrackWatch()
{

}

void __fastcall TCIISSensor_CrackWatch::SM_SensorIni()
{
  if( T >= TNextEvent )
	{

	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;

	switch( NIState )
	{
		case NI_Accept:
		if( !NodeDetected )
		{
		  SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();

			NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
		TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

		case NI_ZoneAdr:
		CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion:
		CIISBICh->RequestVer( SensorRec->SensorCanAdr );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForVer:
		if( VerRecived )
		{
			if( SensorRec->SensorVerMajor > S_CrackWatch_SupportedVer )
			{
				SensorRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				SensorRec->VerNotSupported = false;

				NIState = NI_Ready;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_Ready:
		SensorIniRunning = false;
		SensorRec->SensorConnected = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

		case NI_TimeOut:
		SensorRec->SensorConnected = false;
		SensorIniRunning = false;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

        default:
		break;
	}
  }
  T += SysClock;
}

void __fastcall TCIISSensor_CrackWatch::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "Bits";
}

void __fastcall TCIISSensor_CrackWatch::SensorSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  AnVal1 = UADVal1;
  AnVal1_Scl = AnVal1 * SensorRec->SensorGain + SensorRec->SensorOffset;

	if( !FIniDecaySample )
  {
		CIISMiscValueRec MVR;
		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "P1";
		MVR.ValueUnit = SensorRec->SensorUnit;
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR);

		P_Prj->LastValChanged();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt )) && BPIn->RequestedTag == BPIn->Tag && CheckAlarm() ) P_Prj->AlarmStatusChanged = true;

  if( DB->LocateSensorRec( SensorRec ) )
  {
		DB->SetSensorRec( SensorRec );
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;
		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "P1";
		MValRec.ValueUnit = SensorRec->SensorUnit;
		MValRec.RawValue = AnVal1;
		MValRec.Value = AnVal1_Scl;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		CIISDecayValueRec DValRec;
		DValRec.DateTimeStamp = DTS;
		DValRec.SampleDateTime = BPIn->SampleTimeStamp;
		DValRec.RecNo = BPIn->RecordingNo;
		DValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		DValRec.Value = AnVal1_Scl;
		DValRec.ValueType = "P1";
		DB->AppendDecayValueRec( &DValRec );

		P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

// Camur II CT

__fastcall TCIISSensor_CT::TCIISSensor_CT(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor( SetDB,  SetCIISBusInt,  SetDebugWin,  SetSensorRec, SetCIISParent )
{
  CIISObjType = CIISSensors;
  SensorRec = SetSensorRec;
  SensorIniRunning = false;


  NIState = NI_ZoneAdr;
  IniRetrys = NoOffIniRetrys;
  T = 0;
  TNextEvent = 0;
  ZRALast = 1;
  ZRACurrent = 1;
}

__fastcall TCIISSensor_CT::~TCIISSensor_CT()
{

}

bool __fastcall TCIISSensor_CT::SensorLPRIni()
{
	SensorRec->RequestStatus = 0;
	SensorRec->SensorLPRStep = 0;
	return false;
}

#pragma argsused
void __fastcall TCIISSensor_CT::CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode  )
{
	/*
	if(( CTCh <= SensorRec->SensorChannelCount ) && ( CIISBICh != NULL ))
	{
		CIISBICh->RequestLPRStart( SensorRec->SensorCanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, LPRMode  );
	}
	else
	{
		SensorRec->RequestStatus = 0;
	}
	*/

	SensorRec->RequestStatus = 0;
}

void __fastcall TCIISSensor_CT::SM_SensorIni()
{
	if( T >= TNextEvent )
	{
	if( T >= NodeIniTimeOut ) NIState = NI_TimeOut;
  
	switch( NIState )
	{
	  case NI_Accept :
		if( !NodeDetected )
		{
		  SensorRec->SensorCanAdr = CIISBICh->GetCanAdr();
		  P_Ctrl->IncDetectedNodeCount();
		  NodeDetected = true;
		}
		//else FIniErrorCount++;

		CIISBICh->SendAccept( SensorRec->SensorSerialNo, SensorRec->SensorCanAdr );
		TNextEvent = TNextEvent + AcceptDelay + ( SensorRec->SensorCanAdr - CANStartAdrNode ) * NodToNodDelay;
		NIState = NI_ZoneAdr;
		break;

	  case NI_ZoneAdr :
		CIISBICh->SetGroup( SensorRec->SensorCanAdr, SensorRec->ZoneCanAdr );
		TNextEvent += AcceptDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_ReqVersion;
		break;

		case NI_ReqVersion :
		CIISBICh->RequestVer( SensorRec->SensorCanAdr );
		VerRecived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForVer;
		IniRetrys = NoOffIniRetrys;
		break;

		case NI_WaitForVer :
		if( VerRecived )
		{
			if( SensorRec->SensorVerMajor > S_CTx8_SupportedVer )
			{
				SensorRec->VerNotSupported = true;
				NIState = NI_TimeOut;
			}
			else
			{
				SensorRec->VerNotSupported = false;
				if( SensorRec->SensorVerMajor < 5 ) NIState = NI_ReqCalibValues;
				else NIState = NI_SetCamurIIMode;
				IniRetrys = NoOffIniRetrys;
			}
		}
		else // retry
		{
			FIniErrorCount++;
			if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
			else NIState = NI_TimeOut;
		}
		break;

		case NI_SetCamurIIMode:
			CIISBICh->SetCamurIIMode( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ClrCIIIRecPar;
			break;

		case NI_ClrCIIIRecPar:
			CIISBICh->ClearCIIIRecPar( SensorRec->SensorCanAdr );
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_ReqCalibValues;
			break;

		case NI_ReqCalibValues :
		CIISBICh->RequestRShunt( SensorRec->SensorCanAdr );
		CalibValuesRecived = false;
		CalibR1Recived = false; CalibR2Recived = false; CalibR3Recived = false; CalibR4Recived = false;
		CalibR5Recived = false; CalibR6Recived = false; CalibR7Recived = false; CalibR8Recived = false;
		CalibR9Recived = false; CalibR10Recived = false; CalibR11Recived = false; CalibR12Recived = false;
		CalibR13Recived = false; CalibR14Recived = false; CalibR15Recived = false;
		TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		NIState = NI_WaitForCalibValues;
		break;

		case NI_WaitForCalibValues :
		if( CalibValuesRecived )
		{
		  BitValueRequested = 0;
			CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
			BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh1BitValue;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;


	  case NI_WaitForCh1BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 1;
		  CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
		  BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh2BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh2BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 2;
		  CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
		  BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh3BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh3BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 3;
		  CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
		  BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh4BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh4BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 4;
		  CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
		  BitValueRecived = false;
		  TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh5BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh5BitValue:
		if( BitValueRecived )
		{
		  BitValueRequested = 5;
		  CIISBICh->RequestBitVal( SensorRec->SensorCanAdr, BitValueRequested );
		  BitValueRecived = false;
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
		  NIState = NI_WaitForCh6BitValue;
		  IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

	  case NI_WaitForCh6BitValue:
		if( BitValueRecived )
		{
			CTSelectRFreq(1000);
			TNextEvent += FastCmdDelay + P_Ctrl->DetectedNodeCount * NodToNodDelay;
			NIState = NI_Ready;
			IniRetrys = NoOffIniRetrys;
		}
		else // retry
		{
		  FIniErrorCount++;
		  if( IniRetrys-- > 0 ) NIState = NI_ZoneAdr;
		  else NIState = NI_TimeOut;
		}
		break;

		case NI_Ready :
		SensorIniRunning = false;
		SensorRec->SensorConnected = true;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

	  case NI_TimeOut :
	  SensorIniRunning = false;
		SensorRec->SensorConnected = false;
		if( DB->LocateSensorRec( SensorRec ) )
		{
		  DB->SetSensorRec( SensorRec );
		}
		break;

        default:
		break;
	}
  }
	T += SysClock;


}

void __fastcall TCIISSensor_CT::SensorIniCalibValues( TCIISBusPacket *BPIn )
{
	if( BPIn->MessageCommand == MSG_TX_SHUNTS )
	{
		if( ( SensorRec->SensorVerMajor >= 5 )  )
		{
			int32_t intValue;
			float flValue;

			intValue = BPIn->Byte4 +
								 BPIn->Byte5 * 256 +
								 BPIn->Byte6 * 65536 +
								 BPIn->Byte7 * 16777216;

			 flValue = ntohf( intValue );

			switch (BPIn->Byte3)
			{
				case 0: SensorRec->SensorIShunt = flValue; break;
				case 1: SensorRec->SensorIShunt1 = flValue; break;
				case 2: SensorRec->SensorIShunt2 = flValue; break;
				case 3: SensorRec->SensorIShunt3 = flValue; break;
				case 4: SensorRec->SensorIShunt4 = flValue; break;
				case 5: SensorRec->SensorIShunt5 = flValue; break;
				case 6: SensorRec->SensorIShunt6 = flValue; break;
				case 7: SensorRec->SensorIShunt7 = flValue; break;
				case 8: SensorRec->SensorIShunt8 = flValue; break;
				case 9: SensorRec->SensorIShunt9 = flValue; break;
				case 10: SensorRec->SensorIShunt10 = flValue; break;
				case 11: SensorRec->SensorIShunt11 = flValue; break;
				case 12: SensorRec->SensorIShunt12 = flValue; break;
				case 13: SensorRec->SensorIShunt13 = flValue; break;
				case 14: SensorRec->SensorIShunt14 = flValue; break;
				case 15: SensorRec->SensorIShunt15 = flValue; break;
				case 16: SensorRec->SensorIShunt16 = flValue; break;
				case 17: SensorRec->SensorIShunt17 = flValue; break;
				case 18: SensorRec->SensorIShunt18 = flValue; break;
				case 19: SensorRec->SensorIShunt19 = flValue; break;
				case 20: SensorRec->SensorIShunt20 = flValue; break;
				case 21: SensorRec->SensorIShunt21 = flValue; break;
				case 22: SensorRec->SensorIShunt22 = flValue; break;
				case 23: SensorRec->SensorIShunt23 = flValue; break;
			}

			if( BPIn->Byte3 == 31 ) CalibValuesRecived = true;

			#if DebugCIISBusInt == 1
			Debug( "Sensor R Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) +
						 " / " + IntToStr( SensorRec->SensorCanAdr ) +
						 " = " + FloatToStr( flValue ));
			#endif
		}
		else
		{
			ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
			ADVal2 = BPIn->Byte6; if( ADVal2 > 127 ) ADVal2 = ADVal2 - 256;
			AnVal1 = ADVal1; AnVal1 = AnVal1 * pow(10.0,ADVal2);

			switch (BPIn->Byte3)
			{
				case 0: SensorRec->SensorIShunt = AnVal1; CalibR1Recived = true; break;
				case 1: SensorRec->SensorIShunt1 = AnVal1; CalibR2Recived = true; break;
				case 2: SensorRec->SensorIShunt2 = AnVal1; CalibR3Recived = true; break;
				case 3: SensorRec->SensorIShunt3 = AnVal1; CalibR4Recived = true; break;
				case 4: SensorRec->SensorIShunt4 = AnVal1; CalibR5Recived = true; break;
				case 5: SensorRec->SensorIShunt5 = AnVal1; CalibR6Recived = true; break;
				case 6: SensorRec->SensorIShunt6 = AnVal1; CalibR7Recived = true; break;
				case 7: SensorRec->SensorIShunt7 = AnVal1; CalibR8Recived = true; break;
				case 8: SensorRec->SensorIShunt8 = AnVal1; CalibR9Recived = true; break;
				case 9: SensorRec->SensorIShunt9 = AnVal1; CalibR10Recived = true; break;
				case 10: SensorRec->SensorIShunt10 = AnVal1; CalibR11Recived = true; break;
				case 11: SensorRec->SensorIShunt11 = AnVal1; CalibR12Recived = true; break;
				case 12: SensorRec->SensorIShunt12 = AnVal1; CalibR13Recived = true; break;
				case 13: SensorRec->SensorIShunt13 = AnVal1; CalibR14Recived = true; break;
				case 14: SensorRec->SensorIShunt14 = AnVal1; CalibR15Recived = true; break;

				default: return;
			}

			if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 2 ) )
			{
				CalibValuesRecived =
				CalibR15Recived && CalibR14Recived && CalibR13Recived &&
				CalibR12Recived && CalibR11Recived && CalibR10Recived && CalibR9Recived &&
				CalibR8Recived && CalibR7Recived && CalibR6Recived && CalibR5Recived &&
				CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
			}
			else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 8 ) )
			{
				CalibValuesRecived =
				CalibR12Recived && CalibR11Recived && CalibR10Recived && CalibR9Recived &&
				CalibR8Recived && CalibR7Recived && CalibR6Recived && CalibR5Recived &&
				CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
			}
			else
			{
				CalibValuesRecived = CalibR4Recived && CalibR3Recived && CalibR2Recived && CalibR1Recived;
			}

			#if DebugCIISBusInt == 1
			Debug( "Sensor R Calib recived : " + IntToStr( SensorRec->SensorSerialNo ) + " / " + IntToStr( SensorRec->SensorCanAdr ) +
					 " = " + IntToStr( BPIn->Byte3 ) + ", " + IntToStr( BPIn->Byte4 ) + ", " + IntToStr( BPIn->Byte5 ) + ", " + IntToStr( BPIn->Byte6 ) );
			#endif
		}
	}
  else if ( BPIn->MessageCommand == MSG_TX_RANGE )
  {
		switch (BitValueRequested)
		{
			case 0: SensorRec->Ch1BitVal = BPIn->Byte4 * 0.2 / 32767 * 1000; break;
			case 1: SensorRec->Ch2BitVal = BPIn->Byte4 * 0.2 / 32767 * -1; break;
			case 2: SensorRec->Ch3BitVal = BPIn->Byte4 * 0.2 / 32767 / 100 * -1; break;
			case 3: SensorRec->Ch4BitVal = BPIn->Byte4 * 0.2 / 32767 / 1000 * -1; break;
			case 4: SensorRec->Ch5BitVal = BPIn->Byte4 * 0.2 / 32767 / 1000 * -1; break;
			case 5: SensorRec->Ch6BitVal = BPIn->Byte4 * 0.2 / 32767; break;
		}
		BitValueRecived = true;
  }
  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;
}

void __fastcall TCIISSensor_CT::SetDefaultValuesSubType()
{
  SensorRec->SensorUnit = "";
  SensorRec->SensorUnit2 = "";
	SensorRec->SensorChannelCount = 4;
	SensorRec->SensorLPRIRange = 2;
}

void __fastcall TCIISSensor_CT::SensorSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  VRes = UADVal1;

  AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
  AnVal1_Scl = AnVal1;// * SensorRec->SensorGain + SensorRec->SensorOffset;

	if(( CTMode == 2 ) && !FIniDecaySample ) // P-Mode
  {
		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "U" + IntToStr( BPIn->Tag - 17 );
		MVR.ValueUnit = "mV";
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
  }

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		;
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		;
  }
  else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		if( CTMode == 2 ) // P-Mode
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "U" + IntToStr( BPIn->Tag - 17 );
			MValRec.ValueUnit = "mV";
			MValRec.RawValue = AnVal1;
			MValRec.Value = AnVal1_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal1));
  #endif
}

void __fastcall TCIISSensor_CT::SensorSampleExt( TCIISBusPacket *BPIn )
{
	double RErr;
  TDateTime DTS;
  DTS = Now();

	if( !FIniDecaySample )
  {
		if( CTMode == 0 ) // R-Mode
		{
			UADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
			IRes = UADVal2;

			#if DebugCIISSensor == 1
			Debug("Calc R: VRes = " + FloatToStr(VRes));
			Debug("Calc R: IRes = " + FloatToStr(IRes));
			#endif

			if( IRes == 0 )
			{
			IRes = 1;

			#if DebugCIISSensor == 1
			Debug("Calc Temp: IRes = 0 -> Set to 1");
			#endif
			}

						// Camur III
/*
  			Current =  LastValueRawCH3 * RSlope[SclIdx] + ROffset[SclIdx];
  			Voltage =  LastValueRawCH1;

				LastValueResistace = Voltage / Current  -  RMux[SclIdx];
*/

			if( ( SensorRec->SensorVerMajor >= 5 )  )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
						RErr = SensorRec->SensorIShunt2;
						IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt1;
						break;
					case 2:
						RErr = SensorRec->SensorIShunt5;
						IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt4;
						break;
					case 3:
						RErr = SensorRec->SensorIShunt8;
						IRes = SensorRec->SensorIShunt6 * IRes + SensorRec->SensorIShunt7;
						break;
					case 4:
						RErr = SensorRec->SensorIShunt11;
						IRes = SensorRec->SensorIShunt9 * IRes + SensorRec->SensorIShunt10;
						break;
					case 5:
						RErr = SensorRec->SensorIShunt14;
						IRes = SensorRec->SensorIShunt12 * IRes + SensorRec->SensorIShunt13;
						break;
					case 6:
						RErr = SensorRec->SensorIShunt17;
						IRes = SensorRec->SensorIShunt15 * IRes + SensorRec->SensorIShunt16;
						break;
					case 7:
						RErr = SensorRec->SensorIShunt20;
						IRes = SensorRec->SensorIShunt18 * IRes + SensorRec->SensorIShunt19;
						break;
					case 8:
						RErr = SensorRec->SensorIShunt23;
						IRes = SensorRec->SensorIShunt21 * IRes + SensorRec->SensorIShunt22;
						break;

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 2 ) )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
					break;
					case 2:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
					break;
					case 3:
					RErr = SensorRec->SensorIShunt12;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
					break;
					case 4:
					if( RMeasureFreq == 1000 )
					{
					RErr = SensorRec->SensorIShunt13;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
					}
					else
					{
					RErr = SensorRec->SensorIShunt14;
					IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
					}

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 1 ) )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt8;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt4;
					break;
					case 2:
					RErr = SensorRec->SensorIShunt9;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt5;
					break;
					case 3:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt6;
					break;
					case 4:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt7;
					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 8 ) )
			{
				AnVal2 = VRes / IRes;
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt8);
					AnVal2 = SensorRec->SensorIShunt * AnVal2 + SensorRec->SensorIShunt4;
					break;
					case 2:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt9);
					AnVal2 = SensorRec->SensorIShunt1 * AnVal2 + SensorRec->SensorIShunt5;
					break;
					case 3:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt10);
					AnVal2 = SensorRec->SensorIShunt2 * AnVal2 + SensorRec->SensorIShunt6;
					break;
					case 4:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt11);
					AnVal2 = SensorRec->SensorIShunt3 * AnVal2 + SensorRec->SensorIShunt7;
					break;
				}
				AnVal2 = ( AnVal2 * RErr / ( RErr - AnVal2 )) / 1000;
			}
			else
			{
				AnVal2 = VRes / IRes / 1000;
				switch( BPIn->Byte6 )
				{
					case 1: AnVal2 = SensorRec->SensorIShunt * AnVal2; break;
					case 2: AnVal2 = SensorRec->SensorIShunt1 * AnVal2; break;
					case 3: AnVal2 = SensorRec->SensorIShunt2 * AnVal2; break;
					case 4: AnVal2 = SensorRec->SensorIShunt3 * AnVal2; break;
				}
			}

			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "R" + IntToStr( BPIn->Tag - 3 );
			MVR.ValueUnit = "kOhm";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();

			AnVal2_Scl = AnVal2;
		}
		else if( CTMode == 1 ) // ZRA-Mode
		{
			ADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
			AnVal2 = ADVal2;

			switch( BPIn->Byte6 )
			{
				case 0: AnVal2 = SensorRec->Ch1BitVal * AnVal2; break;
				case 1: AnVal2 = SensorRec->Ch2BitVal * AnVal2; break;
				case 2: AnVal2 = SensorRec->Ch3BitVal * AnVal2; break;
				case 3: AnVal2 = SensorRec->Ch4BitVal * AnVal2; break;
				case 4: AnVal2 = SensorRec->Ch5BitVal * AnVal2; break;
				case 5: AnVal2 = SensorRec->Ch6BitVal * AnVal2; break;
			}

			ZRALast = ZRACurrent;
			ZRACurrent = AnVal2;

			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "I" + IntToStr( BPIn->Tag - 10 );
			MVR.ValueUnit = "mA";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();

			AnVal2_Scl = AnVal2;
		}
		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			;
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			;
		}
		else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat || BPIn->RecType == RT_RExt ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			if( CTMode == 1 ) // ZRA-Mode
			{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "I" + IntToStr( BPIn->Tag - 10 );
			MValRec.ValueUnit = "mA";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
			}
			else if( CTMode == 0 ) // R-Mode
			{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "R" + IntToStr( BPIn->Tag - 3 );
			MValRec.ValueUnit = "kOhm";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
			}
		}
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif

}

void __fastcall TCIISSensor_CT::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

	if( !(( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 3 )))
	{
		AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
		AnVal2 = AnVal2 * SensorRec->Ch6BitVal * 1000;
	}

  #if DebugCIISSensor == 1
  Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
  #endif

  if( AnVal2 == 0 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = 0");
	#endif

  }
  else if( fabs(AnVal1/AnVal2) > 7 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1/AnVal2 > 7 AnVal2 set to -300");
	#endif
  }
  else
  {
	AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif
  }

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

  if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = DTS;
	MValRec.SampleDateTime = BPIn->SampleTimeStamp;
	MValRec.RecNo = BPIn->RecordingNo;
	MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
	MValRec.ValueType = "T";
	MValRec.ValueUnit = "C";
	MValRec.RawValue = AnVal2;
	MValRec.Value = AnVal2;

	DB->AppendMonitorValueRec( &MValRec );

	P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_CT::SensorLPRSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte3 + BPIn->Byte4 * 256;
  ADVal2 = BPIn->Byte5 + BPIn->Byte6 * 256;

  AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

  AnVal2 = ADVal2;
  switch( CTRange )
  {
	case 1: AnVal2 = -SensorRec->Ch2BitVal * AnVal2; break;
	case 2: AnVal2 = -SensorRec->Ch3BitVal * AnVal2; break;
	case 3: AnVal2 = -SensorRec->Ch4BitVal * AnVal2; break;
	case 4: AnVal2 = -SensorRec->Ch5BitVal * AnVal2; break;
	case 5: AnVal2 = -SensorRec->Ch6BitVal * AnVal2; break;
  }

	SensorRec->SensorLastValue = AnVal1;

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "U" + IntToStr(CTCh);
  MVR.ValueUnit = "mV";
  MVR.Value = AnVal1;

  DB->AppendMiscValueRec( &MVR);

  MVR.ValueType = "I" + IntToStr(CTCh);
  MVR.ValueUnit = "mA";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	SensorRec->SensorLPRStep = SensorRec->SensorLPRStep + 1;
  if( DB->LocateSensorRec( SensorRec ) )
  {
	DB->SetSensorRec( SensorRec );
  }

  CIISLPRValueRec LPRValRec;

  LPRValRec.DateTimeStamp = DTS;
  LPRValRec.RecNo = BPIn->RecordingNo;
  LPRValRec.SensorSerialNo = SensorRec->SensorSerialNo;
  LPRValRec.ValueType = "C" + IntToStr(CTCh);
  LPRValRec.ValueV = AnVal1;
  LPRValRec.ValueI = AnVal2;

  DB->AppendLPRValueRec( &LPRValRec );
	//DB->ApplyUpdatesLPRValue();
	//P_Prj->DataChanged();
	//DB->ApplyUpdatesSensor();
	//DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
	//DB->ApplyUpdatesPrj();

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

   #if DebugCIISBusInt == 1
  Debug("Incoming LPRSample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " V = " + FloatToStr(AnVal1) + " / I = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_CT::CTSelectMode( int32_t Mode )
{
	CTMode = Mode;
	if( CIISBICh != NULL ) CIISBICh->CTSelectMode( SensorRec->SensorCanAdr, Mode );
}

void __fastcall TCIISSensor_CT::CTSelectRange( int32_t Range )
{
	if( Range == -1 )
	{
		Range = SensorRec->SensorLPRIRange;
	}

  CTRange = Range;
	if( CIISBICh != NULL ) CIISBICh->CTSelectRange( SensorRec->SensorCanAdr, Range );
}

void __fastcall TCIISSensor_CT::CTSelectRFreq( int32_t Freq )
{
  int32_t FMode;

	RMeasureFreq = Freq;

	if( Freq == 100 ) FMode = 0;
  else FMode = 1;

	if( CIISBICh != NULL ) CIISBICh->CTSelectRFreq( SensorRec->SensorCanAdr, FMode );

}

void __fastcall TCIISSensor_CT::CTSelectCh( int32_t Ch )
{
	CTCh = Ch;
  if( CIISBICh != NULL ) CIISBICh->CTSelectCh( SensorRec->SensorCanAdr, Ch );
}

void __fastcall TCIISSensor_CT::CTDACalib()
{
	if( CIISBICh != NULL ) CIISBICh->CTDACalib( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_CT::CTOffsetAdj()
{
	if( CIISBICh != NULL ) CIISBICh->CTOffsetAdj( SensorRec->SensorCanAdr );
}

void __fastcall TCIISSensor_CT::CTRequestSample( int32_t RequestTag )
{
  if( CIISBICh != NULL ) CIISBICh->RequestSample( SensorRec->SensorCanAdr, RequestTag );
}

void __fastcall TCIISSensor_CT::CTRequestTemp( int32_t RequestTag )
{
  if( CIISBICh != NULL ) CIISBICh->CTRequestTemp( SensorRec->SensorCanAdr, RequestTag );
}


double __fastcall TCIISSensor_CT::CTZRAChange()
{
 double Change;

 if( ZRALast == 0 ) Change = 0;
 else Change = fabs(( ZRACurrent - ZRALast ) / ZRALast );

 return Change;
 }

// Camur II MRE

__fastcall TCIISSensor_MRE::TCIISSensor_MRE(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor_CT( SetDB, SetCIISBusInt, SetDebugWin, SetSensorRec, SetCIISParent )
{

}

__fastcall TCIISSensor_MRE::~TCIISSensor_MRE()
{

}

void __fastcall TCIISSensor_MRE::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

	if( !(( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 3 )))
	{
		AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
		AnVal2 = AnVal2 * SensorRec->Ch6BitVal * 1000;
	}


  #if DebugCIISSensor == 1
  Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
  #endif

  if( AnVal2 == 0 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = 0");
	#endif

  }
  else if( fabs(AnVal1/AnVal2) > 7 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1/AnVal2 > 7 AnVal2 set to -300");
	#endif
  }
  else
  {
	AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif
  }

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		CIISMonitorValueRec MValRec;

		MValRec.DateTimeStamp = DTS;
		MValRec.SampleDateTime = BPIn->SampleTimeStamp;
		MValRec.RecNo = BPIn->RecordingNo;
		MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
		MValRec.ValueType = "T";
		MValRec.ValueUnit = "C";
		MValRec.RawValue = AnVal2;
		MValRec.Value = AnVal2;

		DB->AppendMonitorValueRec( &MValRec );

		P_Prj->DataChanged();
  }

	BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}

void __fastcall TCIISSensor_MRE::SensorSample( TCIISBusPacket *BPIn )
{

  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  VRes = UADVal1;

	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		;
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
  {
		;
  }
  else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(UADVal1));
  #endif
}

void __fastcall TCIISSensor_MRE::SensorSampleExt( TCIISBusPacket *BPIn )
{
  double RErr;
  TDateTime DTS;
	DTS = Now();

	if(( CTMode == 0 ) && !FIniDecaySample ) // R-Mode
	{
		UADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
		IRes = UADVal2;

		#if DebugCIISSensor == 1
		Debug("Calc R: VRes = " + FloatToStr(VRes));
		Debug("Calc R: IRes = " + FloatToStr(IRes));
		#endif

		if( IRes == 0 )
		{
			IRes = 1;

			#if DebugCIISSensor == 1
			Debug("Calc Temp: IRes = 0 -> Set to 1");
			#endif
		}

					// Camur III
/*
  			Current =  LastValueRawCH3 * RSlope[SclIdx] + ROffset[SclIdx];
  			Voltage =  LastValueRawCH1;

				LastValueResistace = Voltage / Current  -  RMux[SclIdx];
*/

			if( ( SensorRec->SensorVerMajor >= 5 )  )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
						RErr = SensorRec->SensorIShunt2;
						IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt1;
						break;
					case 2:
						RErr = SensorRec->SensorIShunt5;
						IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt4;
						break;
					case 3:
						RErr = SensorRec->SensorIShunt8;
						IRes = SensorRec->SensorIShunt6 * IRes + SensorRec->SensorIShunt7;
						break;
					case 4:
						RErr = SensorRec->SensorIShunt11;
						IRes = SensorRec->SensorIShunt9 * IRes + SensorRec->SensorIShunt10;
						break;
					case 5:
						RErr = SensorRec->SensorIShunt14;
						IRes = SensorRec->SensorIShunt12 * IRes + SensorRec->SensorIShunt13;
						break;
					case 6:
						RErr = SensorRec->SensorIShunt17;
						IRes = SensorRec->SensorIShunt15 * IRes + SensorRec->SensorIShunt16;
						break;
					case 7:
						RErr = SensorRec->SensorIShunt20;
						IRes = SensorRec->SensorIShunt18 * IRes + SensorRec->SensorIShunt19;
						break;
					case 8:
						RErr = SensorRec->SensorIShunt23;
						IRes = SensorRec->SensorIShunt21 * IRes + SensorRec->SensorIShunt22;
						break;

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 2 ) )
		{
			switch( BPIn->Byte6 )
			{
			case 1:
				RErr = SensorRec->SensorIShunt10;
				IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
			break;
				case 2:
				RErr = SensorRec->SensorIShunt11;
				IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
				break;
			case 3:
				RErr = SensorRec->SensorIShunt12;
				IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
				break;
			case 4:
				if( RMeasureFreq == 1000 )
				{
					RErr = SensorRec->SensorIShunt13;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
				}
				else
				{
					RErr = SensorRec->SensorIShunt14;
					IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
				}
				break;
			}
			AnVal2 = ( VRes / IRes - RErr ) / 1000;
		}
		else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 1 ) )
		{
			switch( BPIn->Byte6 )
			{
			case 1:
				RErr = SensorRec->SensorIShunt8;
				IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt4;
				break;
			case 2:
				RErr = SensorRec->SensorIShunt9;
				IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt5;
				break;
			case 3:
				RErr = SensorRec->SensorIShunt10;
				IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt6;
				break;
			case 4:
				RErr = SensorRec->SensorIShunt11;
				IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt7;
				break;
			}
			AnVal2 = ( VRes / IRes - RErr ) / 1000;
		}
		else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 8 ) )
		{
			AnVal2 = VRes / IRes;
			switch( BPIn->Byte6 )
			{
			case 1:
				RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt8);
				AnVal2 = SensorRec->SensorIShunt * AnVal2 + SensorRec->SensorIShunt4;
				break;
			case 2:
				RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt9);
				AnVal2 = SensorRec->SensorIShunt1 * AnVal2 + SensorRec->SensorIShunt5;
				break;
			case 3:
				RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt10);
				AnVal2 = SensorRec->SensorIShunt2 * AnVal2 + SensorRec->SensorIShunt6;
				break;
			case 4:
				RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt11);
				AnVal2 = SensorRec->SensorIShunt3 * AnVal2 + SensorRec->SensorIShunt7;
				break;
			}
			AnVal2 = ( AnVal2 * RErr / ( RErr - AnVal2 )) / 1000;
		}
		else
		{
			AnVal2 = VRes / IRes / 1000;
			switch( BPIn->Byte6 )
			{
				case 1: AnVal2 = SensorRec->SensorIShunt * AnVal2; break;
				case 2: AnVal2 = SensorRec->SensorIShunt1 * AnVal2; break;
				case 3: AnVal2 = SensorRec->SensorIShunt2 * AnVal2; break;
				case 4: AnVal2 = SensorRec->SensorIShunt3 * AnVal2; break;
			}
		}

		if( BPIn->Tag == 0 ) // Skip LastValue request on group
		{
			;
		}
		else if(  BPIn->Tag > 210 ) // R-Mode LastValue
		{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "R" + IntToStr( BPIn->Tag - 210 );
			MVR.ValueUnit = "kOhm";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();
		}
		else if( ((BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			if( BPIn->Tag > 200 ) // R-Mode Monitor
			{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType = "R" + IntToStr( BPIn->Tag - 200 );
			MVR.ValueUnit = "kOhm";
			MVR.Value = AnVal2;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();

			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "R" + IntToStr( BPIn->Tag - 200 );
			MValRec.ValueUnit = "kOhm";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
			}
		}
		else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{

		}

  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_MRE::UpdateLastValues()
{
	if( !UpdateLastValueRunning )
  {
		UpdateLastValueRunning = true;
		T_MRE = 0;
		TNextEvent_MRE = 0;
		State_MRE = 20;
  }
}

void __fastcall TCIISSensor_MRE::SM_UpdateLastValue()
{
  if( T_MRE >= TNextEvent_MRE)
  {
	switch (State_MRE)
	{

	case 20: // P-Mode
		CTSelectMode( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 21 ;
	  T_MRE += SysClock;
	  break;

	case 21: // Range = 2
		CTSelectRange( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 22 ;
	  T_MRE += SysClock;
	  break;

	case 22: // R Meassuring freq. = 1000 Hz
		CTSelectRFreq(1000);
	  TNextEvent_MRE += 500;
	  State_MRE = 23 ;
	  T_MRE += SysClock;
	  break;

	case 23: // Channel = 0;
	  CTSelectCh( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 24 ;
	  T_MRE += SysClock;
	  break;

	case 24: // Adj. Offset
	  CTOffsetAdj();
	  TNextEvent_MRE += 2000;
	  State_MRE = 25 ;
	  T_MRE += SysClock;
	  break;

	case 25: // Calibrate DA
	  CTDACalib();
	  TNextEvent_MRE += 3000;
	  State_MRE = 26 ;
	  T_MRE += SysClock;
	  break;

	case 26: // Request temp
		SensorLastValueTempRequest = true;
	  SampleTimeStamp = Now();
	  CTRequestTemp( 210 );
	  TNextEvent_MRE += 3000;
	  State_MRE = 50 ;
	  T_MRE += SysClock;
	  break;

	case 50: // R-Mode
		CTSelectMode( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 51 ;
	  T_MRE += SysClock;
	  break;

	case 51: // Range = 0
		CTSelectRange( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 52 ;
	  T_MRE += SysClock;
	  break;

	case 52: // Channel = 0;
	  CTSelectCh( 0 );
	  TNextEvent_MRE += 500;
	  State_MRE = 53 ;
	  T_MRE += SysClock;
	  break;

	case 53: // 1000 HZ
		CTSelectRFreq(1000);
	  TNextEvent_MRE += 500;
	  MRESelectedCh = 1;
	  State_MRE = 55 ;
	  T_MRE += SysClock;
	  break;

	case 55: // Channel = 1-8
	  CTSelectCh( MRESelectedCh );
	  TNextEvent_MRE = 200;
	  State_MRE = 56 ;
	  T_MRE = SysClock;
	  break;

	case 56: // Request sample ch 1-7
		SensorLastValueRequest = true;
		SensorLastValueExtRequest = true;
	  SampleTimeStamp = Now();
	  CTRequestSample( 210 + MRESelectedCh );
	  {
		MRESelectedCh++;
		if( MRESelectedCh < 8 ) State_MRE = 55 ;
		else State_MRE = 900;
	  }
	  DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
	  DB->ApplyUpdatesPrj();
	  TNextEvent_MRE += 2000;
	  T_MRE += SysClock;
	  break;

	case 900:
	  T_MRE = SysClock;
	  TNextEvent_MRE = ApplyUpdDelay;
	  State_MRE = 910;
	  break;

	case 910:
	  DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
	  DB->ApplyUpdatesPrj();

	  T_MRE += SysClock;
	  TNextEvent_MRE += 200;
	  State_MRE = 950;
	  break;

	case 950: // P-Mode
		CTSelectMode( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 951 ;
	  T_MRE += SysClock;
	  break;

	case 951: // Range = 2
		CTSelectRange( 2 );
	  TNextEvent_MRE += 500;
	  State_MRE = 953 ;
	  T_MRE += SysClock;
	  break;

	case 953: // Channel = 1;
	  CTSelectCh( 1 );
	  TNextEvent_MRE += 500;
	  State_MRE = 999 ;
	  T_MRE += SysClock;
	  break;

	case 999:
		UpdateLastValueRunning = false;
	  break;
	}
  }
  else T_MRE += SysClock;
}

// Camur II CW

__fastcall TCIISSensor_CW::TCIISSensor_CW(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor_CT( SetDB, SetCIISBusInt, SetDebugWin, SetSensorRec, SetCIISParent )
{

}

__fastcall TCIISSensor_CW::~TCIISSensor_CW()
{

}

void __fastcall TCIISSensor_CW::SetDefaultValuesSubType()
{
	SensorRec->SensorUnit = "";
	SensorRec->SensorUnit2 = "";
	SensorRec->SensorChannelCount = 4;
	SensorRec->SensorLPRIRange = 2;
}

bool __fastcall TCIISSensor_CW::SensorLPRIni()
{
	bool SensorStarted;

	if( CTCh <= SensorRec->SensorChannelCount )
	{
		SensorRec->RequestStatus = -1;
		SensorRec->SensorLPRStep = 0;
		SensorStarted = true;
	}
	else
	{
		SensorRec->RequestStatus = 0;
		SensorRec->SensorLPRStep = 0;
		SensorStarted = false;
	}
	return SensorStarted;
}

void __fastcall TCIISSensor_CW::CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode  )
{
	if(( CTCh <= SensorRec->SensorChannelCount ) && ( CIISBICh != NULL ))
	{
		CIISBICh->RequestLPRStart( SensorRec->SensorCanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, LPRMode  );
	}
	else
	{
		SensorRec->RequestStatus = 0;
	}
}

void __fastcall TCIISSensor_CW::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

	if( !(( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 3 )))
	{
		AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
		AnVal2 = AnVal2 * SensorRec->Ch6BitVal * 1000;
	}


  #if DebugCIISSensor == 1
  Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
  #endif

  if( AnVal2 == 0 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = 0");
	#endif

  }
  else if( fabs(AnVal1/AnVal2) > 7 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1/AnVal2 > 7 AnVal2 set to -300");
	#endif
  }
  else
  {
	AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif
  }

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = DTS;
	MValRec.SampleDateTime = BPIn->SampleTimeStamp;
	MValRec.RecNo = BPIn->RecordingNo;
	MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
	MValRec.ValueType = "T";
	MValRec.ValueUnit = "C";
	MValRec.RawValue = AnVal2;
	MValRec.Value = AnVal2;

	DB->AppendMonitorValueRec( &MValRec );

	P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}


void __fastcall TCIISSensor_CW::SensorSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

	ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  VRes = UADVal1;

  AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

  AnVal1_Scl = AnVal1;// * SensorRec->SensorGain + SensorRec->SensorOffset;

	if( BPIn->Tag == 0 ) // Skip LastValue request on group
  {
		;
	}
	else if( CTMode == 2 && !FIniDecaySample && ( BPIn->Tag > 210 ) ) // P-Mode LastValue
	{

		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType =  "U" + IntToStr( BPIn->Tag - 210 );
		MVR.ValueUnit = "mV";
		MVR.Value = AnVal1_Scl;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
  }
	else if( ((BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		if( CTMode == 2  && !FIniDecaySample && ( BPIn->Tag > 200 ) && ( BPIn->Tag <= ( 200 + SensorRec->SensorChannelCount ))) // P-Mode
		{
			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType =  "U" + IntToStr( BPIn->Tag - 200 );
			MVR.ValueUnit = "mV";
			MVR.Value = AnVal1_Scl;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();

			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "U" + IntToStr( BPIn->Tag - 200 );
			MValRec.ValueUnit = "mV";
			MValRec.RawValue = AnVal1;
			MValRec.Value = AnVal1_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
	}
	else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
	{
		;
  }
  else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
		;
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal1));
  #endif
}

void __fastcall TCIISSensor_CW::SensorSampleExt( TCIISBusPacket *BPIn )
{
  double RErr;
  TDateTime DTS;
  DTS = Now();

	if( !FIniDecaySample )
  {
		if( CTMode == 0 ) // R-Mode
		{

			UADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
			IRes = UADVal2;

			#if DebugCIISSensor == 1
			Debug("Calc R: VRes = " + FloatToStr(VRes));
			Debug("Calc R: IRes = " + FloatToStr(IRes));
			#endif

			if( IRes == 0 )
			{
			IRes = 1;

			#if DebugCIISSensor == 1
			Debug("Calc Temp: IRes = 0 -> Set to 1");
			#endif
			}

			// Camur III
/*
  			Current =  LastValueRawCH3 * RSlope[SclIdx] + ROffset[SclIdx];
  			Voltage =  LastValueRawCH1;

				LastValueResistace = Voltage / Current  -  RMux[SclIdx];
*/

			if( ( SensorRec->SensorVerMajor >= 5 )  )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
						RErr = SensorRec->SensorIShunt2;
						IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt1;
						break;
					case 2:
						RErr = SensorRec->SensorIShunt5;
						IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt4;
						break;
					case 3:
						RErr = SensorRec->SensorIShunt8;
						IRes = SensorRec->SensorIShunt6 * IRes + SensorRec->SensorIShunt7;
						break;
					case 4:
						RErr = SensorRec->SensorIShunt11;
						IRes = SensorRec->SensorIShunt9 * IRes + SensorRec->SensorIShunt10;
						break;
					case 5:
						RErr = SensorRec->SensorIShunt14;
						IRes = SensorRec->SensorIShunt12 * IRes + SensorRec->SensorIShunt13;
						break;
					case 6:
						RErr = SensorRec->SensorIShunt17;
						IRes = SensorRec->SensorIShunt15 * IRes + SensorRec->SensorIShunt16;
						break;
					case 7:
						RErr = SensorRec->SensorIShunt20;
						IRes = SensorRec->SensorIShunt18 * IRes + SensorRec->SensorIShunt19;
						break;
					case 8:
						RErr = SensorRec->SensorIShunt23;
						IRes = SensorRec->SensorIShunt21 * IRes + SensorRec->SensorIShunt22;
						break;

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 2 ) )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
					break;
					case 2:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
					break;
					case 3:
					RErr = SensorRec->SensorIShunt12;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
					break;
					case 4:
					if( RMeasureFreq == 1000 )
					{
					RErr = SensorRec->SensorIShunt13;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
					}
					else
					{
					RErr = SensorRec->SensorIShunt14;
					IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
					}

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 1 ) )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = SensorRec->SensorIShunt8;
					IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt4;
					break;
					case 2:
					RErr = SensorRec->SensorIShunt9;
					IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt5;
					break;
					case 3:
					RErr = SensorRec->SensorIShunt10;
					IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt6;
					break;
					case 4:
					RErr = SensorRec->SensorIShunt11;
					IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt7;
					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 8 ) )
			{
				AnVal2 = VRes / IRes;
				switch( BPIn->Byte6 )
				{
					case 1:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt8);
					AnVal2 = SensorRec->SensorIShunt * AnVal2 + SensorRec->SensorIShunt4;
					break;
					case 2:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt9);
					AnVal2 = SensorRec->SensorIShunt1 * AnVal2 + SensorRec->SensorIShunt5;
					break;
					case 3:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt10);
					AnVal2 = SensorRec->SensorIShunt2 * AnVal2 + SensorRec->SensorIShunt6;
					break;
					case 4:
					RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt11);
					AnVal2 = SensorRec->SensorIShunt3 * AnVal2 + SensorRec->SensorIShunt7;
					break;
				}
				AnVal2 = ( AnVal2 * RErr / ( RErr - AnVal2 )) / 1000;
			}
			else
			{
				AnVal2 = VRes / IRes / 1000;
				switch( BPIn->Byte6 )
				{
					case 1: AnVal2 = SensorRec->SensorIShunt * AnVal2; break;
					case 2: AnVal2 = SensorRec->SensorIShunt1 * AnVal2; break;
					case 3: AnVal2 = SensorRec->SensorIShunt2 * AnVal2; break;
					case 4: AnVal2 = SensorRec->SensorIShunt3 * AnVal2; break;
				}
			}
		}
		else if( CTMode == 1 ) // ZRA-Mode
		{
			ADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
			AnVal2 = ADVal2;

			switch( BPIn->Byte6 )
			{
			case 0: AnVal2 = SensorRec->Ch1BitVal * AnVal2; break;
			case 1: AnVal2 = SensorRec->Ch2BitVal * AnVal2; break;
			case 2: AnVal2 = SensorRec->Ch3BitVal * AnVal2; break;
			case 3: AnVal2 = SensorRec->Ch4BitVal * AnVal2; break;
			case 4: AnVal2 = SensorRec->Ch5BitVal * AnVal2; break;
			case 5: AnVal2 = SensorRec->Ch6BitVal * AnVal2; break;
			}

			ZRALast = ZRACurrent;
			ZRACurrent = AnVal2;
		}

		AnVal2_Scl = AnVal2;

		if( BPIn->Tag == 0 ) // Skip LastValue request on group
		{
			;
		}
		else if(  BPIn->Tag > 200 ) // LastValue and Monitor values
		{
			int32_t ChOffset;

			if ( BPIn->Tag > 210 ) ChOffset = BPIn->Tag - 210;
			else ChOffset = BPIn->Tag - 200;

			if( CTMode == 0 ) // R-Mode
			{
				CIISMiscValueRec MVR;

				MVR.DateTimeStamp = DTS;
				MVR.SensorSerialNo = SensorRec->SensorSerialNo;
				MVR.ValueType = "R" + IntToStr( ChOffset );
				MVR.ValueUnit = "kOhm";
				MVR.Value = AnVal2;

				DB->AppendMiscValueRec( &MVR );

				P_Prj->LastValChanged();
				}
				else if( CTMode == 1 ) // ZRA-Mode
				{
				CIISMiscValueRec MVR;

				MVR.DateTimeStamp = DTS;
				MVR.SensorSerialNo = SensorRec->SensorSerialNo;
				MVR.ValueType = "I" + IntToStr( ChOffset );
				MVR.ValueUnit = "mA";
				MVR.Value = AnVal2;

				DB->AppendMiscValueRec( &MVR );

				P_Prj->LastValChanged();
			}
		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) &&
				(	BPIn->RequestedTag == BPIn->Tag ) &&
				( BPIn->Tag > 210 ) &&
				( BPIn->Tag <= ( 210 + SensorRec->SensorChannelCount ))) //Store Monitor Value
		{
			if( CTMode == 1 ) // ZRA-Mode
			{
				CIISMonitorValueRec MValRec;

				MValRec.DateTimeStamp = DTS;
				MValRec.SampleDateTime = BPIn->SampleTimeStamp;
				MValRec.RecNo = BPIn->RecordingNo;
				MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
				MValRec.ValueType = "I" + IntToStr( BPIn->Tag - 210 );
				MValRec.ValueUnit = "mA";
				MValRec.RawValue = AnVal2;
				MValRec.Value = AnVal2_Scl;

				DB->AppendMonitorValueRec( &MValRec );

				P_Prj->DataChanged();
				}
				else if( CTMode == 0 ) // R-Mode
				{
				CIISMonitorValueRec MValRec;

				MValRec.DateTimeStamp = DTS;
				MValRec.SampleDateTime = BPIn->SampleTimeStamp;
				MValRec.RecNo = BPIn->RecordingNo;
				MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
				MValRec.ValueType = "R" + IntToStr( BPIn->Tag - 210 );
				MValRec.ValueUnit = "kOhm";
				MValRec.RawValue = AnVal2;
				MValRec.Value = AnVal2_Scl;

				DB->AppendMonitorValueRec( &MValRec );

				P_Prj->DataChanged();
			}
		}
		else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			;
		}

	}

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
	#endif

}

void __fastcall TCIISSensor_CW::UpdateLastValues()
{
	if( !UpdateLastValueRunning )
	{
		UpdateLastValueRunning = true;
		T_CW = 0;
		TNextEvent_CW = 0;
		State_CW = 20;
	}
}

void __fastcall TCIISSensor_CW::SM_UpdateLastValue()
{
  if( T_CW >= TNextEvent_CW)
  {
	switch (State_CW)
	{
	  case 20: // P-Mode
		CTSelectMode( 2 );
		TNextEvent_CW += 1000;
		State_CW = 21 ;
		T_CW += SysClock;
		break;

		case 21: // Range = -1 -> Selected by user
		CTSelectRange( -1 );
		TNextEvent_CW += 1000;
		State_CW = 22 ;
		T_CW += SysClock;
		break;

	  case 22: // R Meassuring freq. = 1000 Hz
		CTSelectRFreq(1000);
		TNextEvent_CW += 1000;
		State_CW = 23 ;
		T_CW += SysClock;
		break;

	  case 23: // Channel = 0;
		CTSelectCh( 0 );
		TNextEvent_CW += 2000;
		State_CW = 24 ;
		T_CW += SysClock;
		break;

	  case 24: // Adj. Offset
		CTOffsetAdj();
		TNextEvent_CW += 2000;
		State_CW = 25 ;
		T_CW += SysClock;
		break;

	  case 25: // Calibrate DA
		CTDACalib();
		TNextEvent_CW += 3000;
		State_CW = 26 ;
		T_CW += SysClock;
		break;

	  case 26: // Request temp
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		CTRequestTemp( 210 );
		TNextEvent_CW += 3000;
		State_CW = 50 ;
		T_CW += SysClock;
		break;

	  case 50: // P-Mode
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();

		CWSelectedCh = 1;
		TNextEvent_CW += 1000;
		State_CW = 55 ;
		T_CW += SysClock;
		break;

	  case 55: // Channel = 1-8
		CTSelectCh( CWSelectedCh );
		TNextEvent_CW += 2000;
		State_CW = 56 ;
		T_CW += SysClock;
		break;

	  case 56: // Request sample ch 1-6
		SensorLastValueRequest = true;
		SampleTimeStamp = Now();
		CTRequestSample( 210 + CWSelectedCh );
		State_CW = 57 ;
		TNextEvent_CW += 1000;
		T_CW += SysClock;
		break;

	  case 57: // Apply uppdate
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();
		{
			CWSelectedCh++;
			if( CWSelectedCh <= SensorRec->SensorChannelCount ) State_CW = 55 ;
			else State_CW = 900;
		}
		TNextEvent_CW += 500;
		T_CW += SysClock;
		break;

	  case 900:
		CTSelectCh( 1 );
		TNextEvent_CW += 1000;
		State_CW = 999 ;
		T_CW += SysClock;
		break;

	  case 999:
		UpdateLastValueRunning = false;
		break;
	}
  }
  else T_CW += SysClock;
}

// Camur II Ladder

__fastcall TCIISSensor_Ladder::TCIISSensor_Ladder(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISSensorRec *SetSensorRec, TObject *SetCIISParent )
						   :TCIISSensor_CT( SetDB, SetCIISBusInt, SetDebugWin, SetSensorRec, SetCIISParent )
{

}

__fastcall TCIISSensor_Ladder::~TCIISSensor_Ladder()
{

}

void __fastcall TCIISSensor_Ladder::SetDefaultValuesSubType()
{
	SensorRec->SensorUnit = "";
	SensorRec->SensorUnit2 = "";
	SensorRec->SensorChannelCount = 7;
}

bool __fastcall TCIISSensor_Ladder::SensorLPRIni()
{
	bool SensorStarted;

	if( CTCh <= 7 )
	{
		SensorRec->RequestStatus = -1;
		SensorRec->SensorLPRStep = 0;
		SensorStarted = true;
	}
	else
	{
		SensorRec->RequestStatus = 0;
		SensorRec->SensorLPRStep = 0;
		SensorStarted = false;
	}
	return SensorStarted;
}

void __fastcall TCIISSensor_Ladder::CTRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode  )
{
	if(( CTCh <= 7 ) && ( CIISBICh != NULL ))
	{
		CIISBICh->RequestLPRStart( SensorRec->SensorCanAdr, LPRRange, LPRStep, LPRTOn, LPRTOff, LPRMode  );
	}
	else
	{
		SensorRec->RequestStatus = 0;
	}

}


void __fastcall TCIISSensor_Ladder::SensorSampleTemp( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
	AnVal1 = ADVal1;

	ADVal2 = BPIn->Byte6 + BPIn->Byte7 * 256;
	AnVal2 = ADVal2;

	if( !(( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 3 )))
	{
		AnVal1 = AnVal1 * SensorRec->Ch1BitVal;
		AnVal2 = AnVal2 * SensorRec->Ch6BitVal * 1000;
	}


  #if DebugCIISSensor == 1
  Debug("Calc Temp: AnVal1 = " + FloatToStr(AnVal1));
  Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
  #endif

  if( AnVal2 == 0 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = 0");
	#endif

  }
  else if( fabs(AnVal1/AnVal2) > 7 )
  {
	AnVal2 = -300;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal1/AnVal2 > 7 AnVal2 set to -300");
	#endif
  }
  else
  {
	AnVal2 = (-3.9083 + Sqrt( 15.27480889 + 2.31 * ( 1 - fabs( AnVal1 / AnVal2 )))) / -0.001155;

	#if DebugCIISSensor == 1
	Debug("Calc Temp: AnVal2 = " + FloatToStr(AnVal2));
	#endif
  }

  CIISMiscValueRec MVR;

  MVR.DateTimeStamp = DTS;
  MVR.SensorSerialNo = SensorRec->SensorSerialNo;
  MVR.ValueType = "T";
  MVR.ValueUnit = "C";
  MVR.Value = AnVal2;

  DB->AppendMiscValueRec( &MVR );

  P_Prj->LastValChanged();

	if( SensorRec->SensorTemp && (( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
  {
	CIISMonitorValueRec MValRec;

	MValRec.DateTimeStamp = DTS;
	MValRec.SampleDateTime = BPIn->SampleTimeStamp;
	MValRec.RecNo = BPIn->RecordingNo;
	MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
	MValRec.ValueType = "T";
	MValRec.ValueUnit = "C";
	MValRec.RawValue = AnVal2;
	MValRec.Value = AnVal2;

	DB->AppendMonitorValueRec( &MValRec );

	P_Prj->DataChanged();
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleTemp recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + FloatToStr(AnVal2) );
  #endif
}


void __fastcall TCIISSensor_Ladder::SensorSample( TCIISBusPacket *BPIn )
{
  TDateTime DTS;
  DTS = Now();

  ADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  UADVal1 = BPIn->Byte4 + BPIn->Byte5 * 256;
  VRes = UADVal1;

  AnVal1 = ADVal1; AnVal1 = AnVal1 * SensorRec->Ch1BitVal;

  AnVal1_Scl = AnVal1;// * SensorRec->SensorGain + SensorRec->SensorOffset;

	if( !FIniDecaySample )
	{
		if( BPIn->Tag == 0 ) // Skip LastValue request on group
		{
			;
		}
		else if( CTMode == 2 && ( BPIn->Tag > 200 )) // P-Mode LastValue and Monitor values
		{
			int32_t ChOffset;

			if ( BPIn->Tag > 210 ) ChOffset = BPIn->Tag - 210;
			else ChOffset = BPIn->Tag - 200;

			CIISMiscValueRec MVR;

			MVR.DateTimeStamp = DTS;
			MVR.SensorSerialNo = SensorRec->SensorSerialNo;
			MVR.ValueType =  "U" + IntToStr( ChOffset );
			MVR.ValueUnit = "mV";
			MVR.Value = AnVal1_Scl;

			DB->AppendMiscValueRec( &MVR );

			P_Prj->LastValChanged();
		}

		if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag && ( BPIn->Tag > 210 ) ) //Store Monitor Value
		{
			if( CTMode == 2 ) // P-Mode
			{

				CIISMonitorValueRec MValRec;

				MValRec.DateTimeStamp = DTS;
				MValRec.SampleDateTime = BPIn->SampleTimeStamp;
				MValRec.RecNo = BPIn->RecordingNo;
				MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
				MValRec.ValueType = "U" + IntToStr( BPIn->Tag - 210 );
				MValRec.ValueUnit = "mV";
				MValRec.RawValue = AnVal1;
				MValRec.Value = AnVal1_Scl;

				DB->AppendMonitorValueRec( &MValRec );

				P_Prj->DataChanged();
			}
		}
		else if( FIniDecaySample || ( BPIn->RecType == RT_Decay && BPIn->RequestedTag == BPIn->Tag )) //Store Decay Value
		{
			;
		}
		else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
		{
			;
		}
  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming Sample recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal1));
  #endif
}

void __fastcall TCIISSensor_Ladder::SensorSampleExt( TCIISBusPacket *BPIn )
{
  double RErr;
  TDateTime DTS;
  DTS = Now();

  if( !FIniDecaySample )
  {

	if( CTMode == 0 ) // R-Mode
	{
	  UADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
	  IRes = UADVal2;

	  #if DebugCIISSensor == 1
	  Debug("Calc R: VRes = " + FloatToStr(VRes));
	  Debug("Calc R: IRes = " + FloatToStr(IRes));
	  #endif

	  if( IRes == 0 )
	  {
		IRes = 1;

		#if DebugCIISSensor == 1
		Debug("Calc Temp: IRes = 0 -> Set to 1");
		#endif
	  }

	  			// Camur III
/*
  			Current =  LastValueRawCH3 * RSlope[SclIdx] + ROffset[SclIdx];
  			Voltage =  LastValueRawCH1;

				LastValueResistace = Voltage / Current  -  RMux[SclIdx];
*/

			if( ( SensorRec->SensorVerMajor >= 5 )  )
			{
				switch( BPIn->Byte6 )
				{
					case 1:
						RErr = SensorRec->SensorIShunt2;
						IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt1;
						break;
					case 2:
						RErr = SensorRec->SensorIShunt5;
						IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt4;
						break;
					case 3:
						RErr = SensorRec->SensorIShunt8;
						IRes = SensorRec->SensorIShunt6 * IRes + SensorRec->SensorIShunt7;
						break;
					case 4:
						RErr = SensorRec->SensorIShunt11;
						IRes = SensorRec->SensorIShunt9 * IRes + SensorRec->SensorIShunt10;
						break;
					case 5:
						RErr = SensorRec->SensorIShunt14;
						IRes = SensorRec->SensorIShunt12 * IRes + SensorRec->SensorIShunt13;
						break;
					case 6:
						RErr = SensorRec->SensorIShunt17;
						IRes = SensorRec->SensorIShunt15 * IRes + SensorRec->SensorIShunt16;
						break;
					case 7:
						RErr = SensorRec->SensorIShunt20;
						IRes = SensorRec->SensorIShunt18 * IRes + SensorRec->SensorIShunt19;
						break;
					case 8:
						RErr = SensorRec->SensorIShunt23;
						IRes = SensorRec->SensorIShunt21 * IRes + SensorRec->SensorIShunt22;
						break;

					break;
				}
				AnVal2 = ( VRes / IRes - RErr ) / 1000;
			}
			else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 2 ) )
	  {
		switch( BPIn->Byte6 )
		{
		  case 1:
		  RErr = SensorRec->SensorIShunt10;
		  IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt5;
		  break;
		  case 2:
		  RErr = SensorRec->SensorIShunt11;
		  IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt6;
		  break;
		  case 3:
		  RErr = SensorRec->SensorIShunt12;
		  IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt7;
		  break;
		  case 4:
		  if( RMeasureFreq == 1000 )
		  {
			RErr = SensorRec->SensorIShunt13;
			IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt8;
		  }
		  else
		  {
			RErr = SensorRec->SensorIShunt14;
			IRes = SensorRec->SensorIShunt4 * IRes + SensorRec->SensorIShunt9;
		  }

		  break;
		}
		AnVal2 = ( VRes / IRes - RErr ) / 1000;
	  }
	  else if( ( SensorRec->SensorVerMajor >= 4 ) && ( SensorRec->SensorVerMinor >= 1 ) )
	  {
		switch( BPIn->Byte6 )
		{
		  case 1:
		  RErr = SensorRec->SensorIShunt8;
		  IRes = SensorRec->SensorIShunt * IRes + SensorRec->SensorIShunt4;
		  break;
		  case 2:
		  RErr = SensorRec->SensorIShunt9;
		  IRes = SensorRec->SensorIShunt1 * IRes + SensorRec->SensorIShunt5;
		  break;
		  case 3:
		  RErr = SensorRec->SensorIShunt10;
		  IRes = SensorRec->SensorIShunt2 * IRes + SensorRec->SensorIShunt6;
		  break;
		  case 4:
		  RErr = SensorRec->SensorIShunt11;
		  IRes = SensorRec->SensorIShunt3 * IRes + SensorRec->SensorIShunt7;
		  break;
		}
		AnVal2 = ( VRes / IRes - RErr ) / 1000;
	  }
		else if( ( SensorRec->SensorVerMajor >= 3 ) && ( SensorRec->SensorVerMinor >= 8 ) )
	  {
		AnVal2 = VRes / IRes;
		switch( BPIn->Byte6 )
		{
		  case 1:
		  RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt8);
		  AnVal2 = SensorRec->SensorIShunt * AnVal2 + SensorRec->SensorIShunt4;
		  break;
		  case 2:
		  RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt9);
		  AnVal2 = SensorRec->SensorIShunt1 * AnVal2 + SensorRec->SensorIShunt5;
		  break;
		  case 3:
		  RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt10);
		  AnVal2 = SensorRec->SensorIShunt2 * AnVal2 + SensorRec->SensorIShunt6;
		  break;
		  case 4:
		  RErr = 1 / (2*3.14159*RMeasureFreq*SensorRec->SensorIShunt11);
		  AnVal2 = SensorRec->SensorIShunt3 * AnVal2 + SensorRec->SensorIShunt7;
		  break;
		}
		AnVal2 = ( AnVal2 * RErr / ( RErr - AnVal2 )) / 1000;
	  }
	  else
	  {
		AnVal2 = VRes / IRes / 1000;
		switch( BPIn->Byte6 )
		{
		  case 1: AnVal2 = SensorRec->SensorIShunt * AnVal2; break;
		  case 2: AnVal2 = SensorRec->SensorIShunt1 * AnVal2; break;
		  case 3: AnVal2 = SensorRec->SensorIShunt2 * AnVal2; break;
		  case 4: AnVal2 = SensorRec->SensorIShunt3 * AnVal2; break;
		}
	  }
	}
	else if( CTMode == 1 ) // ZRA-Mode
	{
	  ADVal2 = BPIn->Byte4 + BPIn->Byte5 * 256;
	  AnVal2 = ADVal2;

	  switch( BPIn->Byte6 )
	  {
		case 0: AnVal2 = SensorRec->Ch1BitVal * AnVal2; break;
		case 1: AnVal2 = SensorRec->Ch2BitVal * AnVal2; break;
		case 2: AnVal2 = SensorRec->Ch3BitVal * AnVal2; break;
		case 3: AnVal2 = SensorRec->Ch4BitVal * AnVal2; break;
		case 4: AnVal2 = SensorRec->Ch5BitVal * AnVal2; break;
		case 5: AnVal2 = SensorRec->Ch6BitVal * AnVal2; break;
	  }
	  ZRALast = ZRACurrent;
	  ZRACurrent = AnVal2;
	}

	AnVal2_Scl = AnVal2;

	if( BPIn->Tag == 0 ) // Skip LastValue request on group
	{
		;
	}
	else if(  BPIn->Tag > 200 ) // LastValue and Monitor values
	{
	  int32_t ChOffset;

	  if ( BPIn->Tag > 210 ) ChOffset = BPIn->Tag - 210;
	  else ChOffset = BPIn->Tag - 200;
	  if( CTMode == 0 ) // R-Mode
	  {
		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "R" + IntToStr( ChOffset );
		MVR.ValueUnit = "kOhm";
		MVR.Value = AnVal2;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
	  }
	  else if( CTMode == 1 ) // ZRA-Mode
	  {
		CIISMiscValueRec MVR;

		MVR.DateTimeStamp = DTS;
		MVR.SensorSerialNo = SensorRec->SensorSerialNo;
		MVR.ValueType = "I" + IntToStr( ChOffset );
		MVR.ValueUnit = "mA";
		MVR.Value = AnVal2;

		DB->AppendMiscValueRec( &MVR );

		P_Prj->LastValChanged();
	  }
	}


	if((( BPIn->RecType == RT_Monitor ) || ( BPIn->RecType == RT_MonitorExt ) || ( BPIn->RecType == RT_ScheduledValues )) && BPIn->RequestedTag == BPIn->Tag && BPIn->Tag > 210 ) //Store Monitor Value
	{
		if( CTMode == 1 ) // ZRA-Mode
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "I" + IntToStr( BPIn->Tag - 210 );
			MValRec.ValueUnit = "mA";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
		else if( CTMode == 0 ) // R-Mode
		{
			CIISMonitorValueRec MValRec;

			MValRec.DateTimeStamp = DTS;
			MValRec.SampleDateTime = BPIn->SampleTimeStamp;
			MValRec.RecNo = BPIn->RecordingNo;
			MValRec.SensorSerialNo = SensorRec->SensorSerialNo;
			MValRec.ValueType = "R" + IntToStr( BPIn->Tag - 210 );
			MValRec.ValueUnit = "kOhm";
			MValRec.RawValue = AnVal2;
			MValRec.Value = AnVal2_Scl;

			DB->AppendMonitorValueRec( &MValRec );

			P_Prj->DataChanged();
		}
	}
	else if( ( BPIn->RecType == RT_CTNonStat || BPIn->RecType == RT_CTStat ) && BPIn->RequestedTag == BPIn->Tag ) //Store Monitor Value
	{
		;
	}

  }

  BPIn->MsgMode = CIISBus_MsgReady;
  BPIn->ParseMode = CIISParseReady;

  #if DebugCIISBusInt == 1
  Debug("Incoming SampleExt recived Sensor: " + IntToStr( SensorRec->SensorSerialNo ) + " = " + IntToStr(ADVal1) + " / " + IntToStr(ADVal2));
  #endif
}

void __fastcall TCIISSensor_Ladder::UpdateLastValues()
{
	if( !UpdateLastValueRunning )
  {
		UpdateLastValueRunning = true;
		T_Ldr = 0;
		TNextEvent_Ldr = 0;
		State_Ldr = 20;
  }
}

void __fastcall TCIISSensor_Ladder::SM_UpdateLastValue()
{
  if( T_Ldr >= TNextEvent_Ldr)
  {
	switch (State_Ldr)
	{
	  case 20: // P-Mode
		CTSelectMode( 2 );
		TNextEvent_Ldr += 1000;
		State_Ldr = 21 ;
		T_Ldr += SysClock;
		break;

		case 21: // Range = -1 -> Selected by user
		CTSelectRange( -1 );
		TNextEvent_Ldr += 1000;
		State_Ldr = 22 ;
		T_Ldr += SysClock;
		break;

	  case 22: // R Meassuring freq. = 1000 Hz
		CTSelectRFreq(1000);
		TNextEvent_Ldr += 1000;
		State_Ldr = 23 ;
		T_Ldr += SysClock;
		break;

	  case 23: // Channel = 0;
		CTSelectCh( 0 );
		TNextEvent_Ldr += 2000;
		State_Ldr = 24 ;
		T_Ldr += SysClock;
		break;

	  case 24: // Adj. Offset
		CTOffsetAdj();
		TNextEvent_Ldr += 2000;
		State_Ldr = 25 ;
		T_Ldr += SysClock;
		break;

	  case 25: // Calibrate DA
		CTDACalib();
		TNextEvent_Ldr += 3000;
		State_Ldr = 26 ;
		T_Ldr += SysClock;
		break;

	  case 26: // Request temp
		SensorLastValueTempRequest = true;
		SampleTimeStamp = Now();
		CTRequestTemp( 210 );
		TNextEvent_Ldr += 3000;
		State_Ldr = 50 ;
		T_Ldr += SysClock;
		break;


	  case 50: // P-Mode
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();

		LdrSelectedCh = 1;
		TNextEvent_Ldr += 1000;
		State_Ldr = 55 ;
		T_Ldr += SysClock;
		break;

	  case 55: // Channel = 1-7
		CTSelectCh( LdrSelectedCh );
		TNextEvent_Ldr += 500;
		State_Ldr = 56 ;
		T_Ldr += SysClock;
		break;

	  case 56: // Request sample ch 1-7
		SensorLastValueRequest = true;
		SampleTimeStamp = Now();
		CTRequestSample( 200 + LdrSelectedCh );
		State_Ldr = 57 ;
		TNextEvent_Ldr += 1000;
		T_Ldr += SysClock;
		break;

	  case 57: // Apply uppdate
		DB->ApplyUpdatesMiscValue(P_Prj->MiscSize);
		DB->ApplyUpdatesPrj();
		{
			LdrSelectedCh++;
			if( LdrSelectedCh < 8 ) State_Ldr = 55 ;
			else State_Ldr = 900;
		}
		TNextEvent_Ldr += 500;
		T_Ldr += SysClock;
		break;

	  case 900:
		CTSelectCh( 1 );
		TNextEvent_Ldr += 1000;
		State_Ldr = 999 ;
		T_Ldr += SysClock;
		break;

	  case 999:
		UpdateLastValueRunning = false;
		break;
	}
  }
  else T_Ldr += SysClock;
}


