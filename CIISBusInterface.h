//---------------------------------------------------------------------------

#ifndef CIISBusInterfaceH
#define CIISBusInterfaceH

#include "CIISObj.h"
#include "ftd2xx.h"
#include "DebugWinU.h"
#include "CIISDB.h"
#include "CIISCommon.h"
#include "System.IniFiles.hpp"
#include <System.Win.ScktComp.hpp>
#include "IdUDPServer.hpp"

enum RestartBIState
{
	BI_FindBusInterfaces,
	BI_GetEthIntSnr,
	BI_GetNextEthIntSnr,
	BI_InstallInterfaces,
	BI_IniBIChannels,
	BI_OpenBusInterfaces,
	BI_GetBIVersions,
	BI_GetBIIPStatGatwayNetmask,
	BI_GetBIMac,
	BI_SetWDEnable,
	BI_SetWDTime,
	BI_BICANPower,
	BI_BICANForceReset,
	BI_BIReadCANMsg,
	BI_WaitForBusIni,
	BI_Ready,
	BI_TimeOut
};

const int32_t canMSG_STD = 0002; // Message to CIINet ( via USBCANInterface )
const int32_t USBCanMSG  = 0100; // Message to USBCANInterface
const int32_t TempCANBufSize = 1023;
const int32_t CANInBufSize = 1023;
const int32_t  BufMask = 1023;
const int32_t  USBHeaderLength = 5;
const int32_t  MaxUSBMeassageLength = 15;
const int32_t EthSocketInBufSize = 4096;
const int32_t EthSocketInQueueSize = 8192;


enum BIChannelType
			{
				USB = 0,
				Ethernet = 1
      };


class TCIISBusInterface : public TCIISObj
{
private:
	TCIISDBModule *DB;
	ProcMessFP PM;
	FT_HANDLE CurrentFTHandle;
	bool ResetCIISBusRunning, RestartCIISBusRunning;
	int32_t  ResetBusState;
	int32_t T, TNextEvent;

	RestartBIState RBIState;

	int32_t FHighestUSBSerialNo;
	int32_t FBusInterfaceWDStatus;
	int32_t FBusInterfaceAddress;
	int32_t FBIChCount;

	void __fastcall FindEthInterfaces();
	void __fastcall InstallEthInterfaces();
	void __fastcall FindUSBInterfaces();
	void __fastcall InstallUSBInterfaces();
	void __fastcall UnInstallBIChannels();
	void __fastcall BIOpenCom();
	void __fastcall SM_ResetCIISBus();
	void __fastcall SM_RestartCIISBus();

	bool __fastcall SetUSBCanAddress(Word USBCANAddress);
	bool __fastcall BIReadCANMsg(bool Status);
	bool __fastcall BIChResetRunning();
	bool __fastcall BIChIniRunning();


	int32_t __fastcall  GetHighestUSBSerialNo();
	int32_t __fastcall  GetHighestUSBVerMajor();
	int32_t __fastcall  GetHighestUSBVerMinor();

	int32_t CanAdr;

	TIdUDPServer *UDPSrv;

	TList *BIChRecs;
	String IniFilePath;
	String UDPData, UDPEthIP;

	bool AddNewBIChannels;



protected:

	//CIISClientInt
	void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

	//CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
	void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
	__fastcall TCIISBusInterface( TCIISDBModule *SetDB, TDebugWin *SetDebugWin, TObject *SetCIISParent, ProcMessFP SetPM, bool DisableEthernet );
	__fastcall ~TCIISBusInterface();

	void __fastcall Debug(String Message);
	void __fastcall ResetCIISBus();
	void __fastcall RestartCIISBus();
	bool __fastcall NodeIniReady();

	bool __fastcall BICANPower(bool Status);

  int32_t __fastcall  GetZonCanAdr();
  bool __fastcall RequestSample( Word CanAdr, int32_t Tag );
  bool __fastcall RequestTemp( Word CanAdr );
  bool __fastcall RequestDecayIni( Word CanAdr, Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );
  bool __fastcall RequestDecayStart( Word CanAdr );
  bool __fastcall RequestDecayCancel( Word CanAdr );
  bool __fastcall RequestLPRStart( Word CanAdr, Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
  bool __fastcall RequestLPRStop( Word CanAdr );
  bool __fastcall SendClrLPR( Word CanAdr );
	bool __fastcall RequestCANBusReset();
  bool __fastcall RequestBIMode();
	void __fastcall SetWatchDogEnable( bool WDEnable );
	PVOID __fastcall GetBIChannel( int32_t ChNo );
	void __fastcall TestBI();
	void __fastcall UDPRead(TIdUDPListenerThread *AThread, const TIdBytes AData, TIdSocketHandle *ABinding);

	__published:
	void __fastcall TimerCanRxEvent(TObject * Sender);
	__property int32_t HighestUSBVerMajor  = { read=GetHighestUSBVerMajor };
	__property int32_t HighestUSBVerMinor  = { read=GetHighestUSBVerMinor };
	__property int32_t HighestUSBSerialNo = { read=GetHighestUSBSerialNo };
	__property int32_t BIChCount = { read=FBIChCount };

	__property bool ResetRunning = { read = ResetCIISBusRunning };

};

class TCIISBIChannel : public TCIISObj
{
private:

	int32_t BICh;
	int32_t CanAdr;
	bool ResetRunning, WDRunning;
	int32_t ResetBusState;



	bool __fastcall ThisRec( TCamurPacket *P );
	void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
	void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );
	void __fastcall SM_ResetCIISBus();
	virtual void __fastcall SM_BIChannelIni();
	void __fastcall ReConnectBIChannel();
	virtual void __fastcall SM_ReConnectBIChannel();

	virtual bool __fastcall SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask );

	int32_t __fastcall GetSerialNo() { return BIChRec->SerialNo; }
	int32_t __fastcall GetVerMajor() { return BIChRec->VerMajor; }
	int32_t __fastcall GetVerMinor() { return BIChRec->VerMinor; }
	bool __fastcall GetIncluded() { return BIChRec->Included; }

protected:
	int32_t CANInBufReadPnt, CANInBufWritePnt;
	Byte TempCANBuf[TempCANBufSize + 1];
	Byte CANInBuf[CANInBufSize + 1];
	Cardinal CANInFlg;
	bool CanPMActive;
	bool  IniRunning;
	int32_t T, TNextEvent;
	int32_t OnSlowClockTick, OnResetWD;
	RestartBIState RBIState;
	int32_t ReConnectState;
  bool ReConnectRunning;

	//int32_t FBusInterfaceSerialNo;
	int32_t FBusInterfaceVerMinor, FBusInterfaceVerMajor;
	ProcMessFP PM, PMBI;
	TDateTime	TLastSend;
	Byte CANInMsg[8];
	int32_t FBusInterfaceWDStatus;
	int32_t FBusInterfaceAddress;
	Byte BusIntMsg[256];
	CIISBIChannelRec *BIChRec;

	//CIISClientInt
	void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

	//CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

	void __fastcall ParseMessages();

public:

	__fastcall TCIISBIChannel( TCIISDBModule *SetDB,
														 TDebugWin *SetDebugWin,
														 CIISBIChannelRec *SetBIChRec,
														 TObject *SetCIISParent,
														 ProcMessFP SetPM
														);
	__fastcall ~TCIISBIChannel();

	void __fastcall Debug(String Message);
	void __fastcall ResetCIISBus();
	void __fastcall BIChannelIni();

	virtual int32_t __fastcall SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags);
	virtual bool __fastcall BIChClose();
	virtual bool __fastcall BIChOpen();
	virtual void __fastcall OpenCom();

	bool __fastcall BIChCANPower(bool Status);
	bool __fastcall BIChReadCANMsg(bool Status);
	bool __fastcall SetUSBCanAddress(Word USBCANAddress);
	bool __fastcall GetBIChVersion();
	bool __fastcall GetBIChIPSt_GW_NM();
	bool __fastcall GetBIChMac();

	bool __fastcall SetWatchDogTime( Word WDTime );
	bool __fastcall SetWatchDogEnable( bool WDEnable );
	bool __fastcall ResetWatchDog();

	bool __fastcall SetAOut(Word CanAdr, int32_t Value);
	bool __fastcall SetPSVOut(Word CanAdr, int32_t Value, int32_t MaxOut);
	bool __fastcall SetPSIOut(Word CanAdr, int32_t Value, int32_t MaxOut);
	bool __fastcall SetPI_IOut(Word CanAdr, int32_t Value, int32_t MaxOut);
	bool __fastcall SetDigOut(Word CanAdr, Word Channel, bool Status);
	bool __fastcall SetPSOutputOn(Word CanAdr, bool Status);
	bool __fastcall SetPSMode(Word CanAdr, int32_t Status);
	bool __fastcall SetPSFallback(Word CanAdr, int32_t Status);


	int32_t __fastcall GetCanAdr();
	bool __fastcall SendAccept(int32_t SerNo, Word CANAdr);
	bool __fastcall RequestCapability(Word CanAdr);
	bool __fastcall SetGroup(Word CanAdr, Word CanGroup);
	bool __fastcall RequestIShunt(Word CanAdr);
	bool __fastcall RequestRShunt(Word CanAdr);
	bool __fastcall RequestBitVal(Word CanAdr, Word Channel);
	bool __fastcall RequestVer(Word CanAdr);
	bool __fastcall RequestSample( Word CanAdr, int32_t Tag );
	bool __fastcall RequestTemp( Word CanAdr );
	bool __fastcall RequestDecayIni( Word CanAdr, Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );
	bool __fastcall RequestDecayStart( Word CanAdr );
	bool __fastcall RequestDecayCancel( Word CanAdr );
	bool __fastcall RequestLPRStart( Word CanAdr, Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode );
	bool __fastcall RequestLPRStop( Word CanAdr );
	bool __fastcall RequestWLinkPar( Word CanAdr, Word WLinkPar );
	bool __fastcall RequestWLinkMode( Word CanAdr );
	bool __fastcall SetWLinkPar( Word CanAdr, Word WLinkPar, int32_t Val );
	bool __fastcall SetWLinkMode( Word CanAdr, int32_t Mode );
	bool __fastcall WLinkParPermanent( Word CanAdr );

	void __fastcall OffsetAdj( Word CanAdr );
	void __fastcall SetCamurIIMode( Word CanAdr );
	void __fastcall ClearCIIIRecPar( Word CanAdr );
	void __fastcall ClearCIIIAlarm( Word CanAdr );
	void __fastcall InvertOutputOn( Word CanAdr, bool Invert );

	void __fastcall CTSelectMode( Word CanAdr, int32_t Mode );
	void __fastcall CTSelectRange( Word CanAdr, int32_t Range );
	void __fastcall SetCTModeRangeFreq( Byte Mode, bool Apply, Byte ZRARange, Byte ResMesRange, Byte ResMesFreq );
	void __fastcall CTSelectRFreq( Word CanAdr, int32_t FMode );
	void __fastcall CTSelectCh( Word CanAdr, int32_t Ch );
	void __fastcall CTDACalib( Word CanAdr );
	void __fastcall CTOffsetAdj( Word CanAdr );
	void __fastcall CTRequestTemp( Word CanAdr, int32_t Tag );

	bool __fastcall SendClrLPR( Word CanAdr );
	bool __fastcall RequestCANBusReset();

	// uCtrl

	void __fastcall SetPMBI( ProcMessFP SetPMBI );
	ProcMessFP __fastcall GetPMBI();

	bool __fastcall RequestBIMode();
	bool __fastcall SetBIMode( Byte uCtrlMode );
	bool __fastcall SetBITime( Word Year, Word Month, Word Day, Word Hour, Word Minute, Word Sec );
	bool __fastcall RequestBITime();
	bool __fastcall RequestPowerSetting();
	bool __fastcall SetPower( int32_t Voltage, int32_t Current, Byte PowerMode, bool OutputOn, int32_t Interval );
	bool __fastcall StartRecording();
	bool __fastcall StopRecording();
	bool __fastcall RequestNodeCount();
	bool __fastcall RequestNodeInfo( Byte Channel );
	bool __fastcall RequestData( int32_t Start );
	bool __fastcall RequestRecStatus();
	bool __fastcall RequestLastValueUpdate();



__published:
	virtual void __fastcall ReadCanRx();
//	__property int32_t CanBusStatus  = { read=FCanBusStatus, write=FCanBusStatus };
	__property int32_t BusInterfaceVerMajor  = { read=GetVerMajor };
	__property int32_t BusInterfaceVerMinor  = { read=GetVerMinor };
	__property int32_t BusInterfaceSerialNo = { read=GetSerialNo };

	__property bool BIChResetRunning = { read = ResetRunning };
	__property bool BIChIniRunning = { read = IniRunning};
	__property bool Included = { read = GetIncluded };
};

class TCIISBIUSB : public TCIISBIChannel
{
private:
	FT_HANDLE CurrentFTHandle;
	char USBCANSerNo[16];
	virtual bool __fastcall SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask );

public:
	__fastcall TCIISBIUSB( TCIISDBModule *SetDB,
												 TDebugWin *SetDebugWin,
												 CIISBIChannelRec *SetBIChRec,
												 TObject *SetCIISParent,
												 ProcMessFP SetPM
												);
	__fastcall ~TCIISBIUSB();

	virtual void __fastcall OpenCom();
	virtual void __fastcall SM_BIChannelIni();
	virtual void __fastcall SM_ReConnectBIChannel();
	virtual int32_t __fastcall SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags);
	virtual  bool __fastcall BIChClose();
	virtual  bool __fastcall BIChOpen();



__published:
	virtual void __fastcall ReadCanRx();
};

class TCIISBIEthernet : public TCIISBIChannel
{
private:
	TClientSocket *ClientSocket;
	TCustomWinSocket *CWSocket;
	String IPAdr;

	char SocketInBuf[EthSocketInBufSize];
	char SocketInputQueue[EthSocketInQueueSize];

	int32_t SocketInputIdx, PortNo;
	bool SocketOpen;

	void __fastcall AddSocketInputQueue(char* Buf, int32_t N);

	virtual bool __fastcall SetIP_Par( int32_t Mode, String StaticIP, String Gateway, String Netmask );

public:
	__fastcall TCIISBIEthernet( TCIISDBModule *SetDB,
															TDebugWin *SetDebugWin,
															CIISBIChannelRec *SetBIChRec,
															TObject *SetCIISParent,
															ProcMessFP SetPM
														 );
	__fastcall ~TCIISBIEthernet();

	virtual void __fastcall OpenCom();
	virtual void __fastcall SM_BIChannelIni();
	virtual void __fastcall SM_ReConnectBIChannel();
	virtual int32_t __fastcall SendMsg(int32_t id, byte * msg, Cardinal dlc, Cardinal flags);
	virtual bool __fastcall BIChClose();
	virtual bool __fastcall BIChOpen();



	void __fastcall SendMessage( TCamurPacket *Request );

	void __fastcall ServerConnect(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerLookup(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerDisconnect(TObject * Sender, TCustomWinSocket * Socket);
	void __fastcall ServerOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int32_t &ErrorCode);
	void __fastcall ServerRead(TObject * Sender, TCustomWinSocket * Socket);
	//void __fastcall TimerSReadEvent();



__published:
	virtual void __fastcall ReadCanRx();
};

#endif
