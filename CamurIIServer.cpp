//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("CIISMain.cpp", CIISMainFrm);
USEFORM("CIISDB.cpp", CIISDBModule); /* TDataModule: File Type */
USEFORM("DebugWinU.cpp", DebugWin);
USEFORM("CIISDBInit.cpp", CIISDBInitFrm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "CamurIIServer";
                 Application->CreateForm(__classid(TCIISMainFrm), &CIISMainFrm);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
