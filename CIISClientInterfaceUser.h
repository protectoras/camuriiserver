//---------------------------------------------------------------------------

#ifndef CIISClientInterfaceUserH
#define CIISClientInterfaceUserH

#include "CIISDB.h"
#include "DebugWinU.h"
#include "CIISServerInterface.h"
#include <System.Win.scktcomp.hpp>
#include "CIIServerSocket.h"

//---------------------------------------------------------------------------

class TCIISClientInterfaceUser : public TObject
{
private:
	TCIISDBModule *DB;
	TDebugWin *DW;
	TCIISServerInterface *SerInt;
	TCIIClientSocket *SerIntClientSocket;
	CIISPrjRec *PrjRec;
	int32_t PortNoMonitor, PortNoPM;
	TCIIServerSocket *MSocket, *PMSocket;
	TCamurPacket  *PMReply, *MReply;
	TDateTime TOutSM;
	int32_t FT_Timeout;
	bool TimerSMEnabled, SelectPrjRunning,
		PMLoggInRunning, PMLoggOutRunning, PMExitPrjRunning,
		MonLoggInRunning, MonLoggOutRunning, MonCancelSyncRunning;
	CIISSelPrjState SelPrjState;
	CIISPMLoggInState PMLoggInState;
	CIISPMLoggOutState PMLoggOutState;
	CIISPMExitPrjState PMExitPrjState;
	CIISMonLoggInState MonLoggInState;
	CIISMonLoggOutState MonLoggOutState;
	CIISMonCancelSyncState MonCancelSyncState;

	String UserLevel, Password, MonUserLevel, MonPassword;


	void __fastcall Debug(String Message);

	bool __fastcall ParseMonitorCommands();
	bool __fastcall ParsePMCommands();

	void __fastcall MonitorMessage();
	void __fastcall PMMessage();
	bool __fastcall GetLocalClientConnected();
	bool __fastcall GetPMConnected();
	bool __fastcall GetRemoteClientConnected();
	bool __fastcall SelectPrjIni( String SetUserLevel, String SetPassword );
	void __fastcall SM_SelectPrj();
	bool __fastcall PMLoggInIni();
	void __fastcall SM_PMLoggIn();
	bool __fastcall PMLoggOutIni();
	void __fastcall SM_PMLoggOut();
	bool __fastcall PMExitPrjIni();
	void __fastcall SM_PMExitPrj();
	bool __fastcall MonLoggInIni();
	void __fastcall SM_MonLoggIn();
	bool __fastcall MonLoggOutIni();
	void __fastcall SM_MonLoggOut();
	bool __fastcall MonCancelSyncIni();
	void __fastcall SM_MonCancelSync();

	bool __fastcall SM_Running();


protected:

public:
	__fastcall TCIISClientInterfaceUser( TCIISDBModule * SetDB,
									   TDebugWin * SetDebugWin,
									   TCIISServerInterface *SetSerInt,
									   int32_t SetPortNoMonitor,
									   int32_t SetPortNoPM );
	__fastcall ~TCIISClientInterfaceUser();

	TCIIServerSocket* __fastcall GetMSocket();
	TCIIServerSocket* __fastcall GetPMSocket();

	void __fastcall SetDebugWin(TDebugWin * SetDebugWin);
	void __fastcall TransferReply( TCamurPacket *o );
	void __fastcall TimerSMEvent(TObject * Sender);


__published:
	__property bool LocalClientConnected  = { read=GetLocalClientConnected };
	__property bool PMConnected  = { read=GetPMConnected };

};


#endif
