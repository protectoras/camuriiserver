//---------------------------------------------------------------------------

#ifndef CIISServerInterfaceH
#define CIISServerInterfaceH

#include "CIISDB.h"
#include "DebugWinU.h"
#include "CIIClientSocket.h"

//---------------------------------------------------------------------------
enum CIISEvTabStatus { ETSMissingRec, ETSPrjCh, ETSCtrlCh, ETSZoneCh, ETSPSCh,
											 ETSWLinkCh, ETSRecCh, ETSSensorCh, ETSAlarmCh, ETSBIChannelCh };

enum CIISPrjTabStatus { PTSLocalError, PTSMissingRec, PTSConfigCh, PTSDataCh,
                        PTSLastValCh, PTSPrjNoDontMatch };

enum CIISCtrlTabStatus { CTSLocalError, CTSMissingRec };

enum CIISZoneTabStatus { ZTSLocalError, ZTSMissingRec };

enum CIISSensTabStatus { STSLocalError, STSMissingRec };

enum CIISAlarmTabStatus { AlarmTSLocalError, AlarmTSMissingRec };

enum CIISPSTabStatus { PSTSLocalError, PSTSMissingRec };

enum CIISWLinkTabStatus { WLinkTSLocalError, WLinkTSMissingRec };

enum CIISBIChTabStatus { BIChTSLocalError, BIChTSMissingRec };

enum CIISRecTabStatus { RecTSLocalError, RecTSMissingRec };

enum CIISValTabStatus { ValTSLocalError, ValTSMissingRec };

enum CIISLPRTabStatus { LPRTSLocalError, LPRTSMissingRec };

enum CIISCVTabStatus { CVTSLocalError, CVTSMissingRec };

enum CIISDecayTabStatus { DecayTSLocalError, DecayTSMissingRec };

enum CIISMiscValSyncState{ MV_StartSync,
						   MV_RequestNewLastRec, MV_ReadLastRec,
						   MV_RequestFirstRec, MV_RequestNextRec,
						   MV_ReadRec, MV_SyncReady	 };

class TCIISServerInterface : public TObject
{
private:
  TCIISDBModule *DB;
  TDebugWin *DW;
  TransferReplyFP TransferReply;
  int32_t PortNo;
  String IPAdr;
  int32_t ValuesMiscSize;
  bool MiscValueGetLoggerLastRec, MiscValueLoggerEmpty;
  TList *TransferMessageList, *TransferResponseList;
  TCIIClientSocket *ClientSocket;
  Byte MessageNumber;
  String UserLevel, Password;
  bool FLoggedIn;
  int32_t ValueFirstSyncRecord, DecayFirstSyncRecord, LPRFirstSyncRecord, CVFirstSyncRecord;
	int32_t ValueLastSyncRecord, DecayLastSyncRecord, LPRLastSyncRecord, CVLastSyncRecord;
	int32_t ValueCurrSyncRecord, DecayCurrSyncRecord, LPRCurrSyncRecord, CVCurrSyncRecord;
	int32_t SyncReadyValues, SyncReadyDecay, SyncReadyLPR;
	bool ValueFirstRead, DecayFirstRead, LPRFirstRead, CVFirstRead;
  bool MoreRec;

  String Rec, Sensor, LPRStep;
  TDateTime RecStartTime, DTS;
	double LPRInterval;

	TDateTime ApplyUpdateDelay;

	bool TimerSMEnabled, TimerSMCancelEnabled, StopReEntrancy ;

  CIISCheckSyncState CheckSyncState;
  CIISDataSyncState DataSyncState;
  CIISSysSyncState SysSyncState;
  CIISEventSyncState EventSyncState;
  CIISProjectSyncState ProjectSyncState;
  CIISCtrlSyncState CtrlSyncState;
  CIISZoneSyncState ZoneSyncState;
	CIISSensorSyncState SensSyncState;
	CIISAlarmSyncState AlarmSyncState;
  CIISPSSyncState PSSyncState;
	CIISWLinkSyncState WLinkSyncState;
	CIISBIChannelSyncState BIChannelSyncState;
  CIISRecSyncState RecSyncState;
  CIISValSyncState ValSyncState;
	CIISLPRSyncState LPRSyncState;
	CIISCVSyncState CVSyncState;
  CIISDecaySyncState DecaySyncState;
  CIISLastValSyncState LastValSyncState;
  CIISLoggInState LoggInState;
  CIISOpenComState OpenComState;
  CIISCloseComState CloseComState;
  CIISCancelSyncState CancelSyncState;
  CIISMiscValSyncState MiscValSyncState;

  String LastSensor, LastPS;

	int32_t RequestNo;
  bool FSysSyncRunning, FDataSyncRunning, FTransferMessageRunning, FLoggInRunning, FCancelSyncRunning, FOpenComRunning, FCloseComRunning;
  bool SysSyncCancel, DataSyncCancel, SendTransferMessageCancel, LoggInCancel, CancelSyncCancel, OpenComCancel, CloseComCancel;
	bool ValuesTabSyncCancel, ValuesLPRTabSyncCancel, ValuesCVTabSyncCancel, ValuesDecayTabSyncCancel;
	bool EventTabSyncCancel, ZoneTabSyncCancel, SensorTabSyncCancel, AlarmTabSyncCancel,
			 PSTabSyncCancel, RecordingTabSyncCancel, WLinkTabSyncCancel, BIChannelTabSyncCancel,
			 CheckSyncCancel, LastValSyncCancel, SyncMiscValCancel, CheckProjectCancel, ProjectTabSyncCancel, CtrlTabSyncCancel;

  bool ValuesDecayCheckFirstReply;


	Set < CIISEvTabStatus, ETSMissingRec, ETSBIChannelCh > EvTabStatusSet;
  Set < CIISPrjTabStatus, PTSLocalError, PTSPrjNoDontMatch > PrjTabStatusSet;
  Set < CIISCtrlTabStatus, CTSLocalError, CTSMissingRec > CtrlTabStatusSet;
  Set < CIISZoneTabStatus, ZTSLocalError, ZTSMissingRec > ZoneTabStatusSet;
	Set < CIISSensTabStatus, STSLocalError, STSMissingRec > SensTabStatusSet;
	Set < CIISAlarmTabStatus, AlarmTSLocalError, AlarmTSMissingRec > AlarmTabStatusSet;
  Set < CIISPSTabStatus, PSTSLocalError, PSTSMissingRec > PSTabStatusSet;
	Set < CIISWLinkTabStatus, WLinkTSLocalError, WLinkTSMissingRec > WLinkTabStatusSet;
	Set < CIISBIChTabStatus, BIChTSLocalError, BIChTSMissingRec > BIChTabStatusSet;
  Set < CIISRecTabStatus, RecTSLocalError, RecTSMissingRec > RecTabStatusSet;
	Set < CIISValTabStatus, ValTSLocalError, ValTSMissingRec > ValTabStatusSet;
	Set < CIISLPRTabStatus, LPRTSLocalError, LPRTSMissingRec > LPRTabStatusSet;
	Set < CIISCVTabStatus, CVTSLocalError, CVTSMissingRec > CVTabStatusSet;
  Set < CIISDecayTabStatus, DecayTSLocalError, DecayTSMissingRec > DecayTabStatusSet;

  CIISEventRec *EvRec;
  CIISPrjRec *PrjRec, *PrjRecDS, *PrjRecSS, *PrjRecChS, *PrjRecPC, *PrjRecOC, *PrjRecLI, *PrjRecCS;
  CIISCtrlRec *CtrlRec;
  CIISZoneRec *ZRec;
  CIISZoneRec *ZRecLogger;
  CIISZoneRec *ZRecUser;
	CIISSensorRec *SRec;
  CIISSensorRec *SRecLogger;
	CIISSensorRec *SRecUser;
	CIISAlarmRec *AlarmRec;
	CIISAlarmRec *AlarmRecUser;
	CIISAlarmRec *AlarmRecLogger;
  CIISPowerSupplyRec *PSRec;
  CIISPowerSupplyRec *PSRecLogger;
  CIISPowerSupplyRec *PSRecUser;
	CIISWLinkRec *WRec;
  CIISWLinkRec *WRecLogger;
	CIISWLinkRec *WRecUser;
	CIISBIChannelRec *BIChRec;
	CIISBIChannelRec *BIChRecLogger;
	CIISBIChannelRec *BIChRecUser;
  CIISRecordingRec *RRec;
  CIISMonitorValueRec *MRec;
	CIISLPRValueRec *LPRRec;
  CIISCalcValueRec *CVRec;
  CIISDecayValueRec *DRec;
  CIISMiscValueRec *MiscRec;


  TList *Zones;
  TList *Sensors;
  TList *PSs;
	TList *WLinks;
	TList *Alarms;
	TList *BIChannels;

  void __fastcall Debug(String Message);
  bool __fastcall EventTabSync(void);

  void __fastcall SetEvTabStatus(int32_t EvCode);
  void __fastcall MessageRecived();
  bool __fastcall ParseCommands();
  bool __fastcall TransferResponse();
  int32_t  __fastcall Request( CIISCommand Cmd, CIISTable Tab, String Data);

  void __fastcall EventTabSyncIni();
  void __fastcall ProjectTabSyncIni();
	bool __fastcall SM_ProjectTabSync();
  void __fastcall ProjectTabCeckIni();
	bool __fastcall SM_ProjectTabCheck();
  void __fastcall SystemSyncSelectStep();
  void __fastcall CtrlTabSyncIni();
	bool __fastcall SM_CtrlTabSync();
  void __fastcall ZoneTabSyncIni();
  bool __fastcall ZoneTabSync();
  void __fastcall SensorTabSyncIni();
	bool __fastcall SensorTabSync();
	void __fastcall AlarmTabSyncIni();
	bool __fastcall AlarmTabSync();
  void __fastcall PSTabSyncIni();
  bool __fastcall PSTabSync();
	void __fastcall WLinkTabSyncIni();
	bool __fastcall WLinkTabSync();
	void __fastcall BIChannelTabSyncIni();
	bool __fastcall BIChannelTabSync();
  void __fastcall RecordingTabSyncIni();
  bool __fastcall RecordingTabSync();
  void __fastcall ValueTabSyncIni();
	bool __fastcall ValueTabSync();
	void __fastcall ValueLPRTabSyncIni();
	bool __fastcall ValueLPRTabSync();
	void __fastcall ValueCVTabSyncIni();
	bool __fastcall ValueCVTabSync();
  void __fastcall ValueDecayTabSyncIni();
  bool __fastcall ValueDecayTabSync();
  void __fastcall CheckSyncIni();
  bool __fastcall SM_CheckSync();
  void __fastcall LastValSyncIni();
	bool __fastcall SM_LastValSync();
  void __fastcall MiscValSyncIni();
	bool __fastcall SM_MiscValSync();
  void __fastcall SM_SendTransferMessage();
  void __fastcall SM_SystemSync();
  void __fastcall SM_DataSync();
  void __fastcall SM_LoggIn();
  void __fastcall SM_OpenCom();
  void __fastcall SM_CancelSync();
  void __fastcall SM_CloseCom();
  bool __fastcall SM_Running();

  void __fastcall InitZoneRec(CIISZoneRec *ZRec);
  void __fastcall InitSensorRec(CIISSensorRec *SRec);
	void __fastcall InitPowerSupplyRec(CIISPowerSupplyRec *PSRec);
	void __fastcall InitAlarmRec(CIISAlarmRec *AlarmRec);
	void __fastcall InitBIChannelRec(CIISBIChannelRec *BIChRec);
	void __fastcall InitWLinkRec(CIISWLinkRec *WLinkRec);

public:
  __fastcall TCIISServerInterface( TCIISDBModule * SetDB,
								   TDebugWin * SetDebugWin,
								   String SetIP,
								   int32_t SetPortNo);
  __fastcall ~TCIISServerInterface();
  void __fastcall SetTransferReply( TransferReplyFP SetTP );
	void __fastcall SetDebugWin(TDebugWin * SetDebugWin);
	bool __fastcall Connected();
  bool __fastcall TransferMessage( TCamurPacket *p );
  void __fastcall TimerSMEvent(TObject * Sender);
	void __fastcall TimerSMCancelEvent(TObject * Sender);

  bool __fastcall LoggInIni( String SetUserLevel, String SetPassword );
  bool __fastcall SystemSyncIni();
  bool __fastcall DataSyncIni();
  bool __fastcall OpenComIni();
  bool __fastcall CancelSyncIni();
  bool __fastcall CloseComIni();
	String __fastcall StatusMessage();

	TCIIClientSocket* __fastcall GetClientSocket();

__published:
	__property bool LoggedIn = { read=FLoggedIn };
	__property bool SysSyncRunning = { read=FSysSyncRunning };
  __property bool DataSyncRunning = { read=FDataSyncRunning};
  __property bool TransferMessageRunning = { read=FTransferMessageRunning };
  __property bool LoggInRunning = { read=FLoggInRunning };
  __property bool CancelSyncRunning = { read=FCancelSyncRunning };
  __property bool OpenComRunning = { read=FOpenComRunning };
  __property bool CloseComRunning = { read=FCloseComRunning };
};


#endif
