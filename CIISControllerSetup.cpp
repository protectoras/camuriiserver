//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISControllerSetup.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFrmControllerSetup *FrmControllerSetup;
//---------------------------------------------------------------------------
__fastcall TFrmControllerSetup::TFrmControllerSetup(TComponent* Owner,
																										TCIISController* Ctrl
																									 ): TForm(Owner)
{
	P_Ctrl = Ctrl;
	P_BI = P_Ctrl->GetBusInt();
	BICh = (TCIISBIChannel*) P_BI->GetBIChannel( 0 );



	//BICh->SetPMBI( &IncomingBusIntMessage );

	if( BICh != NULL )
	{
		CtrlIncommingBIChMessage = BICh->GetPMBI();
		BICh->SetPMBI( &IncomingBusIntMessage );
	}
	else
	{
		LSerialNo->Enabled = false;
		LBIMode->Enabled = false;
		LBIVer->Enabled = false;
	}

}


void __fastcall TFrmControllerSetup::FormClose(TObject *Sender, TCloseAction &Action)
{
	if( BICh != NULL ) BICh->SetPMBI( CtrlIncommingBIChMessage );
}

//---------------------------------------------------------------------------
void __fastcall TFrmControllerSetup::BGetSerialClick(TObject *Sender)
{
	 LSerialNo->Caption = BICh->BusInterfaceSerialNo;
	 //0P_BI->BusInterfaceSerialNo;
}
//---------------------------------------------------------------------------


void __fastcall TFrmControllerSetup::BGetBIModeClick(TObject *Sender)
{
	BICh->RequestBIMode();
}
//---------------------------------------------------------------------------

void __fastcall TFrmControllerSetup::IncomingBusIntMessage(const Byte *BusIntMsg, PVOID BIChannel)
{
 if ( BusIntMsg[0] == 35 )
 {
	 LBIMode->Caption = IntToStr( BusIntMsg[2] );
 }
}

void __fastcall TFrmControllerSetup::BGetBIVerClick(TObject *Sender)
{
	LBIVer->Caption = IntToStr( BICh->BusInterfaceVerMajor ) + "." + IntToStr( BICh->BusInterfaceVerMinor );
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

