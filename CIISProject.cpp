//---------------------------------------------------------------------------


#pragma hdrstop

#include "CIISProject.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

__fastcall TCIISProject::TCIISProject(TCIISDBModule *SetDB,
																			TDebugWin *SetDebugWin )
						:TCIISObj( SetDB, NULL, SetDebugWin, NULL )
{
	CIISCtrlRec *CR;
  bool MoreRecords;
  TCIISController *Ctrl;
	TCIISZone *Zone;
  BusIntList = new TList;

  CIISObjType = CIISProject;

	P_Prj = NULL;
  P_Ctrl = NULL;
  P_Zone = NULL;

  PR = new CIISPrjRec;
  ER = new CIISEventRec;
  RR = new CIISRecordingRec;
	MR = new CIISMonitorValueRec;
	LR = new CIISLPRValueRec;
	CalcR = new CIISCalcValueRec;
	DR = new CIISDecayValueRec;
	MiscR = new CIISMiscValueRec;

	MonitorValues = new TCIISMonitorValues( SetDB, NULL, SetDebugWin, MR, this );
	ChildList->Add( MonitorValues );
	DecayValues = new TCIISDecayValues( SetDB, NULL, SetDebugWin, DR, this );
	ChildList->Add( DecayValues );
	MiscValues = new TCIISMiscValues( SetDB, NULL, SetDebugWin, MiscR, this );
	ChildList->Add( MiscValues );
	LPRValues = new TCIISLPRValues( SetDB, NULL, SetDebugWin, LR, this );
	ChildList->Add( LPRValues );
	CalcValues = new TCIISCalcValues( SetDB, NULL, SetDebugWin, CalcR, this );
	ChildList->Add( CalcValues );

	Events = new TCIISEvents( SetDB, NULL, SetDebugWin, ER, this );
	ChildList->Add( Events );
	Recordings = new TCIISRecordings( SetDB, NULL, SetDebugWin, RR, this );
	ChildList->Add( Recordings );

  if( DB->GetPrjRec(PR) )
  {
		if( DB->FindFirstCtrl())
		{
			do
			{
				CR = new CIISCtrlRec;
				MoreRecords = DB->GetCtrlRec( CR, true );
				Ctrl = new TCIISController(SetDB, SetDebugWin, CR, this );
				ChildList->Add( Ctrl );
				BusIntList->Add( Ctrl->GetBusInt() );

			}while ( MoreRecords );
		}
		else
		{
			CR = NULL;
		}
	}
  else
  {
		PR->PrjName = "No database";
  }

  FAlarmStatusChanged = false;
  PrjUpdateAlarmDelay = 20;
  OnKeepMySQLAlive = 360;
  OnSlowClockTick = 100;

	FReqReset = false;

	// CIIS Shut Down

	PR->CIISStart = Now();
	FPowerShutdown = false;
	ShutdownReady = false;
	ShutdownNow = false;
	ShutDownDelay = ShutDownDelayTime;
	ShutDownState = 1;


	//uCtrl

	FShowuCtrlInfo = false;
	FuCtrlRecordingOn = false;
	FuCtrlReadingData = false;
	FuCtrlStoringData = false;
	FuCtrlTime = CIISStrToDateTime("1980-01-01 00:00:00");
	FuCtrlSyncTime = false;
	FuCtrlSyncData = false;
	FuCtrlSampleTimeStamp = CIISStrToDateTime("1980-01-01 00:00:00");
	FuCtrlPowerIn = false;
	FuCtrlPowerOn = false;
	FuCtrlPowerOk = false;

}

__fastcall TCIISProject::~TCIISProject()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;

	for( int32_t i = ChildList->Count - 1; i >= 0; i-- )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*)CIISObj;
			delete Ctrl;
			ChildList->Delete(i);
		}
	}

	for( int32_t i = BusIntList->Count - 1; i >= 0; i-- )
	{
		BusIntList->Delete(i);
	}

  delete BusIntList;
  delete Events;
  delete Recordings;
  delete MonitorValues;
  delete LPRValues;
  delete DecayValues;
  delete MiscValues;

  delete PR;

}


void __fastcall TCIISProject::TestBI()
{
  TCIISObj *CIISObj;
	TCIISController *Ctrl;

	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];

		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*)CIISObj;
			Ctrl->TestBI();
		}
	}
}

void __fastcall TCIISProject::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "Project: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
		if( P->UserLevel != ULNone )
		{
			ReadRec( P, O );
		}
		else
		{
			O->MessageCode = CIISMsg_UserLevelError;
		}
		break;

	case CIISWrite:
		if( P->UserLevel == ULErase  || P->UserLevel == ULModify)
		{
			WriteRec( P, O );
		}
		else
		{
			O->MessageCode = CIISMsg_UserLevelError;
		}
		break;

	case CIISAppend:
		O->MessageCode = 112;
		P->CommandMode = CIISCmdReady;
		break;

	case CIISDelete:
	break;

	case CIISLogin:
	Login( P, O );
	break;

	case CIISLogout:
	Logout( P, O );
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
		O->MessageCode = CIISMsg_Ok;
		O->MessageTable = 0;
		O->MessageData = "1=" +  IntToStr( PR->DBVer );
		break;

	case CIISReqPrgVer:
		O->MessageCode = CIISMsg_Ok;
		O->MessageTable = 0;
		O->MessageData = "1=" +  IntToStr( PR->PrgVer );
		break;

	case CIISReqTime:
		O->MessageCode = CIISMsg_Ok;
		O->MessageTable = 0;
		O->MessageData = "1=" + CIISDateTimeToStr(Now());
		break;

	case CIISSetTime:
		O->MessageTable = 0;
		if( DB->RecRunning() ) O->MessageCode = 255;
		else
		{
			TDateTime Adj;
			Adj = Now();
			if( CIISSetSysTime(CIISStrToDateTime(P->GetArg(1))) )
			{
			PR->DTAdj = Adj;
			PR->DTNew = Now();
			DB->SetPrjRec( PR ); DB->ApplyUpdatesPrj();
			this->Log( ClientCom, CIIEventCode_TimeSet, CIIEventLevel_High, P->GetArg(1), 0, 0 );
			O->MessageCode = CIISMsg_Ok;
			}
			else if( DB->RecRunning() ) O->MessageCode = 255;
		}
		break;

	case CIISServerType:
		O->MessageData = "1=CIISLogger\r\n";
    O->MessageCode = CIISMsg_Ok;
		break;


  }
}

#pragma argsused
void __fastcall TCIISProject::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "Project: ParseCommand_End" );
	#endif

	switch( P->CommandMode )
  {
		case CIISDeleteCtrl:
		DeleteCtrl( P );
		break;

		default:
        break;
  }
}

#pragma argsused
bool __fastcall TCIISProject::ThisRec( TCamurPacket *P )
{
  return true;
}

void __fastcall TCIISProject::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  if( P->ArgInc( 200 ))
  {
	 if( P->GetArg( 200 ) == "GetProjectNo" )
	 {
		O->MessageData = "2=" + PR->PrjNo + "\r\n";
		O->MessageCode = CIISMsg_Ok;
	 }
	 else if( P->GetArg( 200 ) == "GetProject" )
	 {
		O->MessageData =
		"1=" + PR->PrjName + "\r\n" +
		"2=" + PR->PrjNo + "\r\n" +
		"3=" + PR->Client + "\r\n" +
		"4=" + PR->Consultant + "\r\n" +
		"5=" + PR->Manager + "\r\n" +
		"6=" + PR->ContractDescription + "\r\n" +
		"7=" + PR->MMResponsibility + "\r\n" +
		"8=" + CIISDateTimeToStr(PR->Commissioning) + "\r\n" +
		"9=" + PR->Drawings + "\r\n" +
		"10=" + PR->Criteria + "\r\n" +
		"11=" + IntToStr(PR->ConnType) + "\r\n" +
		"12=" + PR->ConnRemote + "\r\n" +
		"13=" + PR->ConnServerIP + "\r\n" +
		"14=" + IntToStr(PR->PrjAlarmStatus) + "\r\n"+
		"15=" + IntToStr(PR->EventLogSize) + "\r\n" +
		"16=" + IntToStr(PR->EventLevel) + "\r\n" +
		"17=" + IntToStr(PR->ServerStatus) + "\r\n" +
		"18=" + IntToStr(PR->ServerMode) + "\r\n" +
		"19=" + IntToStr(PR->ConfigVer) + "\r\n" +
		"20=" + IntToStr(PR->DataVer) + "\r\n" +
		"21=" + IntToStr(PR->LastValVer) + "\r\n" +
		"22=" + IntToStr(PR->DBVer) + "\r\n";

		if( P->UserLevel > ULNone ) O->MessageData = O->MessageData + "24=" + PR->PWReadOnly + "\r\n";
		if( P->UserLevel > ULReadOnly ) O->MessageData = O->MessageData + "25=" + PR->PWModify + "\r\n";
		if( P->UserLevel > ULModify ) O->MessageData = O->MessageData + "26=" + PR->PWErase + "\r\n";

		O->MessageData = O->MessageData +
		"27=" + IntToStr(PR->PrgVer) + "\r\n"
		"28=" + CIISDateTimeToStr(PR->DTAdj) + "\r\n"
		"29=" + CIISDateTimeToStr(PR->DTNew) + "\r\n";

		O->MessageCode = CIISMsg_Ok;
	 }
	 else if( P->GetArg( 200 ) == "GetConfigDataVer" )
	 {
	    DB->GetPrjRec( PR ); 
		O->MessageData =
		"1=" + IntToStr(PR->ConfigVer) + "\r\n" +
		"2=" + IntToStr(PR->DataVer) + "\r\n" +
		"3=" + IntToStr(PR->LastValVer) + "\r\n";

		O->MessageCode = CIISMsg_Ok;
	 }
	}
	else
  {
	O->MessageData =
	"1=" + PR->PrjName + "\r\n" +
	"2=" + PR->PrjNo + "\r\n" +
	"3=" + PR->Client + "\r\n" +
	"4=" + PR->Consultant + "\r\n" +
	"5=" + PR->Manager + "\r\n";

	O->MessageCode = CIISMsg_Ok;
  }
  P->CommandMode = CIISCmdReady;
}

void __fastcall TCIISProject::WriteRec( TCamurPacket *P, TCamurPacket *O )
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
	if( P->ArgInc( 200 ))
	{
		if( P->GetArg( 200 ) == "Reset" )
		{
			FReqReset = true;
			O->MessageCode = CIISMsg_Ok;
		}
	}
	else
	{
		if( P->ArgInc(1) )
		{
			 PR->PrjName = P->GetArg(1);

			for( int32_t i = 0; i < ChildList->Count; i++ )
			{
				CIISObj = (TCIISObj*) ChildList->Items[i];
				if( this->CIISObjIs( CIISController ) )
				{
				Ctrl = (TCIISController*)CIISObj;
				Ctrl->SetPrjName( P->GetArg(1));

				}
			}
		}
		if( P->ArgInc(2) ) PR->PrjNo = P->GetArg(2);
		if( P->ArgInc(3) ) PR->Client = P->GetArg(3);
		if( P->ArgInc(4) ) PR->Consultant = P->GetArg(4);
		if( P->ArgInc(5) ) PR->Manager = P->GetArg(5);
		if( P->ArgInc(6) ) PR->ContractDescription = P->GetArg(6);
		if( P->ArgInc(7) ) PR->MMResponsibility = P->GetArg(7);
		if( P->ArgInc(8) ) PR->Commissioning = CIISStrToDateTime(P->GetArg(8));
		if( P->ArgInc(9) ) PR->Drawings = P->GetArg(9);
		if( P->ArgInc(10) ) PR->Criteria = P->GetArg(10);
		if( P->ArgInc(11) ) PR->ConnType = StrToInt( P->GetArg(11));
		if( P->ArgInc(12) ) PR->ConnRemote = P->GetArg(12);
		if( P->ArgInc(13) ) PR->ConnServerIP = P->GetArg(13);
		if( P->ArgInc(24) ) PR->PWReadOnly = P->GetArg(24);
		if( P->ArgInc(25) ) PR->PWModify = P->GetArg(25);
		if( P->ArgInc(26) ) PR->PWErase = P->GetArg(26);

		DB->SetPrjRec( PR ); DB->ApplyUpdatesPrj();
		this->Log( ClientCom, CIIEventCode_PrjTabChanged, CIIEventLevel_High, "", 0, 0 );
		O->MessageCode = CIISMsg_Ok;
	}
	P->CommandMode = CIISCmdReady;
}

void __fastcall TCIISProject::Login( TCamurPacket *P, TCamurPacket *O )
{
  if( P->ArgInc(1) && P->ArgInc(2) )
  {
	  if( P->GetArg(1) == "ReadOnly" && P->GetArg(2) == PR->PWReadOnly )
	  {
		  O->UserLevel = ULReadOnly;
		  O->MessageCode = CIISMsg_Ok;
		  O->MessageTable = 0;
		  O->MessageData = "1=" + DB->DatabaseName();
		  this->Log( ClientCom, CIIEventCode_LogIn, CIIEventLevel_High, P->GetArg(1), P->PortNo, 0 );
	  }
	  else if( P->GetArg(1) == "Modify" && P->GetArg(2) == PR->PWModify )
	  {
		  O->UserLevel = ULModify;
		  O->MessageCode = CIISMsg_Ok;
		  O->MessageTable = 0;
		  O->MessageData = "1=" + DB->DatabaseName();
		  this->Log( ClientCom, CIIEventCode_LogIn, CIIEventLevel_High, P->GetArg(1), P->PortNo, 0 );
	  }
	  else if( P->GetArg(1) == "Erase" && P->GetArg(2) == PR->PWErase )
	  {
		  O->UserLevel = ULErase;
		  O->MessageCode = CIISMsg_Ok;
		  O->MessageTable = 0;
		  O->MessageData = "1=" + DB->DatabaseName();
		  this->Log( ClientCom, CIIEventCode_LogIn, CIIEventLevel_High, P->GetArg(1), P->PortNo, 0 );
	  }
	  else
	  {
		  O->UserLevel = ULNone;
		  O->MessageCode = CIISMsg_LoggInError;
		  O->MessageTable = 0;
		  this->Log( ClientCom, CIIEventCode_LoggInError, CIIEventLevel_High, P->GetArg(1), P->PortNo, 0 );
	  }
  }
  else  // Arg1 or Arg2 not included
  {
	  O->UserLevel = ULNone;
	  O->MessageCode = CIISMsg_LoggInError;
	  O->MessageTable = 0;
	  this->Log( ClientCom, CIIEventCode_LoggInError, CIIEventLevel_High, P->GetArg(1), P->PortNo, 0 );
	}

  P->CommandMode = CIISCmdReady;
}

#pragma argsused
void __fastcall TCIISProject::Logout( TCamurPacket *P, TCamurPacket *O )
{
  O->UserLevel = ULNone;
  O->MessageCode = CIISMsg_Ok;
  O->MessageTable = 0;
  this->Log( ClientCom, CIIEventCode_LogOut, CIIEventLevel_High, "", 0, 0 );
}



void __fastcall TCIISProject::Set_ServerStatus( CIIServerStatus SStat )
{
  PR->ServerStatus = SStat;
  DB->SetPrjRec(PR); DB->ApplyUpdatesPrj();
}

void __fastcall TCIISProject::Set_ServerMode ( CIISServerMode SMode )
{
  PR->ServerMode = SMode;
  DB->SetPrjRec(PR); DB->ApplyUpdatesPrj();
}

void __fastcall TCIISProject::DeleteCtrl( TCamurPacket *P )
{
  TCIISObj *CIISObj;
  TCIISController *CtrlDelete;

  CtrlDelete = (TCIISController*) P->CIISObj;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			if( CtrlDelete == CIISObj )
			{
				ChildList->Delete(i);
				delete CtrlDelete;
				break;
			}
		}
	}
}

// Camur II Bus Interface

TList* __fastcall TCIISProject::GetBusIntList()
{
	return BusIntList;
}

void __fastcall TCIISProject::SysClockEvent()
{
	if( ShutdownNow ) SM_Shutdown();
	else this->ParseClockTick( Now() );
}

void __fastcall TCIISProject::OnSysClockTick( TDateTime TickTime )
{
  if( FAlarmStatusChanged ) UpdateAlarmStatus();

  OnSlowClockTick--;
  if( OnSlowClockTick < 0 )
  {
		OnSlowClockTick = 100; // Every 10s

		if( FPowerShutdown && ( TickTime > PR->CIISStart + TDateTime( 2 * OneMinute )))
		{
			ShutDownDelay -= 10 * Sec_To_mS;

			Debug( "CIIS Shuting down in " + IntToStr( ShutDownDelay ) + " mS " );

			if( ShutDownDelay <= 0 )
			{
			Debug( "CIIS Shutdown" );
			this->Log( SystemEvent, CIIEventCode_CIIShutdown, CIIEventLevel_High, "CII Shutdown", 0, 0 );
			ShutDownState = 1;
			ShutdownNow = true;
			}
		}
		else
		{
			ShutDownDelay = ShutDownDelayTime;
		}

		OnKeepMySQLAlive--;
		if(( OnKeepMySQLAlive < 0 ) && !FPowerShutdown )
		{
			OnKeepMySQLAlive = 360; // Once every houre
			PR->LastValVer++;
			DB->SetPrjRec(PR); DB->ApplyUpdatesPrj();
		}
  }


}

void __fastcall TCIISProject::SM_Shutdown()
{
  if( T >= TNextEvent)
  {
		switch (ShutDownState)
		{
		case 1:
			//Whait for incoming samples
			ShutDownState = 2;
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 2:
			PrepareShutdown();
			ShutDownState = 3;
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 3:
			DB->CloseCIISDB();
			ShutDownState = 9;
			TNextEvent += 5000;
			T += SysClock;
			break;

		case 9:
			if( CII_Controller_Shutdown() )
			{
			Debug( "Shuting down ..." );
			}
			else
			{
			Debug( "Shut down faild" );
			this->Log( SystemEvent, CIIEventCode_CIIShutdownFaild, CIIEventLevel_High, "CII Shutdown faild", 0, 0 );
			}

			FShutdownReady = true;

			ShutDownDelay = 5 * ShutDownDelayTime;

			TNextEvent += 60000;
			T += SysClock;
			break;
		}
  }
  else T += SysClock;
}

void __fastcall TCIISProject::PrepareShutdown()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;

  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISController ) )
	{
	  Ctrl = (TCIISController*) CIISObj;
	  Ctrl->PrepareShutdown();
	}
  }
}



bool __fastcall TCIISProject::CII_Controller_Shutdown()
{
   HANDLE hToken;
   TOKEN_PRIVILEGES tkp; 
 
   // Get a token for this process. 
 
   if (!OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
      return( FALSE ); 
 
   // Get the LUID for the shutdown privilege. 
 
   LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, 
        &tkp.Privileges[0].Luid); 
 
   tkp.PrivilegeCount = 1;  // one privilege to set    
   tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
 
   // Get the shutdown privilege for this process. 
 
   AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, 
        (PTOKEN_PRIVILEGES)NULL, 0); 
 
   if (GetLastError() != ERROR_SUCCESS) 
      return FALSE; 
 
   // Shut down the system and force all applications to close. 
 
   if (!ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE, 
               SHTDN_REASON_MAJOR_OPERATINGSYSTEM |
               SHTDN_REASON_MINOR_UPGRADE |
               SHTDN_REASON_FLAG_PLANNED)) 
      return FALSE; 

   //shutdown was successful
   return TRUE;
}

#pragma argsused
void __fastcall TCIISProject::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn  )
{

}

#pragma argsused
void __fastcall TCIISProject::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}

  /* TODO 1 -oBL -cNew version : Check Reset/Restart of  CANBus. Alarm handling */

bool __fastcall TCIISProject::ResetCIISBus()
{
	TCIISObj *CIISObj;
	TCIISController *Ctrl;
	bool ResetOk;

	ResetOk = true;

	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
/*
			if( CIISObj->CIISObjIs( CIISController ) )
			{
  			Ctrl = (TCIISController*)CIISObj;
  			ResetOk = ResetOk && Ctrl->ResetCIISBus();
			}
*/
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*)CIISObj;
			Ctrl->ResetCIISBus();
			//Ctrl->RestartCIISBus();
		}

	}

  if( ResetOk )
  {
		FAlarmStatusChanged = true;
	}

  return ResetOk;
}

void __fastcall TCIISProject::LastValChanged()
{
  PR->LastValVer++;
  DB->SetPrjRec( PR );
}

void __fastcall TCIISProject::DataChanged()
{
  PR->DataVer++;
  DB->SetPrjRec( PR );
}


void __fastcall TCIISProject::ConfigChanged()
{
  PR->ConfigVer++;
  DB->SetPrjRec( PR ); DB->ApplyUpdatesPrj();
}

int32_t __fastcall TCIISProject::GetNodeCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfNodes;

  NoOfNodes = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISController ) )
	{
	  Ctrl = (TCIISController*) CIISObj;
	  NoOfNodes = NoOfNodes + Ctrl->NodeCount;
	}
  }
  return NoOfNodes;
}

int32_t __fastcall TCIISProject::GetDetectedNodeCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfNodes;

  NoOfNodes = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
	CIISObj = (TCIISObj*) ChildList->Items[i];
	if( CIISObj->CIISObjIs( CIISController ) )
	{
	  Ctrl = (TCIISController*) CIISObj;
	  NoOfNodes = NoOfNodes + Ctrl->DetectedNodeCount;
	}
  }
  return NoOfNodes;
}

int32_t __fastcall TCIISProject::GetIniErrorCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfIniErrors;

  NoOfIniErrors = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfIniErrors = NoOfIniErrors + Ctrl->IniErrorCount;
		}
	}
  return NoOfIniErrors;
}

int32_t __fastcall TCIISProject::GetMonitorCount()
{
	TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfMonitors;

  NoOfMonitors = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfMonitors = NoOfMonitors + Ctrl->MonitorCount;
		}
  }
  return NoOfMonitors;
}

int32_t __fastcall TCIISProject::GetMonitorExtCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
	int32_t NoOfMonitorExt;

	NoOfMonitorExt = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
  {
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfMonitorExt = NoOfMonitorExt + Ctrl->MonitorExtCount;
		}
  }
  return NoOfMonitorExt;
}


int32_t __fastcall TCIISProject::GetDecayCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfDecays;

  NoOfDecays = 0;
  for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfDecays = NoOfDecays + Ctrl->DecayCount;
		}
  }
  return NoOfDecays;
}

int32_t __fastcall TCIISProject::GetLPRCount()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
  int32_t NoOfLPRs;

  NoOfLPRs = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfLPRs = NoOfLPRs + Ctrl->LPRCount;
		}
  }
  return NoOfLPRs;
}

int32_t __fastcall TCIISProject::GetUSBCANCount()
{
	TCIISObj *CIISObj;
	TCIISController *Ctrl;
	int32_t NoOfUSBCANs;

	NoOfUSBCANs = 0;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			Ctrl = (TCIISController*) CIISObj;
			NoOfUSBCANs += Ctrl->BIChCount;
		}
	}
  return NoOfUSBCANs;
}

void __fastcall TCIISProject::SetAlarmStatusChanged( bool AlarmStatusChanged )
{
 if( AlarmStatusChanged )
 {
   PrjUpdateAlarmDelay = 20;
   FAlarmStatusChanged = true;
 }
 else FAlarmStatusChanged = false;
}

void __fastcall TCIISProject::UpdateAlarmStatus()
{
  TCIISObj *CIISObj;
  TCIISController *Ctrl;
	bool Alarm, AlarmOnCtrl;

  PrjUpdateAlarmDelay--;

  if( PrjUpdateAlarmDelay <= 0 )
  {
		PrjUpdateAlarmDelay = 20;
		FAlarmStatusChanged = false;
		Alarm = false;

		for( int32_t i = 0; i < ChildList->Count; i++ )
		{
			CIISObj = (TCIISObj*) ChildList->Items[i];
			if( CIISObj->CIISObjIs( CIISController ) )
			{
				Ctrl = (TCIISController*)CIISObj;
				AlarmOnCtrl = Ctrl->UpdateAlarmStatus();
				Alarm = Alarm || AlarmOnCtrl;
			}
		}

		if( Alarm ) PR->PrjAlarmStatus = 1;
		else PR->PrjAlarmStatus = 0;
		DB->SetPrjRec( PR ); DB->ApplyUpdatesPrj();
  }
}


void  __fastcall TCIISProject::Log( CIIEventType EvType, CIIEventCode EvCode,
									CIIEventLevel EvLevel, String EvString,
									int32_t EvInt, double EvFloat)
{
  PR->ConfigVer++;
  DB->SetPrjRec( PR ); DB->ApplyUpdatesPrj();

  if( EvLevel > PR->EventLevel ) Events->Log( EvType, EvCode, EvLevel, EvString, EvInt, EvFloat, PR->EventLogSize );
}


	// Setup

TCIISController* __fastcall TCIISProject::GetCtrl( int32_t Ctrl_No )
{
	TCIISObj *CIISObj;
	TCIISController *Ctrl;
	int32_t CtrlCnt;

	CtrlCnt = 0;
	Ctrl = NULL;
	for( int32_t i = 0; i < ChildList->Count; i++ )
	{
		CIISObj = (TCIISObj*) ChildList->Items[i];
		if( CIISObj->CIISObjIs( CIISController ) )
		{
			CtrlCnt++;
			if (CtrlCnt == Ctrl_No)
			{
				Ctrl = (TCIISController*) CIISObj;
				break;
			}
		}
	}
	return Ctrl;
}
