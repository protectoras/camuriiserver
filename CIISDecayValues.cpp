//---------------------------------------------------------------------------
// History
//
// Date			Comment							Sign
// 2012-03-04	Converted to Xe2    			Bo H


#pragma hdrstop

#include "CIISDecayValues.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIISDecayValues::TCIISDecayValues( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISDecayValueRec *SetDecValRec, TObject *SetCIISParent )
						:TCIISObj( SetDB, SetCIISBusInt, SetDebugWin, SetCIISParent )
{
  CIISObjType = CIISValuesDecay;
  DecValRec = SetDecValRec;

  CIISObjType = CIISValuesDecay;
}

__fastcall TCIISDecayValues::~TCIISDecayValues()
{
  delete DecValRec;
}

void __fastcall TCIISDecayValues::ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O )
{

	#if DebugMsg == 1
	Debug( "DecayValues: ParseCommand_Begin" );
	#endif

  switch( P->MessageCommand )
  {
	case CIISRead:
	if( P->UserLevel != ULNone )
	{
	  ReadRec( P, O );
	}
	else
	{
	  O->MessageCode = CIISMsg_UserLevelError;
	}
	break;

	case CIISWrite:
	O->MessageCode = 102;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISAppend:
	O->MessageCode = 112;
	P->CommandMode = CIISCmdReady;
	break;

	case CIISDelete:
	break;

	case CIISLogin:
	break;

	case CIISLogout:
	break;

	case CIISSelectProject:
	break;

	case CIISExitProject:
	break;

	case CIISReqStatus:
	break;

	case CIISReqDBVer:
	break;

	case CIISReqPrgVer:
	break;

	case CIISReqTime:
	break;

	case CIISSetTime:
	break;
  }
}
#pragma argsused
void __fastcall TCIISDecayValues::ParseCommand_End( TCamurPacket *P, TCamurPacket *O )
{
	#if DebugMsg == 1
	Debug( "DecayValues: ParseCommand_End" );
	#endif
}

bool __fastcall TCIISDecayValues::ThisRec( TCamurPacket *P )
{
  return P->GetArg(1) == CIISDateTimeToStr( DecValRec->DateTimeStamp ) &&
		 P->GetArg(2) == IntToStr( DecValRec->RecNo ) &&
		 P->GetArg(3) == IntToStr( DecValRec->SensorSerialNo ) &&
		 P->GetArg(4) == DecValRec->ValueType;;
}

void __fastcall TCIISDecayValues::ReadRec( TCamurPacket *P, TCamurPacket *O )
{
  bool MoreRecords;

  //Decode request
  if( P->ArgInc( 200 ) )
  {
	if( P->GetArg( 200 ) == "GetFirstToEnd" )
    {
	  if( DB->FindFirstDecayValue() )P->CommandMode = CIISRecAdd;
	  else
	  {
		O->MessageCode = CIISMsg_RecNotFound;
		P->CommandMode = CIISCmdReady;
	  }
	}
	else if( P->GetArg( 200 ) == "GetNextToEnd" )
	{
	  if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) && P->ArgInc(4))
	  {
		if( DB->LocateDecayValue( CIISStrToDateTime( P->GetArg(1) ), StrToInt( P->GetArg(2) ), StrToInt( P->GetArg(3) ), P->GetArg(4) ))
		{
		  if( DB->FindNextDecayValue() ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_Ok;
			P->CommandMode = CIISCmdReady;
		  }
		}
		else if( DB->FindFirstDecayValue() )
		{
		  DB->GetDecayValueRec( DecValRec, false );
		  if( CIISStrToDateTime( P->GetArg(1) ) < DecValRec->DateTimeStamp ) P->CommandMode = CIISRecAdd;
		  else
		  {
			O->MessageCode = CIISMsg_RecNotFound;
			P->CommandMode = CIISCmdReady;
          }
		}
		else
		{
		  O->MessageCode = CIISMsg_RecNotFound;
		  P->CommandMode = CIISCmdReady;
		}
	  }
	  else if( P->ArgInc(99) )
	  {
		O->MessageCode = CIISMsg_NoLongerSupported;
	  }
	}
	else if( P->GetArg( 200 ) == "GetRecordCount") P->CommandMode = CIISRecCount;
  }

  // Execute request
  if( P->CommandMode == CIISRecAdd )
  {
	do
	{
	  MoreRecords = DB->GetDecayValueRec( DecValRec, true );
	  O->MessageData = O->MessageData +
	  "1=" + CIISDateTimeToStr( DecValRec->DateTimeStamp ) + "\r\n" +
	  "2=" + IntToStr( DecValRec->RecNo ) + "\r\n" +
	  "3=" + IntToStr( DecValRec->SensorSerialNo ) + "\r\n" +
	  "4=" + CIISFloatToStr( DecValRec->Value ) + "\r\n" +
	  "5=" + DecValRec->ValueType + "\r\n" +
	  "6=" + CIISDateTimeToStr( DecValRec->SampleDateTime ) + "\r\n";
	} while( O->MessageData.Length() < OutMessageLimit && MoreRecords  );

	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode == CIISRecCount )
  {
	if( P->ArgInc(1) && P->ArgInc(2) && P->ArgInc(3) && P->ArgInc(4) )
	{
	  DecValRec->DateTimeStamp = CIISStrToDateTime( P->GetArg(1) );
	  DecValRec->RecNo = StrToInt( P->GetArg(2) );
	  DecValRec->SensorSerialNo = StrToInt( P->GetArg(3) );
	  DecValRec->ValueType = P->GetArg(4);

	  O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetDecayValuesRecCountAfter( DecValRec ) ) + "\r\n";
	}
	else
	{
	  O->MessageData = O->MessageData + "100=" + IntToStr((int32_t) DB->GetDecayValuesRecCount() ) + "\r\n";
	}

	O->MessageCode = CIISMsg_Ok;
	P->CommandMode = CIISCmdReady;
  }
  else if( P->CommandMode != CIISCmdReady ) O->MessageCode = CIISMsg_UnknownCommand;
}
#pragma argsused
void __fastcall TCIISDecayValues::OnSysClockTick( TDateTime TickTime )
{

}
#pragma argsused
void __fastcall TCIISDecayValues::ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn )
{

}
#pragma argsused
void __fastcall TCIISDecayValues::ParseCIISBusMessage_End( TCIISBusPacket *BPIn )
{

}
