//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISClientInterfaceUser.h"
#include "CIISCommon.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

__fastcall TCIISClientInterfaceUser::TCIISClientInterfaceUser(TCIISDBModule * SetDB,
															  TDebugWin * SetDebugWin,
															  TCIISServerInterface *SetSerInt,
															  int32_t SetPortNoMonitor,
															  int32_t SetPortNoPM )
{
  DB = SetDB;
  DW = SetDebugWin;
  SerInt = SetSerInt;

	PortNoMonitor = SetPortNoMonitor;
	PortNoPM = SetPortNoPM;

  SelectPrjRunning = false;
  PMLoggInRunning = false;
  PMLoggOutRunning = false;
  PMExitPrjRunning = false;
  MonLoggInRunning = false;
  MonLoggOutRunning = false;
	MonCancelSyncRunning = false;

	SerInt->SetTransferReply( &TransferReply );
	SerIntClientSocket = SerInt->GetClientSocket();
	SerIntClientSocket->SetComTimeout( 60 );

  PrjRec = new CIISPrjRec;

	MSocket = new TCIIServerSocket( &MonitorMessage, PortNoMonitor, DW  );
	PMSocket = new TCIIServerSocket( &PMMessage, PortNoPM, DW );

	#if DebugClientInt == 1
	Debug("CIISClientInterfaceUser Created");
	#endif
}

__fastcall TCIISClientInterfaceUser::~TCIISClientInterfaceUser()
{
	delete MSocket;
  delete PMSocket;
  delete PrjRec;
}

TCIIServerSocket* __fastcall TCIISClientInterfaceUser::GetMSocket()
{
	return MSocket;
}

TCIIServerSocket* __fastcall TCIISClientInterfaceUser::GetPMSocket()
{
	return PMSocket;
}

void __fastcall TCIISClientInterfaceUser::TransferReply( TCamurPacket *o )
{
	#if DebugClientInt == 1
  Debug( "MSG Number: " + IntToStr( o->MessageNumber ) + " to local port" + ": " + o->MessageData  );
  #endif

  if( o->MessageCommand == CIISLogin )
  {
		if( o->MessageCode == CIISMsg_Ok ) o->MessageData = "1=" + DB->DatabaseName();
  }

  MSocket->PutMessage( o );
	MSocket->SendMessage();
}

void __fastcall TCIISClientInterfaceUser::PMMessage()
{
	while( ParsePMCommands() );
}

bool __fastcall TCIISClientInterfaceUser::ParsePMCommands()
{
	TCamurPacket  *p;
	int32_t ZoneNo;
	String Command;
	bool SM_Parse;
	TDateTime TOutOpenCom;

	p = PMSocket->GetMessage();
	if( p != NULL )
	{
		#if DebugClientInt == 1
		Debug( "Parsing PM MSG Number: " + IntToStr( p->MessageNumber ) + " : " + p->MessageData );
		#endif



		SM_Parse = false;

		PMReply = new TCamurPacket;
		PMReply->MessageType = CIISResponse;
		PMReply->PortNo = p->PortNo;
		PMReply->MessageNumber = p->MessageNumber;
		PMReply->MessageCode = CIISUnDefErr;
		PMReply->MessageCommand = p->MessageCommand;
		PMReply->MessageTable = p->MessageTable;
		PMReply->MessageData = "";

		if( p->MessageCommand == CIISLogin )
		{
			PMSocket->CancelSendMessage();

			if( p->ArgInc(3) ) FT_Timeout = StrToInt( p->GetArg( 3 ));
			else FT_Timeout = 60;

			if( PMLoggInIni()) SM_Parse = true;
			else
			{
				SM_Parse = false;
				PMReply->MessageCode = CIISMsg_LoggInError;
			}
		}
		else if( PMSocket->ClientLoggedIn )
		{
			switch( p->MessageCommand )
			{
			case CIISLogout: // Logout
				if( PMLoggOutIni()) SM_Parse = true;
				else
				{
					SM_Parse = false;
					PMReply->MessageCode = CIISUnDefErr;
				}
				break;

			case CIISSelectProject:
				MSocket->DisconnectClient();

				if( DB->SetDBDir( p->GetArg( 1 )))
				{
					PMSocket->PrjSelected = true;
					if( p->GetArg( 2 ) == "OnLine" )
					{
						if( SelectPrjIni(p->GetArg(3), p->GetArg(4)))
						{
							SM_Parse = true;
							PMReply->MessageCode = CIISMsg_Ok;
						}
						else
						{
							SM_Parse = false;
							PMReply->MessageCode = CIISMsg_LServerNotConn;
							MSocket->ConnectClient();
						}
					}
					else
					{
						DB->GetPrjRec( PrjRec );
						PrjRec->ServerStatus = CIISDisconnected;
						DB->SetPrjRec( PrjRec ); DB->ApplyUpdatesPrj();
						PMSocket->PrjSelOnline = false;
						MSocket->ConnectClient();
						PMReply->MessageCode = CIISMsg_Ok;
					}
				}
				break;

			case CIISReqStatus:
				if( SM_Running() || SerInt->DataSyncRunning  ) PMReply->MessageCode = CIISMsg_Busy;
				else PMReply->MessageCode = CIISMsg_Idle;

				PMReply->MessageData = SerInt->StatusMessage();
				break;

			case CIISExitProject:    //Exit project
				if( PMExitPrjIni()) SM_Parse = true;
				else
				{
					SM_Parse = false;
					PMReply->MessageCode = CIISUnDefErr;
				}
				break;

			case CIISWrite:
				if( p->MessageTable == CIISProject &&
					p->ArgInc(200) &&
					p->GetArg(200) == "SyncData" )
				{
					if( SerInt->DataSyncIni() ) PMReply->MessageCode = CIISMsg_Ok;
					else PMReply->MessageCode = CIISMsg_LServerNotConn;
				}
				else if( p->MessageTable == CIISProject &&
						p->ArgInc(200) &&
						p->GetArg(200) == "CancelSyncData" )
				{
					if( PMExitPrjIni()) SM_Parse = true;
					else
					{
						SM_Parse = false;
						PMReply->MessageCode = CIISUnDefErr;
					}
				}
				break;

			} // switch( p->MessageCommand )
		}
		else  // Not logged in
		{
			SM_Parse = false;
			PMReply->MessageCode = CIISMsg_UserLevelError;
			PMReply->MessageTable = 0;
		}

		if( !SM_Parse )
		{
			PMSocket->PutMessage( PMReply );
      PMSocket->SendMessage();
		}

		delete p;
		return true;
	}
	else return false;
}

bool __fastcall TCIISClientInterfaceUser::SelectPrjIni( String SetUserLevel, String SetPassword )
{
	if( SM_Running() ) return false;

  UserLevel = SetUserLevel;
  Password = SetPassword;
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );
	SelPrjState = CIISSelPrjLoggIn;
	SelectPrjRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: SelectPrjIni");
	#endif

  return true;
}

void __fastcall TCIISClientInterfaceUser::SM_SelectPrj()
{
	if( Now() > TOutSM ) SelPrjState = CIISSelPrjTimeOut;

  switch( SelPrjState )
  {
  case CIISSelPrjLoggIn :
		SerInt->LoggInIni( UserLevel, Password );
		SelPrjState = CIISSelPrjCheckLoggIn;

		#if DebugClientInt == 1
		Debug("ClientInt/SelPrj: LoggIn on CIILogger");
		#endif
		break;

  case CIISSelPrjCheckLoggIn :
		if( !SerInt->LoggInRunning )
		{
			if( SerInt->LoggedIn )
			{
				PMReply->MessageCode = CIISMsg_Ok;
				SelPrjState = CIISSelPrjSystemSync;

				#if DebugClientInt == 1
				Debug("ClientInt/SelPrj: LoggIn on CIILogger: OK");
				#endif
			}
			else
			{
				PMReply->MessageCode = CIISMsg_LoggInError;
				SelPrjState = CIISSelPrjReady;

				#if DebugClientInt == 1
				Debug("ClientInt/SelPrj: LoggIn on CIILogger: Faild");
				#endif
			}
		}
		break;

	case CIISSelPrjSystemSync:
		if( SerInt->SystemSyncIni() )
		{
			SelPrjState = CIISSelPrjCheckSync;

			#if DebugClientInt == 1
			Debug("ClientInt/SelPrj: SystemSyncIni: OK");
			#endif
		}
		break;

	case CIISSelPrjCheckSync:
		if( !SerInt->SysSyncRunning )
		{
			SelPrjState = CIISSelPrjConnClient;

			#if DebugClientInt == 1
			Debug("ClientInt/SelPrj: SystemSync: Ready");
			#endif
		}
		break;

	case CIISSelPrjConnClient:
		PMSocket->PrjSelected = true;
		PMSocket->PrjSelOnline = true;
		MSocket->ConnectClient();
		SelPrjState = CIISSelPrjReady;

		#if DebugClientInt == 1
		Debug("ClientInt/SelPrj: MSocket->ConnectClient(): Started");
		#endif
		break;

	case CIISSelPrjTimeOut:
		SerInt->CancelSyncIni();
		PMReply->MessageCode = CIISMsg_SelPrjTimeOut;
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		SelectPrjRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISSelPrjTimeOut");
		#endif

		break;

	case CIISSelPrjReady :
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		SelectPrjRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISSelPrjReady");
		#endif

		break;

  }//switch( SelectPrjState );
}

bool __fastcall TCIISClientInterfaceUser::PMLoggInIni()
{
	if( SM_Running() ) return false;
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );
	PMLoggInState = CIISPMLoggInCancelSync;
  PMLoggInRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: PMLoggInIni");
	#endif

  return true;
}

void __fastcall TCIISClientInterfaceUser::SM_PMLoggIn()
{
  if( Now() > TOutSM ) PMLoggInState =  CIISPMLoggInTimeOut;

  switch( PMLoggInState )
  {
		case CIISPMLoggInCancelSync :
		if( SerInt->CancelSyncIni() ) PMLoggInState = CIISPMLoggInCheckCancelSync;
		break;

  case CIISPMLoggInCheckCancelSync :
		if( !SerInt->CancelSyncRunning )
		{
			if( SerInt->CloseComIni() ) PMLoggInState = CIISPMLoggInCheckCloseCom;
		}
		break;

  case CIISPMLoggInCheckCloseCom :
		if( !SerInt->CloseComRunning) PMLoggInState = CIISPMLoggInOk;
		break;

  case CIISPMLoggInOk :
		PMSocket->UserLevel = ULModify;
		PMSocket->ClientLoggedIn = true;

		PMReply->MessageCode = CIISMsg_Ok;
		PMReply->MessageTable = 0;
		PMReply->MessageData = "1=" + DB->DatabaseName();

		PMLoggInState = CIISPMLoggInReady;
		break;

	case CIISPMLoggInTimeOut :
		PMSocket->UserLevel = ULNone;
		PMSocket->ClientLoggedIn = false;

		PMReply->MessageCode = CIISMsg_LoggInTimeOut;
		PMReply->MessageTable = 0;
		PMReply->MessageData = "";
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		PMLoggInRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISPMLoggInTimeOut");
		#endif

		break;

	case CIISPMLoggInReady :
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		PMLoggInRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISPMLoggInReady");
		#endif

		break;
	}
}

bool __fastcall TCIISClientInterfaceUser::PMLoggOutIni()
{
	if( SM_Running() ) return false;
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );
	PMLoggOutState = CIISPMLoggOutCancelSync;
	PMLoggOutRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: PMLoggOutIni");
	#endif

	return true;
}

void __fastcall TCIISClientInterfaceUser::SM_PMLoggOut()
{
	if( Now() > TOutSM ) PMLoggOutState =  CIISPMLoggOutTimeOut;

	switch( PMLoggOutState )
	{
		case CIISPMLoggOutCancelSync :
			if( SerInt->CancelSyncIni() ) PMLoggOutState = CIISPMLoggOutCheckCancelSync;
			break;

		case CIISPMLoggOutCheckCancelSync :
			if( !SerInt->CancelSyncRunning )
			{
				if( SerInt->CloseComIni() ) PMLoggOutState = CIISPMLoggOutCheckCloseCom;
			}
			break;

		case CIISPMLoggOutCheckCloseCom :
			if( !SerInt->CloseComRunning) PMLoggOutState = CIISPMLoggOutOk;
			break;

		case CIISPMLoggOutOk :
			PMSocket->PrjSelected = false;
			PMSocket->PrjSelOnline = false;
			PMSocket->UserLevel = ULNone;
			PMSocket->ClientLoggedIn = false;

			PMReply->MessageCode = CIISMsg_Ok;
			PMReply->MessageTable = 0;
			PMReply->MessageData = "";
			DB->GetPrjRec( PrjRec );
			if( CIIEventLevel_High > PrjRec->EventLevel ) DB->LogEvent( ClientCom, CIIEventCode_LogOut, CIIEventLevel_High, "", 0, 0, PrjRec->EventLogSize );

			PMLoggOutState = CIISPMLoggOutReady;
			break;

		case CIISPMLoggOutTimeOut :
			PMReply->MessageCode = CIISMsg_LoggOutTimeOut;
			PMReply->MessageTable = 0;
			PMReply->MessageData = "";
			PMSocket->PutMessage( PMReply );
			PMSocket->SendMessage();
			PMLoggOutRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: PMLoggOutIni");
			#endif

			break;

		case CIISPMLoggOutReady :
			PMSocket->PutMessage( PMReply );
			PMSocket->SendMessage();
			PMLoggOutRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: CIISPMLoggOutReady");
			#endif

			break;

	 }
}

bool __fastcall TCIISClientInterfaceUser::PMExitPrjIni()
{
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );

	SelectPrjRunning = false;
	PMLoggInRunning = false;
	PMLoggOutRunning = false;
	MonLoggInRunning = false;
	MonLoggOutRunning = false;
	MonCancelSyncRunning = false;

	PMExitPrjState = CIISExitPrjCancelSync;
	PMExitPrjRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: PMExitPrjIni");
	#endif

  return true;
}

void __fastcall TCIISClientInterfaceUser::SM_PMExitPrj()
{
	if( Now() > TOutSM ) PMExitPrjState =  CIISPMExitPrjTimeOut;

  switch( PMExitPrjState )
  {
  case CIISExitPrjCancelSync :
		if( SerInt->CancelSyncIni() ) PMExitPrjState = CIISExitPrjCheckCancelSync;
		break;

  case CIISExitPrjCheckCancelSync :
		if( !SerInt->CancelSyncRunning )
		{
			if( SerInt->CloseComIni() ) PMExitPrjState = CIISExitPrjCheckCloseCom;
		}
		break;

  case CIISExitPrjCheckCloseCom :
		if( !SerInt->CloseComRunning) PMExitPrjState = CIISPMExitPrjOk;
		break;

  case CIISPMExitPrjOk :
		PMSocket->PrjSelected = false;
		PMSocket->PrjSelOnline = false;
		MSocket->DisconnectClient();
		PMReply->MessageCode = CIISMsg_Ok;
		PMReply->MessageTable = 0;
		PMReply->MessageData = "";
		DB->GetPrjRec( PrjRec );
		if( CIIEventLevel_High > PrjRec->EventLevel ) DB->LogEvent( ClientCom, CIIEventCode_LogOut, CIIEventLevel_High, "", 0, 0, PrjRec->EventLogSize );
		PMExitPrjState = CIISExitPrjReady;
		break;

	case CIISPMExitPrjTimeOut :
		PMReply->MessageCode = CIISMsg_ExitPrjTimeOut;
		PMReply->MessageTable = 0;
		PMReply->MessageData = "";
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		PMExitPrjRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISPMExitPrjTimeOut");
		#endif

		break;

	case CIISExitPrjReady :
		PMSocket->PutMessage( PMReply );
		PMSocket->SendMessage();
		PMExitPrjRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISExitPrjReady");
		#endif

		break;
  }
}

bool __fastcall TCIISClientInterfaceUser::MonLoggInIni()
{
	if( SM_Running() ) return false;
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );
	MonLoggInState = CIISMonLoggInCancelSync;
	MonLoggInRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: MonLoggInIni");
	#endif

	return true;
}

void __fastcall TCIISClientInterfaceUser::SM_MonLoggIn()
{
  if( Now() > TOutSM ) MonLoggInState = CIISMonLoggInTimeOut;

  switch( MonLoggInState )
  {
	case CIISMonLoggInCancelSync :
	  if( SerInt->CancelSyncIni() ) MonLoggInState = CIISMonLoggInCheckCancelSync;
		break;

	case CIISMonLoggInCheckCancelSync :
	  if( !SerInt->CancelSyncRunning )
	  {
			if( SerInt->CloseComIni() ) MonLoggInState = CIISMonLoggInCheckCloseCom;
		}
	  break;

	case CIISMonLoggInCheckCloseCom :
	  if( !SerInt->CloseComRunning) MonLoggInState = CIISMonLoggIn;
	  break;

	case CIISMonLoggIn :
	  SerInt->LoggInIni( MonUserLevel, MonPassword );
	  MonLoggInState = CIISMonCheckLoggIn;
	  break;

	case CIISMonCheckLoggIn :
	  if( !SerInt->LoggInRunning )
	  {
		 if( SerInt->LoggedIn )
		 {
		   MReply->MessageCode = CIISMsg_Ok;
		   MonLoggInState = CIISMonSystemSync;
		 }
		 else MonLoggInState =  CIISMonLoggInTimeOut;
	  }
	  break;

	case  CIISMonSystemSync:
	  if( SerInt->SystemSyncIni() ) MonLoggInState = CIISMonCheckSystemSync;
	  break;

	case  CIISMonCheckSystemSync:
	  if( !SerInt->SysSyncRunning ) MonLoggInState = CIISMonLoggInOk;
	  break;

	case CIISMonLoggInOk :
	  MReply->MessageCode = CIISMsg_Ok;
	  MReply->MessageData = "1=" + DB->DatabaseName();

	  MonLoggInState = CIISMonLoggInReady;
	  break;

	case CIISMonLoggInTimeOut :
	  MReply->MessageCode = CIISMsg_LoggInTimeOut;
	  MReply->MessageTable = 0;
	  MReply->MessageData = "";
	  MSocket->PutMessage( MReply );
		MSocket->SendMessage();
		MonLoggInRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISMonLoggInTimeOut");
		#endif

		break;

	case CIISMonLoggInReady :
		MSocket->PutMessage( MReply );
		MSocket->SendMessage();
		MonLoggInRunning = false;

		#if DebugClientInt == 1
		Debug("ClientInt: CIISMonLoggInReady");
		#endif

		break;
  }
}

bool __fastcall TCIISClientInterfaceUser::MonLoggOutIni()
{
	if( SM_Running() ) return false;
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );
	MonLoggOutState = CIISMonLoggOutCancelSync;
	MonLoggOutRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: MonLoggOutIni");
	#endif

	return true;
}

void __fastcall TCIISClientInterfaceUser::SM_MonLoggOut()
{
	if( Now() > TOutSM ) MonLoggOutState =  CIISMonLoggOutTimeOut;

	switch( MonLoggOutState )
	{
		case CIISMonLoggOutCancelSync :

			if( SerInt->CancelSyncIni() ) MonLoggOutState = CIISMonLoggOutCheckCancelSync;
			break;

		case CIISMonLoggOutCheckCancelSync :
			if( !SerInt->CancelSyncRunning )
			{
				if( SerInt->CloseComIni() ) MonLoggOutState = CIISMonLoggOutCheckCloseCom;
			}
			break;

		case CIISMonLoggOutCheckCloseCom :
			if( !SerInt->CloseComRunning) MonLoggOutState = CIISMonLoggOutOk;
			break;

		case CIISMonLoggOutOk :
			MReply->MessageCode = CIISMsg_Ok;
			MonLoggOutState = CIISMonLoggOutReady;
			break;

		case CIISMonLoggOutTimeOut :
			MReply->MessageCode = CIISMsg_LoggOutTimeOut;
			MSocket->PutMessage( MReply );
			MSocket->SendMessage();
			MonLoggOutRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: CIISMonLoggOutTimeOut");
			#endif

			break;

		case CIISMonLoggOutReady :
			MSocket->PutMessage( MReply );
			MSocket->SendMessage();
			MonLoggOutRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: CIISMonLoggOutReady");
			#endif

			break;
	}
}

bool __fastcall TCIISClientInterfaceUser::MonCancelSyncIni()
{
	TOutSM = Now() + TDateTime( FT_Timeout * OneSecond );

	SelectPrjRunning = false;
	PMLoggInRunning = false;
	PMLoggOutRunning = false;
	MonLoggInRunning = false;
	MonLoggOutRunning = false;
	PMExitPrjRunning = false;

	MonCancelSyncState = CIISMonCancelSync;
	MonCancelSyncRunning = true;
	TimerSMEnabled = true;

	#if DebugClientInt == 1
	Debug("ClientInt: MonCancelSyncIni");
	#endif

	return true;
}

void __fastcall TCIISClientInterfaceUser::SM_MonCancelSync()
{
	if( Now() > TOutSM ) MonCancelSyncState =  CIISMonCanselSyncTimeOut;

	switch( MonCancelSyncState )
	{
		case CIISMonCancelSync:
			if( SerInt->CancelSyncIni() ) MonCancelSyncState = CIISMonCheckCancelSync;
			break;

		case CIISMonCheckCancelSync :
			if( !SerInt->CancelSyncRunning )MonCancelSyncState = CIISMonCancelSyncOk;
			break;

		case CIISMonCancelSyncOk :
			MReply->MessageCode = CIISMsg_Ok;
			MonCancelSyncState = CIISMonCancelSyncReady;
			break;

		case CIISMonCanselSyncTimeOut :
			MReply->MessageCode = CIISMsg_CanSyncTimeOut;
			MSocket->PutMessage( MReply );
			MSocket->SendMessage();
			MonCancelSyncRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: CIISMonCanselSyncTimeOut");
			#endif

			break;

		case CIISMonCancelSyncReady :
			MSocket->PutMessage( MReply );
			MSocket->SendMessage();
			MonCancelSyncRunning = false;

			#if DebugClientInt == 1
			Debug("ClientInt: CIISMonCancelSyncReady");
			#endif

			break;
  }
}

bool __fastcall TCIISClientInterfaceUser::SM_Running()
{
return ( SelectPrjRunning ||
				 PMLoggInRunning ||
				 PMLoggOutRunning  ||
				 PMExitPrjRunning ||
				 MonLoggInRunning ||
				 MonLoggOutRunning ||
				 MonCancelSyncRunning );
}

void __fastcall TCIISClientInterfaceUser::TimerSMEvent(TObject * Sender)
{
	if( !TimerSMEnabled ) return;

	if( SelectPrjRunning ) SM_SelectPrj();
	else if( PMLoggInRunning ) SM_PMLoggIn();
  else if( PMLoggOutRunning ) SM_PMLoggOut();
  else if( PMExitPrjRunning ) SM_PMExitPrj();
  else if( MonLoggInRunning ) SM_MonLoggIn();
  else if( MonLoggOutRunning ) SM_MonLoggOut();
	else if( MonCancelSyncRunning ) SM_MonCancelSync();
	else TimerSMEnabled = false;
}

void __fastcall TCIISClientInterfaceUser::MonitorMessage()
{
	while( ParseMonitorCommands() );
}

bool __fastcall TCIISClientInterfaceUser::ParseMonitorCommands()
{

  TCamurPacket  *p; //Command
  int32_t ZoneNo;
  String Command, Stop;
  bool ReadOK, ModifyOK, EraseOK;
  bool SM_Parse;
  int32_t RNo, LPRStep, Sensor, PS;
  TDateTime RecStart, DTS;
  double  Interval;

  SM_Parse = false;
  ReadOK = false;
  ModifyOK = false;
  EraseOK = false;

  p = MSocket->GetMessage();

  if( p != NULL )
	{
		if( p->PortNo == PortNoMonitor && MSocket->ClientLoggedIn )
		{
			switch (MSocket->UserLevel)
			{
				case ULErase: EraseOK = true;
				case ULModify: ModifyOK = true;
				case ULReadOnly: ReadOK = true;
                default: break;
			}
		}

		#if DebugClientInt == 1
		Debug( "Parsing Monitor MSG Number: " + IntToStr( p->MessageNumber ) + ": " + p->MessageData );
		#endif

		MReply = new TCamurPacket;
		MReply->MessageType = CIISResponse;
		MReply->PortNo = p->PortNo;
		MReply->MessageNumber = p->MessageNumber;
		MReply->MessageCode = CIISUnDefErr;
		MReply->MessageCommand = p->MessageCommand;
		MReply->MessageTable = p->MessageTable;
		MReply->MessageData = "";


		if( p->MessageCommand == CIISLogin )
		{
			if( PMSocket->PrjSelected )
			{
				MSocket->UserLevel = PMSocket->UserLevel;
				MSocket->ClientLoggedIn = PMSocket->ClientLoggedIn;
				MReply->MessageData = "1=" + DB->DatabaseName();
				MReply->MessageCode = CIISMsg_Ok;
			}
			else
			{
				if( p->ArgInc(1) && p->ArgInc(2) )
				{
					MonUserLevel = p->GetArg(1);
					MonPassword = p->GetArg(2);
					DB->GetPrjRec( PrjRec );

					if( MonUserLevel == "ReadOnly" && MonPassword == PrjRec->PWReadOnly )
					{
						MSocket->UserLevel = ULReadOnly;
						MSocket->ClientLoggedIn = true;
					}
					else if( MonUserLevel == "Modify" && MonPassword == PrjRec->PWModify )
					{
						MSocket->UserLevel = ULModify;
						MSocket->ClientLoggedIn = true;
					}
					else if( MonUserLevel == "Erase" && MonPassword == PrjRec->PWErase )
					{
						MSocket->UserLevel = ULErase;
						MSocket->ClientLoggedIn = true;
					}
					else
					{
						MSocket->UserLevel = ULNone;
						MSocket->ClientLoggedIn = false;
					}
				}
				else  // Arg1 or Arg2 not included
				{
					MSocket->UserLevel = ULNone;
					MSocket->ClientLoggedIn = false;
				}

				if( MonLoggInIni()) SM_Parse = true;
				else MReply->MessageCode = CIISMsg_LoggInError;
			}
		}
		else if( ReadOK )
		{
			if( p->MessageCommand == CIISReqStatus )
			{
				if( SM_Running() || SerInt->DataSyncRunning ) MReply->MessageCode = CIISMsg_Busy;
				else MReply->MessageCode = CIISMsg_Idle;
				MReply->MessageData = SerInt->StatusMessage();
				SM_Parse = false;
			}
			else if( p->MessageCommand == CIISServerType )
			{
				MReply->MessageData = "1=CIISUser\r\n";
        MReply->MessageCode = CIISMsg_Ok;
				SM_Parse = false;
      }
			else if( p->MessageCommand == CIISWrite && p->MessageTable == CIISProject &&
				p->ArgInc(200) && p->GetArg(200) == "SyncData" )
			{
				if( SerInt->DataSyncIni() ) MReply->MessageCode = CIISMsg_Ok;
				else MReply->MessageCode = CIISMsg_LServerNotConn;
			}

			else if( p->MessageCommand == CIISWrite && p->MessageTable == CIISProject &&
							 p->ArgInc(200) && p->GetArg(200) == "CancelSyncData" )
			{
				if( MonCancelSyncIni() ) SM_Parse = true;
				else MReply->MessageCode = CIISUnDefErr;
			}

			else if( p->MessageCommand == CIISLogout )
			{
				if( MonLoggOutIni()) SM_Parse = true;
				else MReply->MessageCode = CIISUnDefErr;
			}
			else
			{
				#if DebugClientInt == 1
				Debug( "Tranfering MSG Number  : "+IntToStr( p->MessageNumber ) + ": " + p->MessageData );
				#endif
				if( SerInt->TransferMessage( p ) ) SM_Parse = true;
				else MReply->MessageCode = CIISMsg_LServerNotConn;
			}
		}  // ReadOK
		else
		{
			MReply->MessageCode = CIISMsg_UserLevelError;
		}

		if( !SM_Parse )
		{
			MSocket->PutMessage( MReply );
			MSocket->SendMessage();
		}
		return true;
	}
	else return false;  // No more messages in queue
}

void __fastcall TCIISClientInterfaceUser::SetDebugWin(TDebugWin * SetDebugWin)
{
  DW = SetDebugWin;
  MSocket->SetDebugWin( SetDebugWin );
  PMSocket->SetDebugWin( SetDebugWin );
}

void __fastcall TCIISClientInterfaceUser::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

bool __fastcall TCIISClientInterfaceUser::GetLocalClientConnected()
{
	return MSocket->ClientConnected;
}

bool __fastcall TCIISClientInterfaceUser::GetPMConnected()
{
	return PMSocket->ClientConnected;
}




