//---------------------------------------------------------------------------

#ifndef CIISPowerSupplyH
#define CIISPowerSupplyH
//---------------------------------------------------------------------------
#include "CIISObj.h"
#include "CIISNodeIni.h"
#include "CIISCommon.h"

class TCIISPowerSupply : public TCIISObj
{
private:
  //CIISClientInt
  bool __fastcall ThisRec( TCamurPacket *P );
  void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
  void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );

  virtual void __fastcall SM_UpdateLastValue();

  //CIISBusInt
  void __fastcall PSIni( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  void __fastcall PSIniVersion( TCIISBusPacket *BPIn );
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall uCtrlIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
  virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );

  int32_t __fastcall GetZoneNo() { return PSRec->ZoneNo; }
  int32_t __fastcall GetSerialNo() { return PSRec->PSSerialNo; }
  int32_t __fastcall GetCanAdr() { return PSRec->PSCanAdr; }
  int32_t __fastcall GetZoneCanAdr() { return PSRec->ZoneCanAdr; }
  int32_t __fastcall GetPSStatus() { return PSRec->PSStatus; }
  CIISNodeCapability __fastcall GetPSType() { return (CIISNodeCapability)PSRec->PSType; }
  bool __fastcall GetAlarmStatus() { return ( PSRec->PSAlarmStatusU == 1 ) || ( PSRec->PSAlarmStatusI == 1 ) || ( PSRec->CANAlarmStatus == 1 ); }
  void _fastcall SetZoneCanAdr( int32_t ZoneCanAdr );
  virtual void __fastcall SetDefaultValuesSubType();
  bool __fastcall GetUpdateLastValueRunning() { return UpdateLastValueRunning; }
	bool __fastcall GetPSTemp() { return PSRec->PSTemp; }


  bool LogCanDupTag, LogCanMissingTag, MissingTagDetected;
	int32_t DupCount;

  String __fastcall GetPSName() { return PSRec->PSName; }



protected:
  //CIISClientInt
  CIISPowerSupplyRec *PSRec;

  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt
  //TCIISBIChannel *CIISBICh;
  int32_t IniRetrys;
	bool PSIniRunning, UpdateLastValueRunning;
	bool FSampReqRunnig, FTempReqRunning, FSensGuardReqRunning, FLastValueSampRunning, FLastValueTempRunning;
	NodeIniState NIState;
  int32_t T, TNextEvent;
	int32_t T2, T2NextEvent, T2State;
	int32_t T3, T3NxtEvent;
	bool NodeDetected;
	bool CapabilityRecived;
  bool CalibValuesRecived;
  bool BitValueRecived;
  int32_t  BitValueRequested;
  bool VerRecived;

  int32_t FIniErrorCount;
  int32_t LastStoredTag;

  int32_t OnSlowClockTick;
  int32_t RequestSenseGuardTag, ExpectedSenseGuardTag;
  int32_t SenseGuardDelay;
	bool OutputTmpOff;
  int32_t RampDelay;
  
  CIISPSIniState IniState;
  bool PSIniStateRunning;
  double VRamp, IRamp;
	bool FIniDecaySample;
	bool FRunDistDecay;
  TDateTime IniDecayTime;

  int16_t ADVal1, ADVal2;
  uint16_t UADVal1, UADVal2;
  double AnVal1, AnVal2, AnVal1_Scl, AnVal2_Scl, LowLev1, HighLev1, LowLev2, HighLev2, IShunt;

	bool PSLastValueRequest, PSLastValueExtRequest, PSLastValueTempRequest;
  TDateTime SampleTimeStamp;

	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );
  bool __fastcall CheckAlarm();
  void __fastcall ClearLastValues(void);

  //uCtrl

  bool FuCtrlZone;
  int32_t FuCtrlNodeIndex;

public:
  __fastcall TCIISPowerSupply( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply();
  bool __fastcall ResetCIISBus();
  bool __fastcall NodeIniReady();
  void __fastcall SetDefaultValues();
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );
	void __fastcall PSIniState();
	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();
	void __fastcall PSOnAlarm();
	void __fastcall CheckSampleRecived( int32_t RequestedTag );
	void __fastcall ResetLastStoredTag() { LastStoredTag = 0; }
	void __fastcall SetOutputTmpOffFlag() { OutputTmpOff = true; }

  void __fastcall ResetCANAlarm();

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
  virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
  virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );

__published:

  __property int32_t ZoneNo = { read = GetZoneNo };
  __property int32_t SerialNo = { read = GetSerialNo };
  __property int32_t CanAdr = { read = GetCanAdr };
  __property int32_t ZoneCanAdr = { read = GetZoneCanAdr, write = SetZoneCanAdr };
  __property bool IniRunning = { read = PSIniRunning, write = PSIniRunning };
  __property int32_t PSStatus = { read = GetPSStatus };
  __property CIISNodeCapability PSType = {read = GetPSType };
  __property bool AlarmStatus = { read = GetAlarmStatus };
//  __property TCIISBIChannel* BIChannel = { read = CIISBICh, write = CIISBICh };
  __property int32_t IniErrorCount = { read = FIniErrorCount, write = FIniErrorCount };
	__property bool IniDecaySample = { read = FIniDecaySample, write = FIniDecaySample };
 	__property bool RunDistDecay = { read = FRunDistDecay, write = FRunDistDecay };
	__property bool UpdatingLastValue = { read = GetUpdateLastValueRunning };
	__property bool PSTemp = { read = GetPSTemp };

  __property bool SampReqRunnig = { write = FSampReqRunnig };

	__property String PSName = { read = GetPSName };

	__property bool uCtrlZone = { read = FuCtrlZone, write = FuCtrlZone};
	__property int32_t uCtrlNodeIndex = { read = FuCtrlNodeIndex, write = FuCtrlNodeIndex };
};


class TCIISPowerSupply_PI : public TCIISPowerSupply
{
private:
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
  virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );

protected:

public:
  __fastcall TCIISPowerSupply_PI( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply_PI();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

__published:

};

class TCIISPowerSupply_FixVolt : public TCIISPowerSupply
{
private:
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall uCtrlIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
  virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
  __fastcall TCIISPowerSupply_FixVolt( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply_FixVolt();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

	virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );

	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:

};

class TCIISPowerSupply_FixVolt3A : public TCIISPowerSupply
{
private:
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall uCtrlIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
  virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
  virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
  __fastcall TCIISPowerSupply_FixVolt3A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply_FixVolt3A();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:

};

class TCIISPowerSupply_FixVolt01A : public TCIISPowerSupply
{
private:
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall uCtrlIniState();
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
  __fastcall TCIISPowerSupply_FixVolt01A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply_FixVolt01A();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
  virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:

};

class TCIISPowerSupply_FixVolt1A : public TCIISPowerSupply
{
private:
  virtual void __fastcall SM_PSIni();
  virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
  virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
  virtual void __fastcall SetDefaultValuesSubType();
  virtual void __fastcall uCtrlIniState();
  virtual void __fastcall SM_PSIniState();
  virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
  __fastcall TCIISPowerSupply_FixVolt1A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
  __fastcall ~TCIISPowerSupply_FixVolt1A();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
  virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
  virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:

};

class TCIISPowerSupply_FixVoltHV1A : public TCIISPowerSupply
{
private:
	virtual void __fastcall SM_PSIni();
	virtual void __fastcall PSIniCapability( TCIISBusPacket *BPIn );
	virtual void __fastcall PSIniCalibValues( TCIISBusPacket *BPIn );
	virtual void __fastcall SetDefaultValuesSubType();
	virtual void __fastcall SM_PSIniState();
	virtual void __fastcall uCtrlIniState();
	virtual void __fastcall PSSample( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleExt( TCIISBusPacket *BPIn );
	virtual void __fastcall PSSampleTemp( TCIISBusPacket *BPIn );
	virtual void __fastcall UpdateLastValues();
	virtual void __fastcall SM_UpdateLastValue();

protected:

public:
	__fastcall TCIISPowerSupply_FixVoltHV1A( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISPowerSupplyRec *SetPSRec, TObject *SetCIISParent );
	__fastcall ~TCIISPowerSupply_FixVoltHV1A();

	virtual void __fastcall PSSetOutputOff();
	virtual void __fastcall PSSetOutputState();

  virtual void __fastcall uCtrlNodeInfo( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlPowerSettings( const Byte *BusIntMsg );
	virtual void __fastcall uCtrlSample( const Byte *RecordingMem, int32_t i, int32_t RecNo, TDateTime SampleDateTime );
	virtual void __fastcall UpdateLastTempValues();
	virtual void __fastcall PSRequestTemp();
	virtual bool __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );

__published:

};

#endif
