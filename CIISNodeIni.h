//---------------------------------------------------------------------------

#ifndef CIISNodeIniH
#define CIISNodeIniH
//---------------------------------------------------------------------------
#include "CIISObj.h"
#include "CIISCommon.h"

class TCIISNodeIni : public TCIISObj
{
private:
  //CIISClientInt

  //CIISBusInt
  void __fastcall NodeIni( TCIISBusPacket *BPIn );
  void __fastcall SM_SensorIni();
  void __fastcall NodeIniCapability( TCIISBusPacket *BPIn );

  int32_t __fastcall GetZoneNo() { return NodeRec->ZoneNo; }
  int32_t __fastcall GetSerialNo() { return NodeRec->NodeSerialNo; }
  int32_t __fastcall GetCanAdr() { return NodeRec->NodeCanAdr; }
  int32_t __fastcall GetZoneCanAdr() { return NodeRec->ZoneCanAdr; }
  CIISNodeCapability __fastcall GetNodeCap() { return (CIISNodeCapability)NodeRec->NodeType; }
  void _fastcall SetZoneCanAdr( int32_t ZoneCanAdr ) { NodeRec->ZoneCanAdr = ZoneCanAdr; }

protected:
  //CIISClientInt
  CIISNodeRec *NodeRec;

  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

  //CIISBusInt
	//TCIISBIChannel *CIISBICh;
  int32_t IniRetrys;
  bool SensorIniRunning;
  NodeIniState NIState;
  int32_t T, TNextEvent;
  bool NodeDetected;
  bool CapabilityRecived;
  bool CalibValuesRecived;
  bool VerRecived;

  int32_t FIniErrorCount;


	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
		__fastcall TCIISNodeIni( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISNodeRec *SetNodeRec, TObject *SetCIISParent );
		__fastcall ~TCIISNodeIni();
 bool 	__fastcall ResetCIISBus();
 bool __fastcall NodeIniReady();

__published:

  __property int32_t ZoneNo = { read = GetZoneNo };
  __property int32_t SerialNo = { read = GetSerialNo };
  __property int32_t CanAdr = { read = GetCanAdr };
  __property int32_t ZoneCanAdr = { read = GetZoneCanAdr, write = SetZoneCanAdr };
  __property CIISNodeCapability NodCapability = { read = GetNodeCap };
//  __property TCIISBIChannel* BIChannel = { read = CIISBICh, write = CIISBICh };
  __property int32_t IniErrorCount = { read = FIniErrorCount };
};

#endif