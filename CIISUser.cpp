//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop




USEFORM("DebugWinU.cpp", DebugWin);
USEFORM("UnitMySQLLogin.cpp", FormMySQLLogin);
USEFORM("CIISOnTopMessage.cpp", FormOnTopMessage);
USEFORM("CIISDB.cpp", CIISDBModule); /* TDataModule: File Type */
USEFORM("CIISMainUser.cpp", CIISMainFrm);
USEFORM("CIISDBInit.cpp", CIISDBInitFrm);
//---------------------------------------------------------------------------
WINAPI int32_t WinMain(HINSTANCE, HINSTANCE, LPSTR, int32_t)
{
        HWND hWndApp=FindWindow("TCIISMainFrm", NULL);
        if ( hWndApp != NULL)
        {
            ShowMessage("Application already started!");
            return 0;
        }

        try
        {
                 Application->Initialize();
                 Application->Title = "Camur II Server";
		Application->CreateForm(__classid(TCIISMainFrm), &CIISMainFrm);
		Application->CreateForm(__classid(TFormOnTopMessage), &FormOnTopMessage);
		Application->CreateForm(__classid(TFormMySQLLogin), &FormMySQLLogin);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
