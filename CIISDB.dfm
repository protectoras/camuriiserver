object CIISDBModule: TCIISDBModule
  Height = 385
  Width = 570
  PixelsPerInch = 96
  object TabValuesAppend: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'DateTimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'Fraction'
        DataType = ftInteger
      end
      item
        Name = 'RecNo'
        DataType = ftInteger
      end
      item
        Name = 'SensorSerialNo'
        DataType = ftInteger
      end
      item
        Name = 'ValueType'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'ValueUnit'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Value'
        DataType = ftFloat
      end
      item
        Name = 'RawValue'
        DataType = ftFloat
      end
      item
        Name = 'SampleDateTime'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 464
    Top = 8
  end
  object TabValuesLPRAppend: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'DateTimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'Fraction'
        DataType = ftInteger
      end
      item
        Name = 'RecNo'
        DataType = ftInteger
      end
      item
        Name = 'SensorSerialNo'
        DataType = ftInteger
      end
      item
        Name = 'ValueType'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'ValueV'
        DataType = ftFloat
      end
      item
        Name = 'ValueI'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 464
    Top = 64
  end
  object TabValuesDecayAppend: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'DateTimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'Fraction'
        DataType = ftInteger
      end
      item
        Name = 'RecNo'
        DataType = ftInteger
      end
      item
        Name = 'SensorSerialNo'
        DataType = ftInteger
      end
      item
        Name = 'ValueType'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Value'
        DataType = ftFloat
      end
      item
        Name = 'SampleDateTime'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 464
    Top = 120
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Server=localhost'
      'User_Name=root'
      'Password=camur'
      'Database=ciislogger'
      'DriverID=MySQL'
      'TinyIntFormat=Integer')
    LoginPrompt = False
    Left = 24
    Top = 16
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrNone
    Left = 24
    Top = 72
  end
  object FDTableProject: TFDTable
    CachedUpdates = True
    IndexFieldNames = 'PrjName'
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'project'
    TableName = 'project'
    Left = 128
    Top = 8
  end
  object FDTableControllers: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'controllers'
    TableName = 'controllers'
    Left = 128
    Top = 64
  end
  object FDTableZones: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'zones'
    TableName = 'zones'
    Left = 128
    Top = 128
  end
  object FDTableSensors: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'sensors'
    TableName = 'sensors'
    Left = 128
    Top = 184
  end
  object FDTablePowerSupply: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'powersupply'
    TableName = 'powersupply'
    Left = 128
    Top = 240
  end
  object FDTableRecordings: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'recordings'
    TableName = 'recordings'
    Left = 128
    Top = 296
  end
  object FDTableEventLog: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'eventlog'
    TableName = 'eventlog'
    Left = 224
    Top = 128
  end
  object FDTableWLink: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'wlink'
    TableName = 'wlink'
    Left = 224
    Top = 8
  end
  object FDTableAlarm: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'alarm'
    TableName = 'alarm'
    Left = 224
    Top = 64
  end
  object FDQueryValues: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from valuesmonitor limit 200;')
    Left = 328
    Top = 8
  end
  object FDQueryValuesLPR: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from valueslpr limit 200;')
    Left = 328
    Top = 64
  end
  object FDQueryValuesDecay: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from valuesdecay limit 200;')
    Left = 328
    Top = 120
  end
  object FDTableValuesMisc: TFDTable
    CachedUpdates = True
    AggregatesActive = True
    Connection = FDConnection
    UpdateOptions.AssignedValues = [uvEDelete, uvEInsert, uvEUpdate]
    UpdateOptions.EnableDelete = False
    UpdateOptions.EnableInsert = False
    UpdateOptions.EnableUpdate = False
    UpdateOptions.UpdateTableName = 'valuesmisc'
    TableName = 'valuesmisc'
    Left = 328
    Top = 184
  end
  object FDTableValuesCalc: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'valuescalc'
    TableName = 'valuescalc'
    Left = 328
    Top = 248
  end
  object FDQuery: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'SELECT COUNT(*) FROM  valuesmonitor;')
    Left = 24
    Top = 216
  end
  object ClientDataSetValuesMisc: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'DateTimeStamp'
        DataType = ftDateTime
      end
      item
        Name = 'Fraction'
        DataType = ftInteger
      end
      item
        Name = 'SensorSerialNo'
        DataType = ftInteger
      end
      item
        Name = 'ValueType'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'ValueUnit'
        DataType = ftString
        Size = 4
      end
      item
        Name = 'Value'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 464
    Top = 184
  end
  object FDTableBIChannels: TFDTable
    CachedUpdates = True
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'bichannels'
    TableName = 'bichannels'
    Left = 216
    Top = 192
  end
end
