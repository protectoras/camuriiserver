//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIISDBInit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCIISDBInitFrm *CIISDBInitFrm;
//---------------------------------------------------------------------------
__fastcall TCIISDBInitFrm::TCIISDBInitFrm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TCIISDBInitFrm::Msg( String msg )
{
	MemoDB->Lines->Add( msg );
}

//---------------------------------------------------------------------------

void __fastcall TCIISDBInitFrm::MsgCl( TColor cl )
{
	MemoDB->Font->Color = cl;
}



