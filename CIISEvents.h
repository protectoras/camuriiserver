//---------------------------------------------------------------------------

#ifndef CIISEventsH
#define CIISEventsH
//---------------------------------------------------------------------------

#include "CIISObj.h"


class TCIISEvents : public TCIISObj
{
private:
  CIISEventRec *EventRec;
 bool __fastcall ThisRec( TCamurPacket *P );
 void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
 void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );

protected:
  //CIISClientInt
  void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
  void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );
  //CIISBusInt
	void __fastcall OnSysClockTick( TDateTime TickTime );
  void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
  void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
  void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );  


public:
  __fastcall TCIISEvents( TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt, TDebugWin *SetDebugWin, CIISEventRec *SetEventRec, TObject *SetCIISParent );
  __fastcall ~TCIISEvents();

void  __fastcall Log( CIIEventType EvType, CIIEventCode EvCode,
					  CIIEventLevel EvLevel, String EvString,
					  int32_t EvInt, double EvFloat, int32_t EvLogSize);


__published:

};


#endif
