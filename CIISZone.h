//---------------------------------------------------------------------------

#ifndef CIISZoneH
#define CIISZoneH
//---------------------------------------------------------------------------

#include "CIISObj.h"
#include "CIISNodeIni.h"
#include "CIISSensor.h"
#include "CIISAlarm.h"
#include "CIISPowerSupply.h"



class TCIISZone : public TCIISObj
{
private:


	bool __fastcall ThisRec( TCamurPacket *P );
	void __fastcall ReadRec( TCamurPacket *P, TCamurPacket *O ) ;
	void __fastcall WriteRec( TCamurPacket *P, TCamurPacket *O );
	void __fastcall RemoveSensor( TCamurPacket *P );
	void __fastcall RemovePS( TCamurPacket *P );

	void __fastcall DeleteSensor( TCIISSensor *Sensor );
	void __fastcall DeleteAlarm( TCIISAlarm *Alarm );
	void __fastcall DeletePS( TCIISPowerSupply *PS );
	void __fastcall DeleteNI( TCIISNodeIni *NI );


	virtual bool __fastcall StartMonitor();
	virtual bool __fastcall StartDecay();
	virtual bool __fastcall StartLPR();
	virtual bool __fastcall StartMonitorExtended();
	virtual bool __fastcall StartScheduledValues();
	virtual bool __fastcall StartCTNonStat();
	virtual bool __fastcall StartCTStat();
	virtual bool __fastcall StartRExt();
	virtual bool __fastcall StopRecording();


 int32_t __fastcall GetZoneNo() { return ZoneRec->ZoneNo; }
 int32_t __fastcall GetZoneSampInterval() {return ZoneRec->ZoneSampInterval; }
 String __fastcall GetZoneName() { return ZoneRec->ZoneName; }
 int32_t __fastcall GetBIChSerNo() { return ZoneRec->BIChSerNo; }
 int32_t __fastcall GetRecType() { return ZoneRec->RecType; }
 bool __fastcall GetAlarmStatus() { return ZoneRec->ZoneAlarmStatus; }

protected:

	CIISZoneRec *ZoneRec;
	CIISRecordingRec *RR;
	TCIISProject *Prj;
	//TCIISBIChannel *BICh;
	TCIISPowerSupply *InstalledPS;
	TCIISSensor *InstalledSensor;

	bool ForceStopRec, SMRunning_RestartZone;
	int32_t OnSlowClockTick, State_RestartZone, TRestart, TNextEvent_Restart;
	//int32_t ReadTempDelay;
	bool TempLastValueRequest;

	//Schedule

	int32_t FMonitorStartDelay, MSDCounter;

	//CIISClientInt
	void __fastcall ParseCommand_Begin( TCamurPacket *P, TCamurPacket *O );
	void __fastcall ParseCommand_End( TCamurPacket *P, TCamurPacket *O );

	//CIISBusInt
	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
	virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	virtual void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	virtual void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

	void __fastcall RemoveNI( TCIISBusPacket *BPIn );
	void __fastcall UpdateNode( TCIISBusPacket *BPIn );

  virtual void __fastcall PSOnAlarm();

public:
	__fastcall TCIISZone(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
											 TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
											 TObject *SetCIISParent, bool NewZone );
	__fastcall ~TCIISZone();

	void __fastcall CancelDecay();
	virtual bool __fastcall ResetCIISBus();
	virtual void __fastcall RestartZone();
	virtual void __fastcall PrepareShutdown();
	virtual void __fastcall StartSchedule();
	virtual void __fastcall UpdateLastValues();
	virtual int32_t __fastcall GetNodeCount();

	bool __fastcall SetCtrlName( String CtrlName );
	void __fastcall AddChild( TCIISObj *Child );
	bool __fastcall NodeIniReady();
	int32_t __fastcall GetIniErrorCount();
	bool __fastcall UpdateAlarmStatus();
  void __fastcall ResetCANAlarm();


	//TCIISBIChannel* __fastcall GetBIChannel();
	TCIISSensor* __fastcall GetSensor( int32_t Sensor_No );
	TCIISPowerSupply* __fastcall GetPS( int32_t PS_No );

__published:

  __property int32_t ZoneNo = { read = GetZoneNo };
	__property String ZoneName = { read = GetZoneName };
	__property int32_t BIChSerNo = { read = GetBIChSerNo };
	__property int32_t RecType = { read = GetRecType };
	__property bool AlarmStatus = { read = GetAlarmStatus };
	__property int32_t ZoneSampInterval = { read = GetZoneSampInterval };
  __property int32_t MonitorStartDelay = { write = FMonitorStartDelay };

};


// Zone_Online

class TCIISZone_Online : public TCIISZone
{
private:

	//SM_RestartZone
	int32_t RestartRecType,RestartRecNo, RestartScheduleStatus;
	bool SMRunning_Schedule;
	int32_t TDecayDurationLeft;

	//SM_Recordings

	int32_t RequestTag, ExpectedTag, PrevTag;
	TDateTime SampleTimeStamp, StartSampleTimeStamp;
	uint32_t T, TNextEvent, TInterval, TWarmUp;
	int32_t MaxCWChCount;
	uint32_t AppUpdDelayAdjusted;
	bool FastApplyUppdate, MonitorFirstSample;

	// MRE State Machine
	bool Sample_MRE, SMRunning_MRE;
	uint32_t T_MRE, TNextEvent_MRE, State_MRE, MRESelectedCh;

	// CorroWatch State Machine
	bool Sample_CW, SMRunning_CW;
	uint32_t T_CW, TNextEvent_CW, State_CW, CWSelectedCh;

	bool LPR_CW, SMRunning_LPR_CW, SMRunning_LPR_Ladder;
	uint32_t T_LPR_CW, TNextEvent_LPR_CW, State_LPR_CW, LPR_CWSelectedCh;
	uint32_t T_LPR_Ladder, TNextEvent_LPR_Ladder, State_LPR_Ladder, LPR_LadderSelectedCh;

	// Ladder State Machine
	bool Sample_Ladder, SMRunning_Ladder;
	uint32_t T_Ladder, TNextEvent_Ladder, State_Ladder, LadderSelectedCh;

	//PS Temp sample
	bool Sample_PSTemp, SMRunning_PSTemp;
	uint32_t T_PSTemp, TNextEvent_PSTemp, State_PSTemp;

	//CTx1 Temp sample
	bool Sample_CTx1Temp, SMRunning_CTx1Temp;
	int32_t T_CTx1Temp, TNextEvent_CTx1Temp, State_CTx1Temp;

	// LPR Apply Update State Machine

	bool SMRunning_LPR;
	uint32_t T_LPR, TNextEvent_LPR, State_LPR;

	bool UpdateLastValueRunning;
	uint32_t TimerLastVal, TimeLastValEvent;//, T2State;
	bool WUEnabled;
	bool MultiChSample, MultiChSampleRunning;
	int32_t LPRRange, LPRStep, LPRTOn, LPRTOff, LPRMode, LPRVersion;
	bool MonitorExtended;
	int32_t MonitorExtendedRecNo;

	MonitorState MState;
	DecayState DState;
	int32_t CTState, CTSelectedCh;
	int32_t TDuration, TDecayTotDuration;
	int32_t TDecayInstantOffDelay, TIniInterval, TDecayIniDuration, TDistDecayIniDuration;
  bool OnlyDistDecay;

	virtual bool __fastcall StartMonitor();
	virtual bool __fastcall StartDecay();
	virtual bool __fastcall StartLPR();
	virtual bool __fastcall StartMonitorExtended();
	virtual bool __fastcall StartScheduledValues();
	virtual bool __fastcall StartCTNonStat();
	virtual bool __fastcall StartCTStat();
	virtual bool __fastcall StartRExt();
	virtual bool __fastcall StopRecording();

	void __fastcall SM_RestartZone();
	void __fastcall PSIniState();
	void __fastcall SM_Schedule();
	void __fastcall SM_UpdateLastValue();
	void __fastcall SM_Monitor();
	void __fastcall SM_Sample_CW();
	void __fastcall SM_Sample_MRE();
	void __fastcall SM_Sample_Ladder();
	void __fastcall SM_Sample_PSTemp();
	void __fastcall SM_Sample_CTx1Temp();
	void __fastcall SM_LPR_CW();
	void __fastcall SM_LPR_Ladder();
	void __fastcall SM_LPR();
	void __fastcall LPRSelectRange( int32_t Range );
	bool __fastcall LPRIni();
	bool __fastcall LPREnd();
	bool __fastcall ResumeDecay(int32_t ResumeRecNo, int32_t TDecayDurationLeft);
	void __fastcall SM_Decay();
	void __fastcall SM_CTNonStat();
	void __fastcall SM_CTStat();
	void __fastcall SM_RExt();

	void __fastcall CheckSampleRecived( int32_t RequestedTag );
	void __fastcall ResetLastStoredTag();


	int32_t __fastcall MaxSensorWarmUp();
	void __fastcall SensorWarmUp( bool On );
	void __fastcall PSSetOutputOff();
	void __fastcall PSSetOutputState();

	void __fastcall RequestSample( TDateTime STime );
	int32_t __fastcall GetLPRVersionInZone();
	void __fastcall SetIniDecaySample( bool IniDecaySample );
  void __fastcall SetRunDistDecay( bool RunDistDecay );
	int32_t __fastcall GetMaxCWChCount();

	void __fastcall CTSelectMode( int32_t Mode );
	void __fastcall CTSelectRange( int32_t Range );
	void __fastcall CTSelectRFreq( int32_t Freq );
	void __fastcall CTSelectCh( int32_t Ch );
	void __fastcall CTDACalib();
	void __fastcall CTOffsetAdj();
	void __fastcall SetPSLastValueFlag();
	void __fastcall CTRequestSample( int32_t CTTag, TDateTime STime );
	void __fastcall CTRequestTemp( int32_t CTTag, TDateTime STime );
	void __fastcall CWRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode);
	double __fastcall CTZRAChange();

	//HUM
	void __fastcall HUMRequestSample( int32_t CTTag, TDateTime STime );

	//CW
	bool __fastcall CWInZone();
	bool __fastcall CWUpdatingLastValue();
	void __fastcall CWSelectMode( int32_t Mode );
	void __fastcall CWSelectRange( int32_t Range );
	void __fastcall CWSelectRFreq( int32_t Freq );
	void __fastcall CWSelectCh( int32_t Ch );
	void __fastcall CWDACalib();
	void __fastcall CWOffsetAdj();
	void __fastcall CWRequestSample( int32_t CTTag, TDateTime STime );
	void __fastcall CWRequestTemp( int32_t CTTag, TDateTime STime );
	void __fastcall CWUpdateLastValues();
	//MRE
	bool __fastcall MREInZone();
	bool __fastcall MREUpdatingLastValue();
	void __fastcall MRESelectMode( int32_t Mode );
	void __fastcall MRESelectRange( int32_t Range );
	void __fastcall MRESelectRFreq( int32_t Freq );
	void __fastcall MRESelectCh( int32_t Ch );
	void __fastcall MREDACalib();
	void __fastcall MREOffsetAdj();
	void __fastcall MRERequestSample( int32_t CTTag, TDateTime STime );
	void __fastcall MRERequestTemp( int32_t CTTag, TDateTime STime );
	void __fastcall MREUpdateLastValues();
	//Ladder
	bool __fastcall LadderInZone();
	bool __fastcall LadderUpdatingLastValue();
	void __fastcall LadderSelectMode( int32_t Mode );
	void __fastcall LadderSelectRange( int32_t Range );
	void __fastcall LadderSelectRFreq( int32_t Freq );
	void __fastcall LadderSelectCh( int32_t Ch );
	void __fastcall LadderDACalib();
	void __fastcall LadderOffsetAdj();
	void __fastcall LadderRequestSample( int32_t CTTag, TDateTime STime );
	void __fastcall LadderRequestTemp( int32_t CTTag, TDateTime STime );
	void __fastcall LadderRequestLPRStart( Word LPRRange, Word LPRStep, Word LPRTOn, Word LPRTOff, Word LPRMode);
	void __fastcall LadderUpdateLastValues();
	//PSTemp
	bool __fastcall PSTempInZone();
	bool __fastcall PSUpdatingLastValue();
	void __fastcall PSRequestTemp( TDateTime STime );
	void __fastcall PSUpdateLastTempValues();
	//CTTemp
	bool __fastcall CTx1TempInZone();
	bool __fastcall CTx1UpdatingLastValue();
  bool __fastcall CTx1UpdatingTemp();
	void __fastcall CTx1UpdateLastTempValues();
	void __fastcall CTx1RequestTemp( int32_t CTTag, TDateTime STime );

	void __fastcall RequestDecayIni( Word StartDelay, Word POffDelay, Word NoOfIniSamples, Word IniInterval );
	void __fastcall SetPSOutputTmpOffFlag();

protected:
	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
	virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	virtual void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	virtual void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

	virtual void __fastcall PSOnAlarm();

public:
	__fastcall TCIISZone_Online(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
											 TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
											 TObject *SetCIISParent, bool NewZone );

	__fastcall ~TCIISZone_Online();

	virtual bool __fastcall ResetCIISBus();
	virtual void __fastcall RestartZone();
	virtual void __fastcall PrepareShutdown();
	virtual void __fastcall StartSchedule();
	virtual void __fastcall UpdateLastValues();
	virtual int32_t __fastcall GetNodeCount();

__published:
};


// Zone_uCtrl

class TCIISZone_uCtrl : public TCIISZone
{
private:
	bool  SMRunning_RestartuCtrl, InstalluCtrlNodeReady, ReaduCtrlDataReady,
				ReaduCtrlPowerSettingReady,ReaduCtrlRecStatus;
	int32_t uCtrlNodeCount, uCtrlNodeIndex;
	int32_t uCtrlInPointer, uCtrlDataLenght, uCtrlSampInt, uCtrlRecStatus;
	int32_t PSIndex, P4Index, RequestIndex;
	ProcMessFP OrgIncommingBIChMessage;
	Byte RecordingMem[4*128*1024];
	TDateTime uCtrlSampleTimeStamp, uCtrlRecStart, uCtrlDataTransferredUntil;
	int32_t DebugCnt;
  String DebugStr;

	virtual bool __fastcall StartMonitor();
	virtual bool __fastcall StartDecay();
	virtual bool __fastcall StartLPR();
	virtual bool __fastcall StartMonitorExtended();
	virtual bool __fastcall StartScheduledValues();
	virtual bool __fastcall StartCTNonStat();
	virtual bool __fastcall StartCTStat();
	virtual bool __fastcall StartRExt();
	virtual bool __fastcall StopRecording();

	void __fastcall SM_RestartuCtrl();
	void __fastcall InstalluCtrlNode( const Byte *BusIntMsg );
	void __fastcall UpdateuCtrlNode( const Byte *BusIntMsg );
	void __fastcall ReaduCtrlData();
	void __fastcall ReaduCtrlPowerSettings( const Byte *BusIntMsg );
	void __fastcall IncomingBIChMessage(const Byte *BusIntMsg, PVOID BIChannel);
	bool __fastcall GetuCtrlZone() { return ZoneRec->uCtrl; }
	bool __fastcall GetuCtrlConnected() { return ZoneRec->uCtrlConnected; }
	void __fastcall SetuCtrlConnected( bool uCtrlConnected ) { ZoneRec->uCtrlConnected = uCtrlConnected; }

protected:
	virtual void __fastcall OnSysClockTick( TDateTime TickTime );
	virtual void __fastcall AfterSysClockTick( TDateTime TickTime ) { return; }
	virtual void __fastcall ParseCIISBusMessage_Begin( TCIISBusPacket *BPIn );
	virtual void __fastcall ParseCIISBusMessage_End( TCIISBusPacket *BPIn );

public:
	__fastcall TCIISZone_uCtrl(TCIISDBModule *SetDB, TCIISBusInterface *SetCIISBusInt,
											 TDebugWin *SetDebugWin, CIISZoneRec *SetZoneRec,
											 TObject *SetCIISParent, bool NewZone );
	__fastcall ~TCIISZone_uCtrl();

	virtual bool __fastcall ResetCIISBus();
	virtual void __fastcall RestartZone();
	virtual void __fastcall PrepareShutdown();
	virtual void __fastcall StartSchedule();
	virtual void __fastcall UpdateLastValues();
	virtual int32_t __fastcall GetNodeCount();

	//void __fastcall SetBIChannel( TCIISBIChannel *SetBICh );
	void __fastcall RequestuCtrlNodeInfo( int32_t uCtrlNodeIndex );
 	void __fastcall uCtrlSetPower( double SetVoltage, double SetCurrent,
																 int32_t Mode, bool VOutEnabled);

__published:
	__property bool uCtrlZone = { read = GetuCtrlZone };
	__property bool uCtrlConnected = { read = GetuCtrlConnected, write = SetuCtrlConnected };

};

#endif
