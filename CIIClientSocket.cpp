//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "CIIClientSocket.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall TCIIClientSocket::TCIIClientSocket(MessInQueueFP SetMInQ, String SetIP, int32_t SetPortNo, TDebugWin * SetDebugWin)
{
  MInQ = SetMInQ;
  DW = SetDebugWin;
  IPAdr = SetIP;
	PortNo = SetPortNo;
	MessageList = new TList;   // K� f�r inkommande meddelanden fr�n ServerSocket (Camur II Controller)

	FServerConnected = false;
	FComTimeout = false;

  SocketInputIdx = 0;
	ClientSocket = NULL;


  ClientSocket = new TClientSocket( NULL );
	ClientSocket->OnConnect = ServerConnect;
  ClientSocket->OnLookup = ServerLookup;
  ClientSocket->OnDisconnect = ServerDisconnect;
	ClientSocket->OnError = ServerOnError;
  ClientSocket->OnRead = ServerRead;
	ClientSocket->Tag = 0;

  #if DebugClientSocketEvent == 1
  Debug("CIIClientSocket Created on port " + IntToStr( PortNo ));
  #endif
}

__fastcall TCIIClientSocket::~TCIIClientSocket()
{
	CloseCom();
	delete ClientSocket;
	delete MessageList;
}

void __fastcall TCIIClientSocket::SetComTimeout( int32_t SetT_Timeout )
{
	FT_Timeout = SetT_Timeout;
}

void __fastcall TCIIClientSocket::OpenCom(String SetIP)
{
  int32_t p;

	IPAdr = SetIP;
  PortNo = 5020;

	p = IPAdr.Pos("::");
	if( p > 0 )
	{
		PortNo = StrToInt( IPAdr.SubString( p+2, p+6));
		IPAdr = IPAdr.SubString( 0, p-1 );
	}
	else
	{
		p = IPAdr.Pos(":");
		if( p > 0 )
		{
			PortNo = StrToInt( IPAdr.SubString( p+1, p+5));
			IPAdr = IPAdr.SubString( 0, p-1 );
		}
  }

  ClientSocket->Host = IPAdr;
	ClientSocket->Port = PortNo;
	FComTimeout = false;

	ClientSocket->Open();

  #if DebugClientSocketEvent == 1
  Debug( "OpenCom. IPADr: " + IPAdr );
  #endif
}

void __fastcall TCIIClientSocket::CloseCom()
{

  TCamurPacket *r;

	ClientSocket->Close();

	// K� f�r inkommande meddelanden
  for( int32_t i = 0; i < MessageList->Count; i++ )
  {
		r = (TCamurPacket*)MessageList->Items[i];
		delete r;
		MessageList->Delete(i);
  }

  #if DebugClientSocketEvent == 1
  Debug( "CloseCom. Connection to " + IPAdr );
  #endif

}

#pragma argsused
void __fastcall TCIIClientSocket::ServerConnect(TObject * Sender, TCustomWinSocket * Socket)
{
  SocketInputIdx = 0;
	CWSocket = Socket;
	FServerConnected = true;
	FComTimeout = false;
	FTOut = Now() + TDateTime( FT_Timeout * OneSecond );
  ResponseCount = 0;

  #if DebugClientSocketEvent == 1
	Debug( "ClientSocket_Connect. Connected to " + Socket->RemoteAddress );
  #endif
}

#pragma argsused
void __fastcall TCIIClientSocket::ServerLookup(TObject * Sender, TCustomWinSocket * Socket)
{
  #if DebugClientSocketEvent == 1
	Debug( "ClientSocket_Lookup. Lookup connection to " + IPAdr );
  #endif
	;
}

#pragma argsused
void __fastcall TCIIClientSocket::ServerDisconnect(TObject * Sender, TCustomWinSocket * Socket)
{
	CWSocket = NULL;
  FServerConnected = false;

  #if DebugClientSocketEvent == 1
	Debug( "ClientSocket_Disconnect. Closed connection to " + Socket->RemoteAddress );
  #endif
}

#pragma argsused
void __fastcall TCIIClientSocket::ServerOnError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int32_t &ErrorCode)
{
	ECode = ErrorCode;
	ErrorCode = 0;
	CWSocket = NULL;

  #if DebugClientSocketEvent == 1
	Debug( "ClientSocket_OnError. Error = " + IntToStr(ECode) );
  #endif
}

#pragma argsused
void __fastcall TCIIClientSocket::ServerRead(TObject * Sender, TCustomWinSocket * Socket)
{
  int32_t b;

  b = Socket->ReceiveBuf( SocketInBuf, SocketInBufSize );
  while( b > 0 )
  {
	if( b > 0 ) AddSocketInputQueue( SocketInBuf, b );
    b = Socket->ReceiveBuf( SocketInBuf, SocketInBufSize );
  }
}

void __fastcall TCIIClientSocket::AddSocketInputQueue(char* Buf, int32_t N)
{
  Move( Buf, SocketInputQueue + SocketInputIdx, N );
  SocketInputIdx += N;
	#if DebugClientSocket == 1
  Debug( "SocketInputQueue: Adding " + IntToStr( N ) + ", L = " + IntToStr( SocketInputIdx ) + " bytes" );
  #endif
  while( ParseMessages() );  //Packar upp och kontrollerar inkomna meddelanden
  if( MInQ != NULL ) MInQ();  // Signalera att meddelande finns att h�mta
}

bool __fastcall TCIIClientSocket::ParseMessages()
{
	int32_t c, n, ArgStart, ArgEnd, ArgNum;
  bool FirstArg;
  String s, ArgVal;
  TCamurPacket *p;
  TDataArg *Arg;
  int32_t start = -1;
  int32_t stop = -1;
  int32_t x = 0;

  while( ((start == -1) || (stop == -1)) && ( x < SocketInputIdx ))
  {
    if(( start == -1) && (SocketInputQueue[x] == 1))
    {
      start = x;
      stop = start + SocketInputQueue[x+4]+ SocketInputQueue[x+5]*256+9;
      if( stop > SocketInputIdx )
      {
        stop = -1;
        x = SocketInputIdx;
      }
    }
    x++;
  }
  if( (start >= 0) && (stop >= 0) )
  {
		#if DebugClientSocket == 1
    Debug( "SocketInputQueue: Message found " + IntToStr( start ) + " - " + IntToStr( stop ));
    #endif

    SocketInputIdx = SocketInputIdx - stop;
    p = new TCamurPacket;
    p->MessageType = SocketInputQueue[start+6];
    p->MessageNumber = SocketInputQueue[start+7];
    p->MessageCode = SocketInputQueue[start+8];
    p->MessageCommand = SocketInputQueue[start+9];
    p->MessageTable = SocketInputQueue[start+10];
    c = SocketInputQueue[start+11]+SocketInputQueue[start+12]*256;
    for( int32_t i = 1; i <= c ; i++ )
      p->MessageData = p->MessageData + Char(SocketInputQueue[start+12+i]);
    // Dela upp MessageData i argument
    p->Args->Clear();
    s = p->MessageData;
    n = 99;

    while( ( s.Length() > 0 ) && (( s[1] == 0xA ) || ( s[1] == 0xD )) ) s.Delete(1,1);
    FirstArg = true;
    while( s.Length() > 0 )
    {
      ArgStart =  s.Pos( "=" );
      try
      {
        ArgNum = StrToInt( s.SubString( 1, ArgStart - 1 ));
        s.Delete( 1, ArgStart ); // Remove "xx="
      }
      catch (Exception &exception)
      {
        ArgNum = n;
      }
      ArgEnd = min( s.Pos( "\n" ), s.Pos( "\r" ));
      if( ArgEnd == 0 ) ArgEnd = s.Length();
      ArgVal = s.SubString(1, ArgEnd - 1 );
      s.Delete(1, ArgEnd ); // Remove arg
      // Remove termination sign/signs
      while(( s.Length() > 0 ) && (( s[1] == 0xA ) || ( s[1] == 0xD )))
        s.Delete( 1, 1 );
      if( FirstArg )
      {
        Arg = (TDataArg*)p->Args->Add();
        Arg->Num = ArgNum;
        n = ArgNum;
        Arg->Val = ArgVal;
        FirstArg = false;
      }
      else
      {
        if( Arg->Num == ArgNum ) Arg->Val = Arg->Val + "\r" + "\n" + ArgVal;
        else
        {
          Arg = (TDataArg*)p->Args->Add();
          Arg->Num = ArgNum;
          n = ArgNum;
          Arg->Val = ArgVal;
        }
      }
    }
    MessageList->Add( p );
		Move( SocketInputQueue + stop, SocketInputQueue, SocketInputIdx );
		FTOut = Now() + TDateTime( FT_Timeout * OneSecond );

		#if DebugClientSocket == 1
    Debug( "Recived on port " + IntToStr( PortNo ) );
    Debug( "Type: " + IntToStr( p->MessageType ) +
           " / Number: " + IntToStr( p->MessageNumber ) +
           " / Code: " + IntToStr( p->MessageCode ) +
           " / Command: " + IntToStr( p->MessageCommand ) +
           " / Table: " + IntToStr( p->MessageTable ) +
           " / Length: " + IntToStr( p->MessageData.Length() ));
    Debug( "Data: " + p->MessageData );
    #endif

    return true;
	}
  else if(( start > 0 ) )
  {
		#if DebugClientSocket == 1
    Debug( "SocketInputQueue: Start no stop. Skip " + IntToStr( start ) + " bytes before start");
    #endif

    Move( SocketInputQueue + start, SocketInputQueue, SocketInputIdx - start );
    SocketInputIdx = SocketInputIdx - start;
	}

	#if DebugClientSocket == 1
  Debug( "SocketInputQueue = "+IntToStr( SocketInputIdx )+" bytes" );
  #endif
  return false;
}

void __fastcall TCIIClientSocket::SendMessage(TCamurPacket *Request)
{
  int32_t c;

  SocketOutBuf[0] = 1;
  SocketOutBuf[1] = 50;
  SocketOutBuf[2] = ResponseCount & 255;
  SocketOutBuf[3] = ResponseCount >> 8;
  c = Request->MessageData.Length() + MessageHeaderLength;
  SocketOutBuf[4] = c & 255;
  SocketOutBuf[5] = c >> 8;
  SocketOutBuf[6] = Request->MessageType;
  SocketOutBuf[7] = Request->MessageNumber;
  SocketOutBuf[8] = Request->MessageCode;
	SocketOutBuf[9] = Request->MessageCommand;
  SocketOutBuf[10] = Request->MessageTable;
  SocketOutBuf[11] = Request->MessageData.Length() & 255;
  SocketOutBuf[12] = Request->MessageData.Length() >> 8;
  for( int32_t i = 1; i <= Request->MessageData.Length(); i++ )
	SocketOutBuf[12+i] = Byte(Request->MessageData[i]);
  SocketOutBuf[13 + Request->MessageData.Length()] = 3;
  SocketOutBuf[14 + Request->MessageData.Length()] = 0;
  SocketOutBuf[15 + Request->MessageData.Length()] = 0;

	if( CWSocket != NULL )
	{
		CWSocket->SendBuf(SocketOutBuf, 16 + Request->MessageData.Length());
	}

	ResponseCount++;



  #if DebugClientSocket == 1
  Debug( "Sent to port " + IntToStr( PortNo ) );
  Debug( "Type: " + IntToStr( Request->MessageType ) +
         " / Number: " + IntToStr( Request->MessageNumber ) +
         " / Code: " + IntToStr( Request->MessageCode ) +
         " / Command: " + IntToStr( Request->MessageCommand ) +
         " / Table: " + IntToStr( Request->MessageTable ) +
         " / Length: " + IntToStr( Request->MessageData.Length() ));
  Debug( "Data: " + Request->MessageData );
  #endif
}

TCamurPacket* __fastcall TCIIClientSocket::GetReply(int32_t RequestNo)
{
	TCamurPacket *r;

	r = NULL;

	for( int32_t i = 0; i < MessageList->Count; i++ )
	{
    r = (TCamurPacket*)MessageList->Items[i];
    if( r->MessageNumber == RequestNo )
		{
			MessageList->Delete(i);

			#if DebugClientSocket == 1
			Debug( "Reply found: MSG Number: " + IntToStr( r->MessageNumber ));
			#endif

			break;
		}
		else
		{
			r = NULL;
		}
	}

	if( r == NULL && ( Now() > FTOut )) FComTimeout = true;

	return r;
}

void __fastcall TCIIClientSocket::Debug(String Message)
{
  if( DW != NULL ) DW->Log(Message);
}

void __fastcall TCIIClientSocket::SetDebugWin(TDebugWin * SetDebugWin)
{
  DW = SetDebugWin;
}



